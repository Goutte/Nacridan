<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
require_once (HOMEPATH . "/factory/MissionFactory.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/Land.inc.php");
require_once (HOMEPATH . "/lib/Map.inc.php");
require_once (HOMEPATH . "/lib/Behavior.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

class AIObject
{

    public $m_db;

    public $m_map1;

    public $m_map2;

    public $m_behavior1;

function AIObject()
    {
	}
function init()
    {
        $this->m_db = $db = DB::getDB();
        $this->m_updateNPCGateTime = 0;
        $this->m_map1 = new Map($this->m_db, 1);
        $this->m_behavior1 = new Behavior($this->m_db);
    }
	
	    /* ******************************************************************** ROUTINE DE FONCTIONNEMENT ************************************ */
    function IncreaseLoyalty()
    {
        // Augmentation de 1% de loyauté dans les villages dont la loyauté est de plus 50%
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** Augmentation loyauté =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM City WHERE id_Player!=0 AND loyalty > 49 AND loyalty<100", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $dbv = new DBCollection("UPDATE City SET loyalty=loyalty+1 WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            $dbc->next();
        }
        echo "**************** END  Augmentation Loyauté******************\n\n";
    }

    function StoreInactivePlayer()
    {
        // Mise en chapelle des perso inactif pendant plus deux semaines.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** InactivePlayer =" . $strf . "\n";
        $format2 = "Y-m-d H:i:s";
        $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
        $date = date($format2, $time);
        $dbc = new DBCollection("SELECT id, resurrectid FROM Player WHERE status='PC' and room <>21 AND  playatb < '" . $date . "'", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            if ($dbc->get("resurrectid") > 0) {
                $dbx = new DBCollection("SELECT * FROM Building WHERE id=" . $dbc->get("resurrectid"), $this->m_db);
                $dbv = new DBCollection(
                    "UPDATE Player SET room=21, inbuilding=" . $dbx->get("id") . ", x=" . $dbx->get("x") . ", y=" . $dbx->get("y") . " WHERE id=" . $dbc->get("id"), $this->m_db, 0, 
                    0, false);
            }
            $dbc->next();
        }
        echo "**************** END  inactifPlayer******************\n\n";
    }

    function deleteEquipment()
    {
        // Suppression des objets au sol après une durée de 10 jours.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** deleteEquipment =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM Equipment WHERE state='ground' AND id_Player=0 AND room=0 AND id_Shop=0", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $time = (time() + date("I") * 3600) - gmstrtotime($dbc->get("dropped"));
            if ($time > 10 * 24 * 3600) {
                echo "\n" . $dbc->get("id") . "-";
                $dbd = new DBCollection("DELETE FROM Equipment WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            }
            $dbc->next();
        }
        echo "\n**************** END deleteEquipment ******************\n\n";
    }

    function deleteNewsPJ()
    {
        // Suppression des annonces des pj au bout d'un mois.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** deleteNewsPJ =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM News WHERE type=0", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $time = (time() + date("I") * 3600) - gmstrtotime($dbc->get("date"));
            if ($time > 30 * 24 * 3600)
                $dbd = new DBCollection("DELETE FROM News WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            $dbc->next();
        }
        echo "**************** END deleteNewsPj ******************\n\n";
    }

    function payBankInterest()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        $day = date("d");
        echo "**************** PayBankInterest =" . $strf . "\n";
        if ($day % 7 == 2) {
            $dbc = new DBCollection("UPDATE Player SET moneyBank=moneyBank+moneyBank/400+1 WHERE status='PC' AND moneyBank != 0", $this->m_db, 0, 0, false);
            echo " Interest paid\n";
        }
        echo "**************** END PayBankInterest ******************\n\n";
    }

    function deleteCorpse()
    {
        // Suppression des dépouilles cuir et écaille au sol après une durée de 3 jours.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** deleteCorpse =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM Exploitation WHERE type='body'", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $time = (time() + date("I") * 3600) - gmstrtotime($dbc->get("date"));
            if ($time > 3 * 24 * 3600)
                $dbd = new DBCollection("DELETE FROM Exploitation WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            $dbc->next();
        }
        echo "**************** END deleteCorpse ******************\n\n";
    }

    function autoRepair()
    {
        // Réparation automatique des batiments des grandes cités tous les jours.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** autoRepair =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM Building WHERE currsp < sp AND repair=3", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $speed = 1;
            if ($dbc->get("repair") == 3) {
                $dbv = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=11 AND id_City=" . $dbc->get("id_City"), $this->m_db);
                $speed = $dbv->get("level");
            }
            
            $repair = 3;
            $hp = min($dbc->get("currsp") + mt_rand(floor(CONSTRUCTION_SPEED_MIN / 4), floor(CONSTRUCTION_SPEED_MAX / 4)) * $speed, $dbc->get("sp"));
            if ($hp == $dbc->get("sp"))
                $repair = 0;
            $dbd = new DBCollection("UPDATE Building SET repair=" . $repair . ",  currsp=" . $hp . " WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            
            $dbc->next();
        }
        
        $dbx = new DBCollection("UPDATE Building SET repair=0 WHERE repair=3 AND currsp=sp", $this->m_db, 0, 0, false);
        echo "**************** END autorepair ******************\n\n";
    }

    function destroyBuilding()
    {
        // Réparation automatique des batiments des grandes cités tous les jours.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** destroyBuilding =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM Building WHERE repair=2", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $dbv = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=11 AND id_City=" . $dbc->get("id_City"), $this->m_db);
            $speed = $dbv->get("level");
            $hp = max($dbc->get("progress") - mt_rand(CONSTRUCTION_SPEED_MIN * 1.8, CONSTRUCTION_SPEED_MAX * 1.8) * $speed, 0);
            if ($hp == 0) {
                $building = new Building();
                $building->load($dbv->get("id"), $this->m_db);
                require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
                $BasicActionFactory = new BasicActionFactory();
                $BasicActionFactory::collapseBuilding($building, $building, $this->m_db);
            } else
                $dbd = new DBCollection("UPDATE Building SET progress=" . $hp . " WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            
            $dbc->next();
        }
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE (id_BasicBuilding=27 or id_BasicBuilding=15)", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $sp = $dbc->get("sp");
            $hp = max($dbc->get("currsp") - max(1, floor(($sp * 1.5 / 100))), 0);
            if ($hp == 0) {
                $dbd = new DBCollection("DELETE FROM Building WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            } else
                $dbd = new DBCollection("UPDATE Building SET currsp=" . $hp . " WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            $dbc->next();
        }
        echo "**************** END destroyBuilding ******************\n\n";
    }

    function construction()
    {
        // Construction des bâtiments
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** Construction =" . $strf . "\n";
        $dbc = new DBCollection(
            "SELECT Building.* FROM Building inner join City on Building.id_City = City.id and City.captured > 3 and City.id_player > 0 WHERE progress!=sp AND repair=1", 
            $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $dbv = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=11 AND id_City=" . $dbc->get("id_City"), $this->m_db);
            $repair = 1;
            if (! $dbv->eof()) {
                $hp = min($dbc->get("progress") + mt_rand(CONSTRUCTION_SPEED_MIN, CONSTRUCTION_SPEED_MAX) * $dbv->get("level"), $dbc->get("sp"));
                if ($hp == $dbc->get("sp")) {
                    $repair = 0;
                    $dbl = new DBCollection(
                        "UPDATE Player SET room=1, inbuilding=" . $dbc->get("id") . " WHERE x=" . $dbc->get("x") . " AND y=" . $dbc->get("y") . " AND map=" . $dbc->get("map"), 
                        $this->m_db, 0, 0, false);
                }
                $dbd = new DBCollection("UPDATE Building SET repair=" . $repair . ", progress=" . $hp . " WHERE id=" . $dbc->get("id"), $this->m_db, 0, 0, false);
            }
            $dbc->next();
        }
        echo "**************** END contruction ******************\n\n";
    }

    function updateEquipment()
    {
        // Renouvellement du stock des échoppes tous les 4 jours.
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** updateEquiment =" . $strf . "\n";
        $dbc = new DBCollection("SELECT * FROM Building WHERE name='Echoppe'", $this->m_db, 0, 0);
        while (! $dbc->eof()) {
            $shop = new Building();
            $shop->load($dbc->get("id"), $this->m_db);
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Shop=" . $shop->get("id"), $this->m_db);
            require_once (HOMEPATH . "/factory/CityFactory.inc.php");
            CityFactory::updateEquipmentShop($shop, $this->m_db);
            
            $dbc->next();
        }
        echo "**************** END updateEquipment ******************\n\n";
    }

    function updateKradjeck()
    {
        $nbPlayer = 1; // (*500)
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** updateKradjeck=" . $strf . "\n";
        for ($level = 1; $level < 3; $level ++) {
            $dbc = new DBCollection("SELECT id FROM Player WHERE id_BasicRace=253 AND level<" . (10 * $level) . " AND level>" . (10 * ($level - 1)), $this->m_db, 0, 0);
            $dbg = new DBCollection("SELECT id,level, x, y, map FROM Gate WHERE level <" . (10 * $level) . " AND level>" . (10 * ($level - 1)) . " ORDER BY RAND()", $this->m_db, 0, 
                0);
            echo "Nb Kradject Zone " . $level . " =" . $dbc->count();
            for ($j = 0; $j < min(15 * $nbPlayer - $dbc->count(), $dbg->count()); $j ++) {
                $place = array();
                $place = BasicActionFactory::getAleaFreePlace($dbg->get("x"), $dbg->get("y"), $dbg->get("map"), 3, $this->m_db);
                if ($place != 0)
                    $this->m_map1->insertNPC($dbg->get("level"), 253, $place["x"], $place["y"], $dbg->get("map"), 0);
                echo " Création Kradjeck gate=" . $dbg->get("id") . " ";
                $dbg->next();
            }
        }
        echo "**************** END updateBKradeck ******************\n\n";
    }

    function updateCrop()
    {
        
        // Repousse de 3 lins niveau 1, 2 lins niveau 2 et un 1 lin niveau 3 par semaine par zone par tranche de 500 joueurs dans la limite du max
        $nbPlayer = 1; // (*500)
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** updateCrop =" . $strf . "\n";
        for ($zone = 1; $zone < 5; $zone ++) {
            $dbc = new DBCollection("SELECT level, count(*) as nb FROM Exploitation WHERE name='lin' AND zone=" . $zone . " GROUP BY level", $this->m_db, 0, 0);
            for ($i = 0; $i < 3; $i ++) {
                for ($j = 0; $j < min(15 - 5 * $i - (! $dbc->eof() ? $dbc->get("nb") : 0), (3 - $i) * $nbPlayer); $j ++) {
                    $xz = substr(constant("CROP" . $zone), 0, 3);
                    $yz = substr(constant("CROP" . $zone), 4, 3);
                    $exp = new Exploitation();
                    $exp->set("name", "lin");
                    $exp->set("value", mt_rand(2, 4));
                    $exp->set("type", "crop");
                    $exp->set("zone", $zone);
                    $exp->set("level", $i + 1);
                    $exp->set("date", gmdate("Y-m-d H:i:s"));
                    require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
                    $place = BasicActionFactory::getAleaFreePlace($xz, $yz, 1, 10, $this->m_db, 1);
                    if ($place != 0) {
                        if (getLandType($place["x"], $place["y"], 1, $this->m_db) != "beatenearth") {
                            $exp->set("x", $place["x"]);
                            $exp->set("y", $place["y"]);
                            $exp->addDB($this->m_db);
                            echo "Création Lin zone=" . $zone . " level=" . ($i + 1) . "\n";
                        }
                    }
                }
                
                $dbc->next();
            }
        }
        echo "**************** END updateCrop ******************\n\n";
    }

    function updateEmerald()
    {
        // Repousse de 3 emeraude de niveau 1 et 1 émeraude niveau 2 par semaine par zone par tranche de 500 joueurs dans la limite du max
        $nbPlayer = 1; // (*500)
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** updateEmerald =" . $strf . "\n";
        $day = date("d");
        if ($day % 7 == 3 or $day % 7 == 5) {
            for ($zone = 1; $zone < 10; $zone ++) {
                $dbc = new DBCollection("SELECT level, count(*) as nb FROM Exploitation WHERE name='Emeraude' AND zone=" . $zone . " GROUP BY level", $this->m_db, 0, 0);
                for ($i = 0; $i < 2; $i ++) {
                    for ($j = 0; $j < min(12 - 10 * $i - (! $dbc->eof() ? $dbc->get("nb") : 0), (5 - 2 * $i) * $nbPlayer); $j ++) {
                        $xz = substr(constant("EMERALD" . $zone), 0, 3);
                        $yz = substr(constant("EMERALD" . $zone), 4, 3);
                        $exp = new Exploitation();
                        $exp->set("name", "Emeraude");
                        $exp->set("value", min(1, mt_rand(0, 2)));
                        $exp->set("type", "deposit");
                        $exp->set("zone", $zone);
                        $exp->set("level", $i + 1);
                        $exp->set("date", gmdate("Y-m-d H:i:s"));
                        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
                        $place = BasicActionFactory::getAleaFreePlace($xz, $yz, 1, 10, $this->m_db, 1);
                        if ($place != 0) {
                            if (getLandType($place["x"], $place["y"], 1, $this->m_db) != "beatenearth") {
                                
                                $exp->set("x", $place["x"]);
                                $exp->set("y", $place["y"]);
                                $exp->addDB($this->m_db);
                                echo "Création Emeraude zone=" . $zone . " level=" . ($i + 1) . " en " . $place["x"] . "-" . $place["y"] . "\n";
                            }
                        }
                    }
                    
                    $dbc->next();
                }
            }
        }
        echo "**************** END updateEmerald ******************\n\n";
    }

    function updateRuby()
    {
        // Repousse de 3 rubis de niveau 1 et 1 rubis niveau 2 par semaine par zone par tranche de 500 joueurs dans la limite du max
        $nbPlayer = 1; // (*500)
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** updateRuby =" . $strf . "\n";
        for ($zone = 1; $zone < 5; $zone ++) {
            $dbc = new DBCollection("SELECT level, count(*) as nb FROM Exploitation WHERE name='Rubis' AND zone=" . $zone . " GROUP BY level", $this->m_db, 0, 0);
            for ($i = 0; $i < 2; $i ++) {
                for ($j = 0; $j < min(12 - 10 * $i - (! $dbc->eof() ? $dbc->get("nb") : 0), (3 - 2 * $i) * $nbPlayer); $j ++) {
                    $xz = substr(constant("RUBIS" . $zone), 0, 3);
                    $yz = substr(constant("RUBIS" . $zone), 4, 3);
                    $exp = new Exploitation();
                    $exp->set("name", "Rubis");
                    $exp->set("value", min(1, mt_rand(0, 2)));
                    $exp->set("type", "deposit");
                    $exp->set("zone", $zone);
                    $exp->set("level", $i + 1);
                    $exp->set("date", gmdate("Y-m-d H:i:s"));
                    require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
                    $place = BasicActionFactory::getAleaFreePlace($xz, $yz, 1, 10, $this->m_db, 1);
                    if ($place != 0) {
                        if (getLandType($place["x"], $place["y"], 1, $this->m_db) != "beatenearth") {
                            $exp->set("x", $place["x"]);
                            $exp->set("y", $place["y"]);
                            $exp->addDB($this->m_db);
                            echo "Création Rubis zone=" . $zone . " level=" . ($i + 1) . "\n";
                        }
                    }
                }
                
                $dbc->next();
            }
        }
        echo "**************** END updateRuby ******************\n\n";
    }

    function updateBush()
    {
        // Repousse de 3 buisson niveau 1, 2 buisson niveau 2 et un 1 buisson niveau 3 par semaine par zone par tranche de 500 joueurs dans la limite du max
        $nbPlayer = 1; // (*500)
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** updateBush =" . $strf . "\n";
        $bush = array(
            "Razhot",
            "Garnach",
            "Folliane"
        );
        foreach ($bush as $name) {
            for ($zone = 1; $zone < 4; $zone ++) {
                $dbc = new DBCollection("SELECT level, count(*) as nb FROM Exploitation WHERE name='" . $name . "' AND zone=" . $zone . " GROUP BY level", $this->m_db, 0, 0);
                for ($i = 0; $i < 3; $i ++) {
                    for ($j = 0; $j < min(15 - 5 * $i - (! $dbc->eof() ? $dbc->get("nb") : 0), (3 - $i) * $nbPlayer); $j ++) {
                        $xz = substr(constant("BUSH" . $zone), 0, 3);
                        $yz = substr(constant("BUSH" . $zone), 4, 3);
                        $exp = new Exploitation();
                        $exp->set("name", $name);
                        $exp->set("value", mt_rand(2, 4));
                        $exp->set("type", "bush");
                        $exp->set("zone", $zone);
                        $exp->set("level", $i + 1);
                        $exp->set("date", gmdate("Y-m-d H:i:s"));
                        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
                        $place = BasicActionFactory::getAleaFreePlace($xz, $yz, 1, 10, $this->m_db, 1);
                        if ($place != 0) {
                            if (getLandType($place["x"], $place["y"], 1, $this->m_db) != "beatenearth") {
                                $exp->set("x", $place["x"]);
                                $exp->set("y", $place["y"]);
                                $exp->addDB($this->m_db);
                                echo "Création " . $name . " zone=" . $zone . " level=" . ($i + 1) . "\n";
                            }
                        }
                    }
                    $dbc->next();
                }
            }
        }
        echo "**************** END updateBush ******************\n\n";
    }

    /* ******************************************************************** ROUTINE IA MONSTRE ************************************ */
    function updateNumberGate()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "********** updateNumberGate =" . $strf . "\n";
        $this->m_map1->createGate(1);
        echo "\n ************* END updateNumberGate\n\n";
    }

    function createNPC()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "********** createNPC =" . $strf . "\n";
        $this->m_map1->gateActivation(1);
        echo "\nEND CreateNPC \n\n";
    }

    function playNPC()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "********** playNPC =" . $strf . "\n";
        echo "BEGIN new DLA monstre";
        
        $dbNPC = new DBCollection("SELECT * from Player WHERE status='NPC' AND id_Member=0 AND disabled=0 AND nextatb<=\"" . $strf . "\"", $this->m_db, 0, 0);
        while (! $dbNPC->eof()) {
            // echo $dbNPC->get("id")."-;-";
            $curObj = $dbNPC->getCurObject("Player");
            $curObj->externDBObj("Modifier");
            $curObj->loadExternDBObj($this->m_db);
            PlayerFactory::newATB($curObj, $param, $this->m_db);
            $dbNPC->next();
        }
        echo "\n END NEW DLA MONSTRE \n";
        
        $state = array();
        $nbstate = 0;
        $dbPlayer = new DBCollection("SELECT id FROM Player WHERE ap!=0 AND behaviour!=0 AND status='NPC' AND id_Member=0 AND disabled=0 AND playatb<=\"" . $strf . "\"", 
            $this->m_db, 0, 0);
        // $dbPlayer=new DBCollection("SELECT id FROM Player WHERE status='NPC' AND id_Member=0 AND disabled=0",$this->m_db,0,0,0);
        
        echo "\n BEGIN Action ";
        $nb = 0;
        while (! $dbPlayer->eof()) {
            
            $strf = strftime($format);
            echo "" . $strf . " = " . $dbPlayer->get("id") . "-;-";
            echo "\n";
            $this->m_behavior1->playNPC($dbPlayer->get("id"));
            $dbPlayer->next();
            $nb ++;
        }
        
        echo "\n********** END playNPC *********** \n\n";
    }

    /* ************************************************************************** CARTE FONCTIONNELLE *********************************************** */
    function makeMapTeam()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** Make Map Team =" . $strf . "\n";
        include (HOMEPATH . "/map/scripts-IA/makeMapTeam.php");
        echo "\n **************** END Make Map Team **********\n";
    }

    function makeMapGround()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** Make Map Ground =" . $strf . "\n";
        include (HOMEPATH . "/map/scripts-IA/makeMapGround.php");
        echo "\n **************** END Make Map Ground **********\n";
    }

    function makeMapCapturedCity()
    {
        $format = '%Y-%m-%d %H:%M:%S';
        $strf = strftime($format);
        echo "**************** Make Map Captured City =" . $strf . "\n";
        include (HOMEPATH . "/map/scripts-IA/makeMapCapturedCity.php");
        echo "\n **************** END Make Map Captured City **********\n";
    }

    function reduceWayCountInMapGround()
    {
        $dbNPC = new DBCollection("UPDATE MapGround set wayCount = wayCount -2 where wayCount >= 2", $this->m_db, 0, 0, false);
    }
	
	    /* ************************************************************************** A VOIR *********************************************** */
    function makeMapPlayer()
    {
        exec("cd " . HOMEPATH . "/map;./makeMapPlayer.php");
    }

    function deleteEvents()
    {
        $dbd = new DBCollection("DELETE FROM Event WHERE `typeEvent`=200 AND TO_DAYS(NOW()) - TO_DAYS(date) >= 30", $this->m_db, 0, 0, false);
        $dbd = new DBCollection("DELETE FROM HFightMsg WHERE TO_DAYS(NOW()) - TO_DAYS(date) >= 120", $this->m_db, 0, 0, false);
        $dbd = new DBCollection("DELETE FROM HMagicalFightMsg WHERE TO_DAYS(NOW()) - TO_DAYS(date) >= 120", $this->m_db, 0, 0, false);
        $dbd = new DBCollection("DELETE FROM HMagicalFightMsg WHERE TO_DAYS(NOW()) - TO_DAYS(date) >= 120", $this->m_db, 0, 0, false);
        
        $dbm = new DBCollection("SELECT * FROM MailBody WHERE TO_DAYS(NOW()) - TO_DAYS(date) >= 90", $this->m_db, 0, 0);
        while (! $dbm->eof()) {
            $dbd = new DBCollection("DELETE FROM MailSend WHERE id_MailBody=" . $dbm->get("id"), $this->m_db, 0, 0, false);
            $dbd = new DBCollection("DELETE FROM Mail WHERE id_MailBody=" . $dbm->get("id"), $this->m_db, 0, 0, false);
            $dbm->next();
        }
        $dbm = new DBCollection("DELETE FROM MailBody WHERE TO_DAYS(NOW()) - TO_DAYS(date) >= 90", $this->m_db, 0, 0, false);
        $dbm = new DBCollection("UPDATE Event SET idtype=0 WHERE TO_DAYS(NOW()) - TO_DAYS(date) >= 90", $this->m_db, 0, 0, false);
    }

    function deletePNJMission()
    {
        $playerSrc = new Player();
        
        $dbp = new DBCollection("SELECT * FROM Player WHERE status='NPC' AND id_mission!=0 AND TO_DAYS(NOW()) - TO_DAYS(creation) >= 45 ", $this->m_db, 0, 0);
        
        while (! $dbp->eof()) {
            echo "DELETE Player " . $dbp->get("id") . "\n";
            $opponent = new Player();
            $opponent->load($dbp->get("id"), $this->m_db);
            PlayerFactory::deleteDBPlayer($opponent, $playerSrc, $moneyfall, $equipfall, $this->m_db);
            $dbp->next();
        }
    }

}
?>