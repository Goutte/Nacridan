<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once ('Daemon.class.php');
require_once ("../conf/configAI.ini.php");
require_once (HOMEPATH . "/AI/AIObject.class.php");

DEFINE("LOGFILE", HOMEPATH . "/AI/logfolder/nacridanDeamon.log");

class NacridanAI extends Daemon
{

    public $m_aiObject;

    function NacridanAI()
    {
        parent::Daemon();
        
        // $fp = fopen(LOGFILE, 'a');
        // fclose($fp);
        $this->userID = 1000;
        $this->groupID = 100;
        
        // chmod(LOGFILE, 0777);
    }

    function _logMessage($msg, $status = DLOG_NOTICE)
    {
        if ($status & DLOG_TO_CONSOLE) {
            print $msg . "\n";
        }
        $fp = fopen(LOGFILE, 'a');
        fwrite($fp, gmdate("Y/m/d H:i:s ") . $msg . "\n");
        fclose($fp);
    }

    function _init()
    {
        $this->m_aiObject = new AIObject();
        $this->m_aiObject->init();
    }

    /* ******************************************************************** ROUTINE DE FONCTIONNEMENT ************************************ */
    function IncreaseLoyalty()
    {
        $this->m_aiObject->IncreaseLoyalty();
    }

    function StoreInactivePlayer()
    {
        $this->m_aiObject->StoreInactivePlayer();
    }

    function deleteEquipment()
    {
        $this->m_aiObject->deleteEquipment();
    }

    function deleteNewsPJ()
    {
        $this->m_aiObject->deleteNewsPJ();
    }

    function payBankInterest()
    {
        $this->m_aiObject->payBankInterest();
    }

    function deleteCorpse()
    {
        $this->m_aiObject->deleteCorpse();
    }

    function autoRepair()
    {
        $this->m_aiObject->autoRepair();
    }

    function destroyBuilding()
    {
        $this->m_aiObject->destroyBuilding();
    }

    function construction()
    {
        $this->m_aiObject->construction();
    }

    function updateEquipment()
    {
        $this->m_aiObject->updateEquipment();
    }

    function updateKradjeck()
    {
        $this->m_aiObject->updateKradjeck();
    }

    function updateCrop()
    {
        $this->m_aiObject->updateCrop();
    }

    function updateEmerald()
    {
        $this->m_aiObject->updateEmerald();
    }

    function updateRuby()
    {
        $this->m_aiObject->updateRuby();
    }

    function updateBush()
    {
        $this->m_aiObject->updateBush();
    }

    /* ******************************************************************** ROUTINE IA MONSTRE ************************************ */
    function updateNumberGate()
    {
        $this->m_aiObject->updateNumberGate();
    }

    function createNPC()
    {
        $this->m_aiObject->createNPC();
    }

    function playNPC()
    {
        $this->m_aiObject->playNPC();
    }

    /* ************************************************************************** CARTE FONCTIONNELLE *********************************************** */
    function makeMapTeam()
    {
        $this->m_aiObject->makeMapTeam();
    }

    function makeMapGround()
    {
        $this->m_aiObject->makeMapGround();
    }

    function makeMapCapturedCity()
    {
        $this->m_aiObject->makeMapCapturedCity();
    }

    function reduceWayCountInMapGround()
    {
        $this->m_aiObject->reduceWayCountInMapGround();
    }

    /* ************************************************************************** LE DO TASK *********************************************** */
    function _doTask()
    {
        
        // Les cartes
        $this->_schedule("MakeMapTeam", array(
            "fctname" => "makeMapTeam",
            "params" => "",
            "step" => 60 * 60 * 12
        ));
        $this->_schedule("MakeMapGround", array(
            "fctname" => "makeMapGround",
            "params" => "",
            "step" => 60 * 60 * 12 * 7
        ));
        $this->_schedule("MakeMapCapturedCity", array(
            "fctname" => "makeMapCapturedCity",
            "params" => "",
            "step" => 60 * 60 * 12
        ));
        $this->_schedule("ReduceWayCountInMapGround", 
            array(
                "fctname" => "reduceWayCountInMapGround",
                "params" => "",
                "step" => 60 * 60 * 12 * 7
            ));
        
        // Gestion de l'île
        $this->_schedule("deleteEquipment", array(
            "fctname" => "deleteEquipment",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("deleteCorpse", array(
            "fctname" => "deleteCorpse",
            "params" => "",
            "step" => 60 * 60 * 2
        ));
        $this->_schedule("autoRepair", array(
            "fctname" => "autoRepair",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("construction", array(
            "fctname" => "construction",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("destroyBuilding", array(
            "fctname" => "destroyBuilding",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("UpdateEquipment", array(
            "fctname" => "updateEquipment",
            "params" => "",
            "step" => 60 * 60 * 24 * 4
        ));
        $this->_schedule("deleteNewsPJ", array(
            "fctname" => "deleteNewsPJ",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("storeInactivePlayer", array(
            "fctname" => "storeInactivePlayer",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("payBankInterest", array(
            "fctname" => "payBankInterest",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("IncreaseLoyalty", array(
            "fctname" => "IncreaseLoyalty",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        
        // Gestion des ressources
        $this->_schedule("updateCrop", array(
            "fctname" => "updateCrop",
            "params" => "",
            "step" => 60 * 60 * 24 * 7
        ));
        $this->_schedule("updateEmerald", array(
            "fctname" => "updateEmerald",
            "params" => "",
            "step" => 60 * 60 * 24
        ));
        $this->_schedule("updateRuby", array(
            "fctname" => "updateRuby",
            "params" => "",
            "step" => 60 * 60 * 24 * 7
        ));
        $this->_schedule("updateBush", array(
            "fctname" => "updateBush",
            "params" => "",
            "step" => 60 * 60 * 24 * 7
        ));
        $this->_schedule("updateKradjeck", array(
            "fctname" => "updateKradjeck",
            "params" => "",
            "step" => 60 * 60 * 24 * 2
        ));
        
        // IA monstres
        $this->_schedule("UpdateGATE", array(
            "fctname" => "updateNumberGate",
            "params" => "",
            "step" => 60 * 60 * 6
        ));
        $this->_schedule("CreateNPC", array(
            "fctname" => "createNPC",
            "params" => "",
            "step" => 60 * 60 * 2
        ));
        $this->_schedule("PlayNPC", array(
            "fctname" => "playNPC",
            "params" => "",
            "step" => 60 * 10
        ));
        
        // $this->_schedule("MakeMapPlayer",array("fctname"=>"makeMapPlayer", "params" => "","step"=>60*60*12));
        // $this->_schedule("DeleteEvents",array("fctname"=>"deleteEvents", "params" => "","step"=>60*60*12));
        // $this->_schedule("DeletePNJMission",array("fctname"=>"deletePNJMission", "params" => "","step"=>60*60*12));
        // $this->_schedule("UpdateBesiegedCity",array("fctname"=>"updateBesiegedCity", "params" => "","step"=>60*60*6));
        // $this->_schedule("deleteCorpse",array("fctname"=>"deleteCorpse", "params" => "","step"=>60*60*6));
    }
}
?>
