<?php
require_once ("../conf/configAI.ini.php");
require_once (HOMEPATH . "/AI/AIObject.class.php");

$AI = new AIObject();

$AI->init();
$AI->updateNumberGate();
$AI->createNPC();
$AI->playNPC();
?>
