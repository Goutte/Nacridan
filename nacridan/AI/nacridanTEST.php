#!/usr/bin/php
<?php
require_once ('NacridanAI.class.php');

if (isset($argv[1])) {
    $validParams = array(
        'start',
        'stop'
    );
    if (! in_array($argv[1], $validParams)) {
        die('Veuillez specifier start || stop comme premier parametre');
    }
    
    if (! file_exists('/tmp/nacridan/')) {
        mkdir('/tmp/nacridan/', 0777);
    }
    $Daemon = new NacridanAI();
    
    if ($argv[1] == 'start') {
        $Daemon->start();
    } else {
        $Daemon->kill();
    }
}
?>
