<?php
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");

class NacridanModule
{

    public $sess;

    public $auth;

    public $db;

    public $isRepostForm;

    public $postName;

    public $city;

    function NacridanModule(&$sess, &$authen, $db, $updateRepostId = 1)
    {
        $this->sess = $sess;
        $this->auth = $authen;
        $this->db = $db;
        $this->city = null;
        
        date_default_timezone_set($this->auth->auth["utc"]);
        
        $this->postName = "idform";
        
        $previousIdForm = "";
        $sendIdForm = "NOVALUE";
        
        if (isset($_POST[$this->postName])) {
            $previousIdForm = $this->sess->get($this->postName);
            $sendIdForm = $_POST[$this->postName];
        }
        
        if ($updateRepostId) {
            $pageid = $this->postName;
            $this->sess->set($pageid, getCurrentPageId());
        }
        
        if ($previousIdForm == $sendIdForm) {
            $this->isRepostForm = false;
        } else {
            $this->isRepostForm = true;
        }
        
        if (isset($_POST["dla"]) || isset($_POST["form_dla"])) {
            $players = $this->loadSessPlayers();
            foreach ($players as $key => $val) {
                $var = "DLA" . $key;
                $this->sess->set($var, "on");
            }
        }
    }

    function isRepostForm()
    {
        return $this->isRepostForm;
    }

    function setCity($city)
    {
        $this->city = $city;
    }

    function getCity()
    {
        return $this->city;
    }

    function getPostName()
    {
        return $this->postName;
    }

    function setRepostForm($value)
    {
        $this->isRepostForm = $value;
    }

    function loadSessPlayerID(&$db, $playerid = "cq_playerid")
    {
        if (isset($_POST["__idCurPlayer"])) {
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_Member=" . $this->auth->auth["uid"] . " AND id=" . $_POST["__idCurPlayer"], $db, 0, 0);
            
            if (! $dbp->eof()) {
                $this->sess->set($playerid, $_POST["__idCurPlayer"]);
            } else {
                $this->sess->clear("cq_players");
                $array = $this->loadSessPlayers();
                
                $dbp = new DBCollection("SELECT * FROM Player WHERE id_Member=" . $this->auth->auth["uid"] . " AND id=" . $_POST["__idCurPlayer"], $db, 0, 0);
                
                if (! $dbp->eof()) {
                    $this->sess->set($playerid, $_POST["__idCurPlayer"]);
                } else {
                    redirect(CONFIG_HOST . "/main/logout.php");
                }
            }
        }
        
        return $this->sess->get($playerid);
    }

    function loadSessPlayers($players = "cq_players")
    {
        $arr = array();
        
        $dbp = new DBCollection("SELECT * FROM Player WHERE id_Member=" . $this->auth->auth["uid"] . " ORDER BY id", $this->db, 0, 0);
        if (is_object($dbp)) {
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "")
                    $arr[$dbp->get("id")] = $dbp->get("name");
                else
                    $arr[$dbp->get("id")] = $dbp->get("racename");
                
                $dbp->next();
            }
        }
        $this->sess->set($players, $arr);
        return $arr;
    }

    function loadCurSessPlayer($db)
    {
        static $player;
        
        page_open(array(
            "sess" => "Session",
            "auth" => "Auth"
        ));
        
        if ($player != null) {
            return $player;
        }
        
        $array = $this->loadSessPlayers();
        if (count($array) == 0) {
            $this->sess->clear("cq_players");
            redirect(CONFIG_HOST . "/main/newcharacter.php");
        }
        
        $id = $this->loadSessPlayerID($db, "cq_playerid");
        
        if ($id == "") {
            list ($key, $val) = each($array);
            $id = $key;
            $playerid = "cq_playerid";
            
            $this->sess->set($playerid, $id);
        }
        
        $player = new Player();
        
        $player->externDBObj("Modifier,Team,FighterGroup,Upgrade");
        $player->load($id, $this->db);
        $player->unlinkExternDBObj("Team");
        $player->unlinkExternDBObj("FighterGroup");
        $player->getObj("Modifier")->initCharacStr();
        return $player;
    }

    function loadSessAction()
    {
        return $this->sess->get("cqaction");
    }

    function deleteSessAction()
    {
        $this->sess->clear("cqaction");
    }

    function saveSessAction(&$action)
    {
        $this->sess->set("cqaction", $action);
    }
}
?>