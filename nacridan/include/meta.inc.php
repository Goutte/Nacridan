<?php
$MAIN_HEAD->add('<meta name="description" content="Jeu de rôle en ligne en tour par tour où l\'on incarne un personnage en quête d\'aventures."/>');
$MAIN_HEAD->add('<meta name="keywords" content="Nacridan, jeu, gratuit, asynchrone, médiéval, fantastique, RPG, JDR, montpellier, jeux" />');
$MAIN_HEAD->add('<meta name="verify-v1" content="XMkGA77GGPamutLB6Poe+Fzlp2N/QOGeaesM/KH+3cc=" />');
$MAIN_HEAD->add('<meta name="robots" content="index,follow" />');
$MAIN_HEAD->add('<meta name="revisit-after" content="1 week" />');
$MAIN_HEAD->add('<meta http-equiv="Content-Language" content="fr-FR" />');

?>