<?php
require_once (HOMEPATH . "/lib/DB.inc.php");

if (isset($auth)) {
    $lang = $auth->auth['lang'];
} else {
    $lang = '';
    switch (getBrowserDefaultLang()) {
        case 'fr-fr':
        case 'fr':
            $lang = 'fr';
            break;
        default:
            $lang = 'en';
            break;
    }
}

$MAIN_PAGE = new HTMLObject('html', '', 'xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr"');
$MAIN_PAGE->setDoctype('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject('head', 'head');
$MAIN_HEAD->add('<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>');

$MAIN_HEAD->add('<title>Nacridan - jeu de rôle tour-par-tour</title>');
$MAIN_HEAD->add('<link rel="stylesheet" type="text/css" href="' . CONFIG_HOST . '/css/nacridanMain.css?' . getGoogleKeywords() . '" />');

$MAIN_HEAD->add('<link rel="stylesheet" type="text/css" href="' . CONFIG_HOST . '/css/nacridan' . $lang . '.css" />');
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/nacridan.js?' . getGoogleKeywords() . '"></script>');
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/register.js?' . getGoogleKeywords() . '"></script>');

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject('body', 'body', 'class="index_bg" onLoad="document.forms[\'loginform\'].elements[\'username\'].focus()"');

$CONTENT = $MAIN_BODY->addNewHTMLObject('div', null, 'class="index_content"');
$CONTENT->add('<a href="' . CONFIG_HOST . '" class="index_home"></a>');

// $CONTENT->add ('<div style="position:absolute; z-index: 10; top:13px; left:420px;"><span style="color: #DDDDDD;">Publicité</span><br/><a href="http://www.publicjob.fr" title="L\'emploi et les concours dans la fonction publique" target="_blank"><img src="'.CONFIG_HOST.'/pics/banner/publicjob.gif" border="0" alt="Publicjob" /></a></div>');

// $CONTENT->add ('<a href="http://www.tourdejeu.net" target="_blank" style="position:absolute; z-index: 20; top: 60px; left:30px;"><img src="http://www.tourdejeu.net/images/bouton.gif" width="90" height="30" border=0></a>');
$CONTENT->add(
    '<div style="position:absolute; z-index: 20; top: 50px; left:630px; width:88px;height:31px;"><a href="http://www.jeu-gratuit.net" alt="jeux gratuits" target="_blank"><img src="http://www.jeu-gratuit.net/images/bannieres/88x31_general_5.png" border="0" width="88" height="31" /></a><a href="http://www.jeu-gratuit.net" alt="jeux gratuits" target="_blank" style="position:absolute; top:8px; left:3px; font-family:arial; font-size:11px; font-weight:bold; color:#ffffff; text-decoration:none;">jeu-gratuit.net</a></div>');

$CONTENT->add('<div style="position:absolute; z-index: 20; top: 19px; left:630px; width:88px;height:31px;"><a href="http://www.tourdejeu.net" target="_blank"><img src="http://www.tourdejeu.net/images/bouton.gif" width="88" height="31" border=0></a>
</div>');

$CONTENT->add('<div style="position:absolute; z-index: 20; top: 80px; left:630px; width:88px;height:31px;"><a href="http://www.2001jeux.com" target="_blank">2001jeux.com</a></a>
</div>');

// $CONTENT->add ('<div style="position:relative;width:88px;height:31px;"><a href="http://www.jeu-gratuit.net" alt="jeux gratuits" target="_blank"><img src="http://www.jeu-gratuit.net/images/bannieres/88x31_general_5.png" border="0" width="88" height="31" /></a><a href="http://www.jeu-gratuit.net" alt="jeux gratuits" target="_blank" style="position:absolute; top:8px; left:3px; font-family:arial; font-size:11px; font-weight:bold; color:#ffffff; text-decoration:none;">jeu-gratuit.net</a></div>');

/*
 * $CONTENT->add ('<div style="position:absolute; z-index: 20; top: 20px; left:490px;" >
 * <script type=\'text/javascript\'><!--
 * google_ad_client = \'pub-3725586809828932\';
 * google_ad_width = 234;
 * google_ad_height = 60;
 * google_ad_format = \'234x60_as\';
 * google_cpa_choice = \'CAEaCAvN94uZZezAUAhQDVC8Ag\';
 * google_ad_channel = \'5825834688\';
 * google_color_border = \'000000\';
 * google_color_bg = \'000000\';
 * google_color_link = \'FFFF66\';
 * google_color_text = \'E895CC\';
 * google_color_url = \'002E3F\';
 * //-->
 * </script>
 * <script type=\'text/javascript\' src=\'http://pagead2.googlesyndication.com/pagead/show_ads.js\'>
 * </script></div>');
 */
/*
 *
 * $CONTENT->add ('<div style="position:absolute; z-index: 20; top: 20px; left:490px;" ><script type=\'text/javascript\'><!--
 * google_ad_client = \'pub-3725586809828932\';
 * google_ad_width = 120;
 * google_ad_height = 60;
 * google_ad_format = \'120x60_as_rimg\';
 * google_cpa_choice = \'CAEQ9YX7zwEaCLFJb9zX6eYXKLu493MwAA\';
 * google_ad_channel = \'4792298654\';
 * //-->
 * </script>
 * <script type=\'text/javascript\' src=\'http://pagead2.googlesyndication.com/pagead/show_ads.js\'>
 * </script></div>');
 *
 * $CONTENT->add ('<div style="position:absolute; z-index: 20; top: 20px; left:620px;" >
 * <script type=\'text/javascript\'><!--
 * google_ad_client = \'pub-3725586809828932\';
 * google_ad_width = 120;
 * google_ad_height = 60;
 * google_ad_format = \'120x60_as_rimg\';
 * google_cpa_choice = \'CAEQ4-ybzgEaCPHOl1LqRsD3KK3F93MwAA\';
 * google_ad_channel = \'0333026714\';
 * //-->
 * </script>
 * <script type=\'text/javascript\' src=\'http://pagead2.googlesyndication.com/pagead/show_ads.js\'>
 * </script>
 * </div>');
 *
 */
// $CONTENT->add ('<a href="http://www.tourdejeu.net" target="_blank" style="position:absolute; z-index: 20; top: 20px; left:550px;"><img src="http://www.tourdejeu.net/images/bouton.gif" width="90" height="30" border=0></a>');

/*
 * $CONTENT->add ('<div style="position:absolute; z-index: 10; top:10px; left:575px;"><script language=\'javascript\' src=\'http://media.fastclick.net/w/get.media?sid=38368&m=7&tp=9&d=j&t=n\'></script>
 * <noscript><a href=\'http://media.fastclick.net/w/click.here?sid=38368&m=7&c=1\' target=\'_blank\'>
 * <img src=\'http://media.fastclick.net/w/get.media?sid=38368&m=7&tp=9&d=s&c=1\'
 * width=180 height=150 border=1></a></noscript></div>');
 */

$DIV = $CONTENT->addNewHTMLObject('div', null, 'class="index_title"');
$FORM = $CONTENT->addNewHTMLObject('div', '', 'class="loginfield"');

$FORM->add('<form name="loginform" method="post" action="' . CONFIG_HOST . '/index.php" style="margin:0">');

$FORM->add(localize('Identifiant :') . '<br/>');
$FORM->add(new HTMLInput('text', 'username', '', '', 'size="14" maxlength="32"'));
$FORM->add('<br/>');
$FORM->add(localize('Mot de Passe :') . '<br/>');
$FORM->add(new HTMLInput('password', 'password', '', '', 'size="14" maxlength="32"', 0));
$FORM->add('<br/>');
$str = '<input type="checkbox" name="dla" checked="checked" />' . localize('(activer sa DLA)') . '<br/>';
$str .= '<input type="submit" name="submit" value="ok" /><br/>';

$str .= '<br/>';
$str .= 'GMT : ' . gmdate('H:i:s') . '<br/>';
$str .= '<br/>';

if (! isset($_GET['lostpasswd']) && ! isset($_GET['confirmmail'])) {
    $str .= '<a href="' . CONFIG_HOST . '/main/register.php?lostpasswd=true" class="attackevent">' . localize('Mot de passe perdu ?') . '</a>';
    $str .= '<br/>';
    $str .= '<br/>';
    $str .= '<a href="' . CONFIG_HOST . '/main/register.php?confirmmail=true" class="attackevent">' . localize('Mail d"inscription <br/>non reçu ?') . '</a>';
    $str .= '<br/>';
    
    $str .= '<br/>';
    $str .= '<a href="' . CONFIG_HOST . '/main/contact.php" class="attackevent" class="popupify">Nous Contacter</a>';
    $str .= '<br/>';
    $str .= '<br/>';
    $str .= '<br/>';
    $str .= '<br/>';
    $db = DB::getDB();
    /*
     * $dbq=new DBCollection('SELECT count(id) AS nbquest FROM Quest WHERE In_List=1',$db);
     * $nbq = $dbq->get("nbquest");
     * $str.='<br/>';
     * $str.='<div class="attackevent">Générateur de <br/> Quêtes</div>';
     * $str.="(Quêtes <br/>disponibles : $nbq)";
     * $str.='<br/>';
     */
}

$str .= '</form>';

// display AI status
exec("cd " . HOMEPATH . "/AI;./status.sh", $res);
$status = "unknown";

foreach ($res as $lign) {
    if ($lign >= 2)
        $status = "<img src='" . CONFIG_HOST . "/pics/register/green_ball.png'  style='position: relative; left: 0px; top: 7px; width:20px;' >";
    else
        $status = "<img src='" . CONFIG_HOST . "/pics/register/red_ball.png'  style='position: relative; left: 0px; top: 7px; width:20px;' >";
}
$str .= "<br><br> <b>IA</b> : " . $status . "  ";

$FORM->add($str);
$CENTER = $CONTENT->addNewHTMLObject('div', null, 'class="index_center"');

$MENU_TOP = $DIV->addNewHTMLObject('div', 'div', 'class="index_menu_top"');
$MENU_TOP->add('<a class="index_menu_top1" href="' . CONFIG_HOST . '/main/register.php"></a>');

// $MENU_TOP->add('<a class="index_menu_top2" href="'.CONFIG_HOST.'/i18n/rules/'.$lang.'/rules.php"></a>');
$MENU_TOP->add('<a class="index_menu_top2" href="' . CONFIG_HOST . '/i18n/rules/fr/rules.php"></a>');
$MENU_TOP->add('<a class="index_menu_top3" href="' . CONFIG_HOST . '/forum/index.php"></a>');

$CENTER->add("<div style='padding-left: 210px;'>  <a class='statsevent' href='http://www.nacridan.com/test/nacridan2/forum/viewtopic.php?id=608'><img src='" . CONFIG_HOST . "/pics/register/exp-screen-bouton.png' width='150' height='79' border=0></a> <br/></div>");

$CENTER->add('<img src="' . CONFIG_HOST . '/pics/register/frise.jpg" style="margin-top: 10px; margin-bottom: 10px" alt="" />');

$google = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68594660-1', 'auto');
  ga('send', 'pageview');

</script>";

$BOTTOM = $MAIN_BODY->addNewHTMLObject('div', null, 'class="index_bottom"');
$BOTTOM->add('<div style="padding-left: 140px; text-align:center;"> NACRIDAN <br/> <a href="http://www.journal-officiel.gouv.fr/association/index.php?ACTION=Rechercher&HI_PAGE=1&HI_COMPTEUR=0&original_method=get&WHAT=nacridan" class="stylecontact">Association loi 1901</a><br/>R.N.A N°W212005813</div>');

$MAIN_BODY->add($google);

?>
