<?php

require_once (HOMEPATH . "/include/constants.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

$MAIN_PAGE = new HTMLObject ( 'html', '', 'xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr"' );
$MAIN_PAGE->setDoctype ( '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' );

$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject ( 'head', 'head' );

$MAIN_HEAD->add ( '<title>Nacridan - jeu de rôle tour-par-tour </title>' );
$MAIN_HEAD->add ( '<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>' );

$MAIN_HEAD->add ( '<link rel="stylesheet" type="text/css" href="' . Cache::get_cached_file('/css/nacridanMainTemp.css') . '" />' );
$MAIN_HEAD->add ( '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST .'/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add ( '<script language="javascript" type="text/javascript" src="' . Cache::get_cached_file('/javascript/nacridan.js') . '"></script>' );
$MAIN_HEAD->add ( '<script language="javascript" type="text/javascript" src="' . Cache::get_cached_file('/javascript/register.js') . '"></script>' );

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject ( 'body', 'body', 'class="index_bg" onLoad="document.forms[\'loginform\'].elements[\'username\'].focus()"' );

$CONTENT = $MAIN_BODY->addNewHTMLObject ( 'div', null, 'class="index_content"' );

$DIV = $CONTENT->addNewHTMLObject ( 'div', null, 'class="index_title"' );
$DIV->add ( "<a href='" . CONFIG_HOST . "'><img class='homepage-header' src='" . CONFIG_HOST . "/pics/new-homepage/main-banner-test.png'> </img></a>" );

$MENU_TOP = $DIV->addNewHTMLObject ( 'div', 'div', 'class="index_menu_top"' );
$MENU_TOP->add ( "<div class='index_menu_top1'><a href='" . CONFIG_HOST . "/main/register.php'></a></div>" );
$MENU_TOP->add ( "<div class='index_menu_top3'><a href='" . CONFIG_HOST . "/i18n/rules/fr/rules.php'></a></div>" );
$MENU_TOP->add ( "<div class='index_menu_top2'><a href='" . CONFIG_HOST . "/forum'></a></div>" );

$CENTER = $CONTENT->addNewHTMLObject ( 'div', null, 'class="index_center"' );

$google = '<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1166023-1";
urchinTracker();
</script>';

$MAIN_BODY->add ( $google );

$BOTTOM = $MAIN_BODY->addNewHTMLObject ( 'div', null, 'class="index_bottom"' );

$strTable = "<table class='index-bottom-table' >
					<tr>
						<td class='index-bottom-border-left-top-corner' height='10px;'><div style='height: 203px;'>&nbsp;</div></td>
						<td rowspan=2>";
$BOTTOM->add ( $strTable );
$BOTTOMTitle = $BOTTOM->addNewHTMLObject ( 'div', null, 'class=""' );

$strTable = "     </td>
						<td class='index-bottom-border-right-top-corner' height='10px;'></td>
					</tr>
				<tr>
					<td class='index-bottom-border-left-middle'>&nbsp;</td>	
					";
$BOTTOM->add ( $strTable );

$BOTTOMMainArea = $BOTTOM->addNewHTMLObject ( 'div', null, 'class="index-bottom-mainarea"' );

$strCredits = "<span>NACRIDAN <br>
 					<a class='stylecontact' href='http://www.journal-officiel.gouv.fr/association/index.php?ACTION=Rechercher&HI_PAGE=1&HI_COMPTEUR=0&original_method=get&WHAT=nacridan' >
 						Association loi 1901
 					</a>
 					<br>R.N.A N°W212005813
 					<br>
 					<br>
 					<a class='stylecontact' href='" . CONFIG_HOST . "/main/credits.php'>
 					Voir les <b>crédits</b>
 					</a></span>";

$strTable = "  
					<td class='index-bottom-border-right-middle' ></td>
				</tr>
				<tr>
					<td class='index-bottom-border-left-bottom-corner' ><img src='" . CONFIG_HOST . "/pics/new-homepage/border-bottom-left.png' ></td>
					<td>" . $strCredits . "</td>
					<td class='index-bottom-border-right-bottom-corner' ><img src='" . CONFIG_HOST . "/pics/new-homepage/border-bottom-right.png' ></td>
				</tr>
				</table>";

$BOTTOM->add ( $strTable );

?>
