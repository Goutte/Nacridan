<?php

/*************************
 * Les constantes du jeu *
 *************************/


// Le nombre de secondes ou un message dans la tribune reste accessible, a partir du dernier message non lu.
DEFINE("TRIBUNE_TIME_READABLE", 3 * 24 * 3600); // 3 jours
// Le nombre de secondes ou un message dans la tribune reste accessible au maximum
DEFINE("TRIBUNE_MAX_TIME_READABLE", 7 * 24 * 3600); // 7 jours