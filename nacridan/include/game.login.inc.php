<?php
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");

require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");

$MAIN_PAGE = new HTMLObject("html");
$MAIN_PAGE->setDoctype('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<meta http-equiv='imagetoolbar' content='no' />");
$MAIN_HEAD->add('<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>');

$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "' />\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanfr.css") . "' />\n");
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . CONFIG_HOST . "/javascript/jquery-3.1.0.min.js'></script>\n");
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");


$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$str = "<div class='mainarealogin'>  ";

$MAIN_BODY->add($str);

?>
