<?php
// require_once(HOMEPATH."/lib/Profiler.inc.php");
// profiler_start("start");

// profiler_start("Open Session");
require_once (HOMEPATH . "/include/constants.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));
// profiler_stop("Open Session");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");

// profiler_start("Load Dico");
$lang = $auth->auth['lang'];
Translation::init('gettext', HOMEPATH . '/i18n/messages', $lang, 'UTF-8', "main", true, HOMEPATH . '/i18n/messages/cache', $filename = "");
// profiler_stop("Load Dico");

$db = DB::getDB();
// profiler_start("Load Player");
$nacridan = new NacridanModule($sess, $auth, $db);
$curplayer = $nacridan->loadCurSessPlayer($db);
// profiler_stop("Load Player");

$MAIN_PAGE = new HTMLObject("html");
$MAIN_PAGE->setDoctype('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<meta http-equiv='imagetoolbar' content='no' />");
$MAIN_HEAD->add('<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>');

$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "' />\n");
if ($curplayer->get("incity")) {
    $MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanCity" . $lang . ".css") . "' />\n");
} else {
    $MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "' />\n");
}
if (!empty($css_include)) {
    foreach ($css_include as $css) {
        $MAIN_HEAD->add('<link rel="stylesheet" type="text/css" href="' . Cache::get_cached_file($css) . '" />'."\n");
    }
}

$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . CONFIG_HOST . "/javascript/jquery-3.1.0.min.js'></script>\n");
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");
if (!empty($js_include)) {
    foreach ($js_include as $js) {
        $MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . Cache::get_cached_file($js) . '"></script>'."\n");
    }
}




$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$str = "<div class='mainarea'>  ";

// $str.="<a href='".CONFIG_HOST."/forum/' onclick=\"window.open(this.href,'_blank');return false;\" style='color: #888888;'>Le Forum de Nacridan</a>";

if ($curplayer->get("incity")) {
    $city = new City();
    $dbc = new DBCollection("SELECT " . $city->getASRenamer("City", "CTY") . " FROM City WHERE map=" . $curplayer->get("map") . " AND x=" . $curplayer->get("x") . " AND y=" . $curplayer->get("y"), $db);
    $city->DBLoad($dbc, "CTY");
    $nacridan->setCity($city);
}
/*
 * $pub="<div class='cqpub'>";
 *
 * if($curplayer->get("advert")==0){
 * $rand=mt_rand(0,3);
 * if($rand==0 || $rand==1)
 * {
 * $pub.="
 * <script type=\"text/javascript\"><!--
 * google_ad_client = \"pub-3725586809828932\";
 * /* 728x90, date de création 16/10/08
 */

/*
 * google_ad_slot = \"2479054985\";
 * google_ad_width = 728;
 * google_ad_height = 90;
 * //-->
 * </script>
 * <script type=\"text/javascript\"
 * src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">
 * </script>";
 *
 *
 * }
 * else
 * {
 * $pub.="<script language=\"javascript\" src=\"http://media.fastclick.net/w/get.media?sid=38368&m=1&tp=5&d=j&t=n\"></script>
 * <noscript><a href=\"http://media.fastclick.net/w/click.here?sid=38368&m=1&c=1\" target=\"_blank\">
 * <img src=\"http://media.fastclick.net/w/get.media?sid=38368&m=1&tp=5&d=s&c=1\"
 * width=728 height=90 border=1></a></noscript>";
 *
 *
 * }
 *
 * $pub.="</div>";
 * }
 */

$MAIN_BODY->add($str);

if (isset($_GET["center"]))
    $center = $_GET["center"];

if (isset($_GET["action"]))
    $action = $_GET["action"];

if (isset($_GET["bottom"]))
    $bottom = $_GET["bottom"];

?>
