<?php
// require_once("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

Translation::init('gettext', HOMEPATH . '/i18n/messages', $lang, 'UTF-8', "login", true, HOMEPATH . '/i18n/messages/cache', "");

$AREA1 = $CENTER->addNewHTMLObject("div", "", "style='width: 92%'");

$AREA1->add('<img src="' . CONFIG_HOST . '/pics/register/scene' . mt_rand(1, 4) . '.jpg" alt="" style="float: right; margin-left: 10px"/>');

$AREA1->add("<div style='color: #EEEEEE; font-weight: bold;'>Nacridan, c'est :<br/><br/></div>");
$AREA1->add("<div style='color: #999999; text-align: justify'>- <span style='color: #EEEEEE;'>Un Jeu de Rôle</span>. Vous incarnez un aventurier dans un univers médiéval-fantastique dont les Races Ainées (Elfes, Humains, Nains et Doranes) s'efforcent de sauver leur monde d'une terrible invasion de monstres.  </div>");

// </br>Tout comme le théâtre, c'est également une occasion de développement personnel car le jeu de rôle favorise une expression créative ainsi que la gestion des émotions issus du monde virtuel. Plus de détails sur ces points <a href='".CONFIG_HOST."/i18n/rules/fr/rules.php?page=step15' class='statsevent'>là-bas</a>.

$AREA1->add(
    "<div style='color: #999999; text-align: justify'> </br>- <span style='color: #EEEEEE;'>Un Tour-par-Tour</span>. Nacridan est un jeu à durée de tour fixe. Cela signifie que vous ne pouvez effectuer que très peu d'actions chaque jour, et cela ne changera pas au fil du jeu. Ainsi vous n'avez besoin que de quelques minutes par jour pour jouer, ce qui en fait un jeu idéal pour ceux qui cherchent un univers virtuel riche mais qui n'ont pas le temps pour les grands jeux en temps réel. </div>");

/*
 * $AREA1->add('<div style="color: #999999; text-align: justify">'.localize('Dans l\'archipel de Nacridan perdu au large des sept mers, Elfes, Humains, Nains et Doranes se battent dans une guerre sanguinaire pour sauver leur monde d\'une terrible invasion de monstres.').'</div>');
 *
 * $AREA1->add('<br/><div style="color: #EEEEEE; text-align: justify">'.localize('Devenez l\'une ou l\'un d\'entre eux, formez vos coalitions, améliorez votre équipement et luttez pour le règne de votre communauté en rejoignant le monde de Nacridan...').'</div>');
 */

// annonces ici
$AREA1->add('<img src="' . CONFIG_HOST . '/pics/register/frise.jpg" alt="" style="margin-top: 10px; margin-bottom: 30px" />');

$dbn = new DBCollection("SELECT * FROM News WHERE type=2 ORDER BY id DESC", $db);
if ($dbn->count() > 0) {
    
    $date = strtotime($dbn->get("date"));
    
    $str = "<div style='color: #EEEEEE; font-weight: bold;'>" . gmdate("d-m-Y", $date) . "<center>*** ANNONCES ***</center><br/></div>";
    $str .= "<br/>";
    $str .= "<div style='color: #EEEEEE'>";
    
    $str .= "<b><center>" . $dbn->get("title") . "</center></b><br/><br/>";
    $str .= nl2br($dbn->get("content")) . "<br/><br/>";
    
    $str .= "</div>";
    $str .= "<div style='color: #EEEEEE'>";
    
    $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbn->get("id_Player"), $db);
    
    $str .= "<i>" . $dbp->get("name") . "</i>";
    $str .= "<br/><br/>";
    $str .= "<a href='main/news.php' class='statsevent'>>>>Les anciennes annonces</a>.";
    $str .= "</div>";
    $AREA1->add($str);
}

// Fin annonces

$CENTER->add('<img src="' . CONFIG_HOST . '/pics/register/frise.jpg" alt="" style="margin-top: 10px; margin-bottom: 30px" />');

$db = DB::getDB();
$dblast = new DBCollection('SELECT * FROM Player WHERE authlevel=0 AND id_Member!=0 AND status="PC" AND name IS NOT NULL ORDER BY creation DESC LIMIT 0,9', $db);

$i = $j = 0;
$lastRegistred = array();
while (! $dblast->eof()) {
    /*
     * if($i>=9)
     * {
     * $j++;
     * $i=$j;
     * }
     */
    $lastRegistred[$i] = $dblast->get('name');
    $lastRegistredRace[$i] = $dblast->get('racename');
    $lastRegistredId[$i] = $dblast->get('id');
    // $i+=3;
    $i ++;
    $dblast->next();
}

// print_r($lastRegistred);
$dbActiveMember = new DBCollection('SELECT count(id) AS total FROM Member WHERE  authlevel=0 AND disabled!=99 AND ADDDATE(lastlog, INTERVAL 7 DAY)> NOW()', $db);
$dbActiveCharacter = new DBCollection('SELECT count(id) AS total FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status="PC" AND name IS NOT NULL  AND ADDDATE(playatb, INTERVAL 14 DAY)> NOW()', $db);
$dbbanned = new DBCollection('SELECT count(id) AS banned FROM Banned WHERE 1', $db);

$CENTER->add('<table style="width:93%"><tr><td colspan="11" height="22" align="center" bgcolor="#663333" valign="middle"><strong><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">Les Derniers Inscrits </br> (Nombre de Joueurs Actifs* : ' . $dbActiveMember->get('total') . ') <br> (Nombre de Personnages Actifs** : ' . $dbActiveCharacter->get('total') . ')</font></strong></td></tr>');
$CENTER->add('<tr>');
for ($i = 0; $i < 3; $i ++) {
    $CENTER->add(
        '<td align="center" bgcolor="#330000" height="19" valign="middle" width="37"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">#</font></td><td align="center" bgcolor="#663333" valign="middle" width="84"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1"><strong>Nom</strong></font></td><td align="center" bgcolor="#663333" valign="middle" width="56"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1"><strong>Race</strong></font></td><td rowspan="4" valign="top" width="30">&nbsp;</td>');
}
$CENTER->add('</tr>');
for ($i = 0; $i < 3; $i ++) {
    $CENTER->add('<tr>');
    for ($j = 0; $j < 3; $j ++) {
        $name = '';
        $race = '';
        if (isset($lastRegistred[$i + $j * 3]))
            $name = $lastRegistred[$i + $j * 3];
        if (isset($lastRegistredRace[$i + $j * 3]))
            $race = $lastRegistredRace[$i + $j * 3];
        if (isset($lastRegistredId[$i + $j * 3]))
            $id = $lastRegistredId[$i + $j * 3];
        
        $CENTER->add(
            '<td align="center" bgcolor="#333333" height="19" valign="middle"><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">' . ($i + $j * 3 + 1) . '</font></td><td align="center" bgcolor="#666666" valign="middle"><font color="#ffffff" face="Arial, Helvetica, sans-serif" size="-1"><a href=\'' . CONFIG_HOST . '/main/profile.php?id=' . $id . '\' class=\'stylemainpc popupify\'>' . $name . '</a></font></td>'
           .'<td align="center" bgcolor="#666666" valign="middle">' . $race . '</td>');
    }
    $CENTER->add('</tr>');
}

$CENTER->add('<tr><td>&nbsp;</td></tr><tr><td colspan="11" height="22" align="center" bgcolor="#330000" valign="middle"><strong><font color="#cccccc" face="Arial, Helvetica, sans-serif" size="-1">Nombre de joueurs bannis pour cause de multi-comptes (Total : ' . $dbbanned->get('banned') . ')</font></strong></td></tr>');

$CENTER->add('</table><span style="color: #999999;">* : ayant joué dans les 14 derniers jours. <br>** : un joueur peut contrôler deux personnages. </span><br/> </br><a href="' . CONFIG_HOST . '/main/stats.php" class="statsevent">&gt;&gt;&gt; Plus de statistiques</a><br/>');
$CENTER->add('<img src="' . CONFIG_HOST . '/pics/register/frise.jpg" alt="" style="margin-top: 10px; margin-bottom: 30px" />');

$CENTER->add('<br/> <div class="statsevent">Nacridan 2 a été réalisé grâces au travail d\'une trentaine de personnes :<br/><br/><a href="' . CONFIG_HOST . '/main/credits.php" class="statsevent">&gt;&gt;&gt; Voir les Crédits</a></div> <br/>');

$CENTER->add('<br/> <div class="statsevent">De plus, Nacridan 2  n\'aurait pas été possible sans les nombreux soutiens financiers. Un grand merci à Aksho pour son don impressionant et à Alicia de Lamb, pour son don et pour son prêt important. <br/><br/><a href="' . CONFIG_HOST . '/main/benefactors.php" class="statsevent">&gt;&gt;&gt; Voir la liste des Bienfaiteurs</a></div> <br/>');

$CENTER->add('<br/> <div class="statsevent">Enfin, Nacridan est également une association loi 1901, qui gère le fonctionnement et les finances du site. <br/><br/><a href="' . CONFIG_HOST . '/main/association.php" class="statsevent">&gt;&gt;&gt; En savoir plus sur l\'association</a></div> <br/><br/>');

$CENTER->add('<img src="' . CONFIG_HOST . '/pics/register/frise.jpg" alt="" style="margin-top: 10px; margin-bottom: 30px" />');

// $CENTER->add('<div style="color: #EEEEEE;">Vous êtes sur la version <b>2</b> de Nacridan !<br> La version <b>1</b> est toujours jouable à cette adresse :<br> <a href="'.CONFIG_HOST.'/v1/nacridan/">lien vers la V1</a><br/><br/><br/><br/> ');

$MAIN_PAGE->render();

Translation::saveMessages();

?>
