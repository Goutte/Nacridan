<?php
// profiler_start("Open Session");
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/include/constants.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));
// profiler_stop("Open Session");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");

// profiler_start("Load Dico");
$lang = $auth->auth['lang'];
Translation::init('gettext', HOMEPATH . '/i18n/messages', $lang, 'UTF-8', "main", true, HOMEPATH . '/i18n/messages/cache', $filename = "");
// profiler_stop("Load Dico");

$db = DB::getDB();
// profiler_start("Load Player");
$nacridan = new NacridanModule($sess, $auth, $db);

// Tribune
require_once (HOMEPATH . "/diplomacy/dtribune.inc.php");
$tribune = new DTribune($nacridan, $db);

// Mise en évidence des messages de tribune non-lus
$infos = $tribune->getUnreadMessagesInfos();

$responseJSON = array();
if ($infos['success'] && $infos['id_FighterGroup'] != 0) {
    $responseJSON['unreads'] = $infos['unreads'];
} else {
    $responseJSON['unreads'] = 0;
}
echo json_encode($responseJSON);
?>