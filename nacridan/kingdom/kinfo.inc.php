<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/lib/Map.inc.php");

class KInfo extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function KInfo($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $err = "";
        $str = "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>Royaume</h1></b></td>";
        $str .= "</tr>";
        $str .= "</table>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'> La possibilité de fonder un royaume n'est pas encore implémentée.";
        $str .= "</td>";
        $str .= "</tr>";
        
        $str .= " </table>";
        
        $str .= "<table class='maintable centerareawidth'>";
        
        $map = new Map($db, $curplayer->get("map"));
        $nameKingdom = $map->getStartKingdomNameFromMap($curplayer->get("x"), $curplayer->get("y"));
        
        if ($nameKingdom == "") {
            $str .= "<tr><td class='mainbgtitle'>Vous vous trouvez en dehors de tout royaume. Aucune loi ne s'applique ici, et les monstres seront agressifs. Il est fortement conseillé de ne pas s'aventurer dans ces régions sans un bon équipement et une compétence ou un sortilège de soin.</td></tr>";
        } else {
            if (substr($nameKingdom, 0, 1) == "A" || substr($nameKingdom, 0, 1) == "E" || substr($nameKingdom, 0, 1) == "I" || substr($nameKingdom, 0, 1) == "O" || substr($nameKingdom, 0, 1) == "U" || substr($nameKingdom, 0, 1) == "Y") {
                $nameKingdom = "d'" . $nameKingdom;
            } else {
                $nameKingdom = "de " . $nameKingdom;
            }
            $str .= "<tr><td class='mainbgbody'>Vous vous trouvez dans le <b>Royaume " . $nameKingdom . "</b>. Dans ce royaume les monstres seront faibles et peu agressifs. Les gardes de la cité y font respecter la loi : tout meurtre sera puni d'une peine d'emprisonnement. La même peine sera appliquée si vous tuez une tortue géante qui transporte les marchandises d'une ville à l'autre.</td></tr>";
        }
        $str .= " </table>";
        
        return $str;
    }
}
?>
