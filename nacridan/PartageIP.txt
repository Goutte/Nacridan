Partage d'IP toléré


Leonara (8057) id_Member : 8057
Gildan (8054)  id_Member : 6657

Bonjour,
Merci pour votre message.
Je partage en effet votre beau jeu avec mon épouse, nous jouons chacun un seul personnage.Et nos adresses e mail sont réellement respectivement la mienne et la sienne.
Nous n' avons malheureusement pas les moyens financiers pour payer pour pouvoir jouer un 2 ième personnage. Un de nous deux doit-il dès lors renoncer à jouer?
Je suis désolé de ne pas vous avoir contacté à ce sujet pour déclarer la présence réelle de 2 joueurs différents qui jouent sur un même ordinateur mais je n' ai pas trouvé d ' option permettant de déclarer un partage de connexion.
Dans l' attente de votre réponse, je vous remercie encore pour ce très beau jeu.
Gildan

/////////////////////////

Bibi (8028)    id_Member : 6638
Alex (8034)    id_Member : 6642

L'explication est assez simple : nous sommes un couple, nous avons synchronisé nos DLA pour pouvoir réellement jouer ensemble.
Je jouais déjà à la V1 de nacridan (login : Lord, nom de joueur : Haakon) et n'ai jamais eu de problème de multicompte.
Nous avons utilisé deux de mes adresse mail car ma copine n'a qu'une adresse mail professionnelle qu'elle ne voulait pas utiliser.
J'avais créé un nain (Morph) mais voyant que le multicompte pouvait être un problème, je l'ai supprimé avant tout mail.
Cordialement.

////////////////////////


Grodur (8103)            id_Member : 6669
Tyffene (7983)           id_Member : 6603
chevalierdelamort (7986) id_Member : 6606 
Gudule (7984)			 id_Member : 6602

bonsoir,
Nous comprenons bien la règle des multicomptes.
Nous sommes une famille de 4 et avons chacun notre personnage.... sur 1 seul ordinateur.
Nous jouons ensemble mais dans des secteurs différents pour ne pas transgresser la règle.
Si vous vouliez bien avoir l obligeance de nous permettre de jouer chacun son tour, en famille nous vous en serions reconnaissants et pourrions continuer a passer d agréables moments sur votre jeu.
Par avance merci


////////////////////////

Cafrinelle (7958) id_Member : 6589
Clochette (8179)  id_Member : 6683

Chupa (8178) id_Member : 6684
Nerba (7951) id_Member : 6579

Ce message pour vous prévenir que nos filles ont décidé de créer un perso, c'est leur premiere expérience MMORPG d'ailleurs.
Chupa joué par ma fille jouera sur mon IP une semaine sur deux, Clochette est jouée par la fille de Caf de l'IP de Caf.
Nous n'avons pas l'habitude de jouer des multicomptes, ni même de nous remplacer sur les persos, chacun le sien.
Vous pouvez vérifier que sur la V1 je n'ai pas repris la dorane de Caf lorsqu'elle a arrêté.
Nidaïme nous a déjà vu IRL. Si des Nacridaniens viennent aux IRL liégeoises de Mounty Hall, ils nous auront vus, ainsi que nos filles.
J'espère que ça ne posera pas de problème.
