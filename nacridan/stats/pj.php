<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

$db = DB::getDB();
$format2 = "Y-m-d H:i:s";
$time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
$date = date($format2, $time);

$dbp = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.level,Player.nbdeath,Player.nbkill,Player.nbkillpc,Player.creation,Player.resurrect,Player.id_Team,TeamRankInfo.num,TeamRankInfo.name AS name_rank FROM Player LEFT JOIN TeamRank ON Player.id=TeamRank.id_Player LEFT JOIN TeamRankInfo ON TeamRankInfo.id=TeamRank.id_TeamRankInfo WHERE playatb < '" . $date . "' AND status='PC' ORDER BY id", $db);
echo "id \t name \t racename \t level \t nbdeath \t nbkill \t nbkillpc \t creation \t actif \t ordre \t rang \t nom_rang\n";
while (! $dbp->eof()) {
    echo $dbp->get("id") . "\t" . $dbp->get("name") . "\t" . $dbp->get("racename") . "\t" . $dbp->get("level") . "\t" . $dbp->get("nbdeath") . "\t" . $dbp->get("nbkill") . "\t" . $dbp->get("nbkillpc") . "\t" . $dbp->get("creation") . "\t" . ($dbp->get("resurrect") ? "0" : "1") . "\t" . $dbp->get("id_Team") . "\t" . $dbp->get("num") . "\t" . $dbp->get("name_rank") . "\n";
    
    $dbp->next();
}

?>