class Modifier extends StaticModifier{ function Modifier() {
parent::DBStaticModifier(); ##auto##
$this->m_characLabel=array("attack","dodge","damage","armor","regen","hp","magicSkill");
$this->m_dice=array("3","6","8","10","12","20","100","","");
$this->initCharac(); } function getScore($name) { $modif=array();
for($i=0;$i<9;$i++) {
$modif[$i]=$this->m_charac[$name][$i]+$this->m_charac[$name."_bm"][$i];
} $score=$this->score($modif); return $score; } function
getScoreWithNoBM($name) { $modif=array(); for($i=0;$i<9;$i++) {
$modif[$i]=$this->m_charac[$name][$i]; } $score=$this->score($modif);
return $score; } function addModifObjToBM($obj) { if($this->m_needinit)
$this->initCharac(); if($obj->m_needinit) $obj->initCharac();

foreach($this->m_characLabel as $name) {
if($obj->get($name)!="////////") { for($i=0;$i<9;$i++) {
$this->m_charac[$name."_bm"][$i]+=$obj->getModif($name,$i); //echo
$name."_bm : ".$this->m_charac[$name."_bm"][$i]."\n"; } } }
$this->m_needupdate=1; //$this->updateCharac(); } function
updateFromEquipmentLevel($level) { foreach($this->m_characLabel as
$name) { if($this->get($name)!="////////") { for($i=0;$i<9;$i++) {
$this->m_charac[$name][$i]+=$this->m_charac[$name."_bm"][$i]*$level; } }
} $this->m_needupdate=1; //$this->updateCharac(); } function
updateFromTemplateLevel($level) { foreach($this->m_characLabel as $name)
{ if($this->get($name)!="////////") { for($i=0;$i<9;$i++) {
$this->m_charac[$name][$i]*=$level; } } } $this->m_needupdate=1;
//$this->updateCharac(); } function validateBM() {
foreach($this->m_characLabel as $name) { for($i=0;$i<9;$i++) {
if(abs($this->m_charac[$name."_bm"][$i])<0.01){
$this->m_charac[$name."_bm"][$i]=0; } } } $this->initCharacStr();
$this->m_needupdate=1; //$this->updateCharac(); } function resetBM() {
foreach($this->m_characLabel as $name) { for($i=0;$i<9;$i++) {
$this->m_charac[$name."_bm"][$i]=0; } } $this->initCharacStr();
$this->m_needupdate=1; //$this->updateCharac(); } }
