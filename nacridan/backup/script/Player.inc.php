class Player extends DBObject{ function getScore($val) { return
$this->getObj("Modifier")->getScore($val); } function
getScoreFromDice($val1,$val2,$val3) { return
$this->getObj("Modifier")->getScoreFromDice($val1,$val2,$val3); }

function getScoreWithNoBM($val) { return
$this->getObj("Modifier")->getScoreWithNoBM($val); } function
getModif($field,$dice) { if($this->getObj("Modifier")==null)
trigger_error ("Modifier is null in getModif()
(".$this->m_className.")", E_USER_ERROR); return
$this->getObj("Modifier")->getModif($field,$dice); } function
getModifStr($field) { return
$this->getObj("Modifier")->getModifStr($field); } function
getModifMean($field) { return
$this->getObj("Modifier")->getModifMean($field); } function
getModifMin($field) { return
$this->getObj("Modifier")->getModifMin($field); } function
getModifMax($field) { return
$this->getObj("Modifier")->getModifMax($field); } function
addModif($name,$type,$value) { return
$this->getObj("Modifier")->addModif($name,$type,$value); } function
subModif($name,$type,$value) { return
$this->getObj("Modifier")->subModif($name,$type,$value); } function
multModif($name,$type,$value) { return
$this->getObj("Modifier")->multModif($name,$type,$value); } function
divModif($name,$type,$value) { return
$this->getObj("Modifier")->divModif($name,$type,$value); } function
getHPPercent() { $hp=$this->getObj("Modifier")->getModif("hp",DICE_ADD);
$hppercent=$this->get("currhp")*100; if($hp!=0) { $hppercent/=$hp; }
else { $hppercent=0; } $hppercent=round($hppercent); return $hppercent;
} }
