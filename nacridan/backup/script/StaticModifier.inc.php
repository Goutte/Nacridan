class StaticModifier extends DBStaticModifier{ public $m_characLabel;

public $m_characStr; public $m_characMean; public $m_characMax; public
$m_characMin; function StaticModifier() { ##parent## ##auto##
$this->m_characLabel=array("attack","dodge","damage","armor","regen","hp","magicSkill");
$this->initCharac(); } function initCharac() { foreach ($this->m_attr as
$key => $value) { if($key!="id") { if($value!="") {
$this->m_charac[$key]=explode("/", $value); } else {
$this->set($key,"////////"); $this->m_charac[$key]=explode("/",
"////////"); } } } $this->m_needinit=0; } function initCharacStr() {
foreach ($this->m_attr as $key => $value) { if($key!="id") {
$this->m_characMean[$key]=0; $this->m_characMin[$key]=0;
$this->m_characMax[$key]=0; $str=""; for($i=0;$i
<DICE_CHARAC ;$i++)
	      {
		if($this->m_charac[$key][$i]!="") { $tmp=$this->m_charac[$key][$i];
if($this->m_charac[$key][$i]>=0) { $tmp="+".$tmp; } if($i==DICE_MULT) {
$tmp.="%"; } if($i<DICE_NB )
		      {
			$tmp.=$this->m_diceName[$i]; } if(strstr($key,"_bm")!=false) {
if($this->m_charac[$key][$i]>=0) { $tmp="<span class='bonus'>".$tmp."</span>";
} else { $tmp="<span class='malus'>".$tmp."</span>"; } } $str.=$tmp; } }
if($str!="") $this->m_characStr[$key]=$str; else
$this->m_characStr[$key]="0"; $min=0; $mean=0; $max=0; for($i=0;$i<DICE_CHARAC
	;$i++)
	      {
		$r=$this->m_charac[$key][$i]; if($r!=0) { if($i==DICE_ADD) {
$mean+=$r; $min+=$r; $max+=$r; } else { if($i!=DICE_MULT) { $min+=$r;
$max+=$r*$this->m_dice[$i]; $mean+=($min+$max)/2; } } } }



$key=str_replace("_bm","",$key); $this->m_characMean[$key]+=$mean;
$this->m_characMin[$key]+=$min; $this->m_characMax[$key]+=$max; } } //
foreach ($this->m_attr as $key => $value) //{ //if(strstr($key,"_bm"))
//{ $val=0; $val+=$this->m_charac["magicSkill_bm"][DICE_MULT];
$val+=$this->m_charac["magicSkill"][DICE_MULT];

$this->m_characMean["magicSkill"]+=($this->m_characMean["magicSkill"]*$val)/100;
$this->m_characMin["magicSkill"]+=round($this->m_characMin["magicSkill"]*$val/100);
$this->m_characMax["magicSkill"]+=round($this->m_characMax["magicSkill"]*$val/100);
//} //} } function getModifStr($name) { return
$this->m_characStr[$name]; } function getModifMean($name) { return
max($this->m_characMean[$name],0); } function getModifMax($name) {
return ceil(max($this->m_characMax[$name],0)); } function
getModifMin($name) { return floor(max($this->m_characMin[$name],0)); }

function getDice($dice,$nb) { $score=0; $rnddice=($nb-floor($nb))*100;

if(mt_rand(1,100)<=$rnddice) { for($i=0;$i<=floor($nb);$i++) {
$score+=mt_rand(1,$this->m_dice[$dice]); } } else { for($i=0;$i<floor
	($nb);$i++)
	  {
	    $score+=mt_rand(1,$this->m_dice[$dice]); } } return $score; }

function getScore($name) { $score =$this->score($this->m_charac[$name]);
return $score; } function getScoreFromDice($name,$dicetype,$ratio) {
$score
=$this->getDice($dicetype,floor($this->m_charac[$name][$dicetype]*$ratio));
return $score; } function score(&$array) {
$score=$this->getDice(DICE_D3,$array[DICE_D3]);
$score+=$this->getDice(DICE_D6,$array[DICE_D6]);
$score+=$this->getDice(DICE_D8,$array[DICE_D8]);
$score+=$this->getDice(DICE_D10,$array[DICE_D10]);
$score+=$this->getDice(DICE_D12,$array[DICE_D12]);
$score+=$this->getDice(DICE_D20,$array[DICE_D20]);
$score+=$this->getDice(DICE_D100,$array[DICE_D100]);

$rndadd=($array[DICE_ADD]-floor($array[DICE_ADD]))*100;
if(mt_rand(1,100)<=$rndadd) { $score+=ceil($array[DICE_ADD]); } else {
$score+=floor($array[DICE_ADD]); } if($array[DICE_MULT]!=0)
$score+=round($score*$array[DICE_MULT]/100); return max($score,0); } } 