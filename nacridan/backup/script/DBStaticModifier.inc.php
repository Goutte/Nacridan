class DBStaticModifier extends DBObject{ public $m_charac; public
$m_diceName; public $m_needupdate; public $m_needinit; public $m_dice;

public function DBStaticModifier() { parent::DBObject();
$this->m_charac=array();
$this->m_diceName=array("D3","D6","D8","D10","D12","D20","D100","ADD","MULT");
$this->m_dice=array("3","6","8","10","12","20","100","","");
$this->set("id",0); $this->m_needupdate=0; $this->m_needinit=0; } public
function load($id,&$db) { parent::load($id,$db); } public function
DBLoad(&$records,$extra="") { foreach ($this->m_attr as $key => $val) {
if(isset($this->m_realDBName[$key])) {
$realname=$this->m_realDBName[$key]; } else { $realname=$key; }
if(isset($records->fields[$extra.$realname]))
$this->m_attr[$key]=$records->fields[$extra.$realname]; }
$this->initCharac(); } public function isEmpty() { $empty=1; foreach
($this->m_attr as $key => $value) { if($key!="id") {
if($value!="////////") { $empty=0; } } } return $empty; } public
function addDB(&$db) { if($this->m_needupdate) $this->updateCharac();

if($this->isEmpty()) { return null; } return parent::addDB($db); }

public function updateDB(&$db) { if($this->m_needupdate)
$this->updateCharac(); if($this->isEmpty()) { $this->deleteDB($db);
return 0; } return parent::updateDB($db); } public function
set($varname,$value) { if($this->isExist($varname)) {
$this->m_needinit=1; $this->m_attr[$varname]=$value; } else return -1; }

public function getModif($name,$type) { return
$this->m_charac[$name][$type]; } public function addModifObj($obj) {
if($this->m_needinit) $this->initCharac(); if($obj->m_needinit)
$obj->initCharac(); foreach ($this->m_attr as $key => $value) {
if($obj->get($key)!="////////") { if($key!="id") { for($i=0;$i<9;$i++) {
$this->m_charac[$key][$i]+=$obj->m_charac[$key][$i]; } } } }
$this->m_needupdate=1; } public function updateCharac() { foreach
($this->m_attr as $key => $value) { if($key!="id") { $str="";
for($i=0;$i<9;$i++) { if($i>0) { $str.="/"; }
if($this->m_charac[$key][$i]!=0) $str.=$this->m_charac[$key][$i]; }
$this->set($key,$str); } } } public function
setModif($name,$type,$value) { if($this->m_needinit)
$this->initCharac(); //echo "name: ".$name."\n"; //echo "type:
".$type."\n"; //echo "value: ".$value."\n";
$this->m_charac[$name][$type]=$value; $this->m_needupdate=1; return
$this->m_charac[$name][$type]; } public function
addModif($name,$type,$value) { if($this->m_needinit)
$this->initCharac(); $this->m_charac[$name][$type]+=$value;
$this->m_needupdate=1; return $this->m_charac[$name][$type]; } public
function subModif($name,$type,$value) { if($this->m_needinit)
$this->initCharac(); $this->m_charac[$name][$type]-=$value;
$this->m_needupdate=1; return $this->m_charac[$name][$type]; } public
function multModif($name,$type,$value) { if($this->m_needinit)
$this->initCharac(); $this->m_charac[$name][$type]*=$value;
$this->m_needupdate=1; return $this->m_charac[$name][$type]; } public
function divModif($name,$type,$value) { if($this->m_needinit)
$this->initCharac(); $this->m_charac[$name][$type]/=$value;
$this->m_needupdate=1; return $this->m_charac[$name][$type]; } public
function &getCharac($name) { if($this->m_needinit) $this->initCharac();
return $this->m_charac[$name]; } public function
getDiceValueFromIndex($index) { return $this->m_dice[$index]; } public
function show() { if($this->m_needinit) { $this->initCharac(); }
if($this->m_needupdate) { $this->updateCharac(); } foreach
($this->m_attr as $key => $val) { echo $this->m_attr[$key]."\n"; }

foreach($this->m_object as $key => $fkobj) { $fkobj->show(); } } }
