#!/usr/bin/php
<?php

require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
$db = DB::getDB();

$files1 = scandir(HOMEPATH . MAPS_DYNAMIC_FOLDER);

foreach ($files1 as $filename1) {
    if (strpos($filename1, "mapghost0") === 0) {
        echo "Deleting file " . $filename1 . "<br/>";
        unlink(HOMEPATH . MAPS_DYNAMIC_FOLDER . "/" . $filename1);
    } else {
        echo "Not deleting file " . $filename1 . "<br/>";
    }
}

for ($map = 1; $map < 3; $map ++) {
    $im = imagecreate(640, 400);
    
    $dbc = new DBCollection("
	SELECT MapGround.*,Ground.color FROM  MapGround 
	left outer join GroundEvolution on id_Ground = id_GroundInit and wayCount between startCount and endCount
	inner join Ground on Ground.id = ifnull(id_GroundReplace,id_Ground) where map=" . $map, $db);
    
    // SELECT MapGround.*,Ground.color from MapGround inner join Ground on Ground.id = MapGround.id_Ground WHERE map=".$map." order by x,y",$db);
    $nb = 0;
    $arrayColor = array();
    while (! $dbc->eof()) {
        $arrayColor = preg_split('/[\s]+/', $dbc->get("color"), - 1, PREG_SPLIT_NO_EMPTY);
        $imageColor = imagecolorexactalpha($im, $arrayColor[0], $arrayColor[1], $arrayColor[2], $arrayColor[3]);
        if ($imageColor == - 1) {
            // color does not exist; allocate a new one
            $imageColor = imagecolorallocatealpha($im, $arrayColor[0], $arrayColor[1], $arrayColor[2], $arrayColor[3]);
        }
        imagesetpixel($im, $dbc->get("x"), $dbc->get("y"), $imageColor);
        $dbc->next();
        $nb += 1;
    }
    echo $nb;
    
    imagepng($im, HOMEPATH . MAPS_DYNAMIC_FOLDER . "/mapghost0" . $map . "-" . date("Ymdhisa") . ".png");
    imagedestroy($im);
}

?>
