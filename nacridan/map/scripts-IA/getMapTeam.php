<?php
require_once ("../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);
// $curplayer=
$id = $nacridan->loadSessPlayerID($db, "cq_playerid"); // ->loadCurSessPlayer($db);
$array = $nacridan->loadSessPlayers();
$id = $nacridan->loadSessPlayerID($db, "cq_playerid");

if ($id == "") {
    list ($key, $val) = each($array);
    $id = $key;
}
$dbp = new DBCollection("SELECT id_Team,map FROM Player WHERE id_Member=" . $nacridan->auth->auth["uid"] . " AND id=" . $id, $db, 0, 0);
if (isset($_GET["map"])) {
    $map = $_GET["map"];
} else {
    $map = $dbp->get("map");
}
$idTeam = $dbp->get("id_Team");

ignore_user_abort(true);

$path = HOMEPATH . MAPS_DYNAMIC_FOLDER . "/"; // change the path to fit your websites document structure
$files1 = scandir($path);
$filenameTeam = "";
foreach ($files1 as $filename1) {
    if (strpos($filename1, "mapghostteam0$map-$idTeam") === 0) { // $curplayer->get("map")."-".$curplayer->get("id_Team")) === 0){
        $filenameTeam = $filename1;
        break;
    }
}
if ($filenameTeam != "") {
    $fullPath = $path . $filenameTeam;
    $path_parts = pathinfo($fullPath);
    header("Content-type: image/png; charset=ANSI");
    readfile($fullPath);
}
exit();
?>