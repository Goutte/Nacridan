#!/usr/bin/php
<?php
require_once ("../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
$db = DB::getDB();

for ($map = 1; $map < 3; $map ++) {
    $im = imagecreate(640, 400);
    
    $background = imagecolorallocate($im, 0, 0, 0);
    imagecolortransparent($im, $background);
    imagefill($im, 0, 0, $background);
    
    $dbc = new DBCollection("SELECT * from MapZone WHERE map=" . $map . " and zone=1 order by x,y", $db);
    $nb = 0;
    while (! $dbc->eof()) {
        list ($red, $green, $blue, $alpha) = preg_split('/[\s]+/', $dbc->get("color"), - 1, PREG_SPLIT_NO_EMPTY);
        $imageColor = imagecolorexactalpha($im, $red, $green, $blue, $alpha);
        if ($imageColor == - 1) {
            // color does not exist; allocate a new one
            $imageColor = imagecolorallocatealpha($im, $red, $green, $blue, $alpha);
        }
        imagesetpixel($im, $dbc->get("x"), $dbc->get("y"), $imageColor);
        $dbc->next();
        $nb += 1;
    }
    echo $nb;
    
    imagepng($im, HOMEPATH . MAPS_DYNAMIC_FOLDER . "/mapghostkingdom0" . $map . "-" . date("Ymdhisa") . ".png");
}

?>
