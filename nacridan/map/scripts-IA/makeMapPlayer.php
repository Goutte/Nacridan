#!/usr/bin/php
<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
$db = DB::getDB();

for ($map = 1; $map < 3; $map ++) {
    $im = imagecreate(640, 400);
    
    $background = imagecolorallocate($im, 0, 0, 0);
    $city = imagecolorallocate($im, 255, 0, 0);
    imagecolortransparent($im, $background);
    imagefill($im, 0, 0, $background);
    
    $dbc = new DBCollection("SELECT * from Player WHERE map=" . $map . " AND status='PC'", $db);
    
    while (! $dbc->eof()) {
        imageline($im, $dbc->get("x") - 1, $dbc->get("y"), $dbc->get("x") + 1, $dbc->get("y"), $city);
        imageline($im, $dbc->get("x"), $dbc->get("y") - 1, $dbc->get("x"), $dbc->get("y") + 1, $city);
        $dbc->next();
    }
    
    imagepng($im, HOMEPATH . MAPS_DYNAMIC_FOLDER . "/mapghostplayer0" . $map . ".png");
}

?>
