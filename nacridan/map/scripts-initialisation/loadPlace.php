#!/usr/bin/php
<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
$db = DB::getDB();

DEFINE("PLAIN", "179 213 137");
DEFINE("MOUNTAIN", "148 126 107");
DEFINE("OCEAN", "0 0 255");
DEFINE("MARSH", "0 150 0");
DEFINE("FOREST", "144 169 88");
DEFINE("DESERT", "233 217 173");
DEFINE("RIVER", "64 128 255");
DEFINE("SNOW", "255 255 255");

// $dbd=new DBCollection("DELETE FROM `Place` WHERE 1",$db,0,0,false);
// $dba=new DBCollection("ALTER TABLE `Place` PACK_KEYS =1 CHECKSUM =0 DELAY_KEY_WRITE =0 AUTO_INCREMENT =1",$db);
function insertDB($str, $level, $x, $y, $map)
{
    global $db;
    $name = "";
    // echo $str;
    switch ($str) {
        case PLAIN:
            $name = "Plaine";
            break;
        case MOUNTAIN:
            $name = "Montagne";
            break;
        case OCEAN:
            $name = "Océan";
            break;
        case MARSH:
            $name = "Marécage";
            break;
        case FOREST:
            $name = "Forêt";
            break;
        case DESERT:
            $name = "Terres de sable";
            break;
        case RIVER:
            $name = "Lac";
            break;
        case SNOW:
            $name = "Terres Gelées";
            break;
        default:
            break;
    }
    if ($name != "") {
        $dbi = new DBCollection("INSERT INTO `Place`( `id` ,`name` ,`level`,`x` ,`y` ,`map`) VALUES ('', '" . $name . "', '" . $level . "', '" . $x . "', '" . $y . "', '" . $map . "')", $db, 0, 0, false);
    }
}

// Choix de la carte
$map = 2;

$im = imagecreatefrompng("mapghost0" . $map . ".png");

$imlevel = imagecreatefrompng("maparea0" . $map . ".png");

for ($j = 0; $j < 400; $j += 5) {
    for ($i = 0; $i < 640; $i += 5) {
        $rgb = imagecolorat($im, $i, $j);
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;
        
        $rgblevel = imagecolorat($imlevel, $i, $j);
        $rlevel = ($rgblevel >> 16) & 0xFF;
        $glevel = ($rgblevel >> 8) & 0xFF;
        $blevel = $rgblevel & 0xFF;
        
        if ($rlevel == 255) {
            $rlevel = 10000;
        }
        
        insertDB($r . " " . $g . " " . $b, $rlevel, $i, $j, $map);
    }
}

?> 
