<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
$db = DB::getDB();

set_time_limit(300);

// Choix de la carte
$map = 1;

function getBuildingCoord($idCity, $X, $Y, $map, $distX, $distY, $mapinfo, $logfile, $db)
{
    $possiblePosition = array();
    
    for ($i = 0; $i < 11; $i ++) {
        for ($j = 0; $j < 11; $j ++) {
            $Xtest = $X - 5 + $i;
            $Ytest = $Y - 5 + $j;
            
            $validzone = $mapinfo->getValidMap($Xtest, $Ytest, 0, $map);
            
            $freePlace = 1;
            $dbp = new DBCollection("SELECT id FROM Building WHERE  x=" . $Xtest . " AND y=" . $Ytest . " AND map=" . $map, $db);
            if (! $dbp->eof())
                $freePlace = 0;
            
            if ($validzone[0][0] && $freePlace && (distHexa($X, $Y, $Xtest, $Ytest) < 4))
                $possiblePosition[] = array(
                    "x" => $Xtest,
                    "y" => $Ytest
                );
        }
    }
    
    if (count($possiblePosition) == 0) {
        fputs($logfile, "\nPROBLEME DE PLACE DANS LE VILLAGE id= ");
        fputs($logfile, $idCity . " validzone= " . $validzone[0][0] . " freePlace=" . $freePlace . "\n");
        return 0;
    }
    
    $randKey = array_rand($possiblePosition);
    return $possiblePosition[$randKey];
}

$mapinfo = new MapInfo($db);

// ----- Bâtiments des Villages

$logfile = fopen('buildingcreation.log', 'a');

$dbc = new DBCollection("SELECT id,x,y FROM City WHERE type='Village' AND map=" . $map, $db, 0, 0);

fputs($logfile, "Villages récupérés \n");

while (! $dbc->eof()) {
    
    // ---- Création du temple, centre du village
    
    $X = $dbc->get("x");
    $Y = $dbc->get("y");
    
    $dbi = new DBCollection("INSERT INTO `Building` (`name`, `id_BasicBuilding`, `id_City`, `id_Player`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
('Temple', 14, " . $dbc->get("id") . ", 0, 100, 100, " . $X . ", " . $Y . ", " . $map . ")", $db, 0, 0);
    
    echo '</br>Village ';
    echo $dbc->get("id") . "</br>";
    
    fputs($logfile, "\n Village id= ");
    fputs($logfile, $dbc->get("id"));
    fputs($logfile, "\n Temple créé ");
    
    // Coordonnées de l'auberge, proche du temple
    
    $coord = getBuildingCoord($dbc->get("id"), $X, $Y, $map, 1, 1, $mapinfo, $logfile, $db);
    if ($coord != 0) {
        $Xtest = $coord["x"];
        $Ytest = $coord["y"];
        
        $dbi = new DBCollection("INSERT INTO `Building` (`name`, `id_BasicBuilding`, `id_City`, `id_Player`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
('Auberge', 1, " . $dbc->get("id") . ", 0, 100, 100, " . $Xtest . ", " . $Ytest . ", " . $map . ")", $db, 0, 0);
        
        fputs($logfile, "\n Auberge créée ");
    } else {
        fputs($logfile, "\n Il manque une Auberge ");
    }
    
    // Coordonnées de l'échoppe
    
    $coord = getBuildingCoord($dbc->get("id"), $X, $Y, $map, 3, 3, $mapinfo, $logfile, $db);
    if ($coord != 0) {
        $Xtest = $coord["x"];
        $Ytest = $coord["y"];
        
        $dbi = new DBCollection("INSERT INTO `Building` (`name`, `id_BasicBuilding`, `id_City`, `id_Player`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
('Échoppe', 5, " . $dbc->get("id") . ", 0, 100, 100, " . $Xtest . ", " . $Ytest . ", " . $map . ")", $db, 0, 0);
        
        fputs($logfile, "\n Echope créée ");
    } else {
        fputs($logfile, "\n Il manque une Echope ");
    }
    
    // Coordonnées de la maison
    
    $coord = getBuildingCoord($dbc->get("id"), $X, $Y, $map, 3, 3, $mapinfo, $logfile, $db);
    if ($coord != 0) {
        $Xtest = $coord["x"];
        $Ytest = $coord["y"];
        
        $dbi = new DBCollection("INSERT INTO `Building` (`name`, `id_BasicBuilding`, `id_City`, `id_Player`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
('Maison', 10, " . $dbc->get("id") . ", 0, 100, 100, " . $Xtest . ", " . $Ytest . ", " . $map . ")", $db, 0, 0);
        
        fputs($logfile, "\n Maison créée ");
    } else {
        fputs($logfile, "\n Il manque une Maison ");
    }
    
    // Coordonnées de la maison
    
    $coord = getBuildingCoord($dbc->get("id"), $X, $Y, $map, 3, 3, $mapinfo, $logfile, $db);
    if ($coord != 0) {
        $Xtest = $coord["x"];
        $Ytest = $coord["y"];
        
        $dbi = new DBCollection("INSERT INTO `Building` (`name`, `id_BasicBuilding`, `id_City`, `id_Player`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
('Maison', 10, " . $dbc->get("id") . ", 0, 100, 100, " . $Xtest . ", " . $Ytest . ", " . $map . ")", $db, 0, 0);
        
        fputs($logfile, "\n Maison créée ");
    } else {
        fputs($logfile, "\n Il manque une Maison \n ");
    }
    
    // Coordonnées du Comptoir de Commerce
    
    $coord = getBuildingCoord($dbc->get("id"), $X, $Y, $map, 4, 4, $mapinfo, $logfile, $db);
    if ($coord != 0) {
        $Xtest = $coord["x"];
        $Ytest = $coord["y"];
        
        $dbi = new DBCollection("INSERT INTO `Building` (`name`, `id_BasicBuilding`, `id_City`, `id_Player`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
('Comptoir Commercial', 4, " . $dbc->get("id") . ", 0, 100, 100, " . $Xtest . ", " . $Ytest . ", " . $map . ")", $db, 0, 0);
        
        fputs($logfile, "\n Comptoir créé \n");
    } else {
        fputs($logfile, "\n Il manque le comptoir de commerce \n ");
    }
    
    $dbc->next();
}

fclose($logfile);

?>
