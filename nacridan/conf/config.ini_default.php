<?php
date_default_timezone_set("UTC");

ini_set("magic_quotes_gpc", 0);

error_reporting(E_ALL); // & ~E_NOTICE);
set_error_handler("nacridanErrorHandler", E_RECOVERABLE_ERROR);
set_exception_handler("nacridanExceptionHandler");
register_shutdown_function("fatal_handler");

DEFINE("APACHEHOST", "localhost"); // to change
DEFINE("MYSQLHOST", "localhost"); // to change

DEFINE("SMTPDOMAIN", "nacridan.com"); // to change
DEFINE("SMTPHOST", "localhost"); // to change
DEFINE("SMTPLOGIN", ""); // to change
DEFINE("SMTPPASS", ""); // to change

DEFINE("DB", "nacridan"); // to change
DEFINE("DRIVER", "mysql");
DEFINE("LOGIN", "root"); // to change
DEFINE("PASSWD", ""); // to change

DEFINE("DBGG", "ggsite"); // to change
DEFINE("LOGINGG", "root"); // to change
DEFINE("PASSWDGG", ""); // to change
DEFINE("DBFORUM", "Forumv2");
DEFINE("LOGINFORUM", "root");
DEFINE("PASSWDFORUM", "");

DEFINE("CONFIG_ROOT", "http://localhost/nacridan"); // to change
DEFINE("CONFIG_HOST", "http://localhost/nacridan"); // to change

DEFINE("PAYPAL", "www.paypal.com");
DEFINE("PAYPAL_ACCOUNT", "nacridan.contact@gmail.com");

DEFINE("HOMEPATH", "D:/Nacridan/nacridan"); // to change

DEFINE("LOG_FOLDER_PATH", "D:/Nacridan/nacridan/"); // to change

DEFINE("DEBUG_MODE", 1);
DEFINE("PROFILER_MODE", 0);

DEFINE("PREC", 10000);

require_once (HOMEPATH . "/class/ErrorManagement.inc.php");

function fatal_handler()
{
    $errfile = "unknown file";
    $errstr = "shutdown";
    $errno = E_CORE_ERROR;
    $errline = 0;
    
    $error = error_get_last();
    
    if ($error !== NULL) {
        $errno = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr = $error["message"];
        nacridanErrorHandler($errno, $errstr, $errfile, $errline);
    }
}

function nacridanErrorHandler($errno, $errstr, $errfile, $errline)
{
    ErrorManagement::nacridanErrorHandler($errno, $errstr, $errfile, $errline);
}

function nacridanExceptionHandler($exception)
{
    ErrorManagement::nacridanExceptionHandler($exception);
}

function nacridanFatalError($msg)
{
    ErrorManagement::nacridanFatalError($msg);
}

function dbLogMessage($msg)
{
    ErrorManagement::dbLogMessage($msg);
}

function feLogMessage($msg)
{
    ErrorManagement::feLogMessage($msg);
}

function getCurrentPageId()
{
    static $uniqueid = null;
    
    if ($uniqueid == null) {
        $uniqueid = md5(uniqid(mt_rand(), true));
    }
    return $uniqueid;
}

function redirect($url, $popup = false)
{
    header('Location: ' . $url);
    exit(0);
}

function getBrowserDefaultLang()
{
    if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {
        $acceptlang = strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
        $lang = explode(",", $acceptlang, 2);
        return $lang[0];
    } else {
        return "en-EN";
    }
}

$nacridanNamespaces = array(
    HOMEPATH . "/class"
);

function nacridanImportClass($className)
{
    global $nacridanNamespaces;
    if (class_exists($className, false))
        return true;
    foreach ($nacridanNamespaces as $path) {
        if (is_dir($path) && is_file($path . '/' . $className . ".inc.php")) {
            require_once ($path . '/' . $className . ".inc.php");
            return class_exists($className, false);
        }
    }
    return false;
}

if (! function_exists("__autoload")) {

    function __autoload($className)
    {
        nacridanImportClass($className);
    }
}

function gmstrtotime($string)
{
    $localtime = strtotime($string);
    $localbits = localtime($localtime, true);
    $gmtime = gmmktime($localbits["tm_hour"], $localbits["tm_min"], $localbits["tm_sec"], $localbits["tm_mon"] + 1, $localbits["tm_mday"], 1900 + $localbits["tm_year"]);
    return $gmtime;
}

function quote_smart($value)
{
    if (is_array($value)) {
        foreach ($value as $key => $val) {
            $value[$key] = quote_smart($val);
        }
    } else {
        if (get_magic_quotes_gpc()) {
            $value = stripslashes($value);
        }
        
        $link = mysql_connect(MYSQLHOST, LOGIN, PASSWD); // ouvre une connexion si ce n'est pas déjà fait.
        $value = mysql_real_escape_string($value, $link);
        
        $value = htmlspecialchars($value, ENT_NOQUOTES);
    }
    return $value;
}

function getGoogleAdsPalette()
{
    switch (mt_rand(0, 2)) {
        case 2:
            $pub = "google_color_border = \"000000\";
google_color_bg = \"FFFFCC\";
google_color_link = \"A9501B\";
google_color_text = \"333333\";
google_color_url = \"002E3F\";";
            break;
        case 1:
            $pub = "google_color_border = \"000000\";
google_color_bg = \"F2984C\";
google_color_link = \"000000\";
google_color_text = \"660000\";
google_color_url = \"002E3F\";";
            break;
        default:
            $pub = "google_color_border = \"000000\";
google_color_bg = \"000000\";
google_color_link = \"FFFFCC\";
google_color_text = \"CCCCCC\";
google_color_url = \"999999\";";
            break;
        
        /*
         * $pub="google_color_border = \"000000\";
         * google_color_bg = \"000000\";
         * google_color_link = \"FFFF66\";
         * google_color_text = \"E895CC\";
         * google_color_url = \"002E3F\";";
         *
         *
         * break;
         */
        /*
         * case 1:
         * $pub="google_color_border = \"000000\";
         * google_color_bg = \"11593C\";
         * google_color_link = \"FFFF66\";
         * google_color_text = \"FFFFFF\";
         * google_color_url = \"002E3F\";";
         * break;
         * case 2:
         * $pub="google_color_border = \"000000\";
         * google_color_bg = \"6131BD\";
         * google_color_link = \"FFFF66\";
         * google_color_text = \"FFFFFF\";
         * google_color_url = \"002E3F\";";
         * break;
         */
    }
    
    return $pub;
}

/* APC BUG */
/* Fatal error: Cannot access parent:: when current class scope has no parent in */
require_once (HOMEPATH . "/class/DBObject.inc.php");

?>
