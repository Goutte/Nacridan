<?php
if ($_SERVER['REMOTE_ADDR'] == "90.56.214.151") {
    return;
}

require_once ("./conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");

page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

if (isset($_POST["username"]) && isset($_POST["password"])) {
    $sess->destroy();
    page_open(array(
        "sess" => "Session",
        "auth" => "Auth"
    ));
}

//require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db);

if (isset($_POST["dla"]) || isset($_POST["form_dla"]) || isset($_POST["newplayer"])) {
    $players = $nacridan->loadSessPlayers();
    foreach ($players as $key => $val) {
        $var = "DLA" . $key;
        $sess->set($var, "on");
    }
}

echo redirect(CONFIG_HOST . "/conquest/conquest.php", true);
?>