<?php

class CQBanned extends HTMLObject
{

    public $nacridan;

    public $db;

    public $id;

    public function CQBanned($nacridan, $id, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->id = $id;
    }

    public function toString()
    {
        $db = $this->db;
        $sess = $this->nacridan->sess;
        $auth = $this->nacridan->auth;
        
        $str = "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgbody'>" . localize("Ce compte a été désactivé") . "</td></tr>";
        $dbs = new DBCollection("SELECT * FROM Banned WHERE id_Player\$target=" . $this->id, $db);
        $str .= "<tr><td class='mainbgbody'><b>" . localize("Motif : ") . nl2br($dbs->get("body")) . "</b></td></tr></table>";
        return $str;
    }

    public function render()
    {
        echo $this->toString();
    }
}
?>
