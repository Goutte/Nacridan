<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/class/Detail.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];

Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$player = null;
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$isBodySet = isset($_GET["body"]);
if ($isBodySet) {
    $bodyValue = $_GET["body"];
} else {
    $bodyValue = 0;
}

if (isset($_GET["id"]))
    $_GET["id"] = quote_smart($_GET["id"]);

if (isset($_POST["search"])) {
    if (is_numeric($_POST["search"])) {
        $_GET["id"] = $_POST["search"];
    } else {
        $dbp = new DBCollection("SELECT id FROM Player WHERE name='" . quote_smart($_POST["search"]) . "'", $db);
        if (! $dbp->eof()) {
            $_GET["id"] = $dbp->get("id");
        }
    }
}

if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $player = new Player();
    $player->externDBObj("BasicRace,Modifier,Team,Detail");
    $player->load($id, $db);
    
    $state = array(
        localize("Agonisant"),
        localize("Gravement blessé(e)"),
        localize("Blessé(e)"),
        localize("Légèrement blessé(e)"),
        localize("Indemne"),
        localize("est mort"),
        localize("vit paisiblement dans son monde")
    );
    
    $hp = $player->get("hp");
    if ($hp != 0) {
        $curstate = min(floor($player->get("currhp") * 5 / $hp), 4);
    } else {
        $dbe = new DBCollection("SELECT * FROM Event WHERE typeAction=605 AND id_Player\$src=" . $id, $db);
        if (! $dbe->eof())
            $curstate = 6;
        else {
            $curstate = 5;
        }
        $bodyValue = 1;
        $isBodySet = true;
    }
    
    $str = "<table><tr><td style='vertical-align: top'><table class='maintable mainbgtitle' width='640px'>\n";
    $str .= "<tr><td>\n";
    
    $extra = "";
    if ($isBodySet) {
        $extra = "&body=1";
    }
    
    if ($player->get("status") == "NPC" || $player->get("id") == 0) {
        $str .= "<b><h1>";
        if ($curstate >= 5)
            $str .= "Ce monstre " . $state[$curstate];
        else
            $str .= localize($player->get('racename'));
        $str .= "</h1></b></td>\n";
        $str .= "<td align='right'>\n";
        $str .= "&nbsp;<br/><br/><form action='?id=" . $id . $extra . "' method='POST'>" . localize("Recherche (n°/nom)") .
             " <input name='search' type='text' size=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</form></td>\n";
    } else {
        $str .= "<b><h1>" . $player->get('name') . "</h1></b></td>\n";
        $str .= "<td align='right'>\n";
        
        if ($curplayer->get("id_Mission") == 0) {
            $str .= Envelope::render($player->get('name'), 'Envoyer un message', 'tabcontact toOpener');
            $str .= "<a href='../conquest/conquest.php?center=contact&add=" . $player->get('id') .
                 "' class='tabcontact toOpener'>Ajouter à vos contacts</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        
        $str .= "&nbsp;<br/><br/><form action='?id=" . $id . $extra . "' method='POST'>" . localize("Recherche (n°/nom)") .
             " <input name='search' type='text' size=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</form></td>\n";
    }
    $str .= "</tr>\n";
    $str .= "</table>\n";
    if ($player->get("status") == "PC" || ($player->get("status") == "NPC" && $curstate < 5)) {
        $str .= "<a href='profile.php?id=" . $id . "' class='tabmenu'>&nbsp;&nbsp;" . localize('I N F O R M A T I O N') . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;\n";
        $str .= "<a href='profile.php?id=" . $id . "&body=1' class='tabmenu'>" . localize('É V È N E M E N T S') . "</a>\n";
        
        if ($player->get("status") == "PC")
            $str .= "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='profile.php?id=" . $id . "&body=2' class='tabmenu'>" . localize('É Q U I P E M E N T') . "</a>\n";
        $str .= "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='profile.php?id=" . $id . "&body=3' class='tabmenu'>" . localize('E N C H A N T E M E N T') . "</a>\n";
        $str .= "<br/><br/>\n";
    }
    
    if (! $isBodySet) {
        if ($player->get("disabled") == 99) {
            require_once (HOMEPATH . '/conquest/cqbanned.inc.php');
            $centerobj = new CQBanned($nacridan, $player->get("id"), $db);
            $str .= "<table class='maintable mainbgtitle' width='640px'>\n";
            $str .= "<tr><td>\n";
            $str .= "<b><h1>" . $player->get('name') . "</h1></b></td>\n";
            $str .= "</tr></table><table class='maintable mainbgtitle' width='640px'><tr><td>";
            $str .= $centerobj->toString();
            $str .= "</td></tr><table>";
            if ($curplayer->get("authlevel") > 10) {
                $str .= "<table class='maintable'><tr><td class='mainbgbody'><form action='disable.php?enable=1&id=" . $id . "' method='POST'>" .
                     localize("Ce compte a été suspendu : ") . " <input name='Enable' type='submit' value='réactiver ce compte' size=10><input name='idform' type='hidden' value='" .
                     getCurrentPageId() . "' /><input name='Modify' type='submit' value='Modifier les raisons' size=10><input name='idform' type='hidden' value='" .
                     getCurrentPageId() . "' /></form></td></tr></table>";
            }
            $MAIN_BODY->add($str);
        } else {
            
            $str .= "<table><tr><td valign='top'>\n";
            $str .= "<table class='maintable' width='270px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('I N F O R M A T I O N') . "</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            if ($player->get("status") == "NPC")
                $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('NOM :') . "</b></td><td class='mainbgbody'>" . localize($player->get('name')) . " (" .
                     $_GET["id"] . ")</td></tr>\n";
            else
                $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('NOM :') . "</b></td><td class='mainbgbody'>" . $player->get('name') . ' (' . $player->get('id') .
                     ')' . "</td></tr>\n";
            
            if ($player->get("id_Team") > 0) {
                $alliegance = "<a href='../conquest/alliegance.php?id=" . $player->get("id_Team") . "'  class='styleall'>" . $player->getSub('Team', 'name') . "</a>";
                
                $dbrank = new DBCollection(
                    "SELECT TeamRankInfo.name FROM TeamRank LEFT JOIN TeamRankInfo ON TeamRankInfo.id=id_TeamRankInfo WHERE id_Player=" . $player->get("id") .
                         " AND TeamRank.id_Team=" . $player->get("id_Team"), $db);
                $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ALLÉGEANCE :') . "</b></td><td class='mainbgbody'>" . $alliegance . "<br/>" .
                     $dbrank->get('name') . "</td></tr>\n";
            } else {
                $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ALLÉGEANCE :') . "</b></td><td class='mainbgbody'>" . localize("Aucune") . "</td></tr>\n";
            }
            
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('LEVEL :') . "</b></td><td class='mainbgbody'>" . $player->get('level') . "</td></tr>\n";
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('CLASSE :') . "</b></td><td class='mainbgbody'>" . localize($player->getSub('BasicRace', 'class')) .
                 "</td></tr>\n";
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('RACE :') . "</b></td><td class='mainbgbody'>" . localize($player->getSub('BasicRace', 'name')) .
                 "</td></tr>\n";
            
            $gender = $player->get('gender');
            if ($gender == "M")
                $gender = "Homme";
            else
                $gender = "Femme";
            
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('SEXE :') . "</b></td><td class='mainbgbody'>" . localize($gender) . "</td></tr>\n";
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ÉTAT :') . "</b></td><td class='mainbgbody'>" . $state[$curstate] . "</td></tr>\n";
            
            if ($player->get("id") != 0) {
                $time = strtotime($player->get('creation'));
                $curtime = time();
                $nbday = floor(($curtime - $time) / 3600 / 24) . " " . localize("jours");
                $time = gmdate("Y-m-d", $time);
            } else {
                $nbday = "-";
                $time = "-";
            }
            
            $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('CRÉATION :') . "</b></td><td class='mainbgbody'>" . $time . "</td></tr>\n";
            $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('ANCIENNETÉ :') . "</b></td><td class='mainbgbody'>" . $nbday . "</td></tr>\n";
            if ($player->get("id_BasicRace") == 5)
                $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('ARRESTATION(S) :') . "</b></td><td class='mainbgbody'>" . $player->get('nbkill') . "</td></tr>\n";
            else
                $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('MEURTRE(S) :') . "</b></td><td class='mainbgbody'>" . $player->get('nbkill') . "</td></tr>\n";
            if ($player->get("modeAnimation") == 1)
                $str .= "<tr><td class='mainbglabel' width='120px'><b>" . localize('PARTICULARITÉ :') . "</b></td><td bgcolor='#3b302b'><b>Mode animation actif</b></td></tr>\n";
            
            if ($player->get("id_BasicRace") == 5)
                $str .= "<tr><td class='mainbglabel' width='115px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>" . localize('de force:') . "</b></td><td class='mainbgbody'>" .
                     $player->get('nbkillpc') . "</td></tr>\n";
            else
                $str .= "<tr><td class='mainbglabel' width='115px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>" . localize('Dont PJ :') . "</b></td><td class='mainbgbody'>" .
                     $player->get('nbkillpc') . "</td></tr>\n";
            
            if ($player->get("status") == "PC") {
                $str .= "<tr><td class='mainbglabel' width='115px'><b>" . localize('DÉCÈS :') . "</b></td><td class='mainbgbody'>" . $player->get('nbdeath') . "</td></tr>\n";
                $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('SURSIS PRISON :') . "</b></td><td bgcolor='#3b302b'>" . floor($player->get('prison') * 12) .
                     " heures</td></tr>\n";
            }
            $str .= "</table>\n";
            $str .= "</td><td>";
            switch ($player->get("id_BasicRace")) {
                case 104:
                    $str .= " <a href='http://my.opera.com/Berien/blog/2011/01/02/images'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 107:
                    $str .= " <a href='http://my.opera.com/Berien/blog/2011/01/02/images'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 112:
                    $str .= " <a href='http://celineandstfu.fr'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 117:
                    $str .= " <a href='http://l-archi.deviantart.com/'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 118:
                    $str .= " <a href='http://l-archi.deviantart.com/'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 120:
                    $str .= " <a href='http://my.opera.com/Berien/blog/2011/01/02/images'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 122:
                    $str .= " <a href='http://my.opera.com/Berien/blog/2011/01/02/images'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                case 125:
                    $str .= " <a href='http://celineandstfu.fr'> <img src='" . getImgSrc($player, $db) . "'></a></td></tr>";
                    break;
                default:
                    $str .= "<img src='" . getImgSrc($player, $db) . "'>";
                    // $str.="<div class='mainbglabel'>".getImgSrc($player,$db)."</div>";
                    break;
            }
            $str .= "</table>";
            
            $str .= "<br/>\n";
            $str .= "<table class='maintable' width='590px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D E S C R I P T I O N') . "</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            if ($player->get("id_Detail") == 0)
                $str .= "<tr><td class='mainbgbody'>&nbsp;" . bbcode($player->getSub('BasicRace', 'description')) . "</td></tr>\n";
            else
                $str .= "<tr><td class='mainbgbody'>" . bbcode($player->getSub('Detail', 'body')) . "</td></tr>\n";
            
            $str .= "</table>";
            
            if ($curplayer->get("authlevel") > 10) {
                if ($player->get("disabled") == 0) {
                    $str .= "<table class='maintable'><tr><td class='mainbgbody'><form action='disable.php?id=" . $id . "' method='POST'>" . localize("Triche détectée : ") .
                         " <input name='submit' type='submit' value='désactiver le compte' size=10><input name='idform' type='hidden' value='" . getCurrentPageId() .
                         "' /></form></td></tr></table>";
                }
            }
            
            $MAIN_BODY->add($str);
        }
    } else {
        $MAIN_BODY->add($str);
        if ($bodyValue == 1) {
            require_once (HOMEPATH . "/conquest/cqevent.inc.php");
            $MAIN_BODY->add(new CQEvent($nacridan, "centerareawidth", $db));
        } elseif ($bodyValue == 2) {
            require_once (HOMEPATH . "/class/Equipment.inc.php");
            $str = "<table class='maintable' width='500px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('É Q U I P E M E N T &nbsp;&nbsp;&nbsp; U T I L I S É') . "&nbsp;&nbsp;&nbsp;</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $modifier = new Modifier();
            
            $dbe = new DBCollection(
                "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") .
                     ", mask,wearable,BasicEquipment.durability AS dur, Equipment.maudit FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE EquipmentType.small='No' AND (Equipment.weared != 'No' OR Equipment.maudit=1) AND Equipment.id_Player=" .
                     $id . " order by EQweared asc, EQname asc", $db);
            
            while (! $dbe->eof()) {
                $eq->DBLoad($dbe, "EQ");
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                
                $extraname = "";
                
                if ($eq->get("extraname") != "") {
                    $i = 0;
                    $tmodifier = new Modifier();
                    $template = new Template();
                    $dbt = new DBCollection(
                        "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                             $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                             " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                             $eq->get("id") . " order by Template.pos asc", $db);
                    while (! $dbt->eof()) {
                        $i ++;
                        $template->DBLoad($dbt, "TP");
                        $tmodifier->DBLoad($dbt, "MD");
                        $tmodifier->updateFromTemplateLevel($template->get("level"));
                        $eqmodifier->addModifObj($tmodifier);
                        
                        if ($i == 1)
                            $extraname = $dbt->get("name1");
                        if ($i == 2)
                            $extraname .= " " . $dbt->get("name2");
                        
                        $dbt->next();
                    }
                }
                $eqmodifier->initCharacStr();
                
                $str .= "<tr>";
                $str .= "<td class='mainbgbody'><b>";
                $str .= "<span class='" . ($dbe->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span>";
                $str .= "</b>";
                
                if ($eq->get("extraname") != "") {
                    $str .= "<span class='template'>&nbsp;" . $extraname . "</span>";
                }
                if ($eq->get("weared") == "No") {
                    $str .= "<span class=''>&nbsp;(Non équipé)</span>";
                }
                
                $str .= "</td></tr>";
                
                $dbe->next();
            }
            
            $str .= "</table><br/><br/>";
            $MAIN_BODY->add($str);
        } else {
            $str = "<table class='maintable' width='500px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('E N C H A N T E M E N T S &nbsp;&nbsp;  ET &nbsp;&nbsp;  M A L É D I C T I O N S') . "</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $id, $db);
            if (! $dbbm->eof()) {
                $bm = new StaticModifier();
                $bm->setAttach("StaticModifier_BM");
                $item = array();
                while (! $dbbm->eof()) {
                    $suite = "";
                    if ($dbbm->get("name") != "Embuscade") {
                        if ($dbbm->get("name") == "Malédiction d'Arcxos") {
                            $ident = $dbbm->get("id_StaticModifier_BM");
                            if ($ident == 6)
                                $suite = " : 'Dextérité'";
                            if ($ident == 7)
                                $suite = " : 'Force'";
                            if ($ident == 8)
                                $suite = " : 'Vitesse'";
                        }
                        
                        if ($dbbm->get("name") == "Ailes de Colère") {
                            $ident = $dbbm->get("id_StaticModifier_BM");
                            if ($ident == 5)
                                $suite = " : 'Dégâts'";
                            if ($ident == 4)
                                $suite = " : 'Attaque'";
                        }
                        if ($dbbm->get("name") == "défendu") {
                            $dbc = new DBCollection("SELECT name FROM Player WHERE id=" . $dbbm->get("id_Player\$src"), $db);
                            $suite = " par " . $dbc->get("name");
                        }
                        $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize($dbbm->get("name") . $suite) . "</b></td>></tr>\n";
                    }
                    $dbbm->next();
                }
            }
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player\$src=" . $id . " AND name='défendu'", $db);
            if ($dbbm->count()) {
                $dbc = new DBCollection("SELECT name FROM Player WHERE id=" . $dbbm->get("id_Player"), $db);
                $suite = " : '" . $dbc->get("name") . "'";
                $str .= "<tr><td class='mainbglabel' width='130px'><b> Protection" . $suite . "</b></td></tr>\n";
            }
            $str .= "</table>\n";
            $MAIN_BODY->add($str);
        }
    }
    
    $MAIN_BODY->add("</td><td style='vertical-align: top'>");
    
    // if($curplayer->get("advert")==-1)
    // ECHANGE De BANNIERES
    
    $MAIN_BODY->add(
        '<script type="text/javascript">
    micropolia_echban_id      = 760;
    micropolia_echban_width   = 120;
    micropolia_echban_height  = 600;
    micropolia_echban_jeu     = 13509;
</script>
<script src="http://www.jeu-gratuit.net/tracking/affiche.js" type="text/javascript"></script> <script type="text/javascript">
    micropolia_echban_id      = 778;
    micropolia_echban_width   = 88;
    micropolia_echban_height  = 31;
    micropolia_echban_jeu     = 13509;
</script>
<script src="http://www.jeu-gratuit.net/tracking/affiche.js" type="text/javascript"></script>');
    
    $MAIN_BODY->add("</td></tr></table>");
    
    $MAIN_BODY->add("<script src='" . CONFIG_HOST . "/javascript/astrack.js' type='text/javascript'></script>\n");
    
    $google = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68594660-1', 'auto');
  ga('send', 'pageview');

</script>";
    $MAIN_BODY->add($google);
    
    $MAIN_PAGE->render();
}
