<?php
require_once (HOMEPATH . "/factory/CityFactory.inc.php");
require_once (HOMEPATH . "/class/City.inc.php");

class CQAttack extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAttack($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        $room = $curplayer->get("room");
        $inbuilding = $curplayer->get("inbuilding");
        
        if ($curplayer->get("ap") < $curplayer->getScore("timeAttack")) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser une attaque.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td  width='80px'>";
            $str .= localize("Attaquer") . " </td><td><select id='selector_id' class='selector cqattackselectorsize' name='TARGET_ID'>";
            
            $gap = 1;
            require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
            $TypeArm = PlayerFactory::getTypeArm($curplayer, $db);
            $action = ATTACK;
            if ($TypeArm == 5 or $TypeArm == 22) {
                $gap = 3;
                $action = ARCHERY;
            }
            if ($curplayer->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
                $gap = 3;
                $action = SPELL_FIREBALL;
            }
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
            
            $dbncp = array();
            $dbp = array();
            
            for ($i = 0; $i < $gap + 1; $i ++) {
                // PNJ
                $dbnpc[] = new DBCollection(
                    "SELECT * FROM Player WHERE status='NPC' AND disabled=0 AND (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND map=" . $map .
                         " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=" . $i, $db, 0, 0);
                // PJ
                $dbp[] = new DBCollection(
                    "SELECT * FROM Player WHERE status='PC' AND disabled=0 AND (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND map=" . $map .
                         " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=" . $i, $db, 0, 0);
            }
            
            if ($action == ATTACK) {
                
                if ($inbuilding) {
                    $item[] = array(
                        localize("-- PNJ à portée -- ") => - 1
                    );
                    while (! $dbnpc[0]->eof()) {
                        if ($dbnpc[0]->get("id") != $id && $curplayer->canSeePlayerById($dbnpc[0]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[0]->get("racename")) . " (" . $dbnpc[0]->get("id") . ")" => $dbnpc[0]->get("id")
                            );
                        $dbnpc[0]->next();
                    }
                    $item[] = array(
                        localize("-- PJ à portée -- ") => - 1
                    );
                    while (! $dbp[0]->eof()) {
                        if ($dbp[0]->get("id") != $id && $curplayer->canSeePlayerById($dbp[0]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[0]->get("name")) . " (" . $dbp[0]->get("id") . ")" => $dbp[0]->get("id")
                            );
                        $dbp[0]->next();
                    }
                } else {
                    $item[] = array(
                        localize("-- PNJ à portée -- ") => - 1
                    );
                    while (! $dbnpc[1]->eof()) {
                        if ($dbnpc[1]->get("id") != $id && $curplayer->canSeePlayerById($dbnpc[1]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[1]->get("racename")) . " (" . $dbnpc[1]->get("id") . ")" => $dbnpc[1]->get("id")
                            );
                        $dbnpc[1]->next();
                    }
                    $item[] = array(
                        localize("-- PJ à portée -- ") => - 1
                    );
                    while (! $dbp[1]->eof()) {
                        if ($dbp[1]->get("id") != $id && $curplayer->canSeePlayerById($dbp[1]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[1]->get("name")) . " (" . $dbp[1]->get("id") . ")" => $dbp[1]->get("id")
                            );
                        $dbp[1]->next();
                    }
                }
            } else {
                // Cas des remparts
                if ($room == TOWER_ROOM) {
                    for ($i = 0; $i < $gap + 1; $i ++) {
                        $cond = "";
                        if ($i > 1 && $action == ARCHERY)
                            $cond = (10 * ($i - 1)) . " % malus attaque";
                        
                        if (! $dbnpc[$i]->eof())
                            $item[] = array(
                                localize("-- PNJ à " . $i . " case(s) -- " . $cond) => - 1
                            );
                        while (! $dbnpc[$i]->eof()) {
                            if ($dbnpc[$i]->get("id") != $id && $curplayer->canSeePlayerById($dbnpc[$i]->get("id"), $db))
                                $item[] = array(
                                    localize($dbnpc[$i]->get("racename")) . " (" . $dbnpc[$i]->get("id") . ")" => $dbnpc[$i]->get("id")
                                );
                            $dbnpc[$i]->next();
                        }
                        if (! $dbp[$i]->eof())
                            $item[] = array(
                                localize("-- PJ à " . $i . " case(s) -- " . $cond) => - 1
                            );
                        while (! $dbp[$i]->eof()) {
                            if ($dbp[$i]->get("id") != $id && $curplayer->canSeePlayerById($dbp[$i]->get("id"), $db))
                                $item[] = array(
                                    localize($dbp[$i]->get("name")) . " (" . $dbp[$i]->get("id") . ")" => $dbp[$i]->get("id")
                                );
                            $dbp[$i]->next();
                        }
                    }
                } elseif ($inbuilding) {
                    $item[] = array(
                        localize("-- PNJ à portée -- ") => - 1
                    );
                    while (! $dbnpc[0]->eof()) {
                        if ($dbnpc[0]->get("id") != $id && $curplayer->canSeePlayerById($dbnpc[0]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[0]->get("racename")) . " (" . $dbnpc[0]->get("id") . ")" => $dbnpc[0]->get("id")
                            );
                        $dbnpc[0]->next();
                    }
                    $item[] = array(
                        localize("-- PJ à portée -- ") => - 1
                    );
                    while (! $dbp[0]->eof()) {
                        if ($dbp[0]->get("id") != $id && $curplayer->canSeePlayerById($dbp[0]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[0]->get("name")) . " (" . $dbp[0]->get("id") . ")" => $dbp[0]->get("id")
                            );
                        $dbp[0]->next();
                    }
                } else {
                    for ($i = 1; $i < $gap + 1; $i ++) {
                        $cond = "";
                        if ($i > 1 && $action == ARCHERY)
                            $cond = (10 * ($i - 1)) . " % malus attaque";
                        
                        if (! $dbnpc[$i]->eof())
                            $item[] = array(
                                localize("-- PNJ à " . $i . " case(s) -- " . $cond) => - 1
                            );
                        while (! $dbnpc[$i]->eof()) {
                            if ($dbnpc[$i]->get("id") != $id && $curplayer->canSeePlayerById($dbnpc[$i]->get("id"), $db))
                                $item[] = array(
                                    localize($dbnpc[$i]->get("racename")) . " (" . $dbnpc[$i]->get("id") . ")" => $dbnpc[$i]->get("id")
                                );
                            $dbnpc[$i]->next();
                        }
                        if (! $dbp[$i]->eof())
                            $item[] = array(
                                localize("-- PJ à " . $i . " case(s) -- " . $cond) => - 1
                            );
                        while (! $dbp[$i]->eof()) {
                            if ($dbp[$i]->get("id") != $id && $curplayer->canSeePlayerById($dbp[$i]->get("id"), $db))
                                $item[] = array(
                                    localize($dbp[$i]->get("name")) . " (" . $dbp[$i]->get("id") . ")" => $dbp[$i]->get("id")
                                );
                            $dbp[$i]->next();
                        }
                    }
                }
            }
            
            // Création du selecteur
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1) {
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    } else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td>";
            
            if ($action == ARCHERY) {
                
                $str .= "<td align='center' rowspan=2><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value=" . $action . " />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' /></td></tr>\n";
                
                $arrow = array();
                $str .= "<tr class='mainbgtitle'><td>Avec : </td><td> <select id='selector_id' class='selector cqattackselectorsize' name='ARROW_ID'>";
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une flèche --") . "</option>";
                $dba = new DBCollection("SELECT * FROM Equipment WHERE id_Equipment\$bag= 0 AND id_EquipmentType=28  AND id_Player=" . $id, $db, 0, 0);
                while (! $dba->eof()) {
                    $arrow[] = array(
                        localize($dba->get("name")) . " niv " . $dba->get("level") . " (" . $dba->get("id") . ")" => $dba->get("id")
                    );
                    $dba->next();
                }
                
                if ($id_Bag = playerFactory::getIdBagByNum($id, 5, $db)) {
                    $arrow[] = array(
                        localize("-- Carquois --") => - 1
                    );
                    $dba = new DBCollection("SELECT * FROM Equipment WHERE id_Equipment\$bag=" . $id_Bag . " AND id_EquipmentType=28  AND id_Player=" . $id, $db, 0, 0);
                    while (! $dba->eof()) {
                        $arrow[] = array(
                            localize($dba->get("name")) . " " . localize("Niveau") . " " . $dba->get("level") . " (" . $dba->get("id") . ")" => $dba->get("id")
                        );
                        $dba->next();
                    }
                }
                
                foreach ($arrow as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1) {
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        } else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select></td>";
            } else {
                $str .= "<td align='center'><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value=" . $action . " />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr>";
            }
            $str .= "</table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
 }
}
?>
