<?php

class CQFAQ extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQFAQ($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $str = "<table class='maintable centerareawidth'>\n";
        $str .= "<tr class='mainbgtitle'>";
        $str .= "<td colspan='2' class='mainbgtitle'>\n<b><h1> Foire Aux Questions </h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		Voici la liste des questions fréquentes :<br /><br />
		<ul>
		<li><a class='stylepc' href='#Comment-choisir-mon-profil'>Comment choisir mon profil ? Mon arme ? Quels sont les choix d’évolution possibles ?</a></li><br />
		<li><a class='stylepc' href='#Comment-faire-evoluer-mon-perso'>Comment faire évoluer mon personnage (gagner des niveaux) ?</a></li><br />
		<li><a class='stylepc' href='#La-rose-des-vents'>La « rose des vents » sous mon personnage a trop de branches ?</a></li><br />
		<li><a class='stylepc' href='#Je-n-arrive-pas-à-tuer-un-monstre'>Je n’arrive même pas à tuer un monstre niveau 1 ?</a></li><br />
		<li><a class='stylepc' href='#Quel-est-mon-equipement'>Quel est mon équipement au départ ?</a></li><br />
		<li><a class='stylepc' href='#Comment-passer-au-niveau-superieur'>Comment passer au niveau supérieur ?</a></li><br />
		<li><a class='stylepc' href='#Comment-evaluer-mes-deplacements'>Comment évaluer mes déplacements ?</a></li><br />
		<li><a class='stylepc' href='#J-arrive-devant-les-remparts-d-une-ville'>J’arrive devant les remparts d’une Ville, dois-je faire le tour pour passer par la porte ?</a></li><br />
		<li><a class='stylepc' href='#J-ai-tue-mon-premier-monstre'>J’ai tué mon premier « monstre », que dois-je faire ?</a></li><br />
		<li><a class='stylepc' href='#C-est-quoi-ces-grosses-tortues-geantes'>C’est quoi ces grosses tortues géantes ?</a></li><br />
		<li><a class='stylepc' href='#C-est-quoi-ces-monstres-a-carapace-grise'>C’est quoi ces gros monstres bizarres avec une grosse carapace grise ?</a></li><br />
		<li><a class='stylepc' href='#C-est-quoi-ces-gros-rochers'>C’est quoi ces gros rochers au milieu du jeu de quille ?</a></li><br />
		<li><a class='stylepc' href='#Hier-j-etais-a-un-endroit-different'>Hier j’étais à un endroit et ce matin il me semble que mon personnage a bougé tout seul ?</a></li><br />
		<li><a class='stylepc' href='#Comment-verifier-mes-actions-passees'>Comment vérifier mes actions passées ?</a></li><br />
		<li><a class='stylepc' href='#Quelqu-un-m-a-envoye-un-message'>Quelqu’un m’a envoyé un message ?</a></li><br />
		<li><a class='stylepc' href='#J-ai-recu-un-message-d-un-nain-pas-content'>J’ai reçu un message d’un Nain pas content du tout ?</a></li><br />
		<li><a class='stylepc' href='#Je-suis-perdu'>Je suis perdu ? Y a pas un plan ?</a></li><br />
		<li><a class='stylepc' href='#Je-me-retrouve-dans-le-temple'>Je me retrouve à nouveau au Temple et je n’ai rien fait ?</a></li><br />
		<li><a class='stylepc' href='#'>J’ai reçu un message de la garde qui me dit que je vais aller en prison ?</a></li><br />
		<li><a class='stylepc' href='#Qu'arrive-t-il-a-mon-equipement-si-mort'>Qu’arrive-t-il à mon équipement si je meurs ?</a></li><br />
		<li><a class='stylepc' href='#Mon-arme-est-cassee'>Mon arme est cassée ?</a></li><br />
		<li><a class='stylepc' href='#Ou-acheter-de-l-equipement'>Où acheter de l’équipement ?</a></li><br />
		<li><a class='stylepc' href='#Je-pars-en-vacances'>Je pars en vacances et je ne pourrais pas jouer mon personnage, que faire ?</a></li><br />
		<li><a class='stylepc' href='#Puis-je-porter-n-importe-quel-equipement'>Puis-je porter n’importe quel équipement ?</a></li><br />
		<li><a class='stylepc' href='#Plus-de-dix-places-d-equipement'>Y a-t-il un moyen pour transporter plus de choses que les 10 places de mon sac d’équipement?</a></li><br />
		<li><a class='stylepc' href='#Que-faire-de-mes-PO'>Que faire de toutes mes pièces d’or (POs)?</a></li><br />
		<li><a class='stylepc' href='#Comment-faire-du-commerce'>Comment faire du commerce ? Vendre ou acheter entre joueurs ?</a></li><br />
		<li><a class='stylepc' href='#Comment-obtenir-une-mission-d-escorte'>Comment obtenir une mission d’escorte ?</a></li><br />
		<li><a class='stylepc' href='#Comment-effectuer-une-mission-d-escorte'>Comment effectuer une mission d’escorte ?</a></li><br />
		<li><a class='stylepc' href='#Comment-organiser-une-caravane'>Comment organiser une caravane ?</a></li><br />
		<li><a class='stylepc' href='#Je-veux-personnaliser-mon-profil-IG'>Je voudrais personnaliser mon profil in-game ?</a></li><br />
		<li><a class='stylepc' href='#Je-veux-m-associer-a-d-autres-joueurs'>Je voudrais m’associer avec d’autres joueurs, comment faire ?</a></li><br />
		<li><a class='stylepc' href='#Quand-je-clique-sur-concentration-rien'>Quand je clique sur « Concentration », rien ne se passe ?</a></li><br />
		<li><a class='stylepc' href='#Puis-je-faire-deccendre-une-jauge'>Puis-je faire descendre une de mes jauges d’évolution ?</a></li><br />
        <li><a class='stylepc' href='#Quels-dangers-auxquels-il-faut-etre-préparé'>Quels sont les dangers auxquels il faut être préparé ?</a></li>   
		</ul>
		<br /></td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-choisir-mon-profil'><b>Comment choisir mon profil ? Mon arme ? Quels sont les choix d’évolution possibles ?</b><br /><br />
		Sans entrer dans les détails, toute votre évolution est basée ici sur le principe : « deviens ce que tu fais ».<br />
		Sous la vue, ONGLET profil : Vous voyez vos caractéristiques de base et vos « jauges d’évolution ».
		Chaque action influence ces jauges qui détermineront quelle caractéristique augmentera au passage de niveau.<br />
		De manière TRES simplifiée :<br />
		Chaque arme, équipement, compétence, sortilège, artisanat est lié à une (ou plusieurs) caractéristique de base.<br />
		Exemple: Une arme intermédiaire favorisera la montée de vos jauges de Dextérité (+6) et Force (+2)
		De même pour les compétences. Une compétence comme « Premier soins » fera monter votre jauge de Dextérité.<br />
		Et ainsi de suite pour chaque action possible.<br />
		Il vous faut donc réfléchir au type d’aventurier que vous souhaitez devenir et vous intéresser à ce qui influencera vos jauges dans ce sens.<br />
		Si vous souhaitez devenir un « bon gros bourrin indestructible », intéressez-vous à tout ce qui concerne la Force, par exemple.<br />
		Attention, ça vaut la peine de prendre le temps de s’intéresser aux règles avant de faire un choix définitif !<br /><br />
		<em>Voir Les Règles > ONGLET Combats – Expérience – Compétences – Sortilèges – Equipement – Savoir-faire.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
        <div id='Comment-faire-evoluer-mon-perso'><b>Comment faire évoluer mon personnage (gagner des niveaux) ?</b><br /><br />
        On gagne un niveau quand on a accumulé suffisamment de PXs (Points d’Expérience).<br /><br />
        A droite de la vue entre PA (Point d’Action) et PO (Pièces d’Or) > PX : 0/30 (au niv 1).<br /><br />
        L’utilisation d’un artisanat, d’une compétence ou d’un sort fait gagner des PXs. Mais ce qui en rapporte le plus est de tuer des monstres (PvE) ou d’autres personnages (PvP).<br /><br />
		<em>Voir Les Règles > ONGLET Expérience .</em>
        </div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='La-rose-des-vents'><b>La « rose des vents » sous mon personnage a trop de branches ?</b><br /><br />
		Contrairement à une rose classique celle-ci à 6 branches. C’est à cause du découpage de la carte en petits hexagone (plutôt qu’en carrés).<br />
		Il y a le Nord > Nordan, le Sud> Sudan, l’ouest > Ostan, l’est > Estan.<br />
		Et en plus : Isan > Nord-est et Dustan > Sud-ouest.<br />
		Vous pouvez désactiver la rose des vents de votre vue si vous le souhaitez > Sous votre vue, ONGLET Options.
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Quel-est-mon-equipement'><b>Quel est mon équipement au départ ?</b><br /><br />
		Avez-vous équipé votre arme ?<br />
		Au début du jeu, l’arme choisie se trouve dans le sac d’équipement. Sélectionner en haut à gauche de la vue : Objet  > S’équiper > Puis sous la vue : sélectionner l’arme > action.<br />
		Le jet d’attaque et de défense dépend du type d’arme et de l’équipement porté.<br /><br />
		<em>Voir : Les Règles > ONGLET Combats et ONGLET Equipements.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-passer-au-niveau-superieur'><b>Comment passer au niveau supérieur ?</b><br /><br />
		En haut à droite de votre vue, vous pouvez lire votre nom et en dessous : Vos caractéristiques, vos coordonnées, vos PAs disponibles, vos PXs et vos POs.<br />
		Les PXs (points d’expérience) sont indiqués de la façon suivante : par exemple 12/30. Arrivé à 30/30, vous passez au niveau supérieur.<br />
		Attention, en cas de mort, vous perdez 50% de vos PX !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-evaluer-mes-deplacements'><b>Comment évaluer mes déplacements ?</b><br /><br />
		Les déplacements utilisent des PAs > Points d’Action.<br />
		Pour se déplacer en plaine (Plaine et Plaine fleurie) c’est 1PA/1 case.<br />
		Sur les routes et dans les Villages (Terre battue) et dans les Villes (Espace pavé) c’est 0,5 PA/1 case.<br />
		Attention, sur certains terrains (Forêt, montagne) les déplacements coutent plus !<br />
		Sous votre vue > ONGLET Evènements. Vous pouvez voir tous vos déplacements. Notez que les déplacements dans des bâtiments ne sont pas comptabilisés.
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='J-arrive-devant-les-remparts-d-une-ville'><b>J’arrive devant les remparts d’une Ville, dois-je faire le tour pour passer par la porte ?</b><br /><br />
		Non !<br />
		Vous pouvez entrer et sortir des remparts à n’importe quel endroit.<br />
		Attention, dans le cas d’un Village contrôlé par des joueurs, il se peut que l’entrée soit impossible sans l’accord des propriétaires ?<br />
		Sur la page d’accueil du jeu > Plus de Statistiques > Un tableau indique le nom du personnage responsable de chaque Village contrôlé. Il faut le contacter par MP pour pouvoir entrer.
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='J-ai-tue-mon-premier-monstre'><b>J’ai tué mon premier « monstre », que dois-je faire ?</b><br /><br />
		Le monstre a laissé tomber un « objet » à sa mort. Inutile de se déplacer pour le ramasser s’il n’est pas tombé sur votre case. Sélectionner directement en haut à gauche de la vue: objet > ramasser un objet. Vous économisez ainsi un PA de déplacement.<br />
		Les objets ramassés peuvent être revendus (3 PO pour les objets courants type moustache de rat, langue de crapaud, etc.) à L’échoppe > Salle des ventes. (Dans les villes, Bourgs et Villages)
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='C-est-quoi-ces-grosses-tortues-geantes'><b>C’est quoi ces grosses tortues géantes ?</b><br /><br />
		Les tortues servent à transporter les marchandises. Quand on organise une « caravane », on charge sa tortue, on monte dedans et on voyage ainsi d’un comptoir commercial à un autre.<br />
		Attention, la tortue appartient à un caravanier (un autre joueur) et il est interdit de les tuer dans les royaumes (zone de départ – plaine fleurie) sous peine de prison !<br /><br />
		<em>Voir : Les Règles > ONGLET Villages > Au chapitre: Comptoir commercial > Caravansérail > Cliquez sur le lien « détails ».</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='C-est-quoi-ces-monstres-a-carapace-grise'><b>C’est quoi ces gros monstres bizarres avec une grosse carapace grise ?</b><br /><br />
		Les Kardjeks ferreux !<br />
		Il est inutile de les chasser, surtout si des forgerons sont dans les environs, ils ne vont pas aimer! C’est grâce à ces monstres qu’il est possible d’obtenir du fer.<br /><br />
		<em>Voir : Les Règles > ONGLET Savoir-faire > Les talents d’extraction.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='C-est-quoi-ces-gros-rochers'><b>C’est quoi ces gros rochers au milieu du jeu de quille ?</b><br /><br />
		Ce sont les portails. C’est par ces portails que les monstres arrivent sur l’ile.<br />
		Quand le portail est rougeoyant il est actif, les monstres arrivent. En cliquant dessus vous voyez son niveau, les monstres seront de niveau égal ou +1. Et plus il y a de personnages présents plus il y aura de monstres.<br />
		Bon à savoir : Restez trop longtemps sur place > Un portail apparait > Restez encore > Les monstres apparaissent ! <br />
		Pour de longs déplacements dans des zones dangereuses (des missions d’escortes par exemple) à petit niveau, il vaut donc mieux se déplacer régulièrement de façon à éviter de mauvaises rencontres. De même, à petit niveau, stationner dans la vue d’un monstre d’un niveau un peu trop élevé est risqué. Ceux-ci attaquent sans invitation et peuvent parfois se déplacer très vite ou porter plusieurs attaques rapprochées !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Hier-j-etais-a-un-endroit-different'><b>Hier j’étais à un endroit et ce matin il me semble que mon personnage a bougé tout seul ?</b><br /><br />
		Il se peut que vous ayez été « bousculé » par un autre personnage.<br />
		Cette action consiste à échanger sa place avec un personnage situé juste à côté.<br />
		Attention : Bousculer ne fonctionne à tous les coups qu’en zone fleurie.<br /><br />
		<em>Voir Les Règles > ONGLET Actions > Bousculer.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-verifier-mes-actions-passees'><b>Comment vérifier mes actions passées ?</b><br /><br />
		Sous la vue > ONGLET Evènements.<br />
		Vous voyez 3 colonnes : la date, le type d’action (Déplacement, Attaque, Savoir-faire, Gain, Service, Don, etc) et les détails (Ex : Evinrude le Terrible a frappé une Chauve-souris géante qui a survécu).<br />
		En cliquant sur le « Type » d’action, vous ouvrez une page « Historique » qui précise tous les détails de l’action.
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Quelqu-un-m-a-envoye-un-message'><b>Quelqu’un m’a envoyé un message ?</b><br /><br />
		Lorsque vous activer une nouvelle DLA, dans votre vue, sur fond noir, vous voyez apparaitre : Votre personnage « Evinrude le Terrible » entre dans une nouvelle phase de jeu. Il dispose de 12 points d’action.<br />
		Et juste en dessous :<br />
		Vous avez x message(s) non lu(s)!<br />
		En bas à droite de la vue, l’onglet « message » devient alors « nouveau message » (en rouge). Cliquez dessus et la page s’ouvre sur les messages reçus. <br />
		Attention, il peut y avoir des messages : Standard - Commerce - Alerte (écriture jaune au-dessus de la liste des messages)
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='J-ai-recu-un-message-d-un-nain-pas-content'><b>J’ai reçu un message d’un Nain pas content du tout ?</b><br /><br />
		Vous venez sans doute d’attaquer, voire de tuer une proie que le Nain en question était occupé à chasser ? Ou vous avez ramassé l’objet provenant de sa chasse?...<br />
		D’une façon générale, le « Vol de Kill » ou d’objet au sol est assez mal vu et pourrait vous valoir de violentes représailles.<br />
		Rien ne vous interdit cependant de choisir cette vie de voleur pour votre personnage. Mais à petit niveau, c’est risqué et il y a intérêt à savoir courir vite !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Je-suis-perdu'><b>Je suis perdu ? Y a pas un plan ?</b><br /><br />
		Vous disposez d’une carte de l’ile. Au-dessus de votre vue à droite : ONGLET carte.<br />
		Sous la carte vous pouvez décider d’afficher les Villages, les routes, le niveau des monstres, les royaumes, les autres membres de votre ordre (si vous appartenez à un ordre ?), les villages contrôlés.
		Les Villes principales ainsi que plusieurs Villages ont des Noms. Passez la souris sur les points pour les faires apparaitre.<br />
		Les Villages contrôlés (entourés d’un rond) appartiennent à des joueurs (souvent un ordre).<br />
		Le niveau des monstres : Vert très pale >  Monstres niv 1 et 2.<br />
		Ensuite, le niveau des monstre augmente > Vert > Vert foncé > Jaune > Orange clair > Etc.<br />
		En déplaçant la souris sur la carte, vous pouvez lire en bas à gauche de celle-ci les coordonnées (x/y).<br />
		Si vous affichez les royaumes (zones hachurées), ces zones correspondent aux territoires « débutants » (zones de départ). Quand votre personnage se déplace dans le jeu, ces zones sont visibles au sol : Plaine fleuries pour les royaumes, plaine (verte) dès que vous en sortez.
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Je-me-retrouve-dans-le-temple'><b>Je me retrouve à nouveau au Temple et je n’ai rien fait ?</b><br /><br />
		Vous êtes mort !<br />
		Bonne nouvelle, ici c’est temporaire.<br />
		Attention, quand vous ressuscitez dans un temple, vous êtes dans la chapelle, seul lieu du jeu ou un personnage est intouchable. En contre coup, vous subissez un malus de 40% sur toutes vos caractéristiques durant 3 DLA.<br />
		Le temple de votre résurrection est votre Temple d’arrivée dans le jeu. Vous pouvez choisir un autre Temple pour une éventuelle résurrection. Temple > Hopital du temple > Choisir le temple comme lieu de prochaine résurrection (20 POs).<br /><br />
		<em>Voir les Règles > ONGLET Villages</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='J-ai-recu-un-message-de-la-garde'><b>J’ai reçu un message de la garde qui me dit que je vais aller en prison ?</b><br /><br />
		En zone de départ (Royaumes sur la carte -  plaine fleurie IG), attaquer un autre personnage ou une tortue est interdit !<br />
		Si vous attaquez, vous recevrez un message de la garde et des heures de sursis. Si vous continuez et allez jusqu’au meurtre, vous serez conduit en prison par la garde ! Ne résistez pas, les gardes sont de niv 60 !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Qu'arrive-t-il-a-mon-equipement-si-mort'><b>Qu’arrive-t-il à mon équipement si je meurs ?</b><br /><br />
		A la mort, vous ne perdez aucun équipement.<br />
		Toutefois, vous perdez 50% de vos PXs.<br />
		Si vous êtes tué par un PJ (Un personnage appartenant à un joueur) dans un Royaume (Où le meurtre est interdit) La Garde sauvera vos POs et les placera sur votre compte en banque.<br />
		Si vous êtes tué par un monstre dans un Royaume (ou ailleurs) ou par un PJ hors royaume, 90% de votre or tombera au sol !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Mon-arme-est-cassee'><b>Mon arme est cassée ?</b><br /><br />
		Ne la jetez pas ! Vous pouvez la faire réparer ! Toutefois il vaut mieux éviter d’en arriver là.<br />
		Tous les équipements s’usent ! Vérifiez régulièrement l’état (Sous la vue ONGLET Equipement) et passez dans un échoppe > salle des réparations.<br />
		Plus le niveau de l’arme (outil, équipement) est élevé, plus la réparation coute cher !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Ou-acheter-de-l-equipement'><b>Où acheter de l’équipement ?</b><br /><br />
		Dans les Villes, Bourgs, Villages, à l’échoppe.<br />
		ONGLET Armurerie standard   > Toutes les armes, armures, outils, potions de niv 1.<br />
		ONGLET Boutique  > Certaines (chaque lieu à quelque spécialités) armes, armures, outils, potions et matières premières dont le niveau est maximum le niveau de l’échoppe.<br /><br />
		<em>Voir les Règles > ONGLET Villages > Echoppes</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Je-pars-en-vacances'><b>Je pars en vacances et je ne pourrais pas jouer mon personnage, que faire ?</b><br /><br />
		Le sitting est « Toléré » pour de courtes périodes.<br />
		<em>Voir Page d’accueil du Jeu > Onglet Inscription > Lien : Charte</em><br />
		<em>Ou sur le Forum > Accueil > Les Règles > Charte</em><br />
		Le plus simple est de placer votre personnage « dans la chapelle » d’un Temple. Là et seulement là, rien ne peut lui arriver ! Attention au Malus sur vos caractéristiques en sortant de la chapelle > Moins 40% sur vos caractéristiques durant 3 DLAs.
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Puis-je-porter-n-importe-quel-equipement'><b>Puis-je porter n’importe quel équipement ?</b><br /><br />
		Non !<br />
		Sous votre vue > ONGLET Equipements<br />
		Votre limite est inscrite en bas de la silhouette: Charge : X/X<br />
		Le premier X est l’équipement porté. Chaque pièce vaut son niveau au carré. Exemple une épée longue niv 1 > 1² = 1 + un bouclier niv 2 > 2² =  4 >> TOTAL = 5.<br />
		Le second X est votre charge maximum. C’est votre niveau X 5. Exemple 10 pour un niv 2, 15 pour un niv 3, etc.<br />
		En haut à droite de la silhouette : Plate X/X et Lin X/X.<br />
		Cela concerne l’armure (plate) et les équipements magiques (Lin).<br />
		Le premier X est le niveau de l’équipement. Exemple un heaume de plate niv 2 = 2 + une cuirasse de plate niv 1 = 1 >> TOTAL = 3.<br />
		Le deuxième X est votre charge maximum à savoir vos D6 de Force ou de Magie. (Voir vos caractéristiques de bases > Sous votre vue > ONGLET profil.<br />
		Dans tous les cas, le 1er chiffre ne doit pas dépasser le 2ème.<br /><br />
		<em>Voir Les Règles > ONGLET Equipements > Charge et surcharge.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Plus-de-dix-places-d-equipement'><b>Y a-t-il un moyen pour transporter plus de choses que les 10 places de mon sac d’équipement?</b><br /><br />
		Dans les Echoppes > Armurerie standard ou Boutique. En plus de votre sac d’équipement, vous pouvez acquérir divers autres sacs (Matière premières, gemmes, carquois, ….)<br />
		Il faut « équiper » son nouveau sac. <br />
		Quatre emplacements sont prévus. Voir sous votre vue ONGLET Equipement  > les 4 points gris en haut à gauche de la silhouette.<br />
		Les sacs ne comptent pas dans la « charge » quel que soit leur niveau.<br />
		Attention, on ne peut équiper que des sacs différents.<br />
		Attention, si vous déséquipez un sac plein, vous n’avez plus accès à son contenu et si vous le donner, vous donnez aussi tout ce qu’il contient !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Que-faire-de-mes-PO'><b>Que faire de toutes mes pièces d’or (POs)?</b><br /><br />
		Il y a des banques sur Nacridan dans chaque Comptoir commercial. Vous pouvez y déposer vos économies moyennant un prélèvement de la banque.<br />
		Dans chaque Ville, Bourg, Village > Comptoir Commercial > Banque.<br />
		Au-dessus de votre vue > ONGLET Economie > Vous pouvez voir votre solde.<br />
		Protéger ses économies peut être utile :<br />
		Si vous êtes assassiné par un autre en zone fleurie, vos PO seront sauvés par les gardes et placés en banque pour vous. Par contre si vous mourrez sous les coups d’un monstre ou hors zone fleurie, vos économies tombent au sol ! Mieux vaut donc ne pas se balader avec une fortune !<br /><br />
		<em>Voir Les Règles > ONGLET Villages > Comptoir commercial.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-faire-du-commerce'><b>Comment faire du commerce ? Vendre ou acheter entre joueurs ?</b><br /><br />
		In Game, si vous êtes à côté d’un autre personnage, vous pouvez donner un objet (1PA) ou des POs (1PA). En haut à gauche de votre vue > Soit Actions pour des POs, Soit Objets pour un objet.<br />
		Dans un comptoir commercial (pour 5 POs et 0 PA) > Envois d’objets. Vous pouvez sélectionner des objets dans votre inventaire, inscrire le nom d’un destinataire, décider d’un prix, indiquer le comptoir commercial d’arrivée.<br />
		Seul le destinataire indiqué pourra retirer les objets moyennant payement du prix indiqué. L’argent est versé sur votre compte.<br />
		Au-dessus de votre vue > ONGLET commerce. <br />
		Vous pouvez voir si des joueurs à proximité ont des objets en vente dans leur « liste de vente ». Vous pouvez donc aussi en proposez à la vente en créant votre liste. Si vous achetez ou qu’un autre joueur achète, l’argent va directement dans la poche du vendeur. Ce système coute 0 PO et 0 PA.<br />
		Attention, si vous mettez en vente un objet à l’attention d’un ami, tout autre joueur à proximité peut également acheter l’objet!<br />
		Dans les Auberges > Salle des Annonces. Vous pouvez consulter toutes les listes de ventes des autres joueurs.<br /><br />
		<em>Voir Les Règles > ONGLET Village > Auberge</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-obtenir-une-mission-d-escorte'><b>Comment obtenir une mission d’escorte ?</b><br /><br />
		Dans les auberges (Villes, Bourgs et Villages) > Salle commune  > Discuter avec les clients. Et vous obtiendrez (peut-être) un parchemin.<br /><br />
		<em>Voir les Règles > ONGLET Village > Auberge > Missions d’escortes > Lien « Détails »</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-effectuer-une-mission-d-escorte'><b>Comment effectuer ma mission d’escorte ?</b><br /><br />
		En étant dans l’auberge ou vous avez obtenu le parchemin. Sélectionnez en haut à gauche : objet > utiliser un parchemin.<br />
		En haut à droite, dans la case ou votre nom est indiqué, sélectionnez le personnage à escorter pour activer sa DLA.<br />
		Attention : Utiliser le PNJ à escorter pour combattre (par exemple) peu vous faire échouer dans votre mission ! Trop de missions ratée > Réputation de marchand en baisse. Et si cette réputation est trop basse  > Impossible d’obtenir de nouveaux parchemins !<br /><br />
		<em>Voir les Règles > ONGLET Village > Auberge > Missions d’escortes > Lien « Détails »</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Comment-organiser-une-caravane'><b>Comment organiser une caravane ?</b><br /><br />
		Dans un comptoir commercial (Villes, Bourgs et Villages) > Caravansérail  > Il faut choisir une destination et des marchandises.<br />
		Vous recevez alors un parchemin de mission qui vous indique le temps impartit. Pour monter dans votre tortue > ONGLET écurie.<br />
		Attention, consultez les règles pour une description plus complète !<br /><br />
		<em>Voir les Règles > ONGLET Villages > Comptoir commercial > Caravansérail > Cliquez sur le lien « Détails ».<br />
		Voir les Règles > ONGLET Tutoriels > Réaliser une mission commerciale.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Je-veux-personnaliser-mon-profil-IG'><b>Je voudrais personnaliser mon profil in-game ?</b><br /><br />
		ONGLET options sous votre vue.<br />
		Attention à la dernière option « Supprimer ce personnage », c’est irréversible !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Je-veux-m-associer-a-d-autres-joueurs'><b>Je voudrais m’associer avec d’autres joueurs, comment faire ?</b><br /><br />
		Sans entrer dans un « ordre » (une Guilde), on peut créer avec d’autres un Groupe de Chasse (GdC).
		Au-dessus de votre vue ONGLET Diplomatie > en haut à gauche : Groupe de chasse > Inviter : Il suffit d’inscrire le nom du joueur. <br />
		Celui-ci recevra un message (message d’alerte) et pourra sélectionner dans l’onglet Diplomatie :
		Groupe de chasse > Vos invitation > sélectionner pour accepter l’invitation et valider.<br />
		Une fois dans un GdC, grâce à l’onglet Diplomatie vous pouvez voir les membres du groupe, leur état de santé, leur position et la distance qui vous sépare.<br />
		Grace à l’ONGLET Tribune (en bas à gauche – A côté de FORUM) Vous pouvez communiquer avec les autres membres sans devoir envoyer de MP.<br />
		En GdC, les PX liés à la mort d’un adversaire sont partagés !<br /><br />
		<em>Voir Les Règles > ONGLET Actions > Le mode Diplomatie – Les Groupes de chasse.</em>
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Quand-je-clique-sur-concentration-rien'><b>Quand je clique sur « Concentration », rien ne se passe ?</b><br /><br />
		En haut à gauche de votre vue : Action > Concentration.<br />
		Si concentration est inscrit en italique, il est inutilisable.<br />
		Cette action devient accessible lorsqu’on vient de rater une compétence, un sortilège ou un artisanat.
		On peut alors tenter « concentration » pour 2PAs et ainsi espérer gagner 1% de maitrise.<br />
		Exemple : Je viens de rater la compétence « Dégâts Accrus » > 9PAs avec mon arme lourde. Il me reste 3 PAs > Je peux tenter de gagner 1% de maitrise supplémentaire grâce à « concentration ». J’utilise ainsi 2 de mes PAs restant efficacement !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Puis-je-faire-deccendre-une-jauge'><b>Puis-je faire descendre une de mes jauges d’évolution ?</b><br /><br />
		Oui, mais pas indéfiniment !<br />
		Pour éviter les éventuelles « erreurs de débutant » ou « optimiser » particulièrement un profil. Il est possible de remettre une jauge à zéro.<br />
		Dans une Guilde des Artisans > Salle des enchantements > Rune d’équilibre.<br />
		Cette action coute 30 POs et 10 PAs.<br />
		Attention, c’est possible uniquement jusqu’au niveau 5 !
		</div>
		</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgbody'><br />
		<div id='Quels-dangers-auxquels-il-faut-etre-préparé'><b>Quels sont les dangers auxquels il faut être préparé ?</b><br /><br />
		Nacridan est un jeu PvE/PvP, il est donc toujours possible de mourir sous les coups d’un autre aventurier plutôt que sous les coups d’un monstre, de subir une embuscade, de se faire attaquer sa caravane, etc...<br /><br />
		Certaines compétences sont destinées à créer des profils de « Voleurs ». Il faut donc savoir qu’on peut se faire voler sa bourse, se faire désarmer, subir un « larcin » (perte d’un objet de son inventaire suite à une mort).<br /><br />
		Même si ces compétences sont punies de prison en Royaume (zone fleurie), il faut rester vigilant, penser à consulter les profils des personnages dans votre vue pour voir à qui vous avez à faire, vérifier les différentes pièces d’un bâtiment avant de s’y arrêter, éviter de garder beaucoup d’or (placez le à la banque), etc… Il faut aussi relativiser en cas de coup dur et bien se souvenir que c'est avant tout un jeu!<br /><br />
		Pour comprendre les possibilités des voleurs voir : <em>Les règles > Onglet « Compétences » > Les Compétences de l'école de l’Ombre.<em/><br />
        Pour bien comprendre dans quel esprit il est bon de s'amuser voir sur le Forum: <em>Accueil > Les Règles > La Charte (point 14)<em/>
        </div>
		</td>";
        $str .= "</tr>";
        $str .= "</table>";
        
        return $str;
    }
}

?>
