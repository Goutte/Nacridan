<?php
DEFINE("MAXCONTACT", 40);

class CQContact extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQContact($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $id = $curplayer->get("id");
        $err = "";
        
        if (isset($_POST["Del"]) && is_numeric($_POST["Del"])) {
            $err = $this->deleteContact($db);
        }
        
        if (isset($_GET["add"]) && is_numeric($_GET["add"])) {
            $err = $this->addContact($id, $_GET["add"], $db);
        }
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=search' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr><td class='mainbgtitle tabmenu'>" . "<label>Rechercher :  <input type='text' name='query' size='20px'/> " . "</label>
	<input id='Search' type='submit' name='Search' value='" . localize("Recherche") . "' />
	<span class='mainbgtitle' style='font-size: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp; Attention la recherche ne s'effectue pas dans les alertes.</span></td></tr>";
        $str .= "</table>";
        $str .= "</form>\n";
        
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=contact' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize('M E S S A G E R I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'>";
        $str .= "<a href='../conquest/conquest.php?center=compose' class='tabmenu'>" . localize('Composer un Message') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Message(s) Reçu(s)') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=2' class='tabmenu'>" . localize('Message(s) Envoyé(s)') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=contact' class='tabmenu'>" . localize('Contacts') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=alias' class='tabmenu'>" . localize('Alias') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=archive' class='tabmenu'>" . localize('Archive(s)') . " </a>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td width='30px'><a href='#' class='all' onclick=\"invertCheckboxes('form'); return false;\"><img src='../pics/misc/all.gif' style='border: 0' /></a></td>\n";
        $str .= "<td class='mainbglabel'>" . localize("Nom") . "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable centerareawidth'>\n";
        
        $dbc = new DBCollection("SELECT  MailContact.id,id_Player\$friend,name FROM MailContact,Player WHERE Player.id=id_Player\$friend AND id_Player =" . $id, $db);
        
        $data = array();
        while (! $dbc->eof()) {
            $player = "<a href=\"../conquest/profile.php?id=" . $dbc->get("id_Player\$friend") . "\" class='stylepc popupify'>" . $dbc->get("name") . "</a>";
            $data[] = array(
                "id" => $dbc->get("id"),
                "name" => $player
            );
            $dbc->next();
        }
        
        $cpt = 0;
        
        foreach ($data as $arr) {
            $str .= sprintf("<tr><td class='mainbgtitle' width='30px'><input name='check[]' type='checkbox' value='%s'/>", $arr["id"]);
            $str .= "</td>";
            $str .= sprintf("<td class='mainbgtitle'  align='left'>%s</td></tr>\n", $arr["name"]);
            $cpt ++;
        }
        $str .= "</table>";
        if ($cpt) {
            $str .= "<input id='Del' type='submit' name='Del' value='" . localize("Effacer") . "' />";
        }
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</form>\n";
        
        return $str;
    }

    public function addContact($id, $idfriend, $db)
    {
        if ($this->curplayer->get("id_Mission") != 0) {
            return localize("ERREUR !!! Ce joueur ne peut pas avoir de contact !!! ");
        }
        
        $dbc = new DBCollection("SELECT status FROM Player WHERE id = " . $idfriend, $db);
        if ($dbc->count() == 0) {
            return localize("ERREUR !!! Ce joueur n'existe pas !!! ");
        }
        if ($dbc->get("status") == "NPC") {
            return localize("ERREUR !!! Il n'est pas possible d'ajouter un NPC en contact!!! ");
        }
        $dbc = new DBCollection("SELECT COUNT(id) AS cpt FROM MailContact WHERE id_Player=" . $id, $db);
        if ($dbc->get("cpt") >= MAXCONTACT) {
            return localize("Votre liste de " . MAXCONTACT . " Contacts est pleine.");
        }
        
        $dba = new DBCollection("SELECT * FROM MailContact WHERE id_Player=" . $id . " AND id_Player\$friend=" . $idfriend, $db);
        $founded = $dba->count();
        if ($founded) {
            return localize("Ce contact est déjà enregistré");
        } else {
            $dbc = new DBCollection("INSERT INTO MailContact (id_Player,id_Player\$friend) VALUES (\"" . $id . "\",\"" . $idfriend . "\");", $db, 0, 0, false);
            if ($dbc->errorNoDB() == 0) {
                return localize("Contact ajouté");
            }
        }
    }

    public function deleteContact($db)
    {
        $msgToDelete = "";
        if (isset($_POST["check"]))
            $msgToDelete = getSQLCondFromArray(quote_smart($_POST["check"]), "id", "OR");
        
        if ($msgToDelete != "") {
            $dbm = new DBCollection("DELETE FROM MailContact WHERE " . $msgToDelete, $db, 0, 0, false);
            return localize("Contact(s) effacé(s)");
        } else {
            return localize("Aucun Contact effacé");
        }
    }
}
?>
