<?php

/**
 *Gère l'ensemble du menu en mode Conquête
 *
 * Redirige sur les différentes parties du menu traitées dans les autres fichiers selon le choix du joueur.
 *
 *
 *@author Nacridan
 *@version 1.0
 *@package NacridanV1
 *@subpackage Conquest
 */

/*
 * if($_SERVER['REMOTE_ADDR']!="86.66.160.171")
 * {
 *
 * echo "Maintenance veuillez patienter";
 * exit(-1);
 * }
 */
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/include/game.inc.php");

ini_set('display_errors', 1);
error_reporting(E_ALL);

function activeATB($curplayer)
{
    global $sess;
    $key = $curplayer->get("id");
    if ($sess->has("DLA" . $key)) {
        if ($sess->get("DLA" . $key) == "on") {
            $format = '%Y-%m-%d %H:%M:%S';
            $strf = strftime($format);
            $var = "DLA" . $key;
            $sess->set($var, "off");
            if ((gmstrtotime($curplayer->get("nextatb")) - date("I") * 3600) < time()) {
                $action = new ActionEvt();
                $action->setType(NEWATB);
                return $action;
            }
        }
    }
    return null;
}

$centerobj = null;
$extra = "";
$actionEvt = null;
$minimap = null; // to avoid to load multiple time vue 2D
$radius = SIZEAREA; // to save view2D size
$isMinimapNeeded = true; // boolean to know if needed to load view even if parameter vien2D is set. Sample: in a building

if ($curplayer->get("disabled") == 0) {
    
    if (($actionEvt = activeATB($curplayer)) != null) {
        unset($center);
        $action = "act";
        $nacridan->setRepostForm(false);
        $previousIdForm = "ATB";
        $sendIdForm = "ATB";
    } else {
        if ($curplayer->get("resurrect") == 1) {
            $actionEvt = new ActionEvt();
            $actionEvt->setType(NEWATB);
            unset($center);
            $action = "act";
            $nacridan->setRepostForm(false);
            $previousIdForm = "ATB";
            $sendIdForm = "ATB";
        }
        
        if (isset($_POST["action"])) {
            $actionEvt = new ActionEvt();
            $actionEvt->setAction($_POST);
        }
    }
    
    $move2Daction = "";
    
    if (isset($action)) {
        
        if (! $nacridan->isRepostForm()) {
            require_once (HOMEPATH . '/conquest/cqaction.inc.php');
            
            if (($actionEvt->getType() == ATTACK) or ($actionEvt->getType() == ARCHERY) or ($actionEvt->getType() == ATTACK_BUILDING) or ($actionEvt->getType() == CARAVAN_TERMINATE) or
                 ($actionEvt->getType() == CONCENTRATION) or ($actionEvt->getType() == ANSWER_TO_POLL) or ($actionEvt->getType() > 100 and $actionEvt->getType() < 400) or
                 ($actionEvt->getType() > 500))
                $centerobj = new CQAction($nacridan, $actionEvt, $db);
            
            else {
                if ($actionEvt->getType() == MOVE2D)
                    $move2Daction = new CQAction($nacridan, $actionEvt, $db, "bottomareawidth");
                else
                    $btobj = new CQAction($nacridan, $actionEvt, $db, "bottomareawidth");
                
                if (! isset($_GET["center"]))
                    $center = "view";
            }
        } else {
            $btobj = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            if (! isset($_GET["center"]))
                $center = "view";
        }
    }
    
    if (is_null($centerobj)) {
        if (isset($center)) {
            switch ($center) {
                case "map":
                    require_once (HOMEPATH . '/conquest/cqmap.inc.php');
                    $centerobj = new CQMap($nacridan, $MAIN_HEAD, $MAIN_BODY, $db);
                    break;
                case "view1d":
                    
                    require_once (HOMEPATH . '/conquest/cqview1D.inc.php');
                    $centerobj = new CQView($nacridan, $db);
                    
                    break;
                case "view2d":
                    if ((isset($_POST["room"]) && ($_POST["room"] != TOWER_ROOM && $_POST["room"] != PALACE_BELL_TOWER_ROOM && $_POST["room"] != TEMPLE_ROOF_ROOM) && ($curplayer->get(
                        "inbuilding") != 0)) || (($curplayer->get("inbuilding") != 0) &&
                         ($curplayer->get("room") != TOWER_ROOM && $curplayer->get("room") != PALACE_BELL_TOWER_ROOM && $curplayer->get("room") != TEMPLE_ROOF_ROOM) &&
                         ! isset($_POST["room"]))) {
                        require_once (HOMEPATH . '/conquest/cqinsidebuilding.inc.php');
                        $centerobj = new CQInsideBuilding($nacridan, $MAIN_BODY, $db);
                        $minimap = $centerobj->minimap;
                        $radius = $centerobj->radius;
                        $isMinimapNeeded = false;
                    } else {
                        if (isset($_POST["room"])) {
                            if ($_POST["room"] != $curplayer->get("room")) {
                                $curplayer->set("room", $_POST["room"]);
                                $curplayer->updateDBPartial("room", $db);
                            }
                        }
                        require_once (HOMEPATH . '/conquest/cqview2D.inc.php');
                        $centerview = new CQView2D($nacridan, $MAIN_BODY, $db);
                        $minimap = $centerview->minimap;
                        $radius = $centerview->radius;
                    }
                    break;
                case "view":
                    if ((isset($_POST["room"]) and ($_POST["room"] != TOWER_ROOM && $_POST["room"] != TEMPLE_ROOF_ROOM && $_POST["room"] != PALACE_BELL_TOWER_ROOM)) or (($curplayer->get(
                        "inbuilding") != 0) and
                         ($curplayer->get("room") != TOWER_ROOM && $curplayer->get("room") != PALACE_BELL_TOWER_ROOM && $curplayer->get("room") != TEMPLE_ROOF_ROOM) and
                         ! isset($_POST["room"]))) {
                        
                        require_once (HOMEPATH . '/conquest/cqinsidebuilding.inc.php');
                        $centerobj = new CQInsideBuilding($nacridan, $MAIN_BODY, $db);
                    } else {
                        if (isset($_POST["room"])) {
                            if ($_POST["room"] != $curplayer->get("room")) {
                                $curplayer->set("room", $_POST["room"]);
                                $curplayer->updateDBPartial("room", $db);
                            }
                        }
                        
                        if ($nacridan->auth->auth["view2d"]) {
                            require_once (HOMEPATH . '/conquest/cqview2D.inc.php');
                            $centerview = new CQView2D($nacridan, $MAIN_BODY, $db);
                            $minimap = $centerview->minimap;
                            $radius = $centerview->radius;
                        } else {
                            require_once (HOMEPATH . '/conquest/cqview1D.inc.php');
                            $centerobj = new CQView($nacridan, $db);
                        }
                    }
                    break;
                case "profile":
                    require_once (HOMEPATH . '/conquest/cqprofile.inc.php');
                    $centerobj = new CQProfile($nacridan, $db);
                    break;
                case "equip":
                    require_once (HOMEPATH . '/conquest/cqequip.inc.php');
                    $extra = " style='overflow: hidden;'";
                    $centerobj = new CQEquip($nacridan, $MAIN_HEAD, $MAIN_BODY, $db);
                    break;
                case "event":
                    require_once (HOMEPATH . '/conquest/cqevent.inc.php');
                    $centerobj = new CQEvent($nacridan, "centerareawidth", $db);
                    break;
                case "mail":
                    require_once (HOMEPATH . '/conquest/cqmail.inc.php');
                    $centerobj = new CQMail($nacridan, $db);
                    break;
                case "compose":
                    require_once (HOMEPATH . '/conquest/cqcompose.inc.php');
                    $centerobj = new CQCompose($nacridan, $db);
                    break;
                case "contact":
                    require_once (HOMEPATH . '/conquest/cqcontact.inc.php');
                    $centerobj = new CQContact($nacridan, $db);
                    break;
                case "alias":
                    require_once (HOMEPATH . '/conquest/cqalias.inc.php');
                    $centerobj = new CQAlias($nacridan, $db);
                    break;
                case "search":
                    require_once (HOMEPATH . '/conquest/cqsearchmail.inc.php');
                    $centerobj = new CQSearchMail($nacridan, $db);
                    break;
                case "archive":
                    require_once (HOMEPATH . '/conquest/cqarchive.inc.php');
                    $centerobj = new CQArchive($nacridan, $db);
                    break;
                case "first":
                    require_once (HOMEPATH . '/conquest/cqfirst.inc.php');
                    $centerobj = new CQFirst($nacridan);
                    break;
                case "option":
                    require_once (HOMEPATH . '/conquest/cqoption.inc.php');
                    $centerobj = new CQOption($nacridan, $db);
                    break;
                case "account":
                    require_once (HOMEPATH . '/conquest/cqaccount.inc.php');
                    $centerobj = new CQAccount($nacridan, $db);
                    break;
                case "chanirc":
                    require_once (HOMEPATH . '/conquest/cqchanirc.inc.php');
                    $centerobj = new CQChanIRC($nacridan, $db);
                    break;
                case "faq":
                    require_once (HOMEPATH . '/conquest/cqfaq.inc.php');
                    $centerobj = new CQFAQ($nacridan, $db);
                    break;
                
                case "disabled":
                    if ($curplayer->get("authlevel") > 5) {
                        require_once (HOMEPATH . '/conquest/cqdisabled.inc.php');
                        $centerobj = new CQDisabled($nacridan, "centerareawidth", $db);
                    }
                    break;
                case "questpanel":
                    if ($curplayer->get("authlevel") > 1) {
                        require_once (HOMEPATH . '/conquest/cqquestpanel.inc.php');
                        $centerobj = new CQQuestpanel($nacridan, "centerareawidth", $db);
                    }
                    break;
                case "xprewardpanel":
                    if ($curplayer->get("authlevel") > 5) {
                        require_once (HOMEPATH . '/conquest/cqxprewardpanel.inc.php');
                        $centerobj = new CQXPRewardPanel($nacridan, "centerareawidth", $db);
                    }
                    break;    
                case "newspanel":
                    if ($curplayer->get("authlevel") > 1) {
                        require_once (HOMEPATH . '/conquest/cqnews.inc.php');
                        $centerobj = new CQNews($nacridan, "centerareawidth", $db);
                    }
                    break;
                case "createParchment":
                    if ($curplayer->get("authlevel") > 1) {
		                require_once (HOMEPATH . '/conquest/actiontest/cqcreateParchment.inc.php');
		                $centerobj = new CQCreateParchment($nacridan, $db);
		              }
		              break;    
                case "pollpanel":
                    if ($curplayer->get("authlevel") > 1) {
                        require_once (HOMEPATH . '/conquest/cqpoll.inc.php');
                        $centerobj = new CQPoll($nacridan, "centerareawidth", $db);
                    }
                    break;
                case "serverpanel":
                    if ($curplayer->get("authlevel") > 1) {
                        require_once (HOMEPATH . '/conquest/cqserver.inc.php');
                        $centerobj = new CQServer($nacridan, "centerareawidth", $db);
                    }
                    break;
                
                case "globalMP":
                    if ($curplayer->get("authlevel") > 1) {
                        require_once (HOMEPATH . '/conquest/cqglobalMP.inc.php');
                        $centerobj = new CQGlobalMP($nacridan, "centerareawidth", $db);
                    }
                    break;
                
                case "addDescription":
                    if ($curplayer->get("authlevel") > 1) {
                        require_once (HOMEPATH . '/conquest/cqformbasicmission.inc.php');
                        $centerobj = new CQFormBasicMission($nacridan, $db);
                    }
                    break;
                
                case "monsterProfile":
                    require_once (HOMEPATH . '/conquest/cqmonsterprofile.inc.php');
                    $centerobj = new CQMonsterProfile($nacridan, $db);
                    break;
                
                default:
                    break;
            }
            
            // CITY
            if ($curplayer->get("incity") == 1) {
                switch ($center) {
                    
                    case "arms":
                        require_once (HOMEPATH . '/conquest/cqarms.inc.php');
                        $centerobj = new CQArms($nacridan, $db);
                        break;
                    case "sale":
                        require_once (HOMEPATH . '/conquest/cqsale.inc.php');
                        $centerobj = new CQSale($nacridan, $db);
                        break;
                    case "repair":
                        require_once (HOMEPATH . '/conquest/cqrepair.inc.php');
                        $centerobj = new CQRepair($nacridan, $db);
                        break;
                    case "factory":
                        require_once (HOMEPATH . '/conquest/cqfactory.inc.php');
                        $centerobj = new CQFactory($nacridan, $db);
                        break;
                    
                    case "warehouse":
                        require_once (HOMEPATH . '/conquest/cqwarehouse.inc.php');
                        $centerobj = new CQWareHouse($nacridan, $db);
                        break;
                    
                    case "enchant":
                        require_once (HOMEPATH . '/conquest/cqenchant.inc.php');
                        $centerobj = new CQEnchant($nacridan, $db);
                        break;
                    case "hostel":
                        require_once (HOMEPATH . '/conquest/cqhostel.inc.php');
                        $centerobj = new CQHostel($nacridan, $db);
                        break;
                    case "bank":
                        require_once (HOMEPATH . '/conquest/cqbank.inc.php');
                        $centerobj = new CQBank($nacridan, $db);
                        break;
                    case "alchemist":
                        require_once (HOMEPATH . '/conquest/cqalchemist.inc.php');
                        $centerobj = new CQAlchemist($nacridan, $db);
                        break;
                    case "magicschool":
                        require_once (HOMEPATH . '/conquest/cqmagicschool.inc.php');
                        $centerobj = new CQMagicSchool($nacridan, $db);
                        break;
                    case "abilityschool":
                        require_once (HOMEPATH . '/conquest/cqabilityschool.inc.php');
                        $centerobj = new CQAbilitySchool($nacridan, $db);
                        break;
                    default:
                        break;
                }
            }
        } else {
            require_once (HOMEPATH . '/conquest/cqinfo.inc.php');
            $centerobj = new CQInfo($nacridan, $db);
        }
    }
} else {
    if (isset($center)) {
        switch ($center) {
            case "mail":
                require_once (HOMEPATH . '/conquest/cqmail.inc.php');
                $centerobj = new CQMail($nacridan, $db);
                break;
            case "compose":
                require_once (HOMEPATH . '/conquest/cqcompose.inc.php');
                $centerobj = new CQCompose($nacridan, $db);
                break;
            case "contact":
                require_once (HOMEPATH . '/conquest/cqcontact.inc.php');
                $centerobj = new CQContact($nacridan, $db);
                break;
            case "alias":
                require_once (HOMEPATH . '/conquest/cqalias.inc.php');
                $centerobj = new CQAlias($nacridan, $db);
                break;
            default:
                require_once (HOMEPATH . '/conquest/cqbanned.inc.php');
                $centerobj = new CQBanned($nacridan, $curplayer->get("id"), $db);
                break;
        }
    } else {
        require_once (HOMEPATH . '/conquest/cqbanned.inc.php');
        $centerobj = new CQBanned($nacridan, $curplayer->get("id"), $db);
    }
}

if (isset($centerview)) {
    $MAIN_CENTER = $MAIN_BODY->addNewHTMLObject("div", "", "class='centerareaview'" . $extra);
    $MAIN_CENTER->add($centerview);
} else {
    $MAIN_CENTER = $MAIN_BODY->addNewHTMLObject("div", "", "class='centerarea'" . $extra);
    $MAIN_CENTER->add($centerobj);
}

require_once (HOMEPATH . '/main/mmenu.inc.php');
require_once (HOMEPATH . '/diplomacy/dtribune.inc.php');
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));
$MAIN_BODY->add(new CQMMenu($nacridan, $db, $lang));

if ($curplayer->get("disabled") == 0) {
    
    if ($curplayer->get("authlevel") < 4) {
        
        require_once (HOMEPATH . '/conquest/cqmenu.inc.php');
        $MAIN_BODY->add(new CQMenu($nacridan, $db));
    } else {
        
        require_once (HOMEPATH . '/conquest/cqmenuauth.inc.php');
        $MAIN_BODY->add(new CQMenuAuth($nacridan, $db));
    }
    require_once (HOMEPATH . '/conquest/cqmenubt.inc.php');
    
    $MAIN_BODY->add(new CQMenuBT());
    
    $MAIN_BOTTOM = $MAIN_BODY->addNewHTMLObject("div", "", "class='bottomarea'");
    $btobj = isset($btobj) ? $btobj : null;
    
    if (($btobj == null) && isset($bottom)) {
        switch ($bottom) {
            case "move":
                require_once (HOMEPATH . '/conquest/cqmove.inc.php');
                $btobj = new CQMove($nacridan, $db);
                break;
            case "concentration":
                require_once (HOMEPATH . '/conquest/cqconcentration.inc.php');
                $btobj = new CQConcentration($nacridan, $db);
                break;
            case "appear":
                require_once (HOMEPATH . '/conquest/cqappear.inc.php');
                $btobj = new CQAppear($nacridan, $db);
                break;
            case "free":
                require_once (HOMEPATH . '/conquest/cqfree.inc.php');
                $btobj = new CQFree($nacridan, $db);
                break;
            /*
             * case "newDLA":
             * require_once(HOMEPATH.'/conquest/cqnewdla.inc.php');
             * $btobj=new CQNewDla($nacridan,$db);
             * break;
             */
            case "attack":
                require_once (HOMEPATH . '/conquest/cqattack.inc.php');
                $btobj = new CQAttack($nacridan, $db);
                break;
            case "blood":
                require_once (HOMEPATH . '/conquest/cqblood.inc.php');
                $btobj = new CQBlood($nacridan, $db);
                break;
            case "attackBuilding":
                require_once (HOMEPATH . '/conquest/cqattackbuilding.inc.php');
                $btobj = new CQAttackBuilding($nacridan, $db);
                break;
            case "graffiti":
                require_once (HOMEPATH . '/conquest/cqgraffiti.inc.php');
                $btobj = new CQGraffiti($nacridan, $db);
                break;
            case "continuegraffiti":
                require_once (HOMEPATH . '/conquest/cqcontinuegraffiti.inc.php');
                $btobj = new CQContinueGraffiti($nacridan, $db);
                break;
            case "consultgraffiti":
                require_once (HOMEPATH . '/conquest/cqconsultgraffiti.inc.php');
                $btobj = new CQConsultGraffiti($nacridan, $db);
                break;
            
            // ATTENTION CE BOUT DE CODE EST PROPRE A LA VERSION TEST
            case "addAP":
                require_once (HOMEPATH . '/conquest/actiontest/cqaddAP.inc.php');
                $btobj = new CQAddAP($nacridan, $db);
                break;
            case "addMoney":
                require_once (HOMEPATH . '/conquest/actiontest/cqaddMoney.inc.php');
                $btobj = new CQAddMoney($nacridan, $db);
                break;
            case "levelup":
                require_once (HOMEPATH . '/conquest/actiontest/cqlevelup.inc.php');
                $btobj = new CQLevelUp($nacridan, $db);
                break;
            case "newDLA":
                require_once (HOMEPATH . '/conquest/actiontest/cqnewdla.inc.php');
                $btobj = new CQNewDLA($nacridan, $db);
                break;
            case "renameObject":
                require_once (HOMEPATH . '/conquest/actiontest/cqrenameObject.inc.php');
                $btobj = new CQRenameObject($nacridan, $db);
                break;                
            case "createExploitation":
                require_once (HOMEPATH . '/conquest/actiontest/cqcreateExploitation.inc.php');
                $btobj = new CQCreateExploitation($nacridan, $db);
                break;
            case "changeModifier":
                require_once (HOMEPATH . '/conquest/actiontest/cqchangeModifier.inc.php');
                $btobj = new CQChangeModifier($nacridan, $db);
                break;
            case "deleteChapelMalus":
                require_once (HOMEPATH . '/conquest/actiontest/cqdeleteChapelMalus.php');
                $btobj = new CQDeleteChapelMalus($nacridan, $db);
                break;
            case "createMonster":
                require_once (HOMEPATH . '/conquest/actiontest/cqcreateMonster.inc.php');
                $btobj = new CQCreateMonster($nacridan, $db);
                break;
            case "cloneMonster":
                require_once (HOMEPATH . '/conquest/actiontest/cqcloneMonster.inc.php');
                $btobj = new CQCloneMonster($nacridan, $db);
                break;
            case "playIAMonster":
                require_once (HOMEPATH . '/conquest/actiontest/cqplayIAMonster.inc.php');
                $btobj = new CQPlayIAMonster($nacridan, $db);
                break;
            case "calcSPath":
                require_once (HOMEPATH . '/conquest/actiontest/cqCalcShortWay.inc.php');
                $btobj = new CQCalcShortWay($nacridan, $db);
                break;
            case "createBuilding":
                require_once (HOMEPATH . '/conquest/actiontest/cqcreateBuilding.inc.php');
                $btobj = new CQCreateBuilding($nacridan, $db);
                break;
            case "renameBuilding":
                require_once (HOMEPATH . '/conquest/actiontest/cqrenameBuilding.inc.php');
                $btobj = new CQRenameBuilding($nacridan, $db);
                break;
            case "destroyBuilding":
                require_once (HOMEPATH . '/conquest/actiontest/cqdestroyBuilding.inc.php');
                $btobj = new CQDestroyBuilding($nacridan, $db);
                break;
            case "destroyObject":
                require_once (HOMEPATH . '/conquest/actiontest/cqdestroyObject.inc.php');
                $btobj = new CQDestroyObject($nacridan, $db);
                break;
            case "terraformCase":
                require_once (HOMEPATH . '/conquest/actiontest/cqTerraformation.inc.php');
                $btobj = new CQTerraformation($nacridan, $db);
                break;
            case "addAbilityToPNJ":
                require_once (HOMEPATH . '/conquest/actiontest/cqaddAbilityToPNJ.inc.php');
                $btobj = new CQAddAbilityToPNJ($nacridan, $db);
                break;
            case "addMagicSpellToPNJ":
                require_once (HOMEPATH . '/conquest/actiontest/cqaddMagicSpellToPNJ.inc.php');
                $btobj = new CQAddMagicSpellToPNJ($nacridan, $db);
                break;
            case "addTalentToPNJ":
                require_once (HOMEPATH . '/conquest/actiontest/cqaddTalentToPNJ.inc.php');
                $btobj = new CQAddTalentToPNJ($nacridan, $db);
                break;
            case "giveMoneyToPNJ":
                require_once (HOMEPATH . '/conquest/actiontest/cqgiveMoneyToPNJ.inc.php');
                $btobj = new CQGiveMoneyToPNJ($nacridan, $db);
                break;
            case "giveItemToPNJ":
                require_once (HOMEPATH . '/conquest/actiontest/cqgiveItemToPNJ.inc.php');
                $btobj = new CQGiveItemToPNJ($nacridan, $db);
                break;
            case "emptyBag":
                require_once (HOMEPATH . '/conquest/actiontest/cqemptybag.inc.php');
                $btobj = new CQEmptyBag($nacridan, $db);
                break;
            case "accessBDD":
                require_once (HOMEPATH . '/conquest/actiontest/cqaccessbdd.inc.php');
                $btobj = new CQAccessBDD($nacridan, $db);
                break;
            case "instantTP":
                require_once (HOMEPATH . '/conquest/actiontest/cqinstantTP.inc.php');
                $btobj = new CQInstantTP($nacridan, $db);
                break;
            case "openOrCloseDoors":
                require_once (HOMEPATH . '/conquest/actiontest/cqopenOrCloseDoors.inc.php');
                $btobj = new CQOpenOrCloseDoors($nacridan, $db);
                break;
            case "runAI":
                require_once (HOMEPATH . '/conquest/actiontest/cqrunAI.inc.php');
                $btobj = new CQRunAI($nacridan, $db);
                break;
            
            // FIN DU CODE DE LA VERSION TEST
            case "levelUp":
                require_once (HOMEPATH . '/conquest/cqlevelup.inc.php');
                $btobj = new CQLevelUp($nacridan, $db);
                break;
            case "recall":
                require_once (HOMEPATH . '/conquest/cqrecall.inc.php');
                $btobj = new CQRecall($nacridan, $db);
                break;
            case "arrest":
                require_once (HOMEPATH . '/conquest/cqarrest.inc.php');
                $btobj = new CQArrest($nacridan, $db);
                break;
            case "hustle":
                require_once (HOMEPATH . '/conquest/cqhustle.inc.php');
                $btobj = new CQHustle($nacridan, $db);
                break;
            case "leavebuilding":
                require_once (HOMEPATH . '/conquest/cqleavebuilding.inc.php');
                $btobj = new CQLeaveBuilding($nacridan, $db);
                break;
            case "disabledbravery":
                require_once (HOMEPATH . '/conquest/cqbravery.inc.php');
                $btobj = new CQBravery($nacridan, $db);
                break;
            case "disabledprotection":
                require_once (HOMEPATH . '/conquest/cqprotection.inc.php');
                $btobj = new CQProtection($nacridan, $db);
                break;
            case "revokegolem":
                require_once (HOMEPATH . '/conquest/cqrevoke.inc.php');
                $btobj = new CQRevoke($nacridan, $db);
                break;
            case "revokesoul":
                require_once (HOMEPATH . '/conquest/cqsoul.inc.php');
                $btobj = new CQSoul($nacridan, $db);
                break;
            
            case "turtle":
                require_once (HOMEPATH . '/conquest/cqturtle.inc.php');
                $btobj = new CQTurtle($nacridan, $db);
                break;
            case "disabledresistance":
                require_once (HOMEPATH . '/conquest/cqresistance.inc.php');
                $btobj = new CQResistance($nacridan, $db);
                break;
            case "pick":
                require_once (HOMEPATH . '/conquest/cqpick.inc.php');
                $btobj = new CQPick($nacridan, $db);
                break;
            case "drop":
                require_once (HOMEPATH . '/conquest/cqdrop.inc.php');
                $btobj = new CQDrop($nacridan, $db);
                break;
            case "give":
                require_once (HOMEPATH . '/conquest/cqgive.inc.php');
                $btobj = new CQGive($nacridan, $db);
                break;
            case "wear":
                require_once (HOMEPATH . '/conquest/cqwear.inc.php');
                $btobj = new CQWear($nacridan, $db);
                break;
            case "unwear":
                require_once (HOMEPATH . '/conquest/cqunwear.inc.php');
                $btobj = new CQUnWear($nacridan, $db);
                break;
            case "store":
                require_once (HOMEPATH . '/conquest/cqstore.inc.php');
                $btobj = new CQStore($nacridan, $db);
                break;
            case "usepotion":
                require_once (HOMEPATH . '/conquest/cqusepotion.inc.php');
                $btobj = new CQUsePotion($nacridan, $db);
                break;
            case "useparchment":
                require_once (HOMEPATH . '/conquest/cqparchment.inc.php');
                $btobj = new CQParchment($nacridan, $db);
                break;
            case "givemoney":
                require_once (HOMEPATH . '/conquest/cqgivemoney.inc.php');
                $btobj = new CQGiveMoney($nacridan, $db);
                break;
            case "atbshift":
                require_once (HOMEPATH . '/conquest/cqatbshift.inc.php');
                $btobj = new CQATBShift($nacridan, $db);
                break;
            case "leavearena":
                require_once (HOMEPATH . '/conquest/cqleavearena.inc.php');
                $btobj = new CQLeaveArena($nacridan, $db);
                break;
            case "ability":
                require_once (HOMEPATH . '/conquest/cqability.inc.php');
                $btobj = new CQAbility($nacridan, $db);
                break;
            case "spell":
                require_once (HOMEPATH . '/conquest/cqspell.inc.php');
                $btobj = new CQSpell($nacridan, $db);
                break;
            case "talent":
                require_once (HOMEPATH . '/conquest/cqtalent.inc.php');
                $btobj = new CQTalent($nacridan, $db);
                break;
            case "legend":
                require_once (HOMEPATH . '/conquest/cqmaplegend.inc.php');
                $btobj = new CQMapLegend($nacridan, $db);
                break;
            case "teleport":
                require_once (HOMEPATH . '/conquest/inbuilding/cqteleport.inc.php');
                $btobj = new CQTeleport($nacridan, $db);
                break;
            case "rpaction":
                require_once (HOMEPATH . '/conquest/cqrpaction.inc.php');
                $btobj = new CQRPAction($nacridan, $db);
                // require_once(HOMEPATH.'/conquest/cqgivemoney.inc.php');
                // $btobj=new CQGiveMoney($nacridan,$db);
                break;
            case "caravane":
                require_once (HOMEPATH . '/conquest/inbuilding/cqcaravan.inc.php');
                $btobj = new CQCaravan($nacridan, $db);
                break;
            default:
                break;
        }
        
        if ($curplayer->get("inbuilding") == 1) {
            switch ($bottom) {
                
                case "teleport":
                    require_once (HOMEPATH . '/conquest/inbuilding/cqteleport.inc.php');
                    $btobj = new CQTeleport($nacridan, $db);
                    break;
                case "arena":
                    require_once (HOMEPATH . '/conquest/cqarena.inc.php');
                    $btobj = new CQArena($nacridan, $db);
                    break;
                case "caravane":
                    require_once (HOMEPATH . '/conquest/inbuilding/cqcaravan.inc.php');
                    $btobj = new CQCaravan(nacridan, $db);
                    break;
                default:
                    break;
            }
        }
    }
    
    $MAIN_BOTTOM->add($btobj);
    
    if (! isset($center))
        $center = "";
    
    if (($center == "view" && $nacridan->auth->auth["view2d"]) || $center == "view2d") {
        if (null === $minimap && $isMinimapNeeded) {
            require_once (HOMEPATH . '/conquest/cqview2D.inc.php');
            $tempview = new CQView2D($nacridan, $MAIN_BODY, $db);
            $minimap = $tempview->minimap;
            $radius = $tempview->radius;
        }
        if (null !== $minimap) {
            require_once (HOMEPATH . '/conquest/cqviewpanel.inc.php');
            $btobjmenu = new CQViewpanel($nacridan, $minimap, $move2Daction, $radius, $db);
            
            $MAIN_BOTTOM->add($btobjmenu);
        }
    }
}

require_once (HOMEPATH . '/conquest/cqright.inc.php');
$MAIN_BODY->add(new CQRight($nacridan, $db));

$str = "</div>";
$MAIN_BODY->add($str);

// Nouvel install de google analytic avec le compte nacridan.contact@gmail.com
// Ne pas supprimer le script ci-dessous ! Voir avec Aé Li pour toute questions

$google = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68594660-1', 'auto');
  ga('send', 'pageview');

</script>";

$MAIN_BODY->add($google);

$MAIN_PAGE->render();

Translation::saveMessages();

// profiler_stop("start");
// $prof->printTimers(PROFILER_MODE);

/*
 * mysql_query("update Player set ap='8' where id='6920'");
 * echo mysql_error();
 */
?>
