<?php
require_once (HOMEPATH . "/factory/MailFactory.inc.php");

class CQCompose extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQCompose($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $recipient = "";
        if (isset($_GET["to"])) {
            $to = urldecode($_GET["to"]);
            $to = str_replace("'", "&#8217;", $to);
            $recipient = $to;
        }
        
        $title = "";
        if (isset($_GET["re"])) {
            $re = urldecode($_GET["re"]);
            $re = str_replace("'", "&#8217;", $re);
            $title = $re;
        }
        
        $err = "";
        
        if (isset($_POST["Send"])) {
            $err = $this->sendMsg();
        }
        
        /*
         * if(isset($_POST["Search"]))
         * {
         * $target="/conquest/conquest.php?center=search";
         * }
         *
         * $target="/conquest/conquest.php?center=compose";
         */
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $item = array();
        
        $id = $curplayer->get("id");
        $gap = 5;
        if ($curplayer->get("inbuilding")) {
            $gap = 3;
            if ($curplayer->get("room") == 1)
                $dbp = new DBCollection("SELECT id,name FROM Player WHERE status='PC' AND map=" . $map . " AND (( inbuilding=" . $curplayer->get("inbuilding") . " AND room=1) OR (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap . ")", $db, 0, 0);
            else
                $dbp = new DBCollection("SELECT id,name FROM Player WHERE status='PC' AND map=" . $map . " AND room=" . $curplayer->get("room") . " AND inbuilding =" . $curplayer->get("inbuilding"), $db, 0, 0);
        } else
            $dbp = new DBCollection("SELECT Player.id,racename,name,Member.authlevel,Player.status FROM Player LEFT JOIN Member ON Member.id=Player.id_Member WHERE inbuilding=0 AND name!='' AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap, $db, 0, 0);
        
        $item[] = array(
            localize("-- Personnage(s) dans votre vue --") => - 1
        );
        while (! $dbp->eof()) {
            if ($dbp->get("id") != $id && $curplayer->canSeePlayerById($dbp->get("id"), $this->db)) {
                $item[] = array(
                    $dbp->get("name") => $dbp->get("id")
                );
            }
            $dbp->next();
        }
        
        $dbp = new DBCollection("SELECT id,name FROM Player WHERE status='PC' AND id_FighterGroup != 0 AND id_FighterGroup=" . $curplayer->get("id_FighterGroup"), $db, 0, 0);
        
        $id = $curplayer->get("id");
        
        $item[] = array(
            localize("-- Membre de votre GdC --") => - 1
        );
        while (! $dbp->eof()) {
            if ($dbp->get("id") != $id) {
                $item[] = array(
                    $dbp->get("name") => $dbp->get("id")
                );
            }
            $dbp->next();
        }
        
        $dbp = new DBCollection("SELECT id_Player\$friend,name FROM MailContact LEFT JOIN Player ON id_Player\$friend=Player.id WHERE id_Player=" . $curplayer->get("id"), $db, 0, 0);
        
        $item[] = array(
            localize("-- Le(s) Contact(s) Enregistré(s)--") => - 1
        );
        while (! $dbp->eof()) {
            $item[] = array(
                $dbp->get("name") => $dbp->get("id_Player\$friend")
            );
            $dbp->next();
        }
        
        $dbp = new DBCollection("SELECT MailAlias.id,id_Player,alias,name FROM MailAlias WHERE id_Player =" . $curplayer->get("id"), $db);
        
        $item[] = array(
            localize("-- Alias Enregistré(s)--") => - 1
        );
        while (! $dbp->eof()) {
            $item[] = array(
                "[" . $dbp->get("name") . "]" => $dbp->get("id")
            );
            $dbp->next();
        }
        
        $item[] = array(
            localize("-- Listes spéciales --") => - 1
        );
        if ($curplayer->get("id_Team") != 0)
            $item[] = array(
                "[Tout l'Ordre]" => $curplayer->get("id_Team")
            );
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=search' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr><td class='mainbgtitle tabmenu'>" . "<label>Rechercher :  <input type='text' name='query' size='20px'/> " . "</label>
	<input id='Search' type='submit' name='Search' value='" . localize("Recherche") . "' />
	<span class='mainbgtitle' style='font-size: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp; Attention la recherche ne s'effectue pas dans les alertes.</span></td></tr>";
        $str .= "</table>";
        $str .= "</form>\n";
        
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=compose' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize('M E S S A G E R I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'>";
        $str .= "<a href='../conquest/conquest.php?center=compose' class='tabmenu'>" . localize('Composer un Message') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Message(s) Reçu(s)') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=2' class='tabmenu'>" . localize('Message(s) Envoyé(s)') . "</a> | ";
        $str .= "<a href='../conquest/conquest.php?center=contact' class='tabmenu'>" . localize('Contacts') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=alias' class='tabmenu'>" . localize('Alias') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=archive' class='tabmenu'>" . localize('Archive(s)') . " </a>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td colspan='6'class='mainbglabel' width='150px'><b>" . localize('Titre :') . "</b></td><td class='mainbgtitle'><input id='Title' style='width:300px;' name='Title' type='text' value='" . $title . "' /></td></tr>";
        $str .= "<tr><td colspan='6'class='mainbglabel' width='150px' valign='top'><b>" . localize('Destinataire :') . "</b></td><td class='mainbgtitle'>";
        
        $str .= "<select id='DestDrop' class='cqselector' style='width:250px;' name='DestDrop' onChange='javascript:addDest(\"Recipient\",this,\"" . localize("Pour limiter le SPAM, le nombre de destinaire est limité à 10") . "\")'>";
        
        $str .= "<option value='0' selected='selected'>" . localize('-- Choisissez un destinataire --') . "</option>";
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $val) {
                if ($val == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $val . "'>" . $key . "</option>";
            }
        }
        
        $str .= "</select><br/>";
        $str .= "(" . localize('Le nom de chaque destinataire doit être séparé par une virgule') . ")<br/>";
        $str .= "<input type='text' id='Recipient' name='Recipient' value='" . $recipient . "' style='width:450px;'></td></tr>";
        $str .= "<tr><td colspan='6'class='mainbglabel' width='150px'><b>" . localize('Message :') . "</b></td><td class='mainbgtitle'><textarea id='Body' style='width:450px;' name='Body' rows='9' cols='40'></textarea></td></tr>";
        $str .= "</table>";
        
        $str .= "<input id='Send' type='submit' name='Send' value='" . localize('Envoyer') . "' />";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</form>\n";
        /*
         * $str.="<script type='text/javascript'>\n";
         * $str.="var addLimit=8;\n";
         * $str.="var nbadd=0;\n";
         *
         * $str.="function addDest(dest)\n";
         * $str.="{\n";
         * $str.="if (dest)\n";
         * $str.="{\n";
         * $str.="index=dest.selectedIndex;\n";
         * $str.="if(dest.value>0)\n";
         * $str.="{\n";
         * $str.=" value= ' ' + dest.options[index].text + ',';\n";
         * $str.="curDest=document.getElementById('Recipient');\n";
         * $str.="curDest.value=trim(curDest.value);\n";
         * $str.="if(curDest.value.length!=0 && curDest.value.charAt(curDest.value.length)!=',')\n";
         * $str.=" curDest.value+=',';\n";
         * $str.="curDest.value=undoublecoma(curDest.value);";
         * $str.="if ((pos = curDest.value.indexOf(value,0)) < 0)\n";
         * $str.="{\n";
         * $str.="if(nbadd<addLimit)\n";
         * $str.="{\n";
         * $str.=" curDest.value=curDest.value+value;\n";
         * $str.=" nbadd=nbadd+1;\n";
         * $str.="}\n";
         * $str.="else\n";
         * $str.="{\n";
         * $str.=" alert('".localize("Pour limiter le SPAM, le nombre de destinaire est limité à 10")."');\n";
         * $str.="}\n";
         * $str.="}\n";
         * $str.=" }\n";
         * $str.="dest.selectedIndex=0;\n";
         * $str.=" }\n";
         * $str.="} \n";
         * $str.="</script> \n";
         */
        return $str;
    }

    public function sendMsg()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        if (isset($_POST["Title"]))
            $title = $_POST["Title"];
        if (isset($_POST["Body"]))
            $body = $_POST["Body"];
        if (isset($_POST["Recipient"]))
            $dest = $_POST["Recipient"];
        
        $datetime = gmdate("Y-m-d H:i:s");
        
        if ($title == "") {
            $title = localize("(pas de titre)");
        }
        
        $array = explode(",", $dest);
        $type = '0';
        $err = "";
        MailFactory::sendMsg($curplayer, $title, $body, $array, $type, $err, $db);
        return $err;
    }
}

?>
