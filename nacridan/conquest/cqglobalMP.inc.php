<?php

class CQGlobalMP extends HTMLObject
{

    public $nacridan;

    public $players;

    public $db;

    public $style;

    public function CQGlobalMP($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $id = 0;
        
        $curAdmin = $this->nacridan->loadCurSessPlayer($db);
        
        if (isset($_POST['createconfirm'])) // ------------------- CREATION -----------------
{
            if ($_POST['createconfirm'] == 'yes') {
                
                require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                
                $title = base64_decode($_POST["title"]);
                $body = base64_decode($_POST["MPcontent"]);
                
                $author = $curAdmin;
                
                $target = $curAdmin->get("name");
                
                $dbplayers = new DBCollection('SELECT name FROM Player WHERE id_member!=0 AND authlevel<2 AND disabled!=99 AND status="PC" AND name IS NOT NULL ', $db);
                while (! $dbplayers->eof()) {
                    
                    $target .= "," . $dbplayers->get("name");
                    
                    $dbplayers->next();
                }
                $targetArray = explode(",", $target);
                
                $type = 0; // message normal
                MailFactory::sendSingleDestMsg($author, $title, $body, $targetArray, $type, $err, $this->db, - 1, 0);
                
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>" . $err . "</td></tr>";
                
                $str .= "</table>";
            } else {
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>Le message n'a pas été envoyé.</td></tr>";
                $str .= "</table>";
            }
            return $str;
        } elseif (isset($_POST['check'])) // --------------VERIFICATION DE L'ANNONCE
{
            $content = array();
            $title = array();
            $section = array();
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE ANNONCE') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody'>Cette annonce convient-elle ? Attention le message sera envoyé à tous les joueurs !</td></tr>";
            $str .= "<tr><td>";
            $str .= "<div style='color: #999999'>";
            
            $str .= "<b>" . stripslashes($_POST["title"]) . "</b><br/>";
            $str .= "<br/>";
            $str .= str_replace("\n", "<br/>", stripslashes($_POST["MPcontent"])) . "<br/>";
            $str .= "<br/>";
            $title = stripslashes($_POST["title"]);
            $MPcontent = stripslashes($_POST["MPcontent"]);
            
            $str .= "</div>";
            $str .= "</td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='createconfirm' value='yes' id='yes' />" . "<label for='yes'>Oui.</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='createconfirm' value='no' id='no' />" . "<label for='no'>Non.</label></td></tr>";
            // $str.="<tr><td class='mainbgbody'>".serialize($content)."</td></tr>";
            $str .= "</table>";
            
            $str .= "<input type='hidden' name='title' value='" . base64_encode($title) . "'/>";
            $str .= "<input type='hidden' name='MPcontent' value='" . base64_encode($MPcontent) . "'/>";
            $str .= "<td><input type='submit' name='ok' value='Ok'/></td></tr>";
            $str .= "</form>";
            
            return $str;
        } else // ------- FORMULAIRE DE CREATION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UN MP DIFFUSÉ A TOUS LES JOUEURS') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr>\n";
            $str .= "<td class='mainbgbody' width='100px' align='left'><label for='title'> Sujet du message</label> </td>";
            $str .= "<td class='mainbgbody' width='200px' align='center'> <input type='text' name='title' id='title' /> </td>";
            $str .= "</tr>";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' width='100px' align='left'><label for='MPcontent'> Contenu du message </label> </td>";
            $str .= "<td class='mainbgbody' width='200px' align='center'> <textarea rows=6 cols=40 name='MPcontent' id='MPcontent' /> </textarea></td>";
            $str .= "</tr>";
            
            $str .= "<td><input type='submit' name='check' value='Créer'/></td></tr>";
            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
        }
    }
}
?>

 
      
