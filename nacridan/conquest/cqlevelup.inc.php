<?php

class CQLevelUp extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQLevelUp($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        require_once (HOMEPATH . '/conquest/cqdef.inc.php');
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("levelUp")) {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>" . localize("Passer au niveau suivant ? ") . "</td><td class='mainbgtitle' align='center'>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='" . localize("Valider") . "' />";
            $str .= "<input name='action' type='hidden' value='" . LEVEL_UP . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        } else {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>" . localize("Vous ne satisfaites pas les conditions requises. ") . "</td><td class='mainbgtitle' align='center'>";
            $str .= "</td></tr></table>";
        }
        
        return $str;
    }
}
?>
