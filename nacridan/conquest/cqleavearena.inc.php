<?php

class CQLeaveArena extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQLeaveArena($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("ap") < ARENA_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour retourner vers le continent.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable centerareawidth'><tr><td class='mainbgtitle' width='550px'>" . localize("Êtes vous sûr de vouloir retourner vers le continent ? ") . "</td><td>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='" . localize("Sortir") . "' />";
            $str .= "<input name='action' type='hidden' value='" . LEAVE_ARENA . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>