<?php

class CQMenu extends HTMLObject
{

    public $nacridan;

    public function CQMenu($nacridan, $db)
    {
        $this->nacridan = $nacridan;
        $this->db = $db;
    }

    public function toString()
    {
        $sess = $this->nacridan->sess;
        $id = $sess->get("cq_playerid");
        
        $str = "<div class='menu'>";
        $str .= "<dl class='menuright'>";
        
        $curplayer = $this->nacridan->loadCurSessPlayer($this->db);
        require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
        $typeArm = PlayerFactory::getTypeArm($curplayer, $this->db);
        $basicArm = PlayerFactory::getBasicArm($curplayer, $this->db);
        $state = $curplayer->get("state");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        // calcul du nombre de PA d'un déplacement
        $PA = $curplayer->getTimeToMove($this->db);
        
        if ($sess->has("DLA" . $id)) {
            if ($curplayer->get("room") == 21) {
                if ($curplayer->get("levelUp")) {
                    $str .= "<dt style='background: #BB8F81;' onClick=\"showSubMenu('smenu6');\">" . localize("Niveau") . "</dt>\n";
                    $str .= "<dd style='display: none;' id='smenu6'>\n";
                    $str .= "<ul>\n";
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=levelUp'>" . localize("Passer au niveau suivant (0 PA)") . "</a></li>\n";
                    $str .= "</ul>\n";
                    $str .= "</dd>\n";
                }
            } elseif ($curplayer->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                if ($state == "creeping") {
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Ramper ({ap} PA)",
                        array(
                            "ap" => 5
                        )) . "</a></li>\n";
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=free'>" . localize("Se libérer ({ap} PA)",
                        array(
                            "ap" => 8
                        )) . "</a></li>\n";
                } else
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Se déplacer ({ap} PA)",
                        array(
                            "ap" => $PA
                        )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=attack'>" . localize("Boule de feu ({ap} PA)",
                    array(
                        "ap" => 8
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=blood'>" . localize("Sang de lave ({ap} PA)",
                    array(
                        "ap" => 8
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=attackBuilding'>" . localize("Attaquer un bâtiment ({ap} PA)",
                    array(
                        "ap" => 8
                    )) . "</a></li>\n";
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            } elseif ($curplayer->get("id_BasicRace") == 263 || $curplayer->get("id_BasicRace") == 112) {
                if ($curplayer->get("state") == "creeping") {
                    $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                    $str .= "<dd style='display: none;' id='smenu1'>\n";
                    $str .= "<ul>\n";
                    $str .= "</ul>\n";
                    $str .= "</dd>\n";
                }
            } elseif ($curplayer->get("state") == "questionning") {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=arrest'>" . localize("Gestion de l'interpellation") .
                     "</a></li>\n";
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            } elseif ($curplayer->get("state") == "prison") {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=graffiti'>" . localize("Ecrire sur un mur ({ap} PA)", 
                    array(
                        "ap" => GRAFF_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=continuegraffiti'>" . localize(" Continuer une gravure ({ap} PA)", 
                    array(
                        "ap" => CONTINUE_GRAFF_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=consultgraffiti'>" . localize("Lire les gravures ({ap} PA)", 
                    array(
                        "ap" => CONSULT_GRAFF_AP
                    )) . "</a></li>\n";
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            } elseif ($curplayer->get("state") == "stunned") {} elseif ($curplayer->get("recall")) {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=recall'>" . localize("Gestion du Rappel") . "</a></li>\n";
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            } elseif ($curplayer->get("levelUp")) {
                $str .= "<dt style='background: #BB8F81;' onClick=\"showSubMenu('smenu6');\">" . localize("Niveau") . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu6'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=levelUp'>" . localize("Passer au niveau suivant (0 PA)") . "</a></li>\n";
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            } elseif ($curplayer->get("state") == "turtle") {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=turtle'>" . localize("Descendre ({ap} PA)",
                    array(
                        "ap" => TURTLE_UNMOUNT_AP
                    )) . "</a></li>\n";
                
                $dbt = new DBCollection("SELECT Player.state FROM Player LEFT JOIN Caravan ON Caravan.id=Player.id_Caravan WHERE Caravan.id_Player=" . $curplayer->get("id"), 
                    $this->db);
                if (! $dbt->eof() && $dbt->get("state") == "creeping")
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=free'>" . localize("Désentraver la tortue ({ap} PA)",
                        array(
                            "ap" => FREE_AP
                        )) . "</a></li>\n";
                
                if ($curplayer->get("inbuilding"))
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=leavebuilding'>" . localize(
                        "Sortir du bâtiment avec la tortue ({ap} PA)", array(
                            "ap" => $PA
                        )) . "</a></li>\n";
                else {
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Avancer dans la tortue ({ap} PA)",
                        array(
                            "ap" => $PA
                        )) . "</a></li>\n";
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=hustle'>" . localize("Bousculer ({ap} PA)",
                        array(
                            "ap" => $PA + HUSTLE_AP
                        )) . "</a></li>\n";
                }
                $str .= "</ul>\n";
                $str .= "</dd>\n";
                
                $dbc = new DBCollection(
                    "SELECT * FROM Talent LEFT JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id WHERE (BasicTalent.type=2 or BasicTalent.type=3) AND id_Player=" .
                         $curplayer->get("id"), $this->db);
                if (! $dbc->eof()) {
                    $str .= "<dt onClick=\"showSubMenu('smenu5');\">" . localize("Savoir-faire") . "</dt>\n";
                    $str .= "<dd style='display: none;' id='smenu5'>\n";
                    $str .= "<ul>\n";
                    
                    while (! $dbc->eof()) {
                        $time = $dbc->get("PA");
                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=talent&id=" . $dbc->get("ident") . "'>" . localize(
                            $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", array(
                                "ap" => $time
                            )) . "</a></li>\n";
                        $dbc->next();
                    }
                    $str .= "</ul>\n";
                    $str .= "</dd>\n";
                }
            } else {
                
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Actions') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                if ($state == "creeping") {
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Ramper ({ap} PA)",
                        array(
                            "ap" => 5
                        )) . "</a></li>\n";
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=free'>" . localize("Se libérer ({ap} PA)",
                        array(
                            "ap" => 8
                        )) . "</a></li>\n";
                } elseif ($curplayer->get("hidden") % 10 == 1) {
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Se déplacer ({ap} PA)",
                        array(
                            "ap" => $PA
                        )) . "</a></li>\n";
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=appear'>" . localize("Redevenir visible ({ap} PA)",
                        array(
                            "ap" => 0
                        )) . "</a></li>\n";
                } elseif ($curplayer->get("hidden") % 10 == 2) {
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Se déplacer ({ap} PA)",
                        array(
                            "ap" => 5
                        )) . "</a></li>\n";
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=appear'>" . localize("Redevenir visible ({ap} PA)",
                        array(
                            "ap" => 0
                        )) . "</a></li>\n";
                } else {
                    if (! $curplayer->get("inbuilding")) {
                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=move'>" . localize("Se Déplacer ({ap} PA)",
                            array(
                                "ap" => $PA
                            )) . "</a></li>\n";
                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=hustle'>" . localize("Bousculer ({ap} PA)",
                            array(
                                "ap" => $PA + HUSTLE_AP
                            )) . "</a></li>\n";
                    } else
                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=leavebuilding'>" . localize(
                            "Sortir du bâtiment ({ap} PA)", array(
                                "ap" => $PA
                            )) . "</a></li>\n";
                        
                        // Possibilité de chevaucher sa tortue
                    $dbt = new DBCollection(
                        "SELECT * FROM Player LEFT JOIN Caravan ON Player.id_Caravan=Caravan.id WHERE  Caravan.id_Player=" . $curplayer->get("id") . " AND id_BasicRace=263", 
                        $this->db);
                    if (! $dbt->eof() && distHexa($xp, $yp, $dbt->get("x"), $dbt->get("y")) < 2)
                        if ($curplayer->get("room") == $dbt->get("room"))
                            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=turtle'>" . localize(
                                "Chevaucher la tortue ({ap} PA)", array(
                                    "ap" => RIDE_TURTLE_AP
                                )) . "</a></li>\n";
                }
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=attack'>" . localize("Attaquer ({ap} PA)",
                    array(
                        "ap" => $curplayer->getScore("timeAttack")
                    )) . "</a></li>\n";
                
                $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND name='Concentration'", $this->db);
                if (! $dbbm->eof())
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=concentration'>" . localize("Concentration ({ap} PA)",
                        array(
                            "ap" => CONCENTRATION_AP
                        )) . "</a></li>\n";
                else
                    $str .= "<li><i> Concentration </i></li>\n";
                
                if ($typeArm != 5 && $typeArm != 22)
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=attackBuilding'>" . localize(
                        "Attaquer un bâtiment ({ap}/{pa} PA)", 
                        array(
                            "ap" => $curplayer->getScore("timeAttack"),
                            "pa" => 8
                        )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=givemoney'>" . localize("Donner des PO ({ap} PA)", 
                    array(
                        "ap" => GIVE_MONEY_AP
                    )) . "</a></li>\n";
                // $str.="<li><a href='../conquest/conquest.php?bottom=atbshift'>".localize("Décaler sa DLA ({ap} PA)",array ("ap" => ATBSHIFT_AP))."</a></li>\n";
                
                $str .= "</ul>\n";
                $str .= "</dd>\n";
                $str .= "<dt onClick=\"showSubMenu('smenu2');\">" . localize('Objets') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu2'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=pick&room=" . $curplayer->get("room") . "'>" . localize("Ramasser un objet ({ap} PA)", 
                    array(
                        "ap" => PICK_UP_OBJECT_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=drop&room=" . $curplayer->get("room") . "'>" . localize("Poser un objet ({ap} PA)", 
                    array(
                        "ap" => DROP_OBJECT_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=give&room=" . $curplayer->get("room") . "'>" . localize("Donner un objet ({ap} PA)", 
                    array(
                        "ap" => GIVE_OBJECT_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=wear'>" . localize("S'équiper ({ap} PA)", 
                    array(
                        "ap" => WEAR_EQUIPMENT_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=unwear'>" . localize("Se déséquiper ({ap} PA)", 
                    array(
                        "ap" => UNWEAR_EQUIPMENT_AP
                    )) . "</a></li>\n";
                if ($curplayer->get("inbuilding"))
                    $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=store'>" . localize("Ranger ({ap} PA)", 
                        array(
                            "ap" => STORE_OBJECT_INBUILDING_AP
                        )) . "</a></li>\n";
                else
                    $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=store'>" . localize("Ranger ({ap} PA)", 
                        array(
                            "ap" => STORE_OBJECT_AP
                        )) . "</a></li>\n";
                
                $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=usepotion'>" . localize("Utiliser une potion ({ap1}/{ap2} PA)", 
                    array(
                        "ap1" => USE_BELTPOTION_AP,
                        "ap2" => USE_INVPOTION_AP
                    )) . "</a></li>\n";
                
                // $str.="<li><a href='../conquest/conquest.php?center=view&bottom=parchment'>".localize("Utiliser un parchemin ({ap} PA)",array ("ap" => USE_PARCHMENT_AP))."</a></li>\n";
                
                $str .= "<li><a href='../conquest/conquest.php?center=equip&bottom=useparchment'>" . localize("Utiliser un parchemin ({ap} PA)", 
                    array(
                        "ap" => USE_PARCHMENT_AP
                    )) . "</a></li>\n";
                
                $str .= "</ul>\n";
                $str .= "</dd>\n";
                
                $dbc = new DBCollection(
                    "SELECT * FROM Ability LEFT JOIN BasicAbility ON Ability.id_BasicAbility=BasicAbility.id WHERE id_Player=" . $curplayer->get("id") .
                         " ORDER BY BasicAbility.level, BasicAbility.name", $this->db);
                if (! $dbc->eof()) {
                    $str .= "<dt onClick=\"showSubMenu('smenu3');\">" . localize('Compétences') . "</dt>\n";
                    $str .= "<dd style='display: none;' id='smenu3'>\n";
                    while (! $dbc->eof()) {
                        
                        // Pas de compétence de guerrier avec un arc et pas de lancer de bolas sans bolas et pas compétence d'archer sans arc
                        if (! (($typeArm == 5 && $dbc->get("usable") % 10 == 1) || ($typeArm == 22 && $dbc->get("usable") % 10 == 1)) and
                             ($dbc->get("usable") != 2 or ($dbc->get("usable") % 10 == 2 && $basicArm == 205)) and
                             (! ($typeArm != 5 && $dbc->get("usable") % 10 == 3) or ! ($typeArm != 22 && $dbc->get("usable") % 10 == 3)) and
                             ! ($curplayer->get("inbuilding") && $curplayer->get("room") != TOWER_ROOM && $dbc->get("usable") >= 20)) {
                            
                            // Calcul des PA
                            $time = $dbc->get("modifierPA");
                            if ($dbc->get("timeAttack") == 'YES')
                                $time += $curplayer->getScore("timeAttack");
                            
                            switch ($dbc->get("id")) {
                                case 14:
                                    $dba = new DBCollection(
                                        "SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND id_Player\$src=" . $curplayer->get("id") . " AND name='Aura de courage'", 
                                        $this->db);
                                    if (! $dba->eof())
                                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=disabledbravery'>" . localize(
                                            "Désactiver Aura de courage ({ap} PA)", 
                                            array(
                                                "ap" => 0
                                            )) . "</a></li>\n";
                                    else
                                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=ability&id=" . $dbc->get("ident") . "'>" . localize(
                                            $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                            array(
                                                "ap" => $time
                                            )) . "</a></li>\n";
                                    break;
                                case 15:
                                    $dba = new DBCollection(
                                        "SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND id_Player\$src=" . $curplayer->get("id") .
                                             " AND name='Aura de résistance'", $this->db);
                                    if (! $dba->eof())
                                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=disabledresistance'>" . localize(
                                            "Désactiver Aura de résistance ({ap} PA)", 
                                            array(
                                                "ap" => 0
                                            )) . "</a></li>\n";
                                    else
                                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=ability&id=" . $dbc->get("ident") . "'>" . localize(
                                            $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                            array(
                                                "ap" => $time
                                            )) . "</a></li>\n";
                                    break;
                                case 24:
                                    $dba = new DBCollection("SELECT * FROM BM WHERE id_Player\$src=" . $curplayer->get("id") . " AND name='défendu'", $this->db);
                                    if (! $dba->eof())
                                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=disabledprotection'>" . localize(
                                            "Désactiver la protection ({ap} PA)", 
                                            array(
                                                "ap" => 0
                                            )) . "</a></li>\n";
                                    else
                                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=ability&id=" . $dbc->get("ident") . "'>" . localize(
                                            $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                            array(
                                                "ap" => $time
                                            )) . "</a></li>\n";
                                    break;
                                
                                default:
                                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=ability&id=" . $dbc->get("ident") . "'>" . localize(
                                        $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                        array(
                                            "ap" => $time
                                        )) . "</a></li>\n";
                                    break;
                            }
                        } else
                            $str .= "<li><i>" . $dbc->get("name") . "</i></li>\n";
                        $dbc->next();
                    }
                    $str .= "<ul>\n";
                    $str .= "</ul>\n";
                    $str .= "</dd>\n";
                }
                
                $dbc = new DBCollection(
                    "SELECT * FROM MagicSpell LEFT JOIN BasicMagicSpell ON MagicSpell.id_BasicMagicSpell=BasicMagicSpell.id WHERE id_Player=" . $curplayer->get("id") .
                         " order by BasicMagicSpell.level,BasicMagicSpell.name", $this->db);
                if (! $dbc->eof()) {
                    $str .= "<dt onClick=\"showSubMenu('smenu4');\">" . localize("Sortilèges") . "</dt>\n";
                    $str .= "<dd style='display: none;' id='smenu4'>\n";
                    $str .= "<ul>\n";
                    
                    while (! $dbc->eof()) {
                        $time = $dbc->get("PA");
                        if ($dbc->get("id") == 27) {
                            $dba = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND name='Golem de feu'", $this->db);
                            if (! $dba->eof())
                                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=revokegolem'>" . localize(
                                    "Révoquer le Feu Fol ({ap} PA)", array(
                                        "ap" => 0
                                    )) . "</a></li>\n";
                            else
                                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=spell&id=" . $dbc->get("ident") . "'>" . localize(
                                    $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                    array(
                                        "ap" => $time
                                    )) . "</a></li>\n";
                        } elseif ($dbc->get("id") == 25) {
                            $dba = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND name='" . addslashes("Projection de l'âme") . "'", $this->db);
                            if (! $dba->eof())
                                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=revokesoul'>" . localize(
                                    "Récupérer son âme ({ap} PA)", array(
                                        "ap" => 0
                                    )) . "</a></li>\n";
                            else
                                $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=spell&id=" . $dbc->get("ident") . "'>" . localize(
                                    $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                    array(
                                        "ap" => $time
                                    )) . "</a></li>\n";
                        } elseif (! ($curplayer->get("inbuilding") && $curplayer->get("room") != TOWER_ROOM && $dbc->get("usable") >= 10))
                            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=spell&id=" . $dbc->get("ident") . "'>" . localize(
                                $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", 
                                array(
                                    "ap" => $time
                                )) . "</a></li>\n";
                        else
                            $str .= "<li><i>" . $dbc->get("name") . "</i></li>\n";
                        $dbc->next();
                    }
                    $str .= "</ul>\n";
                    $str .= "</dd>\n";
                }
                
                $dbc = new DBCollection("SELECT * FROM Talent LEFT JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id WHERE id_Player=" . $curplayer->get("id"), $this->db);
                if (! $dbc->eof()) {
                    $str .= "<dt onClick=\"showSubMenu('smenu5');\">" . localize("Savoir-faire") . "</dt>\n";
                    $str .= "<dd style='display: none;' id='smenu5'>\n";
                    $str .= "<ul>\n";
                    
                    while (! $dbc->eof()) {
                        $time = $dbc->get("PA");
                        $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=talent&id=" . $dbc->get("ident") . "'>" . localize(
                            $dbc->get("name") . " ({ap} PA - " . $dbc->get("skill") . "%)", array(
                                "ap" => $time
                            )) . "</a></li>\n";
                        $dbc->next();
                    }
                    $str .= "</ul>\n";
                    $str .= "</dd>\n";
                }
                
                $dbc = new DBCollection("SELECT * FROM RolePlayAction WHERE id_Player=" . $curplayer->get("id"), $this->db);
                
                $str .= "<dt onClick=\"showSubMenu('smenu6');\">" . localize("Action RP") . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu6'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='" . CONFIG_HOST .
                     "/gg/ggsite/index.php?page=shop.ShopHome' onclick='window.open(this.href); return false;'>Soutenez l'association en créant votre propre action à 1PA</a></li>\n";
                while (! $dbc->eof()) {
                    
                    $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=rpaction&id=" . $dbc->get("id") . "'>" . localize($dbc->get("name") . " (1 PA)") .
                         "</a></li>\n";
                    $dbc->next();
                }
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            }
        }
        
        // ------------------- VERSION DE TEST ----------------
        if ($curplayer->get("authlevel") > 2) {
            $str .= "<dt onClick=\"showSubMenu('smenu10');\">" . localize('Test - persos') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu10'>\n";
            $str .= "<ul>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addAP'>" . localize("Ajouter des PA.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addMoney'>" . localize("Ajouter des PO.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=levelup'>" . localize("Level up") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=newDLA'>" . localize("Nouvelle DLA") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=createExploitation'>" . localize("Créer une exploitation.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=changeModifier'>" . localize("Changer carac.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=deleteChapelMalus'>" . localize("Supprimer malus Chapelle") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=instantTP'>" . localize("Téléportation.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=createMonster'>" . localize("Créer un monstre.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=cloneMonster'>" . localize("Cloner un monstre.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=playIAMonster'>" . localize("Jouer Monstre par IA.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=calcSPath'>" . localize("Calculer le chemin le plus court.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addAbilityToPNJ'>" . localize("Donner une comp à un perso.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addMagicSpellToPNJ'>" . localize("Donner un sort à un perso.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=addTalentToPNJ'>" . localize("Donner un savoir-faire à un perso.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=giveItemToPNJ'>" . localize("Donner un objet à un perso.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=giveMoneyToPNJ'>" . localize("Donner des PO à un perso.") .
                 "</a></li>\n";
            // $str.="<li><a href='../conquest/conquest.php?center=view&bottom=accessBDD&".getGoogleConquestKeywords()."'>".localize("Changer une valeur.")."</a></li>\n";
            
            $str .= "</ul>\n";
            $str .= "</dd>\n";
            
            $str .= "<dt onClick=\"showSubMenu('smenu11');\">" . localize('Test - objets') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu11'>\n";
            $str .= "<ul>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=giveItemToPNJ'>" . localize("Créer un objet.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=renameObject'>" . localize("Renommer un objet.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=createParchment'>" . localize("Créer un parchemin personnalisé.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=emptyBag'>" . localize("Vider Sac.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=destroyObject'>" . localize("Détruire un Object au sol.") .
                 "</a></li>\n";
            $str .= "</ul>\n";
            $str .= "</dd>\n";
            
            $str .= "<dt onClick=\"showSubMenu('smenu12');\">" . localize('Test - bâtiments') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu12'>\n";
            $str .= "<ul>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=createBuilding'>" . localize("Créer un bâtiment.") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=renameBuilding'>" . localize("Renommer un bâtiment.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=destroyBuilding'>" . localize("Détruire un bâtiment.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=openOrCloseDoors'>" . localize("Ouvrir ou fermer les portes.") .
                 "</a></li>\n";
            $str .= "</ul>\n";
            $str .= "</dd>\n";
            
            $str .= "<dt onClick=\"showSubMenu('smenu13');\">" . localize('Test - autres') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu13'>\n";
            $str .= "<ul>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=runAI'>" . localize("Faire tourner l'IA") . "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=monsterProfile'>" . localize("Gestion des illus et descriptions.") .
                 "</a></li>\n";
            $str .= "<li><a href='../conquest/conquest.php?center=view&bottom=terraformCase'>" . localize("Terraformation d'une case.") .
                 "</a></li>\n";
            $str .= "</ul>\n";
            $str .= "</dd>\n";
        }
        // ------------------- FIN VERSION DE TEST ----------------
        
        /*
         * $dbc=new DBCollection("SELECT * FROM City WHERE x=".$curplayer->get("x")." AND y=".$curplayer->get("y"),$this->db);
         *
         * if($sess->has("DLA".$id) and $dbc->count()>0)
         * {
         * if(strncmp ($dbc->get("name"),"Pilier",6)==0)
         * {
         * $str.="<dt style='background: #BB8F81;' onClick=\"showSubMenu('smenu6');\">".localize("Lieu")."</dt>\n";
         * $str.="<dd style='display: none;' id='smenu6'>\n";
         * $str.="<ul>\n";
         * $str.="<li><a href='../conquest/conquest.php?bottom=leavearena'>".localize("Retourner vers le continent ({ap} PA)",array ("ap" => ARENA_AP))."</a></li>\n";
         * $str.="</ul>\n";
         * $str.="</dd>\n";
         * }
         * else
         * {
         * $str.="<dt style='background: #BB8F81;' onClick=\"showSubMenu('smenu6');\">".localize("Lieu")."</dt>\n";
         * $str.="<dd style='display: none;' id='smenu6'>\n";
         * $str.="<ul>\n";
         * $str.="<li><a href='../conquest/conquest.php?bottom=entercity'>".localize("Entrer dans le lieu ({ap} PA)",array ("ap" => ENTER_CITY_AP))."</a></li>\n";
         * $str.="</ul>\n";
         * $str.="</dd>\n";
         * }
         * }
         */
        
        $str .= "</dl>\n";
        $str .= "</div>\n";
        return $str;
    }

    public function render()
    {
        echo $this->toString();
    }
}
?>
