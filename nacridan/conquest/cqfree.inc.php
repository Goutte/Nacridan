<?php

class CQFree extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQFree($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        $xp = $curplayer->get("x");
        
        // La tortue
        $dbt = new DBCollection(
            "SELECT Player.state, Player.x, Player.y FROM Player LEFT JOIN Caravan ON Caravan.id=Player.id_Caravan WHERE Caravan.id_Player=" . $curplayer->get("id"), $db);
        
        if ($curplayer->get("ap") < FREE_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour effectuer cette action.");
            $str .= "</td></tr></table>";
        } elseif ($curplayer->get("state") == "creeping") {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable'><tr><td  class='mainbgtitle' width='750px'>";
            $str .= "Vous êtes sur le point de vous libérer, continuer ?" . "</td>";
            $str .= "<td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . FREE . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        } elseif (! $dbt->eof() && $dbt->get("state") == "creeping") {
            if (distHexa($dbt->get("x"), $dbt->get("y"), $curplayer->get("x"), $curplayer->get("y")) > 1) {
                $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Votre personnage {name} est trop loin de la tortue pour la libérer.", array(
                    "name" => $dbc->get("name")
                ));
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable'><tr><td  colspan='2' class='mainbgtitle' width='750px'>";
                // $str.="Attention, cette action utilise les points d'action de votre personnage ".$dbc->get("name")." et non ceux de la tortue ! </td><tr/>";
                $str .= "<tr><td class='mainbgtitle' width='750px'> Vous êtes sur le point de libérer votre tortue de son entrave, continuer ?" . "</td>";
                $str .= "<td class='mainbgtitle'><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . FREE . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        }
        
        return $str;
    }
}

?>