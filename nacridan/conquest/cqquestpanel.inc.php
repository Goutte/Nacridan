<?php

class CQQuestpanel extends HTMLObject
{

    public $nacridan;

    public $players;

    public $db;

    public $style;

    public function CQQuestpanel($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        
        if (! isset($_GET["panel"]))
            $panel = "1";
        else
            $panel = $_GET["panel"];
        if (isset($_POST["delete"]))
            $panel = 2;
        if (isset($_POST["details"]))
            $panel = 3;
        if (isset($_POST["create"]))
            $panel = 4;
        
        switch ($panel) {
            case 1: // ------------- LISTE DES QUÊTES -----------
                $dbq = new DBCollection("SELECT Quest.id,Quest.name,Quest.nbMission,Quest.Player_levelMin,Quest.Player_levelMax FROM Quest WHERE In_List=1", $db);
                
                $data = array();
                
                while (! $dbq->eof()) {
                    $data[] = array(
                        'id' => $dbq->get('id'),
                        "name" => $dbq->get("name"),
                        "lvlmin" => $dbq->get("Player_levelMin")
                    );
                    $dbq->next();
                }
                
                $str = "<form name='form'  method=post target='_self'>\n";
                $str .= "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr>\n";
                $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('LISTES DES QUÊTES DISPONIBLES') . "</b></td>\n";
                $str .= "</tr>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel' width='120px' align='center'>" . localize('id') . "</td>\n";
                $str .= "<td class='mainbglabel' width='300px' align='center'>" . localize('Nom') . "</td>\n";
                $str .= "<td class='mainbglabel' align='center'>" . localize('Level Min') . "</td>\n";
                $str .= "</tr></table>";
                $str .= "<table class='maintable " . $this->style . "'>";
                
                foreach ($data as $arr) {
                    $str .= "<tr><td class='mainbgbody' width='20px' align='center'> <input type='checkbox' name='quest" . $arr["id"] . "' id='quest" . $arr["id"] . "' /> </td>";
                    $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["id"] . " </td>\n";
                    $str .= "<td class='mainbgbody' width='300px' align='left'><label for='quest" . $arr["id"] . "'> " . $arr["name"] . "</label> </td>";
                    $str .= "<td class='mainbgbody'  align='left'> " . $arr["lvlmin"] . " </td>";
                    $str .= "</tr>\n";
                }
                
                $str .= "</table>";
                
                $str .= "<input type='hidden' name='questlist' value='" . base64_encode(serialize($data)) . "'/>";
                $str .= "</table>";
                $str .= "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td><input type='submit' name='delete' value='Supprimer'/></td>";
                $str .= "<td><input type='submit' name='details' value='Voir les détails'/></td>";
                $str .= "<td><input type='submit' name='create' value='Créer une quête'/></td></tr>";
                $str .= "</table>";
                $str .= "</form>";
                return $str;
                break;
            
            case 2: // ---------------- SUPPRESSION ---------------
                if (isset($_POST['confirm'])) {
                    $str = "<table class='maintable " . $this->style . "'>\n";
                    $str .= "<tr>\n";
                    $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('SUPPRESSION DE QUÊTES') . "</b></td>\n";
                    $str .= "</tr>\n";
                    $str .= "</table>";
                    $str .= "<table class='maintable " . $this->style . "'>\n";
                    
                    if ($_POST['confirm'] == 'yes') {
                        $questlist = unserialize(stripslashes($_POST['questlist']));
                        foreach ($questlist as $id) {
                            require_once (HOMEPATH . "/factory/MissionFactory.inc.php");
                            MissionFactory::deletequest($id, $db);
                        }
                        $str .= "<tr><td>Les quêtes sélectionnées ont normalement été supprimées.</td></td>";
                    } else
                        $str .= "<td><td>Rien n'a été changé.</td></td>";
                    
                    $str .= "</table>";
                    return $str;
                } elseif (isset($_POST['delete'])) {
                    
                    $questlist = unserialize(base64_decode($_POST['questlist']));
                    
                    $str = "<form name='form' action='?center=questpanel&panel=2' method=post target='_self'>\n";
                    $str .= "<table class='maintable " . $this->style . "'>\n";
                    $str .= "<tr>\n";
                    $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('SUPPRESSION DE QUÊTES') . "</b></td>\n";
                    $str .= "</tr>\n";
                    $str .= "</table>";
                    $str .= "<table class='maintable " . $this->style . "'>\n";
                    $str .= "<tr><td class='mainbgbody'>Êtes-vous sûr de vouloir supprimer ces quêtes ?</td>";
                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='confirm' value='yes' id='yes' />" . "<label for='yes'>Oui, suppression.</label></td></tr>";
                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='confirm' value='no' id='no' />" . "<label for='no'>Non, on garde.</label></td></tr>";
                    $str .= "</table>";
                    
                    $questidlist = array();
                    
                    $str .= "<table class='maintable " . $this->style . "'>\n";
                    $str .= "<tr>\n";
                    $str .= "<td class='mainbglabel' width='100px' align='center'>" . localize('id') . "</td>\n";
                    $str .= "<td class='mainbglabel' width='300px' align='center'>" . localize('Nom') . "</td>\n";
                    $str .= "<td class='mainbglabel' align='center'>" . localize('Level Min') . "</td>\n";
                    $str .= "</tr></table>";
                    $str .= "<table class='maintable " . $this->style . "'>\n";
                    foreach ($questlist as $arr) {
                        if (array_key_exists("quest" . $arr["id"], $_POST)) {
                            $str .= "<tr><td class='mainbgbody' width='100px' align='left'> " . $arr["id"] . " </td>\n";
                            $str .= "<td class='mainbgbody' width='300px' align='left'><label for='quest" . $arr["id"] . "'> " . $arr["name"] . "</label> </td>";
                            $str .= "<td class='mainbgbody'  align='left'> " . $arr["lvlmin"] . " </td>";
                            $str .= "</tr>\n";
                            
                            $questidlist[] = $arr["id"];
                        }
                    }
                    $str .= "</table>";
                    
                    $str .= "<input type='hidden' name='questlist' value='" . serialize($questidlist) . "'/>";
                    $str .= "<input type='submit' name='ok' value='Ok'/>";
                    $str .= "</form>";
                    
                    return $str;
                }
                break;
            
            case 3: // --------------------- VOIR LES DETAILS --------------
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr>\n";
                $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('DÉTAILS DE LA QUÊTE') . "</b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>";
                $str .= "<table class='maintable " . $this->style . "'>\n";
                
                $questlist = unserialize(base64_decode($_POST['questlist']));
                $nb = 0;
                foreach ($questlist as $arr) // très moche, flemme....
{
                    if (array_key_exists("quest" . $arr["id"], $_POST)) {
                        $id_quest = $arr["id"];
                        
                        $dbq = new DBCollection("SELECT id,name,nbMission,goal,Player_levelMin,Player_levelMax FROM Quest WHERE id=" . $id_quest, $db, 0, 0);
                        
                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de la quête.</td>\n";
                        $str .= "<td class='mainbglabel'  align='center'>" . $dbq->get("name") . "</td></tr>\n";
                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nombre de missions.</td>\n";
                        $str .= "<td class='mainbglabel'  align='left'>" . $dbq->get("nbMission") . "</td></tr>\n";
                        $str .= "<tr><td class='mainbglabel'  align='center'>Niveau minimum.</td>\n";
                        $str .= "<td class='mainbglabel' align='left'>" . $dbq->get("Player_levelMin") . "</td></tr>\n";
                        $str .= "<tr><td class='mainbglabel'  align='center'>Niveau maximum.</td>\n";
                        $str .= "<td class='mainbglabel'  align='left'>" . $dbq->get("Player_levelMax") . "</td></tr>\n";
                        $str .= "<tr><td class='mainbglabel'  align='center'>But de la quête.</td>\n";
                        $str .= "<td class='mainbglabel'  align='center'>" . $dbq->get("goal") . "</td></tr>\n";
                        
                        $dbm = new DBCollection("SELECT id,id_BasicMission,name,RP_Mission FROM Mission WHERE id_Quest=" . $id_quest . " ORDER BY id DESC", $db, 0, 0);
                        $nbm = 1;
                        while (! $dbm->eof()) {
                            $idm = $dbm->get("id");
                            $str .= "<tr><td  align='center'><b>Mission n°" . $nbm . " (id: " . $idm . ")</b></td></tr>\n";
                            
                            $str .= "<tr><td class='mainbglabel' align='center'>Titre de la mission.</td>\n";
                            $str .= "<td class='mainbglabel'  align='center'>" . $dbm->get("name") . "</td></tr>\n";
                            $str .= "<tr><td class='mainbglabel'  align='center'>Role-Play de la mission.</td>\n";
                            $str .= "<td class='mainbglabel'  align='center'>" . $dbm->get("RP_Mission") . "</td></tr>\n";
                            
                            $str .= "<tr><td class='mainbglabel' align='center'>Type de Mission</td>\n";
                            switch ($dbm->get("id_BasicMission")) {
                                case 1:
                                    $str .= "<td class='mainbglabel'  align='center'>Escorte</td></tr>\n";
                                    
                                    $str .= "<tr><td class='mainbglabel' align='center'>Race du personnage à escorter</td>\n";
                                    $dbp = new DBCollection("SELECT racename FROM Player WHERE id_Mission=" . $idm, $db, 0, 0);
                                    $str .= "<td class='mainbglabel'  align='center'>" . $dbp->get("racename") . "</td></tr>\n";
                                    break;
                                
                                case 4:
                                    $str .= "<td class='mainbglabel'  align='center'>Trésor</td></tr>\n";
                                    break;
                                
                                case 5:
                                    $str .= "<td class='mainbglabel'  align='center'>Tuer des Monstres</td></tr>\n";
                                    $str .= "<tr><td class='mainbglabel' align='center'>Liste des monstres à tuer</td>\n";
                                    $dbp = new DBCollection("SELECT racename,level FROM Player WHERE id_Mission=" . $idm, $db, 0, 0);
                                    while (! $dbp->eof()) {
                                        $str .= "<td class='mainbglabel'  align='center'>" . $dbp->get("racename") . " (niveau: " . $dbp->get("level") . ")</td></tr><tr><td></td>\n";
                                        $dbp->next();
                                    }
                                    $str .= "</tr>";
                                    break;
                                
                                case 6:
                                    $str .= "<td class='mainbglabel'  align='center'>Transport d'objet</td></tr>\n";
                                    
                                    $dbe = new DBCollection("SELECT name,extraname FROM Equipment WHERE id_Mission=" . $idm . " ORDER BY id ASC", $db, 0, 0);
                                    $str .= "<tr><td class='mainbglabel' align='center'>Objet à transporter</td>\n";
                                    $str .= "<td class='mainbglabel'  align='center'>" . $dbe->get("name") . " " . $dbe->get("extraname") . "</td></tr>\n";
                                    break;
                                
                                case 7:
                                    $str .= "<td class='mainbglabel'  align='center'>Énigme</td></tr>\n";
                                    $dbmc = new DBCollection("SELECT EnigmAnswer FROM MissionCondition WHERE idMission=" . $idm, $db, 0, 0);
                                    $str .= "<tr><td class='mainbglabel' align='center'>Solution de l'énigme</td>\n";
                                    $str .= "<td class='mainbglabel'  align='center'>" . $dbmc->get("EnigmAnswer") . "</td></tr>\n";
                                    break;
                            }
                            
                            $dbmc = new DBCollection("SELECT distanceX,distanceY FROM MissionCondition WHERE idMission=" . $idm, $db, 0, 0);
                            $str .= "<tr><td class='mainbglabel' align='center'>Distance en X</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $dbmc->get("distanceX") . "</td></tr>\n";
                            $str .= "<tr><td class='mainbglabel' align='center'>Distance en Y</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $dbmc->get("distanceY") . "</td></tr>\n";
                            
                            $dbmr = new DBCollection("SELECT moneyStop,xpStop,NameEquipStop,ExtranameEquipStop FROM MissionReward WHERE idMission=" . $idm, $db, 0, 0);
                            $str .= "<tr><td class='mainbglabel' align='center'>Récompenses</td></tr>\n";
                            $str .= "<tr><td class='mainbglabel' align='center'>Pièces d'Or</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $dbmr->get("moneyStop") . "</td></tr>\n";
                            $str .= "<tr><td class='mainbglabel' align='center'>Expérience</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $dbmr->get("xpStop") . "</td></tr>\n";
                            $str .= "<tr><td class='mainbglabel' align='center'>Objet gagné</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $dbmr->get("NameEquipStop") . " " . $dbmr->get("ExtranameEquipStop") . "</td></tr>\n";
                            
                            $nbm ++;
                            $dbm->next();
                        }
                        $nb ++;
                    }
                    if ($nb > 0)
                        break;
                }
                $str .= "</table>";
                
                return $str;
                break;
            
            case 4: // ---------------------- CRÉATION ----------------------------------------------
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr>\n";
                $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE QUÊTE') . "</b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>";
                
                // --------------- Première étape : nombre de mission et détails
                
                if (isset($_POST['create'])) {
                    $str .= "<form name='form' action='?center=questpanel&panel=4' method=post target='_self'>\n";
                    $str .= "<table class='maintable " . $this->style . "'>\n";
                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='questname' >Nom de la quête</label></td>\n";
                    $str .= "<td class='mainbglabel'  align='left'><input type='text' name='questname' id='questname' /></td></tr>\n";
                    
                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='questRP' >But de la quête</label></td>\n";
                    $str .= "<td class='mainbglabel'  align='left'><textarea name='questRP' id='questRP' cols=50 rows=15></textarea></td></tr>\n";
                    
                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='nbmissions' >Nombre de Missions</label></td>\n";
                    $str .= "<td class='mainbglabel'  align='left'><select name='nbmissions'>";
                    for ($j = 1; $j <= 50; $j ++) {
                        $str .= "<option value='" . $j . "'>" . $j . "</option>";
                    }
                    $str .= "</select></td></tr>";
                    
                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='questlvl' >Niveau moyen des aventuriers qui peuvent tenter cette quête</label></td>\n";
                    $str .= "<td class='mainbglabel'  align='left'><select name='questlvl'>";
                    for ($j = 1; $j <= 150; $j ++) {
                        $str .= "<option value='" . $j . "'>" . $j . "</option>";
                    }
                    $str .= "</select></td></tr>";
                    
                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='nbcopies' >Nombre de copies qui vont être générées</label></td>\n";
                    $str .= "<td class='mainbglabel'  align='left'><select name='nbcopies'>";
                    for ($j = 1; $j <= 50; $j ++) {
                        $str .= "<option value='" . $j . "'>" . $j . "</option>";
                    }
                    $str .= "</select></td></tr>";
                    
                    $str .= "</table>";
                    $str .= "<input type='hidden' name='step' value='1'/>";
                    $str .= "<input type='submit' name='ok' value='Valider'/>";
                    $str .= "</form>";
                    
                    return $str;
                } elseif (isset($_POST['step'])) // ---- 2eme étape : type des missions--------------
{
                    switch ($_POST['step']) {
                        case 1:
                            $str .= "<form name='form' action='?center=questpanel&panel=4' method=post target='_self'>\n";
                            
                            for ($i = 1; $i <= $_POST['nbmissions']; $i ++) {
                                $str .= "<table class='maintable " . $this->style . "'>\n";
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Type de la mission n°" . $i . "</td>\n";
                                $str .= "<td class='mainbglabel' align='left'><input type='radio' name='type" . $i . "' value=0 id='treasure' />";
                                $str .= "<label for='tresor'>Trésor</label></td></tr>\n";
                                $str .= "<tr><td></td><td class='mainbglabel' align='left'><input type='radio' name='type" . $i . "' value=1 id='escort' />";
                                $str .= "<label for='escort'>Escorter</label></td></tr>\n";
                                $str .= "<tr><td></td><td class='mainbglabel' align='left'><input type='radio' name='type" . $i . "' value=2 id='transport' />";
                                $str .= "<label for='transport'>Transporter un objet</label></td></tr>\n";
                                $str .= "<tr><td></td><td class='mainbglabel' align='left'><input type='radio' name='type" . $i . "' value=3 id='kill' />";
                                $str .= "<label for='kill'>Tuer des Monstres</label></td></tr>\n";
                                $str .= "<tr><td></td><td class='mainbglabel' align='left'><input type='radio' name='type" . $i . "' value=4 id='riddle' />";
                                $str .= "<label for='riddle'>Énigme</label></td></tr>\n";
                                $str .= "</table>";
                            }
                            
                            $QuestName = base64_encode($_POST["questname"]);
                            $QuestRP = base64_encode($_POST["questRP"]);
                            
                            $str .= "<input type='hidden' name='questname' value='" . $QuestName . "'/>";
                            $str .= "<input type='hidden' name='questRP' value='" . $QuestRP . "'/>";
                            $str .= "<input type='hidden' name='nbmissions' value='" . $_POST["nbmissions"] . "'/>";
                            $str .= "<input type='hidden' name='questlvl' value='" . $_POST["questlvl"] . "'/>";
                            $str .= "<input type='hidden' name='nbcopies' value='" . $_POST["nbcopies"] . "'/>";
                            $str .= "<input type='hidden' name='step' value='2'/>";
                            $str .= "<input type='submit' name='ok' value='Valider'/>";
                            $str .= "</form>";
                            
                            return $str;
                            break;
                        
                        case 2: // ------ 3eme étape: détails des missions------------------------
                            
                            $str .= "<table class='maintable " . $this->style . "'>\n";
                            $str .= "<tr><td class='mainbglabel' align='center'>ATTENTION, pour le nom du perso à escorter et les compléments de nom des objets gagner et à transporter, on peut mettre des apostrophes, mais pas des guillemets.</td></tr>\n";
                            $str .= "</table>";
                            
                            $str .= "<form name='form' action='?center=questpanel&panel=4' method=post target='_self'>\n";
                            
                            for ($i = 1; $i <= $_POST['nbmissions']; $i ++) {
                                $str .= "<table class='maintable " . $this->style . "'>\n";
                                $str .= "<tr><td  align='center'><b>MISSION N°" . $i . " </b></td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='lvl" . $i . "' >Niveau de la mission</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><select name='lvl" . $i . "'>";
                                for ($j = 1; $j <= 10; $j ++) {
                                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                                }
                                $str .= "</select></td></tr>";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='name" . $i . "' >Nom de la mission</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><input type='text' name='name" . $i . "' id='name" . $i . "' /></td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='RP" . $i . "' >RP de la mission</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><textarea name='RP" . $i . "' id='RP" . $i . "' cols=50 rows=15></textarea></td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='distanceX" . $i . "' >Distance en X entre le but et le point de départ</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><select name='distanceX" . $i . "'>";
                                for ($j = - 50; $j <= 50; $j ++) {
                                    if ($j != 0)
                                        $str .= "<option value='" . $j . "'>" . $j . "</option>";
                                }
                                $str .= "</select></td></tr>";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='distanceY" . $i . "' >Distance en Y entre le but et le point de départ</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><select name='distanceY" . $i . "'>";
                                for ($j = - 50; $j <= 50; $j ++) {
                                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                                }
                                $str .= "</select></td></tr>";
                                
                                $dbe = new DBCollection("SELECT name,id FROM BasicEquipment", $db, 0, 0);
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='EquipWBasicId" . $i . "' >Nom de l'équipement gagné</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><select name='EquipWBasicId" . $i . "'><option value='none'> Aucun </option>";
                                while (! $dbe->eof()) {
                                    $str .= "<option value='" . $dbe->get("id") . "'>" . $dbe->get("name") . "</option>";
                                    $dbe->next();
                                }
                                $str .= "</select></td></tr>";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='equipwlvl" . $i . "' >Niveau de l'équipement gagné</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><select name='equipwlvl" . $i . "'>";
                                for ($j = 1; $j <= 14; $j ++) {
                                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                                }
                                $str .= "</select></td></tr>";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='wextraname" . $i . "' >Complément de nom de l'objet gagné</label></td>\n";
                                $str .= "<td class='mainbglabel'  align='left'><input type='text' name='wextraname" . $i . "' id='wextraname" . $i . "' /></td></tr>\n";
                                
                                switch ($_POST["type" . $i]) {
                                    case 1:
                                        $dbe = new DBCollection("SELECT name,id FROM BasicRace WHERE id<5", $db, 0, 0);
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='idraceEscort" . $i . "' >Nom de la race du perso à escorter</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><select name='idraceEscort" . $i . "'>";
                                        while (! $dbe->eof()) {
                                            $str .= "<option value='" . $dbe->get("id") . "'>" . $dbe->get("name") . "</option>";
                                            $dbe->next();
                                        }
                                        $str .= "</select></td></tr>";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='EscortName" . $i . "' >Nom du perso à escorter(HORS FONCTION POUR LE MOMENT)</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><input type='text' name='EscortName" . $i . "' id='EscortName" . $i . "' /></td></tr>\n";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='EscortGender" . $i . "' >Sexe du perso à escorter</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><select name='EscortGender" . $i . "'>";
                                        $str .= "<option value='M'>Homme</option>";
                                        $str .= "<option value='F'>Femme</option>";
                                        $str .= "</select></td></tr>";
                                        break;
                                    
                                    case 2:
                                        $dbe = new DBCollection("SELECT name,id FROM BasicEquipment", $db, 0, 0);
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='EquipBasicId" . $i . "' >Nom de l'objet à transporter</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><select name='EquipBasicId" . $i . "'>";
                                        while (! $dbe->eof()) {
                                            $str .= "<option value='" . $dbe->get("id") . "'>" . $dbe->get("name") . "</option>";
                                            $dbe->next();
                                        }
                                        $str .= "</select></td></tr>";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='extraname" . $i . "' >Complément de nom de l'objet à transporter </label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><input type='text' name='extraname" . $i . "' id='extraname" . $i . "' /></td></tr>\n";
                                        break;
                                    
                                    case 3:
                                        $dbr = new DBCollection("SELECT name,id FROM BasicRace", $db, 0, 0);
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='idracePNJ" . $i . "' >Race des PNJ à tuer</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><select name='idracePNJ" . $i . "'>";
                                        while (! $dbr->eof()) {
                                            $str .= "<option value='" . $dbr->get("id") . "'>" . $dbr->get("name") . "</option>";
                                            $dbr->next();
                                        }
                                        $str .= "</select></td></tr>";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='nbcible" . $i . "' >Nombre de PNJ à tuer</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><select name='nbcible" . $i . "'>";
                                        for ($j = 1; $j <= 10; $j ++) {
                                            $str .= "<option value='" . $j . "'>" . $j . "</option>";
                                        }
                                        $str .= "</select></td></tr>";
                                        break;
                                    
                                    case 4:
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'><label for='answer" . $i . "' >Solution de l'énigme, (en minuscules)</label></td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'><input type='text' name='answer" . $i . "' id='answer" . $i . "' /></td></tr>\n";
                                        break;
                                    
                                    default:
                                        break;
                                }
                                $str .= "</table>";
                            }
                            
                            $str .= "<input type='hidden' name='questname' value='" . $_POST["questname"] . "'/>";
                            $str .= "<input type='hidden' name='questRP' value='" . $_POST["questRP"] . "'/>";
                            $str .= "<input type='hidden' name='nbmissions' value='" . $_POST["nbmissions"] . "'/>";
                            $str .= "<input type='hidden' name='questlvl' value='" . $_POST["questlvl"] . "'/>";
                            $str .= "<input type='hidden' name='nbcopies' value='" . $_POST["nbcopies"] . "'/>";
                            for ($i = 1; $i <= $_POST['nbmissions']; $i ++) {
                                $str .= "<input type='hidden' name='type" . $i . "' value='" . $_POST["type" . $i] . "'/>";
                            }
                            $str .= "<input type='hidden' name='step' value='3'/>";
                            $str .= "<input type='submit' name='ok' value='Valider'/>";
                            $str .= "</form>";
                            
                            return $str;
                            break;
                        
                        case 3: // ------------- 4eme étape: VERIFICATION DU CONTENU------------
                            $str .= "<form name='form' action='?center=questpanel&panel=4' method=post target='_self'>\n";
                            $str .= "<table class='maintable " . $this->style . "'>\n";
                            
                            $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nombre de copies de la quête</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $_POST["nbcopies"] . "</td></tr>\n";
                            
                            $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de la quête</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . stripslashes(base64_decode($_POST["questname"])) . "</td></tr>\n";
                            
                            $str .= "<tr><td class='mainbglabel' width='200px' align='center'>But de la quête</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . stripslashes(base64_decode($_POST["questRP"])) . "</td></tr>\n";
                            
                            $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Quête destinée à des persos de niveau</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $_POST["questlvl"] . "</td></tr>\n";
                            
                            $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nombre de missions dans la quête</td>\n";
                            $str .= "<td class='mainbglabel'  align='left'>" . $_POST["nbmissions"] . "</td></tr>\n";
                            
                            $str .= "</table>";
                            
                            for ($i = 1; $i <= $_POST['nbmissions']; $i ++) {
                                $str .= "<table class='maintable " . $this->style . "'>\n";
                                $str .= "<tr><td  align='center'><b>MISSION N°" . $i . " </b></td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Niveau de la mission</td>\n";
                                $str .= "<td class='mainbglabel'  align='left'>" . $_POST["lvl" . $i] . "</td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de la mission</td>\n";
                                $str .= "<td class='mainbglabel'  align='left'>" . stripslashes($_POST["name" . $i]) . "</td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>RP de la mission</td>\n";
                                $str .= "<td class='mainbglabel'  align='left'>" . stripslashes($_POST["RP" . $i]) . "</td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Distance en X entre le but et le point de départ</td>\n";
                                $str .= "<td class='mainbglabel'  align='left'>" . $_POST["distanceX" . $i] . "</td></tr>\n";
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Distance en Y entre le but et le point de départ</td>\n";
                                $str .= "<td class='mainbglabel'  align='left'>" . $_POST["distanceY" . $i] . "</td></tr>\n";
                                
                                if ($_POST["EquipWBasicId$i"] != 'none') {
                                    $dbe = new DBCollection("SELECT name FROM BasicEquipment WHERE id=" . $_POST["EquipWBasicId$i"], $db, 0, 0);
                                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de l'équipement gagné</td>\n";
                                    $str .= "<td class='mainbglabel'  align='left'>" . $dbe->get("name") . "</td></tr>\n";
                                    
                                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Niveau de l'équipement gagné</td>\n";
                                    $str .= "<td class='mainbglabel'  align='left'>" . $_POST["equipwlvl" . $i] . "</td></tr>\n";
                                    
                                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Complément de nom de l'objet gagné</td>\n";
                                    $str .= "<td class='mainbglabel'  align='left'>" . stripslashes($_POST["wextraname" . $i]) . "</td></tr>\n";
                                } else {
                                    $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de l'équipement gagné</td>\n";
                                    $str .= "<td class='mainbglabel'  align='left'>Aucun...</td></tr>\n";
                                }
                                
                                $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Mission de type</td>\n";
                                
                                switch ($_POST["type" . $i]) {
                                    case 0:
                                        $str .= "<td class='mainbglabel'  align='left'>Trésor</td></tr>\n";
                                        break;
                                    
                                    case 1:
                                        $str .= "<td class='mainbglabel'  align='left'>Escorte</td></tr>\n";
                                        
                                        $dbr = new DBCollection("SELECT name FROM BasicRace WHERE id=" . $_POST["idraceEscort" . $i], $db, 0, 0);
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de la race du perso à escorter</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . $dbr->get("name") . "</td></tr>\n";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom du perso à escorter</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . stripslashes($_POST["EscortName" . $i]) . "</td></tr>\n";
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Sexe du perso à escorter</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . stripslashes($_POST["EscortGender" . $i]) . "</td></tr>\n";
                                        break;
                                    
                                    case 2:
                                        $str .= "<td class='mainbglabel'  align='left'>Transport</td></tr>\n";
                                        
                                        $dbe = new DBCollection("SELECT name FROM BasicEquipment WHERE id=" . $_POST["EquipBasicId" . $i], $db, 0, 0);
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nom de l'objet à transporter</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . $dbe->get("name") . "</td></tr>\n";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Complément de nom de l'objet à transporter</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . stripslashes($_POST["extraname" . $i]) . "</td></tr>\n";
                                        break;
                                    
                                    case 3:
                                        $str .= "<td class='mainbglabel'  align='left'>Tuer des PNJ</td></tr>\n";
                                        
                                        $dbr = new DBCollection("SELECT name,id FROM BasicRace WHERE id=" . $_POST["idracePNJ" . $i], $db, 0, 0);
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Race des PNJ à tuer</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . $dbr->get("name") . "</td></tr>\n";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Nombre de PNJ à tuer</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . $_POST["nbcible" . $i] . "</td></tr>\n";
                                        break;
                                    
                                    case 4:
                                        $str .= "<td class='mainbglabel'  align='left'>Énigme</td></tr>\n";
                                        
                                        $str .= "<tr><td class='mainbglabel' width='200px' align='center'>Solution de l'énigme</td>\n";
                                        $str .= "<td class='mainbglabel'  align='left'>" . $_POST["answer" . $i] . "</td></tr>\n";
                                        break;
                                    
                                    default:
                                        break;
                                }
                                $str .= "</table>";
                            }
                            
                            $str .= "<input type='hidden' name='questname' value='" . $_POST["questname"] . "'/>";
                            $str .= "<input type='hidden' name='questRP' value='" . $_POST["questRP"] . "'/>";
                            $str .= "<input type='hidden' name='nbmissions' value='" . $_POST["nbmissions"] . "'/>";
                            $str .= "<input type='hidden' name='questlvl' value='" . $_POST["questlvl"] . "'/>";
                            $str .= "<input type='hidden' name='nbcopies' value='" . $_POST["nbcopies"] . "'/>";
                            for ($i = 1; $i <= $_POST['nbmissions']; $i ++) {
                                $str .= "<input type='hidden' name='type" . $i . "' value='" . $_POST["type" . $i] . "'/>";
                                $str .= "<input type='hidden' name='lvl" . $i . "' value='" . $_POST["lvl" . $i] . "'/>";
                                $str .= "<input type='hidden' name='name" . $i . "' value='" . base64_encode($_POST["name" . $i]) . "'/>";
                                $str .= "<input type='hidden' name='RP" . $i . "' value='" . base64_encode($_POST["RP" . $i]) . "'/>";
                                $str .= "<input type='hidden' name='distanceX" . $i . "' value='" . $_POST["distanceX" . $i] . "'/>";
                                $str .= "<input type='hidden' name='distanceY" . $i . "' value='" . $_POST["distanceY" . $i] . "'/>";
                                $str .= "<input type='hidden' name='EquipWBasicId" . $i . "' value='" . $_POST["EquipWBasicId" . $i] . "'/>";
                                $str .= "<input type='hidden' name='wextraname" . $i . "' value='" . base64_encode($_POST["wextraname" . $i]) . "'/>";
                                $str .= "<input type='hidden' name='equipwlvl" . $i . "' value='" . $_POST["equipwlvl" . $i] . "'/>";
                                switch ($_POST["type" . $i]) {
                                    case 1:
                                        $str .= "<input type='hidden' name='idraceEscort" . $i . "' value='" . $_POST["idraceEscort" . $i] . "'/>";
                                        $str .= "<input type='hidden' name='EscortGender" . $i . "' value='" . $_POST["EscortGender" . $i] . "'/>";
                                        $str .= "<input type='hidden' name='EscortName" . $i . "' value='" . base64_encode($_POST["EscortName" . $i]) . "'/>";
                                        break;
                                    
                                    case 2:
                                        $str .= "<input type='hidden' name='EquipBasicId" . $i . "' value='" . $_POST["EquipBasicId" . $i] . "'/>";
                                        $str .= "<input type='hidden' name='extraname" . $i . "' value='" . base64_encode($_POST["extraname" . $i]) . "'/>";
                                        break;
                                    
                                    case 3:
                                        $str .= "<input type='hidden' name='idracePNJ" . $i . "' value='" . $_POST["idracePNJ" . $i] . "'/>";
                                        $str .= "<input type='hidden' name='nbcible" . $i . "' value='" . $_POST["nbcible" . $i] . "'/>";
                                        break;
                                    
                                    case 4:
                                        $str .= "<input type='hidden' name='answer" . $i . "' value='" . $_POST["answer" . $i] . "'/>";
                                        break;
                                    
                                    default:
                                        break;
                                }
                            }
                            
                            $str .= "<input type='hidden' name='step' value='4'/>";
                            $str .= "<input type='submit' name='ok' value='Valider'/>";
                            $str .= "</form>";
                            
                            return $str;
                            break;
                        
                        case 4: // ------------- 5eme étape: GENERATION DES QUÊTES------------
                            $str .= "<table class='maintable " . $this->style . "'>\n";
                            
                            $tabf = serialize($_POST);
                            
                            for ($i = 0; $i < $_POST["nbcopies"]; $i ++) {
                                require_once (HOMEPATH . "/factory/MissionFactory.inc.php");
                                if (MissionFactory::genInListParchment($tabf, $db) == true)
                                    $str .= "<tr><td>Quête crée avec les paramètres indiqués.</tr></td>";
                                else
                                    $str .= "<tr><td>erreur inconnue</tr></td>";
                            }
                            $str .= "</table>";
                            return $str;
                            break;
                        
                        default:
                            break;
                    } // fin du switch des étapes de création de la quête
                } // le else de la première étape, à virer un jour
                break;
            
            default:
                break;
        } // fin du switch des panneaux
    } // fin de tostring
} // fin de la classe
?>

 
      
