<?php

class CQLeaveBuilding extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQLeaveBuilding($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        $state = $curplayer->get("state");
        // calcul du nombre de PA d'un déplacement
        $PA = getTimetoMove($curplayer->get("x"), $curplayer->get("y"), $curplayer->get("map"), $db);
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='Course Celeste' AND id_Player=" . $curplayer->get("id"), $this->db);
        if (! $dbbm->eof())
            $PA = 1;
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='Blessure gênante' AND id_Player=" . $curplayer->get("id"), $this->db);
        if (! $dbbm->eof())
            $PA += 1;
        if ($curplayer->get("ap") < $PA or ($state == "creeping" and $curplayer->get("ap") < 5)) {
            $str = "<table class='maintable bottomleftareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser un déplacement.");
            $str .= "</td></tr></table>";
        } else {
            require_once (HOMEPATH . "/lib/MapInfo.inc.php");
            $mapinfo = new MapInfo($this->db);
            $xp = $curplayer->get("x");
            $yp = $curplayer->get("y");
            $validzone = $mapinfo->getValidMap($xp, $yp, 2, $curplayer->get("map"));
            require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
            $str .= "<table class='maintable'>";
            for ($j = 0; $j < 3; $j ++) {
                
                $str .= "<tr class='tr_move'>";
                if ($j == 0) {
                    $str .= "<td width='175px' rowspan=3>&nbsp;";
                    $str .= "</td>";
                }
                for ($i = 0; $i < 3; $i ++) {
                    
                    if (BasicActionFactory::freePlace($xp - 1 + $i, $yp - 1 + $j, $curplayer->get("map"), $this->db))
                        $CoordEnable = "";
                    else
                        $CoordEnable = "disabled";
                    
                    if (! ($i == 0 && $j == 0) && ! ($i == 2 && $j == 2)) {
                        if ($validzone[$i][$j])
                            $str .= "<td class='mainbgtitle' width='125px'><div class='radio_move'><input type='radio' name='XYNEW'  value='" . ($xp - 1 + $i) . "," . ($yp - 1 + $j) . "'" . $CoordEnable . "/>(" . ($xp - 1 + $i) . ", " . ($yp - 1 + $j) . ")</div></td>";
                        else
                            $str .= "<td class='mainbgtitle' width='125px'><div class='radio_move'><input type='radio' name='XYNEW'  value='" . ($xp - 1 + $i) . "," . ($yp - 1 + $j) . "'" . $CoordEnable . "/>( - , - )</div></td>";
                    }
                }
                
                if ($j == 2) {
                    $str .= "<td>";
                    $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                    $str .= "<input name='action' type='hidden' value='" . LEAVE_BUILDING . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</td>";
                } else {
                    $str .= "<td>&nbsp;</td>";
                }
            }
            
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        
        return $str;
    }
}
?>
