<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQBasicshop extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQBasicshop($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        
        $str = "";
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        elseif (isset($_POST["type"]))
            $type = $_POST["type"];
        else
            $type = 1;
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('A R M U R E R I E &nbsp;&nbsp;&nbsp; S T A N D A R D') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        
        if (isset($_POST["Buy"]) && isset($_POST["IdBasic"])) {
            require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
            $dbbe = new DBCollection("SELECT id,name, id_EquipmentType, frequency  FROM BasicEquipment WHERE id=" . quote_smart($_POST["IdBasic"]), $db);
            // $dbe=new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND state='Carried' AND weared='No' AND id_Player=".$id,$db);
            $price = $dbbe->get("frequency") + floor(
                $dbbe->get("frequency") * (CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), BASIC_SHOP_BUY, $db) - 10) / 100);
            
            if ($curplayer->get("money") < $price)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez d'or pour acheter cet objet.</td></tr>";
            elseif (PlayerFactory::checkingInvFullForObject($curplayer, $dbbe->get("id_EquipmentType"), $param, $db) == - 1)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez de place dans vos sacs.</td></tr>";
            else {
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr><td class='maintable'>Vous allez acheter un(e) " . $dbbe->get("name") . " pour " . $price . " PO ?";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' /> </td></tr>";
                $str .= "<input name='action' type='hidden' value='" . BASIC_SHOP_BUY . "' />";
                $str .= "<input name='ID_BASIC' type='hidden' value='" . $dbbe->get("id") . "' />";
                $str .= "<input name='type' type='hidden' value='" . $type . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</table></form>";
            }
        } else {
            if ($type == 1)
                $cond = "(EquipmentType.id<8 OR EquipmentType.id in (28,21,22))";
            if ($type == 2)
                $cond = "(EquipmentType.id>7 AND EquipmentType.id<28 AND EquipmentType.id not in (21,22))";
            if ($type == 3)
                $cond = "EquipmentType.id = 29";
            if ($type == 4)
                $cond = "EquipmentType.id in (40,41,42,43,44,46)";
            
            $basicequip = new BasicEquipment();
            $eqmodifier = new Modifier();
            
            $dbe = new DBCollection(
                "SELECT " . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," . $basicequip->getASRenamer("BasicEquipment", "BE") .
                     "  FROM BasicEquipment  LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON BasicEquipment.id_EquipmentType=EquipmentType.id  WHERE " .
                     $cond . " order by BEname", $db);
            
            $array = array();
            
            while (! $dbe->eof()) {
                
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel(1);
                $basicequip->DBLoad($dbe, "BE");
                
                $checkbox = "<input type='radio' name='IdBasic' value='" . $basicequip->get("id") . "'>";
                
                // Affichage commun de tous les équipements : name - extraname s'il existe - level - id
                $name = "<b>" . localize($basicequip->get("name")) . "</b>";
                $name .= "<br/>" . localize("Niveau 1");
                
                // Affichages des bonus de caractéristique pour les équipements
                if ($basicequip->get("id_EquipmentType") < 30) {
                    $name .= "<br/>";
                    $eqmodifier->initCharacStr();
                    $modstr = "";
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0")
                            $modstr .= translateAttshort($key) . " : " . $tmp . " | ";
                    }
                    $name .= "(" . substr($modstr, 0, - 3) . ")";
                }
                if (($basicequip->get("id_EquipmentType") > 40 && $basicequip->get("id_EquipmentType") < 44) or $basicequip->get("id_EquipmentType") == 46)
                    $name .= " (4 places)";
                if ($basicequip->get("id_EquipmentType") == 44)
                    $name .= " (8 places)";
                
                $price = $basicequip->get("frequency") +
                     floor($basicequip->get("frequency") * (CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), BASIC_SHOP_BUY, $db) - 10) / 100);
                $LvL = 1;
                
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $price,
                        "class='mainbgbody' align='right'"
                    )
                );
                
                $dbe->next();
            }
            
            // ------------- Affichage -------------------
            
            $target = "/conquest/conquest.php?center=view2d&type=" . $type;
            $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'>\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Armes') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Armures') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=3' class='tabmenu'>" . localize('Potions') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=4' class='tabmenu'>" . localize('Outils') . "</a> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            switch ($type) {
                case 1:
                    $str .= localize("Les Armes ");
                    break;
                case 2:
                    $str .= localize("Les Armures ");
                    break;
                case 3:
                    $str .= localize("Les Potions ");
                    break;
                case 4:
                    $str .= localize("Les Outils ");
                    break;
            }
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $str .= createTable(3, $array, array(), 
                array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("Nom et caractéristiques"),
                        "class='mainbglabel' width='60%' align='left'",
                        "EQ.name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Prix"),
                        "class='mainbglabel' width='15%' align='right'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), "class='maintable insidebuildingleftwidth'", "formid", "order");
            
            $str .= "<input id='Buy' type='submit' name='Buy' value='" . localize("Acheter") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        }
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
