<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
class CQJail extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public $pj;

    public function CQJail($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        // Affichage des gens en prison
        $dbp = new DBCollection("SELECT * FROM Player WHERE inbuilding=" . $curplayer->get("inbuilding") . " AND state='prison'", $db);
        $array = array();
        while (! $dbp->eof()) {
            $dbbm = new DBCollection("SELECT * FROM BM WHERE name='En prison' AND id_Player=" . $dbp->get("id"), $db);
            
            $array[] = array(
                "0" => array(
                    $dbp->get("id"),
                    "class='mainbgbody' align='center'"
                ),
                "1" => array(
                    $dbp->get("name"),
                    "class='mainbgbody' align='left'"
                ),
                "2" => array(
                    date("Y-m-d H:i:s", gmstrtotime($dbbm->get("date"))),
                    "class='mainbgbody' align='right'"
                )
            );
            
            $dbp->next();
        }
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('C E L L U L E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
        $str .= "<td class='mainbgtitle'> Détenus </td>\</tr>\n";
        $str .= createTable(3, $array, array(), array(
            array(
                "",
                "class='mainbglabel' width='5%' align='center'"
            ),
            array(
                localize("Nom "),
                "class='mainbglabel' width='50%' align='left'",
                "EQ.name",
                "mainbglabelhover",
                "mainbglabel"
            ),
            array(
                localize("Date de libération"),
                "class='mainbglabel' width='45%' align='right'",
                "duration",
                "mainbglabelhover",
                "mainbglabel"
            )
        ), "class='maintable insidebuildingleftwidth'", "formid", "order");
        
        // Si la date de libération est dépassée pour le joueur en prison, apparition du bouton pour sortir.
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='En prison' AND id_Player=" . $curplayer->get("id"), $db);
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        if ($curplayer->get("state") == "prison" and gmstrtotime(date("Y-m-d H:i:s", gmstrtotime($dbbm->get("date")))) < gmstrtotime(date("Y-m-d H:i:s"))) {
            $str .= "<input name='action' type='hidden' value='" . EXIT_JAIL . "' />";
            $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Sortir de prison' /></tr></td>";
        }
        $str .= "</table>\n";
        $str .= "</form>\n";
        
        return $str;
    }

    public function build_pj()
    {
        $this->pj = array();
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        $map = $curplayer->get("map");
        
        $idteam = $curplayer->get("id_Team");
        $query = "SELECT
				p.id,p.name,p.id_Team,p.x,p.y,p.hp,p.currhp,
				p.level,p.racename,t.name as alliegance,
				ppj.type as dppj,
				po.type as dpo,
				opj.type as dopj,
				oo.type as doo
			FROM 
				Player as p
				LEFT JOIN Team as t ON t.id=p.id_Team
				LEFT JOIN PJAlliancePJ as ppj ON p.id=ppj.id_Player\$dest and ppj.id_Player\$src='$id'
				LEFT JOIN PJAlliance as po ON p.id_Team=po.id_Team and po.id_Player\$src='$id'
				LEFT JOIN TeamAlliancePJ as opj ON p.id=opj.id_Player and opj.id_Team\$src='$idteam'
				LEFT JOIN TeamAlliance as oo ON p.id_Team=oo.id_Team\$dest and oo.id_Team\$src='$idteam'
			WHERE
				map=" . $map . " AND inbuilding=" . $curplayer->get("inbuilding") . " AND state!='prison' AND room=" . $curplayer->get("room") . " AND hidden=10 AND disabled=0 AND status='PC' 				
			GROUP BY p.id
			ORDER BY alliegance, level DESC";
        
        $dbplayer = new DBCollection($query, $db, 0, 0);
        
        $distVal = 0;
        $state = array(
            localize(", Agonisant"),
            localize(", Gravement blessé(e)"),
            localize(", Blessé(e)"),
            localize(", Légèrement blessé(e)"),
            ""
        );
        
        while (! $dbplayer->eof()) {
            $hp = $dbplayer->get("hp");
            $curstate = min(floor($dbplayer->get("currhp") * 5 / $hp), 4);
            
            $xx = $dbplayer->get("x");
            $yy = $dbplayer->get("y");
            
            $level = $dbplayer->get("level");
            $distVal = max(abs($xp - $xx), abs($yp - $yy));
            $id_Team = $dbplayer->get("id_Team");
            
            if (($value = $dbplayer->get("dppj")) != null) {
                $style = "self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
            } elseif (($value = $dbplayer->get("dopj")) != null) {
                $style = "guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
            } else
                $style = "stylepc";
            
            if (($value = $dbplayer->get("dpo")) != null) {
                $gc = " self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
            } elseif (($value = $dbplayer->get("doo")) != null) {
                $gc = " guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
            } else
                $gc = "styleall";
            
            $gc = ($id_Team == $idteam) ? "myalliegence" : $gc;
            $name = "<a href='../conquest/profile.php?id=" . $dbplayer->get("id") . "' class='$style popupify'>" . $dbplayer->get("name") . "</a>";
            $alliegance = "<a href='../conquest/alliegance.php?id=$id_Team' class='$gc popupify'>" . $dbplayer->get("alliegance") . "</a>";
            $pjtmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "level" => $level,
                "guildcolor" => $gc,
                "dist" => $distVal,
                "name" => $name,
                "race" => $dbplayer->get("racename"),
                "id" => $dbplayer->get("id"),
                "alliegance" => $alliegance,
                "id_Team" => $id_Team,
                "state" => $state[$curstate]
            );
            $this->pj[] = $pjtmp;
            
            $dbplayer->next();
        }
    }
}
?>
