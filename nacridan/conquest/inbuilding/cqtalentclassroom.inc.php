<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQTalentclassroom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $curbuilding;

    public $nacridan;

    public $err;

    public function CQTalentclassroom($building, $nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        $this->curbuilding = $building;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $curbuilding = $this->curbuilding;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        if (isset($_GET["liste"]))
            $liste = $_GET["liste"];
        else
            $liste = 1;
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('S A L L E &nbsp;&nbsp;&nbsp; D E &nbsp;&nbsp;&nbsp; C L A S S E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        if ($curbuilding->get("school") != "")
            $schoolList = unserialize($curbuilding->get("school"));
        else
            $schoolList = array();
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        
        if (isset($_POST["learn"]) && isset($_POST["IDTALENT"])) {
            $reduce = 0;
            $dbBT = new DBCollection("SELECT * FROM BasicTalent WHERE id=" . quote_smart($_POST["IDTALENT"]), $db, 0, 0);
            if ($dbBT->get("type") == 1)
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_0, $db);
            elseif ($dbBT->get("type") == 2)
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_1, $db);
            elseif ($dbBT->get("type") == 3)
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_2, $db);
            elseif ($dbBT->get("type") == 4) {
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_3, $db);
                $reduce = 2;
            }
            $dbT = new DBCollection("SELECT id FROM Talent WHERE id_Player=" . $id . " AND id_BasicTalent=" . quote_smart($_POST["IDTALENT"]), $db, 0, 0);
            if ($dbT->count() > 0 && $dbBT->get("type") != 3)
                $str .= "<tr><td class='mainbglabel'>Vous connaissez déjà ce savoir-faire.</td></tr>";
            elseif ($curplayer->get("money") < $price)
                $str .= "<tr><td class='mainbglabel'>Vous n'avez pas assez de PO.</td></tr>";
            elseif ($curplayer->get("ap") < (LEARN_AP - $curbuilding->get("value") - $reduce))
                $str .= "<tr><td class='mainbglabel'>Vous n'avez pas assez de PA.</td></tr>";
            else {
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                // Cas de l'artisanat apprentissage d'un nouveau patron
                if ($dbBT->get("type") == 3) {
                    switch ($dbBT->get("id")) {
                        case 7:
                            $cond = " id_BasicMaterial=1";
                            break;
                        case 8:
                            $cond = " id_BasicMaterial=2";
                            break;
                        case 9:
                            $cond = " id_BasicMaterial=4";
                            break;
                        case 10:
                            $cond = " id_BasicMaterial=3";
                            break;
                        case 11:
                            $cond = " id_BasicMaterial=5";
                            break;
                        case 12:
                            $cond = " (id_BasicMaterial=6 or id_BasicMaterial=7 or id_BasicMaterial=8) ";
                            break;
                        case 13:
                            $cond = " (id_BasicMaterial=9 or id_BasicMaterial=10)";
                            break;
                    }
                    
                    $str .= "<tr><td colspan=2, class='mainbglabel'>" . $dbBT->get("name") . " pour " . $price . " PO  (et " . (LEARN_AP - $curbuilding->get("value") - $reduce) .
                         " PA)";
                    $str .= "<tr><td colspan=2, class='mainbglabel'>Apprenez une nouvelle pièce à confectionner </td>";
                    $str .= "<tr><td><select class='selector' name='BASICOBJET_ID'>";
                    
                    $dbbe = new DBCollection(
                        "SELECT BasicEquipment.* FROM BasicEquipment 
                        left outer join Knowledge on BasicEquipment.id = Knowledge.id_BasicEquipment and Knowledge.id_Player = " .
                             $curplayer->get("id") . " WHERE " . $cond .
                             " AND Knowledge.id_BasicEquipment is null AND 
                        (id_Modifier_BasicEquipment != 999 OR BasicEquipment.id=300 OR BasicEquipment.id=301 OR BasicEquipment.id=302 OR BasicEquipment.id=303 OR BasicEquipment.id=304)", 
                            $db);
                    
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choississez --") . "</option>";
                    $item[] = array(
                        localize("-- Equipement -- ") => - 1
                    );
                    
                    while (! $dbbe->eof()) {
                        $item[] = array(
                            localize($dbbe->get("name")) => $dbbe->get("id")
                        );
                        $dbbe->next();
                    }
                    
                    if ($dbBT->get("id") == 13) {
                        $item[] = array(
                            localize("-- Enchantement -- ") => - 1
                        );
                        
                        $dbtemp = new DBCollection(
                            "SELECT BasicTemplate.* FROM BasicTemplate left outer join Knowledge on BasicTemplate.id = Knowledge.id_BasicEquipment and Knowledge.id_Player = " .
                                 $curplayer->get("id") . " WHERE Knowledge.id_BasicEquipment is null", $db);
                        while (! $dbtemp->eof()) {
                            
                            $item[] = array(
                                localize($dbtemp->get("name") . "/" . $dbtemp->get("name2")) => $dbtemp->get("id")
                            );
                            $dbtemp->next();
                        }
                    }
                    foreach ($item as $arr)
                        foreach ($arr as $key => $value)
                            if ($value == - 1)
                                $str .= "<optgroup class='group' label='" . $key . "' />";
                            else
                                $str .= "<option value='" . $value . "'>" . $key . "</option>";
                } else {
                    $str .= "<tr><td class='mainbglabel'>Vous allez apprendre le savoir-faire " . $dbBT->get("name") . " pour " . $price . " PO  (et " .
                         (LEARN_AP - $curbuilding->get("value") - $reduce) . " PA) ?";
                    $str .= "<input name='BASICOBJET_ID' type='hidden' value='-1' />";
                }
                $str .= "<td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /> </td></tr>";
                $str .= "<input name='action' type='hidden' value='" . SCHOOL_LEARN_TALENT . "' />";
                $str .= "<input name='IDTALENT' type='hidden' value='" . $_POST["IDTALENT"] . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        } else {
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            if ($curbuilding->get("value") == 0)
                $str .= "<tr><td class='mainbglabel'>Réduction du temps d'apprentissage dans cette école : Aucune.</td></tr>";
            else
                $str .= "<tr><td class='mainbglabel'>Réduction du temps d'apprentissage dans cette école : " . $curbuilding->get("value") . " PA.</td></tr>";
                // $str.="<tr><td class='mainbglabel'>Liste des enseignements : </td></tr>";
            $str .= "<tr><td class='mainbglabel'>Liste des enseignements : ";
            $str .= "<a href='../conquest/conquest.php?center=view2d&liste=1' class='tabmenu'>" . localize('Artisanat') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&liste=2' class='tabmenu'>" . localize('Nature') . "</a> \n";
            $strExtraction = "";
            $strRefining = "";
            $strHandcraft = "";
            $strNature = "";
            
            foreach ($schoolList as $idbasictalent => $type) {
                
                $dbBT = new DBCollection("SELECT * FROM BasicTalent WHERE id=" . $idbasictalent, $db, 0, 0);
                
                if ($type == 1) {
                    $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_0, $db);
                    $strExtraction .= "<tr><td class='mainbgbody'><input type='radio' name='IDTALENT' value='" . $dbBT->get("id") . "' id='" . $dbBT->get("name") .
                         "' /><label for='" . $dbBT->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/talent.php?id=" . $dbBT->get("id") .
                         "\" class='parchment popupify'>" . localize($dbBT->get("name")) . "</a>" . "  (" . $price . " PO)  </label></td></tr>";
                } elseif ($type == 2) {
                    $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_1, $db);
                    $strRefining .= "<tr><td class='mainbgbody'><input type='radio' name='IDTALENT' value='" . $dbBT->get("id") . "' id='" . $dbBT->get("name") . "' /><label for='" .
                         $dbBT->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/talent.php?id=" . $dbBT->get("id") . "\" class='parchment popupify'>" .
                         localize($dbBT->get("name")) . "</a>" . "  (" . $price . " PO)  </label></td></tr>";
                } elseif ($type == 3) {
                    $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_2, $db);
                    $enable = "disabled";
                    
                    if (InsideBuildingFactory::prerequisitesForLearningTalent($curplayer, $dbBT->get("id"), $db))
                        $enable = "";
                    
                    $strHandcraft .= "<tr><td class='mainbgbody'><input type='radio'" . $enable . " name='IDTALENT' value='" . $dbBT->get("id") . "' id='" . $dbBT->get("name") .
                         "' /><label for='" . $dbBT->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/talent.php?id=" . $dbBT->get("id") .
                         "\" class='parchment popupify'>" . localize($dbBT->get("name")) . "</a>" . "  (" . $price . " PO)  </label></td></tr>";
                } elseif ($type == 4) {
                    $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_TALENT_3, $db);
                    $strNature .= "<tr><td class='mainbgbody'><input type='radio' name='IDTALENT' value='" . $dbBT->get("id") . "' id='" . $dbBT->get("name") . "' /><label for='" .
                         $dbBT->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/talent.php?id=" . $dbBT->get("id") . "\" class='parchment popupify'>" .
                         localize($dbBT->get("name")) . "</a>" . "  (" . $price . " PO)  </label></td></tr>";
                }
            }
            
            if ($liste == 1) {
                $str .= "<tr><td class='mainbgbody'><b class='stylepc'>Savoir-faire d'extraction</b> </td></tr>";
                $str .= $strExtraction;
                $str .= "<tr><td class='mainbgbody'><br/> <b class='stylepc'>Savoir-faire de raffinage</b> </td></tr>";
                $str .= $strRefining;
                $str .= "<tr><td class='mainbgbody'><br/> <b class='stylepc'>Savoir-faire d'artisanat</b> </td></tr>";
                $str .= $strHandcraft;
            } elseif ($liste == 2) {
                $str .= "<tr><td class='mainbgbody'><b class='stylepc'>Savoir-faire Connaissance de la nature</b> </td></tr>";
                $str .= $strNature;
            }
            $str .= "<tr><td><input type='submit' name='learn' value='Apprendre'/> </td></tr>";
            $str .= "</form>";
        }
        
        $str .= "</table>\n";
        return $str;
    }
}
?>
