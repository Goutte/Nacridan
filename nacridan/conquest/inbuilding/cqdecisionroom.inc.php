<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQDecisionroom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQDecisionroom($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $idBuilding = $curplayer->get("inbuilding");
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $idBuilding, $db);
        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $dbb->get("id_City"), $db);
        if ($dbc->count() > 0) {
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
        }
        $err = $this->err;
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan=2 class='mainbgtitle'> <b>" . localize('S A L L E &nbsp;&nbsp;&nbsp; D U  &nbsp;&nbsp;&nbsp; G O U V E R N E U R') . "</b> \n";
        $str .= "</td></tr>\n";
        $str .= "<tr>\n";
        $str .= "<tr><td class='mainbgtitle'> GOUVERNEUR : " . $dbc->count() > 0 ? $dbp->get("name") : "";
        $str .= "</td><td class='mainbgtitle'> LOYAUTE : " . $dbc->count() > 0 ? $dbc->get("loyalty") . "%" : "";
        $str .= "</td><tr/></table>\n";
        if ($dbc->count() > 0) {
            $garden = new DBCollection("SELECT * FROM Player WHERE inbuilding='" . $idBuilding . "' AND racename='Garde du Palais' AND status='NPC'", $db);
            
            if (isset($_POST['decision'])) {
                switch ($_POST['decision']) {
                    case 1:
                    case 2:
                        if ($curplayer->get("ap") < PALACE_DECISION_AP) {
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            $str .= "<tr><td class='mainbgbody'> Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                            $str .= "</table>";
                        } elseif ($garden->count() == 0) {
                            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            if ($_POST['decision'] == "1")
                                $str .= "<tr><td class='mainbgbody'>Vous allez oeuvrer pour baisser la loyauté du village envers son protecteur actuel</tr></td>";
                            if ($_POST['decision'] == "2")
                                $str .= "<tr><td class='mainbgbody'>Vous allez oeuvrer pour augmenter la loyauté du village envers son protecteur actuel</tr></td>";
                            
                            $str .= "<tr><td class='mainbgbody'>Cette action va vous couter " . PALACE_DECISION_AP . " PA </tr></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<td><input name='action' type='hidden' value='" . PALACE_DECISION . "' /></td>";
                            $str .= "<input type='hidden' name='deci' value='" . $_POST["decision"] . "'/> ";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                            $str .= "</table>";
                            $str .= "</form>";
                        } elseif ($garden->count() == 1) {
                            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            if ($_POST['decision'] == 2)
                                $str .= "<tr><td class='mainbgbody'> Le pouvoir établie est déjà stable. </tr></td>";
                            if ($_POST['decision'] == 1)
                                $str .= "<tr><td class='mainbgbody'>Le garde du palais vous empêche de réaliser cette action.</tr></td>";
                        }
                        break;
                    case 3:
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr><td class='mainbgbody'>Vous êtes sur le point d'engager un garde pour défendre le palais.</tr></td>";
                        $str .= "<tr><td class='mainbgbody'>Cette action va vous coûter " . PALACE_DECISION_AP . " PA.</tr></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<td><input name='action' type='hidden' value='" . PALACE_DECISION . "' /></td>";
                        $str .= "<input type='hidden' name='deci' value='" . $_POST["decision"] . "'/> ";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        break;
                    
                    case 4:
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr><td colspan=2 class='mainbgbody'>Vous êtes sur le point de lancer un coup d'état pour prendre le village.</tr></td>";
                        $str .= "<tr><td class='mainbgtitle' width='150px'>" . localize("Entrez le nom du chef du putsch :") . "</td>";
                        $str .= "<td class='mainbgtitle'><input name='chef' id='chef' autocomplete='off' maxlength=40 size=40 value='' onKeyUp=\"checkAlliancePJ(this.value,'alliance')\"></td><td>";
                        
                        $str .= "<tr><td colspan=2 class='mainbgbody'>Cette action va vous coûter " . PALACE_DECISION_AP . " PA.</tr></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<td><input name='action' type='hidden' value='" . CREATE_PUTSCH . "' /></td>";
                        $str .= "<input type='hidden' name='deci' value='" . $_POST["decision"] . "'/> ";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        
                        break;
                    case 5:
                        $data = array();
                        $checkbox = array();
                        
                        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db);
                        $city = $dbc->get('id_City');
                        
                        $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $city, $db);
                        $temple = $dbs->get('id');
                        
                        $dbp = new DBCollection("SELECT * FROM Player WHERE resurrectid=" . $temple, $db);
                        
                        while (! $dbp->eof()) {
                            $id = $dbp->get('id');
                            $player = $dbp->get('name');
                            
                            $data[] = array(
                                "id" => $id,
                                "name" => $player
                            );
                            $dbp->next();
                        }
                        
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable'>\n";
                        $str .= "<tr><td class='mainbgbody'>Voici la liste des personnages ressuscitant dans le Temple de votre village : </tr></td>";
                        $str .= "</table>\n";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<td width='30px' align='center'><a href='#' class='all' onclick=\"invertCheckboxes('form'); return false;\"><img src='../pics/misc/all.gif' style='border: 0' /></a></td>\n";
                        $str .= "<td class='mainbglabel' width='100px' align='center'>" . localize('id') . "</td>\n";
                        $str .= "<td class='mainbglabel' width='360px' align='center'>" . localize('Nom') . "</td>\n";
                        
                        foreach ($data as $arr) {
                            $str .= "<tr><td class='mainbgtitle' width='30px' align='center'><input name='id[]' type='checkbox' value='" . $arr["id"] . "'/></td>\n";
                            $str .= "<td class='mainbgtitle' width='100px'  align='center'>(" . $arr["id"] . ")</td>\n";
                            $str .= "<td class='mainbgtitle stylepc' width='360px'  align='center'>" . $arr["name"] . "</td></tr>\n";
                        }
                        
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<td><input name='action' type='hidden' value='" . CONTROL_TEMPLE . "' /></td>";
                        // $str.="<input type='hidden' name='deci' value='".$_POST["decision"]."'/> ";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value=" . localize('Bannir') . " /></td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        
                        break;
                    
                    case 6:
                        if ($dbc->get("money") < 100) {
                            $str .= "<table class='maintable'>\n";
                            $str .= "<tr><td class='mainbgbody'>Il n'y a pas suffisamment d'argent dans les caisses du village pour organiser la cérémonie. </tr></td>";
                            $str .= "</table>\n";
                        } else {
                            
                            $dbt = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $dbc->get("id"), $db);
                            
                            $dbp = new DBCollection("SELECT * FROM Player WHERE racename='Prêtre' AND id_City=" . $dbc->get("id"), $db);
                            $dbpEv = new DBCollection(
                                "SELECT id_city,id_BuildingAction,max(date) as lastDate FROM BuildingEvent WHERE id_city=" . $dbc->get("id") . " and id_BuildingAction=" .
                                     PRIEST_DEATH . " group by id_city,id_BuildingAction", $db);
                            if ($dbp->eof()) {
                                $time = time() - 7 * 24 * 3600;
                                $cal = gmdate("Y-m-d H:i:s", $time);
                                if ($dbpEv->get("lastDate") < $cal) {
                                    if ($dbc->get("captured") == 4)
                                        $hidden = 10;
                                    else
                                        $hidden = 0;
                                    
                                    $xp = $dbt->get("x");
                                    $yp = $dbt->get("y");
                                    InsideBuildingFactory::createPriestForACity($dbc->get("id"), $db);
                                    $str .= "<table class='maintable'>\n";
                                    $str .= "<tr><td class='mainbgbody'>Le prêtre a été ramené et les résurrections dans votre temple sont à nouveau opérationnelles. </tr></td>";
                                    $str .= "</table>\n";
                                    $dbu = new DBCollection("UPDATE City SET money=money-100 WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
                                } else {
                                    $str .= "<table class='maintable'>\n";
                                    $str .= "<tr><td class='mainbgbody'>Il n'est pas possible de ramener le prêtre, le précédant est mort il y a trop peu de temps.</tr></td>";
                                    $str .= "</table>\n";
                                }
                            } else {
                                $str .= "<table class='maintable'>\n";
                                $str .= "<tr><td class='mainbgbody'>Il n'est pas possible de ramener le prêtre, il n'est pas mort.</tr></td>";
                                $str .= "</table>\n";
                            }
                        }
                        break;
                    
                    case 7:
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                        $str .= "<tr><td class='mainbgbody'>Que souhaitez vous faire ?";
                        $str .= "<tr><td class='mainbgbody'><select class='selector cqattackselectorsize' name='CHOICE'>";
                        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une action --") . "</option>";
                        $str .= "<option value=1> Lutter contre </option>";
                        $str .= "<option value=2> Soutenir </option></select>";
                        $str .= "<tr><td class='mainbgbody'> Le putsch de : </td></tr>";
                        $str .= "</table>";
                        $putsch = unserialize($dbc->get("putsch"));
                        $array = array();
                        foreach ($putsch as $name => $percent) {
                            
                            $array[] = array(
                                "0" => array(
                                    "<input type='radio' name='leader' value='" . $name . "'/>",
                                    "class='mainbgbody' align='left'"
                                ),
                                "1" => array(
                                    $name,
                                    "class='mainbgbody' align='center'"
                                ),
                                "2" => array(
                                    $percent . "%",
                                    "class='mainbgbody' align='center'"
                                )
                            );
                        }
                        
                        $str .= createTable(3, $array, array(), 
                            array(
                                array(
                                    localize(""),
                                    "class='mainbglabel' width='10%' align='left'",
                                    "name",
                                    "mainbglabelhover",
                                    "mainbglabel"
                                ),
                                array(
                                    localize("Meneur"),
                                    "class='mainbglabel' width='55%' align='center'",
                                    "price",
                                    "mainbglabelhover",
                                    "mainbglabel"
                                ),
                                array(
                                    localize("Progression"),
                                    "class='mainbglabel' width='35%' align='center'",
                                    "marge",
                                    "mainbglabelhover",
                                    "mainbglabel"
                                )
                            ), "class='maintable insidebuildingleftwidth'", "formid", "order");
                        
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<td><input name='action' type='hidden' value='" . ACT_PUTSCH . "' /></td>";
                        $str .= "<td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        
                        break;
                    
                    // Gérer l'accès à la ville
                    case 8:
                        $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                        $str .= "</td></tr>\n";
                        
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='1' id='openHallDoor' />" .
                             "<label for='resurrect'> Ouvrir toutes les portes.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='2' id='closeHallDoor' />" .
                             "<label for='resurrect'> Fermer toutes les portes.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='3' id='openListFriend' />" .
                             "<label for='resurrect'> Ouvrir à ma liste d'allié uniquement.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='4' id='closeListEnemy' />" .
                             "<label for='resurrect'> Fermer pour mes ennemis.</label></td></tr>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<input name='action_1' type='hidden' value='ACCES_PORTE' />\n";
                        $str .= "<tr><td><input type='submit' name='gotoroom' value='Ok'/> </td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        
                        break;
                    // Gérer l'accès au temple
                    case 9:
                        $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                        $str .= "</td></tr>\n";
                        
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='1' id='openHallDoor' />" .
                             "<label for='resurrect'> Temple accessible pour tout le monde.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='2' id='closeHallDoor' />" .
                             "<label for='resurrect'> Temple inaccessible.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='3' id='openListFriend' />" .
                             "<label for='resurrect'> Temple ouvert à liste d'allié uniquement.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='4' id='closeListEnemy' />" .
                             "<label for='resurrect'> Temple fermé pour mes ennemis.</label></td></tr>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<input name='action_1' type='hidden' value='ACCES_TEMPLE' />\n";
                        $str .= "<tr><td><input type='submit' name='gotoroom' value='Ok'/> </td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        
                        break;
                    // Gérer l'accès aux entrepôts
                    case 10:
                        $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                        $str .= "</td></tr>\n";
                        
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='1' id='openHallDoor' />" .
                             "<label for='resurrect'> Entrepôt(s) accessible pour tout le monde.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='2' id='closeHallDoor' />" .
                             "<label for='resurrect'> Entrepôt(s) inaccessible.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='3' id='openListFriend' />" .
                             "<label for='resurrect'> Entrepôt(s) ouvert à liste d'allié uniquement.</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='management' value='4' id='closeListEnemy' />" .
                             "<label for='resurrect'> Entrepôt(s) fermé pour mes ennemis.</label></td></tr>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<input name='action_1' type='hidden' value='ACCES_ENTREPOT' />\n";
                        $str .= "<tr><td><input type='submit' name='gotoroom' value='Ok'/> </td></tr>";
                        $str .= "</table>";
                        $str .= "</form>";
                        
                        break;
                    
                    default:
                        break;
                }
                $str .= "</table>";
                $str .= "</form>";
            } elseif (isset($_POST['management'])) {
                if ($_POST['action_1'] == "ACCES_PORTE") {
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    if ($_POST['management'] == "1") // ouverture des portes à tous les joueurs
                        $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point d'ouvrir les portes du village à tout le monde. Souhaitez vous continuer ? </tr></td>";
                    if ($_POST['management'] == "2") // fermeture des portes à tous les joueurs
                        $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes du village à tout le monde. Souhaitez vous continuer ? </tr></td>";
                    if ($_POST['management'] == "3") { // ouverture des portes du village uniquement aux amis
                                                       
                        // ----------------------- Choisir une ou plusieur option, pour gere les portes aux alliés -----------------------------------
                        $str .= "<td class='mainbglabel'>Vous souhaitez ouvrir les portes à vos alliés.";
                        $str .= "</td></tr>\n";
                        $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                        $str .= "</td></tr>\n";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3' id='MembersOrder' />" .
                             "<label for='resurrect'>Les membres de mon ordre</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30' id='PersonalAllies' />" .
                             "<label for='resurrect'>Mes alliés personnels</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='300' id='OrdersPersonalAlliances' />" .
                             "<label for='resurrect'>Les ordres de mes alliances personnelles</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3000' id='AlliedMembersOrder' />" .
                             "<label for='resurrect'>Les membres alliés de mon ordre</label></td></tr>";
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30000' id='LevelsCombinedOrder' />" .
                             "<label for='resurrect'>Les ordres alliés à mon ordre</label></td></tr>";
                        $str .= "<td class='mainbglabel'>Valider vos choix puis cliquer sur Continuer";
                        $str .= "</td></tr>\n";
                    }
                    if ($_POST['management'] == "4") // fermeture des portes du village aux ennemis
                        $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes du village à vos ennemis. Souhaitez vous continuer ? </tr></td>";
                    
                    $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt'' value='Continuer' /></td></tr>";
                    $str .= "<td><input name='action' type='hidden' value='" . OPERATE_VILLAGE . "' /></td>";
                    $str .= "<input type='hidden' name='manag' value='" . $_POST["management"] . "'/> ";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</table>";
                    $str .= "</form>";
                } else {
                    if ($_POST['action_1'] == "ACCES_TEMPLE") {
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        if ($_POST['management'] == "1") // ouverture des portes à tous les joueurs
                            $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point d'ouvrir les portes du Temple à tout le monde. Souhaitez vous continuer ? </tr></td>";
                        if ($_POST['management'] == "2") // fermeture des portes à tous les joueurs
                            $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes du Temple à tout le monde. Souhaitez vous continuer ? </tr></td>";
                        if ($_POST['management'] == "3") { // ouverture des portes du village uniquement aux amis
                                                           // ----------------------- Choisir une ou plusieur option, pour gere les portes aux alliés -----------------------------------
                            $str .= "<td class='mainbglabel'>Vous souhaitez ouvrir les portes du temple à vos alliés.";
                            $str .= "</td></tr>\n";
                            $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                            $str .= "</td></tr>\n";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3' id='MembersOrder' />" .
                                 "<label for='resurrect'>Les membres de mon ordre</label></td></tr>";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30' id='PersonalAllies' />" .
                                 "<label for='resurrect'>Mes alliés personnels</label></td></tr>";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='300' id='OrdersPersonalAlliances' />" .
                                 "<label for='resurrect'>Les ordres de mes alliances personnelles</label></td></tr>";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3000' id='AlliedMembersOrder' />" .
                                 "<label for='resurrect'>Les membres alliés de mon ordre</label></td></tr>";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30000' id='LevelsCombinedOrder' />" .
                                 "<label for='resurrect'>Les ordres alliés à mon ordre</label></td></tr>";
                            $str .= "<td class='mainbglabel'>Valider vos choix puis cliquer sur Continuer";
                            $str .= "</td></tr>\n";
                        }
                        if ($_POST['management'] == "4") // fermeture des portes du village aux ennemis
                            $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes du Temple à vos ennemis. Souhaitez vous continuer ? </tr></td>";
                        
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt'' value='Continuer' /></td></tr>";
                        $str .= "<td><input name='action' type='hidden' value='" . OPERATE_VILLAGE_TEMPLE . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<input type='hidden' name='manag' value='" . $_POST["management"] . "'/> ";
                        $str .= "</table>";
                        $str .= "</form>";
                    } else {
                        if ($_POST['action_1'] == "ACCES_ENTREPOT") {
                            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            if ($_POST['management'] == "1") // ouverture des portes à tous les joueurs
                                $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point d'ouvrir les portes de(s) Entrepôt(s) à tout le monde. Souhaitez vous continuer ? </tr></td>";
                            if ($_POST['management'] == "2") // fermeture des portes à tous les joueurs
                                $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes de(s) Entrepôt(s) à tout le monde. Souhaitez vous continuer ? </tr></td>";
                            if ($_POST['management'] == "3") { // ouverture des portes du village uniquement aux amis
                                                               // ----------------------- Choisir une ou plusieur option, pour gere les portes aux alliés -----------------------------------
                                $str .= "<td class='mainbglabel'>Vous souhaitez ouvrir les portes de(s) Entrepôt(s) à vos alliés.";
                                $str .= "</td></tr>\n";
                                $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                                $str .= "</td></tr>\n";
                                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3' id='MembersOrder' />" .
                                     "<label for='resurrect'>Les membres de mon ordre</label></td></tr>";
                                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30' id='PersonalAllies' />" .
                                     "<label for='resurrect'>Mes alliés personnels</label></td></tr>";
                                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='300' id='OrdersPersonalAlliances' />" .
                                     "<label for='resurrect'>Les ordres de mes alliances personnelles</label></td></tr>";
                                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3000' id='AlliedMembersOrder' />" .
                                     "<label for='resurrect'>Les membres alliés de mon ordre</label></td></tr>";
                                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30000' id='LevelsCombinedOrder' />" .
                                     "<label for='resurrect'>Les ordres alliés à mon ordre</label></td></tr>";
                                $str .= "<td class='mainbglabel'>Valider vos choix puis cliquer sur Continuer";
                                $str .= "</td></tr>\n";
                            }
                            if ($_POST['management'] == "4") // fermeture des portes du village aux ennemis
                                $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes de(s) Entrepôt(s) à vos ennemis. Souhaitez vous continuer ? </tr></td>";
                            
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt'' value='Continuer' /></td></tr>";
                            $str .= "<td><input name='action' type='hidden' value='" . OPERATE_VILLAGE_ENTREPOT . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<input type='hidden' name='manag' value='" . $_POST["management"] . "'/> ";
                            $str .= "</table>";
                            $str .= "</form>";
                        }
                    }
                }
            } else {
                if ($curplayer->get("status") == "PC") {
                    $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr>\n";
                    $str .= "<tr>\n";
                    $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                    $str .= "</td></tr>\n";
                    // Pour les gens ayant le droit
                    
                    if ($curplayer->accessRoom($curplayer->get("inbuilding"), DECISION_ROOM, $db) || $curplayer->get("id") == $dbc->get("id_Player")) {
                        if ($dbc->get("loyalty") >= 50 && $garden->count() == 0) {
                            $dbpEv = new DBCollection(
                                "SELECT id_city,id_BuildingAction,max(date) as lastDate FROM BuildingEvent WHERE id_city=" . $dbc->get("id") . " and id_BuildingAction=" .
                                     GUARD_DEATH . " group by id_city,id_BuildingAction", $db);
                            $time = time() - 7 * 24 * 3600;
                            $cal = gmdate("Y-m-d H:i:s", $time);
                            if ($dbpEv->count() > 0 && $dbpEv->get("lastDate") < $cal) {
                                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='3' id='workForControl' />" .
                                     "<label for='resurrect'> Engager un garde royal.</label></td></tr>";
                            }
                        }
                        if ($dbc->get("captured") == 4) {
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='8' id='operateVillage' />" .
                                 "<label for='resurrect'> Gérer l'accès à la ville.</label></td></tr>";
                            
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='5' id='controlAccessTempleRes' />" .
                                 "<label for='resurrect'> Gérer la resurrection au Temple.</label></td></tr>";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='9' id='controlAccessTemple' />" .
                                 "<label for='resurrect'> Contrôler l'accès au Temple.</label></td></tr>";
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='10' id='controlAccessEntrepot' />" .
                                 "<label for='resurrect'> Contrôler l'accès aux Entrepôts.</label></td></tr>";
                            $dbp = new DBCollection("SELECT * FROM Player WHERE racename='Prêtre' AND id_City=" . $dbc->get("id"), $db);
                            $dbpEv = new DBCollection(
                                "SELECT id_city,id_BuildingAction,max(date) as lastDate FROM BuildingEvent WHERE id_city=" . $dbc->get("id") . " and id_BuildingAction=" .
                                     PRIEST_DEATH . " group by id_city,id_BuildingAction", $db);
                            if ($dbp->eof()) {
                                $time = time() - 7 * 24 * 3600;
                                $cal = gmdate("Y-m-d H:i:s", $time);
                                if ($dbpEv->get("lastDate") < $cal)
                                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='6' id='resurrectPriest' />" .
                                         "<label for='resurrect'> Ordonner une cérémonie d'offrande (100po) pour ressussiter le prêtre du temple.</label></td></tr>";
                            }
                        }
                    }
                    
                    // Pour tout le monde
                    if ($dbc->get("loyalty") < 50 && ($dbc->get("putsch") == "" or $dbc->get("putsch") == "a:0:{}"))
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='2' id='workForPeace' />" .
                             "<label for='resurrect'> Oeuvrer pour augmenter la loyauté du village envers le gouverneur actuel.</label></td></tr>";
                    
                    if ($dbc->get("loyalty") > 0)
                        $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='1' id='workForControl' />" .
                             "<label for='resurrect'> Oeuvrer pour baisser la loyauté du village envers son gouverneur actuel.</label></td></tr>";
                    
                    if ($dbc->get("loyalty") <= 0) {
                        $stop = 0;
                        $ph = "";
                        if ($dbc->get("putsch") != "" && $dbc->get("putsch") != "a:0:{}") {
                            
                            $putsch = unserialize($dbc->get("putsch"));
                            $array = array();
                            foreach ($putsch as $name => $percent) {
                                if ($percent > 49)
                                    $stop = 1;
                                $array[] = array(
                                    "0" => array(
                                        $name,
                                        "class='mainbgbody' align='left'"
                                    ),
                                    "1" => array(
                                        $percent . "%",
                                        "class='mainbgbody' align='center'"
                                    )
                                );
                            }
                            
                            $ph = "nouveau";
                        }
                        
                        if (! $stop && $curplayer->get("id") != $dbc->get("id_Player"))
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='4' id='putsch' />" . "<label for='resurrect'> Lancer un " . $ph .
                                 " putsch.</label></td></tr>";
                        
                        if ($dbc->get("putsch") != "" && $dbc->get("putsch") != "a:0:{}")
                            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='decision' value='7' id='putsch' />" .
                                 "<label for='resurrect'> Soutenir ou lutter contre un putsch actuel.</label></td></tr>";
                    }
                    
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<tr><td><input type='submit' name='gotoroom' value='Confirmer'/> </td></tr>";
                    $str .= "</table>\n";
                    
                    if ($dbc->get("loyalty") == 0 && $dbc->get("putsch") != "" && $dbc->get("putsch") != "a:0:{}") {
                        
                        $str .= createTable(2, $array, array(), 
                            array(
                                array(
                                    localize("Meneur de putsch actuel"),
                                    "class='mainbglabel' width='40%' align='left'",
                                    "name",
                                    "mainbglabelhover",
                                    "mainbglabel"
                                ),
                                array(
                                    localize("Progression"),
                                    "class='mainbglabel' width='20%' align='center'",
                                    "price",
                                    "mainbglabelhover",
                                    "mainbglabel"
                                )
                            ), "class='maintable insidebuildingleftwidth'", "formid", "order");
                        
                        // $str.="</table>\n";
                    }
                    $str .= "</table>\n";
                    $str .= "</form>";
                }
            }
        }
        
        return $str;
    }
}
?>
