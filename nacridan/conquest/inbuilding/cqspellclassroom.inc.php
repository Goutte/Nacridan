<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQSpellclassroom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $curbuilding;

    public $nacridan;

    public $err;

    public function CQSpellclassroom($building, $nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        $this->curbuilding = $building;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $curbuilding = $this->curbuilding;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('S A L L E &nbsp;&nbsp;&nbsp; D E &nbsp;&nbsp;&nbsp; C L A S S E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        if ($curbuilding->get("school") != "")
            $schoolList = unserialize($curbuilding->get("school"));
        else
            $schoolList = array();
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        
        if (isset($_POST["learn"]) && isset($_POST["IDSPELL"])) {
            $dbBMS = new DBCollection("SELECT name,level FROM BasicMagicSpell WHERE id=" . $_POST["IDSPELL"], $db, 0, 0);
            if ($dbBMS->get("level") == 0)
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_SPELL_0, $db);
            elseif ($dbBMS->get("level") == 1)
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_SPELL_1, $db);
            elseif ($dbBMS->get("level") == 2)
                $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_SPELL_2, $db);
            
            $dbMS = new DBCollection("SELECT id FROM MagicSpell WHERE id_Player=" . $id . " AND id_BasicMagicSpell=" . $_POST["IDSPELL"], $db, 0, 0);
            if ($dbMS->count() > 0)
                $str .= "<tr><td class='mainbglabel'>Vous connaissez déjà ce sortilège.</td></tr>";
            elseif ($curplayer->get("money") < $price)
                $str .= "<tr><td class='mainbglabel'>Vous n'avez pas assez de PO.</td></tr>";
            elseif ($curplayer->get("ap") < (LEARN_AP - $curbuilding->get("value")))
                $str .= "<tr><td class='mainbglabel'>Vous n'avez pas assez de PA.</td></tr>";
            else {
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<tr><td class='mainbglabel'>Vous allez apprendre le sort " . $dbBMS->get("name") . " pour " . $price . " PO  (et " . (LEARN_AP - $curbuilding->get("value")) . " PA) ?";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' /> </td></tr>";
                $str .= "<input name='action' type='hidden' value='" . SCHOOL_LEARN_SPELL . "' />";
                $str .= "<input name='IDSPELL' type='hidden' value='" . $_POST["IDSPELL"] . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        } else {
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            
            $str .= "<tr><td class='mainbglabel'>Temps d'apprentissage dans cette école : " . (LEARN_AP - $curbuilding->get("value")) . " PA.</td></tr>";
            $str .= "<tr><td class='mainbglabel'>Liste des enseignements : </td></tr>";
            foreach ($schoolList as $idname => $level) {
                if ($idname == SCHOOL_BASIC_WARRIOR || $idname == SCHOOL_BASIC_MAGIC) {
                    $niveau = "";
                    $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_SPELL_0, $db);
                    
                    $str .= "<tr><td class='mainbgbody'><br/> <b class='stylepc'>" . getSchoolName($idname) . $niveau . "</b><br/> </td></tr>";
                    
                    $dbBMS = new DBCollection("SELECT id,name FROM BasicMagicSpell WHERE school=" . $idname, $db, 0, 0);
                    while (! $dbBMS->eof()) {
                        $str .= "<tr><td class='mainbgbody'><input type='radio' name='IDSPELL' value='" . $dbBMS->get("id") . "' id='" . $dbBMS->get("name") . "' /><label for='" . $dbBMS->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/spell.php?id=" . $dbBMS->get("id") . "\" class='parchment popupify'>" . localize($dbBMS->get("name")) . "</a> (" . $price . " PO) </label></td></tr>";
                        $dbBMS->next();
                    }
                } else {
                    
                    if ($level == 1 or $level == 2) {
                        $niveau = ", niveau Aspirant";
                        $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_SPELL_1, $db);
                        
                        $str .= "<tr><td class='mainbgbody'><br/> <b class='stylepc'>" . getSchoolName($idname) . $niveau . "</b><br/> </td></tr>";
                        
                        $enable = "disabled";
                        if (InsideBuildingFactory::prerequisitesForLearningMagicSpell($id, $idname, 1, $db))
                            $enable = "";
                        else
                            $str .= "<tr><td class='mainbgbody'>Vous devez maîtriser un sortilège élémentaire à 80% avant de pouvoir accéder au rang d'Aspirant</td></tr>";
                        
                        $dbBMS = new DBCollection("SELECT id,name FROM BasicMagicSpell WHERE school=" . $idname . " AND level=1", $db, 0, 0);
                        while (! $dbBMS->eof()) {
                            $str .= "<tr><td class='mainbgbody'><input type='radio'" . $enable . " name='IDSPELL' value='" . $dbBMS->get("id") . "' id='" . $dbBMS->get("name") . "' /><label for='" . $dbBMS->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/spell.php?id=" . $dbBMS->get("id") . "\" class='parchment popupify'>" . localize($dbBMS->get("name")) . "</a> (" . $price . " PO) </label></td></tr>";
                            $dbBMS->next();
                        }
                    }
                    
                    if ($level == 2) {
                        $niveau = ", niveau Adepte";
                        $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SCHOOL_LEARN_SPELL_2, $db);
                        
                        $str .= "<tr><td class='mainbgbody'><br/> <b class='stylepc'>" . getSchoolName($idname) . $niveau . "</b><br/> </td></tr>";
                        
                        $enable = "disabled";
                        if (InsideBuildingFactory::prerequisitesForLearningMagicSpell($id, $idname, 2, $db))
                            $enable = "";
                        else
                            $str .= "<tr><td class='mainbgbody'>Vous devez maîtriser deux sortilèges niveau Aspirant à 90% de cette école avant de pouvoir accéder au rang de Adepte</td></tr>";
                        
                        $dbBMS = new DBCollection("SELECT id,name FROM BasicMagicSpell WHERE school=" . $idname . " AND level=2", $db, 0, 0);
                        while (! $dbBMS->eof()) {
                            $str .= "<tr><td class='mainbgbody'><input type='radio'" . $enable . " name='IDSPELL' value='" . $dbBMS->get("id") . "' id='" . $dbBMS->get("name") . "' /><label for='" . $dbBMS->get("name") . "'><a href=\"" . CONFIG_HOST . "/conquest/spell.php?id=" . $dbBMS->get("id") . "\" class='parchment popupify'>" . localize($dbBMS->get("name")) . "</a>  (" . $price . " PO) </label></td></tr>";
                            $dbBMS->next();
                        }
                    }
                }
            }
            
            $str .= "<tr><td><input type='submit' name='learn' value='Apprendre'/> </td></tr>";
            $str .= "</form>";
        }
        
        $str .= "</table>\n";
        return $str;
    }
}
?>
