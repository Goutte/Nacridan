<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQTreasury extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQTreasury($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        $target = "/conquest/conquest.php?center=view2d";
        $dbh = new DBCollection(
            "SELECT City.id as idCity, City.captured as captured, City.money as money FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" .
                 $curplayer->get("inbuilding"), $db);
        $idCity = $dbh->get("idCity");
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('T R E S O R E R I E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        if ($curplayer->accessRoom($curplayer->get("inbuilding"), TREASURY_ROOM, $db) || $curplayer->get("authlevel") > 10) {
            $str .= "<tr><td> Caisse de la cité : " . $dbh->get("money") . " pièce(s) d'or</td></tr>";
        }
        $str .= "</table>\n";
        
        if (isset($_POST['confirm3'])) {
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr><td colspan=3> Attention : augmenter la marge sur un service augmente automatiquement son coût d'autant pour l'utilisateur.</td></tr>";
            if (isset($_POST["idBuilding"]))
                $idBuilding = $_POST["idBuilding"];
            else
                $idBuilding = $_POST["IDBUILDING"];
            
            $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $idBuilding, $db);
            $marge = unserialize($dbc->get("profit"));
            $dba = new DBCollection("SELECT * FROM BuildingAction WHERE taxable='Yes' AND id_BasicBuilding=" . $dbc->get("id_BasicBuilding"), $db);
            $array = array();
            while (! $dba->eof()) {
                
                if ($dba->get("id") == TELEPORT) {
                    $price = TELEPORT_PRICE . "/c +" . $marge[$dba->get("name")];
                    $mar = $marge[$dba->get("name")];
                } elseif ($dba->get("id") == BANK_DEPOSIT || $dba->get("id") == BANK_TRANSFERT) {
                    $price = $dba->get("price") - ($dbc->get("level") - 1) + $marge[$dba->get("name")] . "%";
                    $mar = $marge[$dba->get("name")] . "%";
                } elseif ($dba->get("id") == MAIN_SHOP_BUY || $dba->get("id") == BASIC_SHOP_BUY || $dba->get("id") == SHOP_REPAIR || $dba->get("id") == GUILD_ORDER_EQUIP ||
                     $dba->get("id") == GUILD_ORDER_ENCHANT) {
                    $price = $dba->get("price") + $marge[$dba->get("name")] . "%";
                    $mar = $marge[$dba->get("name")] . "%";
                } elseif ($dba->get("id") == EXCHANGE_OBJECT || $dba->get("id") == EXCHANGE_GET_OBJECT) {
                    $price = $dba->get("price") . "%";
                    $mar = $marge[$dba->get("name")] . "%";
                } else {
                    // $price = 1;
                    // $mar = 1;
                    $price = $dba->get("price") + $marge[$dba->get("name")];
                    $mar = $marge[$dba->get("name")];
                }
                $array[] = array(
                    "0" => array(
                        $dba->get("name"),
                        "class='mainbgbody' align='left'"
                    ),
                    "1" => array(
                        $price,
                        "class='mainbgbody' align='center'"
                    ),
                    "2" => array(
                        $mar,
                        "class='mainbgbody' align='center'"
                    ),
                    "3" => array(
                        "<input type='textbox' name='tax[]' value='" . $marge[$dba->get("name")] . "' />",
                        "class='mainbgbody' align='center'"
                    )
                );
                
                $dba->next();
            }
            $str .= createTable(4, $array, array(), 
                array(
                    array(
                        localize("Nom du service"),
                        "class='mainbglabel' width='40%' align='left'",
                        "name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Prix Actuel"),
                        "class='mainbglabel' width='20%' align='center'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Marge Actuel"),
                        "class='mainbglabel' width='20%' align='center'",
                        "marge",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Modifier la marge"),
                        "class='mainbglabel' width='30%' align='center'",
                        "marge",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), "class='maintable insidebuildingleftwidth'", "formid", "order");
            
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr><td><input type='checkbox' name='default' value='1'> Valeur par défaut </td>";
            $str .= "<td><input id='confirm3' type='submit' name='confirm3' value='Modifier' />";
            
            $str .= "<input name='IDBUILDING' type='hidden' value='" . $idBuilding . "' />";
            $str .= "<input name='action' type='hidden' value='" . TREASURY_SET_TAX . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            
            $str .= "</table></form>";
        } elseif (isset($_POST['confirm4']) && isset($_POST["check"])) {
            $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $where = InsideBuildingFactory::getCondFromArray($_POST["check"], "Building.id", "OR");
            $dbe = new DBCollection("SELECT * FROM Building WHERE " . $where, $db);
            $money = 0;
            while (! $dbe->eof()) {
                $money += $dbe->get("money");
                $dbh = new DBCollection("UPDATE Building SET money=0 WHERE id=" . $dbe->get("id"), $db, 0, 0, false);
                $dbe->next();
            }
            
            $dbc = new DBCollection("UPDATE City SET money=money+" . $money . " WHERE id=" . $idCity, $db, 0, 0, false);
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr><td>" . $money . " pièces d'or ont été transféré vers la caisse centrale de la cité.</td>";
            $str .= "<td><input id='confirm4' type='submit' name='confirm4' value='Terminer' />";
            
            $str .= "<input name='treasurychoice' type='hidden' value='cash' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            
            $str .= "</table></form>";
        } elseif (isset($_POST['confirm5']) && isset($_POST["check"])) {
            $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $dbe = new DBCollection("SELECT * FROM Building WHERE " . $where, $db);
            $money = 0;
            while (! $dbe->eof()) {
                $money += $dbe->get("money");
                $dbh = new DBCollection("UPDATE Building SET money=0 WHERE id=" . $dbe->get("id"), $db, 0, 0, false);
                $dbe->next();
            }
            
            $dbc = new DBCollection("UPDATE City SET money=money+" . $money . " WHERE id=" . $idCity, $db, 0, 0, false);
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr><td>" . $money . " pièces d'or ont été transféré vers la caisse centrale de la cité.</td>";
            $str .= "<td><input id='confirm4' type='submit' name='confirm4' value='Terminer' />";
            
            $str .= "<input name='treasurychoice' type='hidden' value='cash' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            
            $str .= "</table></form>";
        } elseif (isset($_POST['treasurychoice'])) {
            $architectchoice = $_POST['treasurychoice'];
            
            switch ($architectchoice) {
                case "withdraw":
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<tr><td class='mainbgbody'> Retirer : <input type='textbox' name='withdraw' />" . " " . localize("PO");
                    $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Valider' /></td></tr>";
                    $str .= "<input name='action' type='hidden' value='" . PALACE_WITHDRAW . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    
                    break;
                
                case "deposit":
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<tr><td class='mainbgbody'> Déposer : <input type='textbox' name='deposit' />" . " " . localize("PO");
                    $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Valider' /></td></tr>";
                    $str .= "<input name='action' type='hidden' value='" . PALACE_DEPOSIT . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    break;
                
                case "manage":
                    
                    $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "</td></tr><tr><td> Choississez le bâtiment dont vous voulez gérer les taxes.</td></tr>";
                    $dbb = new DBCollection("SELECT * FROM Building WHERE id_City=" . $idCity . " AND money!=-1", $db);
                    $str .= "<tr><td><select class='selector cqattackselectorsize' name='idBuilding'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bâtiment --") . "</option>";
                    $item = array();
                    while (! $dbb->eof()) {
                        $item[] = array(
                            localize($dbb->get("name")) => $dbb->get("id")
                        );
                        $dbb->next();
                    }
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value)
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                    $str .= "</select>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<tr><td><input id='confirm3' type='submit' name='confirm3' value='Confirmer' /></td></tr>";
                    
                    break;
                
                case "cash":
                    
                    $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $dbb = new DBCollection(
                        "SELECT Building.* FROM Building inner join City on Building.id_City = City.id WHERE Building.id_City=" . $idCity .
                             " AND Building.money!=-1 AND Building.id_BasicBuilding!=11 AND Building.id_BasicBuilding!=10 ", $db);
                    $array = array();
                    while (! $dbb->eof()) {
                        $checkbox = "<input type='checkbox' name='check[]' value='" . $dbb->get("id") . "'>";
                        $array[] = array(
                            "0" => array(
                                $checkbox,
                                "class='mainbgbody' align='left'"
                            ),
                            "1" => array(
                                $dbb->get("name") . " (" . $dbb->get("id") . ")",
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $dbb->get("level"),
                                "class='mainbgbody' align='center'"
                            ),
                            "3" => array(
                                $dbb->get("money") . " PO",
                                "class='mainbgbody' align='center'"
                            )
                        );
                        
                        $dbb->next();
                    }
                    
                    $str .= createTable(4, $array, array(), 
                        array(
                            array(
                                localize(""),
                                "class='mainbglabel' width='5%' align='left'",
                                "name",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Bâtiment"),
                                "class='mainbglabel' width='40%' align='center'",
                                "price",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Niveau"),
                                "class='mainbglabel' width='25%' align='center'",
                                "price",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Caisse"),
                                "class='mainbglabel' width='30%' align='center'",
                                "marge",
                                "mainbglabelhover",
                                "mainbglabel"
                            )
                        ), "class='maintable insidebuildingleftwidth'", "formid", "order");
                    
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<tr><td> Transférer vers la caisse centrale </td>";
                    $str .= "<td><input id='confirm4' type='submit' name='confirm4' value='Action' /></td></tr>";
                    $str .= "<input name='treasurychoice' type='hidden' value='cash' />";
                    
                    break;
                case "cash_out":
                    
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $dbb = new DBCollection("SELECT * FROM Building WHERE id_City=" . $idCity . " AND money!=-1 AND id_BasicBuilding!=11 AND id_BasicBuilding!=14", $db);
                    $str .= "<tr><td><select class='selector cqattackselectorsize' name='basicBuilding'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bâtiment --") . "</option>";
                    while (! $dbb->eof()) {
                        $str .= "<option value='" . $dbb->get("id") . "'>" . $dbb->get("name") . "</option>";
                        $dbb->next();
                    }
                    $str .= "</select>";
                    $str .= "</td></tr><tr><td>";
                    $str .= localize("Transférer :") . " <input type='textbox' name='deposit' />" . " " . localize("PO");
                    $str .= "</td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<tr><td> Transférer vers la caisse du bâtiment </td>";
                    $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Valider' /></td></tr>";
                    $str .= "<input name='action' type='hidden' value='" . PALACE_TRANSFER_MONEY . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    
                    break;
                default:
                    break;
            }
            $str .= "</table></form>";
        } else {
            if ($curplayer->accessRoom($curplayer->get("inbuilding"), TREASURY_ROOM, $db) || $curplayer->get("authlevel") > 10) {
                $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr><td class='mainbglabel'>Dans cette salle, vous pouvez gérer les bâtiments de la cité. Que souhaitez-vous faire ? </td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='treasurychoice' value='deposit' id='deposit' />" .
                     "<label for='heal'> Déposer de l'argent dans les caisses de la cité.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='treasurychoice' value='withdraw' id='withdraw' />" .
                     "<label for='blessing'> Retirer de l'argent des caisses de la cité.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='treasurychoice' value='manage' idcu='manage' />" .
                     "<label for='manage'> Gérer les taxes de la cité.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='treasurychoice' value='cash' id='cash' />" .
                     "<label for='cash'> Vider les caisses des bâtiments de la cité.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='treasurychoice' value='cash_out' id='cash_out' />" .
                     "<label for='cash_out'> Alimenter la caisse d'un bâtiment de la cité.</label></td></tr>";
                $str .= "<tr><td><input type='submit' name='ok' value='Ok'/><a href=\"" . CONFIG_HOST . "/conquest/inbuilding/treasuryaccount.php?id=" . $idCity .
                     "\" class='parchment popupify'> Accéder au livre des comptes </a></label></td></tr>";
                
                $str .= "</table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
