<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");

class CQCaravan extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQCaravan($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $money = $curplayer->get("money");
        $err = $this->err;
        $str = "";
        
        $dbB = new DBCollection("SELECT level,id_City FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db, 0, 0);
        
        $Blevel = $dbB->get("level");
        
        $strTitle = "<table class='maintable insidebuildingleftwidth'>\n";
        $strTitle .= "<tr>\n";
        $strTitle .= "<td class='mainbgtitle'><b>" . localize('C A R A V A N S E R A I L') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
        $strTitle .= "</td>\n";
        $strTitle .= "</tr>\n";
        $strTitle .= "</table>\n";
        
        if (isset($_POST["conf"])) {
            require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
            $price = InsideBuildingFactory::cancelCaravan($curplayer, $db);
            $str .= $strTitle;
            $str .= "<table class='maintable insidebuildingleftwidth'>";
            if ($price > 0) {
                $str .= "<tr><td class='mainbgtitle'> La mission a été annulée. Vous avez récupéré votre investissement, soit " . $price . " PO. </td></tr>";
            } else {
                $str .= "<tr><td class='mainbgtitle'> Votre personnage <b>" . $curplayer->get("name") .
                     "</b> était responsable d'une caravane mais la tortue a été tuée. La mission commerciale a été annulée. Votre réputation de marchand diminue.. </td></tr>";
            }
        } elseif (isset($_POST["annulation"])) { // ANNULATION d'une mission
            $str .= $strTitle;
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&action=act" . "' target='_self'>";
            $str .= "<table class='maintable insidebuildingleftwidth'>";
            $str .= "<tr><td class='mainbgtitle'> <b>Voulez vous vraiment abandonner cette mission ? </b> </td></tr>";
            $str .= "<tr><td ><input id='conf' type='submit' name='conf' value='Confirmer' /></tr></td>";
        } elseif (isset($_POST["CARAVAN_LOAD_TYPE_1"]) && ! isset($_POST["RETOUR"])) { // CREATION DE CARAVANE ---- ETAPE 3
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>";
            $str .= "<table class='maintable'>";
            $str .= "<tr><td class='mainbgtitle' colspan=4 > <b>" . localize('C A R A V A N S E R A I L') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err .
                 "</span> - &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Organiser une caravane</b> </td></tr>";
            $str .= "<tr><td class='mainbglabel' colspan=4> Composition du chargement </td></tr>";
            $str .= "<tr class='mainbglabel'><td> Type </td> <td> Niveau </td> <td> Quantité </td> <td> Prix </td> </tr>";
            $load = array();
            
            $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $_POST["CARAVAN_LOAD_TYPE_1"], $db, 0, 0);
            $lvl = $_POST["CARAVAN_LOAD_LEVEL_1"];
            $nb1 = $_POST["CARAVAN_LOAD_QUANTITY_1"];
            $price1 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
            $price1 = $price1 * $nb1;
            $name = $dbbe->get("name");
            $str .= "<tr class='mainbgbody'><td> " . $name . " </td> <td> " . $lvl . " </td><td> " . $nb1 . " </td> <td> " . $price1 . " </td> </tr>";
            $load["type1"] = $dbbe->get("id");
            $load["level1"] = $lvl;
            $load["quantity1"] = $nb1;
            
            $price2 = 0;
            $price3 = 0;
            $nb2 = 0;
            $nb3 = 0;
            if ($_POST["CARAVAN_LOAD_TYPE_2"] != 0) {
                $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $_POST["CARAVAN_LOAD_TYPE_2"], $db, 0, 0);
                $lvl = $_POST["CARAVAN_LOAD_LEVEL_2"];
                $nb2 = $_POST["CARAVAN_LOAD_QUANTITY_2"];
                $price2 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
                $price2 = $price2 * $nb2;
                $name = $dbbe->get("name");
                $str .= "<tr class='mainbgbody'><td> " . $name . " </td> <td> " . $lvl . " </td><td> " . $nb2 . " </td> <td> " . $price2 . " </td> </tr>";
                $load["type2"] = $dbbe->get("id");
                $load["level2"] = $lvl;
                $load["quantity2"] = $nb2;
            }
            
            if ($_POST["CARAVAN_LOAD_TYPE_3"] != 0) {
                $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $_POST["CARAVAN_LOAD_TYPE_3"], $db, 0, 0);
                $lvl = $_POST["CARAVAN_LOAD_LEVEL_3"];
                $nb3 = $_POST["CARAVAN_LOAD_QUANTITY_3"];
                $price3 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
                $price3 = $price3 * $nb3;
                $name = $dbbe->get("name");
                $str .= "<tr class='mainbgbody'><td> " . $name . " </td> <td> " . $lvl . " </td> <td> " . $nb3 . " </td> <td> " . $price3 . " </td> </tr>";
                $load["type3"] = $dbbe->get("id");
                $load["level3"] = $lvl;
                $load["quantity3"] = $nb3;
            }
            
            $totalprice = $price1 + $price2 + $price3;
            $totalquantity = $nb1 + $nb2 + $nb3;
            $pricelimit = 60 + 100 * ($_POST["Nc"] - 1);
            $str .= "<tr><td class='mainbglabel' colspan=4> Valeur totale du chargement : <b>" . $totalprice . " PO</b> - Nombre total de pièces : <b>" . $totalquantity .
                 "</b></td></tr>";
            
            if ($totalprice > $pricelimit) {
                $str .= "<tr><td class='mainbglabel' colspan=4> Valeur totale du chargement est supérieure la limite de " . $pricelimit .
                     " PO imposée par le niveau de la mission. Choisissez un chargement moins onéreux.</td></tr>";
                $str .= "</form>";
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
                $str .= "<input name='CARAVAN_LEVEL' type='hidden' value='" . $_POST["Nc"] . "' />\n";
                $str .= "<input name='CARAVAN_TARGET' type='hidden' value='" . $_POST["CARAVAN_TARGET"] . "' />\n";
                $str .= "<input name='LOAD' type='hidden' value='" . base64_encode(serialize($load)) . "' />\n";
                $str .= "<input name='action' type='hidden' value='" . CARAVAN_CREATE . "' />";
                $str .= "<input name='RETOUR' type='hidden' value='RETOUR' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Retour' /></tr></td>";
            } elseif ($totalquantity < NB_MIN_LOAD_CARAVAN) {
                $str .= "<tr><td class='mainbglabel' colspan=4> Le nombre total d'objets transportés est inférieur à la limite de " . NB_MIN_LOAD_CARAVAN .
                     ". Complétez votre chargement avec des objets peu onéreux, comme des herbes ou du bois.</td></tr>";
                $str .= "</form>";
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
                $str .= "<input name='CARAVAN_LEVEL' type='hidden' value='" . $_POST["Nc"] . "' />\n";
                $str .= "<input name='CARAVAN_TARGET' type='hidden' value='" . $_POST["CARAVAN_TARGET"] . "' />\n";
                $str .= "<input name='LOAD' type='hidden' value='" . base64_encode(serialize($load)) . "' />\n";
                $str .= "<input name='action' type='hidden' value='" . CARAVAN_CREATE . "' />";
                $str .= "<input name='RETOUR' type='hidden' value='RETOUR' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Retour' /></tr></td>";
            } else {
                $dbtarget = new DBCollection("SELECT id,x,y,id_City FROM Building WHERE id=" . $_POST["CARAVAN_TARGET"], $db);
                if (! $dbtarget->eof()) {
                    $dbcity = new DBCollection("SELECT name,x,y FROM City WHERE id=" . $dbtarget->get("id_City"), $db);
                    $dist = distHexa($curplayer->get("x"), $curplayer->get("y"), $dbtarget->get("x"), $dbtarget->get("y"));
                    $tname = $dbcity->get("name");
                    
                    if (($tname == "Bourg") || ($tname == "Village"))
                        $name = "d'un " . $tname;
                    else
                        $name = "de " . $tname;
                    
                    $duration = 3 * ceil($dist / 24); // en jours
                    $interval = new DateInterval("P" . $duration . "D");
                    $limitdate = strtotime(date_format(date_add(date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s")), $interval), "Y-m-d H:i:s"));
                    
                    $Mois = array(
                        "janvier",
                        "février",
                        "mars",
                        "avril",
                        "mai",
                        "juin",
                        "juillet",
                        "août",
                        "septembre",
                        "octobre",
                        "novembre",
                        "décembre"
                    );
                    $strdate = date("j", $limitdate) . " " . $Mois[(date("n", $limitdate) - 1)] . " à " . date("G", $limitdate) . "h" . date("i", $limitdate);
                    
                    $str .= "<tr><td class='mainbgbody' colspan=4> Vous allez organiser une caravane de niveau " . $_POST["Nc"] . " à destination du comptoir commercial " . $name .
                         " situé en X=" . $dbtarget->get("x") . "/Y=" . $dbtarget->get("y") . ". Vous devez arriver à destination <b>avant le " . $strdate . "</b>. </td></tr>";
                    $str .= "<tr><td colspan='3'>";
                    $str .= "<input name='CARAVAN_LEVEL' type='hidden' value='" . $_POST["Nc"] . "' />\n";
                    $str .= "<input name='CARAVAN_TARGET' type='hidden' value='" . $_POST["CARAVAN_TARGET"] . "' />\n";
                    $str .= "<input name='LOAD' type='hidden' value='" . base64_encode(serialize($load)) . "' />\n";
                    $str .= "<input name='action' type='hidden' value='" . CARAVAN_CREATE . "' />";
                    $str .= "<input name='RETOUR' type='hidden' value='RETOUR' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<input id='submitbt' type='submit' name='submitbt' style='float: left; 'value='Acheter le chargement' /></form>";
                } else {
                    $str .= "<tr><td class='mainbgbody' colspan=4> Veuillez choisir une destination valide en cliquant sur une des croix de la carte.</td></tr>";
                    $str .= "<tr><td colspan='3'>";
                    $str .= "</form>";
                }
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
                $str .= "<input name='CARAVAN_LEVEL' type='hidden' value='" . $_POST["Nc"] . "' />\n";
                $str .= "<input name='CARAVAN_TARGET' type='hidden' value='" . $_POST["CARAVAN_TARGET"] . "' />\n";
                $str .= "<input name='LOAD' type='hidden' value='" . base64_encode(serialize($load)) . "' />\n";
                $str .= "<input name='action' type='hidden' value='" . CARAVAN_CREATE . "' />";
                $str .= "<input name='RETOUR' type='hidden' value='RETOUR' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<input id='submitbt' type='submit' name='submitbt' style='float: right';  value='Retour' /></tr></td>";
            }
        } elseif (isset($_POST["CARAVAN_LEVEL"]) && ! isset($_POST["RETOUR_SEL_LVL"]) && (! isset($_POST["RETOUR"]) || $_POST["RETOUR"] == "RETOUR")) { // CREATION DE CARAVANE ---- ETAPE 2
            $Nc = $_POST["CARAVAN_LEVEL"];
            if (isset($_POST["LOAD"]))
                $previousLoad = unserialize(base64_decode($_POST["LOAD"]));
            $str .= $strTitle;
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=map&bottom=caravane' target='_self'>";
            $str .= "<table class='maintable insidebuildingleftwidth'>";
            $str .= "<tr><td class='mainbgtitle' > <b>Organiser une caravane</b> </td></tr>";
            $str .= "<tr><td class='mainbgbody'>  ";
            $str .= " Choisissez le comptoir commercial de destination : ";
            
            $distmin = min(400, 15 + 6 * $Nc);
            $distmax = 6 * $Nc + 80;
            $dbend = new DBCollection(
                "SELECT Building.id,city.name as cityName, Building.x, Building.y, (abs(Building.x-" . $xp . ") + abs(Building.y-" . $yp . ") + abs(Building.x+Building.y-" . $xp .
                     "-" . $yp . "))/2 as dist FROM Building left outer join City as city on id_City=city.id WHERE Building.id!=" . $curplayer->get("inbuilding") .
                     " AND id_BasicBuilding=4 AND Building.map=" . $map . " AND (abs(Building.x-" . $xp . ") + abs(Building.y-" . $yp . ") + abs(Building.x+Building.y-" . $xp . "-" .
                     $yp . "))/2 >=" . $distmin . " AND (abs(Building.x-" . $xp . ") + abs(Building.y-" . $yp . ") + abs(Building.x+Building.y-" . $xp . "-" . $yp . "))/2 <=" .
                     $distmax . " order by dist ", $db, 0, 0);
            
            $IdCoComList = '';
            $str .= "<select id='target' class='selector' name='CARAVAN_TARGET'>";
            
            $selected = "";
            
            while (! $dbend->eof()) {
                if (isset($_POST["CARAVAN_TARGET"]) && $_POST["CARAVAN_TARGET"] == $dbend->get("id"))
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $IdCoComList .= $IdCoComList != '' ? ',' . $dbend->get("id") : $dbend->get("id");
                $str .= "<option value='" . $dbend->get("id") . "' " . $selected . " >" . $dbend->get("cityName") . " (" . $dbend->get("x") . "/" . $dbend->get("y") . ", " .
                     floor($dbend->get("dist")) . " lieues) </option>";
                
                $dbend->next();
            }
            
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  ";
            $str .= " Choisissez le chargement de la caravane. Vous devez choisir au moins " . NB_MIN_LOAD_CARAVAN .
                 " objets répartis en 3 catégories maximum tels que la somme des prix à la vente ne dépasse pas " . (60 + 100 * ($Nc - 1)) .
                 " PO. <a class='parchment' href='#' onclick='javascript:detailedRules(\"" . CONFIG_HOST .
                 "/i18n/rules/fr/rules.php?page=step9\");'>Indicatif des prix</a>.</td></tr>";
            $str .= "<tr><td class='mainbgbody'>  Première catégorie :  </td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Type : ";
            $dbl = new DBCollection("SELECT id,name FROM BasicEquipment WHERE id_EquipmentType != 33  AND id_EquipmentType != 45 order by name asc", $db, 0, 0);
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_TYPE_1'>";
            
            $selected = "";
            
            while (! $dbl->eof()) {
                if (isset($_POST["LOAD"]) && $previousLoad["type1"] == $dbl->get("id"))
                    $selected = "selected='selected'";
                else
                    $selected = "";
                
                $str .= "<option value='" . $dbl->get("id") . "' " . $selected . ">" . $dbl->get("name") . " </option>";
                $dbl->next();
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Niveau : ";
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_LEVEL_1'>";
            $selected = "";
            for ($i = 1; $i <= 10; $i ++) {
                if (isset($_POST["LOAD"]) && $previousLoad["level1"] == $i)
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Quantité : ";
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_QUANTITY_1'>";
            $selected = "";
            for ($i = 1; $i <= 10; $i ++) {
                if (isset($_POST["LOAD"]) && $previousLoad["quantity1"] == $i)
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Deuxième catégorie :  </td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Type : ";
            $dbl->first();
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_TYPE_2'>";
            $str .= "<option value='0'>--- Aucun --- </option>";
            $selected = "";
            while (! $dbl->eof()) {
                if (isset($_POST["LOAD"]) && isset($previousLoad["type2"]) && $previousLoad["type2"] == $dbl->get("id"))
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $dbl->get("id") . "' " . $selected . ">" . $dbl->get("name") . " </option>";
                $dbl->next();
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Niveau : ";
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_LEVEL_2'>";
            $selected = "";
            for ($i = 1; $i <= 10; $i ++) {
                if (isset($_POST["LOAD"]) && isset($previousLoad["type2"]) && $previousLoad["level2"] == $i)
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Quantité : ";
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_QUANTITY_2'>";
            $selected = "";
            for ($i = 1; $i <= 10; $i ++) {
                if (isset($_POST["LOAD"]) && isset($previousLoad["type2"]) && $previousLoad["quantity2"] == $i)
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Troisième catégorie :  </td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Type : ";
            $dbl->first();
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_TYPE_3'>";
            
            $str .= "<option value='0'>--- Aucun --- </option>";
            $selected = "";
            while (! $dbl->eof()) {
                if (isset($_POST["LOAD"]) && isset($previousLoad["type3"]) && $previousLoad["type3"] == $dbl->get("id"))
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $dbl->get("id") . "' " . $selected . ">" . $dbl->get("name") . " </option>";
                $dbl->next();
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Niveau : ";
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_LEVEL_3'>";
            $selected = "";
            for ($i = 1; $i <= 10; $i ++) {
                if (isset($_POST["LOAD"]) && isset($previousLoad["type3"]) && $previousLoad["level3"] == $i)
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
            }
            $str .= "</select></td></tr>";
            
            $str .= "<tr><td class='mainbgbody'>  Quantité : ";
            $str .= "<select id='target' class='selector' name='CARAVAN_LOAD_QUANTITY_3'>";
            $selected = "";
            for ($i = 1; $i <= 10; $i ++) {
                if (isset($_POST["LOAD"]) && isset($previousLoad["type3"]) && $previousLoad["quantity3"] == $i)
                    $selected = "selected='selected'";
                else
                    $selected = "";
                $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
            }
            $str .= "</select></td></tr>";
            $str .= "<input name='Nc' type='hidden' value='" . $Nc . "' />\n";
            $str .= "<tr><td colspan='3'>";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "<input name='IdCoComList' type='hidden' value='" . $IdCoComList . "' />\n";
            $str .= "<input id='submitbt' type='submit' name='submitbt' style='float: left;' value='Continuer' /></form>";
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
            $str .= "<input name='CARAVAN_LEVEL' type='hidden' value='" . $Nc . "' />\n";
            $str .= "<input name='RETOUR_SEL_LVL' type='hidden' value='RETOUR_SEL_LVL' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "<input id='submitbt' type='submit' name='submitbt' style='float: right;'  value='Retour' /></tr></td>";
        } else {
            $str .= $strTitle;
            if ($curplayer->get("merchant_level") <= 0) {
                $str .= "<table class='maintable insidebuildingleftwidth'>";
                $str .= "<tr><td class='mainbgbody'>";
                $str .= "Votre réputation de marchand n'est pas suffisante pour que vous puissiez organiser une caravane.";
                $str .= "</td></tr>";
            } else {
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
                $str .= "<table class='maintable insidebuildingleftwidth'>";
                $str .= "<tr><td class='mainbgbody'>";
                $str .= "Vous pouvez monter une caravane en fonction de votre niveau marchand. Vous devez payer pour participer au financement de la caravane. Vous n'avez pas le droit de créer une caravane si vous avez déjà une mission commerciale en cours.";
                $str .= "</td></tr>";
                
                // Organiser une caravane étape 1
                
                $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $id, $db, 0, 0);
                if (($dbc->count() == 0) && ($curplayer->get("merchant_level") > 0)) {
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr><td class='mainbgtitle' colspan='5'> <b>Organiser une caravane</b> </td></tr>";
                    $str .= "<tr><td class='mainbgbody'>  ";
                    $str .= " Choisissez le niveau de la caravane : ";
                    $str .= "<select id='target' class='selector' name='CARAVAN_LEVEL'>";
                    if (($curplayer->get("merchant_level") < 1) && ($curplayer->get("merchant_level") > 0))
                        $str .= "<option value='1'>1</option>";
                    else {
                        $selected = "";
                        for ($i = floor($curplayer->get("merchant_level")); $i >= 1; $i --) {
                            if (isset($_POST["RETOUR_SEL_LVL"]) && isset($_POST["CARAVAN_LEVEL"]) && $_POST["CARAVAN_LEVEL"] == $i)
                                $selected = "selected='selected'";
                            else
                                $selected = "";
                            $str .= "<option value='" . $i . "' " . $selected . ">" . $i . "</option>";
                        }
                    }
                    
                    $str .= "</select></td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></tr></td>";
                }
                
                $str .= "</form>";
                $str .= "</table>\n";
                
                // Mission en cours
                
                $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $id, $db, 0, 0);
                
                // heureusement que LT passe partout.
                $dbt = new DBCollection(
                    "SELECT * FROM Player LEFT JOIN Caravan ON Player.id_Caravan=Caravan.id WHERE  Caravan.id_Player=" . $curplayer->get("id") . " AND id_BasicRace=263", $this->db);
                // $dbt=new DBCollection("SELECT id,inbuilding FROM Player WHERE id_BasicRace=263 AND id_Member=".$curplayer->get("id_Member"),$db,0,0);
                
                $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                $str .= "<tr><td class='mainbgtitle' colspan='3'> <b>Mission en cours</b> </td></tr>";
                // $str.="<tr><td class='mainbglabel' colspan='3'> Les seules missions affichées sont les missions valides. Si vous êtes arrivés un peu en retard la mission ne sera pas affichée et elle sera détruite à votre prochaine DLA. </td></tr>";
                $str .= "<tr class='mainbglabel'>  <td>Id</td> <td> Niveau </td> <td> Destination </td>  </tr>";
                if ($dbc->count() == 1) {
                    $dbfb = new DBCollection("SELECT id,id_City,x,y FROM Building WHERE id=" . $dbc->get("id_endbuilding"), $db, 0, 0);
                    $dbfc = new DBCollection("SELECT id,name FROM City WHERE id=" . $dbfb->get("id_City"), $db, 0, 0);
                    
                    $str .= "<tr class='mainbgbody'> <td>" . $dbc->get('id') . " </td><td> " . $dbc->get("level") . " </td> <td> " . $dbfc->get("name") . " (" . $dbfb->get("x") .
                         "/" . $dbfb->get("y") . ") </td></tr>"; // $dbc->get("id_endbuilding")."|".$dbt->get("inbuilding")."|".$dbt->count()." </td> </tr>";
                }
                
                if (($dbc->count() == 1) && ($dbc->get("id_endbuilding") == $curplayer->get("inbuilding")) && ($dbt->count() == 1) &&
                     ($dbc->get("id_endbuilding") == $dbt->get("inbuilding"))) {
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    
                    $str .= "<input name='action' type='hidden' value='" . CARAVAN_TERMINATE . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<tr><td colspan='3'><input id='submitbt' type='submit' name='submitbt' value='Décharger la caravane' /></tr></td>";
                } elseif ($dbc->count() == 1 && $dbc->get("id_startbuilding") == $curplayer->get("inbuilding")) {
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<tr><td colspan='3'><input id='annulation' type='submit' name='annulation' value='Abandonner cette mission' /></tr></td>";
                }
            }
        }
        $str .= "</table>\n";
        
        $str .= "</form>";
        
        return $str;
    }
}
?>
