<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQSalesroom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQSalesroom($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('S A L L E &nbsp;&nbsp;&nbsp; D E S &nbsp;&nbsp;&nbsp; V E N T E S') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        
        if (isset($_POST["Sell"]) && isset($_POST["check"])) {
            $where = InsideBuildingFactory::getCondFromArray($_POST["check"], "Equipment.id", "OR");
            $dbe = new DBCollection("SELECT id,name,extraname  FROM Equipment WHERE " . $where, $db);
            
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr><td class='mainbglabel'>Désirez-vous vraiment vendre ces objets :<b></td></tr>";
            $idEquip = "";
            while (! $dbe->eof()) {
                $str .= "<tr><td class='mainbglabel'>" . $dbe->get("name") . " " . $dbe->get("extraname") . " </b>pour " .
                     floor(EquipFactory::getPriceForSaleWithReductionDurability($dbe->get("id"), $db)) . " PO ?";
                $str . "</tr></td>";
                $idEquip .= $dbe->get("id") . ",";
                $dbe->next();
            }
            
            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Oui' /> </td></tr>";
            $str .= "<input name='action' type='hidden' value='" . SHOP_SELL . "' />";
            $str .= "<input name='ID_EQUIP' type='hidden' value='" . substr($idEquip, 0, strlen($idEquip) - 1) . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</table></form>";
        } else {
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $template = new Template();
            $tmodifier = new Modifier();
            $mission = new Mission();
            
            $dbe = new DBCollection(
                "SELECT " . $eq->getASRenamer("E1", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," . $mission->getASRenamer("Mission", "MISS") .
                     ",mask,wearable,frequency,BasicEquipment.durability AS dur   FROM Equipment E1 left outer join Equipment E2 on E1.id_Equipment\$bag = E2.id LEFT JOIN BasicEquipment  ON E1.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON E1.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON E1.id_Mission=Mission.id WHERE E1.state='Carried' AND ((E1.weared='NO' and E1.id_Equipment\$bag =0) or E2.weared='Yes') AND E1.id_BasicEquipment != 600 AND E1.progress=100 AND E1.templateProgress = 100 AND E1.id_Player=" .
                     $id . " order by EQname", $db);
            
            $array = array();
            $name = "";
            while (! $dbe->eof()) {
                $eq->DBLoad($dbe, "EQ");
                if ($dbe->get("EQMid") != 0) {
                    $checkbox = "<input type='checkbox' name='check[]' value='" . $eq->get("id") . "'>";
                    $eqmodifier->DBLoad($dbe, "EQM");
                    $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                    if ($eq->get("sharpen") > 1)
                        $eqmodifier->addModif("damage", DICE_ADD, $eq->get("sharpen"));
                        
                        // $checkbox="<input type='radio' name='idequip' value='".$eq->get("id")."'>";
                    $name = "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                    
                    if ($eq->get("extraname") != "") {
                        $i = 0;
                        $tmodifier = new Modifier();
                        $template = new Template();
                        $dbt = new DBCollection(
                            "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                                 $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                                 " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                                 $eq->get("id") . " order by Template.pos asc", $db);
                        
                        while (! $dbt->eof()) {
                            $i ++;
                            $template->DBLoad($dbt, "TP");
                            $tmodifier->DBLoad($dbt, "MD");
                            $tmodifier->updateFromTemplateLevel($template->get("level"));
                            $eqmodifier->addModifObj($tmodifier);
                            
                            if ($i == 1) {
                                $extraname = $dbt->get("name1") . "(" . $template->get("level") . ") ";
                            }
                            if ($i == 2) {
                                $extraname .= $dbt->get("name2") . "(" . $template->get("level") . ")";
                            }
                            
                            $dbt->next();
                        }
                        $name .= " " . "<span class='template'>" . $extraname . "</span>";
                    }
                    $eqmodifier->initCharacStr();
                }
                
                $name .= "<br/>";
                if ($eq->get("id_BasicEquipment") <= 499)
                    $name .= localize("Niveau") . " " . $eq->get("level");
                $name .= " (id : " . $eq->get("id") . ") ";
                
                // Affichage de la durability pour les équipements et les sacs
                if ($eq->get("id_EquipmentType") < 28 or ($eq->get("id_EquipmentType") >= 40 and $eq->get("id_EquipmentType") <= 44) or $eq->get("id_EquipmentType") == 46)
                    $name .= " - " . EquipFactory::getDurability($eq, $db);
                    // Affichages des bonus de caractéristique pour les équipements
                $name .= "<br/>";
                if ($eq->get("id_EquipmentType") < 30) {
                    $modstr = "";
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0")
                            $modstr .= translateAttShort($key) . " : " . $tmp . " | ";
                    }
                    $name .= "(" . substr($modstr, 0, - 3) . ")";
                }
                
                $rachat = (50 - 4 * (max(0, $eq->get("level") - 3))) . "%";
                $price = EquipFactory::getPriceForSaleWithReductionDurability($eq->get("id"), $db);
                
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $price,
                        "class='mainbgbody' align='right'"
                    ),
                    "3" => array(
                        $rachat,
                        "class='mainbgbody' align='right'"
                    )
                );
                
                $dbe->next();
            }
            
            // ------------- Affichage -------------------
            
            $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d' target='_self'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $str .= createTable(4, $array, array(), 
                array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("Nom et caractéristiques"),
                        "class='mainbglabel' width='60%' align='left'",
                        "EQ.name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Prix"),
                        "class='mainbglabel' width='15%' align='right'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("%"),
                        "class='mainbglabel' width='15%' align='right'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), "class='maintable insidebuildingleftwidth'", "formid", "order");
            
            $str .= "<tr><td class='mainbgtitle'><input id='Sell' type='submit' name='Sell' value='" . localize("Vendre") . "' /></td></tr>";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        }
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
