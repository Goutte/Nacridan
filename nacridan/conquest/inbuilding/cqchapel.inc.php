<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
class CQChapel extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQChapel($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $money = $curplayer->get("money");
        $err = $this->err;
        $str = "";
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('C H A P E L L E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr><td> Vous vous trouvez dans la chapelle du temple. Dans ce lieu saint, aucune action n'est possible. Vous êtes donc protégé de tout danger tant que vous y restez.";
        $str .= "</td><tr/></table>\n";
        
        return $str;
    }
}
?>
