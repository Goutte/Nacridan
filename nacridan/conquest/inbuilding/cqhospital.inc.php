<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQHospital extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQHospital($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $village = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('H Ô P I T A L  &nbsp;&nbsp;&nbsp;D U   &nbsp;&nbsp;&nbsp;T E M P L E') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        // on vérifie que le formulaire a été soumis
        if (isset($_POST['hospitalchoice'])) {
            if (BasicActionFactory::canEnterTemple($this->curplayer, $this->curplayer->get("inbuilding"), $this->db)) {
                $hospitalchoice = $_POST['hospitalchoice'];
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                
                switch ($hospitalchoice) {
                    case "heal":
                        if ($curplayer->get("ap") < TEMPLE_HEAL_AP) {
                            $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                        } elseif ($curplayer->get("money") < min($curplayer->get("level"), CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_HEAL, $db))) {
                            $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                        } else {
                            $temple = new Building();
                            $temple->load($curplayer->get("inbuilding"), $this->db);
                            
                            $str .= "<td><input name='TEMPLELEVEL' type='hidden' value='" . $temple->get("level") . "' /></td>";
                            $str .= "<td><input name='action' type='hidden' value='" . TEMPLE_HEAL . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td>Le moine s'apprête à vous soigner pour " .
                                 min($curplayer->get("level"), CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_HEAL, $db)) . " PO (et " . TEMPLE_HEAL_AP .
                                 " PA).</td></tr>";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        }
                        break;
                    
                    case "blessing":
                        if ($curplayer->get("ap") < TEMPLE_BLESSING_AP) {
                            $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                        } elseif ($curplayer->get("money") < CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_BLESSING, $db)) {
                            $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                        } else {
                            $temple = new Building();
                            $temple->load($curplayer->get("inbuilding"), $this->db);
                            
                            $str .= "<td><input name='TEMPLELEVEL' type='hidden' value='" . $temple->get("level") . "' /></td>";
                            $str .= "<td><input name='action' type='hidden' value='" . TEMPLE_BLESSING . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td>Le moine s'apprête à vous bénir pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_BLESSING, $db) .
                                 " PO (et " . TEMPLE_BLESSING_AP . " PA).</td></tr>";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        }
                        break;
                    
                    case "resurrect":
                        
                        if ($curplayer->get("money") < CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_RESURRECT, $db)) {
                            $str .= "<tr><td>Vous n'avez pas assez de P0 pour effectuer cette action</td></tr>";
                        } else {
                            
                            $str .= "<td><input name='action' type='hidden' value='" . TEMPLE_RESURRECT . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td>Le moine s'apprête à fixer votre âme à ce temple pour " .
                                 CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_RESURRECT, $db) . " PO.</td></tr>";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        }
                        break;
                    
                    case "palace":
                        
                        if ($curplayer->get("money") < TEMPLE_PALACE_PRICE) {
                            $str .= "<tr><td>Vous n'avez pas assez de PO pour effectuer cette action</td></tr>";
                        } elseif ($dbh->get("captured") != 3 && $dbh->get("captured") != 0) {
                            $str .= "<tr><td> Action impossible : ce village est déjà sous la protecteur d'un joueur.<td></tr>";
                        } else {
                            $str .= "<td><input name='action' type='hidden' value='" . TEMPLE_PALACE . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $sp = CityFactory::getBuildingSP(11, $db);
                            
                            $str .= "<tr class='mainbglabel'><td colspan=2>Vous êtes sur le point d'ordonner la construction d'un palais. La durée de construction sera de " .
                                 ceil($sp / CONSTRUCTION_SPEED_MAX) . " à " . ceil($sp / CONSTRUCTION_SPEED_MIN) .
                                 " jours. Une fois achevé, vous serez officiellement proclamé gouverneur du village et vous en obtiendrez la gestion.";
                            $str .= " Cette action vous coûtera " . TEMPLE_PALACE_PRICE . " pièces d'or et " . TEMPLE_PALACE_AP . " PA. </br></td></tr>";
                            $item = array();
                            
                            $str .= "</td></tr><tr class='mainbglabel'><td colspan=2> Choississez un emplacement vide dans le village pour construire le palais </td></tr>";
                            $str .= "<tr><td><select class='selector cqattackselectorsize' name='Emplacement'>";
                            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un emplacement --") . "</option>";
                            for ($i = - 3; $i < 4; $i ++) {
                                for ($j = - 3; $j < 4; $j ++) {
                                    if (BasicActionFactory::freePlace($xp + $i, $yp + $j, $map, $this->db, 1) && distHexa($xp, $yp, $xp + $i, $yp + $j) < 4)
                                        $item[] = array(
                                            localize("(X= " . ($xp + $i) . " , Y= " . ($yp + $j) . ")") => ($xp + $i) . "," . ($yp + $j)
                                        );
                                }
                            }
                            foreach ($item as $arr) {
                                foreach ($arr as $key => $value)
                                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                            }
                            $str .= "</select>";
                            // $str.="<input name='Emplacement' type='hidden' value='' />\n";
                            /* Code pour nommer village </br>Votre village ce nomme".$village->get('name')." */
                            $str .= "<td><tr></td></tr>";
                            $str .= "<td class='mainbgtitle'> <b>N O M M E R &nbsp;&nbsp;&nbsp; V O T R E  &nbsp;&nbsp;&nbsp; V I L L A G E</b> \n";
                            $str .= "</td></tr>\n";
                            $str .= "<tr>\n";
                            $str .= "<tr><td class='mainbgbody'> Ce village ne porte actuellement aucun nom </tr></td>";
                            $str .= "<tr><td class='mainbgbody'> <label>Nom du village: <input type='text' name='nameVillage' value='nom village' /> </label> </tr></td>"; // champ de type text pour nommer le village
                            
                            /* Fin nom village */
                            
                            $str .= "<tr><td class='mainbgbody'><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        }
                        break;
                    
                    default:
                        break;
                }
                $str .= "</table></form>";
            }
        } else {
            if (BasicActionFactory::canEnterTemple($this->curplayer, $this->curplayer->get("inbuilding"), $this->db)) {
                $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr><td class='mainbglabel'>Le moine peut mettre ses services de guérisseur à votre disposition :</td></tr>";
                $dbc = new DBCollection(
                    "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $curplayer->get("inbuilding"), 
                    $db);
                if ($dbc->get("captured") > 3)
                    $price = CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_HEAL, $db);
                else
                    $price = min($curplayer->get("level"), 5);
                
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='hospitalchoice' value='heal' id='heal' />" . "<label for='heal'>Soin, pour " . $price .
                     " pièce(s) d'or (" . TEMPLE_HEAL_AP . "PA)</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='hospitalchoice' value='blessing' id='blessing' />" .
                     "<label for='blessing'>Effacer les malédictions, pour " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_BLESSING, $db) .
                     " pièces d'or (" . TEMPLE_BLESSING_AP . " PA)</label></td></tr>";
                
                $dbb = new DBCollection("SELECT id,x,y  FROM Building WHERE id=" . $curplayer->get("resurrectid"), $db);
                if ($dbb->count() > 0) {
                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='hospitalchoice' value='resurrect' id='resurrect' />" .
                         "<label for='resurrect'>Choisir ce temple comme lieu de votre prochaine prochaine résurrection, pour " .
                         CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TEMPLE_RESURRECT, $db) .
                         " pièces d'or. Votre temple de résurrection actuel se trouve en (X=" . $dbb->get("x") . "/Y=" . $dbb->get("y") . ") </label></td></tr>";
                }
                if ($dbh->get("captured") == 3 || $dbh->get("captured") == 0)
                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='hospitalchoice' value='palace' id='palace' />" .
                         "<label for='resurrect'>Ordonner la construction d'un palais pour devenir gouverneur du village (" . TEMPLE_PALACE_PRICE . " po). </label></td></tr>";
                
                $str .= "<tr><td><input type='submit' name='ok' value='Ok'/> </td></tr>";
                $str .= "</table>";
                $str .= "</form>";
            }
        }
        
        return $str;
    }
}
?>
	  
