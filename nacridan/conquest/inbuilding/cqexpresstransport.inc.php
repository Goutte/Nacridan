<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQExpressTransport extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQExpressTransport($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        else
            $type = 1;
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('T R A N S P O R T &nbsp;&nbsp;&nbsp; D &nbsp;&nbsp;&nbsp; O B J E T S') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $target = "/conquest/conquest.php?center=view2d&type=" . $type;
        $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
        
        if (isset($_POST["Prendre"]) && isset($_POST["Colis"])) {
            list ($exp, $date) = explode(",", $_POST["Colis"], 2);
            
            $dbe = new DBCollection("SELECT * FROM Equipment WHERE id_Caravan=" . $exp . " AND id_Warehouse=" . $curplayer->get("id") . " AND collected='" . $date . "'", $db);
            
            $price = $dbe->get("po");
            
            if ($price > $curplayer->get("money"))
                $str .= "<tr><td class='mainbglabel'>  Vous n'avez pas suffisament d'argent pour retirer ce colis. </td></tr>";
            else {
                require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
                InsideBuildingFactory::collectPackage($curplayer, $dbe, $price, $err, $db);
            }
        }
        
        if (isset($_POST["Recuperer"]) && isset($_POST["Colis"])) {
            list ($dest, $date) = explode(",", $_POST["Colis"], 2);
            
            $dbe = new DBCollection("SELECT * FROM Equipment WHERE id_Caravan=" . $curplayer->get("id") . " AND id_Warehouse=" . $dest . " AND collected='" . $date . "'", $db);
            
            $price = 2 * CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SEND_PACKAGE, $db);
            
            if ($price > $curplayer->get("money"))
                $str .= "<tr><td class='mainbglabel'>  Vous n'avez pas suffisament d'argent pour retirer ce colis. </td></tr>";
            else {
                require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
                InsideBuildingFactory::retrievePackage($curplayer, $dbe, $price, $err, $db);
            }
        }
        
        if (isset($_POST["submitbt2"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
                InsideBuildingFactory::sendPackage($curplayer, $_POST["ID_EQUIP"], $_POST["ID_PLAYERDEST"], $_POST["ID_BUILDINGDEST"], $_POST["MONEY"], $err, $db);
            }
        }
        
        if (isset($_POST["valider"])) {
            $arrayidequip = array();
            $good = 1;
            $champs = 0;
            for ($i = 1; $i < 5; $i ++) {
                if ($_POST["ID_OBJECT" . $i] != 0) {
                    $champs = 1;
                    $equip = new Equipment();
                    $equip->load($_POST["ID_OBJECT" . $i], $db);
                    if ($equip->get("id_Player") != $curplayer->get("id"))
                        $good = 0;
                    
                    $arrayidequip[] = $equip->get("id");
                }
            }
            
            $dbp = new DBCollection("SELECT id FROM Player WHERE name='" . $_POST["playerdest"] . "'", $db);
            if ($_POST["playerdest"] == "")
                $str .= "<tr><td class='mainbglabel'>  Aucun destinataire n'a été choisi. </td></tr>";
            
            elseif (strcasecmp($_POST["playerdest"], $curplayer->get("name")) == 0)
                $str .= "<tr><td class='mainbglabel'>  Vous ne pouvez pas vous envoyer un colis à vous même. </td></tr>";
            
            elseif ($dbp->count() == 0)
                $str .= "<tr><td class='mainbglabel'>  Le nom du destinataire n'est pas valide </td></tr>";
            
            elseif ($_POST["PRICE"] <= 0)
                $str .= "<tr><td class='mainbglabel'>  Le prix demandé n'est pas valide </td></tr>";
            
            elseif (! $champs)
                $str .= "<tr><td class='mainbglabel'>  Aucun n'objet n'a été selectionné. </td></tr>";
            
            elseif ($_POST["ID_BUILDING"] == 0)
                $str .= "<tr><td class='mainbglabel'> La destination n'a pas été choisie. </td></tr>";
            
            elseif (! $good)
                $str .= "<tr><td class='mainbglabel'> Vous ne pouvez pas envoyer ces objets car l'un d'eux ne vous appartient plus. </td></tr>";
            
            elseif (count(array_unique($arrayidequip)) != count($arrayidequip))
                $str .= "<tr><td class='mainbglabel'> Vous avez selectionné deux fois le même objet </td></tr>";
            
            else {
                $where = EquipFactory::getCondFromArray($arrayidequip, "Equipment.id", "OR");
                $dbe = new DBCollection("SELECT * FROM Equipment WHERE " . $where, $db);
                $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $_POST["ID_BUILDING"], $db);
                if ($dbe->count() > 1)
                    $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point d'envoyer ces objets en (" . $dbb->get("x") . "/" . $dbb->get("y") . ") à l'attention de " . $_POST["playerdest"] . " : </td></tr>";
                else
                    $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point d'envoyer cet objet en (" . $dbb->get("x") . "/" . $dbb->get("y") . ") à l'attention de " . $_POST["playerdest"] . " : </td></tr>";
                
                while (! $dbe->eof()) {
                    $str .= "<tr><td class='mainbglabel'> - " . $dbe->get("name") . " niveau " . $dbe->get("level") . " " . $dbe->get("extraname") . " (" . $dbe->get("id") . ")";
                    $str .= "</tr></td>";
                    $dbe->next();
                }
                
                $str .= "<tr><td class='mainbglabel'>" . $_POST["playerdest"] . " pourra les récupérer en payant " . $_POST["PRICE"] . " PO";
                $str .= "<tr><td class='mainbglabel'>Il vous en coutera un total de " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SEND_PACKAGE, $db) . " PO";
                $time = ceil((floor(distHexa($curplayer->get("x"), $curplayer->get("y"), $dbb->get("x"), $dbb->get("y")) / 24) + 3) / 2);
                // $str.="<tr><td class='mainbglabel'>Temps estimé : ".$time." jours";
                $str .= "<tr><td><input id='submitbt2' type='submit' name='submitbt2' value='Accepter' /> </td></tr>";
                $str .= "<input name='ID_EQUIP' type='hidden' value='" . $where . "' />";
                $str .= "<input name='ID_PLAYERDEST' type='hidden' value='" . $dbp->get("id") . "' />";
                $str .= "<input name='ID_BUILDINGDEST' type='hidden' value='" . $dbb->get("id") . "' />";
                $str .= "<input name='MONEY' type='hidden' value='" . $_POST["PRICE"] . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</table></form>";
        } else {
            
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'>\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Expédition') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Réception') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=3' class='tabmenu'>" . localize('Expédition en attente') . "</a> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            if ($type == 1) {
                // $str.="<form name='form' method='POST' target='_self'>";
                $str .= "<tr><td class='mainbgtitle'><b> E X P E D I T I O N </b></td></tr>";
                
                $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db);
                $gap = 60 * $dbb->get("level");
                $xp = $curplayer->get("x");
                $yp = $curplayer->get("y");
                $str .= "<td class='mainbgtitle'> Entrez le nom du destinataire <input name='playerdest' id='playerdest' autocomplete='off' maxlength=30 size=30 value=''\"></td><td>";
                // AND Building.id!=".$curplayer->get("inbuilding")."
                $dbe = new DBCollection("SELECT *,Building.id as id, Building.x as x, Building.y as y, City.name as cityname FROM Building LEFT JOIN City ON Building.id_City = City.id WHERE Building.id_BasicBuilding=4   AND (abs(Building.x-" . $xp . ") + abs(Building.y-" . $yp . ") + abs(Building.x+Building.y-" . $xp . "-" . $yp . "))/2 <= (" . $gap . "+(40*Building.level))", $db);
                $str .= "<tr><td class='mainbgbody'>  ";
                $str .= " Choisissez le comptoir commercial destination : ";
                $str .= "<select id='target' class='selector' name='ID_BUILDING'>";
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un Comptoir --") . "</option>";
                $item = array();
                while (! $dbe->eof()) {
                    if ($dbe->get("id") != $curplayer->get("inbuilding"))
                        $item[] = array(
                            $dbe->get("cityname") . " (" . $dbe->get("x") . "/" . $dbe->get("y") . ")" => $dbe->get("id")
                        );
                    $dbe->next();
                }
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select></td></tr>";
                
                $dbe = new DBCollection("SELECT * FROM Equipment WHERE state='Carried' AND weared='NO' AND id_Player=" . $id, $db);
                $str .= "<tr><td class='mainbgbody'>  ";
                $str .= "<b>Choisissez les objets à livrer</b> (maximum 4)</td></tr>";
                for ($i = 1; $i < 5; $i ++) {
                    $str .= "<tr><td class='mainbgbody'>  Objet  " . $i . " : ";
                    $str .= "<select id='target' class='selector' name='ID_OBJECT" . $i . "'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un objet --") . "</option>";
                    $item = array();
                    $item[] = array(
                        localize("-- Mon inventaire --") => - 1
                    );
                    $dbe->first();
                    while (! $dbe->eof()) {
                        if ($dbe->get("name") != "" && $dbe->get("id_Equipment\$bag") == 0) {
                            if ($dbe->get("id_BasicEquipment") > 499)
                                $item[] = array(
                                    localize($dbe->get("name")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            else
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                        }
                        $dbe->next();
                    }
                    
                    if ($id_Bag = playerFactory::getIdBagByNum($id, 1, $db)) {
                        $item[] = array(
                            localize("-- Sac de gemmes --") => - 1
                        );
                        $dbe->first();
                        while (! $dbe->eof()) {
                            if ($dbe->get("id_Equipment\$bag") == $id_Bag)
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            
                            $dbe->next();
                        }
                    }
                    
                    if ($id_Bag = playerFactory::getIdBagByNum($id, 2, $db)) {
                        $item[] = array(
                            localize("-- Sac de matières premières --") => - 1
                        );
                        $dbe->first();
                        while (! $dbe->eof()) {
                            if ($dbe->get("id_Equipment\$bag") == $id_Bag)
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            
                            $dbe->next();
                        }
                    }
                    
                    if ($id_Bag = playerFactory::getIdBagByNum($id, 3, $db)) {
                        $item[] = array(
                            localize("-- Sac d'herbes --") => - 1
                        );
                        $dbe->first();
                        while (! $dbe->eof()) {
                            if ($dbe->get("id_Equipment\$bag") == $id_Bag)
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            
                            $dbe->next();
                        }
                    }
                    
                    if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                        $item[] = array(
                            localize("-- Ceinture --") => - 1
                        );
                        $dbe->first();
                        while (! $dbe->eof()) {
                            if ($dbe->get("id_Equipment\$bag") == $id_Bag)
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            
                            $dbe->next();
                        }
                    }
                    
                    if ($id_Bag = playerFactory::getIdBagByNum($id, 5, $db)) {
                        $item[] = array(
                            localize("-- Carquois --") => - 1
                        );
                        $dbe->first();
                        while (! $dbe->eof()) {
                            if ($dbe->get("id_Equipment\$bag") == $id_Bag)
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            
                            $dbe->next();
                        }
                    }
                    if ($id_Bag = playerFactory::getIdBagByNum($id, 6, $db)) {
                        $item[] = array(
                            localize("-- Trousse à outils --") => - 1
                        );
                        $dbe->first();
                        while (! $dbe->eof()) {
                            if ($dbe->get("id_Equipment\$bag") == $id_Bag)
                                $item[] = array(
                                    localize($dbe->get("name")) . " " . localize("Niveau") . " " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                                );
                            
                            $dbe->next();
                        }
                    }
                    
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value) {
                            if ($value == - 1)
                                $str .= "<optgroup class='group' label='" . $key . "' />";
                            else
                                $str .= "<option value='" . $value . "'>" . $key . "</option>";
                        }
                    }
                    $str .= "</select></td></tr>";
                }
                
                $str .= "<tr><td class='mainbgtitle'> Le destinataire pourra récupérer ces objets en payant un montant total de : ";
                $str .= "<input type='text' name='PRICE' value'' maxlength=9 size=10/> " . localize("PO") . " qui seront virés sur votre compte bancaire.</td></tr>";
                $str .= "<tr><td class='mainbglabel'>Il vous en coutera un total de " . CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SEND_PACKAGE, $db) . " PO, payable de suite.";
                $str .= "<tr><td class='mainbgtitle'><input id='valider' type='submit' name='valider' value='" . localize("Valider") . "' /></td></tr>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>\n";
            }
            if ($type == 2) {
                $colis = array();
                // $str.="<form name='form' method='POST' target='_self'>";
                $str .= "<tr><td class='mainbgtitle'><b> R E C E P T I O N </b></td></tr>";
                
                $dbe = new DBCollection("SELECT id_Caravan as exp, id_Warehouse as dest,collected FROM Equipment WHERE id_Player=0 AND id_Shop=" . $curplayer->get("inbuilding") . " GROUP BY id_Warehouse, collected", $db);
                
                while (! $dbe->eof()) {
                    $dbw = new DBCollection("SELECT * FROM Equipment WHERE id_Shop=" . $curplayer->get("inbuilding") . " AND id_Player=0 AND id_Warehouse=" . $dbe->get("dest") . " AND collected='" . $dbe->get("collected") . "'", $db);
                    $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("dest"), $db);
                    $dbf = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("exp"), $db);
                    $col1 = $dbf->count() > 0 ? $dbf->get("name") : "Inconnu";
                    $col2 = $dbp->count() > 0 ? $dbp->get("name") : "Inconnu";
                    $enable = "disabled";
                    if ($curplayer->get("id") == $dbe->get("dest"))
                        $enable = "";
                    
                    $col0 = "<input type='radio' name='Colis' value='" . $dbe->get("exp") . "," . $dbe->get("collected") . "'" . $enable . ">";
                    $price = 0;
                    // $str.="<tr><td class='mainbglabel'>";
                    $col3 = "";
                    $price = $dbw->get("po");
                    while (! $dbw->eof()) {
                        
                        $col3 .= $dbw->get("name") . " niveau " . $dbw->get("level") . " " . $dbw->get("extraname");
                        $col3 .= "<br>";
                        $dbw->next();
                    }
                    
                    // $str.="<tr><td class='mainbgbody'> ";
                    $col4 = $price;
                    
                    $colis[] = array(
                        "0" => array(
                            $col0,
                            "class='mainbgbody' align='left'"
                        ),
                        "1" => array(
                            $col1,
                            "class='mainbgbody' align='left'"
                        ),
                        "2" => array(
                            $col2,
                            "class='mainbgbody' align='left'"
                        ),
                        "3" => array(
                            $col3,
                            "class='mainbgbody' align='left'"
                        ),
                        "4" => array(
                            $col4,
                            "class='mainbgbody' align='right'"
                        )
                    );
                    $dbe->next();
                }
                
                if (count($colis) > 0) {
                    $str .= createTable(5, $colis, array(
                        array(
                            localize("Les colis disponibles"),
                            "class='mainbgtitle' colspan=5 align='left'"
                        )
                    ), array(
                        array(
                            "",
                            "class='mainbglabel' width='5%' align='center'"
                        ),
                        array(
                            localize("Expéditeur"),
                            "class='mainbglabel' width='15%' align='center'"
                        ),
                        array(
                            localize("Destinataire"),
                            "class='mainbglabel' width='15%' align='center'"
                        ),
                        array(
                            localize("Contenance"),
                            "class='mainbglabel' width='50%' align='center'"
                        ),
                        array(
                            localize("Prix"),
                            "class='mainbglabel' width='5%' align='left'"
                        )
                    ), "class='maintable' style='width:480px'");
                    
                    $str .= "<tr><td class='mainbgtitle'><input id='Prendre' type='submit' name='Prendre' value='" . localize("Récupérer") . "' /></td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                }
                if (count($colis) == 0) {
                    $str .= "<tr><td class='mainbgbody'>  ";
                    $str .= "Il n'y a aucun colis à récupérer</td></tr>";
                }
                
                $str .= "</form>\n";
            }
            
            if ($type == 3) {
                $colis = array();
                // $str.="<form name='form' method='POST' target='_self'>";
                $str .= "<tr><td class='mainbgtitle'><b> E X P E D I T I O N&nbsp;&nbsp;E N&nbsp;&nbsp;A T T E N T E </b></td></tr>";
                
                $dbe = new DBCollection("SELECT Equipment.id_Caravan as exp, Equipment.id_Warehouse as dest,Equipment.collected,Building.x, Building.y FROM Equipment inner join Building on Building.id = Equipment.id_Shop WHERE Equipment.id_Player=0 AND Equipment.id_Caravan = " . $curplayer->get("id") . " GROUP BY Equipment.id_Warehouse, Equipment.collected", $db);
                
                while (! $dbe->eof()) {
                    $dbw = new DBCollection("SELECT * FROM Equipment WHERE id_Player=0 AND id_Warehouse=" . $dbe->get("dest") . " AND collected='" . $dbe->get("collected") . "'", $db);
                    $dbp = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("dest"), $db);
                    $dbf = new DBCollection("SELECT name FROM Player WHERE id=" . $dbe->get("exp"), $db);
                    $col1 = $dbf->get("name");
                    $col2 = $dbp->get("name");
                    $enable = "disabled";
                    if ($curplayer->get("id") == $dbe->get("exp"))
                        $enable = "";
                    
                    $col0 = "<input type='radio' name='Colis' value='" . $dbe->get("dest") . "," . $dbe->get("collected") . "'" . $enable . ">";
                    $price = 0;
                    // $str.="<tr><td class='mainbglabel'>";
                    $col3 = "";
                    $price = $dbw->get("po");
                    while (! $dbw->eof()) {
                        
                        $col3 .= $dbw->get("name") . " niveau " . $dbw->get("level") . " " . $dbw->get("extraname");
                        $col3 .= "<br>";
                        $dbw->next();
                    }
                    
                    // $str.="<tr><td class='mainbgbody'> ";
                    $col4 = $price;
                    $col5 = "Comptoir en (X=" . $dbe->get("x") . ",Y=" . $dbe->get("y") . ")";
                    $colis[] = array(
                        "0" => array(
                            $col0,
                            "class='mainbgbody' align='left'"
                        ),
                        "1" => array(
                            $col1,
                            "class='mainbgbody' align='left'"
                        ),
                        "2" => array(
                            $col2,
                            "class='mainbgbody' align='left'"
                        ),
                        "3" => array(
                            $col3,
                            "class='mainbgbody' align='left'"
                        ),
                        "4" => array(
                            $col4,
                            "class='mainbgbody' align='right'"
                        ),
                        "5" => array(
                            $col5,
                            "class='mainbgbody' align='left'"
                        )
                    );
                    $dbe->next();
                }
                
                if (count($colis) > 0) {
                    $str .= createTable(6, $colis, array(
                        array(
                            localize("Les colis disponibles"),
                            "class='mainbgtitle' colspan=6 align='left'"
                        )
                    ), array(
                        array(
                            "",
                            "class='mainbglabel' width='5%' align='center'"
                        ),
                        array(
                            localize("Expéditeur"),
                            "class='mainbglabel' width='15%' align='center'"
                        ),
                        array(
                            localize("Destinataire"),
                            "class='mainbglabel' width='15%' align='center'"
                        ),
                        array(
                            localize("Contenance"),
                            "class='mainbglabel' width='30%' align='center'"
                        ),
                        array(
                            localize("Prix"),
                            "class='mainbglabel' width='5%' align='left'"
                        ),
                        array(
                            localize("Lieu"),
                            "class='mainbglabel' width='20%' align='left'"
                        )
                    ), "class='maintable' style='width:480px'");
                    $str .= "<table class='maintable insidebuildingleftwidth'>\n";
                    $str .= "<tr><td class='mainbglabel'>Il vous en coutera un total de " . 2 * CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), SEND_PACKAGE, $db) . " PO, payable de suite.";
                    $str .= "<tr><td class='mainbgtitle'><input id='Prendre' type='submit' name='Recuperer' value='" . localize("Récupérer") . "' /></td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                }
                if (count($colis) == 0) {
                    $str .= "<tr><td class='mainbgbody'>  ";
                    $str .= "Il n'y a aucun colis à récupérer</td></tr>";
                }
                
                $str .= "</form>\n";
            }
        }
        
        if ($err != "") {
            $str = "<table class='maintable' width='480px'>\n";
            $str .= "<tr><td class='mainbgtitle' width='480px'>" . $err . "</td></tr></table>";
        }
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
