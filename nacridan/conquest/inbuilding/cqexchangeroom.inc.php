<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQExchangeRoom extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQExchangeRoom($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        
        $str = "";
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('S A L L E  D E S  E C H A N G E S') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        elseif (isset($_POST["type"]))
            $type = $_POST["type"];
        else
            $type = 1;
        
        if ((isset($_POST["Buy"]) || isset($_POST["Put"])) && isset($_POST["idequip"])) {
            require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
            $dbe = new DBCollection("SELECT id,id_EquipmentType, name,extraname,id_BasicEquipment FROM Equipment WHERE id=" . quote_smart($_POST["idequip"]), $db);
            
            if (($dbe->get("id_BasicEquipment") >= 207 && $dbe->get("id_BasicEquipment") <= 212) || $dbe->get("id_BasicEquipment") == 220) {
                $price = EquipFactory::getPriceForSaleWithReductionDurability($dbe->get("id"), $db);
            } else {
                $price = EquipFactory::getPriceEquipment($dbe->get("id"), $db);
            }
            switch ($type) {
                case 1:
                    $price = ceil($price * CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), EXCHANGE_OBJECT, $db) / 100);
                    break;
                case 2:
                    $price = ceil($price * CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), EXCHANGE_GET_OBJECT, $db) / 100);
                    break;
            }
            
            if ($type == 2 && $curplayer->get("money") <= $price)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez d'or pour acheter cet objet.</td></tr>";
            elseif ($type == 2 && PlayerFactory::checkingInvFullForObject($curplayer, $dbe->get("id_EquipmentType"), $param, $db) == - 1)
                $str .= "<tr><td class='mainbgtitle'> Vous n'avez pas assez de place dans vos sacs.</td></tr>";
            elseif ($type == 1 && EquipFactory::getPriceToRepair($dbe->get("id"), $db) > 0)
                $str .= "<tr><td class='mainbgtitle'>L'object n'est pas dans un état impécable, veuillez le réparer.</td></tr>";
            else {
                
                $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                switch ($type) {
                    case 1:
                        $str .= "<tr><td class='mainbglabel'>Vous allez vendre un(e) <b> " . $dbe->get("name") . " " . $dbe->get("extraname") . " </b>pour " . $price . " PO.";
                        break;
                    case 2:
                        $str .= "<tr><td class='mainbglabel'>Vous allez acheter un(e) <b> " . $dbe->get("name") . " " . $dbe->get("extraname") . " </b>pour " . $price . " PO.";
                        break;
                }
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' /> </td></tr>";
                switch ($type) {
                    case 1:
                        $str .= "<input name='action' type='hidden' value='" . EXCHANGE_OBJECT . "' />";
                        break;
                    case 2:
                        $str .= "<input name='action' type='hidden' value='" . EXCHANGE_GET_OBJECT . "' />";
                        break;
                }
                $str .= "<input name='ID_EQUIP' type='hidden' value='" . $dbe->get("id") . "' />";
                $str .= "<input name='type' type='hidden' value='" . $type . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</table></form>";
            }
        } else {
            // Affichage des objets en vente ou a acheter
            switch ($type) {
                case 1:
                    $cond = " E1.state='Carried' AND (( E1.weared='NO' and E1.id_Equipment\$bag =0) or E2.weared='Yes') AND E1.id_BasicEquipment != 600 AND E1.progress=100 AND E1.templateProgress = 100 AND E1.id_Player=" .
                         $curplayer->get("id");
                    break;
                case 2:
                    $cond = " E1.id_Equipment\$bag =0 AND E1.id_Shop=" . $curplayer->get("inbuilding");
                    break;
            }
            
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $template = new Template();
            $tmodifier = new Modifier();
            $mission = new Mission();
            
            $dbe = new DBCollection(
                "SELECT " . $eq->getASRenamer("E1", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," . $mission->getASRenamer("Mission", "MISS") .
                     ",mask,wearable,frequency,BasicEquipment.durability AS dur FROM Equipment E1 left outer join Equipment E2 on E1.id_Equipment\$bag = E2.id LEFT JOIN BasicEquipment ON E1.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON E1.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON E1.id_Mission=Mission.id WHERE " .
                     $cond, $db);
            
            $array = array();
            
            while (! $dbe->eof()) {
                
                $eq->DBLoad($dbe, "EQ");
                if ($dbe->get("EQMid") != 0) {
                    $eqmodifier->DBLoad($dbe, "EQM");
                    $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                    
                    $checkbox = "<input type='radio' name='idequip' value='" . $eq->get("id") . "'>";
                    $name = "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                    
                    if ($eq->get("extraname") != "") {
                        $i = 0;
                        $tmodifier = new Modifier();
                        $template = new Template();
                        $dbt = new DBCollection(
                            "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                                 $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                                 " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                                 $eq->get("id") . " order by Template.pos asc", $db);
                        
                        while (! $dbt->eof()) {
                            $i ++;
                            $template->DBLoad($dbt, "TP");
                            $tmodifier->DBLoad($dbt, "MD");
                            $tmodifier->updateFromTemplateLevel($template->get("level"));
                            $eqmodifier->addModifObj($tmodifier);
                            
                            if ($i == 1)
                                $extraname = $dbt->get("name1") . "(" . $template->get("level") . ") ";
                            if ($i == 2)
                                $extraname .= $dbt->get("name2") . "(" . $template->get("level") . ")";
                            
                            $dbt->next();
                        }
                        $name .= " " . "<span class='template'>" . $extraname . "</span>";
                    }
                    $eqmodifier->initCharacStr();
                }
                
                $name .= "<br/>";
                $name .= localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")  <br/>";
                
                // Affichages des bonus de caractéristique pour les équipements
                if ($eq->get("id_EquipmentType") < 30) {
                    $modstr = "";
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0")
                            $modstr .= translateAttShort($key) . " : " . $tmp . " | ";
                    }
                    $name .= "(" . substr($modstr, 0, - 3) . ")";
                }
                
                if (($dbe->get("EQid_BasicEquipment") >= 207 && $dbe->get("EQid_BasicEquipment") <= 212) || $dbe->get("EQid_BasicEquipment") == 220) {
                    $price = EquipFactory::getPriceForSaleWithReductionDurability($dbe->get("EQid"), $db);
                } else {
                    $price = EquipFactory::getPriceEquipment($dbe->get("EQid"), $db);
                }
                
                switch ($type) {
                    case 1:
                        $price = ceil($price * CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), EXCHANGE_OBJECT, $db) / 100);
                        break;
                    case 2:
                        $price = ceil($price * CityFactory::getBuildingActionProfit($curplayer->get("inbuilding"), EXCHANGE_GET_OBJECT, $db) / 100);
                        break;
                }
                
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $price,
                        "class='mainbgbody' align='right'"
                    )
                );
                
                $dbe->next();
            }
            
            // ------------- Affichage -------------------
            
            $target = "/conquest/conquest.php?center=view2d&type=" . $type;
            $str .= "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'>\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=1' class='tabmenu'>" . localize('Dépôts') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=view2d&type=2' class='tabmenu'>" . localize('Prendre') . "</a> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'><b>\n";
            switch ($type) {
                case 1:
                    $str .= localize("D E P O T");
                    break;
                case 2:
                    $str .= localize("P R E N D R E");
                    break;
            }
            $str .= "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= createTable(3, $array, array(), 
                array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("Nom et caractéristiques"),
                        "class='mainbglabel' width='60%' align='left'",
                        "EQ.name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Prix"),
                        "class='mainbglabel' width='15%' align='right'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), "class='maintable insidebuildingleftwidth'", "formid", "order");
            
            switch ($type) {
                case 1:
                    $str .= "<tr><td class='mainbgtitle'><input id='Put' type='submit' name='Put' value='" . localize("Déposer") . "' /></td></tr>";
                    break;
                case 2:
                    $str .= "<tr><td class='mainbgtitle'><input id='Buy' type='submit' name='Buy' value='" . localize("Prendre") . "' /></td></tr>";
                    break;
            }
            
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        }
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
