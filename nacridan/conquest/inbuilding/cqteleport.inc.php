<?php
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQTeleport extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQTeleport($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $lastdeath = gmstrtotime($curplayer->get("lastdeath"));
        
        $str = "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize('T E M P L E  &nbsp;&nbsp;&nbsp;D E  &nbsp;&nbsp;&nbsp;L A  &nbsp;&nbsp;&nbsp;T É L É P O R T A T I O N') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        /*
         * if((time()-$lastdeath)<3600*24*3)
         * {
         * $teleportEnabled=$lastdeath+3600*24*3;
         * $str.="<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
         * $str.=localize("Votre corps est encore trop faible pour subir une téléportation.")."<br/>";
         * $str.=localize("Vous devez attendre le : ").date("Y-m-d H:i:s",$teleportEnabled);
         * $str.="</td></tr></table>";
         * }
         * else
         */
        if (! isset($_POST["IdCity"])) {
            $str .= "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=map&bottom=teleport' . "' target='_self'>\n";
            
            $str .= "<table class='maintable' ><tr><td class='mainbgtitle' width='500px'>";
            
            $str .= localize("Vous pouvez vous téléporter vers l'un des temples suivant (pour " . TELEPORT_AP . " PA) :") . " <select id='Object' class='selector cqattackselectorsize' name='IdCity'>";
            
            $dbct = new DBCollection("SELECT level,id_City FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db, 0, 0);
            $gap = 50 + 30 * $dbct->get("level");
            
            $dbp = new DBCollection("SELECT City.id,City.name,City.x,City.y FROM City WHERE name!='Pilier' AND id !=" . $dbct->get("id_City") . " AND map=" . $map . "  AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap . " ORDER BY City.x", $db, 0, 0);
            
            $previous = '';
            if (isset($_POST["IdCityReturn"])) {
                $previous = $_POST["IdCityReturn"];
            }
            $IdCityList = '';
            $str .= "<option value='0' " . ($previous == '' ? "selected='selected'" : "") . ">" . localize("-- Choisir une destination --") . "</option>";
            $item = array();
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "") {
                    $item[] = array(
                        localize($dbp->get("name")) . " " . $dbp->get("x") . "," . $dbp->get("y") => $dbp->get("id")
                    );
                }
                $IdCityList .= $IdCityList != '' ? ',' . $dbp->get("id") : $dbp->get("id");
                $dbp->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "' " . ($previous == $value ? "selected='selected'" : "") . ">" . $key . "</option>";
                }
            }
            $str .= "</select></td></tr><tr class='mainbgtitle'> <td>";
            $str .= "<input id='Teleport' type='submit' name='Teleport' value='" . localize("Voir le prix et la carte.") . "' />";
            $str .= "<input name='IdCityList' type='hidden' value='" . $IdCityList . "' />\n";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        } elseif (isset($_POST["Teleport"]) && $_POST["IdCity"] == 0) {
            $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Veuillez sélectionner une destination valide.");
            $str .= "</td></tr></table>";
        } else {
            
            $dbp = new DBCollection("SELECT City.id,City.name,City.x,City.y FROM City WHERE id=" . quote_smart($_POST["IdCity"]), $db, 0, 0);
            $dbtemple = new DBCollection("SELECT id FROM Building WHERE id !=" . $curplayer->get("inbuilding") . " AND map=" . $map . " AND x=" . $dbp->get("x") . " AND y=" . $dbp->get("y"), $db, 0, 0);
            
            $distTP = distHexa($xp, $yp, $dbp->get("x"), $dbp->get("y"));
            $price = floor(TELEPORT_PRICE * $distTP + CityFactory::getBuildingActionPrice($curplayer->get("inbuilding"), TELEPORT, $db));
            
            if ($curplayer->get("money") < $price) {
                $str = "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Une téléportation sur cette distance (" . $distTP . " lieues) coûte " . $price . " PO, vous n'avez pas assez d'or.");
                $str .= "</td></tr></table>";
            } elseif ($curplayer->get("ap") < TELEPORT_AP) {
                $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Cette téléportation coûte <b> " . $price . "PO </b>. <br/> Cependant Vous n'avez pas assez de Points d'Action (PA) pour cette Action.");
                $str .= "</td></tr></table>";
            } elseif (! $dbp->eof()) {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act'>";
                $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Voulez-vous vous téléporter en : X={resx}, Y={resy}", array(
                    "resx" => $dbp->get("x"),
                    "resy" => $dbp->get("y")
                )) . "<br/>";
                
                $str .= localize("Cette action vous coûtera " . $price . " PO (et " . TELEPORT_AP . " PA)");
                $str .= "</td></tr></table>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' style='float: left;'/>";
                $str .= "<input name='PRICE' type='hidden' value='" . $price . "' />";
                $str .= "<input name='NEWTEMPLEID' type='hidden' value='" . $dbtemple->get("id") . "' />";
                $str .= "<input name='action' type='hidden' value='" . TELEPORT . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Erreur, destination non valide !");
                $str .= "</td></tr></table>";
            }
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view&room=" . TELEPORT_ROOM . "'>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Retour' style='float: left; margin-left: 10px;'/>";
            $str .= "<input name='IdCityReturn' type='hidden' value='" . quote_smart($_POST["IdCity"]) . "' />";
            $str .= "</form>";
        }
        
        return $str;
    }
}

?>
