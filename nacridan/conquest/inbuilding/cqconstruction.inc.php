<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
require_once (HOMEPATH . "/factory/CityFactory.inc.php");

class CQConstruction extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQConstruction($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        $dbh = new DBCollection(
            "SELECT City.name as name, City.money as money, City.id as idCity, City.captured as captured, City.id_Player as gouvernor FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" .
                 $curplayer->get("inbuilding"), $db);
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'><b>" . localize("S A L L E   &nbsp;&nbsp;&nbsp;D E   &nbsp;&nbsp;L'&nbsp;A R C H I T E C T E") . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        if ($curplayer->accessRoom($curplayer->get("inbuilding"), CONSTRUCTION_ROOM, $db) || $curplayer->get("authlevel") > 10) {
            $str .= "<tr><td> Caisse de la cité : " . $dbh->get("money") . " pièce(s) d'or</td></tr>";
        }
        $str .= "</table>\n";
        
        if (isset($_POST["updateDesc"])) {
            $dbu = new DBCollection("UPDATE City SET description='" . quote_smart($_POST["desc"]) . "' WHERE id=" . $dbh->get("idCity"), $db, 0, 0, false);
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel'> La description a été mise à jour avec succès.";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
        } elseif (isset($_POST["updateImg"])) {
            if ($_FILES['imgCity']['name'] != "") {
                $dbu = new DBCollection("UPDATE City SET bigpic='" . stripAccents($dbh->get("name")) . ".jpg' WHERE id=" . $dbh->get("idCity"), $db, 0, 0, false);
                $tmp_name = $_FILES['imgCity']['tmp_name'];
                $name = $_FILES['imgCity']['name'];
                $size = $_FILES['imgCity']['size'];
                $typefile = $_FILES['imgCity']['type'];
                $erreur = $_FILES['imgCity']['error'];
                
                // $path="../pics/city/";
                
                // Get new sizes
                list ($width, $height) = getimagesize($tmp_name);
                if ($width != 0 && $height != 0) {
                    $newwidth = 640;
                    $newheight = 480;
                    
                    $ratioA = $width / $height;
                    $ratioB = $newwidth / $newheight;
                    
                    if ($ratioA > $ratioB) {
                        $newwidth2 = 640;
                        $newheight2 = $newwidth2 / $ratioA;
                    } else {
                        $newheight2 = 480;
                        $newwidth2 = $newheight * $ratioA;
                    }
                    
                    // Load
                    $thumb = imagecreatetruecolor($newwidth2, $newheight2);
                    $typeimage = exif_imagetype($tmp_name);
                    
                    switch ($typeimage) {
                        case IMAGETYPE_GIF:
                            $source = @imagecreatefromgif($tmp_name);
                            break;
                        case IMAGETYPE_JPEG:
                            $source = @imagecreatefromjpeg($tmp_name);
                            break;
                        case IMAGETYPE_PNG:
                            $source = @imagecreatefrompng($tmp_name);
                            break;
                        case IMAGETYPE_BMP:
                            $source = @imagecreatefrombmp($tmp_name);
                            break;
                        default:
                            $source = "";
                            break;
                    }
                    
                    $source = @imagecreatefromjpeg($tmp_name);
                    
                    if ($source != "") {
                        // Resize
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth2, $newheight2, $width, $height);
                        exec("rm " . HOMEPATH . "/pics/city/" . stripAccents($dbh->get("name")) . "*.jpg");
                        // Output
                        imagejpeg($thumb, HOMEPATH . "/pics/city/" . stripAccents($dbh->get("name")) . ".jpg", 90);
                    }
                } else
                    $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
                
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> L'image de la cité a été mise à jour avec succès.";
                $str .= "</td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
            } else {
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> L'image n'a pas pu être chargée.";
                $str .= "</td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
            }
        } elseif (isset($_POST["upgradefin"])) // MAJ du bâtiment pour le nouveau niveau
{
            $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $_POST["ID_BUILDING"], $db);
            $bvalue = $dbb->get("value");
            $nextprice = CityFactory::getPriceToUpgradeBuilding($dbb->get("id"), $db);
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            if (isset($_POST["structure"])) {
                $price = floor($dbb->get("sp") / 10);
                if ($dbh->get("money") < $price) {
                    $str .= "<tr>\n";
                    $str .= "<td class='mainbglabel'> Il n'y a pas suffisamment d'or dans les caisses de la cité pour améliorer la solidité de ce bâtiment";
                    $str .= "</td>\n";
                    $str .= "</tr>\n";
                } else {
                    $dbu = new DBCollection("UPDATE Building SET sp=sp+" . UPGRADE_PS . ", progress=progress+" . UPGRADE_PS . ", repair=3 WHERE id=" . $dbb->get("id"), $db, 0, 0, 
                        false);
                    $dbu = new DBCollection("UPDATE City SET money=money-" . $price . " WHERE id=" . $dbh->get("idCity"), $db, 0, 0, false);
                    $str .= "<tr>\n";
                    $str .= "<td class='mainbglabel'>Les travaux dureront 1 à 2 jours. A leur achèvement le bâtiment aura gagné " . UPGRADE_PS . " points de structure.";
                    $str .= "</td>\n";
                    $str .= "</tr>\n";
                }
            } else {
                if ($dbh->get("money") < $nextprice) {
                    $str .= "<tr>\n";
                    $str .= "<td class='mainbglabel'> Il n'y a pas suffisamment d'or dans les caisses de la cité pour améliorer ce bâtiment";
                    $str .= "</td>\n";
                    $str .= "</tr>\n";
                } else {
                    $levelup = 1;
                    if (isset($_POST["schoolchoice"])) {
                        if ($dbb->get("school") != "")
                            $schoolList = unserialize($dbb->get("school"));
                        else
                            $schoolList = array();
                        
                        if ($dbb->get("id_BasicBuilding") == 8) {
                            $dbBT = new DBCollection("SELECT id,name,type FROM BasicTalent", $db, 0, 0);
                            $check = 0;
                            $talentnames = "";
                            while (! $dbBT->eof()) {
                                if (array_key_exists($dbBT->get("id"), $_POST)) {
                                    $schoolList[$_POST[$dbBT->get("id")]] = $dbBT->get("type");
                                    $check ++;
                                    $talentnames .= $dbBT->get("name") . ", ";
                                }
                                $dbBT->next();
                            }
                            $talentnames = substr($talentnames, 0, - 2);
                            // Comment
                            if ($check < 6) {
                                $dbu = new DBCollection("UPDATE Building SET school='" . serialize($schoolList) . "' WHERE id=" . $dbb->get("id"), $db, 0, 0, false);
                                $str .= "<tr><td class='mainbglabel'> Vous avez ouvert les cours pour les savoir-faire : " . $talentnames . ". </td></tr>";
                            } else {
                                $str .= "<tr><td class='mainbglabel'> Vous avez sélectionnés trop de cours. Vous êtes limités à 5 nouveaux cours.</td></tr>";
                                $levelup = 0;
                            }
                        } else {
                            $schoolList[$_POST["schoolchoice"]] = 1;
                            $dbu = new DBCollection("UPDATE Building SET school='" . serialize($schoolList) . "' WHERE id=" . $dbb->get("id"), $db, 0, 0, false);
                            $str .= "<tr><td class='mainbglabel'> Vous avez ouvert les cours niveau Aspirant de l'" . getSchoolName($_POST["schoolchoice"]) . " </td></tr>";
                        }
                    } elseif (isset($_POST["upschoolchoice"])) {
                        if ($dbb->get("school") != "")
                            $schoolList = unserialize($dbb->get("school"));
                        else
                            $schoolList = array();
                        if ($_POST["upschoolchoice"] > - 1) {
                            $schoolList[$_POST["upschoolchoice"]] = 2;
                            $dbu = new DBCollection("UPDATE Building SET school='" . serialize($schoolList) . "' WHERE id=" . $dbb->get("id"), $db, 0, 0, false);
                            $str .= "<tr><td class='mainbglabel'> Vous avez ouvert les cours niveau Adepte de l'" . getSchoolName($_POST["upschoolchoice"]) . " </td></tr>";
                        } else {
                            $str .= "<tr><td class='mainbglabel'> Vous ne pouvez pas améliorer votre école.</td></tr>";
                            $levelup = 0;
                        }
                    } elseif (isset($_POST["AP"]) && ($bvalue < 6)) {
                        $dbu = new DBCollection("UPDATE Building SET value=value+2 WHERE id=" . $dbb->get("id"), $db, 0, 0, false);
                        $str .= "<tr><td class='mainbglabel'> Vous avez réduit le temps d'apprentissage de 2 PA. </td></tr>";
                    }
                    
                    if ($levelup) {
                        $dbu = new DBCollection("UPDATE Building SET level=level+1 WHERE id=" . $_POST["ID_BUILDING"], $db, 0, 0, false);
                        $dbu = new DBCollection("UPDATE City SET money=money-" . $nextprice . " WHERE id=" . $dbh->get("idCity"), $db, 0, 0, false);
                        
                        if ($dbb->get("id_BasicBuilding") == 14) {
                            // apparition d'un nouveau prêtre avec disparition de l'ancien
                            BasicActionFactory::killPriest($dbh->get("idCity"), $db);
                            InsideBuildingFactory::createPriestForACity($dbh->get("idCity"), $db);
                        }
                        if ($dbb->get("id_BasicBuilding") == 11) {
                            // apparition d'un nouveau garde avec disparition de l'ancien
                            BasicActionFactory::killGuard($dbh->get("idCity"), $db);
                            InsideBuildingFactory::createGardForACity($dbh->get("idCity"), $dbb->get("id"), $db);
                        }
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbglabel'> Le bâtiment gagne un niveau. Les services qu'il propose seront de meilleur qualité.";
                        $str .= "</td>\n";
                        $str .= "</tr>\n";
                    }
                }
            }
            $str .= "</table>";
        } elseif (isset($_POST["upgrade2"])) { // Ecole - détail du choix de l'option du nouveau niveau
            $isError = 0;
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $_POST["ID_BUILDING"], $db);
            $bvalue = $dbb->get("value");
            $blevel = $dbb->get("level");
            $nextprice = CityFactory::getPriceToUpgradeBuilding($dbb->get("id"), $db);
            
            if ($dbb->get("school") != "")
                $schoolList = unserialize($dbb->get("school"));
            else
                $schoolList = array();
            
            if ($_POST["upgrade2"] == "newschool") {
                $str .= "<tr><td class='mainbgbody'>Liste des écoles que vous pouvez ouvrir :";
                
                if ($dbb->get("id_BasicBuilding") == 7) {
                    $smin = 5;
                    $smax = 8;
                    $str .= " <select id='schoolchoice' class='selector cqattackselectorsize' name='schoolchoice'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisir une école --") . "</option>";
                    for ($i = $smin; $i <= $smax; $i ++) {
                        if (! array_key_exists($i, $schoolList))
                            $str .= "<option value='" . $i . "' >" . getSchoolName($i) . "</option>";
                    }
                    $str .= "</select></td></tr>";
                } elseif ($dbb->get("id_BasicBuilding") == 6) {
                    $smin = 1;
                    $smax = 4;
                    $str .= " <select id='schoolchoice' class='selector cqattackselectorsize' name='schoolchoice'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisir une école --") . "</option>";
                    for ($i = $smin; $i <= $smax; $i ++) {
                        if (! array_key_exists($i, $schoolList))
                            $str .= "<option value='" . $i . "' >" . getSchoolName($i) . "</option>";
                    }
                    $str .= "</select></td></tr>";
                } elseif ($dbb->get("id_BasicBuilding") == 8) {
                    $dbBT = new DBCollection("SELECT id,name FROM BasicTalent", $db, 0, 0);
                    $str .= "</td></tr><tr><td class='mainbgbody'>(Vous pouvez choisir d'ouvrir au maximum cinq nouveaux cours. )</td></tr>";
                    while (! $dbBT->eof()) {
                        if (! array_key_exists($dbBT->get("id"), $schoolList))
                            $str .= "<tr><td class='mainbgbody'><input type='checkbox' name='" . $dbBT->get("id") . "' value='" . $dbBT->get("id") . "' id='" . $dbBT->get("name") .
                                 "' /><label for='" . $dbBT->get("name") . "'>" . $dbBT->get("name") . " </label></td></tr>";
                        
                        $dbBT->next();
                    }
                    $str .= "<input type='hidden' name='schoolchoice' value='yes'/> ";
                }
            } elseif ($_POST["upgrade2"] == "upschool") {
                
                $str .= "<tr><td class='mainbgbody'>Liste des écoles niveau Aspirant que vous pouvez passer niveau Adepte : ";
                $strval1 = "<select id='upschoolchoice' class='selector cqattackselectorsize' name='upschoolchoice'>";
                $strval1 .= "<option value='0' selected='selected'>" . localize("-- Choisir une école --") . "</option>";
                $strtmp = "";
                foreach ($schoolList as $idname => $level) {
                    if ($level == 1) {
                        $strtmp .= "<option value='" . $idname . "' >" . getSchoolName($idname) . "</option>";
                    }
                }
                $strval1 .= $strtmp;
                $strval1 .= "</select>";
                
                if (strlen($strtmp) > 0) {
                    $str .= $strval1;
                } else {
                    $str .= "<br/><b>Il n'y a pas d'école que vous pouvez améliorer.</b>";
                    $isError = 1;
                    $str .= "<input type='hidden' id='upschoolchoice' value='-1' name='upschoolchoice'>";
                }
                $str .= "</select></td></tr>";
            } elseif ($_POST["upgrade2"] == "PA") {
                $str .= "<tr><td class='mainbgbody'>Vous avez choisi de diminuer le coût d'apprentissage.</td></tr>";
                $str .= "<input type='hidden' name='AP' value='yes'/> ";
            }
            
            if ($_POST["upgrade2"] == "structure") {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Vous allez améliorer la solidité du bâtiment de " . UPGRADE_PS . " points de structure en payant " . (floor($dbb->get("sp") / 10)) .
                     " PO.";
                $str .= "</td></tr><tr><td>";
                $str .= "<input id='upgradefin' type='submit' name='upgradefin' value='Confirmer'/> ";
                $str .= "<input type='hidden' name='ID_BUILDING' value='" . $dbb->get("id") . "'/>";
                $str .= "<input type='hidden' name='structure' value='" . $dbb->get("id") . "'/>";
                $str .= "</td></tr>\n";
            } else {
                if ($isError == 0) {
                    $str .= "<td class='mainbglabel'> Vous allez passer le bâtiment : " . $dbb->get("name") . " du niveau " . $blevel . " au niveau " . ($blevel + 1) . " en payant " .
                         $nextprice . " PO.";
                    $str .= "<tr><td class='mainbgbody'><input id='upgradefin' type='submit' name='upgradefin' value='Confirmer'/></td></tr> ";
                    $str .= "<input type='hidden' name='ID_BUILDING' value='" . $dbb->get("id") . "'/> ";
                } else {
                    $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Retour'/></td></tr> ";
                }
            }
            $str .= "</table></form>";
        } 

        elseif (isset($_POST["upgrade1"])) // Amélioration du bâtiment - phase 1
{
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $_POST["ID_BUILDING"], $db);
            
            $bvalue = $dbb->get("value");
            $blevel = $dbb->get("level");
            
            $nextprice = CityFactory::getPriceToUpgradeBuilding($dbb->get("id"), $db);
            
            if (($dbb->get("id_BasicBuilding") == 7) or ($dbb->get("id_BasicBuilding") == 6)) {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                $str .= "</td></tr>\n";
                
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='newschool' id='newschool' /><label for='newschool'>Ouvrir une nouvelle école niveau Aspirant.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='upschool' id='upschool' /><label for='upschool'>Passer niveau Adepte une école de niveau Aspirant déjà ouverte.</label></td></tr>";
                
                if ($bvalue < 6)
                    $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='PA' id='PA' /><label for='PA'>Diminuer le coût d'apprentissage de 2PA</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Ok'/> </td></tr> ";
                $str .= "<input type='hidden' name='ID_BUILDING' value='" . $dbb->get("id") . "'/> ";
                // $str.="<input type='hidden' name='room' value='".ENTRANCE_ROOM."'/>";
            } elseif ($dbb->get("id_BasicBuilding") == 8) {
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='newschool' id='newschool' /><label for='newschool'>Ouvrir des nouveaux cours.</label></td></tr>";
                if ($bvalue < 6)
                    $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='PA' id='PA' /><label for='PA'>Diminuer le coût d'apprentissage de 2PA</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='structure' id='structure' /><label for='newschool'>Améliorer la solidité du bâtiment.</label></td></tr>";
                // $str.="<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='service' id='service' /><label for='PA'>Améliorer les fonctionnalités du bâtiment</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Ok'/> </td></tr> ";
                $str .= "<input type='hidden' name='ID_BUILDING' value='" . $dbb->get("id") . "'/> ";
                // $str.="<input type='hidden' name='room' value='".ENTRANCE_ROOM."'/>";
            } else {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                $str .= "</td></tr>\n";
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='structure' id='structure' /><label for='newschool'>Améliorer la solidité du bâtiment.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='service' id='service' /><label for='PA'>Améliorer les fonctionnalités du bâtiment</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Ok'/> </td></tr> ";
                $str .= "<input type='hidden' name='ID_BUILDING' value='" . $dbb->get("id") . "'/> ";
            }
            $str .= "</table>";
            $str .= "</form>";
        }        

        // ------------------- Gestion des portes du village --------------------------------
        elseif (isset($_POST['management'])) {
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
            if ($_POST['management'] == "1") // ouverture des portes à tous les joueurs
                $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point d'ouvrir les portes du village à tout le monde. Souhaitez vous continuer ? </tr></td>";
            if ($_POST['management'] == "2") // fermeture des portes à tous les joueurs
                $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes du village à tout le monde. Souhaitez vous continuer ? </tr></td>";
            if ($_POST['management'] == "3") { // ouverture des portes du village uniquement aux amis
                                               // ----------------------- Choisir une ou plusieur option, pour gere les portes aux alliés -----------------------------------
                $str .= "<td class='mainbglabel'>Vous souhaitez ouvrir les portes à vos alliés.";
                $str .= "</td></tr>\n";
                $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                $str .= "</td></tr>\n";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3' id='MembersOrder' />" .
                     "<label for='resurrect'>Les membres de mon ordre</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30' id='PersonalAllies' />" .
                     "<label for='resurrect'>Mes alliés personnels</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='300' id='OrdersPersonalAlliances' />" .
                     "<label for='resurrect'>Les ordres de mes alliances personnelles</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='3000' id='AlliedMembersOrder' />" .
                     "<label for='resurrect'>Les membres alliés de mon ordre</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='checkbox' name='openDoorFriend[]' value='30000' id='LevelsCombinedOrder' />" .
                     "<label for='resurrect'>Les ordres alliés à mon ordre</label></td></tr>";
                $str .= "<td class='mainbglabel'>Valider vos choix puis cliquer sur Continuer";
                $str .= "</td></tr>\n";
            }
            if ($_POST['management'] == "4") // fermeture des portes du village aux ennemis
                $str .= "<tr><td class='mainbgbody'> Vous êtes sur le point de fermer les portes du village à vos ennemis. Souhaitez vous continuer ? </tr></td>";
            
            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt'' value='Continuer' /></td></tr>";
            $str .= "<td><input name='action' type='hidden' value='" . OPERATE_VILLAGE . "' /></td>";
            $str .= "<input type='hidden' name='manag' value='" . $_POST["management"] . "'/> ";
            
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</table>";
            $str .= "</form>";
        } elseif (isset($_POST['architectchoice'])) {
            $architectchoice = $_POST['architectchoice'];
            
            switch ($architectchoice) {
                case "construct":
                    if ($curplayer->get("ap") < BUILDING_CONSTRUCTION_AP) {
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action.</td></tr>";
                    } else {
                        $dbt = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=14 AND id_City=" . $dbh->get("idCity"), $db);
                        $dbb = new DBCollection(
                            "SELECT MapGround.* FROM MapGround left outer join Building on MapGround.x=Building.x and MapGround.y=Building.y and MapGround.map =Building.map 
                            WHERE (abs(MapGround.x-" .
                                 $dbt->get("x") . ") + abs(MapGround.y-" . $dbt->get("y") . ") + abs(MapGround.x+MapGround.y-" . $dbt->get("x") . "-" . $dbt->get("y") .
                                 "))/2 <4 and MapGround.map=1 and Building.y is null", $db);
                        if ($dbb->count() <= 3) {
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            $str .= "<tr><td>Vous devez laisser au moins 3 espaces libres dans votre village.</td></tr>";
                        } else {
                            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            $str .= "<tr><td> Choississez le type de bâtiment que vous souhaitez construire.</td></tr>";
                            $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE (id< 11 AND id!=2 AND id!=3) or id=28", $db);
                            $str .= "<tr><td><select class='selector cqattackselectorsize' name='basicBuilding'>";
                            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bâtiment --") . "</option>";
                            while (! $dbb->eof()) {
                                $item[] = array(
                                    localize($dbb->get("name") . " (" . $dbb->get("price") . " po)") => $dbb->get("id")
                                );
                                $dbb->next();
                            }
                            foreach ($item as $arr) {
                                foreach ($arr as $key => $value)
                                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                            }
                            $str .= "</select>";
                            $str .= "</td></tr><tr><td> Choississez un emplacement vide dans le village pour construire le palais </td></tr>";
                            $str .= "<tr><td><select class='selector cqattackselectorsize' name='Emplacement'>";
                            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un emplacement --") . "</option>";
                            unset($item);
                            $dbt = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=14 AND id_City=" . $dbh->get("idCity"), $db);
                            for ($i = - 3; $i < 4; $i ++) {
                                for ($j = - 3; $j < 4; $j ++) {
                                    if (BasicActionFactory::freePlace($dbt->get("x") + $i, $dbt->get("y") + $j, $map, $this->db, 1) &&
                                         distHexa($dbt->get("x"), $dbt->get("y"), $dbt->get("x") + $i, $dbt->get("y") + $j) < 4)
                                        $item[] = array(
                                            localize("(X= " . ($dbt->get("x") + $i) . " , Y= " . ($dbt->get("y") + $j) . ")") => ($dbt->get("x") + $i) . "," . ($dbt->get("y") + $j)
                                        );
                                }
                            }
                            foreach ($item as $arr) {
                                foreach ($arr as $key => $value)
                                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                            }
                            $str .= "</select>";
                            $str .= "<td><input name='action' type='hidden' value='" . BUILDING_CONSTRUCT . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        }
                    }
                    break;
                /* Choit permettant de réparer un bâtiment */
                case "repair":
                    if ($curplayer->get("ap") < BUILDING_REPAIR_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } else {
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $item = array();
                        $str .= "</td></tr><tr><td> Choississez le bâtiment à réparer.</td></tr>";
                        $dbb = new DBCollection("SELECT * FROM Building WHERE repair=0 AND id_City=" . $dbh->get("idCity") . " AND currsp<sp", $db);
                        $str .= "<tr><td><select class='selector cqattackselectorsize' name='ID_BUILDING'>";
                        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bâtiment --") . "</option>";
                        while (! $dbb->eof()) {
                            require_once (HOMEPATH . "/factory/CityFactory.inc.php");
                            $price = CityFactory::getPriceToRepairBuilding($dbb->get("id"), $db);
                            $item[] = array(
                                localize($dbb->get("name") . " (" . $price . " po)") => $dbb->get("id")
                            );
                            $dbb->next();
                        }
                        /*
                         * $dbr = new DBCollection("SELECT * FROM Building WHERE repair=0 AND progress=sp AND currsp=sp AND (name='Rempart' or id_BasicBuilding=21 or id_BasicBuilding=26) AND id_City=".$dbh->get("idCity"), $db);
                         *
                         * $nb = array(6,6,4,1,1,1,1,1,1,2);
                         * $i=0;
                         * if($dbr->count() != 24)
                         * {
                         * $level = $dbr->get("level");
                         * $item[]=array (localize("Rempart(s) et porte(s) détruits (".$dbs->get("price")." po)") => 1);
                         *
                         * $dbs = new DBCollection("SELECT * , count( * ) AS nb FROM Building LEFT JOIN BasicBuilding ON Building.id_BasicBuilding = BasicBuilding.id WHERE id_City =124 AND (BasicBuilding.name = 'Rempart' OR BasicBuilding.id =21 OR BasicBuilding.id =26) GROUP BY id_BasicBuilding", $db);
                         * while(!$dbs->eof())
                         * {
                         * if($dbs->get("nb") < $nb[$i])
                         * $item[]=array (localize($dbs->get("name")." (".$dbs->get("price")." po)") => -$dbb->get("id_BasicBuilding"));
                         *
                         * $i++;
                         * $dbs->next();
                         * }
                         * }
                         */
                        
                        foreach ($item as $arr) {
                            foreach ($arr as $key => $value)
                                $str .= "<option value='" . $value . "'>" . $key . "</option>";
                        }
                        $str .= "</select>";
                        $str .= "<td><input name='action' type='hidden' value='" . BUILDING_REPAIR . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                    }
                    break;
                /* Choit permettant de détruire un bâtiment */
                case "destroy":
                    
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    if ($curplayer->get("ap") < BUILDING_DESTROY_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } else {
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        
                        $item = array();
                        $str .= "</td></tr><tr><td>Détruire un bâtiment coûte " . BUILDING_DESTROY_PRICE . " PO pour chaques 500 PS. Choississez le bâtiment à détruire.</td></tr>";
                        $dbb = new DBCollection(
                            "SELECT * FROM Building WHERE name!='Rempart' AND id_BasicBuilding!=26 AND id_BasicBuilding!=21 AND repair!= 2 AND id_BasicBuilding != 4 AND id_BasicBuilding != 14 AND id_BasicBuilding != 11 AND id_City=" .
                                 $dbh->get("idCity"), $db);
                        $str .= "<tr><td><select class='selector cqattackselectorsize' name='ID_BUILDING'>";
                        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bâtiment --") . "</option>";
                        while (! $dbb->eof()) {
                            require_once (HOMEPATH . "/factory/CityFactory.inc.php");
                            $item[] = array(
                                localize($dbb->get("name") . " (côut: " . floor($dbb->get("sp") * BUILDING_DESTROY_PRICE / 500) . ")") => $dbb->get("id")
                            );
                            $dbb->next();
                        }
                        foreach ($item as $arr) {
                            foreach ($arr as $key => $value)
                                $str .= "<option value='" . $value . "'>" . $key . "</option>";
                        }
                        $str .= "</select>";
                        $str .= "<td><input name='action' type='hidden' value='" . BUILDING_DESTROY . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                    }
                    
                    break;
                /* Choix permetant d'améliorer un batiment */
                case "upgrade":
                    
                    if ($curplayer->get("ap") < BUILDING_UPGRADE_AP) {
                        $str .= "<tr><td>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } else {
                        $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $item = array();
                        $str .= "</td></tr><tr><td>Choississez le bâtiment à améliorer.</td></tr>";
                        $dbb = new DBCollection(
                            "SELECT * FROM Building WHERE name!='Rempart' AND id_BasicBuilding!=21 AND id_BasicBuilding!=26 AND progress=sp AND repair=0 AND id_City=" .
                                 $dbh->get("idCity"), $db);
                        $str .= "<tr><td><select class='selector cqattackselectorsize' name='ID_BUILDING'>";
                        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bâtiment --") . "</option>";
                        while (! $dbb->eof()) {
                            $item[] = array(
                                localize($dbb->get("name") . " (" . $dbb->get("id") . ") niveau " . $dbb->get("level")) => $dbb->get("id")
                            );
                            $dbb->next();
                        }
                        foreach ($item as $arr) {
                            foreach ($arr as $key => $value)
                                $str .= "<option value='" . $value . "'>" . $key . "</option>";
                        }
                        $str .= "</select>";
                        $str .= "<td><input name='action' type='hidden' value='" . BUILDING_UPGRADE . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td><input id='upgrade1' type='submit' name='upgrade1' value='Confirmer' /></td></tr>";
                    }
                    
                    break;
                /* Choix permettant de nommé ou renommé un village */
                case "nameVillage":
                    $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db);
                    $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    if ($curplayer->get("ap") < NAME_VILLAGE_AP) {
                        $str .= "<tr><td> Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } elseif ($dbh->get("money") < NAME_VILLAGE_PRICE) {
                        $str .= "<tr><td> Vous n'avez pas assez d'argent dans les caisses de la cité</td></tr>";
                    } else {
                        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                        $str .= "<tr><td class='mainbgbody'> Votre village ce nomme actuellement: " . $dbh->get("name") . "</td></tr>";
                        $str .= "<tr><td></td></tr>";
                        $str .= "<tr><td></td></tr>";
                        $str .= "<tr><td class='mainbgbody'> Indiquez le nom du village ou renommé celui-ci </td></tr>";
                        $str .= "<tr><td class='mainbgbody'> <label>Nom du village: <input type='text' name='nameVillage' value='" . $dbh->get("name") . "' /> </label> </td></tr>"; // champ de type text pour nommer le village
                        $str .= "<tr><td class='mainbgbody'> Cela va vous coutez " . NAME_VILLAGE_AP . " pa et " . NAME_VILLAGE_PRICE . " pièces d'or.</td></tr>";
                        $str .= "<td><input name='action' type='hidden' value='" . NAME_VILLAGE . "' /></td>";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Confirmer' /></td></tr>";
                        $str .= "</form>";
                    }
                    
                    break;
                
                case "updateDescription":
                    $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db);
                    $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
                    // $str.="<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self' enctype='multipart/form-data'>\n";
                    $str .= "<table class='maintable insidebuildingleftwidth'><tr><td class='mainbglabel'>" . localize("Description de la cité") . "</td></tr>\n";
                    $text = $dbh->get("description");
                    
                    $str .= "<tr><td><textarea name='desc' cols=58 rows=10>" . $dbh->get("description") . "</textarea></td></tr>";
                    $str .= "<tr><td><input type='submit' name='updateDesc' value='" . localize("Mettre à jour la description") . "' /></td></tr>";
                    $str .= "<tr><tr/><tr><tr/><tr><tr/>";
                    $str .= "<tr><td class='mainbglabel'>" . localize("Image de la cité") . "</td></tr>\n";
                    
                    $str .= "<tr><td class='mainbgtitle'>" . localize("Format recommandé : (640 x 480)") . "</td><tr/>";
                    $str .= "<tr><td class='mainbgtitle'>" . localize("Taille maximum : 500kb") . "</td><tr/>";
                    $str .= "<tr><td class='mainbgtitle'>" . localize("L'image ne doit pas être protégée en copyright ou protégée par une marque déposée.") . "</td><tr/>";
                    
                    $str .= "<tr><td class='mainbgtitle'><input type='file' name='imgCity' size='30'></td></tr>";
                    
                    $str .= "<input type='hidden' name='max_file_size' value='500000'>\n";
                    $str .= "<tr><td><font color='#ff0000'><b>attention</b></font> <font color='#ffffff'>" .
                         localize("L'utilisation d'images pouvant heurter la morale pourra donner suite à l'annulation de votre compte sans préavis");
                    $str .= "</font><td/><tr/>";
                    $str .= "<tr><td><input type='submit' name='updateImg' value='" . localize("Mettre à jour le fichier image") . "'>\n";
                    $str .= "</td></tr>";
                    $str .= "</table>\n";
                    
                    $str .= "</form>";
                    
                    break;
                /* Choix permettant de fortifié un village */
                case "fortifyVillage":
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    if ($curplayer->get("ap") < FORTIFY_VILLAGE_AP) {
                        $str .= "<tr><td class='mainbgbody'>Vous n'avez pas assez de PA pour effectuer cette action</td></tr>";
                    } else {
                        $citySize = 4;
                        $dbjBeatenEarth = new DBCollection(
                            "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map and City.id = " . $dbh->get("idCity") .
                                 " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize . " and City.y+" .
                                 $citySize .
                                 " inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                                 $citySize . " and Ground.ground not in('cobblestone','beatenearth') group by City.id", $db);
                        if ($dbjBeatenEarth->count() > 0) {
                            $str .= "<tr><td class='mainbgbody'>Vous devez terrasser avant de fortifier votre village.</td></tr>";
                        } else {
                            $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE name='Rempart'", $db);
                            $price = $dbb->get("price") * 24;
                            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<tr><td class='mainbgbody'> Construction d'une fortification </tr></td>";
                            $str .= "<tr><td class='mainbgbody'> Cela va vous coutez " . FORTIFY_VILLAGE_AP . " pa et " . $price . " pièces d'or.</tr></td>";
                            $str .= "<td><input name='action' type='hidden' value='" . FORTIFY_VILLAGE . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                            $str .= "</form>";
                        }
                    }
                    
                    break;
                case "upgradeFortification":
                    $dbb = new DBCollection(
                        "SELECT * FROM Building WHERE repair=0 AND progress=sp AND currsp=sp AND (name='Rempart' or id_BasicBuilding=21 or id_BasicBuilding=26) AND id_City=" .
                             $dbh->get("idCity"), $db);
                    if ($dbb->count() != 24) {
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbglabel'> Vous ne pouvez pas améliorer vos remparts avant qu'ils ne soit tous finis ou réparés.";
                        $str .= "</td></tr>\n";
                    } else {
                        $citySize = 4;
                        $dbjBeatenEarth = new DBCollection(
                            "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map and City.id = " . $dbh->get("idCity") .
                                 " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize . " and City.y+" .
                                 $citySize .
                                 " inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                                 $citySize . " and Ground.ground not in('cobblestone','beatenearth') group by City.id", $db);
                        if ($dbjBeatenEarth->count() > 0) {
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            $str .= "<tr><td class='mainbgbody'>Vous devez terrasser avant d'améliorer les fortifications de votre village.</td></tr>";
                        } else {
                            $nextprice = CityFactory::getPriceToUpgradeBuilding($dbb->get("id"), $db) * 24;
                            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                            $str .= "<tr><td class='mainbgbody'> Tous les remparts gagneront " . FRAGILE . " points de structure. </tr></td>";
                            $str .= "<tr><td class='mainbgbody'> Cela va vous coutez " . FORTIFY_VILLAGE_AP . " pa et " . $nextprice . " pièces d'or.</tr></td>";
                            $str .= "<td><input name='action' type='hidden' value='" . UPGRADE_FORTIFICATION . "' /></td>";
                            $str .= "<td><input name='IDCITY' type='hidden' value='" . $dbh->get("idCity") . "' /></td>";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                            $str .= "</form>";
                        }
                    }
                    break;
                case "sellhouse":
                    // Ajoute d'une maison à la vente
                    if (isset($_POST["sell"])) {
                        $price = $_POST["PRICE"];
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr>\n";
                        if ($price == 0 || $price < 0 || $price == "")
                            $str .= "<td class='mainbglabel'> Le prix indiqué n'est pas valide";
                        elseif ($_POST["ID_BUILDING"] == 0)
                            $str .= "<td class='mainbglabel'> Vous devez sélectionner une maison.";
                        else {
                            $dbu = new DBCollection("UPDATE Building SET value=" . $_POST["PRICE"] . " WHERE id=" . $_POST["ID_BUILDING"], $db, 0, 0, false);
                            $str .= "<td class='mainbglabel'> La maison a été mise en vente à " . $_POST["PRICE"] . " PO";
                        }
                        $str .= "</td></tr></table>\n";
                    } elseif (isset($_POST["check"])) {
                        
                        $where = EquipFactory::getCondFromArray($_POST["check"], "id", "OR");
                        $dbu = new DBCollection("UPDATE Building SET value=0 WHERE " . $where, $db, 0, 0, false);
                        $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbglabel'> Les maisons ont été retiré de la vente";
                        $str .= "</td></tr></table>\n";
                    }
                    
                    $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                    // Affichage des maison en vente
                    $dbj = new DBCollection("SELECT * FROM Building WHERE name='Maison' AND value>0 AND id_Player = 0 AND id_City=" . $dbh->get("idCity"), $db);
                    $array = array();
                    while (! $dbj->eof()) {
                        $col1 = "<input type='checkbox' name='check[]' value=" . $dbj->get("id") . ">";
                        
                        $lieu = $dbj->get("x") . "/" . $dbj->get("y");
                        $etat = $dbj->get("currsp") . "/" . $dbj->get("sp");
                        
                        $array[] = array(
                            "0" => array(
                                $col1,
                                "class='mainbgbody' align='left'"
                            ),
                            "1" => array(
                                "Maison",
                                "class='mainbgbody' align='center'"
                            ),
                            "2" => array(
                                $lieu,
                                "class='mainbgbody' align='center'"
                            ),
                            "3" => array(
                                $etat,
                                "class='mainbgbody' align='center'"
                            ),
                            "4" => array(
                                $dbj->get("value"),
                                "class='mainbgbody' align='center'"
                            )
                        );
                        $dbj->next();
                    }
                    
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<tr><td class='mainbglabel'>Maison en vente</td></tr>";
                    
                    $str .= createTable(5, $array, array(), 
                        array(
                            array(
                                "",
                                "class='mainbglabel' width='10%' align='left'",
                                "name",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Bâtiment"),
                                "class='mainbglabel' width='24%' align='center'",
                                "price",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Position"),
                                "class='mainbglabel' width='22%' align='center'",
                                "marge",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Etat"),
                                "class='mainbglabel' width='22%' align='center'",
                                "marge",
                                "mainbglabelhover",
                                "mainbglabel"
                            ),
                            array(
                                localize("Prix"),
                                "class='mainbglabel' width='22%' align='center'",
                                "marge",
                                "mainbglabelhover",
                                "mainbglabel"
                            )
                        ), "class='maintable insidebuildingleftwidth'", "formid", "order");
                    
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<td><input name='architectchoice' type='hidden' value='sellhouse' /></td>";
                    $str .= "<tr><td class='mainbglabel'><input name='sub' type='submit' value='Retirer de la vente' /></td></tr>";
                    
                    $str .= "<tr><td> &nbsp </td></tr>";
                    
                    $str .= "<tr><td class='mainbglabel'>Mettre en vente une maison</td></tr>";
                    $dbb = new DBCollection("SELECT * FROM Building WHERE name='Maison' AND value=0 AND id_Player=0 AND id_City=" . $dbh->get("idCity"), $db);
                    $str .= "<tr><td class='mainbglabel'> Vendre <select class='selector cqattackselectorsize' name='ID_BUILDING'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Maison disponible --") . "</option>";
                    $item = array();
                    while (! $dbb->eof()) {
                        $item[] = array(
                            localize($dbb->get("name") . " (" . $dbb->get("x") . "/" . $dbb->get("y") . ")") => $dbb->get("id")
                        );
                        $dbb->next();
                    }
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value)
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                    $str .= "</select>";
                    $str .= localize(" à ") . " <input type='text' name='PRICE' value'' maxlength=9 size=10/> " . localize("PO");
                    // $str.="<td><input name='IDCITY' type='hidden' value='".$dbh->get("idCity")."' /></td>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "<td><input name='architectchoice' type='hidden' value='sellhouse' /></td>";
                    $str .= "<tr><td class='mainbglabel'><input id='sell' type='submit' name='sell' value='Ajouter à la vente' /></td></tr>";
                    $str .= "</form>";
                    
                    break;
                
                case "terrassement":
                    $citySize = 4;
                    $dbjBeatenEarth = new DBCollection(
                        "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " .
                             $dbh->get("idCity") . " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize .
                             " and City.y+" . $citySize . "
               inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                             $citySize . " and Ground.ground not in('cobblestone','beatenearth') group by City.id", $db);
                    $dbjCobbleStone = new DBCollection(
                        "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " .
                             $dbh->get("idCity") . " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize .
                             " and City.y+" . $citySize . "
               inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                             $citySize . " and Ground.ground not in('cobblestone') group by City.id", $db);
                    $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE name='Maison'", $db);
                    $price = floor($dbb->get("price") / 4);
                    
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    if ($dbjBeatenEarth->count() > 0 && $dbjBeatenEarth->get("nb") > 0)
                        $str .= "<tr><td class='mainbgbody'><input type='radio' name='terrassement2' value='beatenearth' id='typebeatenearth' /><label for='typebeatenearth'>Terrasser autour des remparts en terre battue: cela vous coutera " .
                             TERRASSEMENT_AP . " pa et " . $price * $dbjBeatenEarth->get("nb") . " pièces d'or.</label></tr></td>";
                    if ($dbjCobbleStone->count() > 0 && $dbjCobbleStone->get("nb") > 0) {
                        $str .= "<tr><td class='mainbgbody'><input type='radio' name='terrassement2' value='cobblestone' id='typecobblestone' /><label for='typecobblestone'>Terrasser autour des remparts en espace pavé: cela vous coutera " .
                             TERRASSEMENT_AP . " pa et " . $price * $dbjCobbleStone->get("nb") * 2 . " pièces d'or.</label></tr></td>";
                    } else {
                        $str .= "<tr><td class='mainbgbody'>Il n'y a pas de terrassement possible.</tr></td>";
                    }
                    $str .= "<tr><td><input name='action' type='hidden' value='" . TERRASSEMENT . "' /></td><tr>";
                    $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</form>";
                    break;
                case "terraformation":
                    $citySize = 4;
                    $dbjBeatenEarth = new DBCollection(
                        "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map  
               and City.id = " . $dbh->get("idCity") .
                             " and MapGround.x between City.x-" . ($citySize + 1) . " and City.x+" . ($citySize + 1) . " and MapGround.y between City.y-" . ($citySize + 1) .
                             " and City.y+" . ($citySize + 1) . "
               inner join Ground on Ground.id = MapGround.id_Ground
			   where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" . ($citySize + 1) .
                             " and Ground.ground not in('cobblestone','beatenearth') group by City.id", $db);
                    $dbjCobbleStone = new DBCollection(
                        "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " . $dbh->get("idCity") .
                             " and MapGround.x between City.x-" . ($citySize + 1) . " and City.x+" . ($citySize + 1) . " and MapGround.y between City.y-" . ($citySize + 1) .
                             " and City.y+" . ($citySize + 1) . "
               inner join Ground on Ground.id = MapGround.id_Ground
			   where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" . ($citySize + 1) .
                             " and Ground.ground not in('cobblestone') group by City.id", $db);
                    $dbjChoix = new DBCollection(
                        "SELECT MapGround.id,MapGround.x,MapGround.y,Ground.cost_PA FROM `City` inner join MapGround on City.map=MapGround.map
               and City.id = " . $dbh->get("idCity") .
                             " and MapGround.x between City.x-" . ($citySize + 2) . " and City.x+" . ($citySize + 2) . " and MapGround.y between City.y-" . ($citySize + 2) .
                             " and City.y+" . ($citySize + 2) . "
               inner join Ground on Ground.id = MapGround.id_Ground
			   where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 between " .
                             ($citySize + 1) . " and " . ($citySize + 2) . " order by MapGround.x,MapGround.y", $db);
                    $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE name='Maison'", $db);
                    $price = floor($dbb->get("price") / 4);
                    $dbjGround = new DBCollection("SELECT * from Ground where ground not in ('river','ocean','flowers','polar') order by label", $db);
                    $strOptionCase = "<select id='case' name='case'><option>Choisissez une case</option>";
                    while (! $dbjChoix->eof()) {
                        $strOptionCase .= "<option value='" . $dbjChoix->get("id") . "'>" . $dbjChoix->get("x") . "/" . $dbjChoix->get("y") . " (" .
                             ($dbjChoix->get("cost_PA") * $price * 4) . " PO)</option>";
                        $dbjChoix->next();
                    }
                    $strOptionCase .= "</select>";
                    
                    $strOptionGround = "<select id='ground' name='ground'><option>Choisissez un type de terrain</option>";
                    while (! $dbjGround->eof()) {
                        $strOptionGround .= "<option value='" . $dbjGround->get("ground") . "'>" . $dbjGround->get("label") . " (coef " . $dbjGround->get("cost_PA") . ")</option>";
                        $dbjGround->next();
                    }
                    $strOptionGround .= "</select>";
                    $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                    $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                    if ($dbjBeatenEarth->count() > 0 && $dbjBeatenEarth->get("nb") > 0)
                        $str .= "<tr><td class='mainbgbody'><input type='radio' name='terraformation2' value='beatenearth' id='typebeatenearth' /><label for='typebeatenearth'>Ajouter une route autour des remparts en terre battue: cela vous coutera " .
                             TERRAFORMATION_AP . " pa et " . $price * $dbjBeatenEarth->get("nb") . " pièces d'or.</label></tr></td>";
                    if ($dbjCobbleStone->count() > 0 && $dbjCobbleStone->get("nb") > 0) {
                        $str .= "<tr><td class='mainbgbody'><input type='radio' name='terraformation2' value='cobblestone' id='typecobblestone' /><label for='typecobblestone'>Ajouter une route autour des remparts en espace pavé: cela vous coutera " .
                             TERRAFORMATION_AP . " pa et " . $price * $dbjCobbleStone->get("nb") * 2 . " pièces d'or.</label></tr></td>";
                    }
                    $str .= "<tr><td class='mainbgbody'><input type='radio' name='terraformation2' value='choice' id='typechoice' checked='true' /><label for='typechoice'>Terraformer la case " .
                         $strOptionCase . " en " . $strOptionGround . ": cela vous coutera " . (TERRAFORMATION_AP / 10) .
                         " pa et le prix de la case multiplié par le coefficient en pièces d'or.</label></tr></td>";
                    $str .= "<tr><td><input name='action' type='hidden' value='" . TERRAFORMATION . "' /></td><tr>";
                    $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Continuer' /></td></tr>";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</form>";
                    break;
                    break;
                default:
                    break;
            }
            $str .= "</table></form>";
        } else {
            if ($curplayer->accessRoom($curplayer->get("inbuilding"), CONSTRUCTION_ROOM, $db) || $curplayer->get("authlevel") > 10) {
                $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                $str .= "<table class='maintable insidebuildingleftwidth' >\n";
                $str .= "<tr><td class='mainbglabel'>Dans cette salle, vous pouvez gérer les bâtiments de la cité. Que souhaitez-vous faire ? </td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='construct' id='construct' />" .
                     "<label for='heal'> Constuire un nouveau bâtiment.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='repair' id='repair' />" .
                     "<label for='blessing'> Réparer un bâtiment endommagé.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='destroy' id='destroy' />" .
                     "<label for='resurrect'> Détruire un bâtiment.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='upgrade' id='upgrade' />" .
                     "<label for='resurrect'> Améliorer un bâtiment.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='nameVillage' id='nameVillage' />" .
                     "<label for='resurrect'> Renommer la ville.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='updateDescription' id='description' />" .
                     "<label for='resurrect'> Changer la description de la ville.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='sellhouse' id='sellhouse' />" .
                     "<label for='resurrect'> Gestion des maisons.</label></td></tr>";
                if ($dbh->get("captured") == 5)
                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='fortifyVillage' id='fortifyVillage' />" .
                         "<label for='resurrect'> Fortifier le village.</label></td></tr>";
                if ($dbh->get("captured") == 4) {
                    $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='upgradeFortification' id='upgradeFortification' />" .
                         "<label for='resurrect'> Améliorer les fortifications de la ville.</label></td></tr>";
                }
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='terrassement' id='terrassement' />" .
                     "<label for='resurrect'> Opérations de terrassement</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='architectchoice' value='terraformation' id='terraformation' />" .
                     "<label for='resurrect'> Opérations de terraformation</label></td></tr>";
                
                $str .= "<tr><td><input type='submit' name='ok' value='Ok'/> </td></tr>";
                $str .= "</table>";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
