<?php

/**
 *Affiche le contenu du parchemin
 *
 * Définit une fonction par type de mission
 *
 *@author Nacridan
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once ("../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/class/MissionReward.inc.php");
require_once (HOMEPATH . "/class/MissionCondition.inc.php");
require_once (HOMEPATH . "/class/Quest.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];
Translation::init('gettext', '../../i18n/messages', $lang, 'UTF-8', "main", true, '../../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");


$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
    
    if (isset($_POST["__stepEvt"])) {
        if (is_numeric($_POST["__stepEvt"])) {
            $step = quote_smart($_POST["__stepEvt"]);
            if (isset($_POST["NextEvt"]) || isset($_POST["NextEvt_x"]))
                $step += 1;
            if (isset($_POST["PrevEvt"]) || isset($_POST["PrevEvt_x"]))
                $step -= 1;
        } else {
            $step = 0;
        }
    }
    
    if (! isset($step) || $step < 0)
        $step = 0;
    
    $id = quote_smart($_GET["id"]);
    $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $id, $db);
    $dbv = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=11 AND id_City=" . $dbc->get("id"), $db);
    $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
    
    $str = "<form name='form' method=post target='_self'>\n";
    $str .= "<table><tr><td valign='top'>\n";
    $str .= "<table class='maintable' width='720px'>\n";
    $str .= "<tr>\n";
    $str .= "<td class='mainbgtitle'><b>" . localize('L I V R E &nbsp;&nbsp;&nbsp; D E S &nbsp;&nbsp;&nbsp; C O M P T E S ') . "</b>\n";
    $str .= "</td>\n";
    $str .= "<td  class='mainbgtitle'><b> Ville : " . $dbc->get("name") . "</b>\n";
    $str .= "</td>\n";
    $str .= "<td  class='mainbgtitle'><b> Gouverneur : " . $dbp->get("name") . "</b>\n";
    $str .= "</td>\n";
    $str .= "<td rowspan=2 width='35px' class='mainbgtitle'><input id='Previous' class='eventbtnext' type='image' name='NextEvt' src='../../pics/misc/left.gif' value='" .
         localize("previous") . "' /></td>\n";
    $str .= "<td rowspan=2 width='35px' class='mainbgtitle'><input id='Previous' class='eventbtprevious' type='image' name='PrevEvt' src='../../pics/misc/right.gif' value='" .
         localize("next") . "' /></td>\n";
    $str .= "</tr></table>\n";
    $str .= "<input name='__stepEvt' id='__stepEvt' type='hidden' Value='" . $step . "' />\n";
    $str .= "</form>\n";
    
    if ((! $dbv->eof() && $curplayer->accessRoom($dbv->get("id"), TREASURY_ROOM, $db)) || $curplayer->get("authlevel") > 10) {
        $dbe = new DBCollection(
            "SELECT BuildingEvent.date AS date, BuildingEvent.price AS price, BuildingEvent.profit AS profit, Player.name AS nameP, BasicEvent.description AS description, 
            Building.name AS nameB, BuildingAction.name AS nameA FROM BuildingEvent 
            LEFT JOIN Building ON BuildingEvent.id_Building=Building.id 
            LEFT JOIN BuildingAction ON BuildingEvent.id_BuildingAction=BuildingAction.id and BuildingAction.id_BasicBuilding = Building.id_BasicBuilding
            LEFT JOIN BasicEvent ON BuildingEvent.id_BasicEvent=BasicEvent.id 
            LEFT JOIN Player ON BuildingEvent.id_Player=Player.id WHERE BuildingEvent.id_City=" . $id . " and BuildingAction.treasureAccount = 'Yes' ORDER BY date DESC", $db, 
            $step * STEP_TRESEARY_ACCOUNT, STEP_TRESEARY_ACCOUNT);
        $array = array();
        while (! $dbe->eof()) {
            $body = localize($dbe->get("description"), array(
                "player" => $dbe->get("nameP")
            ));
            
            $array[] = array(
                "0" => array(
                    date("Y-m-d H:i:s", gmstrtotime($dbe->get("date"))),
                    "class='mainbgbody' align='left'"
                ),
                "1" => array(
                    $dbe->get("nameB"),
                    "class='mainbgbody' align='center'"
                ),
                "2" => array(
                    $dbe->get("nameA"),
                    "class='mainbgbody' align='center'"
                ),
                "3" => array(
                    $body,
                    "class='mainbgbody' align='left'"
                ),
                "4" => array(
                    $dbe->get("price"),
                    "class='mainbgbody' align='center'"
                ),
                "5" => array(
                    $dbe->get("profit"),
                    "class='mainbgbody' align='center'"
                )
            );
            $dbe->next();
        }
        
        $str .= createTable(6, $array, array(), 
            array(
                array(
                    localize("Date"),
                    "class='mainbglabel' width='12%' align='left'",
                    "name",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Bâtiment"),
                    "class='mainbglabel' width='13%' align='center'",
                    "price",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Service"),
                    "class='mainbglabel' width='19%' align='center'",
                    "marge",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Description"),
                    "class='mainbglabel' width='38%' align='center'",
                    "marge",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Prix payé"),
                    "class='mainbglabel' width='8%' align='center'",
                    "marge",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Taxe perçue"),
                    "class='mainbglabel' width='8%' align='center'",
                    "marge",
                    "mainbglabelhover",
                    "mainbglabel"
                )
            ), "class='maintable' width='720px'", "formid", "order");
    }
    $MAIN_BODY->add($str);
    $MAIN_PAGE->render();
}
 
