<?php

/**
 *Définit la classe CQHostel
 *
 * La <b>fonction toString</b> affiche la partie du menu qui gère <b>l'auberge</b>.
 *
 *
 *@author Aé Li
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class CQEntrance extends HTMLObject
{

    public $db;

    public $curplayer;

    public $curbuilding;

    public $nacridan;

    public $minimap;

    public $radius;

    public $err;

    public $body;

    public function CQEntrance($building, &$body, $nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        $this->curbuilding = $building;
        $this->body = $body;
        $this->minimap = null;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $curbuilding = $this->curbuilding;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        $blevel = $curbuilding->get("level");
        $bsp = $curbuilding->get("sp");
        $idbb = $curbuilding->get("id_BasicBuilding");
        $bvalue = $curbuilding->get("value");
        
        // $dbbasicb = new DBCollection("SELECT price,solidity FROM BasicBuilding WHERE id=".$idbb,$db,0,0,0);
        require_once (HOMEPATH . "/factory/CityFactory.inc.php");
        $nextprice = CityFactory::getPriceToUpgradeBuilding($curbuilding->get("id"), $db);
        $nextsp = CityFactory::getNextSPBuilding($curbuilding->get("id"), $db);
        
        /*
         * floor($dbbasicb->get("price") * pow((3/2),$blevel) + pow((3/2),($blevel+1)));
         *
         * $solidity = $dbbasicb->get("solidity");
         *
         * if($solidity == "Fragile")
         * $nextsp = 50 + 10*pow($blevel,2);
         * elseif($solidity == "Medium")
         * $nextsp = 100 + 20*pow($blevel,2);
         * elseif($solidity == "Solid")
         * $nextsp = 150 + 30*pow($blevel,2);
         */
        
        $str .= "<table class='maintable insidebuildingleftwidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan=2 class='mainbgtitle'> <b>" . localize('A C C U E I L') . "</b> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        if (isset($_POST["buy"])) {
            if ($id == $curbuilding->get("id_Player"))
                $str .= "<td class='mainbgtitle'>Cette maison vous appartient déjà !</td></tr>";
            elseif ($curplayer->get("money") < $bvalue)
                $str .= "<td class='mainbgtitle'>Vous n'avez pas suffisant d'argent pour acheter cette maison !</td></tr>";
            else {
                $curplayer->set("money", $curplayer->get("money") - $bvalue);
                $curplayer->updateDB($db);
                $curbuilding->set("value", 0);
                $curbuilding->set("id_Player", $id);
                $curbuilding->updateDB($db);
                $dbx = new DBCollection("UPDATE City SET money=money+" . $bvalue . " WHERE id=" . $curbuilding->get("id_City"), $db, 0, 0, false);
                $str .= "<td class='mainbgtitle'>Vous êtes désormais propriétaire de cette maison. </td></tr>";
            }
        } elseif ($bvalue > 0 and $idbb == 10) {
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<td class='mainbgtitle'>Cette maison est en vente au prix de " . $bvalue . " PO  \n";
            $str .= "</td><td><input name='buy' type='submit' value='Acheter'/></td></tr></form>";
        }
        
        if (isset($_POST["upgradefin"])) { // MAJ du bâtiment pour le nouveau niveau
            
            if ($curplayer->get("money") < $nextprice) {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Vous n'avez pas assez d'or pour payer les ouvriers";
                $str .= "</td>\n";
                $str .= "</tr>\n";
            } else {
                $levelup = 1;
                if (isset($_POST["schoolchoice"])) {
                    if ($curbuilding->get("school") != "")
                        $schoolList = unserialize($curbuilding->get("school"));
                    else
                        $schoolList = array();
                    
                    if ($idbb == 8) {
                        $dbBT = new DBCollection("SELECT id,name,type FROM BasicTalent", $db, 0, 0);
                        $check = 0;
                        $talentnames = "";
                        while (! $dbBT->eof()) {
                            if (array_key_exists($dbBT->get("id"), $_POST)) {
                                $schoolList[$_POST[$dbBT->get("id")]] = $dbBT->get("type");
                                $check ++;
                                $talentnames .= ", " . $dbBT->get("name");
                            }
                            $dbBT->next();
                        }
                        
                        if ($check < 6) {
                            $curbuilding->set("school", serialize($schoolList));
                            $curbuilding->updateDB($db);
                            $str .= "<tr><td class='mainbglabel'> Vous avez ouvert les cours pour les savoir-faire : " . $talentnames . ". </td></tr>";
                        } else {
                            $str .= "<tr><td class='mainbglabel'> Vous avez sélectionnés trop de cours. Vous êtes limités à 5 nouveaux cours.</td></tr>";
                            $levelup = 0;
                        }
                    } else {
                        $schoolList[$_POST["schoolchoice"]] = 1;
                        $curbuilding->set("school", serialize($schoolList));
                        $curbuilding->updateDB($db);
                        
                        $str .= "<tr><td class='mainbglabel'> Vous avez ouvert les cours niveau Aspirant de l'" . getSchoolName($_POST["schoolchoice"]) . " </td></tr>";
                    }
                } elseif (isset($_POST["upschoolchoice"])) {
                    if ($curbuilding->get("school") != "")
                        $schoolList = unserialize($curbuilding->get("school"));
                    else
                        $schoolList = array();
                    
                    if ($_POST["upschoolchoice"] > - 1) {
                        $schoolList[$_POST["upschoolchoice"]] = 2;
                        $curbuilding->set("school", serialize($schoolList));
                        $curbuilding->updateDB($db);
                        $str .= "<tr><td class='mainbglabel'> Vous avez ouvert les cours niveau Adepte de l'" . getSchoolName($_POST["upschoolchoice"]) . " </td></tr>";
                    } else {
                        $str .= "<tr><td class='mainbglabel'> Vous ne pouvez pas améliorer votre école.</td></tr>";
                        $levelup = 0;
                    }
                } elseif (isset($_POST["AP"]) && ($bvalue < 6)) {
                    $curbuilding->set("value", $curbuilding->get("value") + 2);
                    $curbuilding->updateDB($db);
                    $str .= "<tr><td class='mainbglabel'> Vous avez réduit le temps d'apprentissage de 2 PA. </td></tr>";
                }
                
                if ($levelup) {
                    $curbuilding->set("level", $blevel + 1);
                    // $curbuilding->set("sp",$nextsp);
                    // $curbuilding->set("currsp",$curbuilding->get("currsp")+$nextsp - $bsp);
                    $curplayer->set("money", $curplayer->get("money") - $nextprice);
                    $curplayer->updateDB($db);
                    $curbuilding->updateDB($db);
                    $str .= "<tr>\n";
                    // $str.="<td class='mainbglabel'> Le bâtiment gagne un niveau et ".($nextsp - $bsp)." PS.";
                    $str .= "</td>\n";
                    $str .= "</tr>\n";
                }
            }
        } elseif (isset($_POST["upgrade2"])) { // Ecole - détail du choix de l'option du nouveau niveau
            $isError = 0;
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            
            if ($curbuilding->get("school") != "")
                $schoolList = unserialize($curbuilding->get("school"));
            else
                $schoolList = array();
            
            if ($_POST["upgrade2"] == "newschool") {
                $str .= "<tr><td class='mainbgbody'>Liste des écoles que vous pouvez ouvrir :";
                
                if ($idbb == 7) {
                    $smin = 5;
                    $smax = 8;
                    $str .= " <select id='schoolchoice' class='selector cqattackselectorsize' name='schoolchoice'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisir une école --") . "</option>";
                    for ($i = $smin; $i <= $smax; $i ++) {
                        if (! array_key_exists($i, $schoolList))
                            $str .= "<option value='" . $i . "' >" . getSchoolName($i) . "</option>";
                    }
                    $str .= "</select></td></tr>";
                } elseif ($idbb == 6) {
                    $smin = 1;
                    $smax = 4;
                    $str .= " <select id='schoolchoice' class='selector cqattackselectorsize' name='schoolchoice'>";
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisir une école --") . "</option>";
                    for ($i = $smin; $i <= $smax; $i ++) {
                        if (! array_key_exists($i, $schoolList))
                            $str .= "<option value='" . $i . "' >" . getSchoolName($i) . "</option>";
                    }
                    $str .= "</select></td></tr>";
                } elseif ($idbb == 8) {
                    $dbBT = new DBCollection("SELECT id,name FROM BasicTalent", $db, 0, 0);
                    $str .= "</td></tr><tr><td class='mainbgbody'>(Vous pouvez choisir d'ouvrir au maximum cinq nouveaux cours. )</td></tr>";
                    while (! $dbBT->eof()) {
                        if (! array_key_exists($dbBT->get("id"), $schoolList))
                            $str .= "<tr><td class='mainbgbody'><input type='checkbox' name='" . $dbBT->get("id") . "' value='" . $dbBT->get("id") . "' id='" . $dbBT->get("name") .
                                 "' /><label for='" . $dbBT->get("name") . "'>" . $dbBT->get("name") . " </label></td></tr>";
                        
                        $dbBT->next();
                    }
                    $str .= "<input type='hidden' name='schoolchoice' value='yes'/> ";
                }
            } elseif ($_POST["upgrade2"] == "upschool") {
                $str .= "<tr><td class='mainbgbody'>Liste des écoles niveau Aspirant que vous pouvez passer niveau Adepte : ";
                $strval1 = "<select id='upschoolchoice' class='selector cqattackselectorsize' name='upschoolchoice'>";
                $strval1 .= "<option value='0' selected='selected'>" . localize("-- Choisir une école --") . "</option>";
                $strtmp = "";
                foreach ($schoolList as $idname => $level) {
                    if ($level == 1) {
                        $strtmp .= "<option value='" . $idname . "' >" . getSchoolName($idname) . "</option>";
                    }
                }
                $strval1 .= $strtmp;
                $strval1 .= "</select>";
                
                if (strlen($strtmp) > 0) {
                    $str .= $strval1;
                } else {
                    $isError = 1;
                    $str .= "<br/><b>Il n'y a pas d'école que vous pouvez améliorer.</b>";
                    $str .= "<input type='hidden' id='upschoolchoice' value='-1' name='upschoolchoice'>";
                }
                
                $str .= "</td></tr>";
            } elseif ($_POST["upgrade2"] == "PA") {
                $str .= "<tr><td class='mainbgbody'>Vous avez choisi de diminuer le coût d'apprentissage.</td></tr>";
                $str .= "<input type='hidden' name='AP' value='yes'/> ";
            }
            
            if ($isError == 0) {
                $str .= "<tr><td class='mainbglabel'> Vous allez passer ce bâtiment du niveau " . $blevel . " au niveau " . ($blevel + 1) . " et cela va vous coûter " . $nextprice .
                     " PO.</td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Confirmer'/></td></tr> ";
                $str .= "<input type='hidden' name='upgradefin' value='yes'/> ";
            } else {
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Retour'/></td></tr> ";
            }
            $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
            $str .= "</form>";
        } elseif (isset($_POST["upgrade1"])) { // Ecole - choix de l'option du nouveau niveau
            
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            
            if (($idbb == 7) or ($idbb == 6)) {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                $str .= "</td></tr>\n";
                
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='newschool' id='newschool' /><label for='newschool'>Ouvrir une nouvelle école niveau Aspirant.</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='upschool' id='upschool' /><label for='upschool'>Passer niveau Adepte une école de niveau Aspirant déjà ouverte.</label></td></tr>";
                
                if ($bvalue < 6)
                    $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='PA' id='PA' /><label for='PA'>Diminuer le coût d'apprentissage de 2PA</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Ok'/> </td></tr> ";
                $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
            } elseif ($idbb == 8) {
                
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Choisissez parmi les possibilités suivantes :";
                $str .= "</td></tr>\n";
                
                $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='newschool' id='newschool' /><label for='newschool'>Ouvrir un nouveau cours.</label></td></tr>";
                if ($bvalue < 6)
                    $str .= "<tr><td class='mainbgbody'><input type='radio' name='upgrade2' value='PA' id='PA' /><label for='PA'>Diminuer le coût d'apprentissage de 2PA</label></td></tr>";
                $str .= "<tr><td class='mainbgbody'><input type='submit' name='gotoroom' value='Ok'/> </td></tr> ";
                $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
            } else {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'> Vous allez passer ce bâtiment du niveau " . $blevel . " au niveau " . ($blevel + 1) . " et cela va vous coûter " . $nextprice .
                     " PO.";
                
                $str .= "<input type='submit' name='gotoroom' value='Confirmer'/> ";
                $str .= "<input type='hidden' name='upgradefin' value='yes'/> ";
                $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
                
                $str .= "</td></tr>\n";
            }
            $str .= "</form>";
        } else {
            if ($curbuilding->get("id_BasicBuilding") < 0) {
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel'>" . $curbuilding->get("name") . " de niveau " . $blevel . ".  Payer des ouvriers pour rajouter un niveau ? \n";
                $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                $str .= "<input type='submit' name='gotoroom' value='Ok'/> ";
                $str .= "<input type='hidden' name='upgrade1' value='yes'/> ";
                $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
                $str .= "</form>";
                $str .= "</td>\n";
                $str .= "</tr>\n";
            }
            require_once (HOMEPATH . '/conquest/cqview2D.inc.php');
            $tempview = new CQView2D($this->nacridan, $this->body, $this->db);
            $this->radius = $tempview->radius;
            $this->minimap = $tempview->minimap;
            $str .= $tempview->str;
        }
        
        $str .= "</table>\n";
        
        return $str;
    }
}
?>
