<?php

class CQSpell extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $id;

    public function CQSpell($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (isset($_GET["id"])) {
            $this->id = $id = quote_smart($_GET["id"]);
            $this->str = $this->getForm();
        } else {
            $this->str = "";
        }
    }

    protected function getBuilding($name)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        $db = $this->db;
        
        $dbc = new DBCollection(
            "SELECT * FROM Building WHERE id_BasicBuilding!=14 AND name='" . $name . "' AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" .
                 $yp . "))/2 <= 2", $db);
        $item = array();
        $str = "<select style='margin-left:20px;' class='selector cqattackselectorsize' name='TARGET_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un bassin --") . "</option>";
        $item[] = array(
            localize("-- Bassin à portée --") => - 1
        );
        while (! $dbc->eof()) {
            if ($this->curplayer->canSeeCase($dbc->get("x"), $dbc->get("y"), $db))
                $item[] = array(
                    "Bassin divin " . localize($dbc->get("x") . "-" . $dbc->get("y")) . " (" . $dbc->get("id") . ")" => $dbc->get("id")
                );
            $dbc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        return $str;
    }

    protected function getStuff()
    {
        $str = "<select style='margin-left:20px;' class='selector cqattackselectorsize' name='SLOT'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un équipement --") . "</option>";
        $str .= "<option value='1'> Casque </option>";
        $str .= "<option value='2'> Bottes </option>";
        $str .= "<option value='3'> Jambières </option>";
        $str .= "<option value='4'> Buste </option>";
        $str .= "<option value='5'> Epaulière gauche </option>";
        $str .= "<option value='6'> Epaulière droite </option>";
        $str .= "<option value='7'> Bouclier </option>";
        $str .= "<option value='8'> Gants </option>";
        $str .= "<option value='9'> Ceinture </option>";
        $str .= "<option value='10'> Corps (PNJ) </option>";
        
        $str .= "</select>";
        
        return $str;
    }

    protected function getDirection()
    {
        $str = "<select style='margin-left:20px;'class='selector cqattackselectorsize' name='SLOT'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une direction --") . "</option>";
        $str .= "<option value='1'> Estan </option>";
        $str .= "<option value='2'> Sudan </option>";
        $str .= "<option value='3'> Ouestan </option>";
        $str .= "<option value='4'> Nordan </option>";
        $str .= "<option value='5'> Izan </option>";
        $str .= "<option value='6'> Dustan </option>";
        $str .= "</select>";
        
        return $str;
    }

    protected function getFigthCharac()
    {
        $str = "<select style='margin-left: 20px;' class='selector' name='CHARAC'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une caractéristique --") . "</option>";
        $str .= "<option value='1'> Attaque </option>";
        $str .= "<option value='2'> Dégâts </option>";
        $str .= "</select>";
        
        return $str;
    }

    protected function getCharac()
    {
        $str = '<select style="margin-left: 20px;" class="selector" name="CHARAC">';
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une caractéristique --") . "</option>";
        $str .= "<option value='1'> Dextérité </option>";
        $str .= "<option value='2'> Force </option>";
        $str .= "<option value='3'> Vitesse </option>";
        $str .= "<option value='4'> Enchantements </option>";
        $str .= "</select>";
        
        return $str;
    }

    protected function getPlaceFree($name)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        $curplayer = $this->curplayer;
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        $item = array();
        $str = "<select style='margin-left:20px; margin-right:20px;' class='selector cqattackselectorsize' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un emplacement --") . "</option>";
        
        for ($i = - 2; $i < 3; $i ++) {
            for ($j = - 2; $j < 3; $j ++) {
                echo "(X= " . ($xp + $i) . " , Y= " . ($yp + $j) . ")";
                echo "CanSee = " . $curplayer->canSeeCase($xp + $i, $yp + $j, $this->db);
                echo "freePlace = " . BasicActionFactory::freePlace($xp + $i, $yp + $j, $map, $this->db, 1) . "\n";
                
                if (BasicActionFactory::freePlace($xp + $i, $yp + $j, $map, $this->db, 1) && distHexa($xp, $yp, $xp + $i, $yp + $j) < 3 &&
                     $curplayer->canSeeCase($xp + $i, $yp + $j, $this->db))
                    $item[] = array(
                        localize("(X= " . ($xp + $i) . " , Y= " . ($yp + $j) . ")") => ($xp + $i) . "," . ($yp + $j)
                    );
            }
        }
        if ($name != "Case1")
            $item[] = array(
                localize("Ignorer ce champs") => - 1
            );
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value)
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
        }
        
        $str .= "</select>";
        return $str;
    }

    protected function getPlace($name, $checkIsCanSee = true)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        $curplayer = $this->curplayer;
        
        $item = array();
        $str = "<select style='margin-left:20px;' class='selector' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Lieu ciblé --") . "</option>";
        
        for ($i = - 2; $i < 3; $i ++) {
            for ($j = - 2; $j < 3; $j ++) {
                if (distHexa($xp, $yp, $xp + $i, $yp + $j) < 3 && ($curplayer->canSeeCase($xp + $i, $yp + $j, $this->db) || ! $checkIsCanSee))
                    $item[] = array(
                        localize("(X= " . ($xp + $i) . " , Y= " . ($yp + $j) . ")") => ($xp + $i) . "," . ($yp + $j)
                    );
            }
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value)
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
        }
        $str .= "</select>";
        return $str;
    }

    protected function getTeleport()
    {
        $strAct = "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
        
        $str = localize(" en </td><td style='text-align: center;'> X = ");
        $str .= "<input type='textbox' name='NEWX' />";
        $str .= localize(" </td><td style='text-align: center;'> Y = ");
        $str .= "<input type='textbox' name='NEWY' />";
        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
        $str .= "</tr><tr style='height:45px;' class='mainbgtitle'><td style='font-size:10px;' colspan='4'>";
        $str .= localize("Attention ! Si votre jet de MM n'est pas suffisant, la téléportation n'aura pas lieu et si la case est occupée vous mourrez !");
        return $str;
    }

    protected function getMassiveTeleport()
    {
        $strAct = "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
        
        $str = localize(" en </td><td style='text-align: center;'> X = ");
        $str .= "<input type='textbox' name='NEWX' />";
        $str .= localize(" </td><td style='text-align: center;'> Y = ");
        $str .= "<input type='textbox' name='NEWY' />";
        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
        $str .= "</tr><tr style='height:45px;' class='mainbgtitle'><td style='font-size:10px;' colspan='4'>";
        $str .= localize(
            "Vous devez être dans un temple pour utiliser ce sort. Attention ! Si votre jet de MM et si la portée du temple ne sont pas suffisants, la téléportation n'aura pas lieu!");
        return $str;
    }

    protected function getSoul()
    {
        $strAct = "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
        
        $str = localize(" en </td><td style='text-align: center;'> X = ");
        $str .= "<input type='textbox' name='NEWX' />";
        $str .= localize(" </td><td style='text-align: center;'> Y = ");
        $str .= "<input type='textbox' name='NEWY' />";
        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
        $str .= "</tr><tr style='height:45px;' class='mainbgtitle'><td style='font-size:10px;' colspan='4'>";
        $str .= localize("Attention si votre de jet de MM n'est pas suffisant, la projection de l'âme n'aura lieu");
        return $str;
    }

    protected function getContact()
    {
        $id = $this->curplayer->get("id");
        $map = $this->curplayer->get("map");
        
        $dbc = new DBCollection("SELECT Player.id as id, Player.name as name FROM MailContact LEFT JOIN Player ON MailContact.id_Player\$friend = Player.id WHERE id_Player=" . $id, 
            $this->db);
        $item = array();
        $str = "<select style='margin-left:20px;' class='selector cqattackselectorsize' name='TARGET_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un contact --") . "</option>";
        
        while (! $dbc->eof()) {
            $item[] = array(
                $dbc->get("name") => $dbc->get("id")
            );
            $dbc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getGdC($gap = 1)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $curplayer = $this->curplayer;
        $map = $this->curplayer->get("map");
        $inbuilding = $this->curplayer->get("inbuilding");
        $room = $this->curplayer->get("room");
        
        $item = array();
        $str = "</td><td><select style='margin-left:20px;' class='selector cqattackselectorsize' name='TARGET_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
        
        $dbp = new DBCollection(
            "SELECT * FROM Player WHERE status='PC' AND id_FighterGroup=" . $curplayer->get("id_FighterGroup") . " AND inbuilding=" . $inbuilding . " AND room=" . $room .
                 " AND disabled=0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap, $this->db, 0, 0);
        $item[] = array(
            localize("-- Personnage(s) à portée --") => - 1
        );
        while (! $dbp->eof()) {
            if (($dbp->get("id") != $id) && $curplayer->canSeePlayerById($dbp->get("id"), $this->db)) {
                $item[] = array(
                    $dbp->get("name") => $dbp->get("id")
                );
            }
            $dbp->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getSelector($myself = 0, $pj = 1, $pnj = 1, $gap = 1, $name = "TARGET_ID", $onlyFeuFol = 0, $emptyIfNoTarget = 0)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $curplayer = $this->curplayer;
        $map = $this->curplayer->get("map");
        $inbuilding = $this->curplayer->get("inbuilding");
        $room = $this->curplayer->get("room");
        $db = $this->db;
        $action = constant($this->id);
        
        $item = array();
        $str = "</td><td style='padding-left:20px;'><select class='selector cqattackselectorsize' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
        
        for ($i = 0; $i < $gap + 1; $i ++) {
            // PNJ
            $dbnpc[] = new DBCollection(
                "SELECT * FROM Player WHERE status='NPC' AND disabled=0 AND (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND map=" . $map .
                     " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=" . $i .
                     ($onlyFeuFol == 1 ? " AND id_Member = 0 AND id_BasicRace = " . ID_BASIC_RACE_FEU_FOL : ""), $db, 0, 0);
            // PJ
            $dbp[] = new DBCollection(
                "SELECT * FROM Player WHERE status='PC' AND disabled=0 AND (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND map=" . $map .
                     " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=" . $i .
                     ($onlyFeuFol == 1 ? " AND id_Member = 0 AND id_BasicRace = " . ID_BASIC_RACE_FEU_FOL : ""), $db, 0, 0);
        }
        
        if ($room == TOWER_ROOM) {
            if ($gap == 1)
                $gap = 0;
            for ($i = 0; $i < $gap + 1; $i ++) {
                if ($pnj) {
                    if ($i == 0 && ! $dbnpc[0]->eof())
                        $item[] = array(
                            localize("-- Créature à portée -- ") => - 1
                        );
                    elseif (! $dbnpc[$i]->eof())
                        $item[] = array(
                            localize("-- Créature à " . $i . " case(s) -- ") => - 1
                        );
                    
                    while (! $dbnpc[$i]->eof()) {
                        if (($dbnpc[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbnpc[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[$i]->get("racename")) . " (" . $dbnpc[$i]->get("id") . ")" => $dbnpc[$i]->get("id")
                            );
                        $dbnpc[$i]->next();
                    }
                }
                if ($pj) {
                    if ($i == 0 && ! $dbp[0]->eof())
                        $item[] = array(
                            localize("-- Personnage à portée -- ") => - 1
                        );
                    elseif (! $dbp[$i]->eof()) {
                        
                        $item[] = array(
                            localize("-- Personnage à " . $i . " case(s) -- ") => - 1
                        );
                    }
                    while (! $dbp[$i]->eof()) {
                        if (($dbp[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbp[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[$i]->get("name")) . " (" . $dbp[$i]->get("id") . ")" => $dbp[$i]->get("id")
                            );
                        $dbp[$i]->next();
                    }
                }
            }
        } elseif ($inbuilding) {
            if ($pnj) {
                $item[] = array(
                    localize("-- Créature à portée -- ") => - 1
                );
                while (! $dbnpc[0]->eof()) {
                    if (($dbnpc[0]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbnpc[0]->get("id"), $db))
                        $item[] = array(
                            localize($dbnpc[0]->get("racename")) . " (" . $dbnpc[0]->get("id") . ")" => $dbnpc[0]->get("id")
                        );
                    $dbnpc[0]->next();
                }
            }
            if ($pj) {
                $item[] = array(
                    localize("-- Personnage à portée -- ") => - 1
                );
                while (! $dbp[0]->eof()) {
                    if (($dbp[0]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbp[0]->get("id"), $db))
                        $item[] = array(
                            localize($dbp[0]->get("name")) . " (" . $dbp[0]->get("id") . ")" => $dbp[0]->get("id")
                        );
                    $dbp[0]->next();
                }
            }
        } else {
            for ($i = 1; $i < $gap + 1; $i ++) {
                if ($pnj) {
                    if ($i == 1 && ! $dbnpc[1]->eof())
                        $item[] = array(
                            localize("-- Créature à portée -- ") => - 1
                        );
                    elseif (! $dbnpc[$i]->eof())
                        $item[] = array(
                            localize("-- Créature à " . $i . " case(s) -- ") => - 1
                        );
                    
                    while (! $dbnpc[$i]->eof()) {
                        if (($dbnpc[$i]->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbnpc[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbnpc[$i]->get("racename")) . " (" . $dbnpc[$i]->get("id") . ")" => $dbnpc[$i]->get("id")
                            );
                        $dbnpc[$i]->next();
                    }
                }
                if ($pj) {
                    if ($i == 1 && (! $dbp[1]->eof() || $myself)) {
                        $item[] = array(
                            localize("-- Personnage à portée -- ") => - 1
                        );
                        if ($myself)
                            $item[] = array(
                                localize($curplayer->get("name")) . " (" . $curplayer->get("id") . ")" => $curplayer->get("id")
                            );
                    } elseif (! $dbp[$i]->eof())
                        $item[] = array(
                            localize("-- Personnage à " . $i . " case(s) -- ") => - 1
                        );
                    
                    while (! $dbp[$i]->eof()) {
                        if ($dbp[$i]->get("id") != $id && $curplayer->canSeePlayerById($dbp[$i]->get("id"), $db))
                            $item[] = array(
                                localize($dbp[$i]->get("name")) . " (" . $dbp[$i]->get("id") . ")" => $dbp[$i]->get("id")
                            );
                        $dbp[$i]->next();
                    }
                }
            }
        }
        if (count($item, 1) == 0 && $emptyIfNoTarget == 1) {
            return "";
        }
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    public function toString()
    {
        return $this->str;
    }

    /*
     * Fonction permettant l'affichage du menu des sortilèges
     */
    public function getForm()
    {
        $dbc = new DBCollection(
            "SELECT * FROM MagicSpell LEFT JOIN BasicMagicSpell ON MagicSpell.id_BasicMagicSpell=BasicMagicSpell.id WHERE ident='" . $this->id . "' AND id_Player=" .
                 $this->curplayer->get("id"), $this->db);
        if (! $dbc->eof()) {
            $time = $dbc->get("PA");
            if ($this->curplayer->get("ap") < $time) {
                $str = "<table class='maintable bottomareawidth'><tr style='height:45px;' class='mainbgtitle'><td>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser le sortilège : {spell}.", 
                    array(
                        "spell" => localize($dbc->get("name"))
                    ));
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr style='height:45px;' class='mainbgtitle'><td >";
                
                $strAct = "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . constant($this->id) . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                
                $str .= localize($dbc->get("name")) . " ";
                switch (constant($this->id)) {
                    case SPELL_BURNING:
                        $str .= $this->getSelector();
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_BLOOD:
                        $str .= $this->getSelector(0, 1, 1, 2);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case M_INSTANTBLOOD:
                        $str .= $this->getSelector(0, 1, 1, 2);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_RECALL:
                        $str .= "</td><td>" . $this->getContact();
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_TEARS:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_ARMOR:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_CURE:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_BUBBLE:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_TELEPORT:
                        $str .= $this->getTeleport();
                        break;
                    case S_MASSIVEINSTANTTP:
                        $str .= $this->getMassiveTeleport();
                        break;
                    case SPELL_SOUL:
                        $str .= $this->getSoul();
                        break;
                    case SPELL_REGEN:
                        $str .= $this->getSelector(1, 1, 1, 2);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_ANGER:
                        $str .= "sur " . $this->getSelector(1, 1, 1, 2) . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr style='height:45px;' class='mainbgtitle'> <td  width='180px'> pour la caractéristique </td><td>" . $this->getFigthCharac();
                        break;
                    case SPELL_CURSE:
                        $str .= "sur " . $this->getSelector(0, 1, 1, 2) . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= '<tr style="height:45px;" class="mainbgtitle"> <td width="180px"> pour la caractéristique </td><td>' . $this->getCharac();
                        break;
                    case SPELL_PILLARS:
                        $str .= "</td><td>" . $this->getPlace("Case1", false) . "<td align='center' rowspan=3> " . $strAct . " </td></tr>";
                        $str .= "<tr style='height:45px;' class='mainbgtitle'> <td width='250px' rowspan=2> Rappel : les 3 lieux ciblés doivent être adjacents </td><td>" .
                             $this->getPlace("Case2", false) . " </td></tr>";
                        $str .= "<tr style='height:45px;' class='mainbgtitle'> <td>" . $this->getPlace("Case3", false) . " </td></tr>";
                        break;
                    case SPELL_WALL:
                        $str .= " </td><td> " . $this->getPlaceFree("Case1");
                        $str .= " </td><td align='center' rowspan=4>" . $strAct;
                        $str .= " </td></tr><tr style='height:45px;' class='mainbgtitle'><td style='text-align:right; padding-right:10px;'>et</td><td> " .
                             $this->getPlaceFree("Case2");
                        $str .= " </td></tr><tr style='height:45px;' class='mainbgtitle'><td style='text-align:right; padding-right:10px;'>et</td><td> " .
                             $this->getPlaceFree("Case3");
                        $str .= " </td></tr><tr style='height:45px;' class='mainbgtitle'><td style='text-align:right; padding-right:10px;'>et</td><td> " .
                             $this->getPlaceFree("Case4");
                        $str .= " </td></tr>";
                        
                        break;
                    case SPELL_BARRIER:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_BLESS:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_SHIELD:
                        $str .= $this->getSelector(1, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_LIGHTTOUCH:
                        $str .= $this->getSelector(0, 1, 1, 2);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_FIREBALL:
                        $str .= $this->getSelector(0, 1, 1, 3);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_NEGATION:
                        $str .= $this->getGdC(3);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_SPRING:
                        $str .= "</td><td>" . $this->getPlaceFree("Case1");
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_RELOAD:
                        $str .= "</td><td>" . $this->getBuilding("Bassin divin");
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_SUN:
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_FIRE:
                        $str .= "</td><td>" . $this->getPlace("TARGET_ID");
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_RAIN:
                        $str .= $this->getPlace("TARGET_ID");
                        $str .= "</td><td width='100px' style='text-align: center;' align='right'>" . $strAct;
                        break;
                    case SPELL_DEMONPUNCH:
                        $str .= $this->getSelector(0, 1, 1);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right' rowspan=2>" . $strAct;
                        $str .= "</td></tr><tr style='height:45px;' class='mainbgtitle'><td></td><td>" . $this->getStuff();
                        break;
                    case SPELL_KINE:
                        $str .= $this->getSelector(0, 1, 1, 5);
                        $str .= "</td><td width='100px' style='text-align: center;' align='right' rowspan=2>" . $strAct;
                        $str .= "</td></tr><tr style='height:45px;' class='mainbgtitle'><td>vers</td><td>" . $this->getDirection();
                        break;
                    case SPELL_FIRECALL:
                        $resultSelector = $this->getSelector(0, 1, 1, 1, "TARGET_ID", 1, 1);
                        if ($resultSelector == "") {
                            $str .= $strAct;
                        } else {
                            $str .= " pour domestiquer la cible: " . $resultSelector . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                            $str .= '<tr style="height:45px;" class="mainbgtitle"> <td colspan=2 width="180px"> Laissez vide pour créer un nouveau feu fol </td></tr>';
                        }
                        break;
                    default:
                        $str .= $strAct;
                        break;
                }
                
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        } else {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous ne possédez pas ce sortilège.");
            $str .= "</td></tr></table>";
        }
        return $str;
    }
}
?>
