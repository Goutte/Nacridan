<?php
DEFINE("STEP", 8);
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/BasicEquipment.inc.php");
require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");

class CQInsideBuilding extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $minimap;

    public $radius;

    public $err;

    public $pj;

    public $pnj;

    public $items;

    public $horses;

    public $body;

    public $result;

    public function CQInsideBuilding($nacridan, &$body, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->body = $body;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        $this->build_horses(); // les montures vont direct dans l'écurie
        $this->minimap = null;
        $this->radius = SIZEAREA;
        $body->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/minimap.js") . "'></script>");
        $this->prepareToString();
    }

    public function build_horses()
    {
        $this->horses = array();
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $idb = $curplayer->get("inbuilding");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $this->pnj = array();
        
        // A decommenter quand les montures seront codés
        // $dbnpc=new DBCollection("SELECT * FROM Player WHERE rider=1 AND inbuilding=".$idb." AND hidden=0 AND disabled=0 AND status='NPC' order by level desc, racename",$db,0,0,0);
        
        // Les tortues
        $dbnpc = new DBCollection("SELECT * FROM Player WHERE id_BasicRace=263 AND room=2 AND inbuilding=" . $idb . "  AND disabled=0 AND status='NPC' order by level desc", $db, 0, 
            0);
        $state = array(
            localize("Agonisant"),
            localize("Gravement blessé(e)"),
            localize("Blessé(e)"),
            localize("Légèrement blessé(e)"),
            ""
        );
        
        while (! $dbnpc->eof()) {
            $hp = $dbnpc->get("hp");
            $hp = 10;
            $curstate = min(floor($dbnpc->get("currhp") * 5 / $hp), 4);
            
            $level = $dbnpc->get("level");
            
            $name = "<a href='../conquest/profile.php?id=" . $dbnpc->get("id") .
                 "' class='stylenpc popupify'>" .
                 localize($dbnpc->get("racename")) . "</a> (" . $dbnpc->get("id") . ")";
            
            $dbc = new DBCollection("SELECT * FROM Player LEFT JOIN Caravan ON Player.id=Caravan.id_Player WHERE  Caravan.id=" . $dbnpc->get("id_Caravan"), $this->db);
            $owner = $dbc->get("name");
            
            $pnjtmp = array(
                "level" => $level,
                "racename" => $name,
                "id" => $dbnpc->get("id"),
                "state" => $state[$curstate],
                "owner" => $owner
            );
            
            $this->horses[] = $pnjtmp;
            $dbnpc->next();
        }
    }

    public function build_pj()
    {
        $this->pj = array();
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        $map = $curplayer->get("map");
        
        $idteam = $curplayer->get("id_Team");
        $query = "SELECT
				p.id,p.name,p.id_Team,p.x,p.y,p.hp,p.currhp,
				p.level,p.racename,t.name as alliegance,
				ppj.type as dppj,
				po.type as dpo,
				opj.type as dopj,
				oo.type as doo
			FROM 
				Player as p
				LEFT JOIN Team as t ON t.id=p.id_Team
				LEFT JOIN PJAlliancePJ as ppj ON p.id=ppj.id_Player\$dest and ppj.id_Player\$src='$id'
				LEFT JOIN PJAlliance as po ON p.id_Team=po.id_Team and po.id_Player\$src='$id'
				LEFT JOIN TeamAlliancePJ as opj ON p.id=opj.id_Player and opj.id_Team\$src='$idteam'
				LEFT JOIN TeamAlliance as oo ON p.id_Team=oo.id_Team\$dest and oo.id_Team\$src='$idteam'
			WHERE
				map=" . $map . " AND inbuilding=" . $curplayer->get("inbuilding") . " AND state!='prison' AND room=" . $curplayer->get("room") . " AND disabled=0 AND status='PC' 				
			GROUP BY p.id
			ORDER BY alliegance, level DESC";
        
        $dbplayer = new DBCollection($query, $db, 0, 0);
        
        $distVal = 0;
        $state = array(
            localize(", Agonisant"),
            localize(", Gravement blessé(e)"),
            localize(", Blessé(e)"),
            localize(", Légèrement blessé(e)"),
            ""
        );

        $count = $dbplayer->count();
        while (! $dbplayer->eof()) {
            if ($curplayer->canSeePlayerById($dbplayer->get("id"), $db)) {
                $hp = $dbplayer->get("hp");
                $curstate = min(floor($dbplayer->get("currhp") * 5 / $hp), 4);
                
                $xx = $dbplayer->get("x");
                $yy = $dbplayer->get("y");
                
                $level = $dbplayer->get("level");
                $distVal = max(abs($xp - $xx), abs($yp - $yy));
                $id_Team = $dbplayer->get("id_Team");
                
                if (($value = $dbplayer->get("dppj")) != null) {
                    $style = "self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } elseif (($value = $dbplayer->get("dopj")) != null) {
                    $style = "guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } else
                    $style = "stylepc";
                
                if (($value = $dbplayer->get("dpo")) != null) {
                    $gc = " self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } elseif (($value = $dbplayer->get("doo")) != null) {
                    $gc = " guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } else
                    $gc = "styleall";
                
                $gc = ($id_Team == $idteam) ? "myalliegence" : $gc;

                $name  = '';
                if ($dbplayer->get("id") != $id) {
                    $name .= Envelope::render($dbplayer->get('name'));
                } else if ($count > 1) {
                    $name .= Envelope::renderEmptySize();
                }
                $name .= "<a href='../conquest/profile.php?id=" . $dbplayer->get("id") .
                     "' class='$style popupify'>" . $dbplayer->get("name") . "</a>";
                
                $alliegance = "<a href='../conquest/alliegance.php?id=$id_Team' class='$gc popupify' >" .
                     $dbplayer->get("alliegance") . "</a>";
                
                $pjtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "level" => $level,
                    "guildcolor" => $gc,
                    "dist" => $distVal,
                    "name" => $name,
                    "race" => $dbplayer->get("racename"),
                    "id" => $dbplayer->get("id"),
                    "alliegance" => $alliegance,
                    "id_Team" => $id_Team,
                    "state" => $state[$curstate]
                );
                $this->pj[] = $pjtmp;
            }
            $dbplayer->next();
        }
    }

    public function build_items()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $this->items = array();
        
        // Equipment
        $dbequipment = new DBCollection(
            "SELECT id,x,y,name,po,extraname,level,maudit FROM Equipment WHERE state='Ground' AND inbuilding=" . $curplayer->get("inbuilding") . " AND room=" .
                 $curplayer->get("room"), $db, 0, 0);
        
        while (! $dbequipment->eof()) {
            $xx = $dbequipment->get("x");
            $yy = $dbequipment->get("y");
            $level = $dbequipment->get("level");
            $distVal = max(abs($xp - $xx), abs($yp - $yy));
            
            if ($dbequipment->get("name") != "") {
                $name = "<span class='" . ($dbequipment->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($dbequipment->get("name")) . "</span>";
                
                if ($dbequipment->get("extraname") != "") {
                    $i = 0;
                    $tmodifier = new Modifier();
                    $template = new Template();
                    $dbt = new DBCollection(
                        "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                             $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                             " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                             $dbequipment->get("id"), $db);
                    while (! $dbt->eof()) {
                        $i ++;
                        $template->DBLoad($dbt, "TP");
                        $tmodifier->DBLoad($dbt, "MD");
                        $tmodifier->updateFromTemplateLevel($template->get("level"));
                        // $eqmodifier->addModifObj($tmodifier);
                        if ($i == 1)
                            $extraname = $dbt->get("name1") . "(" . $template->get("level") . ") ";
                        if ($i == 2)
                            $extraname .= $dbt->get("name2") . "(" . $template->get("level") . ")";
                        
                        $dbt->next();
                    }
                }
                
                if ($dbequipment->get("extraname") != "") {
                    $name .= " <span class='template'>" . localize($extraname) . "</span>";
                }
                $name .= " (" . $dbequipment->get("level") . ")";
                
                $itemtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "dist" => $distVal,
                    "name" => $name,
                    "level" => $level
                );
                $this->items[] = $itemtmp;
            } else {
                $name = $dbequipment->get("po") . " " . localize("Pièce(s) d'Or");
                $itemtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "dist" => $distVal,
                    "name" => $name
                );
                $this->items[] = $itemtmp;
            }
            
            $dbequipment->next();
        }
    }

    public function build_pnj()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $idb = $curplayer->get("inbuilding");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $this->pnj = array();
        
        $dbnpc = new DBCollection(
            "SELECT * FROM Player WHERE inbuilding=" . $idb . " AND room=" . $curplayer->get("room") . " AND disabled=0 AND status='NPC' order by level desc, racename", $db, 0, 0);
        $state = array(
            localize("Agonisant"),
            localize("Gravement blessé(e)"),
            localize("Blessé(e)"),
            localize("Légèrement blessé(e)"),
            ""
        );
        
        while (! $dbnpc->eof()) {
            if ($curplayer->canSeePlayerById($dbnpc->get("id"), $db) && ($curplayer->get("room") != 2 || $dbnpc->get("id_BasicRace") != 263)) {
                
                $hp = $dbnpc->get("hp");
                $curstate = min(floor($dbnpc->get("currhp") * 5 / $hp), 4);
                
                $xx = $dbnpc->get("x");
                $yy = $dbnpc->get("y");
                $level = $dbnpc->get("level");
                $distVal = max(abs($xp - $xx), abs($yp - $yy));
                $comp = "";
                
                if ($dbnpc->get("name") != '') {
                    $name = "<a href=\"../conquest/profile.php?id=" . $dbnpc->get("id") .
                         "\" class='stylenpc popupify'>" . localize($dbnpc->get("name")) . "</a> (" . $dbnpc->get("id") . ")" . $comp;
                } else {
                    $name = "<a href=\"../conquest/profile.php?id=" . $dbnpc->get("id") .
                         "\" class='stylenpc popupify'>" . localize($dbnpc->get("racename")) . "</a> (" . $dbnpc->get("id") . ")" . $comp;
                }
                
                $dbc = new DBCollection("SELECT name FROM Player WHERE status='PC' AND id=" . $dbnpc->get("id_Owner"), $db);
                if (! $dbc->eof())
                    $owner = " appartenant à " . $dbc->get("name");
                else
                    $owner = "";
                
                $pnjtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "level" => $level,
                    "dist" => $distVal,
                    "name" => $name,
                    "id" => $dbnpc->get("id"),
                    "state" => $state[$curstate],
                    "id_BasicRace" => $dbnpc->get("id_BasicRace"),
                    "owner" => $owner
                );
                
                $this->pnj[] = $pnjtmp;
            }
            $dbnpc->next();
        }
    }

    public function checkPossibleRoom($room, $id)
    {
        if (($room == ENTRANCE_ROOM) || ($room == STABLE_ROOM))
            return true;
        
        switch ($id) {
            case 1:
                if (($room == HOSTEL_MAIN_ROOM) || ($room == HOSTEL_BOARDS_ROOM))
                    return true;
                else
                    return false;
                break;
            
            case 2:
                if ($room == BANK_OFFICE_ROOM)
                    return true;
                else
                    return false;
                break;
            
            case 4:
                if ($room == CARAVAN_ROOM || $room = EXPRESS_TRANSPORT_ROOM || BANK_OFFICE_ROOM)
                    return true;
                else
                    return false;
                break;
            
            case 5:
                if (($room == MAIN_SHOP_ROOM) || ($room == BASIC_SHOP_ROOM) || ($room == SALES_ROOM) || ($room == REPARE_ROOM))
                    return true;
                else
                    return false;
                break;
            
            case 6:
                if ($room == SCHOOL_ABILITY_CLASS_ROOM)
                    return true;
                else
                    return false;
                break;
            
            case 7:
                if ($room == SCHOOL_SPELL_CLASS_ROOM)
                    return true;
                else
                    return false;
                break;
            
            case 8:
                if ($room == SCHOOL_TALENT_CLASS_ROOM)
                    return true;
                else
                    return false;
                break;
            
            case 9:
                if (($room == WORKSHOP_ROOM) || ($room == ENCHANT_ROOM))
                    return true;
                else
                    return false;
                break;
            
            case 10:
                $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $this->curplayer->get("inbuilding"), $this->db);
                $dbf = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $this->db);
                $dbp = new DBCollection("SELECT * FROM Player WHERE racename='Prêtre' AND id_City=" . $dbf->get("id"), $this->db);
                if ($room == BEDROOM && ($dbc->get("id_Player") == $this->curplayer->get("id") || ($dbf->get("captured") >= 4 && $dbp->eof())))
                    return true;
                else
                    return false;
                break;
            
            case 11:
                if ($room == CONSTRUCTION_ROOM || $room == TREASURY_ROOM || $room == DECISION_ROOM || $room == PALACE_BELL_TOWER_ROOM)
                    return true;
                else
                    return false;
                break;
            
            case 14:
                if ($room == HOSPITAL_ROOM) {
                    return true;
                } else {
                    if (($room == TELEPORT_ROOM) || ($room == TEMPLE_ROOF_ROOM) || $room == CHAPEL_ROOM || ($room == CHAPEL2_ROOM)) {
                        if (BasicActionFactory::canEnterTemple($this->curplayer, $this->curplayer->get("inbuilding"), $this->db))
                            return true;
                        else
                            return false;
                    } else
                        return false;
                }
                break;
            case 13:
                if ($room == JAIL_ROOM)
                    return true;
                else
                    return false;
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
                if ($room == TOWER_ROOM)
                    return true;
                else
                    return false;
                break;
            case 28:
                if ($room == EXCHANGE_ROOM || $room == REPARE_ROOM)
                    return true;
                else
                    return false;
                break;
        }
    }
    
    // ------------------------------------ DEBUT de l'AFFICHAGE
    public function prepareToString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $err = $this->err;
        $str = "";
        
        $curbuilding = new Building();
        $curbuilding->load($curplayer->get("inbuilding"), $db);
        $blevel = $curbuilding->get("level");
        $bsp = $curbuilding->get("sp");
        $idbb = $curbuilding->get("id_BasicBuilding");
        $id_Player = $curbuilding->get("id_Player");
        
        if (isset($_POST["room"]) && $this->checkPossibleRoom($_POST["room"], $idbb))
            $room = $_POST["room"];
        elseif ($curplayer->get("room") != 0)
            $room = $curplayer->get("room");
        else
            $room = "1";
        
        $dbcity = new DBCollection("SELECT name,type, captured FROM City WHERE id=" . $curbuilding->get("id_City"), $db, 0, 0);
        $cityname = "";
        if ($dbcity->count() > 0) {
            if ($dbcity->get("type") == "Ville")
                $cityname = "de la ville <span class=\"buildingname\">" . $dbcity->get("name") . "</span>";
            else {
                if ($dbcity->get("name") == "Bourg" or $dbcity->get("name") == "Village")
                    $cityname = "d'un " . $dbcity->get("name");
                else
                    $cityname = "de " . $dbcity->get("name");
            }
        }
        
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td> Vous êtes à l'intérieur d'un(e) " . $curbuilding->get("name") . " (id " . $curbuilding->get("id") . ") " . $cityname . ".</td>";
        
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        // Menu interne au bâtiment, dépend du type de bâtiment
        $str .= "<table class='maintable2 centerareawidth' style='background-image:url(../pics/conquest/menu.jpg);'>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbgtitle'>\n";
        
        if (! ($curplayer->get("state") == "prison") && ! ($curplayer->get("state") == "questionning") && ! $curplayer->get("recall") && ! $curplayer->get("levelUp")) {
            
            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Accueil'/>
    <br/><input type='image' src='../pics/conquest/test.png' name='room' value='" . ENTRANCE_ROOM . "'/></center> ";
            $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
            $str .= "</form></td>";
            
            // Peu élégant, à réfléchir.
            if (! ($curplayer->get("state") == "turtle")) {
                
                switch ($idbb) {
                    case 14: // TEMPLE
                        
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Hôpital du Temple'/><br/><input type='image' src='../pics/conquest/test.png' name='room' value='" .
                             HOSPITAL_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . HOSPITAL_ROOM . "'/>";
                        $str .= "</form></td>";
                        if (BasicActionFactory::CanEnterTemple($this->curplayer, $this->curplayer->get("inbuilding"), $this->db)) {
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Temple de la téléportation'/><br/><input type='image' src='../pics/conquest/test.png' name='room'value='" .
                                 TELEPORT_ROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . TELEPORT_ROOM . "'/>";
                            $str .= "</form></td>";
                            
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Chapelle'/><br/><input type='image' src='../pics/conquest/test.png' name='room'value='" .
                                 CHAPEL2_ROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . CHAPEL2_ROOM . "'/>";
                            $str .= "</form></td>";
                            
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Observatoire'/><br/><input type='image' src='../pics/conquest/test.png' name='room'value='" .
                                 TEMPLE_ROOF_ROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . TEMPLE_ROOF_ROOM . "'/>";
                            $str .= "</form></td>";
                        }
                        break;
                    
                    case 13: // PRISON
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Cellule'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . JAIL_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . JAIL_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 10: // MAISON
                        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $curbuilding->get("id_City"), $this->db);
                        $dbs = new DBCollection("SELECT * FROM Player WHERE id_City=" . $curbuilding->get("id_City") . " AND racename='Prêtre'", $db);
                        
                        if ($curbuilding->get("id_Player") == $id || ($dbc->get("captured") >= 4 and $dbs->eof())) {
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Chambre'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . BEDROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . BEDROOM . "'/>";
                            $str .= "</form></td>";
                        }
                        break;
                    
                    case 2: // BANQUE
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Guichet'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . BANK_OFFICE_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . BANK_OFFICE_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 4: // COMPTOIR DE COMMERCE
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Caravansérail'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . CARAVAN_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . CARAVAN_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Transport d objets'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . EXPRESS_TRANSPORT_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . EXPRESS_TRANSPORT_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Banque'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . BANK_OFFICE_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . BANK_OFFICE_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 5: // ECHOPPE
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Boutique'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . MAIN_SHOP_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . MAIN_SHOP_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Armurerie Standard'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . BASIC_SHOP_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . BASIC_SHOP_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle des Ventes'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . SALES_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . SALES_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle des Réparations'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . REPARE_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . REPARE_ROOM . "'/>";
                        
                        $str .= "</form></td>";
                        break;
                    
                    case 1: // AUBERGE
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Grand-Salle'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . HOSTEL_MAIN_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . HOSTEL_MAIN_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle des Annonces'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . HOSTEL_BOARDS_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . HOSTEL_BOARDS_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 6: // ECOLE DE COMBAT
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle de Classe'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . SCHOOL_ABILITY_CLASS_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . SCHOOL_ABILITY_CLASS_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 7: // ECOLE DE MAGIE
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle de Classe'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . SCHOOL_SPELL_CLASS_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . SCHOOL_SPELL_CLASS_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 8: // ECOLE DES METIERS
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle de Classe'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . SCHOOL_TALENT_CLASS_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . SCHOOL_TALENT_CLASS_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 9: // GUILDE DES ARTISANS
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Atelier'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . WORKSHOP_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . WORKSHOP_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle des Enchantements'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . ENCHANT_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . ENCHANT_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 11: // PALAIS DU GOUVERNEUR
                        if ($dbcity->count() > 0 && $dbcity->get("captured") != 6) {
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle de l`architecte'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . CONSTRUCTION_ROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . CONSTRUCTION_ROOM . "'/>";
                            $str .= "</form></td>";
                        }
                        if ($dbcity->count() > 0 && $dbcity->get("captured") != 6) {
                            
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Trésorerie'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . TREASURY_ROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . TREASURY_ROOM . "'/>";
                            $str .= "</form></td>";
                        }
                        if ($dbcity->count() > 0) {
                            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle du gouverneur'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . DECISION_ROOM . "'/></center> ";
                            $str .= "<input type='hidden' name='room' value='" . DECISION_ROOM . "'/>";
                            $str .= "</form></td>";
                        }
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Clocher'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . PALACE_BELL_TOWER_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . PALACE_BELL_TOWER_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    case 16: // REMPART et porte fermée
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Tour de garde'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . TOWER_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . TOWER_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    case 28: // Entrepôt
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Dépôt/Vente'/> 
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . EXCHANGE_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . EXCHANGE_ROOM . "'/>";
                        $str .= "</form></td>";
                        $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                        $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Salle des Réparations'/>
        <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . REPARE_ROOM . "'/></center> ";
                        $str .= "<input type='hidden' name='room' value='" . REPARE_ROOM . "'/>";
                        $str .= "</form></td>";
                        break;
                    
                    default:
                        
                        break;
                }
            }
            $str .= "<td><form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<center><input class='inputMenu3' type='submit' name='gotoroom' value='Écuries'/>
    <br/><input type='image' src='../pics/conquest/test.png' name='room'value='" . STABLE_ROOM . "'/></center> ";
            $str .= "<input type='hidden' name='room' value='" . STABLE_ROOM . "'/>";
            $str .= "</form></td>";
        }
        
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>\n";
        
        // Intérieur du bâtiment
        
        $str .= "<div class='insidebuildingleft' >";
        
        switch ($room) {
            case ENTRANCE_ROOM: // ------------- ACCUEIL, Commun à tous les bâtiments
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                
                require_once (HOMEPATH . '/conquest/inbuilding/cqentrance.inc.php');
                $tpobj = new CQEntrance($curbuilding, $this->body, $this->nacridan, $db);
                $str .= $tpobj->ToString();
                $this->minimap = $tpobj->minimap;
                $this->radius = $tpobj->radius;
                break;
            
            case STABLE_ROOM: // ------------- ECURIES, Commun à tous les bâtiments
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                $str .= "<table class='maintable insidebuildingleftwidth'>";
                $str .= "<tr><td colspan='6' class='mainbgtitle'><b>" . localize('M O N T U R E S') . "</b></td></tr>\n";
                
                $str .= "</table>\n";
                
                $str .= "<table class='maintable insidebuildingleftwidth'>";
                $str .= "<tr>\n";
                
                $str .= "<td class='mainbglabel' width='30' align='center'>" . localize('Niveau') . "</td>\n";
                $str .= "<td class='mainbglabel' width='175'>" . localize('Race') . "</td>\n";
                $str .= "<td class='mainbglabel'  >" . localize('Propriétaire') . "</td>\n";
                
                $str .= "</tr>\n";
                
                foreach ($this->horses as $pnj) {
                    $str .= "<tr class='mainbgbody'><td>" . $pnj["level"] . "</td> <td> " . $pnj["racename"] . " " . $pnj["state"] . "</td><td> " . $pnj["owner"] . " </td></tr>\n";
                }
                $str .= "</table>";
                
                $str .= "<table class='maintable insidebuildingleftwidth'>";
                // $str.="<tr><td colspan='6' class='mainbgtitle'><b>".localize('Les montures ne sont pas encore codées.')."</b></td></tr>\n";
                
                $str .= "</table>\n";
                
                break;
            
            // ----------------------- TEMPLE-----------------------------
            
            case HOSPITAL_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqhospital.inc.php');
                $tpobj = new CQHospital($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case TELEPORT_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqteleport.inc.php');
                $tpobj = new CQTeleport($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case CHAPEL2_ROOM:
                $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
                $str .= "<table class='maintable insidebuildingleftwidth'>";
                $str .= "<tr><td> La chapelle du temple est un lieu saint où vous serez protégé de tout danger.";
                $str .= " Malheureusement, en sortant de ce lieu saint, vous serez considérablement affaibli pour une petite période.";
                $str .= "</table><table class='maintable insidebuildingleftwidth'>";
                $str .= "<tr><td> Êtes-vous sur de vouloir entrer dans la chapelle ?";
                $str .= "<tr><td><input type='submit' name='gotoroom' value='Valider'/></tr><td/></table> ";
                $str .= "<input type='hidden' name='room' value='" . CHAPEL_ROOM . "'/>";
                break;
            
            case CHAPEL_ROOM:
                $date = gmdate('Y-m-d H:i:s');
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND id_StaticModifier_BM=19", $db);
                if ($dbc->count() == 0)
                    $dbi = new DBCollection(
                        "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (19," . $curplayer->get("id") . ",2, 1 , 'POSITIVE', '" .
                             addslashes("Protection Chapelle") . "' ,2,'" . $date . "')", $db, 0, 0, false);
                else
                    $dbc = new DBCollection("UPDATE BM SET life=2 WHERE id_Player=" . $curplayer->get("id") . " AND id_StaticModifier_BM=19", $db, 0, 0, false);
                
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                PlayerFactory::initBM($curplayer, $curplayer->getObj("Modifier"), $param, $db, 0);
                
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqchapel.inc.php');
                $tpobj = new CQChapel($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- MAGIC SCHOOL -----------------------------
            
            case SCHOOL_SPELL_CLASS_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqspellclassroom.inc.php');
                $tpobj = new CQSpellclassroom($curbuilding, $this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- ABILITY SCHOOL -----------------------------
            
            case SCHOOL_ABILITY_CLASS_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqabilityclassroom.inc.php');
                $tpobj = new CQAbilityclassroom($curbuilding, $this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- TALENT SCHOOL -----------------------------
            
            case SCHOOL_TALENT_CLASS_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqtalentclassroom.inc.php');
                $tpobj = new CQTalentclassroom($curbuilding, $this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- HOUSE -----------------------------
            
            case BEDROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqbedroom.inc.php');
                $tpobj = new CQBedroom($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- BANK -----------------------------
            
            case BANK_OFFICE_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqbankoffice.inc.php');
                $tpobj = new CQBankoffice($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- COMMERCIAL CENTER -----------------------------
            
            case CARAVAN_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqcaravan.inc.php');
                $tpobj = new CQCaravan($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case EXPRESS_TRANSPORT_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqexpresstransport.inc.php');
                $tpobj = new CQExpressTransport($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- CRAFTSMAN GUILD -----------------------------
            
            case WORKSHOP_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqworkshop.inc.php');
                $tpobj = new CQWorkshop($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case REPARE_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqrepareroom.inc.php');
                $tpobj = new CQRepareroom($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case EXCHANGE_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqexchangeroom.inc.php');
                $tpobj = new CQExchangeRoom($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case ENCHANT_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqenchantroom.inc.php');
                $tpobj = new CQEnchantroom($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- SHOP -----------------------------
            
            case MAIN_SHOP_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqshop.inc.php');
                $tpobj = new CQShop($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case BASIC_SHOP_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqbasicshop.inc.php');
                $tpobj = new CQBasicshop($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case SALES_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqsalesroom.inc.php');
                $tpobj = new CQSalesroom($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- HOSTEL -----------------------------
            
            case HOSTEL_MAIN_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqhostel.inc.php');
                $tpobj = new CQHostel($this->nacridan, $db);
                
                $str .= $tpobj->ToString();
                break;
            
            case HOSTEL_BOARDS_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqboards.inc.php');
                $tpobj = new CQBoards($this->nacridan, $db);
                
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- PRISON -----------------------------
            
            case JAIL_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqjail.inc.php');
                $tpobj = new CQJail($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            // ----------------------- PALAIS DU GOUVERNEUR -----------------------------
            
            case CONSTRUCTION_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqconstruction.inc.php');
                $tpobj = new CQConstruction($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case TREASURY_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqtreasury.inc.php');
                $tpobj = new CQTreasury($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case DECISION_ROOM:
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqdecisionroom.inc.php');
                $tpobj = new CQDecisionroom($this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
            
            case TEMPLE_ROOF_ROOM: // ----------------------- TEMPLE -----------------------------
            case PALACE_BELL_TOWER_ROOM: // ----------------------- PALAIS -----------------------------
            case TOWER_ROOM: // ----------------------- REMPART -----------------------------
                if ($room != $curplayer->get("room")) {
                    $curplayer->set("room", $room);
                    $curplayer->updateDBPartial("room", $this->db);
                }
                require_once (HOMEPATH . '/conquest/inbuilding/cqtower.inc.php');
                $tpobj = new CQTower($curbuilding, $this->body, $this->nacridan, $db);
                $str .= $tpobj->ToString();
                break;
        }
        $str .= "</div>";
        
        // ------------------------------------------ Liste perso/objet dans la pièce---------------------------------
        
        $this->build_pj();
        $this->build_pnj();
        $this->build_items();
        
        $str .= "<div class='insidebuildingright' >";
        
        $str .= "<table class='maintable insidebuildingrightwidth'><tr><td colspan='3' class='mainbgtitle'><b>" . localize('P E R S O N N A G E S') . "</b></td></tr>\n";
        $str .= "</table>";
        $str .= "<table class='maintable insidebuildingrightwidth'>";
        
        foreach ($this->pj as $pj) {
            if ($pj["id_Team"] != 0)
                $alliegance = ", " . $pj["alliegance"];
            else
                $alliegance = " ";
            
            $str .= "<tr class='mainbgbody'><td width=50px>Niv. " . $pj["level"] . "</td> <td> " . $pj["name"] . " (" . $pj["id"] . ") " . $pj["state"] . " " . $alliegance .
                 " </td></tr>\n";
        }
        $str .= "</table>";
        
        $str .= "<table class='maintable insidebuildingrightwidth'>";
        $str .= "<tr><td colspan='3' class='mainbgtitle'><b>" . localize('P N J') . "</b></td></tr>\n";
        $str .= "</table>";
        $str .= "<table class='maintable insidebuildingrightwidth'>";
        
        foreach ($this->pnj as $pnj) {
            $str .= "<tr class='mainbgbody'><td width=50px>Niv. " . $pnj["level"] . "</td> <td> " . $pnj["name"] . " " . $pnj["state"] . $pnj["owner"] . "  </td></tr>\n";
        }
        $str .= "</table>";
        
        $str .= "<table class='maintable insidebuildingrightwidth'>";
        $str .= "<tr><td colspan='6' class='mainbgtitle'><b>" . localize('O B J E T S') . "</b></td></tr>\n";
        
        $str .= "</table>\n";
        
        $str .= "<table class='maintable insidebuildingrightwidth'>";
        
        foreach ($this->items as $item) {
            $str .= "<tr class='mainbgbody'> <td> " . $item["name"] . " </td></tr>\n";
        }
        $str .= "</table>";
        $str .= "</div>";
        
        $this->result = $str;
    }

    function toString()
    {
        return $this->result;
    }
}
?>
