<?php

class CQMapLegend extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $str;

    public function CQMapLegend($nacridan, $db)
    {
        $this->curplayer = $nacridan->loadCurSessPlayer($db);
        
        $currmap = $this->curplayer->get("map");
        // Choix de la carte - une seule carte disponible au début.
        /*
         * $str="<table class='maintable bottomleftareawidth'>";
         *
         * $str.="<tr><td class='mainbgtitle'><input type=radio name='map' onClick='loadMap(\"map01\",".$currmap.")'>".localize("Contrées Centrales")."</td></tr>";
         * $str.="<tr><td class='mainbgtitle'><input type=radio name='map' onClick='loadMap(\"map02\",".$currmap.")'>".localize("Contrées Nordiques")."</td></tr>";
         * $str.="</table>";
         */
        
        // Options de la carte
        $str = "<table class='legend bottomareawidth'>";
        $str .= "<tr><td class='mainbgtitle'><input type=checkbox onClick='setStyleCity(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Village") . "</td>
	<td class='mainbgtitle'><input type=checkbox onClick='setStyleCityCaptured(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Villages contrôlés") . "</td>";
        
        if ($this->curplayer->get("id_Team") != 0)
            $str .= "<td class='mainbgtitle'><input type=checkbox onClick='setStyleTeam( \"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Les autres Membres de Votre Ordre") . "</td>";
        
        $str .= "</tr>";
        // $str.="<tr><td class='mainbgtitle'><input type=checkbox onClick='setStylePlayers(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>".localize("Démographie")."</td>";
        
        // if($this->curplayer->get("id_Team")!=0)
        // $str.="<td class='mainbgtitle'> </td>";
        $str .= "</tr>";
        
        $str .= "<tr><td class='mainbgtitle'><input type=checkbox onClick='setStyleLevelMonster(\"visibility\", this.checked==true ? \"visible\" : \"hidden\" )'>" . localize("Niveau des monstres") . "</td>";
        $str .= "<td class='mainbgtitle'><input type=checkbox onClick='setStyleMapTopo(\"visibility\", this.checked==true ? \"visible\" : \"hidden\" )'>" . localize("Carte topographique") . "</td>";
        if ($this->curplayer->get("id_Team") != 0)
            $str .= "<td class='mainbgtitle'> </td>";
        $str .= "</tr>";
        
        // Affichage des royaumes
        $str .= "<tr><td class='mainbgtitle'><input type=checkbox onClick='setStyleKingdom(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Royaumes") . "</td>";
        $str .= "<tr><td class='mainbgtitle'><input type=checkbox onClick='setStyleRoad(\"visibility\", this.checked==true ? \"visible\" : \"hidden\")'>" . localize("Les routes") . "</td>";
        
        $str .= "</table>";
        
        $this->str = $str;
    }

    public function toString()
    {
        return $this->str;
    }
}
?>
