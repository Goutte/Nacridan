<?php

/**
 *Affiche le contenu du parchemin
 *
 * Définit une fonction par type de mission
 *
 *@author Nacridan
 *@version 1.1
 *@package NacridanV1
 *@subpackage Conquest
 */
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/class/MissionReward.inc.php");
require_once (HOMEPATH . "/class/MissionCondition.inc.php");
require_once (HOMEPATH . "/class/Quest.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];
Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$str = "<table><tr><td valign='top'>\n";
$str .= "<table class='maintable' width='620px'>\n";
$str .= "<tr>\n";
$str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D E S C R I P T I O N') . "</b>\n";
$str .= "</td>\n";
$str .= "</tr>\n";

if (isset($_GET["id"])) {
    $id = quote_smart($_GET["id"]);
    $dbBT = new DBCollection("SELECT * FROM BasicMagicSpell WHERE id=" . $id, $db);
    $str = "<table class='maintable mainbgtitle' width='640px'>\n";
    $str .= "<tr><td>\n";
    $str .= "<b><h1>" . localize($dbBT->get('name')) . "</h1></b></td>\n";
    $str .= "</tr>\n";
    $str .= "</table>\n";
    $str .= "<table class='maintable mainbgtitle' width='640px'><tr>";
    $str .= "<td class='mainbglabel' width='640px'>";
    
    switch ($id) {
        
        case 1:
            $str .= <<<EOF
TOUCHER BRULANT est un sortilège de base d'attaque au contact. Il permet de blesser une créature à l'aide de votre magie.<br/><br/>

Si vous ne portez pas d'arme magique votre attaque est réduite à un jet de Dextérité, sinon elle est classique. Si vous surpassez la défense de votre adversaire, alors vous lui infligerez des dégâts basés sur votre Maitrise de la Magie.<br/><br/>

Après apprentissage de ce sortilège, vous la maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Sorts de Base'>Les sorts de base.</a></br/>
EOF;
            break;
        
        case 2:
            $str .= <<<EOF
ARMURE D'ATHLAN est un sortilège de base de protection. Il permet de créer une armure magique sur un personnage au contact du magicien. Celle-ci va s'ajouter à la propre armure du personnage.<br/><br/>
Le sort fera effet pendant 2 DLA de la personne ciblée et est basé sur votre Maitrise de la Magie ainsi que votre Force. Plus vos jets dans ces caractéristiques seront élevés et plus l'armure magique créée sera puissante.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Sorts de Base'>Les sorts de base.</a></br/>
EOF;
            break;
        
        case 3:
            $str .= <<<EOF
LARMES DE VIE est un sortilège de base de soin. Il est le premier sortilège de guérison auquel vous avez accès. Il permet de redonner des points de vie à un personnage au contact.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus celle-ci sera grande et plus vos soins seront efficaces.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 7 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Sorts de Base'>Les sorts de base.</a></br/>
EOF;
            break;
        
        case 4:
            $str .= <<<EOF
SOUFFLE D'ATHLAN est un sortilège de niveau aspirant de l'école de guérison. Il permet d'effacer toutes les malédictions et les maux qui accablent un personnage.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus celle-ci sera grande et plus vous serez en mesure de dissiper des malédictions puissantes.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Guérison'>L'Ecole de Guérison.</a></br/>
EOF;
            break;
        
        case 5:
            $str .= <<<EOF
CHARME DE VITALITE est un sortilège de niveau aspirant de l'école de guérison. Il permet au magicien qui l'utilise de créer un enchantement sur un personnage au contact ou sur lui-même qui lui redonnera des points de vie à chaque nouvelle DLA.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus celle-ci sera grande et plus l'enchantement créé sera puissant et la régénération à chaque DLA importante.<br/><br/>

Les effets du sort dureront 3 DLA de la personne ciblée.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Guérison'>L'Ecole de Guérison.</a></br/>
EOF;
            break;
        
        case 6:
            $str .= <<<EOF
BASSIN DIVIN est un sortilège de niveau aspirant de l'école de guérison. Il permet au magicien qui le possède de créer un bassin occupant 1 case de terrain jusqu'alors vide.<br/>
Une fois le bassin créé, n'importe quel aventurier peut venir y boire pour regagner des points de vie en échange de 2 points d'action. La contenance du bassin permet à 5 aventuriers de venir y boire.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus celle-ci sera grande et plus les aventuriers qui boiront l'eau du bassin seront soignés efficacement.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 9 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Guérison'>L'Ecole de Guérison.</a></br/>
EOF;
            break;
        
        case 7:
            $str .= <<<EOF
BENEDICTION est un sortilège de niveau aspirant de l'école de protection. Tout personnage touché par cet enchantement sera immunisé contre les malédictions et les maux pendant 3 DLA.<br/><br/>

Ce sortilège est basé sur votre Force et votre Maitrise de la Magie. Plus ceux-ci seront élevés et plus la bénédiction sera à même de contrer de puissante malédiction.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 6 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Protection'>L'Ecole de Protection.</a></br/>
EOF;
            break;
        
        case 8:
            $str .= <<<EOF
REVEIL DE LA TERRE est un sortilège de niveau aspirant de l'école de protection. Il permet au magicien de créer de 1 à 4 murs sortant de terre. Les murs doivent être adjacents, se trouver à un maximum de 2 cases du lanceur de sort et être érigés sur des cases libres.<br/><br/> 

Ce sortilège est basé sur votre Force et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le mur sera solide.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Protection'>L'Ecole de Protection.</a></br/>
EOF;
            break;
        
        case 9:
            $str .= <<<EOF
BARRIERE ENFLAMMEE est un sortilège de niveau aspirant de l'école de protection. Il permet au magicien de créer une barrière de feu autour du personnage ciblé occasionnant des dégâts à toute créature réalisant une attaque au corps à corps sur ce personnage, pendant 2 DLA. <br/><br/>


Ce sortilège est basé sur votre Force et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus la barrière occasionnera des dégâts importants.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>


Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Protection'>L'Ecole de Protection.</a></br/>
EOF;
            break;
        
        case 10:
            $str .= <<<EOF
BOUCLIER MAGIQUE est un sortilège de niveau 2 de l'école de Protection. Il permet au magicien de greffer un bouclier de magie sur le bras de la cible, lui octroyant un bonus en défense pendant 2 DLA.<br/><br/>

Ce sortilège est basé sur votre Force et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le bonus en défense sera important.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Protection'>L'Ecole de Protection.</a></br/>
EOF;
            break;
        
        case 11:
            $str .= <<<EOF
BOULE DE FEU est un sortilège de niveau aspirant de l'école de Destruction. Il permet au magicien d'envoyer une Boule de Feu à une portée de 3 cases qui explosera sur la cible occasionnant des points de dégats.<br/><br/>

Ce sortilège est basé sur votre Dextérité et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus les dégats seront importants.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Destruction'>L'Ecole de Destruction.</a></br/>
EOF;
            break;
        
        case 12:
            $str .= <<<EOF
POING DU DEMON est un sortilège de niveau aspirant de l'école de Destruction. Il permet au magicien de prendre pour cible un équipement d'un personnage au contact autre que son arme et en reduit sa durabilité d'un nombre de points fonction du niveau de sort atteint. Suivant la puissance du sort et la durabilité de la pièce visée, plusieurs équipements peuvent être touchés.<br/><br/>

Ce sortilège est basé sur votre Dextérité et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 7 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Destruction'>L'Ecole de Destruction.</a></br/>
EOF;
            break;
        
        case 13:
            $str .= <<<EOF
BRASIER est un sortilège de niveau 2 de l'école de Destruction. Ce sort crée un brasier enflammant une zone de rayon 1 et centrée sur une case à 2 de distance maximum du magicien. Toutes les créatures (y compris le lanceur) à l'intérieur de la zone subissent une attaque de feu. Au centre de la zone, les dégâts infligés sont plus important.<br/>

Ce sortilège est basé sur votre Dextérité et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 10 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Destruction'>L'Ecole de Destruction.</a></br/>
EOF;
            break;
        
        case 14:
            $str .= <<<EOF
SANG DE LAVE est un sortilège de niveau 2 de l'école de Destruction. Il permet au magicien de faire bouillir le sang de son adversaire lui infligeant des dégâts ignorant l'armure pendant 5 DLA. <br/>
Ce sortilège est basé sur une opposition magique pure et a une portée de 2 cases. Plus vous dominerez votre adversaire en Maitrise de la magie et plus votre sort sera puissant. <br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Destruction'>L'Ecole de Destruction.</a></br/>
EOF;
            break;
        
        case 15:
            $str .= <<<EOF
SOUFFLE DE NEGATION est un sortilège de niveau 2 de l'école de Protection. Il permet au magicien de propulser le corps d'un membre de son Groupe de Chasse hors de la réalité. La cible devient agonisante mais en contrepartie demeure invisible pendant 3 DLA. Toute autre action de sa part qu'un déplacement la rendra de nouveau visible.<br/><br/>

Ce sortilège est basé sur votre Force et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Protection'>L'Ecole de Protection.</a></br/>

EOF;
            break;
        
        case 16:
            $str .= <<<EOF
PLUIE SACREE est un sortilège de niveau 2 de l'école de Guérison. Il permet au magicien de soigner tous les personnages touchés par la pluie dans une zone de rayon 1 et centrée sur une case à 2 de distance maximum du lanceur de sort.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus elle sera élevée et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 9 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Guérison'>L'Ecole de Guérison.</a></br/>
EOF;
            break;
        
        case 17:
            $str .= <<<EOF
RECHARGE DU BASSIN est un sortilège de niveau 2 de l'école de Guérison. Il permet au magicien de remplir un Bassin Divin existant pour de nouveau en permettre 5 utilisations.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus elle sera élevée et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Guérison'>L'Ecole de Guérison.</a></br/>
EOF;
            break;
        
        case 18:
            $str .= <<<EOF
SOLEIL DE GUERISON est un sortilège de niveau 2 de l'école de Guérison. Pendant 3 DLA, tant que ce soleil reste actif, tout membre du Groupe de Chasse subissant des dégâts est automatiquement soigné d'une partie de la perte subie.<br/><br/>

Ce sortilège est basé uniquement sur votre Maitrise de la Magie. Plus elle sera élevée et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 10 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Guérison'>L'Ecole de Guérison.</a></br/>
EOF;
            break;
        
        case 19:
            $str .= <<<EOF
BULLE DE VIE est un sortilège de niveau 2 de l'école de Protection. Il permet au magicien de placer Une Bulle de Vie autour de la cible absorbant les prochains dégâts qui lui seront infligés. La Bulle n'est pas efficace contre les pertes de vie venant de maux intérieurs.<br/><br/>

Ce sortilège est basé sur votre Force et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Protection'>L'Ecole de Protection.</a></br/>
EOF;
            break;
        
        case 20:
            $str .= <<<EOF
PILIERS INFERNAUX est un sortilège de niveau 2 de l'école de Destruction. Il permet au magicien de faire apparaitre 3 piliers de feu qui iront automatiquement frapper le batiment au contact du magicien ainsi que deux autres batiments alignés avec le premier. <br/>

Ce sortilège est basé sur votre Dextérité et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 9 points d'action.<br/><br/>

Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Destruction'>L'Ecole de Destruction.</a></br/>

EOF;
            break;
        
        case 21:
            $str .= <<<EOF
MALEDICTION D'ARCXOS est un sortilège de niveau aspirant de l'école d'Altération. Il permet au magicien de jeter une malédiction pour réduire, au choix, la Dextérité, la Force ou la Vitesse de sa cible pendant 2 DLA.<br/><br/>

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus la malédiction sera puissante.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Altération'>L'Ecole d'Altération.</a></br/>
EOF;
            break;
        
        case 22:
            $str .= <<<EOF
AILES DE LA COLERE est un sortilège de base. Il permet au magicien d'invoquer la fureur des vents pour soutenir le bras de sa cible pendant 2 DLA en lui octroyant un bonus en Attaque ou en Dégâts, au choix du magicien.<br/><br/> 

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus les bonus octroyés seront importants.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Altération'>L'Ecole d'Altération.</a></br/>
EOF;
            break;
        
        case 23:
            $str .= <<<EOF
TELEPORTATION est un sortilège de niveau 2 de l'école d'Altération. Il permet au magicien de se déplacer instantanément sur la case de son choix. Si la case ciblée se trouve hors de portée pour le niveau du sort, le sort échoue. Si la case ciblée se trouve occupée, le magicien meurt.<br/><br/>

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus la portée du sort sera grande.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 8 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Altération'>L'Ecole d'Altération.</a></br/>
EOF;
            break;
        
        case 24:
            $str .= <<<EOF
TOUCHER DE LUMIERE est un sortilège de niveau 2 de l'école d'Altération. Il permet au magicien de redonner des points d'action à la cible dans la limite maximale des PAs. En contrepartie, le magicien subira un malus en MM.<br/><br/>

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Altération'>L'Ecole d'Altération.</a></br/>
EOF;
            break;
        
        case 25:
            $str .= <<<EOF
PROJECTION DE L'AME est un sortilège de niveau aspirant de l'école d'Altération. Il permet au magicien de projeter une partie de son âme hors de son corps lui permettant de voir une zone éloignée. Si la case ciblée se trouve hors de portée pour le niveau du sort, le sort échoue. Le magicien ne peut rien faire tant que dure le sort et subit un malus important.<br/><br/>

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 5 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Altération'>L'Ecole d'Altération.</a></br/>
EOF;
            break;
        
        case 26:
            $str .= <<<EOF
RAPPEL est un sortilège de niveau 2 de l'école d'Altération. Il permet au magicien de rappeler auprès de lui un autre personnage lors de sa prochaine DLA. Si la case ciblée se trouve hors de portée pour le niveau du sort, le sort échoue. La cible aura le choix d'accepter ou refuser le Rappel à sa prochaine DLA. Si elle l'accepte cela lui coutera 12 PA.<br/><br/>

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus la portée du sort sera grande.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 12 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Altération'>L'Ecole d'Altération.</a></br/>
EOF;
            break;
        
        case 27:
            $str .= <<<EOF
APPEL DU FEU est un sortilège de niveau aspirant de l'école de Destruction. Il permet au magicien d'invoquer un Golem de Feu sous son contrôle. Le Golem lance des boules de feu et peut se déplacer mais doit rester dans la vue du magicien. Celui-ci peut révoquer le Golem à tout moment et ne peut pas en invoquer un deuxième. Tant que le Golem reste actif, le magicien subit un malus de -10% en Maitrise de la Magie.

Ce sortilège est basé sur votre Dextérité et votre Maitrise de la Magie. Plus ceux-là seront élevés et plus le Golem sera puissant.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 10 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Destruction'>L'Ecole de Destruction.</a></br/>
EOF;
            break;
        
        case 28:
            $str .= <<<EOF
FORCE D'AETHER est un sortilège de niveau aspirant de l'école d'Altération qui permet au magicien de déplacer une créature d'une case de son choix.<br/><br/>

Ce sortilège est basé sur votre Vitesse et votre Maitrise de la Magie. Plus celles-ci seront élevées et plus le sort sera efficace.<br/><br/>

Après apprentissage de ce sortilège, vous le maitriserez à 50%. Chaque utilisation réussie vous coûtera 4 points d'action.<br/><br/>
Règles: <a href='../i18n/rules/fr/rules.php?page=step8#Sorts de Base'>Les sorts de base.</a></br/>
EOF;
            break;
        
        default:
            $str . " En cours";
            break;
    }
    $str .= "</td></tr>\n";
    $MAIN_BODY->add($str);
    $MAIN_PAGE->render();
}
 
