<?php
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/MinimapCase.inc.php");

class CQViewpanel extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $minimap;

    public $move2Daction;

    public $radius;

    public function CQViewpanel($nacridan, $minimap, $move2Daction, $radius, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->minimap = $minimap;
        $this->move2Daction = $move2Daction;
        $this->radius = $radius;
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $radius = $this->radius;
        $db = $this->db;
        
        $str = "";
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        $timeToMove = $curplayer->getTimeToMove($db);
        
        for ($i = 0; $i <= ($radius * 2); $i ++)
            for ($j = 0; $j <= ($radius * 2); $j ++) {
                $curX = $xp - $radius + $j;
                $curY = $yp - $radius + $i;
                
                if (distHexa($curX, $curY, $xp, $yp) <= $radius && $curX >= 0 && $curX < MAPWIDTH && $curY >= 0 && $curY < MAPHEIGHT) {
                    $case = $this->minimap[$i][$j];
                    $str .= $case->to_panel($this, $db, $timeToMove);
                }
            }
        
        if (! ($this->move2Daction == ""))
            $str .= $this->move2Daction->toString();
        
        return $str;
    }
}
  
