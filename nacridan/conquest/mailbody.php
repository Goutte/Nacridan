<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/class/Detail.inc.php");
require_once (HOMEPATH . "/class/HActionMsg.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 0);

$lang = $auth->auth['lang'];
Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = $nacridan->loadCurSessPlayer($db);
$prefix = "";
$players = $nacridan->loadSessPlayers();

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$idmsg = quote_smart($_GET["id"]);

$dbm = new DBCollection("SELECT body,id_Player,recipient,recipientid,title,date,name, racename FROM MailBody LEFT JOIN Player AS P1 ON id_Player=P1.id WHERE MailBody.id=" . $idmsg . " ORDER BY date DESC", $db);
$mode = 0;

if (isset($players[$dbm->get("id_Player")])) {
    $mode = 1;
}

$idArr = explode(",", $dbm->get("recipientid"));

foreach ($idArr as $id) {
    if (isset($players[$id]))
        $mode = 1;
}
$str = "<table><tr><td style='vertical-align: top'>";

if ($mode) {
    $time = gmstrtotime($dbm->get("date"));
    $date = date("Y-m-d H:i:s", $time);
    
    $str .= "<div class='maintable centerareawidth mainbgtitle'>";
    $str .= "<b><h1>" . localize('M E S S A G E') . "</h1></b>";
    $str .= "</div>";
    $str .= "<table class='maintable centerareawidth'>";
    $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('Destinataire :') . "</b></td><td class='mainbgtitle'>" . str_replace(",", ", ", $dbm->get("recipient")) . "</td></tr>";
    
    if ($dbm->get("name") == "")
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('Expéditeur :') . "</b></td><td class='mainbgtitle'>" . $dbm->get("racename") . "</td></tr>";
    else
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('Expéditeur :') . "</b></td><td class='mainbgtitle'>" . $dbm->get("name") . "</td></tr>";
    
    $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('Date :') . "</b></td><td class='mainbgtitle'>" . $date . "</td></tr>";
    $str .= "</table>";
    $str .= "<br/>";
    $str .= "<table class='maintable centerareawidth''>";
    $str .= "<tr><td class='mainbglabel' width='110px'><b><h3>" . localize('Titre :') . "</h3></b></td><td class='mainbgbody'><h3>" . $dbm->get("title") . "</h3></td></tr>";
    $str .= "</table>";
    $str .= "<div class='histomsg'>";
    $str .= "<br/>&nbsp;&nbsp;&nbsp;" . nl2br(bbcode($dbm->get("body"), "histomsglink")) . "<br/>&nbsp;";
    $str .= "</div>";
    
    $exp = $dbm->get("name");
    $arr = explode(",", $dbm->get("recipient"));
    $curname = $players[$nacridan->loadSessPlayerID($db, "cq_playerid")];
    $expAll = "";
    foreach ($arr as $val) {
        if ($val != $curname)
            $expAll .= $val . ",";
    }
    
    $expAll .= $exp;
    // &mail=1
    // http://execbase.no-ip.org/nacridanNew/conquest/conquest.php?center=compose&to=Elric+de+Melnibon%C3%A9&re=Re%3A+Hello+...
    
    $str .= "<table><tr><td><input id='Reply' type='button' name='Reply' value='" . localize("Répondre") . "' onClick=\"return openOpener('../conquest/conquest.php?center=compose&to=" . urlencode($exp) . "&re=Re:%20" . urlencode($dbm->get("title")) . "')\" /></td>";
    $str .= "<td><input id='ReplyToAll' type='button' name='ReplyToAll' value='" . localize("Répondre à tous") . "' onClick=\"return openOpener('../conquest/conquest.php?center=compose&to=" . urlencode($expAll) . "&re=Re:%20" . urlencode($dbm->get("title")) . "')\" /></td>";
    // $str.="<td><input id='Del' type='button' name='Del' value='".localize("Effacer")."' onClick=\"openOpener('../conquest/conquest.php?center=mail&mail=".$_GET["mail"]."&delete=".$_GET["init"]."');window.close();\" /></td>";
    $str .= "</tr></table>";
    
    $dbm = new DBCollection("UPDATE Mail SET new=0 where id_Player\$receiver=" . $sess->get("cq_playerid") . " AND id_MailBody=" . $idmsg, $db, 0, 0, false);
    $dbmtd = new DBCollection("UPDATE MailTrade SET new=0 where id_Player\$receiver=" . $sess->get("cq_playerid") . " AND id_MailBody=" . $idmsg, $db, 0, 0, false);
    $dbmalrt = new DBCollection("UPDATE MailAlert SET new=0 where id_Player\$receiver=" . $sess->get("cq_playerid") . " AND id_MailBody=" . $idmsg, $db, 0, 0, false);
} else {}

$MAIN_BODY->add($str);

$MAIN_BODY->add("</td><td style='vertical-align: top'>");

/*
 * if($curplayer->get("advert")==0)
 * if(false) //if(mt_rand(0,1)==0)
 * {
 * $MAIN_BODY->add("<script language=\"javascript\" src=\"http://media.fastclick.net/w/get.media?sid=38368&m=3&tp=7&d=j&t=n\"></script>
 * <noscript><a href=\"http://media.fastclick.net/w/click.here?sid=38368&m=3&c=1\" target=\"_blank\">
 * <img src=\"http://media.fastclick.net/w/get.media?sid=38368&m=3&tp=7&d=s&c=1\"
 * width=160 height=600 border=1></a></noscript>");
 * }
 * else
 * {
 *
 * $MAIN_BODY->add("
 * <script type=\"text/javascript\"><!--
 * google_ad_client = \"pub-3725586809828932\";
 * google_ad_width = 160;
 * google_ad_height = 600;
 * google_ad_format = \"160x600_as\";
 * google_ad_type = \"text_image\";
 * //2007-07-23: InGamePopUp-Right-Skyscraper
 * google_ad_channel = \"9411983126\";".getGoogleAdsPalette()."google_ui_features = \"rc:6\";
 *
 * //-->
 * </script>
 * <script type=\"text/javascript\"
 * src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">
 * </script>");
 *
 *
 * }
 */

$MAIN_BODY->add("</td></tr></table>");

$google = "<script src='http://www.google-analytics.com/urchin.js' type='text/javascript'>
</script>
<script type='text/javascript'>
_uacct = 'UA-1166023-1';
urchinTracker();
</script>";

$MAIN_BODY->add($google);
$MAIN_BODY->add("<script src='" . CONFIG_HOST . "/javascript/astrack.js' type='text/javascript'></script>\n");
$MAIN_PAGE->render();

?>

