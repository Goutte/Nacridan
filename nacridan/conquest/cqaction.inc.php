<?php
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
require_once (HOMEPATH . "/factory/TalentFactory.inc.php");
require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");
require_once (HOMEPATH . "/factory/InteractionChecking.inc.php");
require_once (HOMEPATH . "/conquest/actionmsg.php");
require_once (HOMEPATH . "/class/Event.inc.php");

class CQAction extends HTMLObject
{

    public $actionEvt;

    public $nacridan;

    public $curplayer;

    public $db;

    public $str;

    public function CQAction($nacridan, $action, $db, $widthmsg = "centerareawidthmsg")
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->actionEvt = $action;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($this->db);
        
        $curplayer = $this->curplayer;
        $actionEvt = $this->actionEvt;
        $sess = $this->nacridan->sess;
        $actionEvt = $this->actionEvt;
        
        if ($actionEvt != null) {
            $showATB = 0;
            if ($actionEvt->getType() == ATBON) {
                $players = $this->nacridan->loadSessPlayers();
                $i = 0;
                foreach ($players as $key => $val) {
                    if (isset($_POST["check" . $i])) {
                        $var = "DLA" . quote_smart($_POST["check" . $i]);
                        $sess->set($var, "on");
                        if ($_POST["check" . $i] == $curplayer->get("id"))
                            $showATB = 1;
                    }
                    $i ++;
                }
                
                if ($showATB == 0) {
                    require_once ('../conquest/cqinfo.inc.php');
                    $obj = new CQInfo($this->nacridan, $this->db);
                    $str = $obj->toString();
                    return $str;
                } else
                    $actionEvt->setType(NEWATB);
            }
            $str = "<div class='content'>\n";
            $str .= "<div id='cqaction' class='mainmsg " . $widthmsg . "'>\n";
            if ($sess->has("DLA" . $sess->get("cq_playerid"))) {
                if (InteractionChecking::allowedToAct($curplayer, $actionEvt->getType(), $db)) {
                    $db = $this->db;
                    $error = 0;
                    $paramability = array();
                    $malus = 0;
                    switch ($actionEvt->getType()) {
                        
                        /* ************************************************************* NOUVELLE DLA ******************************************************** */
                        case NEWATB:
                            $msgTurtle = "";
                            $obj = new NewATBMsg();
                            $dbeTurtle = new DBCollection("SELECT id FROM Player WHERE id_owner=" . $curplayer->get("id") . " and id_BasicRace=263", $db);
                            if (! $dbeTurtle->eof()) {
                                $paramTurtle = array();
                                $objPlayer = new Player();
                                $objPlayer->externDBObj("Modifier");
                                $objPlayer->load($dbeTurtle->get("id"), $db);
                                $errorTurtle = PlayerFactory::newATB($objPlayer, $paramTurtle, $db);
                                $msgTurtle .= "<BR/>" . $obj->msgBuilder($errorTurtle, $paramTurtle);
                                // mise a jour du hidden et du state au cas ou la tortue meure à l'activation de sa dla
                                $curplayer->reloadPartialAttribute($curplayer->get("id"), "state", $db);
                                $curplayer->reloadPartialAttribute($curplayer->get("id"), "hidden", $db);
                                unset($paramTurtle);
                            }
                            
                            $error = PlayerFactory::newATB($curplayer, $param, $db);
                            $msg = $obj->msgBuilder($error, $param);
                            $msg .= $msgTurtle;
                            $str .= $msg;
                            
                            $mailState = $this->getMailState();
                            $str .= $mailState;
                            if ($curplayer->get("id_Team") != 0 && $curplayer->getSub("Team", "id_Player") == $curplayer->get("id")) {
                                $asker = $this->getAskJoin();
                                $str .= $asker;
                            }
                            
                            break;
                        
                        /* ************************************************************* LEVEL UP ******************************************************** */
                        case LEVEL_UP:
                            $error = PlayerFactory::levelUp($curplayer, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new LevelUpMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        /* *********************************************************** SONDAGE *********************************************************** */
                        case ANSWER_TO_POLL:
                            $param = $actionEvt->m_parameters;
                            $error = BasicActionFactory::answerToPoll($curplayer->get("id_Member"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new AnswerToPollMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        /* ****************************************************** MENU ACTION ******************************************************** */
                        case MOVE2D:
                        case MOVE:
                            if ($actionEvt->getParameter("XYNEW") == null) {
                                $error = 1;
                                $str .= localize("Vous devez choisir une case");
                            } else {
                                list ($xnew, $ynew) = explode(",", $actionEvt->getParameter("XYNEW"), 2);
                                $error = BasicActionFactory::move($curplayer, $xnew, $ynew, $param, $db, $actionEvt->getParameter("MOVE_TYPE"));
                                $obj = new MoveMsg();
                                $msg = $obj->msgBuilder($error, $param);
                                $str .= $msg;
                                $actionEvt->setType(MOVE);
                            }
                            break;
                        
                        case ENTER_BUILDING:
                            $error = BasicActionFactory::enterBuilding($curplayer, $actionEvt->getParameter("IDBUILDING"), $param, $db);
                            $obj = new EnterBuildingMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case LEAVE_BUILDING:
                            if ($actionEvt->getParameter("XYNEW") == null) {
                                $error = 1;
                                $str .= localize("Vous devez choisir une case");
                            } else {
                                list ($xnew, $ynew) = explode(",", $actionEvt->getParameter("XYNEW"), 2);
                                $error = BasicActionFactory::leaveBuilding($curplayer, $xnew, $ynew, $param, $db);
                                $obj = new MoveMsg();
                                $msg = $obj->msgBuilder($error, $param);
                                $str .= $msg;
                            }
                            break;
                        
                        case HUSTLE:
                            $error = InteractionChecking::checkingHustle($curplayer, $actionEvt->getParameter("TARGET_ID"), $param, $db);
                            $obj = new HustleMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case DRINK_SPRING:
                            $error = BasicActionFactory::drinkSpring($curplayer, $actionEvt->getParameter("IDBUILDING"), $param, $db);
                            $obj = new DrinkSpringMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CONCENTRATION:
                            $error = BasicActionFactory::Concentration($curplayer, $param, $db);
                            $obj = new ConcentrationMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case ATTACK:
                            $error = InteractionChecking::checkingAttack($curplayer, $actionEvt->getParameter("TARGET_ID"), $param, $db);
                            $param["ERROR"] = $error;
                            
                            $obj = new AttackMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case ATTACK_BUILDING:
                            $error = InteractionChecking::checkingAttackBuilding($curplayer, $actionEvt->getParameter("TARGET_ID"), $actionEvt->getParameter("ATTACK_TYPE"), $param, 
                                $db);
                            $param["ERROR"] = $error;
                            $obj = new AttackBuildingMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case ARCHERY:
                            $error = InteractionChecking::checkingArchery($curplayer, $actionEvt->getParameter("TARGET_ID"), $actionEvt->getParameter("ARROW_ID"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new ArcheryMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GIVE_MONEY:
                            $error = InteractionChecking::checkingGiveMoney($curplayer, $actionEvt->getParameter("TARGET_ID"), $actionEvt->getParameter("MONEY"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new GiveMoneyMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case RP_ACTION:
                            $error = BasicActionFactory::rpAction($curplayer, $actionEvt->getParameter("ACTION_ID"), $param, $db);
                            $obj = new RPActionMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case ATBSHIFT:
                            $time = gmstrtotime($curplayer->get("nextatb"));
                            if ($_POST["MIN"] < 60 && $_POST["HOUR"] < 12 && $curplayer->get("id_Mission") == 0) {
                                $time += abs(quote_smart($_POST["MIN"])) * 60 + abs(quote_smart($_POST["HOUR"])) * 3600;
                                $date = gmdate("Y-m-d H:i:s", $time);
                                $curplayer->set("nextatb", $date);
                                $curplayer->updateDB($db);
                                $error = 0;
                                $str .= localize("Votre DLA a bien été décalée !");
                                if (isset($_POST["SHIFT_ALL_CHARACTERS"])) {
                                    $dbe = new DBCollection("UPDATE Player SET nextatb='" . $date . "' WHERE id_Member=" . $curplayer->get("id_Member"), $db, 0, 0, false);
                                    $str .= " La DLA de tous vos personnages a été décalée à la même heure.";
                                }
                            } else {
                                $error = 1;
                                $str .= localize("Erreur, la DLA n'a pas pu être décalée !");
                            }
                            break;
                        
                        /* ****************************************************** ACTION CONTEXTUEL ******************************************************** */
                        case APPEAR:
                            $error = BasicActionFactory::appear($curplayer, $param, $db);
                            $obj = new AppearMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case DESTROY:
                            $error = BasicActionFactory::destroy($curplayer, $actionEvt->getParameter("TARGET_ID"), $param, $db);
                            $obj = new DestroyMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case EXIT_JAIL:
                            $error = BasicActionFactory::exitJail($curplayer, $param, $db);
                            $obj = new ExitJailMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GRAFFITI:
                            $error = BasicActionFactory::graff($curplayer, $param, $actionEvt->getParameter("GRAF_MSG"), $actionEvt->getParameter("GRAF_FINI"), $db);
                            $obj = new PrisonGraffitiMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CONTINUE_GRAFFITI:
                            $error = BasicActionFactory::continueGraff($curplayer, $param, $actionEvt->getParameter("GRAF_MSG"), $actionEvt->getParameter("GRAFF_ID"), 
                                $actionEvt->getParameter("GRAF_FINI"), $db);
                            $obj = new ContinueGraffitiMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CONSULT_GRAFFITI:
                            $error = BasicActionFactory::consultGraff($curplayer, $param, $db);
                            $obj = new ConsultGraffitiMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case RECALL:
                            $error = BasicActionFactory::recall($curplayer, $actionEvt->getParameter("CHOICE"), $param, $db);
                            $obj = new RecallMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case ARREST:
                            $error = BasicActionFactory::arrest($curplayer, $actionEvt->getParameter("CHOICE"), $param, $db);
                            $obj = new ArrestMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case FREE:
                            
                            $error = BasicActionFactory::free($curplayer, $param, $db);
                            $obj = new FreeMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case RIDE_TURTLE:
                            $error = BasicActionFactory::rideTurtle($curplayer, $actionEvt->getParameter("ID_TARGET"), $param, $db);
                            $str .= localize("Vous êtes désormais à l'intérieur de la tortue et vous pouvez la faire avancer");
                            break;
                        
                        case TURTLE_UNMOUNT:
                            if ($actionEvt->getParameter("XYNEW") == null) {
                                $error = 1;
                                $str .= localize("Vous devez choisir une case");
                            } else {
                                list ($xnew, $ynew) = explode(",", $actionEvt->getParameter("XYNEW"), 2);
                                $error = BasicActionFactory::unmountTurtle($curplayer, $actionEvt->getParameter("ID_TARGET"), $xnew, $ynew, $param, $db);
                                $str .= localize("Vous êtes descendu de votre tortue");
                            }
                            break;
                        
                        /* ****************************************************** MENU OBJET ************************************************************* */
                        case PICK_UP_OBJECT:
                            if ($actionEvt->getParameter("INV_ID"))
                                $inv = $actionEvt->getParameter("INV_ID");
                            else
                                $inv = 1;
                            $error = BasicActionFactory::pick($curplayer, $actionEvt->getParameter("OBJECT_ID"), $inv, $param, $db);
                            $obj = new PickMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case DROP_OBJECT:
                            $error = BasicActionFactory::drop($curplayer, $actionEvt->getParameter("OBJECT_ID"), $param, $db);
                            $obj = new DropMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GIVE_OBJECT:
                            $error = InteractionChecking::checkingGive($curplayer, $actionEvt->getParameter("OBJECT_ID"), $actionEvt->getParameter("PLAYER_ID"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new GiveMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case WEAR_EQUIPMENT:
                            $error = BasicActionFactory::wear($curplayer, $actionEvt->getParameter("OBJECT_ID"), $param, $db);
                            $obj = new WearMsg();
                            $msg = $obj->msgBuilder($error, $curplayer->get("incity"));
                            $str .= $msg;
                            break;
                        
                        case UNWEAR_EQUIPMENT:
                            $error = BasicActionFactory::unWear($curplayer, $actionEvt->getParameter("OBJECT_ID"), $param, $db);
                            $obj = new UnWearMsg();
                            $msg = $obj->msgBuilder($error, $curplayer->get("incity"));
                            $str .= $msg;
                            break;
                        
                        case STORE_OBJECT:
                            $error = BasicActionFactory::store($curplayer, $actionEvt->getParameter("OBJECT_ID"), $actionEvt->getParameter("INV_ID"), $param, $db);
                            $obj = new StoreMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case USE_POTION:
                            $error = BasicActionFactory::usePotion($curplayer, $actionEvt->getParameter("OBJECT_ID"), $param, $db);
                            $obj = new UsePotionMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        // Utilisation du parchemin
                        case USE_PARCHMENT:
                            $error = BasicActionFactory::useParchment($curplayer, $actionEvt->getParameter("PARCHMENT_ID"), $param, $db);
                            $obj = new UseParchmentMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        /* ************************************************* ACTIONS PROPRES AUX BATIMENTS **************************************************** */
                        
                        case TELEPORT:
                            $error = InsideBuildingFactory::teleport($curplayer, $actionEvt->getParameter("NEWTEMPLEID"), $param, $db);
                            $obj = new TempleTeleportMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case TEMPLE_HEAL:
                            $error = InsideBuildingFactory::templeHeal($curplayer, $actionEvt->getParameter("TEMPLELEVEL"), $param, $db);
                            $obj = new TempleHealMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case TEMPLE_BLESSING:
                            $error = InsideBuildingFactory::templeBlessing($curplayer, $actionEvt->getParameter("TEMPLELEVEL"), $param, $db);
                            $obj = new TempleBlessingMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case TEMPLE_RESURRECT:
                            $error = InsideBuildingFactory::templeResurrect($curplayer, $param, $db);
                            $obj = new TempleResurrectMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case TEMPLE_PALACE:
                            if ($actionEvt->getParameter("Emplacement") <= 0)
                                $str .= "Vous n'avez pas sélectionner d'emplacement pour votre palais";
                            else {
                                list ($x, $y) = explode(",", $actionEvt->getParameter("Emplacement"), 2);
                                if (! $error)
                                    $error = InsideBuildingFactory::templePalace($curplayer, $x, $y, $actionEvt->getParameter("nameVillage"), $param, $db);
                                
                                $obj = new TemplePalaceMsg();
                                $msg = $obj->msgBuilder($error, $param);
                                $str .= $msg;
                            }
                            break;
                        
                        case BEDROOM_SLEEPING:
                            $error = InsideBuildingFactory::bedroomSleeping($curplayer, $param, $db);
                            $obj = new BedroomSleepingMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case BEDROOM_DEPOSIT:
                            $error = InsideBuildingFactory::bedroomDeposit($curplayer, $actionEvt->getParameter("deposit"), $param, $db);
                            $obj = new BedroomDepositMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case BEDROOM_WITHDRAW:
                            $error = InsideBuildingFactory::bedroomWithDraw($curplayer, $actionEvt->getParameter("withdraw"), $param, $db);
                            $obj = new BedroomWithDrawMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case HOSTEL_DRINK:
                            $error = InsideBuildingFactory::hostelDrink($curplayer, $param, $db);
                            $obj = new HostelDrinkMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case HOSTEL_CHAT:
                            $error = InsideBuildingFactory::hostelChat($curplayer, $param, $db);
                            $obj = new HostelChatMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case HOSTEL_ROUND:
                            $error = InsideBuildingFactory::hostelRound($curplayer, $param, $db);
                            $obj = new HostelRoundMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        /* ------------------------ Action appeler pour la le message commercial --------------------------------------- */
                        case HOSTEL_TRADE:
                            $error = InsideBuildingFactory::hostelTrade($curplayer, $actionEvt->getParameter("Title"), $actionEvt->getParameter("Body"), $param, $db);
                            $obj = new HostelTradeMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case HOSTEL_NEWS:
                            $error = InsideBuildingFactory::hostelNews($curplayer, $actionEvt->getParameter("TITLE"), $actionEvt->getParameter("CONTENT"), 
                                $actionEvt->getParameter("CREATE"), $param, $db);
                            $obj = new HostelNewsMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case SCHOOL_LEARN_SPELL:
                            $error = InsideBuildingFactory::learnSpell($curplayer, $actionEvt->getParameter("IDSPELL"), $param, $db);
                            $obj = new LearnSpellMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case SCHOOL_LEARN_ABILITY:
                            $error = InsideBuildingFactory::learnAbility($curplayer, $actionEvt->getParameter("IDABILITY"), $param, $db);
                            $obj = new LearnAbilityMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case SCHOOL_LEARN_TALENT:
                            $error = InsideBuildingFactory::learnTalent($curplayer, $actionEvt->getParameter("IDTALENT"), $actionEvt->getParameter("BASICOBJET_ID"), $param, $db);
                            $obj = new LearnTalentMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BANK_DEPOSIT:
                            $error = InsideBuildingFactory::bankDeposit($curplayer, $actionEvt->getParameter("MONEYDEP"), $param, $db);
                            $obj = new BankDepositMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BANK_WITHDRAW:
                            $error = InsideBuildingFactory::bankWithdraw($curplayer, $actionEvt->getParameter("MONEY"), $param, $db);
                            $obj = new BankWithdrawMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BANK_TRANSFERT:
                            $error = InsideBuildingFactory::bankTransfert($curplayer, $actionEvt->getParameter("MONEY"), $actionEvt->getParameter("TARGET_ID"), 
                                $actionEvt->getParameter("TYPE"), $param, $db);
                            $obj = new BankTransfertMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BASIC_SHOP_BUY:
                            $error = InsideBuildingFactory::basicBuy($curplayer, $actionEvt->getParameter("ID_BASIC"), $param, $db);
                            $obj = new BasicBuyMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case MAIN_SHOP_BUY:
                            $error = InsideBuildingFactory::mainBuy($curplayer, $actionEvt->getParameter("ID_EQUIP"), $param, $db);
                            $obj = new mainBuyMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case SHOP_SELL:
                            
                            $idEquip = array();
                            foreach (explode(",", $actionEvt->getParameter("ID_EQUIP")) as $fullname)
                                $idEquip[] = $fullname;
                            
                            $error = InsideBuildingFactory::shopSell($curplayer, $idEquip, $param, $db);
                            $obj = new ShopSellMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case SHOP_REPAIR:
                            
                            $idEquip = array();
                            foreach (explode(",", $actionEvt->getParameter("ID_EQUIP")) as $fullname)
                                $idEquip[] = $fullname;
                            
                            $error = InsideBuildingFactory::shopRepair($curplayer, $idEquip, $actionEvt->getParameter("price"), $param, $db);
                            $obj = new ShopRepairMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case EXCHANGE_GET_OBJECT:
                            $error = InsideBuildingFactory::buyEntrepot($curplayer, $actionEvt->getParameter("ID_EQUIP"), $param, $db);
                            $obj = new mainBuyMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case EXCHANGE_OBJECT:
                            
                            $idEquip = array();
                            foreach (explode(",", $actionEvt->getParameter("ID_EQUIP")) as $fullname)
                                $idEquip[] = $fullname;
                            
                            $error = InsideBuildingFactory::sellEntrepot($curplayer, $idEquip, $param, $db);
                            $obj = new ShopSellMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case SHOP_REPAIR_EX:
                            
                            $idEquip = array();
                            foreach (explode(",", $actionEvt->getParameter("ID_EQUIP")) as $fullname)
                                $idEquip[] = $fullname;
                            
                            $error = InsideBuildingFactory::repairEntrepot($curplayer, $idEquip, $actionEvt->getParameter("price"), $param, $db);
                            $obj = new ShopRepairMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GUILD_ORDER_EQUIP:
                            $error = InsideBuildingFactory::orderEquip($curplayer, $actionEvt->getParameter("ID_BASIC"), $actionEvt->getParameter("EQ_LEVEL"), $param, $db);
                            $obj = new OrderEquipMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GUILD_ORDER_ENCHANT:
                            $error = InsideBuildingFactory::orderEnchant($curplayer, $actionEvt->getParameter("ID_EQUIP"), $actionEvt->getParameter("EN_LEVEL"), 
                                $actionEvt->getParameter("ID_BASIC_ENCHANT"), $param, $db);
                            $obj = new OrderEnchantMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GUILD_TAKE_EQUIP:
                            $error = InsideBuildingFactory::takeEquip($curplayer, $actionEvt->getParameter("ID_EQUIP"), $param, $db);
                            $obj = new TakeEquipMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GUILD_TAKE_ENCHANT:
                            $error = InsideBuildingFactory::takeEnchant($curplayer, $actionEvt->getParameter("ID_EQUIP"), $param, $db);
                            $obj = new TakeEnchantMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case GUILD_RESET_GAUGE:
                            $error = InsideBuildingFactory::resetGauge($curplayer, $actionEvt->getParameter("CARACT"), $param, $db);
                            $obj = new ResetGaugeMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CARAVAN_CREATE:
                            $error = InsideBuildingFactory::caravanCreate($curplayer, $actionEvt->getParameter("CARAVAN_LEVEL"), $actionEvt->getParameter("CARAVAN_TARGET"), 
                                $actionEvt->getParameter("LOAD"), $param, $db);
                            $obj = new CaravanCreateMsg();
                            if ($error == 0) {
                                $_POST["RETOUR"] = "OK";
                            }
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CARAVAN_TERMINATE:
                            $error = InsideBuildingFactory::caravanTerminate($curplayer, $param, $db);
                            $obj = new CaravanTerminateMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BUILDING_CONSTRUCT:
                            if ($actionEvt->getParameter("Emplacement") == 0)
                                $error = PLACE_FIELD_INVALID;
                            else
                                list ($x, $y) = explode(",", $actionEvt->getParameter("Emplacement"), 2);
                            if (! $error)
                                $error = InsideBuildingFactory::buildingConstruct($curplayer, $x, $y, $actionEvt->getParameter("basicBuilding"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new BuildingConstructMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BUILDING_REPAIR:
                            if (! $error)
                                $error = InsideBuildingFactory::buildingRepair($curplayer, $actionEvt->getParameter("ID_BUILDING"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new BuildingRepairMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case BUILDING_DESTROY:
                            if (! $error)
                                $error = InsideBuildingFactory::buildingDestroy($curplayer, $actionEvt->getParameter("ID_BUILDING"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new BuildingDestroyMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        /* Action donner un nom au village dans le palais */
                        case NAME_VILLAGE:
                            $nameVillage = $actionEvt->getParameter("nameVillage");
                            if (! $error)
                                $error = InsideBuildingFactory::nameVillage($curplayer, $nameVillage, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new NameVillageMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        /* construire une fortification */
                        case FORTIFY_VILLAGE:
                            if (! $error)
                                $error = InsideBuildingFactory::fortifyVillage($curplayer, $actionEvt->getParameter("fortifyVillage"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new FortifyVillageMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case UPGRADE_FORTIFICATION:
                            if (! $error)
                                $error = InsideBuildingFactory::upgradeFortification($curplayer, $actionEvt->getParameter("IDCITY"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new UpgradeFortificationMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case OPERATE_VILLAGE:
                            if (! $error)
                                $error = InsideBuildingFactory::operateVillage($curplayer, $actionEvt->getParameter("manag"), $actionEvt->getParameter("openDoorFriend"), $param, 
                                    $db);
                            $param["ERROR"] = $error;
                            $obj = new OperateVillageMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case OPERATE_VILLAGE_TEMPLE:
                            if (! $error)
                                $error = InsideBuildingFactory::operateVillageTemple($curplayer, $actionEvt->getParameter("manag"), $actionEvt->getParameter("openDoorFriend"), 
                                    $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new OperateVillageTempleMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case OPERATE_VILLAGE_ENTREPOT:
                            if (! $error)
                                $error = InsideBuildingFactory::operateVillageEntrepot($curplayer, $actionEvt->getParameter("manag"), $actionEvt->getParameter("openDoorFriend"), 
                                    $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new OperateVillageEntrepotMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case TREASURY_SET_TAX:
                            if (! $error)
                                $error = InsideBuildingFactory::setTax($curplayer, $actionEvt->getParameter("tax"), $actionEvt->getParameter("IDBUILDING"), 
                                    $actionEvt->getParameter("default"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SetTaxMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case PALACE_DEPOSIT:
                            if (! $error)
                                $error = InsideBuildingFactory::palaceDeposit($curplayer, $actionEvt->getParameter("deposit"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new PalaceDepositMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case PALACE_WITHDRAW:
                            if (! $error)
                                $error = InsideBuildingFactory::palaceWithdraw($curplayer, $actionEvt->getParameter("withdraw"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new PalaceWithdrawMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case PALACE_TRANSFER_MONEY:
                            if (! $error)
                                $error = InsideBuildingFactory::palaceTransferMoneyBat($curplayer, $actionEvt->getParameter("basicBuilding"), $actionEvt->getParameter("deposit"), 
                                    $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new PalaceTransferMoneyBatMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case TERRASSEMENT:
                            if (! $error)
                                $error = InsideBuildingFactory::terrassementVillage($curplayer, $actionEvt->getParameter("terrassement2"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TerrassementMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        case TERRAFORMATION:
                            if (! $error)
                                $error = InsideBuildingFactory::terraformationVillage($curplayer, $actionEvt->getParameter("terraformation2"), $actionEvt->getParameter("case"), 
                                    $actionEvt->getParameter("ground"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TerraformationMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        // --------------- Décision salle du gouverneur -----------------------
                        case PALACE_DECISION:
                            if (! $error)
                                $error = InsideBuildingFactory::palaceDecision($curplayer, $actionEvt->getParameter("deci"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new PalaceDecisionMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CREATE_PUTSCH:
                            if (! $error)
                                $error = InsideBuildingFactory::createPutsch($curplayer, $actionEvt->getParameter("chef"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new CreatePutschMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case ACT_PUTSCH:
                            if (! $error)
                                $error = InsideBuildingFactory::actPutsch($curplayer, $actionEvt->getParameter("leader"), $actionEvt->getParameter("CHOICE"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new ActPutschMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        case CONTROL_TEMPLE:
                            if (! $error)
                                $error = InsideBuildingFactory::controlTemple($curplayer, $actionEvt->getParameter("id"), $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new ControlTempleMsg();
                            $msg = $obj->msgBuilder($error, $param);
                            $str .= $msg;
                            break;
                        
                        /* ******************************************************** LES COMPETENCES **************************************************** */
                        
                        case ABILITY_DAMAGE:
                        case ABILITY_THRUST:
                        case ABILITY_TREACHEROUS:
                        case ABILITY_POWERFUL:
                        case ABILITY_STUNNED:
                        case ABILITY_KNOCKOUT:
                        case ABILITY_LIGHT:
                        case ABILITY_PROJECTION:
                            $error = InteractionChecking::checkingAbilityAttack($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, 
                                $db);
                            $param["ERROR"] = $error;
                            $obj = new AbilityAttackMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_FAR:
                        case ABILITY_DISABLING:
                        case ABILITY_NEGATION:
                            $error = InteractionChecking::checkingAbilityArchery($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), 
                                $actionEvt->getParameter("ARROW_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new AbilityArcheryMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_FLYARROW:
                            $target = array();
                            $target[1] = $actionEvt->getParameter("TARGET1_ID");
                            $target[2] = $actionEvt->getParameter("TARGET2_ID");
                            $target[3] = $actionEvt->getParameter("TARGET3_ID");
                            $target[4] = $actionEvt->getParameter("ARROW1_ID");
                            $target[5] = $actionEvt->getParameter("ARROW2_ID");
                            $target[6] = $actionEvt->getParameter("ARROW3_ID");
                            $error = InteractionChecking::checkingAbilityFlyArrow($curplayer, $actionEvt->getType(), $target, $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new AbilityFlyArrowMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_LARCENY:
                        case ABILITY_PROTECTION:
                        case ABILITY_STEAL:
                        case ABILITY_DISARM:
                        case ABILITY_FIRSTAID:
                            $error = InteractionChecking::checkingAbilityAttack($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, 
                                $db);
                            $param["ERROR"] = $error;
                            $obj = new AbilitySpecialMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_GUARD:
                        case ABILITY_AUTOREGEN:
                        case ABILITY_RUN:
                        case ABILITY_AMBUSH:
                        case ABILITY_RESISTANCE:
                        case ABILITY_BRAVERY:
                        case ABILITY_EXORCISM:
                            $error = InteractionChecking::checkingAbilityWithoutTarget($curplayer, $actionEvt->getType(), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new AbilityWithoutTargetMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_FLAMING:
                            $error = InteractionChecking::checkingFlaming($curplayer, $actionEvt->getType(), $actionEvt->getParameter("BUILDING_ID"), 
                                $actionEvt->getParameter("ARROW_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new AbilityFlamingMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_BOLAS:
                            $error = InteractionChecking::checkingBolas($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new BolasMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_TWIRL:
                            $error = InteractionChecking::checkingTwirl($curplayer, $actionEvt->getType(), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TwirlMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case ABILITY_SHARPEN:
                            $error = InteractionChecking::checkingSharpen($curplayer, $actionEvt->getType(), $actionEvt->getParameter("OBJECT_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SharpenMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        /* ******************************************************** DESACTIVATION DES AURAS **************************************************** */
                        
                        case DISABLED_BRAVERY:
                            $error = AbilityFactory::disabledBravery($curplayer, $param, $db);
                            $param["ERROR"] = $error;
                            $str .= "L'aura de courage a été désactivée";
                            break;
                        
                        case DISABLED_PROTECTION:
                            $error = AbilityFactory::disabledProtection($curplayer, $param, $db);
                            $param["ERROR"] = $error;
                            $str .= "La protection a été désactivée";
                            break;
                        
                        case DISABLED_RESISTANCE:
                            $error = AbilityFactory::disabledResistance($curplayer, $param, $db);
                            $param["ERROR"] = $error;
                            $str .= "L'aura de résistance a été désactivée";
                            break;
                        
                        case REVOKE_GOLEM:
                            $error = SpellFactory::revokeGolem($curplayer, $param, $db);
                            $param["ERROR"] = $error;
                            $str .= "Le golem de feu a été révoqué";
                            break;
                        
                        case REVOKE_SOUL:
                            $error = SpellFactory::revokeSoul($curplayer, $param, $db);
                            $param["ERROR"] = $error;
                            $str .= "Vous avez récupéré votre âme";
                            break;
                        
                        /* ******************************************************** LES SORTILEGES ****************************************************** */
                        case SPELL_BURNING:
                        case SPELL_TEARS:
                        case SPELL_ARMOR:
                        case SPELL_REGEN:
                        case SPELL_CURE:
                        case SPELL_BARRIER:
                        case SPELL_BUBBLE:
                        case SPELL_FIREBALL:
                        case SPELL_BLESS:
                        case SPELL_BLOOD:
                        case SPELL_SHIELD:
                        case SPELL_LIGHTTOUCH:
                        case M_INSTANTBLOOD:
                            $error = InteractionChecking::checkingSpell($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        case SPELL_RECALL:
                            $error = InteractionChecking::checkingSpellRecall($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        case SPELL_NEGATION:
                            $error = InteractionChecking::checkingNegation($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_KINE:
                            $error = InteractionChecking::checkingKine($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $actionEvt->getParameter("SLOT"), 
                                $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new KineMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_ANGER:
                        case SPELL_CURSE:
                            $error = InteractionChecking::checkingAngerCurse($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), 
                                $actionEvt->getParameter("CHARAC"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_FIRECALL:
                            $error = InteractionChecking::checkingFireCall($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new FireCallMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_SPRING:
                            if ($actionEvt->getParameter("Case1") == 0)
                                $error = PLACE_FIELD_INVALID;
                            else
                                list ($x, $y) = explode(",", $actionEvt->getParameter("Case1"), 2);
                            if (! $error)
                                $error = InteractionChecking::checkingSpring($curplayer, $actionEvt->getType(), $x, $y, $paramability, $param, $db);
                            
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_WALL:
                            $x = array();
                            $y = array();
                            for ($i = 1; $i < 5; $i ++) {
                                if ($actionEvt->getParameter("Case" . $i) == 0)
                                    $error = PLACE_FIELD_INVALID;
                                
                                if ($actionEvt->getParameter("Case" . $i) > 0)
                                    list ($x[$i], $y[$i]) = explode(",", $actionEvt->getParameter("Case" . $i), 2);
                                else {
                                    $x[$i] = - 1;
                                    $y[$i] = - 1;
                                }
                            }
                            if (! $error)
                                $error = InteractionChecking::checkingWall($curplayer, $actionEvt->getType(), $x, $y, $paramability, $param, $db);
                            
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_PILLARS:
                            $x = array();
                            $y = array();
                            for ($i = 1; $i < 4; $i ++) {
                                if ($actionEvt->getParameter("Case" . $i) == 0)
                                    $error = PLACE_FIELD_INVALID;
                                
                                if ($actionEvt->getParameter("Case" . $i) > 0)
                                    list ($x[$i], $y[$i]) = explode(",", $actionEvt->getParameter("Case" . $i), 2);
                                else {
                                    $x[$i] = - 1;
                                    $y[$i] = - 1;
                                }
                            }
                            if (! $error)
                                $error = InteractionChecking::checkingPillars($curplayer, $actionEvt->getType(), $x, $y, $paramability, $param, $db);
                            
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_DEMONPUNCH:
                            $error = InteractionChecking::checkingDemonpunch($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), 
                                $actionEvt->getParameter("SLOT"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_FIRE:
                        case SPELL_RAIN:
                            list ($x, $y) = explode(",", $actionEvt->getParameter("TARGET_ID"), 2);
                            $error = InteractionChecking::checkingZone($curplayer, $actionEvt->getType(), $x, $y, $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_RELOAD:
                            $error = InteractionChecking::checkingReload($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_SUN:
                            $error = InteractionChecking::checkingSun($curplayer, $actionEvt->getType(), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_TELEPORT:
                            $error = InteractionChecking::checkingTeleport($curplayer, $actionEvt->getType(), $actionEvt->getParameter("NEWX"), $actionEvt->getParameter("NEWY"), 
                                $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case SPELL_SOUL:
                            
                            $error = InteractionChecking::checkingSoul($curplayer, $actionEvt->getType(), $actionEvt->getParameter("NEWX"), $actionEvt->getParameter("NEWY"), 
                                $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case S_MASSIVEINSTANTTP:
                            $error = InteractionChecking::checkingMassiveTP($curplayer, $actionEvt->getType(), $actionEvt->getParameter("NEWX"), $actionEvt->getParameter("NEWY"), 
                                $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpellMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        /* ******************************************************** LES SAVOIRS FAIRE EXTRACTION ****************************************************** */
                        
                        case TALENT_MINE:
                        case TALENT_DISMEMBER:
                        case TALENT_SCUTCH:
                        case TALENT_HARVEST:
                            $error = InteractionChecking::checkingExtraction($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new ExtractionMsg();
                            $msg = $obj->msgBuilder($error, $actionEvt->getType(), $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case TALENT_SPEAK:
                            $error = InteractionChecking::checkingSpeak($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new SpeakMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case TALENT_CUT:
                            $error = InteractionChecking::checkingCut($curplayer, $actionEvt->getType(), $paramability, $param, $db);
                            
                            $param["ERROR"] = $error;
                            $obj = new CutMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        /* ******************************************************** LES SAVOIRS FAIRE DE RAFINNAGE ****************************************************** */
                        case TALENT_IRONREFINE:
                        case TALENT_WOODREFINE:
                        case TALENT_LINENREFINE:
                        case TALENT_LEATHERREFINE:
                        case TALENT_SCALEREFINE:
                        case TALENT_GEMREFINE:
                        case TALENT_PLANTREFINE:
                            $error = InteractionChecking::checkingRefine($curplayer, $actionEvt->getType(), $actionEvt->getParameter("ID_OBJECT1"), 
                                $actionEvt->getParameter("ID_OBJECT2"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentRefineMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        /* ******************************************************** LES SAVOIRS FAIRE ARTISANAT ****************************************************** */
                        case TALENT_IRONCRAFT:
                        case TALENT_WOODCRAFT:
                        case TALENT_LINENCRAFT:
                        case TALENT_LEATHERCRAFT:
                        case TALENT_SCALECRAFT:
                        case TALENT_PLANTCRAFT:
                            $error = InteractionChecking::checkingCraft($curplayer, $actionEvt->getType(), $actionEvt->getParameter("BASICOBJET_ID"), 
                                $actionEvt->getParameter("ID_OBJECT"), $actionEvt->getParameter("NIVEAU"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentCraftMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case TALENT_GEMCRAFT:
                            $error = InteractionChecking::checkingGemCraft($curplayer, $actionEvt->getType(), $actionEvt->getParameter("EQUIP_ID"), 
                                $actionEvt->getParameter("ID_OBJECT1"), $actionEvt->getParameter("ID_OBJECT2"), $actionEvt->getParameter("ID_TEMPLATE"), 
                                $actionEvt->getParameter("NIVEAU"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentEnchantMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            
                            break;
                        
                        /* ******************************************************** LES SAVOIRS FAIRE NATURE ****************************************************** */
                        
                        case TALENT_DETECTION:
                            $error = InteractionChecking::checkingTalentDetection($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), 
                                $actionEvt->getParameter("NIVEAU"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentDetectionMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case TALENT_MONSTER:
                            $error = InteractionChecking::checkingTalentMonster($curplayer, $actionEvt->getType(), $actionEvt->getParameter("TARGET_ID"), 
                                $actionEvt->getParameter("CHARAC1"), $actionEvt->getParameter("CHARAC2"), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentMonsterMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case TALENT_KRADJECKCALL:
                            $error = InteractionChecking::checkingKrajeckCall($curplayer, $actionEvt->getType(), $paramability, $param, $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentKradjeckCallMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        case TALENT_CLOSEGATE:
                            $error = InteractionChecking::checkingTalentCloseGate($curplayer, $actionEvt->getType(), $actionEvt->getParameter("ID_GATE"), $paramability, $param, 
                                $db);
                            $param["ERROR"] = $error;
                            $obj = new TalentCloseGateMsg();
                            $msg = $obj->msgBuilder($error, $paramability, $param);
                            $str .= $msg;
                            if (! $error)
                                $error = ! $paramability["SUCCESS"];
                            break;
                        
                        /* ************************************************************* FIN DES ACTIONS - LOG DES ACTIONS ******************************* */
                    }
                    $curplayer->updateDBr($db);
                    if (! $error) {
                        if (isset($param["TYPE_ACTION"]) && $param["TYPE_ACTION"] != $actionEvt->getType())
                            Event::logAction($db, $param["TYPE_ACTION"], $param);
                        else
                            Event::logAction($db, $actionEvt->getType(), $param);
                    }
                } else
                    $str .= localize("Il est impossible de faire d'Action avec ce personnage ! ") . "<br/>";
            } else
                $str .= localize("Il est impossible de faire d'Action dans le mode restreint !") . "<br/>" . localize("Vous devez d'abord activer la DLA de ce joueur.");
            
            if (isset($msg)) {
                $str .= "<script type=\"text/javascript\">\n";
                $str .= $this->getWriterJS($msg, "cqaction");
                $str .= "</script>\n ";
            }
            $str .= "</div>";
            $str .= "</div>";
        }
        
        $this->str = $str;
    }

    public function toString()
    {
        return $this->str;
    }

    public function getMailState()
    {
        $dbm = new DBCollection(
            "SELECT Player.id,Player.ap,count(Mail.id) AS count FROM Player LEFT JOIN Mail ON id_Player\$receiver = Player.id AND Mail.new=1 WHERE id_Member =" .
                 $this->nacridan->auth->auth["uid"] . " GROUP BY Player.id", $this->db);
        $dbmch = new DBCollection(
            "SELECT Player.id,Player.ap,count(MailTrade.id) AS count FROM Player LEFT JOIN MailTrade ON id_Player\$receiver = Player.id AND MailTrade.new=1 WHERE id_Member =" .
                 $this->nacridan->auth->auth["uid"] . " GROUP BY Player.id", $this->db);
        
        // $typemsg = new DbCollection("SELECT * FROM MailBody", $this->db);
        // $type = $typemsg->get("type");
        $players = $this->nacridan->loadSessPlayers();
        $str = "";
        
        $i = 0;
        
        while (! $dbm->eof()) {
            $add = "";
            if ($dbm->get("count") > 0) {
                $add = "!!!";
                $str .= $players[$dbm->get("id")] . ", " . localize('Message(s) non lu(s)') . " : <b>" . $dbm->get("count") . " " . $add . "</b><br/>";
            }
            
            $dbm->next();
        }
        
        while (! $dbmch->eof()) {
            $add = "";
            if ($dbmch->get("count") > 0) {
                $add = "!!!";
                if ($dbmch->get("count") == 1)
                    $str .= $players[$dbmch->get("id")] . ", " . localize('Message commercial non lu') . " : <b>" . $dbmch->get("count") . " " . $add . "</b><br/>";
                else
                    $str .= $players[$dbmch->get("id")] . ", " . localize('Messages commerciaux non lus') . " : <b>" . $dbmch->get("count") . " " . $add . "</b><br/>";
            }
            
            $dbmch->next();
        }
        
        return $str;
    }

    public function getWriterJS($msg, $idClientDest)
    {
        $str = 'var chaine = "' . $msg . '";';
        $str .= 'var interval=0;
var tableau = chaine.split("$");
var nb_car = tableau.length;
texte = new Array;
var txt = "";
var nb_msg = nb_car - 1;
for (i=0; i<nb_car; i++) {
texte[i] = txt+tableau[i];
var txt = texte[i];
}

actual_texte = 0;

function changeMessage()
{
document.getElementById("' . $idClientDest . '").innerHTML = texte[actual_texte];
actual_texte++;
if(actual_texte >= nb_car)
 {
  clearInterval(interval);
 }
}

if(document.getElementById)
  interval=setInterval("changeMessage()",80)';
        return $str;
    }

    public function getAskJoin()
    {
        $dbm = new DBCollection("SELECT count(id) AS count FROM TeamAskJoin WHERE id_Team=" . $this->curplayer->get("id_Team"), $this->db);
        
        $str = "";
        $add = "";
        if ($dbm->get("count") > 0) {
            $add = "!!!";
            $str .= localize('Nouveau(x) Demandeur(s) voulant rejoindre votre Ordre') . " : <b>" . $dbm->get("count") . " " . $add . "</b><br/>";
        }
        return $str;
    }
}

?>
