<?php

class CQGiveMoney extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQGiveMoney($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        $building = $curplayer->get("inbuilding");
        $room = $curplayer->get("room");
        
        if ($curplayer->get("ap") < GIVE_MONEY_AP) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour faire un don de Pièces d'Or (PO).");
            $str .= "</td></tr></table>";
        } else {
            if (! isset($_POST["action"])) {
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>";
                $str .= localize("Donner") . " <input type='textbox' name='MONEY' />" . " " . localize("PO") . " " . localize("à") . " ";
                
                $str .= "<select id='target' class='selector' name='TARGET_ID'>";
                
                $gap = 1;
                $dbp = new DBCollection("SELECT id,name FROM Player WHERE disabled=0 AND inbuilding=" . $building . " AND room=" . $room . " AND status='PC' AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $gap, $db, 0, 0);
                
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
                $item[] = array(
                    localize("-- Personnage(s) à portée --") => - 1
                );
                while (! $dbp->eof()) {
                    if ($dbp->get("id") != $id && $curplayer->canSeePlayerById($dbp->get("id"), $this->db)) {
                        $item[] = array(
                            $dbp->get("name") => $dbp->get("id")
                        );
                    }
                    $dbp->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                $str .= "</select></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . GIVE_MONEY . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                if ($_POST["MONEY"] <= 0) {
                    $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                    $str .= localize("Le nombre de PO n'est pas valide.");
                    $str .= "</td></tr></table>";
                } else {
                    if ($_POST["MONEY"] > $curplayer->get("money")) {
                        $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                        $str .= localize("Vous n'avez pas assez de PO.");
                        $str .= "</td></tr></table>";
                    } else {
                        $dbp = new DBCollection("SELECT name FROM Player WHERE status='PC' AND id=" . quote_smart($_POST["TARGET_ID"]), $db, 0, 0);
                        if (! $dbp->eof()) {
                            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>";
                            
                            $str .= localize("Donner {nb} PO à {name}. Continuer ?", array(
                                "nb" => $_POST["MONEY"],
                                "name" => $dbp->get("name")
                            )) . " :</td><td>";
                            
                            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                            $str .= "<input name='TARGET_ID' type='hidden' value='" . quote_smart($_POST["TARGET_ID"]) . "' />";
                            $str .= "<input name='MONEY' type='hidden' value='" . quote_smart($_POST["MONEY"]) . "' />";
                            $str .= "<input name='action' type='hidden' value='" . GIVE_MONEY . "' />";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "</td></tr></table>";
                            $str .= "</form>";
                        } else {
                            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                            $str .= localize("Le destinataire n'est pas un joueur valide.");
                            $str .= "</td></tr></table>";
                        }
                    }
                }
            }
        }
        return $str;
    }
}
?>
