<?php

class CQTalent extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $id;

    public function CQTalent($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (isset($_GET["id"])) {
            $this->id = $id = quote_smart($_GET["id"]);
            $this->str = $this->getForm();
        } else {
            $this->str = "";
        }
    }

    protected function getBasicTemplate()
    {
        $str = "<select class='selector' name='ID_TEMPLATE'>";
        $item = array();
        $dbtemp = new DBCollection(
            "SELECT * FROM BasicTemplate WHERE id IN (SELECT id_BasicEquipment FROM Knowledge WHERE id_Player=" . $this->curplayer->get("id") . " AND id_BasicTalent=" .
                 (constant($this->id) - 300) . ")", $this->db, 0, 0);
        
        while (! $dbtemp->eof()) {
            
            $item[] = array(
                localize($dbtemp->get("name") . "/" . $dbtemp->get("name2")) => $dbtemp->get("id")
            );
            $dbtemp->next();
        }
        
        $item[] = array(
            localize("ignorer ce champs") => 0
        );
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getGate()
    {
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $str = "<select class='selector' name='ID_GATE'>";
        $item = array();
        $item[] = array(
            localize("-- Portail à proximité --") => - 1
        );
        
        $dbtemp = new DBCollection("SELECT * FROM Gate WHERE nbNPC>0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=1", 
            $this->db);
        
        while (! $dbtemp->eof()) {
            
            $item[] = array(
                localize("Portail (" . $dbtemp->get("id") . ")") => $dbtemp->get("id")
            );
            $dbtemp->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getNiveau($max = 16)
    {
        $str = "<select class='selector' name='NIVEAU'>";
        $item = array();
        for ($i = 1; $i < $max; $i ++)
            $item[] = array(
                $i => $i
            );
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getEquipmentGemCraft()
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $item = array();
        $str = "<select class='selector cqattackselectorsize' name='EQUIP_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Vous désirez --") . "</option>";
        
        // Equipement pour enchantement mineur
        $item[] = array(
            localize("-- Enchantement mineur --") => - 1
        );
        $dbe = new DBCollection(
            "SELECT * FROM Equipment WHERE id_EquipmentType <= 23 AND durability!=0 AND progress=100 AND weared='NO' AND state='Carried' AND extraname ='' AND id_Player=" . $id, 
            $this->db, 0, 0);
        while (! $dbe->eof()) {
            $item[] = array(
                localize($dbe->get("name")) . " niveau " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
            );
            $dbe->next();
        }
        
        // Equipement pour enchantement majeur LEFT JOIN Template ON Equipment.id = Template.id_Equipment
        $item[] = array(
            localize("-- Enchantement majeur -- ") => - 1
        );
        $dbe = new DBCollection(
            "SELECT * FROM Equipment  WHERE id_EquipmentType < 23 AND durability!=0 AND progress=100 AND Templateprogress=100 AND weared='NO' AND state='Carried' AND extraname !='' AND id_Player=" .
                 $id, $this->db, 0, 0);
        while (! $dbe->eof()) {
            $dbx = new DBCollection("SELECT * FROM Template WHERE id_Equipment=" . $dbe->get("id"), $this->db, 0, 0);
            if ($dbx->count() < 2)
                $item[] = array(
                    localize($dbe->get("name")) . " niveau " . $dbe->get("level") . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                );
            $dbe->next();
        }
        
        // Equipement à frabriquer
        $item[] = array(
            localize("-- Fabrication --") => - 1
        );
        $dbnpc = new DBCollection(
            "SELECT * FROM BasicEquipment WHERE id>5 AND id IN (SELECT id_BasicEquipment FROM Knowledge WHERE id_Player=" . $this->curplayer->get("id") . " AND id_BasicTalent=" .
                 (constant($this->id) - 300) . ")", $this->db, 0, 0);
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("name")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        // Equipement à finir
        $item[] = array(
            localize("-- Continuer un équipement --") => - 1
        );
        $dbe = new DBCollection(
            "SELECT * FROM Equipment  WHERE ((id_BasicEquipment = 11 or id_BasicEquipment = 57  or id_BasicEquipment = 58 or id_BasicEquipment = 204 or id_BasicEquipment = 220 or id_BasicEquipment =12) AND progress != 100) AND id_Player=" .
                 $id, $this->db, 0, 0);
        while (! $dbe->eof()) {
            $item[] = array(
                localize($dbe->get("name")) . " niveau " . $dbe->get("level") . " (" . $dbe->get("progress") . "%)  - (" . $dbe->get("id") . ")" => $dbe->get("id")
            );
            $dbe->next();
        }
        
        $item[] = array(
            localize("-- Continuer un enchantement --") => - 1
        );
        $dbnpc = new DBCollection("SELECT * FROM Equipment  WHERE extraname !='' AND templateProgress != 100  AND id_Player=" . $id, $this->db, 0, 0);
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("name")) . " niveau " . $dbnpc->get("level") . "  " . $dbnpc->get("extraname") . " (" . $dbnpc->get("templateProgress") . "%)  - (" .
                     $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getBasicEquipment($id_BasicMaterial)
    {
        $str = "<select class='selector' name='BASICOBJET_ID'>";
        $gap = 1;
        $item = array();
        
        // $dbnpc=new DBCollection("SELECT * FROM BasicEquipment WHERE id_Modifier_BasicEquipment != 999 AND id_BasicMaterial=".$id_BasicMaterial, $this->db,0,0,0);
        $dbnpc = new DBCollection(
            "SELECT * FROM BasicEquipment WHERE id IN (SELECT id_BasicEquipment FROM Knowledge WHERE id_Player=" . $this->curplayer->get("id") . " AND id_BasicTalent=" .
                 (constant($this->id) - 300) . ")", $this->db, 0, 0);
        $str .= "<option value='0' selected='selected'>" . localize("-- Confectionner un nouvel objet --") . "</option>";
        
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("name")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "<option value='0'>" . localize(" -- Continuer la fabrication --") . "</option>";
        
        $str .= "</select>";
        return $str;
    }

    protected function getEquipmentType($id)
    {
        $str = "<select class='selector cqattackselectorsize' name='BASICOBJET_ID'>";
        $gap = 1;
        $item = array();
        $dbnpc = new DBCollection(
            "SELECT * FROM BasicEquipment WHERE id IN (SELECT id_BasicEquipment FROM Knowledge WHERE id_Player=" . $this->curplayer->get("id") . " AND id_BasicTalent=" .
                 (constant($this->id) - 300) . ")", $this->db, 0, 0);
        
        $str .= "<option value='0' selected='selected'>" . localize("-- Potion à créer --") . "</option>";
        
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("name")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "<option value='0'>" . localize(" -- Continuer la fabrication --") . "</option>";
        
        $str .= "</select>";
        return $str;
    }

    protected function getRawMaterial($id_BasicEquipment, $name = "ID_OBJECT", $ident = 0)
    {
        $str = "<select class='selector cqattackselectorsize' name='" . $name . "'>";
        $cond = "";
        if (($ident == TALENT_GEMCRAFT || $ident = TALENT_GEMREFINE) && $id_BasicEquipment == 100)
            $cond = " OR E1.id_BasicEquipment = 101";
        
        if (($ident == TALENT_PLANTCRAFT || $ident = TALENT_PLANTREFINE) && $id_BasicEquipment == 107)
            $cond = " OR E1.id_BasicEquipment = 108 OR E1.id_BasicEquipment = 109 ";
        
        $item = array();
        $dbnpc = new DBCollection(
            "SELECT E1.* FROM Equipment E1 left outer join Equipment E2 on E1.id_Equipment\$bag = E2.id WHERE E1.id_Player=" . $this->curplayer->get("id") .
                 " AND (E1.id_BasicEquipment=" . $id_BasicEquipment . $cond . ") and (E1.id_Equipment\$bag =0 or E2.weared='Yes') ", $this->db, 0, 0);
        $str .= "<option value='0' selected='selected'>" . localize("-- Matières premières --") . "</option>";
        
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("name")) . " niv. " . $dbnpc->get("level") . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        if ($ident != TALENT_GEMCRAFT) {
            $item[] = array(
                localize("-- Objets inachevés --") => - 1
            );
            $dbc = new DBCollection(
                "SELECT id_BasicMaterial FROM BasicEquipment LEFT JOIN Equipment ON Equipment.id_BasicEquipment = BasicEquipment.id WHERE BasicEquipment.id=" . $id_BasicEquipment, 
                $this->db);
            $dbnpc = new DBCollection(
                "SELECT Equipment.level as level, Equipment.progress as progress, Equipment.name as name, Equipment.id as id FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment = BasicEquipment.id WHERE id_Player=" .
                     $this->curplayer->get("id") . " AND Equipment.progress != 100 AND BasicEquipment.id_BasicMaterial=" . $dbc->get("id_BasicMaterial"), $this->db, 0, 0);
            while (! $dbnpc->eof()) {
                $item[] = array(
                    localize($dbnpc->get("name")) . " niveau " . $dbnpc->get("level") . " (" . $dbnpc->get("progress") . "%)  - (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
                );
                $dbnpc->next();
            }
        }
        
        $item[] = array(
            localize("Ignorer ce champs") => 0
        );
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getEquipmentFromEquipmentType($id_EquipmentType, $name = "ID_OBJECT")
    {
        $str = "<select class='selector cqattackselectorsize' name='" . $name . "'>";
        $gap = 1;
        $item = array();
        $dbnpc = new DBCollection("SELECT * FROM Equipment WHERE id_Player=" . $this->curplayer->get("id") . " AND id_EquipmentType=" . $id_EquipmentType, $this->db, 0, 0);
        $str .= "<option value='0' selected='selected'>" . localize("-- Inventaire --") . "</option>";
        
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("name")) . " niv. " . $dbnpc->get("level") . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getKradjeck()
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $str = "<select class='selector cqattackselectorsize' name='TARGET_ID'>";
        $gap = 1;
        $item = array();
        $dbnpc = new DBCollection(
            "SELECT * FROM Player WHERE map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 =" . $gap . " AND id_BasicRace = 253", 
            $this->db, 0, 0);
        $str .= "<option value='0' selected='selected'>" . localize("-- Kradjeck à proximité --") . "</option>";
        
        while (! $dbnpc->eof()) {
            $item[] = array(
                localize($dbnpc->get("racename")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
            );
            $dbnpc->next();
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getSelector($name)
    {
        $id = $this->curplayer->get("id");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $gap = 1;
        $item = array();
        $str = "<select class='selector ' name='TARGET_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une ressource à exploiter --") . "</option>";
        if ($this->curplayer->get("inbuilding") == 0) {
            $dbr = new DBCollection(
                "SELECT * FROM Exploitation WHERE type='" . $name . "' AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 =" .
                     $gap, $this->db, 0, 0);
            while (! $dbr->eof()) {
                $item[] = array(
                    localize($dbr->get("name")) . " (X=" . $dbr->get("x") . " ; Y=" . $dbr->get("y") . ")" => $dbr->get("id")
                );
                $dbr->next();
            }
        }
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getPlayer($myself = 0, $pj = 1, $pnj = 1, $gap = 1, $name = "TARGET_ID")
    {
        $id = $this->curplayer->get("id");
        $curplayer = $this->curplayer;
        $inbuilding = $this->curplayer->get("inbuilding");
        $room = $this->curplayer->get("room");
        $xp = $this->curplayer->get("x");
        $yp = $this->curplayer->get("y");
        $map = $this->curplayer->get("map");
        
        $item = array();
        
        $str = "<select class='selector cqattackselectorsize' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
        
        if ($pnj) {
            if ($inbuilding && $room != TOWER_ROOM)
                $dbnpc = new DBCollection("SELECT * FROM Player WHERE status='NPC' and inbuilding=" . $inbuilding . " AND room=" . $room, $this->db, 0, 0);
            else
                $dbnpc = new DBCollection(
                    "SELECT * FROM Player WHERE status='NPC' AND disabled=0 AND inbuilding=0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp .
                         "-" . $yp . "))/2 <=" . $gap, $this->db, 0, 0);
            
            $item[] = array(
                localize("-- PNJ à portée --") => - 1
            );
            while (! $dbnpc->eof()) {
                if ($curplayer->canSeePlayerById($dbnpc->get("id"), $this->db))
                    $item[] = array(
                        localize($dbnpc->get("racename")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
                    );
                $dbnpc->next();
            }
        }
        
        if ($pj) {
            if ($inbuilding)
                $dbp = new DBCollection("SELECT * FROM Player WHERE status='PC' and inbuilding=" . $inbuilding . " AND room=" . $room, $this->db, 0, 0);
            else
                $dbp = new DBCollection(
                    "SELECT * FROM Player WHERE status='PC' AND disabled=0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp .
                         "))/2 <=" . $gap, $this->db, 0, 0);
            
            $item[] = array(
                localize("-- Personnage(s) à portée --") => - 1
            );
            while (! $dbp->eof()) {
                if (($dbp->get("id") != $id || $myself) && $curplayer->canSeePlayerById($dbp->get("id"), $this->db)) {
                    $item[] = array(
                        $dbp->get("name") => $dbp->get("id")
                    );
                }
                $dbp->next();
            }
        }
        
        foreach ($item as $arr) {
            foreach ($arr as $key => $value) {
                if ($value == - 1)
                    $str .= "<optgroup class='group' label='" . $key . "' />";
                else
                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }

    protected function getFightCharac($name)
    {
        $str = "<select class='selector' name='" . $name . "'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une caractéristique --") . "</option>";
        $str .= "<option value='1'> Attaque </option>";
        $str .= "<option value='2'> Dégâts </option>";
        $str .= "<option value='3'> Défense </option>";
        $str .= "<option value='4'> Armure </option>";
        $str .= "<option value='5'> Maitrise de la magie </option>";
        $str .= "</select>";
        
        return $str;
    }

    protected function detection()
    {
        $str = "<select class='selector' name='TARGET_ID'>";
        $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une ressource --") . "</option>";
        $str .= "<option value='1'> Champs de Lin </option>";
        $str .= "<option value='2'> Buisson de Razhot </option>";
        $str .= "<option value='3'> Buisson de Garnarch </option>";
        $str .= "<option value='4'> Buisson de Folliane </option>";
        $str .= "<option value='5'> Gisement d'Emeraude </option>";
        $str .= "<option value='6'> Gisement de Rubis </option>";
        $str .= "</select>";
        
        return $str;
    }

    public function toString()
    {
        return $this->str;
    }

    public function getForm()
    {
        $dbc = new DBCollection(
            "SELECT * FROM Talent LEFT JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id WHERE ident='" . $this->id . "' AND id_Player=" . $this->curplayer->get("id"), 
            $this->db);
        if (! $dbc->eof()) {
            $time = $dbc->get("PA");
            if ($this->curplayer->get("ap") < $time) {
                $str = "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser le savoir-faire : {talent}.", 
                    array(
                        "talent" => localize($dbc->get("name"))
                    ));
                $str .= "</td></tr></table>";
            } else {
                $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td >";
                $str .= localize($dbc->get("name")) . " ";
                
                $strAct = "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . constant($this->id) . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                
                switch (constant($this->id)) {
                    case TALENT_MINE:
                        $str .= $this->getSelector("deposit");
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_DISMEMBER:
                        $str .= $this->getSelector("body");
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_SCUTCH:
                        $str .= $this->getSelector("crop");
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_HARVEST:
                        $str .= $this->getSelector("bush");
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_SPEAK:
                        $str .= $this->getKradjeck();
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_CUT:
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_KRADJECKCALL:
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_DETECTION:
                        $str .= $this->Detection();
                        $str .= " de niveau " . $this->getNiveau(4);
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_CLOSEGATE:
                        $str .= $this->getGate();
                        $str .= "</td><td width='50px' align='right'>" . $strAct;
                        break;
                    case TALENT_MONSTER:
                        $str .= " </td><td>" . $this->getPlayer(0, 0, 1, 5) . "<td align='center' rowspan=3> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  rowspan=2> </td><td>" . $this->getFightCharac("CHARAC1");
                        $str .= "<tr class='mainbgtitle'> <td>" . $this->getFightCharac("CHARAC2");
                        break;
                    case TALENT_IRONCRAFT:
                        $str .= "</td><td>" . $this->getBasicEquipment(1);
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='120px'> avec </td><td>" . $this->getRawMaterial(106);
                        break;
                    case TALENT_WOODCRAFT:
                        $str .= "</td><td>" . $this->getBasicEquipment(2);
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='120px'> avec </td><td>" . $this->getRawMaterial(110);
                        break;
                    case TALENT_LEATHERCRAFT:
                        $str .= "</td><td>" . $this->getBasicEquipment(4);
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='120px'> avec </td><td>" . $this->getRawMaterial(103);
                        break;
                    case TALENT_SCALECRAFT:
                        $str .= "</td><td>" . $this->getBasicEquipment(3);
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='120px'> avec </td><td>" . $this->getRawMaterial(104);
                        break;
                    case TALENT_LINENCRAFT:
                        $str .= "</td><td>" . $this->getBasicEquipment(5);
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='120px'> avec </td><td>" . $this->getRawMaterial(105);
                        break;
                    case TALENT_PLANTCRAFT:
                        $str .= "</td><td>" . $this->getEquipmentType(29);
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='140px'> avec </td><td>" . $this->getRawMaterial(107);
                        break;
                    case TALENT_GEMCRAFT:
                        $str .= "</td><td>" . $this->getEquipmentGemCraft();
                        $str .= " de niveau " . $this->getNiveau() . "<td align='center' rowspan=4> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='150px'> avec la gemme </td><td>" . $this->getRawMaterial(100, "ID_OBJECT1", constant($this->id)) .
                             " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='150px'> et le bois </td><td>" . $this->getRawMaterial(110, "ID_OBJECT2", constant($this->id)) . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='150px'> Type d'enchantement  </td><td>" . $this->getBasicTemplate();
                        
                        break;
                    case TALENT_IRONREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(106, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(106, "ID_OBJECT2");
                        break;
                    case TALENT_WOODREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(110, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(110, "ID_OBJECT2");
                        break;
                    case TALENT_LEATHERREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(103, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(103, "ID_OBJECT2");
                        break;
                    case TALENT_SCALEREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(104, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(104, "ID_OBJECT2");
                        break;
                    case TALENT_LINENREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(105, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(105, "ID_OBJECT2");
                        break;
                    case TALENT_PLANTREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(107, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(107, "ID_OBJECT2");
                        break;
                    case TALENT_GEMREFINE:
                        $str .= "</td><td>" . $this->getRawMaterial(100, "ID_OBJECT1") . "<td align='center' rowspan=2> " . $strAct . " </td></tr>";
                        $str .= "<tr class='mainbgtitle'> <td  width='80px'> avec </td><td> " . $this->getRawMaterial(100, "ID_OBJECT2");
                        break;
                }
                
                $str .= "</td></tr></table>";
                $str .= "</form>";
            }
        } else {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous ne possédez pas ce savoir-faire.");
            $str .= "</td></tr></table>";
        }
        return $str;
    }
}
?>
