<?php

class CQConcentration extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQConcentration($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
        $str .= "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>";
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $id . " AND name='Concentration' ORDER BY date DESC", $db);
        
        if ($curplayer->get("ap") < 2)
            $str .= "<tr><td> Vous n'avez pas suffisamment de PA pour effectuer cette action.</td></tr>";
        elseif ($dbbm->eof())
            $str .= "<tr><td> Vous n'avez raté aucune action durant ce tour, vous ne pouvez pas utiliser cette action </td></tr>";
        else {
            $num = floor($dbbm->get("value") / 100);
            if ($num == 1)
                $dba = new DBCollection("SELECT * FROM BasicAbility WHERE id=" . ($dbbm->get("value") - 100), $db);
            if ($num == 2)
                $dba = new DBCollection("SELECT * FROM BasicMagicSpell WHERE id=" . ($dbbm->get("value") - 200), $db);
            if ($num == 3)
                $dba = new DBCollection("SELECT * FROM BasicTalent WHERE id=" . ($dbbm->get("value") - 300), $db);
            
            $str .= "Utilisez votre bonus de concentration pour tenter d'améliorer votre maitrise de l'action " . $dba->get("name") . " ?";
            $str .= "</td><td class='mainbgtitle'><input id='submitbt' type='submit' name='submitbt' value='" . localize("Valider") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "<input name='action' type='hidden' value='" . CONCENTRATION . "' />\n";
        }
        $str .= "</td></tr></table>";
        $str .= "</form>";
        
        /*
         * else
         * {
         * require_once(HOMEPATH."/factory/PlayerFactory.inc.php");
         * PlayerFactory::newATB($curplayer, $param, $db);
         * $curplayer->updateDB($db);
         * $str="<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>".localize("Vous avez un level up ")."</td></tr></table>";
         * }
         */
        return $str;
    }
}
?>
