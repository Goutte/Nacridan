<?php

class CQUse extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQUse($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        if ($curplayer->get("ap") < USE_OBJECT_AP) {
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour utiliser un objet.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
            $str .= localize("Utiliser") . " <select id='object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            
            $dbo = new DBCollection("SELECT Equipment.id,Equipment.name FROM Equipment LEFT JOIN EquipmentType ON EquipmentType.id=id_EquipmentType WHERE EquipmentType.id=8 AND id_Player=" . $id, $db);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre objet --") . "</option>";
            while (! $dbo->eof()) {
                $item[] = array(
                    localize($dbo->get("name")) . " (" . $dbo->get("id") . ")" => $dbo->get("id")
                );
                $dbo->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select></td><td>";
            
            $str .= "<select id='target' class='selector cqattackselectorsize' name='TARGET_ID'>";
            
            $gap = 0;
            $dbp = new DBCollection("SELECT id,name FROM Player WHERE status='PC' AND map=" . $map . " AND ABS(x-" . $xp . ")<=" . $gap . " AND ABS(y-" . $yp . ")<=" . $gap, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
            $item[] = array(
                localize("-- Personnage(s) sur votre zone --") => - 1
            );
            while (! $dbp->eof()) {
                if ($dbp->get("id") != $id) {
                    $item[] = array(
                        $dbp->get("name") => $dbp->get("id")
                    );
                } else {
                    $item[] = array(
                        localize("Sur moi") => $dbp->get("id")
                    );
                }
                $dbp->next();
            }
            
            $dbnpc = new DBCollection("SELECT id,racename FROM Player WHERE status='NPC' AND map=" . $map . " AND ABS(x-" . $xp . ")<=" . $gap . " AND ABS(y-" . $yp . ")<=" . $gap, $db, 0, 0);
            $item[] = array(
                localize("-- PNJ sur votre zone --") => - 1
            );
            while (! $dbnpc->eof()) {
                $item[] = array(
                    localize($dbnpc->get("racename")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
                );
                $dbnpc->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "'>";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select></td><td>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value='" . USE_OBJECT . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
