<?php

class CQHustle extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQHustle($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        // calcul du nombre de PA d'une bousculade
        $PA = $curplayer->getTimetoMove($db);
        
        if ($curplayer->get("ap") < ($PA + HUSTLE_AP)) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour bousculer quelqu'un. PAm " . $PA . " et PAh");
            $str .= "</td></tr></table>";
        } else {
            if (! isset($_POST["action"])) {
                $inbuilding = $curplayer->get("inbuilding");
                $room = $curplayer->get("room");
                $str = "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td  width='550px'>";
                $str .= localize("Bousculer") . " <select id='selector_id' class='selector cqattackselectorsize' name='TARGET_ID'>";
                $gap = 1;
                $item = array();
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez votre cible --") . "</option>";
                $dbp = new DBCollection("SELECT * FROM Player WHERE (inbuilding=0 or (inbuilding=" . $inbuilding . " AND room=" . $room . ")) AND (id_BasicRace < 5 or id_BasicRace=263 or level<3) AND inbuilding!= 0 AND disabled=0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 =" . $gap, $db, 0, 0);
                $item[] = array(
                    localize("-- Personnage à portée -- ") => - 1
                );
                while (! $dbp->eof()) {
                    if ($dbp->get("id") != $id && $curplayer->canSeePlayerById($dbp->get("id"), $db) && ! ($inbuilding != 0 && $dbp->get("id_BasicRace") >= 5)) {
                        if ($dbp->get("name") != "")
                            $item[] = array(
                                $dbp->get("name") => $dbp->get("id")
                            );
                        else
                            $item[] = array(
                                $dbp->get("racename") => $dbp->get("id")
                            );
                    }
                    $dbp->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value) {
                        if ($value == - 1)
                            $str .= "<optgroup class='group' label='" . $key . "' />";
                        
                        else
                            $str .= "<option value='" . $value . "'>" . $key . "</option>";
                    }
                }
                
                $str .= "</select></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='action' type='hidden' value='" . HUSTLE . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                if ($_POST["TARGET_ID"] == 0) {
                    $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
                    $str .= localize("Vous devez choisir une cible valide !") . "</td>";
                    $str .= "</tr></table>";
                } else {
                    $dbc = new DBCollection("SELECT name, racename FROM Player WHERE id=" . quote_smart($_POST["TARGET_ID"]), $db);
                    $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act&center=view" . "' target='_self'>";
                    $str .= "<table class='maintable bottomareawidth' ><tr class='mainbgtitle'><td  width='550px'>";
                    if ($dbc->get("name") != "")
                        $str .= localize("Echanger de place avec {name} ?", array(
                            "name" => $dbc->get("name")
                        )) . "</td>";
                    else
                        $str .= localize("Echanger de place avec {name} ?", array(
                            "name" => $dbc->get("racename")
                        )) . "</td>";
                    
                    $str .= "<td class='mainbgtitle'><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                    $str .= "</td></tr></table>";
                    $str .= "<input name='TARGET_ID' type='hidden' value='" . quote_smart($_POST["TARGET_ID"]) . "' />";
                    $str .= "<input name='action' type='hidden' value='" . HUSTLE . "' />";
                    $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    $str .= "</form>";
                }
            }
        }
        return $str;
    }
}
?>
