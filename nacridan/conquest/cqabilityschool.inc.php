<?php
DEFINE("STEP", 8);
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/BasicEquipment.inc.php");
require_once (HOMEPATH . "/factory/SchoolFactory.inc.php");

class CQAbilitySchool extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQAbilitySchool($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        
        if (isset($_POST["Buy"])) {
            if (! $this->nacridan->isRepostForm()) {
                if (isset($_POST["IdAbility"]) && isset($_POST["IdAbility"]))
                    SchoolFactory::learnAbility($this->curplayer, quote_smart($_POST["IdAbility"]), $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner un sortilège.");
            } else
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        if ($err == "" && $str == "") {
            
            // Enregistrement des compétences déjà apprises
            $dbe2 = new DBCollection("SELECT BasicAbility.id FROM BasicAbility LEFT JOIN Ability ON BasicAbility.id=Ability.id_BasicAbility WHERE Ability.id_Player=" . $id, $db);
            $always = array();
            while (! $dbe2->eof()) {
                $always[$dbe2->get("id")] = 1;
                $dbe2->next();
            }
            
            // Mise en page
            $target = "/conquest/conquest.php?center=abilityschool";
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'><b>" . localize('É C O L E &nbsp;&nbsp;&nbsp;D E S &nbsp;&nbsp;&nbsp;C O M P É T E N C E S') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            
            $dbe = new DBCollection("SELECT * FROM BasicAbility", $db);
            $array = array();
            while (! $dbe->eof()) {
                $enable = "disabled";
                if (! isset($always[$dbe->get("id")])) {
                    if (SchoolFactory::prerequisitesForLearnAbility($curplayer, $dbe->get("id"), $db))
                        $enable = "";
                    $checkbox = "<input type='radio'" . $enable . " name='IdAbility' value='" . $dbe->get("id") . "'>";
                    $name = "<b>" . localize($dbe->get("name")) . "</b><br/>";
                    $ecole = "<b>" . localize($dbe->get("school")) . "</b><br/>";
                    $price = $dbe->get("costPO");
                    $prerequis = "prérequis";
                    $array[] = array(
                        "0" => array(
                            $checkbox,
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $name,
                            "class='mainbgbody' align='left'"
                        ),
                        "2" => array(
                            $ecole,
                            "class='mainbgbody' align='left'"
                        ),
                        "3" => array(
                            $price,
                            "class='mainbgbody' align='right'"
                        ),
                        "4" => array(
                            $prerequis,
                            "class='mainbgbody' align='right'"
                        )
                    );
                }
                $dbe->next();
            }
            $str .= "</table>\n";
            $str .= createTable(5, $array, array(), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("Nom du sortilège"),
                    "class='mainbglabel' width='30%' align='left'"
                ),
                array(
                    localize("Ecole"),
                    "class='mainbglabel' width='30%' align='left'"
                ),
                array(
                    localize("Prix"),
                    "class='mainbglabel' width='15%' align='right'"
                ),
                array(
                    localize("Prerequis"),
                    "class='mainbglabel' width='20%' align='right'"
                )
            ), "class='maintable centerareawidth'");
            
            $str .= "<input id='Buy' type='submit' name='Buy' value='" . localize("Apprendre cette Compétence") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>\n";
        } else {
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=abilityschool' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
