<?php

class CQArena extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQArena($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $this->curplayer->get("id");
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        $lastarena = gmstrtotime($curplayer->get("lastarena"));
        
        if ((time() - $lastarena) < 3600 * 24 * 7) {
            $teleportEnabled = $lastarena + 3600 * 24 * 7;
            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Votre corps est encore trop faible pour subir une téléportation vers l'arène.") . "<br/>";
            $str .= localize("Vous devez attendre le : ") . date("Y-m-d H:i:s", $teleportEnabled);
            $str .= "</td></tr></table>";
        } else {
            if ($curplayer->get("ap") < ARENA_AP) {
                $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour vous cette Action.");
                $str .= "</td></tr></table>";
            } else {
                if ($curplayer->get("money") < ARENA_PRICE) {
                    $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                    $str .= localize("Vous n'avez pas assez les " . ARENA_PRICE . " Pièces d'Or nécessaires (PO)") . "</td>";
                    $str .= "</tr></table>";
                } else {
                    if (! isset($_POST["IdCity"])) {
                        $str = "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=map&bottom=arena' . "' target='_self'>\n";
                        $str .= "<table class='maintable centerareawidth'>\n";
                        $str .= "<tr>\n";
                        $str .= "<td class='mainbgtitle'><b>" . localize('A R È N E') . "</b>\n";
                        $str .= "</td>\n";
                        $str .= "</tr>\n";
                        $str .= "</table>\n";
                        $str .= "<table class='maintable' ><tr><td class='mainbgtitle' width='500px'>";
                        
                        $str .= localize("Lieu possible de téléportation :") . " <select id='Object' class='selector cqattackselectorsize' name='IdCity'>";
                        
                        $gap = 0;
                        $dbp = new DBCollection("SELECT City.id,City.name,City.x,City.y FROM City WHERE name LIKE 'Pilier%' ORDER BY City.id", $db, 0, 0);
                        
                        $str .= "<option value='0' selected='selected'>" . localize("-- Choisir une destination --") . "</option>";
                        $item = array();
                        while (! $dbp->eof()) {
                            if ($dbp->get("name") != "") {
                                $item[] = array(
                                    localize($dbp->get("name")) . " " . $dbp->get("x") . "," . $dbp->get("y") => $dbp->get("id")
                                );
                            }
                            $dbp->next();
                        }
                        
                        foreach ($item as $arr) {
                            foreach ($arr as $key => $value) {
                                if ($value == - 1)
                                    $str .= "<optgroup class='group' label='" . $key . "' />";
                                else
                                    $str .= "<option value='" . $value . "'>" . $key . "</option>";
                            }
                        }
                        $str .= "</select></td><td>";
                        $str .= "<input id='Teleport' type='submit' name='Teleport' value='" . localize("Lancer la Téléportation") . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                        $str .= "</td></tr></table>";
                    } else {
                        $dbp = new DBCollection("SELECT City.id,City.name,City.x,City.y FROM City WHERE id=" . quote_smart($_POST["IdCity"]), $db, 0, 0);
                        if (! $dbp->eof()) {
                            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act'>";
                            $str .= "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                            $str .= localize("Vous téléporter vers l'arène en : X={resx}, Y={resy}", array(
                                "resx" => $dbp->get("x"),
                                "resy" => $dbp->get("y")
                            )) . "<br/>";
                            $str .= localize("Cette action vous coûtera {price} PO", array(
                                "price" => ARENA_PRICE
                            ));
                            $str .= "</td></tr></table>";
                            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Continuer' />";
                            $str .= "<input name='ARENAX' type='hidden' value='" . $dbp->get("x") . "' />";
                            $str .= "<input name='ARENAY' type='hidden' value='" . $dbp->get("y") . "' />";
                            $str .= "<input name='action' type='hidden' value='" . ARENA . "' />";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $str .= "</form>";
                        } else {
                            $str = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
                            $str .= localize("Erreur, destination non valide !");
                            $str .= "</td></tr></table>";
                        }
                    }
                }
            }
        }
        return $str;
    }
}

?>