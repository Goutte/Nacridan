<?php

class CQAccount extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAccount($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $str = "";
        $err = "";
        
        if (isset($_GET["type"])) {
            $type = quote_smart($_GET["type"]);
        } else {
            $type = "pc";
        }
        $errmdp = "";
        $errmdp2 = "";
        $errmdp3 = "";
        $errmdp4 = "";
        
        $erremail = "";
        
        if (isset($_POST["deleteMember"])) {
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_Member=" . $this->nacridan->auth->auth["uid"], $db, 0, 0);
            while (! $dbp->eof()) {
                
                // Relatif au village et au bâtiment
                $dbb = new DBCollection("UPDATE Building SET id_Player=0 WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbc = new DBCollection("UPDATE City SET id_Player=0, money=0, captured=(case when captured=4 then 3 else 0 end) WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, 
                    false);
                $dbb = new DBCollection("DELETE FROM Building WHERE (id_BasicBuilding=11 or id_BasicBuilding> 14)and id_BasicBuilding != 28 AND id_Player=" . $dbp->get("id"), $db, 
                    0, 0, false);
                
                // Nettoyage de la bdd
                $dbt = new DBCollection("DELETE FROM Template WHERE id_Equipment IN (SELECT id FROM Equipment WHERE id_Player=" . $dbp->get("id") . ")", $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Equipment WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Detail WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Modifier WHERE id=" . $dbp->get("id_Modifier"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Upgrade WHERE id=" . $dbp->get("id_Upgrade"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Player WHERE id=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Ability WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM MagicSpell WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Talent WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM BM WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM Caravan WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                
                // Suppression des player controlés et des BM infligés
                $dbt = new DBCollection("DELETE FROM Player WHERE id_Owner=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM BM WHERE id_Player\$src=" . $dbp->get("id"), $db, 0, 0, false);
                
                $dbt = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Player=" . $dbp->get("id") . "", $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Guest=" . $dbp->get("id") . "", $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM TeamAskJoin WHERE id_Player=" . $dbp->get("id") . "", $db, 0, 0, false);
                
                // Suppression des mails - Tester les alias
                $dbt = new DBCollection("DELETE FROM MailSend WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM MailContact WHERE id_Player=" . $dbp->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM MailContact WHERE id_Player\$friend=" . $dbp->get("id"), $db, 0, 0, false);
                
                // Suppression des Alliances
                $dbt = new DBCollection("DELETE FROM PJAlliancePJ WHERE id_Player\$src=" . $dbp->get("id") . "", $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM PJAlliancePJ WHERE id_Player\$dest=" . $dbp->get("id") . "", $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM PJAlliance WHERE id_Player\$src=" . $dbp->get("id") . "", $db, 0, 0, false);
                $dbt = new DBCollection("DELETE FROM TeamAlliancePJ WHERE id_Player=" . $dbp->get("id") . "", $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM TeamRank WHERE id_Player=" . $dbp->get("id") . "", $db, 0, 0, false);
                
                // Si chef d'un ordre
                $dbs = new DBCollection("SELECT id,id_Player,name from Team WHERE id=" . $dbp->get("id_Team") . "", $db);
                if ($dbs->count() > 0) {
                    if ($dbs->get("id_Player") == $dbp->get("id")) {
                        // Envoi d'un message au membre de l'ordre
                        $dbu = new DBCollection("SELECT * FROM Player WHERE id_Team=" . $dbp->get("id_Team"), $db);
                        if ($dbu->count() > 0) {
                            while (! $dbu->eof()) {
                                $dest[$dbu->get("name")] = $dbu->get("name");
                                $dbu->next();
                            }
                            
                            require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                            MailFactory::sendSingleDestMsg($curplayer, localize("Information"), localize("L'ordre auquel vous apparteniez a été dissous."), $dest, $type, $err, $db, 
                                - 1, 0);
                            
                            $dbd = new DBCollection("DELETE FROM TeamRank WHERE id_Team=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbd = new DBCollection("DELETE FROM TeamRankInfo WHERE id_Team=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbd = new DBCollection("DELETE FROM TeamAskJoin WHERE id_Team=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbd = new DBCollection("DELETE FROM TeamBody WHERE id_Team=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbd = new DBCollection("DELETE FROM TeamAlliance WHERE id_Team\$dest=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbd = new DBCollection("DELETE FROM TeamAlliance WHERE id_Team\$src=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbd = new DBCollection("DELETE FROM Team WHERE id=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                            $dbu = new DBCollection("UPDATE Player SET id_Team=0 WHERE id_Team=" . $dbp->get("id_Team") . "", $db, 0, 0, false);
                        }
                    }
                }
                
                // Si chef d'un groupe de chasse
                $dbs = new DBCollection("SELECT * from FighterGroup WHERE id=" . $dbp->get("id_FighterGroup") . "", $db);
                $dest2 = array();
                if ($dbs->count() > 0) {
                    if ($dbs->get("id_Player") == $dbp->get("id")) {
                        // Envoi d'un message au membre de l'ordre
                        $dbu = new DBCollection("SELECT * FROM Player WHERE id_FighterGroup=" . $dbp->get("id_Fighter"), $db);
                        if ($dbu->count() > 0) {
                            while (! $dbu->eof()) {
                                $dest2[$dbu->get("name")] = $dbu->get("name");
                                $dbu->next();
                            }
                        }
                        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                        MailFactory::sendSingleDestMsg($curplayer, localize("Information"), localize("Le groupe de chasse auquel vous apparteniez a été dissous."), $dest2, $type, 
                            $err, $db, - 1, 0);
                        
                        $dbd = new DBCollection("DELETE FROM FighterGroupMsg WHERE id_FighterGroup=" . $dbp->get("id_FighterGroup") . "", $db, 0, 0, false);
                        $dbd = new DBCollection("DELETE FROM FighterGroup WHERE id=" . $dbp->get("id_FighterGroup") . "", $db, 0, 0, false);
                        $dbd = new DBCollection("DELETE FROM FighterGroupMsg WHERE id_FighterGroup=" . $dbp->get("id_FighterGroup") . "", $db, 0, 0, false);
                        $dbu = new DBCollection("UPDATE Player SET id_FighterGroup=0 WHERE id_FighterGroup=" . $dbp->get("id_FighterGroup") . "", $db, 0, 0, false);
                    }
                }
                
                $dbp->next();
            }
            
            $dbt = new DBCollection("DELETE FROM MemberDetail WHERE id_Member=" . $curplayer->get("id_Member"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM MemberOption WHERE id_Member=" . $curplayer->get("id_Member"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Member WHERE id=" . $curplayer->get("id_Member"), $db, 0, 0, false);
            
            $this->nacridan->sess->destroy();
            $this->nacridan->auth->logout();
            echo "<h1>" . localize("Votre compte a correctement été effacé.") . "</h1>";
            exit(0);
            /*
             * $str="<form name='form' method='POST' action='".CONFIG_HOST."' target='_self' enctype='multipart/form-data'>\n";
             * $str="<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
             * $str.=localize("Votre compte a correctement été effacé.");
             * $str.="<input type='submit' name='thatsallfolks' value='".localize("Good Bye")."'/>";
             * $str.="</td></tr></table></form>";
             */
        }
        
        if (isset($_POST["updatePasswd"])) {
            if (! $this->nacridan->isRepostForm()) {
                if (isset($_POST["pass1"]) && isset($_POST["pass2"])) {
                    if (quote_smart($_POST["pass1"]) == quote_smart($_POST["pass2"])) {
                        if (strlen($_POST["pass1"]) > 4) {
                            $dbm = new DBCollection("SELECT * FROM Member WHERE id=" . $this->nacridan->auth->auth["uid"], $db, 0, 0);
                            $dbu = new DBCollection("UPDATE Member SET password=\"" . quote_smart($_POST["pass1"]) . "\" WHERE id=" . $this->nacridan->auth->auth["uid"], $db, 0, 0, 
                                false);
                            $errmdp = "<span class='mainerror'><br>(" . localize("Mot de Passe mis à jour") . ")</span> <br>";
                            /**
                             * ***************************************************************************
                             */
                            /**
                             * ************** UPDATE FORUM PASSWD ****************************************
                             */
                            /**
                             * ***************************************************************************
                             */
                            
                            try {
                                $dbh = new PDO("mysql:host=" . MYSQLHOST . ";dbname=" . DBFORUM, LOGINFORUM, PASSWDFORUM);
                                $initUTF8 = $dbh->prepare('SET NAMES UTF8');
                                $initUTF8->execute();
                                
                                /**
                                 * * prepare the SQL statement **
                                 */
                                $stmt = $dbh->prepare("UPDATE flux_users SET password=:password WHERE username=:login");
                                
                                /**
                                 * * execute the prepared statement **
                                 */
                                $success = $stmt->execute(
                                    array(
                                        'password' => sha1($_POST["pass1"]),
                                        'login' => $dbm->get("login")
                                    ));
                                
                                if ($success)
                                    $errmdp .= "<span class='mainerror'> - (Le mot de passe du forum s'est également mis à jour).</span> <br>";
                                
                                /**
                                 * * close the database connection **
                                 */
                                $dbh = null;
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }
                            
                            /**
                             * ***************************************************************************
                             */
                            /**
                             * ************** UPDATE GG PASSWD *******************************************
                             */
                            /**
                             * ***************************************************************************
                             */
                            
                            try {
                                $dbh = new PDO("mysql:host=" . MYSQLHOST . ";dbname=" . DBGG, LOGINGG, PASSWDGG);
                                $initUTF8 = $dbh->prepare('SET NAMES UTF8');
                                $initUTF8->execute();
                                
                                /**
                                 * * prepare the SQL statement **
                                 */
                                $stmt = $dbh->prepare("UPDATE user SET password=:password WHERE username=:login");
                                
                                /**
                                 * * execute the prepared statement **
                                 */
                                $success = $stmt->execute(
                                    array(
                                        'password' => $_POST["pass1"],
                                        'login' => $dbm->get("login")
                                    ));
                                
                                if ($success)
                                    $errmdp .= "<span class='mainerror'> - (Le mot de passe du site annexe s'est également mis à jour).</span> <br>";
                                
                                /**
                                 * * close the database connection **
                                 */
                                $dbh = null;
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                            }
                        } else {
                            $errmdp = "<span class='mainerror'>(" . localize("Votre Mot de Passe doit au moins être égal à 5 caractères") . ")</span>";
                        }
                    } else {
                        $errmdp = "<span class='mainerror'>(" . localize("Les deux Mots de Passe doivent être identique") . ")</span>";
                    }
                } else {
                    $errmdp = "<span class='mainerror'>(" . localize("Les deux Mots de Passe doivent être identique") . ")</span>";
                }
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["updateEmail"])) {
            if (! $this->nacridan->isRepostForm()) {
                if (isset($_POST["email1"]) && isset($_POST["email2"])) {
                    if (quote_smart($_POST["email1"]) == quote_smart($_POST["email2"])) {
                        $validEmail = true;
                        
                        $dbp = new DBCollection(
                            "SELECT email FROM MemberDetail WHERE id_Member!=" . $curplayer->get("id_Member") . " AND email='" . quote_smart($_POST["email1"]) . "'", $db);
                        if (! $dbp->eof()) {
                            $validEmail = false;
                            $erremail = "<span class='mainerror'>(" . localize("Cette adresse E-mail a déjà été utilisée pour une inscription !") . ")</span>";
                        }
                        
                        if (filter_var($_POST["email1"], FILTER_VALIDATE_EMAIL) == false) {
                            $erremail = "<span class='mainerror'>(" . localize("Adresse email non valide !") . ")</span>";
                            $validEmail = false;
                        }
                        
                        if ($validEmail) {
                            $dbu = new DBCollection("UPDATE MemberDetail SET email='" . quote_smart($_POST["email1"]) . "' WHERE id_Member=" . $curplayer->get("id_Member"), $db, 0, 
                                0, false);
                            $erremail = "<span class='mainerror'>(" . localize("Adresse email mise à jour !") . ")</span>";
                        }
                    } else
                        $erremail = "<span class='mainerror'>(" . localize("Les deux Mots de Passe doivent être identique") . ")</span>";
                } else {
                    $erremail = "<span class='mainerror'>(" . localize("Les deux Mots de Passe doivent être identique") . ")</span>";
                }
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["updateMail"])) {
            if (! $this->nacridan->isRepostForm()) {
                $mailmsg = 0;
                $fightmsg = 0;
                $deadmsg = 0;
                
                if (isset($_POST["mailmsg"]))
                    $mailmsg = 1;
                if (isset($_POST["fightmsg"]))
                    $fightmsg = 1;
                if (isset($_POST["deadmsg"]))
                    $deadmsg = 1;
                
                $dbu = new DBCollection(
                    "UPDATE MemberOption SET mailMsg=" . $mailmsg . ",fightMsg=" . $fightmsg . ",deadMsg=" . $deadmsg . " WHERE id_Member=" . $this->nacridan->auth->auth["uid"], 
                    $db, 0, 0, false);
                $errmdp2 = "<span class='mainerror'>(" . localize("Modification(s) effectuée(s)") . ")</span>";
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["updateArea"])) {
            if (! $this->nacridan->isRepostForm()) {
                $lang = "fr";
                $zone = "Europe/Paris";
                if (isset($_POST["zone"]))
                    $zone = quote_smart($_POST["zone"]);
                if (isset($_POST["lang"]))
                    $lang = quote_smart($_POST["lang"]);
                
                $dbu = new DBCollection("UPDATE Member SET utc='" . $zone . "',lang='" . $lang . "' WHERE id=" . $this->nacridan->auth->auth["uid"], $db, 0, 0, false);
                $this->nacridan->auth->auth["utc"] = $zone;
                date_default_timezone_set($zone);
                
                $errmdp3 = "<span class='mainerror'>(" . localize("Modification(s) effectuée(s)") . ")</span>";
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($str == "") {
            $target = "/conquest/conquest.php?center=account";
            
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self' enctype='multipart/form-data'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('C O M P T E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $str .= "</table>\n";
            
            $str .= "<table class='maintable centerareawidth'><tr><td colspan=2 class='mainbglabel'>" . localize("Mot de Passe") . " " . $errmdp . "</td></tr>\n";
            $str .= "<tr><td class='mainbgtitle'>";
            $str .= localize("Nouveau Mot de Passe") .
                 ": </td><td class='mainbgtitle'><input type='password' name='pass1' size='30'> Uniquement des chiffres et des lettres.</td></tr> ";
            $str .= "<tr><td class='mainbgtitle'>";
            $str .= localize("Confirmation") . ": </td><td class='mainbgtitle'><input type='password' name='pass2' size='30'></td></tr> ";
            $str .= "</td></tr>";
            $str .= "</table>";
            $str .= "<input type='submit' name='updatePasswd' value='" . localize("Mettre à jour votre Mot de Passe") . "' />";
            
            $str .= "<br/><br/>\n";
            
            $str .= "<table class='maintable centerareawidth'><tr class='mainbglabel'><td colspan=3 >" . localize("Adresse email") . " " . $erremail . "</td></tr>\n";
            $str .= "<tr class='mainbgtitle'><td  width='150px'>";
            $str .= localize("Nouvelle adresse") .
                 ": </td><td class='mainbgtitle'><input type='text' name='email1' size='30'> </td> <td> Notez que certaines boites, comme hotmail, placent Nacridan en spam par défaut.</td></tr> ";
            $str .= "<tr class='mainbgtitle'><td >";
            $str .= localize("Confirmation") . ": </td><td ><input type='text' name='email2' size='30'></td><td></td> </tr> ";
            $str .= "</td></tr>";
            $str .= "</table>";
            $str .= "<input type='submit' name='updateEmail' value='" . localize("Mettre à jour votre adresse email") . "' />";
            
            $str .= "<br/><br/>\n";
            
            $dbs = new DBCollection(
                "SELECT utc,lang,mailMsg,fightMsg,deadMsg,view2d FROM Member LEFT JOIN MemberOption ON Member.id=id_Member WHERE id_Member=" . $this->nacridan->auth->auth["uid"], 
                $db);
            
            if (! $dbs->eof()) {
                $mailmsg = $dbs->get("mailMsg") ? "checked" : "";
                $fightmsg = $dbs->get("fightMsg") ? "checked" : "";
                $deadmsg = $dbs->get("deadMsg") ? "checked" : "";
                
                $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Mail") . " " . $errmdp2 . "</td></tr>\n";
                $str .= "<tr><td class='mainbgtitle'><input type='checkbox' name='mailmsg' " . $mailmsg . " value='1'>" .
                     localize("Me Prévenir par mail si mon Personnage reçoit un message") . "</td></tr>";
                $str .= "<tr><td class='mainbgtitle'><input type='checkbox' name='fightmsg' " . $fightmsg . " value='1'>" .
                     localize("Me Prévenir par mail si mon Personnage est impliqué dans un combat") . "</td></tr>";
                $str .= "<tr><td class='mainbgtitle'><input type='checkbox' name='deadmsg' " . $deadmsg . " value='1'>" .
                     localize("Me Prévenir par mail si mon Personnage meurt dans un combat") . "</td></tr>";
                $str .= "</table>\n";
                $str .= "<input type='submit' name='updateMail' value='" . localize("Mettre à jour") . "' /><br/><br/>";
            }
            
            // $str.="<table class='maintable centerareawidth'><tr><td class='mainbglabel'>".localize("Langue et Zone")." ".$errmdp3."</td></tr>\n";
            $str .= "<table class='maintable centerareawidth'><tr><td class='mainbglabel'>" . localize("Zone") . " " . $errmdp3 . "</td></tr>\n";
            $str .= "<tr><td class='mainbgtitle'><select name='zone' size='1' id='zone'>";
            
            $dbm = new DBCollection("SELECT * FROM Timezone ORDER BY name", $this->db);
            while (! $dbm->eof()) {
                $extra = $dbm->get("name") == $dbs->get("utc") ? "selected" : "";
                $str .= "<option value='" . $dbm->get("name") . "' " . $extra . ">" . $dbm->get("name") . "</option>\n";
                $dbm->next();
            }
            
            $str .= "</select> ";
            $str .= localize("Définissez votre zone horaire") . "</td></tr>";
            // $str.="<tr><td class='mainbgtitle'><select name='lang' size='1' id='zone'>";
            // $str.="<option value='fr' ".($dbs->get("lang")=="fr" ? "selected" : "").">Fran&ccedil;ais</option>";
            // $str.="<option value='en' ".($dbs->get("lang")=="en" ? "selected" : "").">English</option>";
            // $str.="</select> ";
            // $str.=localize("Langue par défaut")."</td></tr>";
            $str .= "<input type='hidden' name='lang' value='fr' />";
            $str .= "</table>\n";
            $str .= "<input type='submit' name='updateArea' value='" . localize("Mettre à jour") . "' />";
            
            $str .= "<br/><br/>\n";
            $str .= "<table class='maintable centerareawidth'><tr><td colspan=2 class='mainbglabel'>" . localize("Supprimer votre Compte") . "</td></tr>\n";
            $str .= "<tr><td class='mainbgtitle mainerror'><b>ATTENTION CETTE OPÉRATION EST IRRÉVERSIBLE !!! </b> La suppression de votre compte inclut la suppression de tous vos personnages !</td></tr> ";
            $str .= "</table>";
            $str .= "<input type='submit' name='deleteMember' value='" . localize("Supprimer Votre Compte") .
                 "' onclick=\"if (window.confirm('Êtes vous certain de vouloir supprimer votre compte ?')){return true;} else {return false;}\" />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
        }
        
        return $str;
    }
}
?>
