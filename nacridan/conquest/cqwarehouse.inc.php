<?php
DEFINE("STEP", 8);
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/BasicEquipment.inc.php");

class CQWareHouse extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $err;

    public function CQWareHouse($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
        
        if (isset($_POST["Take"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["IdBasic"]))
                    EquipFactory::takeWarehouseEquipment($this->curplayer, quote_smart($_POST["IdBasic"]), $this->nacridan->getCity()->get("id"), $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner un équipement à prendre.");
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if (isset($_POST["Sell"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["IdBasic"]) && quote_smart($_POST["PRICE"]) > 0)
                    EquipFactory::saleWarehouseEquipment($this->curplayer, quote_smart($_POST["IdBasic"]), quote_smart($_POST["PRICE"]), $this->err, $this->db);
                else {
                    if (! isset($_POST["IdBasic"]))
                        $this->err = localize("Vous devez d'abord sélectionner un équipement à vendre.");
                    else
                        $this->err = localize("Vous devez entrer un prix de vente valide.");
                }
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        /*
         * if(isset($_POST["RemoveSell"]))
         * {
         * if(!$this->nacridan->isRepostForm())
         * {
         * require_once(HOMEPATH."/factory/EquipFactory.inc.php");
         * if(isset($_POST["IdBasic"]))
         * EquipFactory::removeSaleWarehouseEquipment($this->curplayer,quote_smart($_POST["IdBasic"]), $this->err,$this->db);
         *
         * if(!isset($_POST["IdBasic"]))
         * $this->err=localize("Vous devez d'abord sélectionner un équipement.");
         * }
         * else
         * {
         * $err=eval(file_get_contents(HOMEPATH."/include/reposterror.inc.php"));
         * }
         * }
         */
        if (isset($_POST["Give"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["IdBasic"]))
                    EquipFactory::giveWarehouseEquipment($this->curplayer, quote_smart($_POST["IdBasic"]), $this->nacridan->getCity()->get("id"), $this->err, $this->db);
                else
                    $this->err = localize("Vous devez d'abord sélectionner un équipement à prendre.");
            } else {
                $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = $this->err;
        $str = "";
        
        if (isset($_GET["type"])) {
            $type = quote_smart($_GET["type"]);
        } else {
            $type = 1;
        }
        
        if ($err == "" && $str == "") {
            
            switch ($type) {
                case 1:
                    $cond = "Equipment.id_EquipmentType=1 OR Equipment.id_EquipmentType=2";
                    break;
                case 2:
                    $cond = "Equipment.id_EquipmentType!=1 AND Equipment.id_EquipmentType!=2 AND Equipment.id_EquipmentType!=8 AND Equipment.id_EquipmentType!=9 AND Equipment.id_EquipmentType!=12 AND Equipment.id_EquipmentType <20";
                    break;
                case 3:
                    $cond = "Equipment.id_EquipmentType=20 OR Equipment.id_EquipmentType=21 OR Equipment.id_EquipmentType=22 OR Equipment.id_EquipmentType=8 OR Equipment.id_EquipmentType=9";
                    break;
                case 4:
                    $cond = "Equipment.id_EquipmentType=23";
                    break;
            }
            /*
             * if(isset($_POST["order"]) && $_POST["order"]!="")
             * {
             * $order=quote_smart($_POST["order"]);
             * if(isset($_POST["orderprev"]) && $order==$_POST["orderprev"])
             * {
             * $order.=" DESC";
             * }
             * }
             * else
             * {
             * $order="EQ.id_EquipmentType";
             * }
             *
             */
            
            $target = "/conquest/conquest.php?center=warehouse&type=" . $type;
            $str = "<form id='formid' name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'><b>" . localize('E N T R E P Ô T') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fightevent'>" . $err . "</span> \n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbgtitle'>\n";
            $str .= "<a href='../conquest/conquest.php?center=warehouse&type=1' class='tabmenu'>" . localize('Armes') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=warehouse&type=2' class='tabmenu'>" . localize('Armures') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=warehouse&type=4' class='tabmenu'>" . localize('Outils') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=warehouse&type=3' class='tabmenu'>" . localize('Divers') . "</a> |\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            switch ($type) {
                case 1:
                    $str .= localize("Les Armes ");
                    break;
                case 2:
                    $str .= localize("Les Armures ");
                    break;
                case 3:
                    $str .= localize("Divers ");
                    break;
                case 4:
                    $str .= localize("Les ouils ");
                    break;
            }
            
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $modifier = new Modifier();
            $mission = new Mission();
            // profiler_start("SQLQuery");
            
            $dbe = new DBCollection(
                "SELECT zone, " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                     $modifier->getASRenamer("Modifier_BasicTemplate", "MD") . "," . $mission->getASRenamer("Mission", "MISS") .
                     ",mask,wearable, BasicEquipment.frequency AS frequency, BasicTemplate.frequency AS freq  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN BasicTemplate ON Equipment.id_BasicTemplate=BasicTemplate.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN Modifier_BasicTemplate ON id_Modifier_BasicTemplate=Modifier_BasicTemplate.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE (" .
                     $cond . ") AND EquipmentType.small='No' AND id_Warehouse=" . $this->nacridan->getCity()->get("id"), $db);
            $array = array();
            // profiler_stop("SQLQuery");
            
            while (! $dbe->eof()) {
                
                $eq->DBLoad($dbe, "EQ");
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                if ($eq->get("id_BasicTemplate") != 0) {
                    $modifier->DBLoad($dbe, "MD");
                    $modifier->updateFromTemplateLevel($eq->get("templateLevel"));
                    $eqmodifier->addModifObj($modifier);
                }
                $eqmodifier->initCharacStr();
                
                $checkbox = "<input type='radio' name='IdBasic' value='" . $eq->get("id") . "'>";
                $name = "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                if ($eq->get("extraname") != "") {
                    $tlevel = $eq->get("templateLevel");
                    $name .= " " . "<span class='template'>" . localize($eq->get("extraname")) . "(" . $tlevel . ")</span>";
                }
                $name .= "<br/>" . localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")<br/>";
                
                if ($eq->get("id_BasicEquipment") != 60 and $eq->get("id_BasicEquipment") != 61 and $eq->get("id_BasicEquipment") != 62 and $eq->get("id_BasicEquipment") != 58 and
                     $eq->get("id_BasicEquipment") != 57 and $eq->get("id_BasicEquipment") != 56) {
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0") {
                            $name .= " " . localize($key) . ": " . $tmp . " |";
                        }
                    }
                }
                $price = 0;
                $sellprice = $eq->get("sell");
                if ($sellprice == 0)
                    $sellprice = "Non";
                else
                    $sellprice .= " PO";
                if ($eq->get("extraname") != "")
                    $price = floor(1000 * (($eq->get("templateLevel")) * ($eq->get("templateLevel"))) / $dbe->get("freq"));
                
                $price += floor(100 * ($eq->get("level") * $eq->get("level") + 1) / $dbe->get("frequency"));
                $zone = $dbe->get("zone");
                $array[] = array(
                    "0" => array(
                        $checkbox,
                        "class='mainbgbody' align='center'"
                    ),
                    "1" => array(
                        $name,
                        "class='mainbgbody' align='left'"
                    ),
                    "2" => array(
                        $zone,
                        "class='mainbgbody' align='center'"
                    ),
                    "3" => array(
                        $sellprice,
                        "class='mainbgbody' align='center'"
                    ),
                    "4" => array(
                        $price,
                        "class='mainbgbody' align='center'"
                    )
                );
                
                $dbe->next();
            }
            $str .= "</table>\n";
            
            $str .= createTable(5, $array, array(), 
                array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("Nom et caractéristiques"),
                        "class='mainbglabel' width='55%' align='center'",
                        "EQ.name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Nombre de Zone(s)"),
                        "class='mainbglabel' width='10%' align='center'",
                        "zone",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("En vente"),
                        "class='mainbglabel' width='15%' align='center'",
                        "sellprice",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Prix de base"),
                        "class='mainbglabel' width='15%' align='center'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), 

                "class='maintable centerareawidth'", "formid", "order");
            
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='615px'>";
            $str .= "<input id='Take' type='submit' name='Take' value='" . localize("Prendre cet objet") . "' /></br></table>";
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='615px'>";
            $str .= "<input id='Sell' type='submit' name='Sell' value='" . localize("Mettre en vente cet objet") . "' />";
            $str .= " à " . " <input type='text' name='PRICE' value'' maxlength=9 size=10/> " . localize("PO");
            $str .= "<br/></table>";
            $str .= "<input id='RemoveSell' type='submit' name='RemoveSell' value='" . localize("Supprimer de la vente cet objet") . "' />";
            $str .= "<br/></table>";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>\n";
            $str .= "<br>";
            
            // -------- POSER UN OBJET
            
            $str .= "<form name='form'  method='POST' target='_self'>";
            
            $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
            $str .= localize("Déposer dans l'entrepôt") . " <select id='Object' class='selector cqattackselectorsize' name='IdBasic'>";
            
            $gap = 0;
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE weared=\"No\" AND id_Player=" . $id, $db, 0, 0);
            
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Équipement à déposer ?--") . "</option>";
            while (! $dbp->eof()) {
                if ($dbp->get("name") != "") {
                    $item[] = array(
                        localize($dbp->get("name")) . " " . localize("Niveau") . " " . $dbp->get("level") . " (" . $dbp->get("id") . ")" => $dbp->get("id")
                    );
                }
                $dbp->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select></td><td>";
            $str .= "<input id='Give' type='submit' name='Give' value='" . localize("Déposer") . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
            
            // FIN poser un objet
        } else {
            if ($str == "") {
                $str = "<form method='POST' action='" . CONFIG_HOST . '/conquest/conquest.php?center=warehouse&type=' . $type . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        
        return $str;
    }
}
?>
