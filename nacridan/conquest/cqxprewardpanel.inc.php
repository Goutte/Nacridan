<?php
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

class CQXPRewardPanel extends HTMLObject
{

    public $nacridan;

    public $players;

    public $curplayer;

    public $db;

    public $style;

    public function CQXPRewardPanel($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        $this->style = $style;
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $curplayer = $this->curplayer;
        $id = 0;
        
        
        
        if (isset($_POST['createconfirm'])) // ------------------- CREATION -----------------
{
            if ($_POST['createconfirm'] == 'yes') {
                require_once (HOMEPATH . "/class/Mission.inc.php");
                require_once (HOMEPATH . "/class/MissionReward.inc.php");
                
                $mission = new Mission();                
                $mission->set("PlayerList", $_POST["ID_LIST"]);                
                $mission->set("name", base64_decode($_POST["TITLE"]));
                $mission->set("content", base64_decode($_POST["CONTENT"]));
                $mission->set("date", gmdate("Y-m-d h:m:s"));
                $mission->set("id_BasicMission", 100); // Mission personnalisée avec MJ                
                $mission->updateDB($db);
                
                $missionReward = new MissionReward();                
                $missionReward->set("idMission", $mission->get("id"));                
                $missionReward->set("xpStop", $_POST["XP"]);                
                $missionReward->updateDB($db);
                
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>La récompense a été distribuée. TODO distribuer vraiment</td></tr>";                
                $str .= "</table>";
            } else {
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>La récompense a été distribuée.</td></tr>";
                $str .= "</table>";
            }
            return $str;
        } elseif (isset($_POST['check'])) // --------------VERIFICATION DE L'ANNONCE
{
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE RÉCOMPENSE') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody' colspan='3'>Cette récompense convient-elle ?</td></tr>";
            $str .= "<tr class='mainbgbody' >";            
            $str .= "<td width='100px'>" . gmdate("d-m-Y") . "</td>";
            $str .= "<td width='250px' >*** <b>" . stripslashes($_POST["title"]) . "</b> ***</td>";
            $str .= "</tr>";            
            
            $content = stripslashes($_POST["content"]);
            $content = nl2br(bbcode($content));            
            $str .= "<tr class='mainbgbody' >";
            $str .= "<td colspan=3>" . $content . "</td>";
            $str .= "</tr>";
            
            if(is_numeric($_POST["xp"])){
            	$str .= "<tr class='mainbgbody' >";
	            $str .= "<td colspan=3> Quantité totale de PX pour le groupe : <b>" . $_POST["xp"] . "</b></td>";
	            $str .= "</tr>";
            }
            $str .= "<tr class='mainbgtitle' ><td colspan=3>Liste des participants</td></tr>"; 
				$idarray = explode(",",$_POST['idlist']);
				foreach($idarray as $participant_id){
					if (is_numeric($participant_id)) {
        				$participant = new Player();
        				$participant->load($participant_id, $db);
        				$str .= "<tr class='mainbgbody' ><td colspan=3>".$participant->get('name')." (".$participant_id.") niv ".$participant->get('level')."</td></tr>";
    				}
				}            
            
            $str .= "<input type='hidden' name='TITLE' value='" . base64_encode($_POST['title']) . "'/>";
            $str .= "<input type='hidden' name='CONTENT' value='" . base64_encode($_POST['content']) . "'/>";
            $str .= "<input type='hidden' name='ID_LIST' value='" . $_POST['idlist'] . "'/>";
            $str .= "<input type='hidden' name='XP' value='" . $_POST['xp'] . "'/>";
            $str .= "<input type='hidden' name='createconfirm' value='yes'/>";
            $str .= "<td><input type='submit' name='ok' value='Ok'/></td></tr>";
            $str .= "</table></form>";
            
            return $str;
        } elseif (isset($_POST['create'])) // ------- FORMULAIRE DE CREATION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE RÉCOMPENSE') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<tr><td class='mainbgbody' ><label for='title'>Titre : </label><input type='text' maxlength='30' name='title'  id='title' /></td></tr>";
            $str .= "<tr><td class='mainbgbody' ><label for='px'>Nombre total de PX pour le groupe : </label><input type='text' maxlength='30' name='xp'  id='px' /></td></tr>";
            $str .= "<tr><td class='mainbgbody' ><label for='idlist'>Liste des participants sous la forme id1,id2, id3 etc. : </label><input type='text' maxlength='200' name='idlist'  id='idlist' /></td></tr>";            
            $str .= "<tr><td class='mainbgbody' align='left'><label for='content'> Description courte de la mission, au minimum mettre le lien vers le récit dans le forum : </label> </td></tr>";
            $str .= "";
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' > <textarea rows=6 cols=60 name='content' id='content' /> </textarea></td>";
            $str .= "</tr>";
            $str .= "<td><input type='submit' name='check' value='Créer'/></td></tr>";                        

            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
        } elseif (isset($_POST['deleteconfirm'])) // --------------------- SUPPRESSION
{
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            if ($_POST['deleteconfirm'] == 'yes') {
                $newslist = unserialize(stripslashes($_POST['newslist']));
                foreach ($newslist as $id) {
                    $dbd = new DBCollection("DELETE FROM News WHERE id=" . $id, $db, 0, 0, false);
                }
                $str .= "<tr><td>Les annonces sélectionnées ont normalement été supprimées.</td></td>";
            } else
                $str .= "<td><td>Rien n'a été changé.</td></td>";
            
            $str .= "</table>";
            return $str;
        } elseif (isset($_POST['delete'])) // --------------- CONFIRMATION DE LA SUPPRESSION
{
            
            $newslist = unserialize(stripslashes($_POST['newslist']));
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td class='mainbgbody'>Êtes-vous sûr de vouloir supprimer ces news ?</td>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='deleteconfirm' value='yes' id='yes' />" . "<label for='yes'>Oui, suppression.</label></td></tr>";
            $str .= "<tr><td class='mainbgbody'>" . "<input type='radio' name='deleteconfirm' value='no' id='no' />" . "<label for='no'>Non, on garde.</label></td></tr>";
            $str .= "</table>";
            
            $newsidlist = array();
            
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('id') . "</td>\n";
            $str .= "<td class='mainbglabel' width='100px' align='center'>" . localize('type') . "</td>\n";
            $str .= "<td class='mainbglabel' width='200px' align='center'>" . localize('Auteur') . "</td>\n";
            $str .= "<td class='mainbglabel' align='center'>" . localize('Date') . "</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            foreach ($newslist as $arr) {
                if (array_key_exists("news" . $arr["id"], $_POST)) {
                    $str .= "<tr><td class='mainbgbody' width='80px' align='left'> " . $arr["id"] . " </td>\n";
                    $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["type"] . " </td>\n";
                    $str .= "<td class='mainbgbody' width='200px' align='left'>" . $arr["author"] . " </td>";
                    $str .= "<td class='mainbgbody'  align='left'> " . $arr["date"] . " </td>";
                    $str .= "</tr>\n";
                    
                    $newsidlist[] = $arr["id"];
                }
            }
            $str .= "</table>";
            
            $str .= "<input type='hidden' name='newslist' value=" . serialize($newsidlist) . "/>";
            $str .= "<input type='submit' name='ok' value='Ok'/>";
            $str .= "</form>";
            return $str;
        } else // ---------------- LISTE des Récompenses -------------
{
            
            $dbm = new DBCollection("SELECT * FROM Mission WHERE id_BasicMission=100", $db);
            
            $data = array();
            
            while (! $dbm->eof()) {
                
                
                $data[] = array(
                    'id' => $dbm->get('id'),
                    "title" => $dbm->get('name'),                    
                    "date" => $dbm->get("date")
                );
                $dbm->next();
            }
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='3' class='mainbgtitle'><b>" . localize('LISTES DES ANCIENNES RÉCOMPENSES') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('id') . "</td>\n";
            $str .= "<td class='mainbglabel' width='80px' align='center'>" . localize('Title') . "</td>\n";            
            $str .= "<td class='mainbglabel' align='center'>" . localize('Date') . "</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>";
            
            foreach ($data as $arr) {
                $str .= "<tr><td class='mainbgbody' width='27px' align='center'> <input type='checkbox' name='news" . $arr["id"] . "' id='news" . $arr["id"] . "' /> </td>";
                $str .= "<td class='mainbgbody' width='80px' align='left'> " . $arr["id"] . " </td>\n";
                $str .= "<td class='mainbgbody' width='100px' align='left'> " . $arr["title"] . " </td>\n";                
                $str .= "<td class='mainbgbody'  align='left'> " . $arr["date"] . " </td>";
                $str .= "</tr>\n";
            }
            $str .= "</table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td><input type='submit' name='delete' value='Supprimer'/></td>";
            
            $str .= "</table>";
            $str .= "<input type='hidden' name='newslist' value='" . serialize($data) . "'/>";
            
            $str .= "</form>";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UNE RÉCOMPENSE') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<td><input type='submit' name='create' value='Créer'/></td></tr>";
            $str .= "</table>";
            $str .= "</form>";
            return $str;
        }
    }
}
?>

 
      
