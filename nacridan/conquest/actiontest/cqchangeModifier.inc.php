<?php

class CQChangeModifier extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQChangeModifier($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["OBJECT_ID"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='650px'>";
                $item = array();
                $str .= "Changer ";
                $str .= "<select class='selector cqattackselectorsize' name='OBJECT_ID'>";
                $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une carac --") . "</option>";
                $str .= "<option value='hp'> HP </option>";
                $str .= "<option value='strength'> Force </option>";
                $str .= "<option value='dexterity'> Dextérité </option>";
                $str .= "<option value='speed'> Vitesse </option>";
                $str .= "<option value='magicSkill'> MM </option>";
                $str .= "</select>";
                
                $str .= " D6 ";
                $str .= "<input type='textbox' name='D6' />";
                $str .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                if ($_POST["OBJECT_ID"] == "hp") {
                    $curplayer->setModif("hp", DICE_ADD, $_POST["D6"]);
                    $curplayer->set("currhp", $_POST["D6"]);
                    $curplayer->set("hp", $_POST["D6"]);
                } else
                    $curplayer->setModif($_POST["OBJECT_ID"], DICE_D6, $_POST["D6"]);
                
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                PlayerFactory::initBM($curplayer, $curplayer->getObj("Modifier"), $param, $db, 0);
                $curplayer->updateDBr($db);
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("La carac a été modifié.");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>