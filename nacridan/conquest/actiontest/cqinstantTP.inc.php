<?php
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");

class CQInstantTP extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQInstantTP($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["NEWX"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "<tr><td class='mainbgtitle'><label for='target_id' >Identifiant du perso à téléporter</label></td><td><input type='textbox' name='target_id' value='" .
                     $curplayer->get("id") . "' /></td></tr>";
                $str .= "<tr  class='mainbgtitle'><td>Nouvelle position en X ?</td>";
                $str .= "<td><input type='textbox' name='NEWX' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Nouvelle position en Y ?</td>");
                $str .= "<td><input type='textbox' name='NEWY' /></td></tr>";
                $str .= "<tr><td colspan=2 class='mainbgtitle' width='550px'>";
                $str .= localize("ATTENTION AUCUNE RESTRICTION--VOUS POUVEZ SORTIR DE LA CARTE");
                $str .= "</td></tr>";
                $str .= "<tr><td colspan=2 class='mainbgtitle' width='550px'>";
                $str .= "<label for='DEL_FEUFOL' >&nbsp;&nbsp;&nbsp;Supprimer Feu Fol?</label><input type='checkbox' name='DEL_FEUFOL' value='1' />&nbsp;&nbsp;&nbsp;&nbsp;
                    <label for='DEL_MALUS' >Supprimer Malus?</label><input type='checkbox' name='DEL_MALUS' value='1' />";
                $str .= "<label for='MODE_ANIMATION' >&nbsp;&nbsp;&nbsp;Mode Animation?</label><input type='checkbox' name='MODE_ANIMATION' value='1' /></td></tr>";
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } elseif (isset($_POST["NEWX"])) {
                $room = 0;
                $inbuilding = 0;
                
                $isDeleteFeuFol = isset($_POST["DEL_FEUFOL"]) ? quote_smart($_POST["DEL_FEUFOL"]) : 0;
                $isDeleteMalus = isset($_POST["DEL_MALUS"]) ? quote_smart($_POST["DEL_MALUS"]) : 0;
                $isModeAnimation = isset($_POST["MODE_ANIMATION"]) ? quote_smart($_POST["MODE_ANIMATION"]) : 0;
                
                $dbc = new DBCollection("SELECT * FROM Building  WHERE Building.x=" . quote_smart($_POST["NEWX"]) . " and Building.y=" . quote_smart($_POST["NEWY"]), $db);
                if (! $dbc->eof()) {
                    $room = ENTRANCE_ROOM;
                    $inbuilding = $dbc->get("id");
                }
                
                $id = quote_smart($_POST["target_id"]);
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($id, $db);
                
                if ($isDeleteMalus == 1) {
                    if ($player->get("state") == "creeping") {
                        $array_result = PlayerFactory::getListIdBolas($player, $db);
                        foreach ($array_result as $i => $value) {
                            $idBolas = $value;
                            $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $player->get("id") . " AND name='A terre'", $db, 0, 0, false);
                            $dbe = new DBCollection("SELECT name FROM Equipment WHERE id=" . $idBolas, $db);
                            $cond = "";
                            if ($dbe->get("name") == "Branche d'Etranglesaule")
                                $cond = ",id_BasicEquipment = 507, id_EquipmentType=33 ";
                            $dbe = new DBCollection(
                                "UPDATE Equipment set x=" . $player->get("x") . ", y=" . $player->get("y") . ", room=" . $player->get("room") . ", inbuilding=" .
                                     $player->get("inbuilding") . ", state='Ground', id_Player=0 " . $cond . " WHERE id=" . $idBolas, $db, 0, 0, false);
                        }
                        $player->set("state", "walking");
                    }
                    PlayerFactory::deleteAllBM($player, $param, $db, 1, $isDeleteFeuFol);
                }
                
                $player->set("x", quote_smart($_POST["NEWX"]));
                $player->set("y", quote_smart($_POST["NEWY"]));
                $player->set("room", $room);
                $player->set("inbuilding", $inbuilding);
                $player->set("modeAnimation", $isModeAnimation);
                
                $player->updateHidden($db);
                $player->updateDB($db);
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize(
                    "Okey vous vous trouvez normalement en " . $_POST["NEWX"] . "," . $_POST["NEWY"] .
                         ", rechargez la vue pour voir le résultat, si vous êtes près de la destination de la cible.");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>