<?php

class CQCreateMonster extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQCreateMonster($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["idracePNJ"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "Entrez l'identifiant du perso qui contrôlera de ce monstre. Choisissez 0 si vous le donnez à l'IA.</td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='controlPNJ' value='0' /></td></tr>";
                $str .= "</td></tr>";
                
                $str .= "<tr class='mainbgtitle'><td>Facultatif - Entrez le nom de ce PNJ. Seuls les PNJ avec un nom peuvent recevoir des messages.</td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='PNJname' value='' /></td></tr>";
                $str .= "</td></tr>";
                
                $dbr = new DBCollection("SELECT name,id FROM BasicProfil order by name asc", $db, 0, 0);
                $str .= "<tr><td class='mainbgtitle' width='550px'><label for='idracePNJ' >Race du PNJ à créer</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='idracePNJ'>";
                while (! $dbr->eof()) {
                    $str .= "<option value='" . $dbr->get("id") . "'>" . $dbr->get("name") . "</option>";
                    $dbr->next();
                }
                $str .= "</select></td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='lvl' >Niveau du PNJ</label></td>\n";
                $str .= "<td class='mainbglabel'  align='left'><select name='lvl'>";
                for ($j = 1; $j <= 200; $j ++) {
                    $str .= "<option value='" . $j . "'>" . $j . "</option>";
                }
                $str .= "</select></td></tr>";
                
                $xfinal = $xp + 1;
                $yfinal = $yp;
                
                $str .= "<tr><td class='mainbgtitle'>ATTENTION le PNJ va apparaitre en X=" . $xfinal . "/Y=" . $yfinal .
                     " </td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                
                $npc = PNJFactory::getPNJFromLevel($_POST["idracePNJ"], $_POST["lvl"], $db);
                $xfinal = $xp + 1;
                $yfinal = $yp;
                $npc->set("x", $xfinal);
                $npc->set("y", $yfinal);
                $npc->set("map", $map);
                $npc->set("hidden", $curplayer->get("hidden"));
                
                if ($_POST["controlPNJ"] != 0) {
                    $dbmaster = new DBCollection("SELECT name,id,id_Member,status FROM Player WHERE id=" . quote_smart($_POST["controlPNJ"]), $db, 0, 0);
                    if ($dbmaster->count() > 0) {
                        $npc->set("id_Member", $dbmaster->get("id_Member"));
                        $npc->set("status", $dbmaster->get("status"));
                        $npc->set("resurrectid", 0);
                        $npc->set("authlevel", - 1);
                        $npc->set("name", $npc->get("racename"));
                    }
                }
                
                // $npc->set("currhp",1);
                $npc->set("playATB", gmdate(time() - 20 * date("I")));
                // $npc->set("id_Member", $curplayer->get("id_Member"));
                if ($npc->get("status") == "NPC") {
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                } else {
                    $upgrade = new Upgrade();
                    $npc->setObj("Upgrade", $upgrade, true);
                }
                $npc->updateDBr($db);
                $this->errorDB($npc->errorNoDB());
                // $npc->set("id", $id);
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                PlayerFactory::NewATB($npc, $param, $db);
                
                $dbu = new DBCollection("UPDATE Player SET name='" . quote_smart($_POST["PNJname"]) . "' WHERE id=" . $npc->get("id"), $db, 0, 0, false);
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize(
                    " Okey vous avez normalement un " . ($npc->get("status") == "NPC" ? "PNJ" : "PJ") . " de niveau " . $_POST["lvl"] . " en X=" . $xfinal . "/Y=" . $yfinal .
                         ", rechargez la vue pour le voir.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion PNJ/Equipement (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau PNJ/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
