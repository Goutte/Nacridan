<?php

class CQEmptyBag extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQEmptyBag($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["EMPTY"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='650px'>";
                $str .= "Tous les objets de l'inventaire seront posés au sol";
                $str .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='EMPTY' type='hidden' value='EMPTY' />\n";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                // ."
                $dbc = new DBCollection("UPDATE Equipment SET state='Ground' , x=" . $xp . ", y=" . $yp . " WHERE id_Player=" . $id . " AND state='Carried' AND weared='No'", $db, 0, 0, false);
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='650px'>";
                $str .= "Tous les objets sont au sol";
                $str .= "</table>";
            }
            return $str;
        }
    }
}
?>