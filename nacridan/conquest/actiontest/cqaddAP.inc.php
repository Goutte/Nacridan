<?php

class CQAddAP extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAddAP($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["ADDAP"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle'><label for='target_id' >Identifiant du perso qui recevra les PA</label></td><td><input type='textbox' name='target_id' value='" . $curplayer->get("id") . "' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Combien de PA voulez vous lui donner ?");
                $str .= "</td><td><input type='textbox' name='ADDAP' />";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $dbb = new DBCollection("UPDATE Player SET ap=" . quote_smart($_POST["ADDAP"]) . " WHERE id=" . quote_smart($_POST["target_id"]), $db, 0, 0, false);
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Le perso ciblé a normalement " . $_POST["ADDAP"] . " PA.");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>