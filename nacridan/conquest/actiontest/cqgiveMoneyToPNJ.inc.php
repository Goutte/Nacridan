<?php

class CQGiveMoneyToPNJ extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQGiveMoneyToPNJ($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["owner_id"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                
                $str .= "<tr><td class='mainbgtitle' width='550px'><label for='amount' >Combien de PO</label></td><td><input type='textbox' name='amount' value='0' /></td></tr>";
                
                $str .= "<tr><td class='mainbgtitle'><label for='owner_id' >Identifiant du perso</label></td><td><input type='textbox' name='owner_id' value='0' /></td></tr>\n";
                
                $str .= "<tr><td class='mainbgtitle'> Ces PO vont être offerts au perso sélectionné </td><td class='mainbglabel'  align='left'> <input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $owner = new Player();
                $owner->load($_POST["owner_id"], $db);
                
                $owner->set("money", $owner->get("money") + $_POST["amount"]);
                
                $owner->updateDBr($db);
                $this->errorDB($owner->errorNoDB());
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= localize("Okey vous avez normalement donné " . $_POST["amount"] . " à un perso.");
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion bâtiment (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau perso/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
