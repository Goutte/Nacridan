<?php

class CQLevelUp extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQLevelUp($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["LEVEL_UP"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>" . localize("Passer au niveau suivant ? ") . "</td><td class='mainbgtitle' align='center'>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='" . localize("Valider") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<input name='LEVEL_UP' type='hidden' value='LEVEL_UP' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                $curplayer->set("levelUp", 1);
                
                $curplayer->set("xp", getXPLevelUP($curplayer->get("level")));
                $curplayer->updateDB($db);
                $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle' width='550px'>" . localize("Vous avez un level up ") . "</td></tr></table>";
            }
            
            return $str;
        }
    }
}
?>
