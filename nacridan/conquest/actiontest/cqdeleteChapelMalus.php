<?php

class CQDeleteChapelMalus extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQDeleteChapelMalus($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["target_id"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "<tr><td class='mainbgtitle'><label for='target_id' >Identifiant du perso que vous souhaiter libérer du malus de la Chapelle</label></td><td><input type='textbox' name='target_id' value='" . $curplayer->get("id") . "' /></td></tr>";
                
                $str .= "<tr><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } elseif (isset($_POST["target_id"])) {
                $dbb = new DBCollection("SELECT * FROM BM WHERE id_StaticModifier_BM=19 AND id_Player=" . quote_smart($_POST["target_id"]), $db, 0, 0);
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                if ($dbb->count() == 0)
                    $str .= localize("Ce personnage ne possède pas de malus de chapelle.");
                else {
                    $dbd = new DBCollection("DELETE FROM BM WHERE id_StaticModifier_BM=19 AND id_Player=" . quote_smart($_POST["target_id"]), $db, 0, 0, false);
                    require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                    PlayerFactory::initBM($curplayer, $curplayer->getObj("Modifier"), $param, $db, 0);
                    $curplayer->updateDBr($db);
                    $str .= localize("La cible est débarassé du malus de chapelle.");
                }
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>