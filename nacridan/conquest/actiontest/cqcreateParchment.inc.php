<?php

class CQCreateParchment extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQCreateParchment($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            if (! isset($_POST["create_parchment"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= "Entrez l'identifiant du perso qui possèdera ce parchemin. Choisissez 0 si vous le voulez au sol.</td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='controlEquip' value='0' /></td></tr>";
                $str .= "</td></tr>";
                
                $str .= "<tr><td class='mainbgtitle' width='550px'>";
                $str .= "Entrez le nom du \"Parchemin\", cela peut être un autre type d'objet qui contient de l'écriture: pierre de runes, écorce gravée, carte, page de livre, etc. </td>";
                
                $str .= "<td class='mainbglabel'  align='left'>";
                $str .= "<input type='textbox' name='name' value='Parchemin' /></td></tr>";
                $str .= "</td></tr>";
                
                $str .= "<tr class='mainbgtitle'><td colspan=2>Entrez le contenu du parchemin (le bbcode fonctionne)</td></tr>";
                
                $str .= "<tr class='mainbgtitle'><td colspan=2  align='left'>";
                $str .= "<textarea id='content' style='width:600px;' name='content' rows='9' cols='40'></textarea></td></tr>";
                $str .= "</td></tr>";
                
                
                $str .= "<tr><td class='mainbglabel'  align='left'><input id='submitbt' type='submit' name='submitbt' value='Invocation!' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "<input name='create_parchment' type='hidden' value='go' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                                
                $parchment = new Equipment();                                
                $parchment->set("x", $xp);
                $parchment->set("y", $yp);
                $parchment->set("map", $map);
                $parchment->set("name", $_POST["name"]);
                $parchment->set("id_BasicEquipment", 600);
                $parchment->set("description", $_POST["content"]);
                $parchment->set("id_EquipmentType", 45);
                $parchment->set("date", gmdate("Y-m-d H:i:s"));
                
                
                if ($_POST["controlEquip"] != 0) {                    
                    $parchment->set("id_Player", $_POST["controlEquip"]);
                    $parchment->set("state", 'Carried');
                    $strWhere = "en possession du perso avec id=".quote_smart($_POST["controlEquip"]);                    
                }
                else{
                	  $parchment->set("state", 'Ground');
                	  $strWhere = "en X=".$xp."/Y=".$yp;
                }
                	
                
                $parchment->updateDBr($db);
                $this->errorDB($parchment->errorNoDB());
                
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle'>";
                $str .= " Okey vous avez normalement un parchemin ".$strWhere;
                $str .= "</td></tr>	</table>";
            }
            return $str;
        }
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion PNJ/Equipement (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau PNJ/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
