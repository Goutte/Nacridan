<?php
require_once (HOMEPATH . "/lib/GraphDijkstra.inc.php");

class CQCalcShortWay extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQCalcShortWay($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("authlevel") > 2) {
            
            if (! isset($_POST["DestX"])) {
                $str = "<form name='form'  method='POST'  target='_self'>";
                $str .= "<table class='maintable'>";
                $str .= "<tr><td class='mainbgtitle'><label for='target_id' >Identifiant du perso qui doit se déplacer</label></td><td><input type='textbox' name='target_id' value='" .
                     $curplayer->get("id") . "' /></td></tr>";
                $str .= "<tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Ou doit-il aller ?   X:");
                $str .= "<input type='textbox' name='DestX' />&nbsp;&nbsp;&nbsp;Y: <input type='textbox' name='DestY' /></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</td></tr></table>";
                $str .= "</form>";
            } else {
                
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($_POST["target_id"], $this->db);
                
                $graph = new GraphDijkstra($player, 5, $this->db);
                
                $str = "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
                $str .= localize("Le chemin le plus court est " . $graph->printShortestPath($_POST["DestX"], $_POST["DestY"]) . " PA.");
                $str .= "</td></tr></table>";
            }
            return $str;
        }
    }
}
?>