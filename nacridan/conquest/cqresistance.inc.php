<?php

class CQResistance extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQResistance($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>\n";
        $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='550px'>";
        $str .= localize("Désactiver l'aura de Résistance ?");
        $str .= "</td><td><input id='submitbt' type='submit' name='submitbt' value='Action' />";
        $str .= "<input name='action' type='hidden' value='" . DISABLED_RESISTANCE . "' />";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</td></tr></table>";
        $str .= "</form>";
        
        return $str;
    }
}
?>
