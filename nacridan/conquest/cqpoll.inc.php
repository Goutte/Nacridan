<?php
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");



class CQPoll extends HTMLObject
{

    public $nacridan;

    public $players;

    public $curplayer;

    public $db;

    public $style;

    public function CQPoll($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        $this->style = $style;
        
    }

    public function toString()
    {
        $db = $this->db;
        $players = $this->players;
        $curplayer = $this->curplayer;
        $id = 0;
        
        
        
        if (isset($_POST['create_last_step'])){ // ------------------- CREATION -----------------
		
            
                require_once (HOMEPATH . "/class/Nacripoll_Main.inc.php");
                require_once (HOMEPATH . "/class/Nacripoll_Question.inc.php");
				require_once (HOMEPATH . "/class/Nacripoll_Answer.inc.php");
				
				$poll = new Nacripoll_Main();                                
                $poll->set("name", base64_decode($_POST["TITLE"]));
                $poll->set("description", base64_decode($_POST["DESCRIPTION"]));
				$poll->set('participant_list',serialize(array()));		
                $poll->updateDB($db);
                
				$nbQ = $_POST['NB_QUESTION'];
				$nbA = $_POST['NB_MAX_CHOICE'];
				
				for ($iQ = 1; $iQ <= $nbQ; $iQ++){
					$question = new Nacripoll_Question();
					$question->set("id_Nacripoll_Main",$poll->get("id"));
					$question->set("text", base64_decode($_POST['question_'.$iQ]));					
					$question->updateDB($db);
										
					for ($iAns = 1; $iAns <= $nbA; $iAns++){
						if(base64_decode($_POST['q_'.$iQ.'_ans_'.$iAns]) != ''){						
							$answer = new Nacripoll_Answer();
							$answer->set("id_Nacripoll_Question",$question->get("id"));
							$answer->set("text", base64_decode($_POST['q_'.$iQ.'_ans_'.$iAns]));					
							$answer->updateDB($db);
						}
					}
				}
                
				
                $str = "<table class='maintable " . $this->style . "'>\n";
                $str .= "<tr><td class='mainbgbody'>Le sondage a été créé, il devrait être visible dans la liste.</td></tr>";                
                $str .= "</table>";
            
            return $str;
        } 
		elseif (isset($_POST['check'])){ // --------------VERIFICATION DU SONDAGE 
		            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION DU SONDAGE') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgbody' colspan='3'>Ce sondage convient-il ?</td></tr>";
            
			$str .= "<tr class='mainbgbody' >";                        
            $str .= "<td width='250px' >*** <b>" . base64_decode($_POST["TITLE"]) . "</b> ***</td>";
            $str .= "</tr>";
                        
            $description = base64_decode($_POST["DESCRIPTION"]);
            $description = nl2br(bbcode($description));
                        
            $str .= "<tr class='mainbgbody' ><td>" . $description . "</td></tr>";
            $nbQ = $_POST['NB_QUESTION'];
			$nbA = $_POST['NB_MAX_CHOICE'];
			
			for ($iQ = 1; $iQ <= $nbQ; $iQ++){
				$str .= "<tr><td class='mainbgbody' > <b>Question</b> : ".$_POST['question_'.$iQ]." </td></tr>";
				$str .= "<input type='hidden' name='question_".$iQ."' value='" . base64_encode($_POST['question_'.$iQ]) . "'/>";
				for ($iAns = 1; $iAns <= $nbA; $iAns++){
					$str .= "<tr><td class='mainbgbody' > Choix ".$iAns." : ".$_POST['q_'.$iQ.'_ans_'.$iAns]." </td></tr>";
					$str .= "<input type='hidden' name='q_".$iQ."_ans_".$iAns."' value='" . base64_encode($_POST['q_'.$iQ.'_ans_'.$iAns]) . "'/>";
				}
			}
			
			$str .= "<input type='hidden' name='TITLE' value='" . $_POST['TITLE'] . "'/>";
			$str .= "<input type='hidden' name='DESCRIPTION' value='" . $_POST['DESCRIPTION'] . "'/>";
			$str .= "<input type='hidden' name='NB_QUESTION' value='" . $_POST['NB_QUESTION'] . "'/>";
			$str .= "<input type='hidden' name='NB_MAX_CHOICE' value='" . $_POST['NB_MAX_CHOICE'] . "'/>";
			            
            $str .= "<td><input type='submit' name='create_last_step' value='Ok'/></td></tr>";
            $str .= "</table></form>";
            
            return $str;
        } 
		elseif (isset($_POST['create_step_1'])){ // ------- FORMULAIRE DE CREATION - STEP 1
		
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UN SONDAGE - ETAPE 1') . "</b></td>\n";
            $str .= "</tr>\n";
            
			
            $str .= "<tr><td class='mainbgbody' ><label for='title'>Titre : </label><input type='text' maxlength='30' name='title'  id='title' /></td></tr>";
			$str .= "<tr><td class='mainbgbody' ><label for='title'>Nombre de questions : </label><input type='text' maxlength='30' name='nb_question'  id='nb_question' /></td></tr>";
			$str .= "<tr><td class='mainbgbody' ><label for='title'>Nombre max de choix par question : </label><input type='text' maxlength='30' name='nb_max_choice'  id='nb_max_choice' /></td></tr>";            
            $str .= "<tr><td class='mainbgbody' align='left'><label for='content'> Description : </label> </td></tr>";
            
            $str .= "<tr>";
            $str .= "<td class='mainbgbody' > <textarea rows=6 cols=60 name='description' id='description' /> </textarea></td>";
            $str .= "</tr>";
            
            $str .= "<td><input type='submit' name='create_step_2' value='Etape suivante - corps du sondage'/></td></tr>";
            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
        }
		elseif (isset($_POST['create_step_2'])){ // ------- FORMULAIRE DE CREATION - STEP 2
		
            $str = "<table class='maintable " . $this->style . "'>\n";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UN SONDAGE - ETAPE 2') . "</b></td>\n";
            $str .= "</tr>\n";
            for ($iQ = 1; $iQ <= $_POST['nb_question']; $iQ++){
				$str .= "<tr><td class='mainbgbody' ><label for='title'><b>Question ".$iQ." </b>: </label><input type='text' maxlength='2000' name='question_".$iQ."'  id='question_".$iQ."' /></td></tr>";
				for ($iAns = 1; $iAns <= $_POST['nb_max_choice']; $iAns++){
					$str .= "<tr><td class='mainbgbody' ><label for='title'>Réponse ".$iAns." : </label><input type='text' maxlength='2000' name='q_".$iQ."_ans_".$iAns."'  id='q_".$iQ."_ans_".$iAns."' /></td></tr>";
				}
			}
			
			$str .= "<input type='hidden' name='TITLE' value='" . base64_encode($_POST['title']) . "'/>";
			$str .= "<input type='hidden' name='DESCRIPTION' value='" . base64_encode($_POST['description']) . "'/>";
			$str .= "<input type='hidden' name='NB_QUESTION' value='" . $_POST['nb_question'] . "'/>";
			$str .= "<input type='hidden' name='NB_MAX_CHOICE' value='" . $_POST['nb_max_choice'] . "'/>";
            $str .= "<td><input type='submit' name='check' value='Vérifier le sondage'/></td></tr>";
            $str .= "</form>";
            
            $str .= "</table>";
            return $str;
      }
      elseif(isset($_POST['activate'])) { // --------------- ACTIVATION
            	
      	$dbu = new DBCollection("UPDATE Nacripoll_Main SET status='active',start_date=NOW() WHERE id=" . quote_smart($_POST['selectedpoll']), $db, 0, 0, false);
			$str = "<table class='maintable " . $this->style . "'>\n";      	
			$str .= "<tr><td class='mainbgbody' >Le sondage sélectionné est normalement activé.</td></tr>";
      	$str .= "</table>";
         return $str;
      }
      elseif(isset($_POST['deactivate'])) { // --------------- DESACTIVATION
            	
      	$dbu = new DBCollection("UPDATE Nacripoll_Main SET status='done',end_date=NOW() WHERE id=" . quote_smart($_POST['selectedpoll']), $db, 0, 0, false);
			$str = "<table class='maintable " . $this->style . "'>\n";      	
			$str .= "<tr><td class='mainbgbody' >Le sondage sélectionné est normalement désactivé.</td></tr>";
      	$str .= "</table>";
         return $str;
      }	  
		elseif (isset($_POST['deleteconfirm'])){ // --------------------- SUPPRESSION

            
        } elseif (isset($_POST['delete'])) // --------------- CONFIRMATION DE LA SUPPRESSION
{
            
            
        } else // ---------------- LISTE des SONDAGES -------------
		{
            
            $dbpoll = new DBCollection("SELECT * FROM Nacripoll_Main", $db);
            
            $data = array();
            
            while (! $dbpoll->eof()) {
                
				if($dbpoll->get('start_date') == '0000-00-00 00:00:00')
					$start_date = '---';
				else 
					$start_date = $dbpoll->get('start_date');	
				if($dbpoll->get('end_date') == '0000-00-00 00:00:00')
					$end_date = '---';
				else 
					$end_date = $dbpoll->get('end_date');		
				
                $data[] = array(
                    'id' => $dbpoll->get('id'),
                    "name" => $dbpoll->get('name'),
                    "status" => $dbpoll->get('status'),
                    "start_date" => $start_date,
					"end_date" => $end_date
                );
                $dbpoll->next();
            }
            
            $str = "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='4' class='mainbgtitle'><b>" . localize('LISTES DES SONDAGES') . "</b></td>\n";
            $str .= "</tr>\n";
            $str .= "<tr>\n";
            $str .= "<td class='mainbglabel' width='53px' align='center'>" . localize('id') . "</td>\n";
            $str .= "<td class='mainbglabel' width='200px' align='center'>" . localize('Titre') . "</td>\n";
            $str .= "<td class='mainbglabel' width='150px' align='center'>" . localize('Statut') . "</td>\n";
            $str .= "<td class='mainbglabel' width='150px' align='center'>" . localize('Début') . "</td>\n";
			$str .= "<td class='mainbglabel' align='center'>" . localize('Fin') . "</td>\n";
            $str .= "</tr></table>";
            $str .= "<table class='maintable " . $this->style . "'>";
            
            foreach ($data as $arr) {
                $str .= "<tr><td class='mainbgbody' width='30px' align='center'> <input type='radio' name='selectedpoll' id='poll" . $arr["id"] . "' value=" . $arr["id"] . " /> </td>";
                $str .= "<td class='mainbgbody' width='20px' align='left'> " . $arr["id"] . " </td>\n";
                $str .= "<td class='mainbgbody' width='200px' align='left'> " . $arr["name"] . " </td>\n";
                $str .= "<td class='mainbgbody' width='100px'  align='left'><label for='news" . $arr["status"] . "'> " . $arr["status"] . "</label> </td>";
                $str .= "<td class='mainbgbody' width='150px' align='left'> " . $arr["start_date"] . " </td>";
				$str .= "<td class='mainbgbody'  align='left'> " . $arr["end_date"] . " </td>";
                $str .= "</tr>\n";
            }
            $str .= "</table>";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr><td><input type='submit' name='delete' value='Supprimer'/></td></tr>";
            $str .= "<tr><td><input type='submit' name='activate' value='Activer'/></td></tr>";
            $str .= "<tr><td><input type='submit' name='deactivate' value='Terminer'/></td></tr>";
            
            
            $str .= "</table>";
            
            
            $str .= "</form>";
            
            $str .= "<form name='form'  method=post target='_self'>\n";
            $str .= "<table class='maintable " . $this->style . "'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('CRÉATION D\'UN SONDAGE') . "</b></td>\n";
            $str .= "</tr>\n";
            
            $str .= "<td><input type='submit' name='create_step_1' value='Créer'/></td></tr>";
            $str .= "</table>";
            $str .= "</form>";
            return $str;
        }
    }
}
?>

 
      
