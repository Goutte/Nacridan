<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");

class CQEquip extends HTMLObject
{

    public $id;

    public $db;

    public $equip;

    public $str;

    public function CQEquip($nacridan, $head, $body, $db)
    {
        $this->curplayer = $nacridan->loadCurSessPlayer($db);
        
        $this->id = $this->curplayer->get("id");
        $this->db = $db;
        
        // --------- Gère la position des slots d'équipement portés dans l'inventaire
        $stepX = - 100;
        $stepY = 0;
        $this->equip[1] = array(
            210 - $stepX,
            255 - $stepY
        );
        $this->equip[2] = array(
            333 - $stepX,
            255 - $stepY
        );
        $this->equip[4] = array(
            273 - $stepX,
            185 - $stepY
        );
        $this->equip[8] = array(
            235 - $stepX,
            155 - $stepY
        );
        $this->equip[16] = array(
            311 - $stepX,
            155 - $stepY
        );
        $this->equip[32] = array(
            273 - $stepX,
            98 - $stepY
        );
        $this->equip[64] = array(
            array(
                200 - $stepX,
                415 - $stepY
            ),
            array(
                345 - $stepX,
                415 - $stepY
            )
        );
        $this->equip[128] = array(
            273 - $stepX,
            145 - $stepY
        );
        $this->equip[256] = array(
            273 - $stepX,
            235 - $stepY
        ); // ceinture
        $this->equip[512] = array(
            array(
                240 - $stepX,
                300 - $stepY
            ),
            array(
                302 - $stepX,
                300 - $stepY
            )
        );
        $this->equip[1024] = array(
            array(
                215 - $stepX,
                365 - $stepY
            ),
            array(
                330 - $stepX,
                365 - $stepY
            )
        );
        $this->equip[4096] = array(
            200 - $stepX,
            235 - $stepY
        );
        $this->equip[8192] = array(
            345 - $stepX,
            235 - $stepY
        );
        $this->equip[16384] = array(
            180 - $stepX,
            88 - $stepY
        );
        $this->equip[32768] = array(
            180 - $stepX,
            108 - $stepY
        );
        $this->equip[65536] = array(
            180 - $stepX,
            128 - $stepY
        );
        $this->equip[131072] = array(
            180 - $stepX,
            148 - $stepY
        );
        
        $head->add("<style type=\"text/css\">\n" . $this->getEquipCss() . "</style>");
        $id = $this->id;
        $db = $this->db;
        $eq = new Equipment();
        $eqmodifier = new Modifier();
        $modifier = new Modifier();
        $mission = new Mission();
        $caravan = new Caravan();
        $template = new Template();
        $tmodifier = new Modifier();
        // profiler_start("SQLQuery");
        
        $dbe = new DBCollection(
            "SELECT BasicEquipment.id_BasicMaterial AS BM, " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                 $mission->getASRenamer("Mission", "MISS") .
                 ",mask,wearable,BasicEquipment.durability AS dur  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE EquipmentType.small='No' AND Equipment.id_Player=" .
                 $id, $db);
        $str2 = "";
        $loads = 0;
        $plate = 0;
        $linen = 0;
        
        /* ******************************************************************* AFFICHAGE DES EQUIPEMENTS PORTES ************************************************************* */
        while (! $dbe->eof()) {
            if ($dbe->get("EQweared") != "No") {
                $eq->DBLoad($dbe, "EQ");
                if ($dbe->get("EQMid") != 0) {
                    $eqmodifier->DBLoad($dbe, "EQM");
                    $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                    if ($eq->get("sharpen") > 1)
                        $eqmodifier->addModif("damage", DICE_ADD, $eq->get("sharpen"));
                    
                    if ($eq->get("extraname") != "") {
                        $i = 0;
                        $tmodifier = new Modifier();
                        $template = new Template();
                        $dbt = new DBCollection(
                            "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                                 $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                                 " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                                 $eq->get("id") . " order by Template.pos asc", $db);
                        while (! $dbt->eof()) {
                            $i ++;
                            $template->DBLoad($dbt, "TP");
                            $tmodifier->DBLoad($dbt, "MD");
                            $tmodifier->updateFromTemplateLevel($template->get("level"));
                            $eqmodifier->addModifObj($tmodifier);
                            
                            if ($i == 1)
                                $extraname = $dbt->get("name1") . "(" . $template->get("level") . ") ";
                            if ($i == 2)
                                $extraname .= " " . $dbt->get("name2") . "(" . $template->get("level") . ")";
                            
                            $dbt->next();
                        }
                    }
                    $eqmodifier->initCharacStr();
                }
                if ($eq->get("durability") != 0 && $eq->get("id_EquipmentType") < 41)
                    $loads += $eq->get("level") * $eq->get("level");
                
                if ($dbe->get("BM") == 1 && $eq->get("id_EquipmentType") > 6 && $eq->get("id_EquipmentType") < 22)
                    $plate += $eq->get("level");
                
                if ($dbe->get("BM") == 5 && $eq->get("id_EquipmentType") > 6 && $eq->get("id_EquipmentType") < 22)
                    $linen += $eq->get("level");
                
                $str2 .= $this->displayWeared($eq, $eqmodifier, $modifier, $dbe->get("mask"));
            }
            $dbe->next();
        }
        
        $str = "<div class='equipinventpos' style='background-image: url(../pics/conquest/inventory.jpg)'>";
        if ($loads > $this->curplayer->get("level") * 5)
            $str .= "<div style='position: absolute; font-weight: bold; color: #AA0000; left: 15px; bottom: 14px;'>" . localize("Surcharge : {pt} / {pt2}", 
                array(
                    "pt" => $loads,
                    "pt2" => $this->curplayer->get("level") * 5
                )) . "</div>";
        else
            $str .= "<div style='position: absolute; font-weight: bold; color: #333333; left: 15px; bottom: 14px;'>" . localize("Charge : {pt} / {pt2}", 
                array(
                    "pt" => $loads,
                    "pt2" => $this->curplayer->get("level") * 5
                )) . "</div>";
        
        $strength = $this->curplayer->getObj("Modifier")->getAtt("strength", DICE_D6);
        if ($plate > $strength)
            $str .= "<div style='position: absolute; font-weight: bold; color: #AA0000; left: 158px; bottom: 366px;'>" . localize("plate : {pt}/{pt2}", 
                array(
                    "pt" => $plate,
                    "pt2" => $strength
                )) . "</div>";
        else
            $str .= "<div style='position: absolute; font-weight: bold; color: #333333; left: 158px; bottom: 366px;'>" . localize("plate: {pt}/{pt2}", 
                array(
                    "pt" => $plate,
                    "pt2" => $strength
                )) . "</div>";
        
        $MM = $this->curplayer->getObj("Modifier")->getAtt("magicSkill", DICE_D6);
        if ($linen > $MM)
            $str .= "<div style='position: absolute; font-weight: bold; color: #AA0000; left: 158px; bottom: 346px;'>" . localize("lin : {pt}/{pt2}", 
                array(
                    "pt" => $linen,
                    "pt2" => $MM
                )) . "</div>";
        else
            $str .= "<div style='position: absolute; font-weight: bold; color: #333333; left: 158px; bottom: 346px;'>" . localize("lin: {pt}/{pt2}", 
                array(
                    "pt" => $linen,
                    "pt2" => $MM
                )) . "</div>";
        
        $str .= "</div>";
        
        /* ************************************************** CALCUL DU SAC A AFFICHER ********************************************************* */
        if (isset($_GET["type"]))
            $type = quote_smart($_GET["type"]);
        else {
            $type = 0;
        }
        
        $cond = "Equipment.id_Player=" . $id;
        if ($type == - 1) {
            $cond .= " AND weared='YES' ";
        } else {
            $cond .= " AND weared='NO' AND  id_Equipment\$bag =" . playerFactory::getIdBagByNum($id, $type, $db);
        }
        
        $titre = "";
        
        if ($type != - 1) {
            
            $nameBag = PlayerFactory::getNameBagByNum($id, $type, $db);
            if (PlayerFactory::getIdBagByNum($id, $type, $db) || $type == 0) {
                $titre = $nameBag;
                $titre .= " (" . PlayerFactory::getNbElementInBag($id, $nameBag, $db) . "/" . PlayerFactory::getNbElementMaxInBag($id, $nameBag, $db) . ")";
            } else {
                $titre = $nameBag . ": vous n'avez pas ce type d'équipement.";
            }
        } else {
            $titre = localize("Détail de l'équipement porté");
        }
        
        /* ****************************************************** AFFICHAGE DES EQUIPEMENTS ******************************************************* */
        
        $str .= "<div class='equippos scrollable equipwidth'>\n";
        $str .= "<table class='maintable' width='520px' >\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('I N V E N T A I R E') . "</b></td></tr>\n";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle' colspan='6'>\n";
        $str .= "<a href='../conquest/conquest.php?center=equip&type=0' class='tabmenu'>" . localize("Sac d'équipement") . "</a> |\n";
        if (PlayerFactory::getIdBagByName($id, "Sac de gemmes", $db))
            $str .= "<a href='../conquest/conquest.php?center=equip&type=1' class='tabmenu'>" . localize('Sac de gemmes') . "</a> |\n";
        if (PlayerFactory::getIdBagByNum($id, "2", $db))
            $str .= "<a href='../conquest/conquest.php?center=equip&type=2' class='tabmenu'>" . localize('Sac de matières premières') . "</a> |\n";
        if (PlayerFactory::getIdBagByNum($id, "3", $db))
            $str .= "<a href='../conquest/conquest.php?center=equip&type=3' class='tabmenu'>" . localize("Sac d'herbes") . "</a> |\n";
        if (PlayerFactory::getIdBagByNum($id, "4", $db))
            $str .= "<a href='../conquest/conquest.php?center=equip&type=4' class='tabmenu'>" . localize("Ceinture") . "</a> |\n";
        if (PlayerFactory::getIdBagByNum($id, "5", $db))
            $str .= "<a href='../conquest/conquest.php?center=equip&type=5' class='tabmenu'>" . localize("Carquois") . "</a> |\n";
        if (PlayerFactory::getIdBagByNum($id, "6", $db))
            $str .= "<a href='../conquest/conquest.php?center=equip&type=6' class='tabmenu'>" . localize("Trousse à outils") . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=equip&type=-1' class='tabmenu'>" . localize("Equipement porté") . "</a>\n";
        $str .= "</td>\n</tr>\n";
        $str .= "<tr><td colspan='6' class='mainbgtitle'><b>" . localize($titre) . "</b>\n</tr>\n";
        
        /* ****************************************************************************** AFFICHAGE DES GROS EQUIPEMENTS ******************************************************************** */
        $dbe = new DBCollection(
            "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," . $mission->getASRenamer("Mission", "MISS") .
                 "," . $caravan->getASRenamer("Caravan", "CAR") .
                 ",mask,wearable  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id  LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id LEFT JOIN Caravan ON Equipment.id_Caravan=Caravan.id WHERE state='Carried' AND " .
                 $cond, $db);
        while (! $dbe->eof()) {
            $eq->DBLoad($dbe, "EQ");
            if ($dbe->get("EQMid") != 0) {
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                if ($eq->get("sharpen") > 0)
                    $eqmodifier->addModif("damage", DICE_ADD, $eq->get("sharpen"));
                
                if ($eq->get("extraname") != "") {
                    $i = 0;
                    $tmodifier = new Modifier();
                    $template = new Template();
                    $dbt = new DBCollection(
                        "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                             $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                             " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                             $eq->get("id") . " order by Template.pos asc", $db);
                    while (! $dbt->eof()) {
                        $i ++;
                        $template->DBLoad($dbt, "TP");
                        $tmodifier->DBLoad($dbt, "MD");
                        $tmodifier->updateFromTemplateLevel($template->get("level"));
                        $eqmodifier->addModifObj($tmodifier);
                        if ($i == 1)
                            $extraname = $dbt->get("name1") . "(" . $template->get("level") . ") ";
                        if ($i == 2)
                            $extraname .= $dbt->get("name2") . "(" . $template->get("level") . ")";
                        
                        $dbt->next();
                    }
                }
                $eqmodifier->initCharacStr();
            }
            $str .= "<tr><td class='mainbgbody'>";
            
            // Parchemin
            if ($eq->get("id_BasicEquipment") == 600) {
            	 
                if ($dbe->get("MISSid_BasicMission") == 1)
                    $str .= "<a href=\"../conquest/parchment.php?type=mission&id=" . $eq->get("id") . "\" class='parchment popupify'>" . localize($eq->get("name")) .
                         "</a>";
                elseif($dbe->get("CARid")!=0)
                    $str .= "<a href=\"../conquest/parchment.php?type=caravan&id=" . $eq->get("id") . "\" class='parchment popupify'>" . localize($eq->get("name")) .
                         "</a>";
                elseif($eq->get("id_Mission") == 0)
            	 	 $str .= "<a href=\"../conquest/parchment.php?type=anim&id=" . $eq->get("id") . "\" class='parchment popupify'>" . localize($eq->get("name")) .
                         "</a>";
                $str .= " (" . $eq->get("id") . ")";
                
                if ($dbe->get("MISSname") != "")
                    $str .= "<br/>" . localize($dbe->get("MISSname"));
                elseif ($dbe->get("CARid") != "")
                    $str .= "<br/> Mission commerciale";
            } elseif ($eq->get("id_BasicEquipment") > 499) {
                $str .= "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>" . "<br/>(" . $eq->get("id") . ")";
            } else {
                // Affichage commun de tous les équipements : name - extraname s'il existe - level - id
                $str .= "<b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                if ($eq->get("extraname") != "") {
                    $str .= " <span class='template'>" . $extraname . "</span>";
                }
                
                $str .= "<br/>" . localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")";
            }
            
            // Si la pièce est en cours de fabrication
            if (($eq->get("templateProgress") < 100 && $eq->get("templateProgress") > 0) or $eq->get("progress") < 100) {
                if ($eq->get("templateProgress") < 100 && $eq->get("templateProgress") > 0)
                    $str .= " - Confection : " . $eq->get("templateProgress") . "%";
                
                if ($eq->get("progress") < 100)
                    $str .= " - Confection : " . $eq->get("progress") . "%";
            } else {
                // Affichage de la durability pour les équipements et les sacs
                if ($eq->get("id_EquipmentType") < 28 or ($eq->get("id_EquipmentType") >= 40 and $eq->get("id_EquipmentType") <= 44) or $eq->get("id_EquipmentType") == 46)
                    $str .= " - " . EquipFactory::getDurability($eq, $db);
                
                if ($eq->get("sharpen"))
                    $str .= " - Aiguisé";
                
                $str .= "<br/>";
                // Affichages des bonus de caractéristique pour les équipements
                if ($eq->get("id_EquipmentType") < 30) {
                    $modstr = "";
                    foreach ($eqmodifier->m_characLabel as $key) {
                        $tmp = $eqmodifier->getModifStr($key);
                        if ($tmp != "0")
                            $modstr .= translateAttshort($key) . " : " . $tmp . " | ";
                        
                        // $modstr.=" ".strtoupper(substr($key,0,3)).": ".$tmp." | ";
                    }
                    $str .= "(" . substr($modstr, 0, - 3) . ")";
                }
            }
            
            // Si l'objet est en vente
            $sale = "";
            if ($eq->get("sell") > 0) {
                $sale = "(" . localize("En vente") . ")";
                $sale .= "<br/>" . $eq->get("sell") . " " . localize("PO");
            }
            $str .= "</td><td class='mainbgbody' valign='top' align='center' width='75px'>" . $sale . "</td></tr>";
            
            $dbe->next();
        }
        
        /* ****************************************************************************** AFFICHAGE DES PETITS EQUIPEMENTS ******************************************************************** */
        
        $str .= "</table></div>\n";
        $body->add($str2);
        $this->str = $str;
    }

    public function toString()
    {
        return $this->str;
    }

    protected function displayWeared($eq, $eqmodifier, $modifier, $mask)
    {
        $equip = $this->equip;
        
        $str = "";
        $i = 1;
        while ($i <= 131072) {
            if ($mask & $i) {
                if (is_array($equip[$i][0])) {
                    $j = 0;
                    foreach ($equip[$i] as $equipval) {
                        $str .= $this->drawEquip($eq, $eqmodifier, $equipval, $i . $j);
                        $j ++;
                    }
                } else {
                    $str .= $this->drawEquip($eq, $eqmodifier, $equip[$i], $i);
                }
            } else {
                if (isset($this->equip[$i])) {
                    if (is_array($equip[$i][0])) {
                        $j = 0;
                        foreach ($equip[$i] as $equipval) {
                            $str .= $this->drawEmpty($i . $j);
                            $j ++;
                        }
                    } else {
                        $str .= $this->drawEmpty($i);
                    }
                }
            }
            $i *= 2;
        }
        return $str;
    }

    protected function drawEmpty($i)
    {
        $str = "<img class='img' id='equip" . $i . "' src='../pics/misc/button02.gif' usemap='#equipment" . $i . "' />\n";
        $str .= "<map name='equipment" . $i . "' style='z-index: 2;'>\n";
        $str .= "<area shape=circle coords='8,8,10' onMouseOut=\"hideTip()\" onMouseOver=\"showTip('equipdiv" . $i . "')\">\n";
        $str .= "</map>\n";
        return $str;
    }

    protected function drawEquip($eq, $eqmodifier, $equipval, $i)
    {
        $str = "<img class='img' id='equip" . $i . "' src='../pics/misc/button01.gif'  style='z-index: 2;' usemap='#equipment" . $i . "' />\n";
        $str .= "<map name='equipment" . $i . "' style='z-index: 2;'>\n";
        $str .= "<area shape=circle coords='8,8,10' onMouseOut=\"hideTip()\" onMouseOver=\"showTip('equipdiv" . $i . "')\">\n";
        $str .= "</map>\n";
        
        $str .= "<table class='maintable tabletip' style='left:" . $equipval[0] . "px; top:" . ($equipval[1] + 16) . "px; ' id='" . "equipdiv" . $i .
             "'><tr><td class='mainbgtitle'><b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b></td></tr>";
        if ($eq->get("extraname") != "") {
            $tlevel = $eq->get("templateLevel");
            $str .= "<tr><td class='mainbgtitle'><span class='template'>" . localize($eq->get("extraname")) . "</span></td></tr>";
        }
        
        $str .= "<tr><td class='mainbgtitle'>" . localize("Niveau") . " " . $eq->get("level") . " (" . $eq->get("id") . ")";
        if ($eq->get("durability") == 0)
            $str .= "</td></tr><tr><td class='mainbgtitle' style='font-weight: bold; color: #AA0000;'>" . EquipFactory::getDurability($eq, $this->db) . "</td></tr>";
        else {
            $str .= " - " . EquipFactory::getDurability($eq, $this->db);
            if ($eq->get("sharpen"))
                $str .= " - Aiguisé";
            
            $str .= "</td></tr>";
            foreach ($eqmodifier->m_characLabel as $key) {
                $tmp = $eqmodifier->getModifStr($key);
                if ($tmp != "0")
                    $str .= "<tr><td class='mainbgbody'>" . translateAttshort($key) . ": " . $tmp . "</td></tr>";
            }
        }
        $str .= "</table>";
        return $str;
    }

    protected function getEquipCss()
    {
        $equip = $this->equip;
        
        $str = ".img{position: absolute; z-index: 1; border: 0;}\n";
        
        $i = 1;
        while ($i <= 131072) {
            if (isset($equip[$i])) {
                if (is_array($equip[$i][0])) {
                    $j = 0;
                    foreach ($equip[$i] as $equipval) {
                        $str .= "#equip" . $i . $j . " { left: " . $equipval[0] . "px; top: " . $equipval[1] . "px; }\n";
                        $j ++;
                    }
                } else {
                    $str .= "#equip" . $i . " { left: " . $equip[$i][0] . "px; top: " . $equip[$i][1] . "px; }\n";
                }
            }
            $i *= 2;
        }
        
        $str .= ".tabletip{visibility: hidden;
               position: absolute; 
               z-index: 5;            
               padding: 0;
               margin: 0;
               -moz-opacity: .90;
               filter: alpha (opacity =90);
               width: 150px;";
        $str .= "top: 16px;\n";
        $str .= "left: 0px;}\n";
        
        $str .= ".tabletip td {width: 150px;}\n";
        return $str;
    }
}
?>
