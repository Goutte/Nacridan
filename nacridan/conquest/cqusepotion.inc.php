<?php

class CQUsePotion extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQUsePotion($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        $id = $curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        if ($curplayer->get("ap") < USE_BELTPOTION_AP) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour utiliser un objet.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>";
            $str .= localize("Utiliser") . " <select id='object' class='selector cqattackselectorsize' name='OBJECT_ID'>";
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE id_EquipmentType=29 AND id_Player=" . $id, $db);
            // Test de tolérance des potions
            $dbh = new DBCollection("SELECT sum(value) as drug, min(life) as life FROM BM WHERE id_Player=" . $curplayer->get("id") . " AND (name='Potion de force' OR name='Potion de dextérité' OR name='Potion de vitesse' OR name='Potion de magie')", $db);
            $drug = $dbh->get("drug") - 1;
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez une potion --") . "</option>";
            if ($id_Bag = playerFactory::getIdBagByNum($id, 4, $db)) {
                $item[] = array(
                    localize("-- Votre ceinture --  (1PA) ") => - 1
                );
                while (! $dbp->eof()) {
                    if (! $dbh->eof() && ($drug + $dbp->get("level")) * ($drug + $dbp->get("level")) > $curplayer->get("level"))
                        $drugstr = " Potion trop forte, vous prenez des risques.";
                    else
                        $drugstr = "";
                    if ($dbp->get("id_Equipment\$bag") == $id_Bag)
                        $item[] = array(
                            localize($dbp->get("name")) . " niv. " . $dbp->get("level") . " (" . $dbp->get("id") . ")" . $drugstr => $dbp->get("id")
                        );
                    $dbp->next();
                }
            }
            $dbp = new DBCollection("SELECT * FROM Equipment WHERE id_EquipmentType=29 AND id_Player=" . $id, $db);
            $item[] = array(
                localize("-- Votre sac d équipement -- (2PA) ") => - 1
            );
            if ($curplayer->get("ap") >= USE_INVPOTION_AP) {
                
                while (! $dbp->eof()) {
                    if (! $dbh->eof() && ($drug + $dbp->get("level")) * ($drug + $dbp->get("level")) > $curplayer->get("level"))
                        $drugstr = " Potion trop forte, vous prenez des risques.";
                    else
                        $drugstr = "";
                    if ($dbp->get("id_Equipment\$bag") == 0)
                        $item[] = array(
                            localize($dbp->get("name")) . " niv. " . $dbp->get("level") . " (" . $dbp->get("id") . ")" . $drugstr => $dbp->get("id")
                        );
                    $dbp->next();
                }
            } else
                $item[] = array(
                    localize("vous n'avez pas suffisament de PA pour accéder aux potions de votre inventaire") => - 1
                );
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            $str .= "</select>";
            
            $str .= "</select></td><td align='center'>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            
            $str .= "<input name='action' type='hidden' value='" . USE_POTION . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr>";
            $str .= "<tr><td colspan = 2> Attention, boire une potion située dans votre inventaire utilise 2PA, tandis que celles de votre ceinture sont accessibles pour 1PA</td><tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
