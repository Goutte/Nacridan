<?php
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/MinimapCase.inc.php");

class CQView2D extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public $minimap;

    public $body;

    public $str;

    public $radius;

    private $data = array();
    private $dist = array();
    private $list = array();

    public function CQView2D($nacridan, &$head, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $curplayer = $this->curplayer;
        $mapnumber = $curplayer->get("map");
        require_once (HOMEPATH . "/lib/MapInfo.inc.php");
        
        $this->mapinfo = new MapInfo($db);
        
        $db = $this->db;
        $this->str = "";
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        $radius = SIZEAREA;
        if (($curplayer->get("inbuilding") != 0) &&
             ($curplayer->get("room") != TOWER_ROOM && $curplayer->get("room") != PALACE_BELL_TOWER_ROOM && $curplayer->get("room") != TEMPLE_ROOF_ROOM)) {
            $radius = 2;
        } else {
            $radius = SIZEAREA;
            if ($curplayer->get("room") == TEMPLE_ROOF_ROOM) {
                $radius = SIZEAREA;
            }
        }
        
        $this->radius = $radius;
        
        $this->minimap = $this->mapinfo->getMap($xp, $yp, $this->radius, $curplayer->get("map"));
        
        $this->build_wrs();
        $this->build_pj();
        $this->build_pnj();
        $this->build_items();
        $this->build_places();
        
        $this->str .= $this->draw_map();
        
        $head->add("<script language='javascript' type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js'></script>");
        $head->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/minimap.js") . "'></script>");
    }
    
    // ------------------------ Fonctions de construction des éléments de la vue
    public function build_places()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        // ------- ajout des Places
        
        $dbplace = new DBCollection(
            "SELECT id,name,x,y FROM Place WHERE map=" . $map . " AND x<=" . $this->radius . "+" . $xp . " AND x>=-" . $this->radius . "+" . $xp . " AND y<=" . $this->radius . "+" .
                 $yp . " AND y>=-" . $this->radius . "+" . $yp, $db, 0, 0);
        
        $data = array();
        $dist = array();
        
        while (! $dbplace->eof()) {
            $xx = $dbplace->get("x");
            $yy = $dbplace->get("y");
            $distVal = max(abs($xp - $xx), abs($yp - $yy));
            $name = "<span class=\"placename\">" . localize($dbplace->get("name")) . "</span>";
            $placetmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "dist" => $distVal,
                "name" => $name
            );
            $this->add_place($xx, $yy, $placetmp);
            $dbplace->next();
        }
        
        // ------- ajout des Portails
        
        $dbgate = new DBCollection(
            "SELECT id,x,y,level,nbNPC FROM Gate WHERE map=" . $map . " AND x<=" . ($this->radius) . "+" . $xp . " AND x>=-" . ($this->radius) . "+" . $xp . " AND y<=" .
                 ($this->radius) . "+" . $yp . " AND y>=-" . ($this->radius) . "+" . $yp, $db, 0, 0);
        
        while (! $dbgate->eof()) {
            $xx = $dbgate->get("x");
            $yy = $dbgate->get("y");
            $level = $dbgate->get("level");
            $distVal = max(abs($xp - $xx), abs($yp - $yy));
            if ($dbgate->get("nbNPC") > 0)
                $name = "<span class=\"gatename\">" . localize("Portail") . "</span>";
            else
                $name = "<span class=\"gatename\">" . localize("Portail Inactif") . "</span>";
            $gatetmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "dist" => $distVal,
                "name" => $name,
                "level" => $level,
                "id" => $dbgate->get("id"),
                "nbNPC" => $dbgate->get("nbNPC")
            );
            
            $this->add_portail($xx, $yy, $gatetmp);
            $dbgate->next();
        }
        
        // ------- ajout des bâtiments
        
        $dbbuilding = new DBCollection(
            "SELECT Building.id,Building.id_Player,Building.x,Building.y,Building.name,Building.level,Building.sp,Building.currsp, Building.progress, Building.repair, Building.value,BasicBuilding.pic, 
             Building.id_BasicBuilding,City.id as idCity, City.name as nameCity, City.type as typeCity FROM Building inner join BasicBuilding on id_BasicBuilding=BasicBuilding.id left outer join City on Building.id_City = City.id WHERE Building.map=" .
                 $map . " AND Building.x<=" . ($this->radius) . "+" . $xp . " AND Building.x>=-" . ($this->radius) . "+" . $xp . " AND Building.y<=" . ($this->radius) . "+" . $yp .
                 " AND Building.y>=-" . ($this->radius) . "+" . $yp, $db, 0, 0);
        
        $state = array(
            localize("En ruine"),
            localize("Gravement Endommagé"),
            localize("Endommagé"),
            localize("Légèrement Endommagé"),
            ""
        );
        
        while (! $dbbuilding->eof()) {
            $xx = $dbbuilding->get("x");
            $yy = $dbbuilding->get("y");
            $sp = $dbbuilding->get("sp");
            $buildingName = $dbbuilding->get("name");
            $buildingType = $dbbuilding->get("id_BasicBuilding");
            $currsp = $dbbuilding->get("currsp");
            $owner = new Player();
            $owner->load($dbbuilding->get("id_Player"), $db);
            
            $curstate = min(floor($currsp * 5 / $sp), 4);
            
            $distVal = max(abs($xp - $xx), abs($yp - $yy));
            $name = "<span class=\"buildingname\">" . localize($buildingName) . "</span>";
            
            $pic = $dbbuilding->get("pic");
            
            if (($buildingType == 15) || ($buildingType == 27)) { // cas des Bassin Divins et des murs
                $cityname = "créé par " . $owner->get("name");
                $buildingtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "dist" => $distVal,
                    "value" => $dbbuilding->get("value"),
                    "name" => $buildingName,
                    "type" => $buildingType,
                    "sp" => $sp,
                    "currsp" => $currsp,
                    "pic" => $pic,
                    "cityname" => $cityname,
                    "id" => $dbbuilding->get("id"),
                    "repair" => $dbbuilding->get("repair"),
                    "progress" => $dbbuilding->get("progress"),
                    "state" => $state[$curstate],
                    "level" => $dbbuilding->get('level')
                );
            } else {
                // $dbcity = new DBCollection("SELECT City.id as idCity, City.name as nameCity, City.type as typeCity FROM City WHERE id=" . $dbbuilding->get("id_City"), $db, 0, 0);
                
                if ($dbbuilding->get("nameCity") != "Bourg" && $dbbuilding->get("nameCity") != "Village")
                    $cityname = "de la ville <a href=\"" . CONFIG_HOST . "/conquest/profilecity.php?id=" . $dbbuilding->get("idCity") .
                         "\" class='buildingname popupify'>" . $dbbuilding->get("nameCity") . "</a>";
                else {
                    if ($dbbuilding->get("nameCity") == "Bourg" or $dbbuilding->get("nameCity") == "Village")
                        $cityname = "d'un " . $dbbuilding->get("nameCity");
                    else
                        $cityname = "de " . $dbbuilding->get("nameCity");
                }
                
                $ownerString = "";
                
                if ($dbbuilding->get("id_Player") > 0) {
                    $ownerString = " propriété de <a href='../conquest/profile.php?id=" . $owner->get("id") .
                         "' class='buildingname popupify'>" . $owner->get("name") . "</a>";
                }
                
                $buildingtmp = array(
                    "xx" => $xx,
                    "value" => $dbbuilding->get("value"),
                    "yy" => $yy,
                    "dist" => $distVal,
                    "name" => $buildingName,
                    "type" => $buildingType,
                    "sp" => $sp,
                    "currsp" => $currsp,
                    "pic" => $pic,
                    "cityname" => $cityname,
                    "id" => $dbbuilding->get("id"),
                    "progress" => $dbbuilding->get("progress"),
                    "repair" => $dbbuilding->get("repair"),
                    "typeCity" => $dbbuilding->get("typeCity"),
                    "id_City" => $dbbuilding->get("idCity"),
                    "state" => $state[$curstate],
                    "level" => $dbbuilding->get('level'),
                    "owner" => $ownerString
                );
            }
            
            $this->add_building($xx, $yy, $buildingtmp);
            
            $dbbuilding->next();
        }
        
        // ------- ajout des exploitations
        
        $dbex = new DBCollection(
            "SELECT id,x,y,name,type,level FROM Exploitation WHERE map=" . $map . " AND x<=" . ($this->radius) . "+" . $xp . " AND x>=-" . ($this->radius) . "+" . $xp . " AND y<=" .
                 ($this->radius) . "+" . $yp . " AND y>=-" . ($this->radius) . "+" . $yp, $db, 0, 0);
        
        while (! $dbex->eof()) {
            $xx = $dbex->get("x");
            $yy = $dbex->get("y");
            $exName = $dbex->get("name");
            $exType = $dbex->get("type");
            $exLevel = $dbex->get("level");
            $distVal = max(abs($xp - $xx), abs($yp - $yy));
            
            $exploitationtmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "dist" => $distVal,
                "name" => $exName,
                "type" => $exType,
                "level" => $exLevel
            );
            
            $this->add_exploitation($xx, $yy, $exploitationtmp);
            
            $dbex->next();
        }
    }

    public function build_items()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        // Equipment
        $dbequipment = new DBCollection(
            "SELECT id,x,y,name,po,extraname,level,id_BasicEquipment,maudit FROM Equipment WHERE  map=" . $map . " AND  inbuilding=0 AND state=\"Ground\" AND map=" . $map .
                 " AND x<=" . $this->radius . "+" . $xp . " AND x>=-" . $this->radius . "+" . $xp . " AND y<=" . $this->radius . "+" . $yp . " AND y>=-" . $this->radius . "+" . $yp, 
                $db, 0, 0);
        
        while (! $dbequipment->eof()) {
            $xx = $dbequipment->get("x");
            $yy = $dbequipment->get("y");
            $level = $dbequipment->get("level");
            
            if ($curplayer->canSeeCase($xx, $yy, $db)) {
                if ($dbequipment->get("name") != "") {
                    if ($dbequipment->get("id_BasicEquipment") > 499) {
                        
                        $name = "<td colspan=2>" . $dbequipment->get("name") . " (id " . $dbequipment->get("id") . ")";
                        
                        $itemtmp = array(
                            "xx" => $xx,
                            "yy" => $yy,
                            "name" => $name,
                            "level" => $level
                        );
                        $this->add_item($xx, $yy, $itemtmp);
                    } else {
                        $name = " <td width=50px >Niv. " . $dbequipment->get("level") . " </td>";
                        $name .= "<td><span class='" . ($dbequipment->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($dbequipment->get("name")) . " (id " .
                             $dbequipment->get("id") . ")</span>";
                        
                        if ($dbequipment->get("extraname") != "") {
                            // $tlevel=$dbequipment->get("templateLevel");
                            $name .= " <span class='template'>" . localize($dbequipment->get("extraname")) . "</span>";
                        }
                        
                        $itemtmp = array(
                            "xx" => $xx,
                            "yy" => $yy,
                            "name" => $name,
                            "level" => $level
                        );
                        $this->add_item($xx, $yy, $itemtmp);
                    }
                } else {
                    $name = "<td colspan=2>" . $dbequipment->get("po") . " " . localize("Pièce(s) d'Or");
                    $itemtmp = array(
                        "xx" => $xx,
                        "yy" => $yy,
                        "name" => $name
                    );
                    $this->add_gold($xx, $yy, $itemtmp);
                }
            }
            
            $dbequipment->next();
        }
    }
    // ------------------------------- Construction de la rose des vents --------------------------------
    public function build_wrs()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        $map = $curplayer->get("map");
        
        $query = "SELECT
					p.id,p.x,p.y
				FROM 
					Player as p
				WHERE
					id=" . $id;
        
        $dbwindroses = new DBCollection($query, $db, 0, 0);
        $distVal = 0;
        while (! $dbwindroses->eof()) {
            $xx = $dbwindroses->get("x");
            $yy = $dbwindroses->get("y");
            
            $pjtmp = array(
                "xx" => $xx,
                "yy" => $yy,
                "id" => $id
            );
            $this->add_wrs($xx, $yy, $pjtmp);
            
            $dbwindroses->next();
        }
    }
    // ---------------------------------- FIN CONSTRUCTION ROSE DES VENTS -----------------------
    public function build_pj()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $id = $curplayer->get("id");
        $map = $curplayer->get("map");
        
        $idteam = $curplayer->get("id_Team");
        $query = "SELECT
				p.id,p.name,p.gender,p.hp,p.currhp,p.id_Team,p.x,p.y,p.id_BasicRace,p.picture,p.authlevel,
				p.level,p.racename,t.name as alliegance,
				ppj.type as dppj,
				po.type as dpo,
				opj.type as dopj,
				oo.type as doo
			FROM 
				Player as p
				LEFT JOIN Team as t ON t.id=p.id_Team
				LEFT JOIN PJAlliancePJ as ppj ON p.id=ppj.id_Player\$dest and ppj.id_Player\$src='$id'
				LEFT JOIN PJAlliance as po ON p.id_Team=po.id_Team and po.id_Player\$src='$id'
				LEFT JOIN TeamAlliancePJ as opj ON p.id=opj.id_Player and opj.id_Team\$src='$idteam'
				LEFT JOIN TeamAlliance as oo ON p.id_Team=oo.id_Team\$dest and oo.id_Team\$src='$idteam'
			WHERE
				map=" . $map . " AND inbuilding=0 AND disabled=0 AND status='PC' 
				AND x between " . $xp . "-" . $this->radius . " and " . $xp . "+" . $this->radius . " and y between " . $yp . "-" . $this->radius . " and " . $yp . "+" . $this->radius .
             " and (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $this->radius . "
			GROUP BY p.id
			ORDER BY alliegance, level DESC";
        
        $dbplayer = new DBCollection($query, $db, 0, 0);
        $state = array(
            localize("Agonisant"),
            localize("Gravement blessé(e)"),
            localize("Blessé(e)"),
            localize("Légèrement blessé(e)"),
            ""
        );
        $state[- 1] = "mort";
        
        $distVal = 0;
        while (! $dbplayer->eof()) {
            $xx = $dbplayer->get("x");
            $yy = $dbplayer->get("y");
            
            $opponent = new Player();
            $opponent->load($dbplayer->get("id"), $db);
            $hidden = $curplayer->canSeePlayer($opponent, $db);
            if (($hidden == 1) || ($hidden == 2)) {
                $level = $dbplayer->get("level");
                $hp = $dbplayer->get("hp");
                $curstate = min(floor($dbplayer->get("currhp") * 5 / $hp), 4);
                $distVal = max(abs($xp - $xx), abs($yp - $yy));
                $id_Team = $dbplayer->get("id_Team");
                
                if (($value = $dbplayer->get("dppj")) != null) {
                    $style = "self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } elseif (($value = $dbplayer->get("dopj")) != null) {
                    $style = "guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } else
                    $style = "stylepc";
                
                if (($value = $dbplayer->get("dpo")) != null) {
                    $gc = " self_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } elseif (($value = $dbplayer->get("doo")) != null) {
                    $gc = " guild_" . ((strtolower($value) == "ennemi") ? "ennemy" : "ally");
                } else
                    $gc = "styleall";
                
                $gc = ($id_Team == $idteam) ? "myalliegence" : $gc;


                $name = '';
                if ($dbplayer->get("id") != $id) {
                    $name .= Envelope::render($dbplayer->get('name'));
                }

                $name .= "<a href='../conquest/profile.php?id=" . $dbplayer->get("id") .
                     "' class='$style popupify'>" . $dbplayer->get("name") . "</a>";
                
                $alliegance = "<a href='../conquest/alliegance.php?id=$id_Team' class='$gc popupify'>" .
                     $dbplayer->get("alliegance") . "</a>";
                
                $pjtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "level" => $level,
                    "guildcolor" => $gc,
                    "dist" => $distVal,
                    "name" => $name,
                    "cleanName" => $dbplayer->get('name'),
                    "race" => $dbplayer->get("racename"),
                    "id_BasicRace" => $dbplayer->get("id_BasicRace"),
                    "id" => $dbplayer->get("id"),
                    "alliegance" => $alliegance,
                    "id_Team" => $id_Team,
                    "state" => $state[$curstate],
                    "hidden" => $hidden,
                    "gender" => $dbplayer->get("gender"),
                    "picture" => $dbplayer->get("picture"),
                    "authlevel" => $dbplayer->get("authlevel")
                );
                $this->add_pj($xx, $yy, $pjtmp);
            }
            $dbplayer->next();
        }
    }

    function build_pnj()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        
        $dbnpc = new DBCollection(
            "SELECT p.id,t.name FROM Player as p LEFT JOIN Team as t ON t.id=p.id_Team WHERE map=" . $map . " AND inbuilding=0 AND disabled=0 AND status='NPC' AND x between " . $xp .
                 "-" . $this->radius . " and " . $xp . "+" . $this->radius . " and y between " . $yp . "-" . $this->radius . " and " . $yp . "+" . $this->radius . " and (abs(x-" .
                 $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=" . $this->radius . " order by level desc, racename", $db, 0, 0);
        $state = array(
            localize("Agonisant"),
            localize("Gravement blessé(e)"),
            localize("Blessé(e)"),
            localize("Légèrement blessé(e)"),
            ""
        );
        
        while (! $dbnpc->eof()) {
            
            $opponent = new Player();
            $opponent->load($dbnpc->get("id"), $db);
            $hidden = $curplayer->canSeePlayer($opponent, $db);
            
            if ($opponent->get("id_Owner") != 0) {
                $dbowner = new DBCollection("SELECT name FROM Player WHERE id=" . $opponent->get("id_Owner"), $db, 0, 0);
                if (! $dbowner->eof()) {
                    $owner = " appartenant à <a href='../conquest/profile.php?id=" . $opponent->get("id_Owner") .
                         "' class='stylepc popupify'>" . $dbowner->get("name") . "</a>";
                } else {
                    $owner = "";
                }
            } else
                $owner = "";
            
            if ($hidden) {
                $hp = $opponent->get("hp");
                $curstate = min(floor($opponent->get("currhp") * 5 / $hp), 4);
                $xx = $opponent->get("x");
                $yy = $opponent->get("y");
                $level = $opponent->get("level");
                $distVal = max(abs($xp - $xx), abs($yp - $yy));
                $comp = "";
                
                if ($dbnpc->get("name") != '') // Pour l'anim les templier du chaos
                    $team = "<div class='guild_ennemy'>" . $dbnpc->get("name") . "</div>";
                else
                    $team = '';
                
                if ($opponent->get("name") != '') {
                    $name = "<a href=\"../conquest/profile.php?id=" . $dbnpc->get("id") .
                         "\" class='stylenpc popupify'>" . localize($opponent->get("name")) . "</a> (id : " . $dbnpc->get("id") . ") " . $team . " " . $comp;
                } else {
                    $name = "<a href=\"../conquest/profile.php?id=" . $dbnpc->get("id") .
                         "\" class='stylenpc popupify'>" . localize($opponent->get("racename")) . "</a> (id : " . $dbnpc->get("id") . ") " . $team . " " . $comp;
                }
                
                $pnjtmp = array(
                    "xx" => $xx,
                    "yy" => $yy,
                    "level" => $level,
                    "dist" => $distVal,
                    "name" => $name,
                    "id" => $dbnpc->get("id"),
                    "state" => $state[$curstate],
                    "id_BasicRace" => $opponent->get("id_BasicRace"),
                    "owner" => $owner,
                    "picture" => $opponent->get("picture")
                );
                
                $this->add_pnj($xx, $yy, $pnjtmp);
            }
            $dbnpc->next();
        }
    }

    /*
     * public function can_move($curplayer){
     * return !$curplayer->get("incity") && ($curplayer->get("ap") > ((($curplayer->get("encircle") > 0)? 5 : 0))) && ($curplayer->get("state") != 2 && ($curplayer->get("rider") !=2));
     *
     * }
     */
    public function add_item($xx, $yy, $pjtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_item($pjtmp);
    }

    public function add_gold($xx, $yy, $pjtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_gold($pjtmp);
    }

    public function add_portail($xx, $yy, $portailtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_portail($portailtmp);
    }

    public function add_exploitation($xx, $yy, $exploitationtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_exploitation($exploitationtmp);
    }

    public function add_place($xx, $yy, $arr)
    {
        $this->dist[] = $arr["dist"];
        $this->data[] = "<tr><td class='mainbgbody' align='center'> " . $arr["dist"] . " </td>\n" . "<td class='mainbgbody'>" . $arr["name"] . "</td>\n" .
             "<td class='mainbgbody' align='right'> " . $arr["xx"] . " </td>\n" . "<td class='mainbgbody' align='right'> " . $arr["yy"] . " </td></tr>\n";
    }

    public function add_building($xx, $yy, $pjtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_building($pjtmp);
    }
    
    // ----------------------------------- Ajout de la rose des vents sous le joueur ----------------------------------
    public function add_wrs($xx, $yy, $pjtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_rose($pjtmp);
    }
    // -------------------- FIN ROSE DES VENTS ----------------------
    public function add_pj($xx, $yy, $pjtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->list[$pjtmp['id']] = $pjtmp['cleanName'];
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_pj($pjtmp);
    }

    public function add_pnj($xx, $yy, $pjtmp)
    {
        $curplayer = $this->curplayer;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $this->minimap[$yy - $yp + $this->radius][$xx - $xp + $this->radius]->add_pnj($pjtmp);
    }

    public function toString()
    {
        return $this->str;
    }

    public function draw_map()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $this->db = $db;
        // $can_move = $this->can_move($curplayer);
        $minimap = $this->minimap;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        
        $str = "";
        $dbpoll = new DBCollection("SELECT * FROM Nacripoll_Main WHERE status='active'", $db);
        if ($dbpoll->count() > 0) {
            $participant_list = unserialize($dbpoll->get('participant_list'));
            if (! in_array($curplayer->get('id_Member'), $participant_list))
                $str = "<div class='ig-announcement'>Un sondage est en cours, il est accessible en cliquant sur le menu conquête.</div>";
            else
                $str = "";
            // TODO trouver un cadre
        }
        
        if ($curplayer->get("room") == TOWER_ROOM || $curplayer->get("room") == PALACE_BELL_TOWER_ROOM || $curplayer->get("room") == TEMPLE_ROOF_ROOM) {
            $str .= "<form method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=view2d&bottom=viewpanel' target='_self'>\n";
            $str .= "<input  type='submit' style='position:absolute; top:50px; left:100px';' name='gotoroom' value=\"Retour à l'accueil\"/> ";
            $str .= "<input type='hidden' name='room' value='" . ENTRANCE_ROOM . "'/>";
            $str .= "</form>";
        }
        
        $radius = $this->radius;
        
        $caseborder = false;
        
        $vbshift = 0;
        $hbshift = 0;
        if (($curplayer->get("inbuilding") != 0) &&
             ($curplayer->get("room") != TOWER_ROOM && $curplayer->get("room") != PALACE_BELL_TOWER_ROOM && $curplayer->get("room") != TEMPLE_ROOF_ROOM)) {
            $vbshift = 170;
            $hbshift = 40;
        }
        $posv1 = - 45 + $vbshift;
        $posh1 = - 70 + $hbshift;
        // -------------------------- 1ère boucle créant les terrains sur la vue 2 d ---------------------
        for ($i = 0; $i <= ($radius * 2); $i ++) {
            $posv1 = $posv1 + 36;
            $posh1 = $posh1 + 21;
            
            for ($j = 0; $j <= ($radius * 2); $j ++) {
                
                $posh = $j * 72 + $posh1;
                $posv = $posv1 + 9 * $j;
                $curX = $xp - $radius + $j;
                $curY = $yp - $radius + $i;
                
                if (distHexa($curX, $curY, $xp, $yp) <= $radius && $curX >= 0 && $curX < MAPWIDTH && $curY >= 0 && $curY < MAPHEIGHT) {
                    
                    $str .= "<div style='position:absolute; top:" . $posv . "px; left:" . $posh . "px;' class='" . $minimap[$i][$j]->type . "'";
                    
                    $str .= " >";
                    $str .= "</div>";
                }
            }
        }
        // ---------------------------- Insertion de la rose des vents ente les deux boucle -------------------------------------------------
        $activerose = new DBCollection("SELECT windRose FROM Member LEFT JOIN MemberOption ON Member.id=id_Member WHERE id_Member=" . $curplayer->get("id_Member"), $db);
        while (! $activerose->eof()) {
            if ($activerose->get("windRose") == 0 && $curplayer->get("inbuilding") == 0)
                $str .= "<div  class='minimapwindroses' style='background-image: url(../pics/conquest/rose6_petit_directions.png);visibility:visible'></div>";
            elseif ($activerose->get("windRose") == 1)
                $str .= "<div  class='minimapwindroses' style='background-image: url(../pics/conquest/rose6_petit_directions.png);visibility:hidden'></div>";
            $activerose->next();
        }
        
        $minimap = $this->minimap;
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $curX = 0;
        $curY = 0;
        $posv1 = - 45 + $vbshift;
        $posh1 = - 70 + $hbshift;
        // ------------------------------------------------ boucle permettant de remplir les cases ---------------------------------------------
        for ($i = 0; $i <= ($radius * 2); $i ++) {
            $posv1 = $posv1 + 36;
            $posh1 = $posh1 + 21;
            
            for ($j = 0; $j <= ($radius * 2); $j ++) {
                
                $posh = $j * 72 + $posh1;
                $posv = $posv1 + 9 * $j;
                $curX = $xp - $radius + $j;
                $curY = $yp - $radius + $i;
                
                if (distHexa($curX, $curY, $xp, $yp) <= $radius && $curX >= 0 && $curX < MAPWIDTH && $curY >= 0 && $curY < MAPHEIGHT) {
                    
                    // -------- création d'une div avec un fichier png transparent à 99%
                    $str .= "<div style='position:absolute; top:" . $posv . "px; left:" . $posh . "px'; class='terrainvide'";
                    
                    $classname = $minimap[$i][$j]->type;
                    $panelid = "panel_" . $curX . "_" . $curY;
                    $onClick = " onClick='javascript:showPanel(\"" . $panelid . "\");'";
                    $click = "onClick='javascript:highlightCurrentTile(this,\"terrainvide\");'";
                    // $click=" onClick='func(this,this.id,\"".$classname."\");' onMouseOver='mover(this,this.id,\"".$classname."\");' onMouseOut='mout(this,this.id,\"".$classname."\");'";
                    // $onMouseOut="onMouseOut='javascript:highlightCurrentTile(this,\"".$classname."\");'";
                    
                    $str .= $click;
                    $str .= " >";
                    
                    if (in_array(1, $minimap[$i][$j]->flags))
                        $str .= $minimap[$i][$j]->to_case();
                        
                        // $str .= "<br/> dist :".distHexa($curX,$curY,$xp,$yp)."";
                        
                    // $str.= "<br/>(".$curX." : ".$curY.")";
                        // $str.= "<br/>(".$minimap[$i][$j]->xx." : ".$minimap[$i][$j]->yy.")";
                    
                    if ($caseborder == true)
                        $borderstyle = "isoborder";
                    else
                        $borderstyle = "noborder";
                    
                    $str .= "<div class='" . $borderstyle . " " . $classname . "'";
                    
                    $str .= $onClick;
                    
                    $str .= "></div>";
                    
                    $str .= "</div>";
                }
            }
        }
        if (count($this->list) > 1) {
            $players = $this->list;
            unset($players[$this->curplayer->get('id')]);
            $str .= '<div class="send-to-all">'.Envelope::render($players).'</div>';
        }
        return $str;
    }
}

