<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/class/Detail.inc.php");
// require_once(HOMEPATH."/class/HFightMsg.inc.php");
// require_once(HOMEPATH."/class/HGiveXPMsg.inc.php");
// require_once(HOMEPATH."/class/HGiveMoneyMsg.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();
$nacridan = new NacridanModule($sess, $auth, $db, 1);

$lang = $auth->auth['lang'];
Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = null;
$prefix = "";
$players = $nacridan->loadSessPlayers();

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$idmsg = quote_smart($_GET["id"]);

$curplayer = $nacridan->loadCurSessPlayer($db);

if ($curplayer->get("authlevel") > 10 && isset($_GET["id"])) {
    $id = $_GET["id"];
    $err = null;
    if (isset($_POST["Enable"])) {
        $err = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
        $err .= enable($curplayer, $id, $db);
        $err .= "</td></tr></table>";
    }
    
    if (isset($_POST["Disable"])) {
        if (! $nacridan->isRepostForm()) {
            $err = "<table class='maintable centerareawidth'><tr><td class='mainbgtitle'>";
            $err .= disable($curplayer, $id, $db);
            $err .= "</td></tr></table>";
        } else {
            $err = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
        }
    }
    
    $body = "";
    
    if (isset($_POST["Modify"])) {
        $dbs = new DBCollection("SELECT * FROM Banned WHERE id_Player\$target=" . $id, $db);
        $body = $dbs->get("body");
    }
    
    $dbm = new DBCollection("SELECT body,id_Player,recipient,recipientid,title,date,name FROM MailBody LEFT JOIN Player AS P1 ON id_Player=P1.id WHERE MailBody.id=" . $idmsg . " ORDER BY date DESC", $db);
    
    if ($err == null) {
        $str = "<div class='maintable centerareawidth mainbgtitle'>";
        $str .= "<b><h1>" . localize('D É S A C T I V A T I O  N') . "</h1></b>";
        $str .= "</div>";
        $str .= "<form action='disable.php?id=" . $id . "' method='POST'>";
        $str .= "<table class='maintable'>";
        $str .= "<tr><td colspan='6'class='mainbglabel' width='150px'><b>" . localize('Motif de la désactivation :') . "</b></td><td class='mainbgtitle'><textarea id='Body' style='width:450px;' name='Body' rows='9' cols='40'>" . $body . "</textarea></td></tr>";
        
        $str .= "<tr><td><input id='Disable' type='submit' name='Disable' value='" . localize('Désactivation') . "' />";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</td></tr></table></form>\n";
    } else {
        $str = $err;
    }
} else {
    $str = localize("Droits insuffisants");
}

$MAIN_BODY->add($str);
$MAIN_PAGE->render();

function enable(&$player, $id, &$db)
{
    _setDisabled(0, $id, $db);
    $dbi = new DBCollection("DELETE FROM Banned WHERE id_Player\$target=" . $id, $db, 0, 0, false);
    return localize("Le personnage numéro {id} a été réactivé", array(
        "id" => $id
    ));
}

function disable(&$player, $id, &$db)
{
    _setDisabled(99, $id, $db);
    if (isset($_POST["Body"])) {
        $body = $_POST["Body"];
    } else {
        $body = "";
    }
    $date = date("Y-m-d H:i:s");
    
    $dbi = new DBCollection("DELETE FROM Banned WHERE id_Player\$target=" . $id, $db, 0, 0, false);
    $dbi = new DBCollection("INSERT INTO Banned (id_Player,body,id_Player\$target,date) VALUES (" . $player->get("id") . ",\"" . addslashes(stripslashes($body)) . "\"," . $id . ",\"" . $date . "\")", $db, 0, 0, false);
    
    return localize("Le personnage numéro {id} a été désactivé", array(
        "id" => $id
    ));
}

function _setDisabled($value, $id, &$db)
{
    $dbm = new DBCollection("SELECT Member.* FROM Member LEFT JOIN Player ON Player.id_Member=Member.id WHERE Player.id=" . $id, $db);
    if (! $dbm->eof()) {
        $dbp = new DBCollection("SELECT * FROM Player WHERE id_Member=" . $dbm->get("id"), $db, 0, 0);
        if (is_object($dbp)) {
            while (! $dbp->eof()) {
                $dbu = new DBCollection("UPDATE Player SET disabled=" . $value . " WHERE id=" . $dbp->get("id"), $db, 0, 0, false);
                $dbp->next();
            }
        }
        /*
         * $opposite=null;
         *
         * if($value=99)
         * $opposite=0;
         *
         * if($value=0)
         * $opposite=99;
         *
         * $dbs=new DBCollection("SELECT * FROM Player WHERE id_Member=".$dbm->get("id")." AND value=".$opposite,$db,0,0);
         * $dbu=new DBCollection("UPDATE Member SET disabled=".$value." WHERE id=".$dbm->get("id"),$db,0,0);
         */
    }
}

?>

