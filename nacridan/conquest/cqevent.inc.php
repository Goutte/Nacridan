<?php

class CQEvent extends HTMLObject
{

    public $nacridan;

    public $players;

    public $db;

    public $style;

    public function CQEvent($nacridan, $style, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        if ($nacridan == null)
            $this->players = array();
        else
            $this->players = $this->nacridan->loadSessPlayers();
        
        $this->style = $style;
    }

    public function toString()
    {
        /* ************************************************** LES EVENTS DES ACTIONS ****************************************************** */
        $db = $this->db;
        
        if (is_object($this->nacridan))
            $curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (! isset($_GET["id"])) {
            $id = $curplayer->get("id");
        } else {
            $id = quote_smart($_GET["id"]);
        }
        
        if (isset($_POST["__stepEvt"])) {
            if (is_numeric($_POST["__stepEvt"])) {
                $step = quote_smart($_POST["__stepEvt"]);
                if (isset($_POST["NextEvt"]) || isset($_POST["NextEvt_x"]))
                    $step += 1;
                if (isset($_POST["PrevEvt"]) || isset($_POST["PrevEvt_x"]))
                    $step -= 1;
                if (isset($_POST["NextEvt10"]) || isset($_POST["NextEvt10_x"]))
                    $step += 10;
                if (isset($_POST["PrevEvt10"]) || isset($_POST["PrevEvt10_x"]))
                    $step -= 10;
            } else {
                $step = 0;
            }
        }
        
        if (! isset($step) || $step < 0)
            $step = 0;
            
            /*
         * auth = 0 -> visible de tous
         * auth = 1 -> n'apparait pas sur les évènement du src --> feinte pour les sorts de zone
         * auth = 2 --> n'est pas visible pas le dest ni sur ses évènements ni sur les évènement du src
         */
            
        // Disjonction de cas suivant si on mate des évènements
        if (isset($curplayer) && $curplayer->get("authlevel") > 10)
            $cond = "((id_Player\$src=" . $id . " AND auth != 1) OR (id_Player\$dest=" . $id . "))";
        elseif (isset($curplayer) && $curplayer->get("id") == $id)
            $cond = "((id_Player\$src=" . $id . " AND auth != 1) OR (id_Player\$dest=" . $id . " AND auth<2))";
        else
            $cond = "((id_Player\$src=" . $id . " AND auth=0) OR (id_Player\$dest=" . $id . " AND auth<2))";
        
        if (isset($_POST["idBasicEventType"]) && $_POST["idBasicEventType"] != "-1") {
            $cond .= " and (BasicEventType.id = " . $_POST["idBasicEventType"] . " or RolePlayAction.idBasicEventType = " . $_POST["idBasicEventType"] . ")";
        }
        
        $dbe = new DBCollection(
            "SELECT Event.*,ifnull(BasicEventType.code,RolePlayAction.name) as codeEvent,ifnull(BasicEvent.description,RolePlayAction.description) as descriptionEvent, P1.id AS idsrc,P1.name AS namesrc,P1.gender AS gendersrc,P1.status as statussrc,P2.id AS iddest,P2.name AS namedest,P2.gender AS genderdest,P2.status as statusdest, BR1.name AS nameracesrc, BR1.gender AS racegendersrc, BR2.name AS nameracedest,BR2.gender AS racegenderdest FROM Event LEFT JOIN BasicEvent on BasicEvent.id = Event.typeEvent LEFT JOIN BasicEventType on BasicEvent.idBasicEventType = BasicEventType.id LEFT JOIN RolePlayAction on (2000+RolePlayAction.id) = Event.typeEvent LEFT JOIN Player as P1 ON id_Player\$src=P1.id LEFT JOIN Player as P2 ON id_Player\$dest=P2.id LEFT JOIN BasicRace as BR1 ON id_BasicRace\$src=BR1.id LEFT JOIN BasicRace as BR2 ON id_BasicRace\$dest=BR2.id WHERE " .
                 $cond . " ORDER BY date DESC", $db, $step * STEP_EVENT, STEP_EVENT);
        $data = array();
        $cptEvents = $dbe->count();
        
        while (! $dbe->eof()) {
            $owner = 0;
            $style = "";
            if (isset($this->players[$dbe->get("id_Player\$src")]) || (isset($curplayer) && $curplayer->get("authlevel") > 5) || isset($this->players[$dbe->get("id_Player\$dest")])) {
                $owner = 1;
                switch ($dbe->get("codeEvent")) {
                    case "DÉPLACEMENT":
                    case "OBJET":
                    case "PARCHEMIN":
                    case "POTION":
                    case "BOUSCULADE":
                    case "GAIN":
                    case "RESSOURCE":
                        $style = "classicevent";
                        break;
                    
                    case "COMPÉTENCE":
                    case "VOL":
                    case "LARCIN":
                    case "ATTAQUE":
                    case "BOLAS":
                    case "DESARMER":
                    case "COMPÉTENCE":
                        $style = "attackevent";
                        break;
                    
                    case "MORT":
                        $style = "deathevent";
                        break;
                    
                    case "SORTILÈGE":
                    case "TÉLÉPORTATION":
                        $style = "magicevent";
                        break;
                    
                    case "DON":
                    case "NIVEAU":
                    case "EXPÉRIENCE":
                        $style = "givemoneyevent";
                        break;
                    
                    case "SAVOIR-FAIRE":
                    case "SERVICE AUBERGE":
                    case "SERVICE BANCAIRE":
                    case "SERVICE ECHOPPE":
                    case "SERVICE GUILDE":
                    case "SERVICE TEMPLE":
                    case "REPOS":
                    case "RECEPTION DE COLIS":
                    case "PUTSCH":
                    case "PRISON":
                    case "ENVOI DE COLIS":
                    case "GESTION VILLAGE":
                    case "CARAVANE":
                    case "BÂTIMENT":
                    case "ARRESTATION":
                    case "APPRENTISSAGE":
                        $style = "talentevent";
                        break;
                }
            }
            
            $time = gmstrtotime($dbe->get("date"));
            $date = date("Y-m-d H:i:s", $time);
            
            if (isset($curplayer) && ($owner || $curplayer->get("authlevel") > 5) && $dbe->get("idtype") != 0)
                $type = "<a href=\"" . CONFIG_HOST . "/conquest/history.php?type=" . $dbe->get("type") . "&id=" . $dbe->get("idtype")
                    . "\"  class='" . $style . " popupify'>" . $dbe->get("codeEvent") . "</a>";
            else {
                $type = localize($dbe->get("codeEvent"));
            }

            if ($this->nacridan != null)
                $link = '/conquest/profile.php';
            else
                $link = '/main/profile.php';
            
            $art = "";
            $envelopeSrc = '';
            $namePlayer = $dbe->get("namesrc");
            if ($namePlayer == "") {
                $namePlayer = $dbe->get("nameracesrc");
                $namePlayer = localize($namePlayer);
                
                if ($dbe->get("racegendersrc") == "M")
                    $art = localize("un") . " ";
                else
                    $art = localize("une") . " ";
                
                $style = "stylenpc";
            } else {
                $style = ($dbe->get("statussrc") == "PC") ? "stylepc" : "stylenpc";
                if ($this->isCurrentPlayerOrWatchedPlayer($dbe->get('idsrc'), $id, $curplayer)) {
                    $envelopeSrc = Envelope::render($namePlayer);
                }
            }
            $gendersrc = ($dbe->get("gendersrc") == "M") ? "" : localize("e");
            $namePlayer = $envelopeSrc . "<a href=\"" . CONFIG_HOST . $link. '?id=' . $dbe->get("id_Player\$src") . "\" "
                . "class='" . $style . " popupify'>" . $art . $namePlayer . "</a> (" . $dbe->get("id_Player\$src") . ")";

            
            $art = "";
            $envelopeTarget = '';
            $nameTarget = $dbe->get("namedest");
            if ($nameTarget == "") {
                $nameTarget = $dbe->get("nameracedest");
                $nameTarget = localize($nameTarget);
                
                if ($dbe->get("racegenderdest") == "M")
                    $art = localize("un") . " ";
                else
                    $art = localize("une") . " ";
                $style = "stylenpc";
            } else {
                $style = ($dbe->get("statusdest") == "PC") ? "stylepc" : "stylenpc";
                if ($this->isCurrentPlayerOrWatchedPlayer($dbe->get('iddest'), $id, $curplayer)) {
                    $envelopeTarget = Envelope::render($nameTarget);
                }
            }
            $genderdest = ($dbe->get("genderdest") == "M") ? "" : localize("e");
            $nameTarget = $envelopeTarget . "<a href=\"" . CONFIG_HOST . $link . "?id=" . $dbe->get("id_Player\$dest") . "\" "
                . "class='" . $style . " popupify'>" . $art . $nameTarget . "</a>" . " (" .
                     $dbe->get("id_Player\$dest") . ")";
            
            $body = localize($dbe->get("descriptionEvent"), 
                array(
                    "target" => $nameTarget,
                    "player" => $namePlayer,
                    "gendersrc" => $gendersrc,
                    "genderdest" => $genderdest
                ));
            
            $data[] = array(
                "date" => $date,
                "typeAction" => $type,
                "action" => $dbe->get("typeAction"),
                "idtype" => $dbe->get("idtype"),
                "type" => $dbe->get("type"),
                "eventBody" => $body,
                "ip" => $dbe->get("ip")
            );
            
            $dbe->next();
        }
        $extra = "";
        if (isset($_GET["body"])) {
            $extra = "&body=1";
        }
        if (isset($_GET["center"])) {
            $extra .= "&center=" . $_GET["center"];
        }
        
        $str = "<form name='form' action='?id=" . $id . $extra . "' method=post target='_self'>\n";
        $str .= "<table class='maintable " . $this->style . "'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('É V È N E M E N T S') . "</b></td>\n";
        $str .= "<td class='mainbgtitle' align='right'>Filtrer: ";
        $str .= "<select class='selector cqattackselectorsize' name='idBasicEventType' onchange='document.getElementById(\"__stepEvt\").value=0;submit();' align='right'>";
        $str .= "<option value='-1' selected='selected' >" . localize("-- Choisissez un type --") . "</option>";
        
        $dbe = new DBCollection("SELECT * FROM BasicEventType", $this->db);
        while (! $dbe->eof()) {
            if (isset($_POST["idBasicEventType"]) && $_POST["idBasicEventType"] == $dbe->get("id")) {
                $strSelected = "selected='selected'";
            } else {
                $strSelected = "";
            }
            $str .= "<option value='" . $dbe->get("id") . "' " . $strSelected . ">" . $dbe->get("code") . "</option>";
            $dbe->next();
        }
        $str .= "</select>";
        $str .= "</td>\n";
        
        if ($cptEvents == STEP_EVENT) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtnext' type='image' name='NextEvt' src='../pics/misc/left.gif' value='" . localize("previous") .
                 "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/left.gif' /></td>\n";
        }
        
        if ($step > 0) {
            $str .= "<td rowspan=2 width='35px'><input id='Previous' class='eventbtprevious' type='image' name='PrevEvt' src='../pics/misc/right.gif' value='" . localize("next") .
                 "' /></td>\n";
        } else {
            $str .= "<td rowspan=2 width='35px'><img src='../pics/misc/right.gif' /></td>\n";
        }
        
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        $str .= "<td class='mainbglabel' width='130px' align='center'>" . localize('Date') . "</td>\n";
        $str .= "<td class='mainbglabel' width='130px' align='center'>" . localize('Type') . "</td>\n";
        $str .= "<td class='mainbglabel' >" . localize('Détails') . "</td>\n";
        
        // echo $str;
        $str .= "</tr></table>";
        $str .= "<table class='maintable " . $this->style . "'>";
        
        foreach ($data as $arr) {
            $str .= "<tr><td class='mainbgbody' width='130px' align='center'> " . $arr["date"] . " </td>\n";
            $str .= "<td class='mainbgbody' width='130px' align='center'> " . $arr["typeAction"] . " </td>\n";
            $str .= "<td class='mainbgbody' align='left'> " . $arr["eventBody"] . " </td>";
            if (isset($curplayer) && $curplayer->get("authlevel") > 5) {
                $str .= "<td class='mainbgbody' align='left'> " . $arr["ip"] . " </td>";
                if ($arr["action"] == GIVE_MONEY) {
                    $id = quote_smart($arr["idtype"]);
                    $class = quote_smart($arr["type"]);
                    $event = new $class();
                    $event->load($id, $db);
                    $param = unserialize($event->get("body"));
                    $str .= "<td class='mainbgbody' align='left'> " . $param["VALUE"] . " </td>";
                } else {
                    $str .= "<td class='mainbgbody' align='left'></td>";
                }
            }
            $str .= "</tr>\n";
        }
        
        $str .= "</table>";
        $str .= "<input name='__stepEvt' id='__stepEvt' type='hidden' Value='" . $step . "' />\n";
        
        $str .= "</form>";
        return $str;
    }

    /**
     * Tells if the $idToCompare id is the one of the $curplayer or is $watchedId.
     *
     * @param $idToCompare
     * @param $watchedId
     * @param $curplayer
     * @return bool
     */
    private function isCurrentPlayerOrWatchedPlayer($idToCompare, $watchedId, $curplayer)
    {
        return $watchedId != $idToCompare && (!isset($curplayer) || $curplayer->get('id') != $idToCompare);
    }
}
?>

 
      
