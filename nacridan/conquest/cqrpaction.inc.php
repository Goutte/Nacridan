<?php

class CQRPAction extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQRPAction($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        if (isset($_GET["id"])) {
            $idaction = quote_smart($_GET["id"]);
            
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>";
            $str .= "Vous allez dépenser 1PA pour une action personnalisée.</td><td>";
            
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Confirmer' />";
            $str .= "<input name='action' type='hidden' value='" . RP_ACTION . "' />";
            $str .= "<input name='ACTION_ID' type='hidden' value='" . $idaction . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</td></tr></table>";
            $str .= "</form>";
        } else {
            $str = "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td>id de l'action non défini</td><td></table>";
        }
        
        return $str;
    }
}
?>
