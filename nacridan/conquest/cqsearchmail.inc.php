<?php

class CQSearchMail extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQSearchMail($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        if (isset($_POST["Reply"])) {
            $err = $this->reply($db);
        }
        
        if (isset($_POST["ReplyToAll"])) {
            $err = $this->replyToAll($db);
        }
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $err = "";
        $data = array();
        
        if (isset($_GET["search"])) {
            $search = quote_smart($_GET["search"]);
        } else {
            $search = 1;
        }
        
        if (isset($_POST["Important"])) {
            $err = $this->importantMail($search, $db);
        }
        
        if (isset($_GET["priority"])) {
            $err = $this->importantMail($search, $db);
        }
        
        if (isset($_GET["delete"])) {
            $err = $this->deleteMail($search, $db);
        }
        
        if (isset($_POST["Del"])) {
            $err = $this->deleteMail($search, $db);
        }
        
        if (isset($_POST["FlagRead"])) {
            $err = $this->FlagReadMail($db);
        }
        
        if (isset($_POST["Archive"])) {
            
            $dbr = new DBCollection("SELECT * FROM NameRepertory", $db);
            $norep = $dbr->count();
            $playerid = "";
            if ($norep == 0) // on vérifie qu'un répertoire à bien été créer
{
                $err = localize("Aucun répertoire existant. Veuillez en créer un");
                $arch = 0;
            } else {
                $arch = 1;
                $archive = $archivem = $this->getCheckedDelMailCond("id_MailBody");
                $archivem = $this->getCheckedDelMailCond("Mail.id_MailBody");
                $archivemtd = $this->getCheckedDelMailCond("MailTrade.id_MailBody");
                $archivems = $this->getCheckedDelMailCond("MailSend.id_MailBody");
                $archivemsModulo = $this->getCheckedDelMailCond2("MailSend.id_MailBody");
            }
        } else {
            $arch = 0;
            $archive = "";
            $archivem = "";
            $archivems = "";
            $archivemtd = "";
            $archivemsModulo = "";
        }
        
        // on test le déplacemnet des messages
        if (isset($_POST["Place"])) {
            if ($_POST["CHOOSE_REPERTORY_ID"] == 0) // on vérifie qu'un répertoire à bien été sélectionné
                $err = localize("Aucun répertoire sélectionner. Veuillez en choisir un dans la liste.");
            else {
                $playerid = $id;
                $err = $this->archiveMail($_POST["select"], $_POST["send"], $playerid, $db);
            }
        }
        
        $target = "/conquest/conquest.php?center=search&search=" . $search;
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr><td class='mainbgtitle tabmenu'>" . "<label>Rechercher :  <input type='text' name='query' size='20px'/> " . "</label>
	<input id='Search' type='submit' name='Search' value='" . localize("Recherche") . "' />
	<span class='mainbgtitle' style='font-size: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp; Attention la recherche ne s'effectue pas dans les alertes.</span></td></tr>";
        $str .= "</table>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize('M E S S A G E R I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span> \n";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'>";
        $str .= "<a href='../conquest/conquest.php?center=compose' class='tabmenu'>" . localize('Composer un Message') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Message(s) Reçu(s)') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=mail&mail=2' class='tabmenu'>" . localize('Message(s) Envoyé(s)') . "</a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=contact' class='tabmenu'>" . localize('Contacts') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=alias' class='tabmenu'>" . localize('Alias') . " </a> |\n";
        $str .= "<a href='../conquest/conquest.php?center=archive' class='tabmenu'>" . localize('Archive(s)') . " </a>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "</table>";
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbglabel'>";
        $str .= "Résultat de la recherche: ";
        $str .= "</td>";
        $str .= "</tr>\n";
        $str .= "</table>";
        
        if (isset($_POST['query']) && $_POST['query'] != NULL) // on vérifie d'abord l'existence du POST et aussi si la requete n'est pas vide.
{
            
            $str .= "<table class='maintable centerareawidth'>";
            $query = quote_smart($_POST['query']);
            $id = $curplayer->get("id");
            
            $dbm = new DBCollection(
                "SELECT Mail.id AS idMail,Mail.id_MailBody,MailBody.recipient,MailBody.title,MailBody.body,Mail.new,Mail.id_Player\$receiver,
		Mail.important,NameRepertory.name_Repertory,Mail.date
		FROM Mail 
		LEFT JOIN MailBody ON id_MailBody=MailBody.id 
		LEFT JOIN NameRepertory on NameRepertory.id = Mail.id_repertory
		WHERE id_Player\$receiver=" . $id . " ORDER BY Mail.date DESC", $db);
            
            $dbmtd = new DBCollection(
                "SELECT MailTrade.id AS idMailTrade,MailTrade.id_MailBody,MailBody.recipient,MailBody.title,MailBody.body,MailTrade.new,
		MailTrade.id_Player\$receiver,MailTrade.id_Player\$sender,MailTrade.important,NameRepertory.name_Repertory,MailTrade.date
		FROM MailTrade
		LEFT JOIN MailBody ON id_MailBody=MailBody.id 
		LEFT JOIN NameRepertory on NameRepertory.id = MailTrade.id_repertory
		WHERE id_Player\$receiver=" . $id . " ORDER BY MailTrade.date DESC", $db);
            
            $dbmsd = new DBCollection(
                "SELECT MailSend.id AS idMailSend,MailBody.title,MailBody.body,MailBody.recipient,MailBody.date,MailSend.id_Player,MailSend.important,
		NameRepertory.name_Repertory,MailSend.id_MailBody 
		FROM MailSend LEFT JOIN MailBody ON id_MailBody=(MailBody.id)
		LEFT JOIN NameRepertory on NameRepertory.id = MailSend.id_repertory
		WHERE MailSend.id_Player=" . $id . " ORDER BY MailBody.date DESC", $db);
            
            $dbarch = new DBCollection(
                "SELECT MailArchive.id AS idMailSend,MailArchive.id_MailBody,MailBody.title,MailBody.body,MailBody.recipient,MailArchive.new,
		MailArchive.id_Player\$receiver,MailArchive.date,NameRepertory.name_Repertory,MailArchive.important,MailArchive.id_Player 
		FROM MailArchive  
		LEFT JOIN MailBody ON id_MailBody=MailBody.id 
		LEFT JOIN NameRepertory on NameRepertory.id = MailArchive.id_repertory
		WHERE MailArchive.id_Player=" . $id . " OR MailArchive.id_Player\$receiver=" . $id . " ORDER BY MailArchive.date DESC", 
                $db);
            
            $first = 1;
            $names = "";
            $arr = array();
            $p = 0;
            $nb_result = 0;
            while (! $dbm->eof()) // On effectue la recherche sur la messagerie standard
{
                
                $findme = $query;
                $titlesh = $dbm->get("title");
                $bodysh = $dbm->get("body");
                $playersh = $dbm->get("recipient");
                
                $search_title = stripos($titlesh, $findme);
                $search_body = stripos($bodysh, $findme);
                $search_player = stripos($playersh, $findme);
                
                $idm = $dbm->get('id_MailBody');
                $idbody = 0;
                
                $title = "";
                $player = "";
                $body = "";
                // On test les valeurs retourner par stripose qui compare 2 chaîne de caractère et qui renvoie la position de celle-ci
                // Si la position est retourner on peut tester que celà est différent de faux.
                if ($search_title !== false || $search_body !== false || $search_player !== false) {
                    if ($dbm->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $idm . "&search=1&\" class='none popupify'>" . $dbm->get("title") . "</a>";
                    
                    $body = $dbm->get('body');
                    $player = $dbm->get('recipient');
                    
                    if ($dbm->get("important") == "1")
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $repertory = "";
                    if ($dbm->get("name_Repertory") != "")
                        $repertory = $dbm->get("name_Repertory");
                    
                    if ($dbm->get("new") == "1")
                        $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                    else
                        $img = "";
                    
                    $data[] = array(
                        "title" => $title,
                        "name" => $player,
                        "body" => $body,
                        "imp" => $important,
                        "new" => $img,
                        "rep" => $repertory,
                        "id" => $idm,
                        "idbody" => $idbody
                    );
                    $nb_result = $p + 1;
                }
                $dbm->next();
            }
            
            while (! $dbmtd->eof()) // On effectue la recherche sur la messagerie commercial
{
                
                $findme = $query;
                $titlesh = $dbmtd->get("title");
                $bodysh = $dbmtd->get("body");
                $playersh = $dbmtd->get("recipient");
                
                $search_title = stripos($titlesh, $findme);
                $search_body = stripos($bodysh, $findme);
                $search_player = stripos($playersh, $findme);
                
                $idm = $dbmtd->get('id_MailBody');
                $idbody = 0;
                
                $title = "";
                $player = "";
                $body = "";
                if ($search_title !== false || $search_body !== false || $search_player !== false) {
                    if ($dbmtd->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $idm . "&search=1&\" class='none popupify'>" . $dbmtd->get("title") . "</a>";
                    
                    $body = $dbmtd->get('body');
                    $player = $dbmtd->get('recipient');
                    
                    if ($dbmtd->get("important") == "1")
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $repertory = "";
                    if ($dbmtd->get("name_Repertory") != "")
                        $repertory = $dbmtd->get("name_Repertory");
                    
                    if ($dbmtd->get("new") == "1")
                        $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                    else
                        $img = "";
                    
                    $data[] = array(
                        "title" => $title,
                        "name" => $player,
                        "body" => $body,
                        "imp" => $important,
                        "new" => $img,
                        "rep" => $repertory,
                        "id" => $idm,
                        "idbody" => $idbody
                    );
                    
                    $nb_result = $p + 1;
                }
                $dbmtd->next();
            }
            
            while (! $dbmsd->eof()) { // On effectue la recherche sur les messages envoyés
                
                $findme = $query;
                $titlesh = $dbmsd->get("title");
                $bodysh = $dbmsd->get("body");
                $playersh = $dbmsd->get("recipient");
                
                $search_title = stripos($titlesh, $findme);
                $search_body = stripos($bodysh, $findme);
                $search_player = stripos($playersh, $findme);
                
                $idm = $dbmsd->get('id_MailBody');
                $idbody = 0;
                
                $title = "";
                $player = "";
                $body = "";
                if ($search_title !== false || $search_body !== false || $search_player !== false) {
                    if ($dbmsd->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $idm . "&search=1&\" class='none popupify'>" . $dbmsd->get("title") . "</a>";
                    
                    $body = $dbmsd->get('body');
                    $player = $dbmsd->get('recipient');
                    
                    if ($dbmsd->get("important") == "1")
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $repertory = "";
                    if ($dbmsd->get("name_Repertory") != "")
                        $repertory = $dbmsd->get("name_Repertory");
                    
                    $data[] = array(
                        "title" => $title,
                        "name" => $player,
                        "body" => $body,
                        "imp" => $important,
                        "new" => "Msg Envoyer",
                        "rep" => $repertory,
                        "id" => $idm,
                        "idbody" => $idbody
                    );
                    
                    $nb_result = $p + 1;
                }
                $dbmsd->next();
            }
            
            while (! $dbarch->eof()) // On effectue la recherche sur les archives
{
                $findme = $query;
                
                $titlesh = $dbarch->get("title");
                $bodysh = $dbarch->get("body");
                $playersh = $dbarch->get("recipient");
                
                $search_title = stripos($titlesh, $findme);
                $search_body = stripos($bodysh, $findme);
                $search_player = stripos($playersh, $findme);
                
                $idm = $dbarch->get('id_MailBody');
                $idbody = 0;
                
                $title = "";
                $player = "";
                $body = "";
                if ($search_title !== false || $search_body !== false || $search_player !== false) {
                    if ($dbarch->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $idm . "&search=1&\" class='none popupify'>" . $dbarch->get("title") . "</a>";
                    
                    $body = $dbarch->get('body');
                    $player = $dbarch->get('recipient');
                    
                    if ($dbarch->get("important") == "1")
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $repertory = "";
                    if ($dbarch->get("name_Repertory") != "")
                        $repertory = $dbarch->get("name_Repertory");
                    
                    if ($dbarch->get("new") == "1")
                        $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                    else
                        $img = "";
                    
                    $data[] = array(
                        "title" => $title,
                        "name" => $player,
                        "body" => $body,
                        "imp" => $important,
                        "new" => $img,
                        "rep" => $repertory,
                        "id" => $idm,
                        "idbody" => $idbody
                    );
                    
                    $nb_result = $p + 1;
                }
                $dbarch->next();
            }
            
            if ($nb_result != 0) // si resultat différent de 0 on affiche les messages trouvé
{
                
                $str .= "<tr>";
                $str .= "<td class='mainbglabel'>";
                // $str.="".localize('Nombre de résultat trouvé')." : ".$nb_result."";
                $str .= "</td>";
                $str .= "</tr>";
                $str .= " </table>";
                $str .= "<table class='maintable centerareawidth'>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel' width='30px' align='center'><a href='#' class='all' onclick=\"invertCheckboxes('form'); return false;\"><img src='../pics/misc/all.gif' style='border: 0' /></a></td>\n";
                $str .= "<td class='mainbglabel' width='180px' align='center'>" . localize('Personnage(s)') . "</td>\n";
                $str .= "<td class='mainbglabel' width='60px'>" . localize('Non lu') . "</td>\n";
                $str .= "<td class='mainbglabel' width='80px'  align='center'>" . localize('Important') . "</td>\n";
                $str .= "<td class='mainbglabel' width='100px'  align='center'>" . localize('Archivé') . "</td>\n";
                $str .= "<td class='mainbglabel' width='200px' align='center'>" . localize('Titre du message') . "</td>\n";
                $str .= "<td class='mainbglabel' width='350px' align='center'>" . localize('Contenu du message') . ": </td>\n";
                $str .= "</tr>\n";
                foreach ($data as $arr) // pour afficher la liste des message dans un tableau
{
                    $str .= "<tr>";
                    $str .= "<td class='mainbgtitle' width='30px' align='center'><input name='check[]' type='checkbox' value='" . $arr["id"] . "'/>\n</td>";
                    $str .= "<input name='title" . $arr["id"] . "' type='hidden' value='" . localize("Re: ") . $title . "' />\n";
                    $str .= "<td class='mainbgtitle stylepc' width='180px' align='center'>" . $arr["name"] . "</td>";
                    $str .= "<td class='mainbgtitle' width='60px'  align='center'>" . $arr["new"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='80px'  align='center'>" . $arr["imp"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='100px'  align='center'>" . $arr["rep"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='200px' align='center'>" . $arr["title"] . "</td>";
                    $str .= "<td class='mainbgtitle' width='350px'>" . $arr["body"] . "</td>";
                    $str .= "</tr>";
                }
                $str .= " </table>";
            } else // si non aucun message ne contient la chaîne caractère rechercher
{
                $str .= "<tr>";
                $str .= "<td class='mainbglabel'>";
                $str .= " " . localize('Désoler aucun résultat n\'a été trouver veuillez réessayer avec d\'autres mots') . ".";
                $str .= "</td>";
                $str .= "</tr>";
            }
            
            $str .= "<table>";
            $str .= "<tr>";
            $str .= "<td><input id='Important' type='submit' name='Important' value='" . localize("Marquer comme important") . "' />";
            // $str.="<input id='Reply' type='submit' name='Reply' value='".localize("Répondre")."' />";
            // $str.="<input id='ReplyToAll' type='submit' name='ReplyToAll' value='".localize("Répondre à tous")."' />";
            $str .= "<input id='FlagRead' type='submit' name='FlagRead' value='" . localize("Tout Marquer comme lu") . "' />";
            $str .= "<input id='Del' type='submit' name='Del' value='" . localize("Effacer") . "' /></td>";
            $str .= "<td><input id='Archive' type='submit' name='Archive' value='" . localize("Archiver") . "' /></td>";
            $str .= "</tr>";
            $str .= " </table>";
        } elseif ($arch == 1) // ---------------------------------------------------------------- Traitement de l'archivage ------------------------------------------------
{
            $sans_title2 = "";
            $playerid = $curplayer->get("id");
            
            $dbmisd = new DBCollection(
                "SELECT MailSend.id AS idMailSend,MailSend.important,MailSend.id_MailBody,MailBody.recipient,MailBody.recipientid,MailBody.title,MailBody.date 
		FROM MailSend LEFT JOIN MailBody ON id_MailBody=(MailBody.id) WHERE MailSend.id_Player=" . $playerid . " AND (" . $archivems . ") ", $db);
            
            $dbmitd = new DBCollection(
                "SELECT MailTrade.id AS idMailSend,MailTrade.id_MailBody,MailTrade.title,MailTrade.new,MailTrade.id_Player\$receiver,
		MailTrade.id_Player\$sender,MailTrade.date,Player.name,Player.racename, Player.id AS idplayer FROM MailTrade LEFT JOIN Player ON id_Player\$sender=Player.id 
		WHERE MailTrade.id_Player\$receiver=" . $playerid . " AND (" . $archivemtd . ") ", $db);
            
            $dbm = new DBCollection(
                "SELECT Mail.id AS idMailSend,Mail.id_MailBody,Mail.title,Mail.new,Mail.id_Player\$receiver,Mail.date,Player.name, 
		Player.racename, Player.id AS idplayer FROM Mail LEFT JOIN Player ON id_Player\$sender=Player.id 
		WHERE Mail.id_Player\$receiver=" . $playerid . " AND (" . $archivem . ") ", $db);
            
            $addr = localize("Expéditeur |  Destinataire");
            
            $str .= "<table class='maintable'>\n";
            $str .= "<tr>\n";
            $str .= "<tr><td class='mainbgtitle'>Sélection : </td><td class='mainbgtitle'>
		<table><tr><td class='mainbgtitle' width='270px'>Titre</td><td class='mainbgtitle' width='170px'>" . $addr . "</td><td class='mainbgtitle'>Date</td></table>
		</td></tr>\n";
            $str .= "<tr><td class='mainbgtitle' align='center'><select class='selector cqattackselectorsize' name='CHOOSE_REPERTORY_ID'>";
            $item = array();
            $dbrep = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $playerid, $db);
            // On affiche dans la liste déroulante les répertoires appartenant au joueur.
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un répertoire --") . "</option>";
            while (! $dbrep->eof()) {
                $item[] = array(
                    $dbrep->get("id") => localize($dbrep->get("name_Repertory"))
                );
                $dbrep->next();
            }
            foreach ($item as $arr) {
                foreach ($arr as $key => $value)
                    $str .= "<option value='" . $key . "'>" . $value . "</option>";
            }
            $str .= "</select></td><td>";
            $str .= "<table>"; // On affiche dans le deuxième tableau, les messages que l'on a sélectionner pour être archivé
            while (! $dbmisd->eof()) // Boucle pour les messages de type envoyer sélectionnés
{
                $idarray = explode(",", $dbmisd->get("recipientid"));
                $namearray = explode(",", $dbmisd->get("recipient"));
                
                $sans_title2 = 1;
                $player = "";
                $first = 1;
                foreach ($idarray as $id) {
                    list ($key, $name) = each($namearray);
                    if (! $first) {
                        $player .= ", ";
                    }
                    $first = 0;
                    $player .= "<a href=\"../conquest/profile.php?id=" . $id . "\" class='stylepc popupify'>" . $name . "</a>";
                }
                $id = $dbmisd->get("idMailSend");
                
                $title = "<a href=\"../conquest/mailbody.php?id=" . $dbmisd->get("idMailSend") . "&mail=2\" class='none popupify'>" .
                    $dbmisd->get("title") . "</a>";
                
                $time = gmstrtotime($dbmisd->get("date"));
                $date = date("Y-m-d H:i:s", $time);
                
                // Construction du tableau
                $str .= "<tr><td>";
                if ($sans_title2 == 0)
                    $str .= "<input name='title" . $id . "' type='hidden' value='" . localize("Re: ") . $title . "' />\n";
                $str .= "</td>";
                $str .= "<td class='mainbgtitle' width='270px'>" . $title . "</td>\n";
                $str .= "<td class='mainbgtitle' width='125px' align='center'>" . $player . "</td>\n";
                $str .= "<td class='mainbgtitle' width='140px' align='center'>" . $date . "</td></tr>\n";
                
                $dbmisd->next();
            }
            while (! $dbmitd->eof()) // Boucle pour les messages de type commercial sélectionnés
{
                $sans_title2 = 0;
                $id = $dbmitd->get("idMailSend");
                $player = "";
                if ($dbmitd->get("name") != "")
                    $player = "<a href=\"../conquest/profile.php?id=" . $dbmitd->get("idplayer") . "\" class='stylepc popupify'>" . $dbmitd->get("name") . "</a>";
                else
                    $player = "<a href=\"../conquest/profile.php?id=" . $dbmitd->get("idplayer") . "\" class='stylepc popupify'>" . $dbmitd->get("racename") . "</a>";
                $title = "";
                if ($dbmitd->get("title") != "")
                    $title = "<a href=\"../conquest/mailbody.php?id=" . $dbmitd->get("id_MailBody") . "&mail=3\" class='none popupify'>" .
                        $dbmitd->get("title") . "</a>";
                
                $time = gmstrtotime($dbmitd->get("date"));
                $date = date("Y-m-d H:i:s", $time);
                
                $str .= "<tr><td>";
                if ($sans_title2 == 0)
                    $str .= "<input name='title" . $id . "' type='hidden' value='" . localize("Re: ") . $title . "' />\n";
                $str .= "</td>";
                $str .= "<td class='mainbgtitle' width='270px'>" . $title . "</td>\n";
                $str .= "<td class='mainbgtitle' width='125px' align='center'>" . $player . "</td>\n";
                $str .= "<td class='mainbgtitle' width='140px' align='center'>" . $date . "</td></tr>\n";
                $dbmitd->next();
            }
            
            while (! $dbm->eof()) // Boucle pour les messages sélectionnés
{
                $sans_title2 = 0;
                $id = $dbm->get("idMailSend");
                $player = "";
                if ($dbm->get("name") != "")
                    $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("name") . "</a>";
                else
                    $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("racename") . "</a>";
                $title = "";
                if ($dbm->get("title") != "")
                    $title = "<a href=\"../conquest/mailbody.php?id=" . $dbm->get("idMailSend") . "&mail=3\" class='none popupify'>" .
                        $dbm->get("title") . "</a>";
                
                $time = gmstrtotime($dbm->get("date"));
                $date = date("Y-m-d H:i:s", $time);
                
                $str .= "<tr><td>";
                if ($sans_title2 == 0)
                    $str .= "<input name='title" . $id . "' type='hidden' value='" . localize("Re: ") . $title . "' />\n";
                $str .= "</td>";
                $str .= "<td class='mainbgtitle' width='270px'>" . $title . "</td>\n";
                $str .= "<td class='mainbgtitle' width='125px' align='center'>" . $player . "</td>\n";
                $str .= "<td class='mainbgtitle' width='140px' align='center'>" . $date . "</td></tr>\n";
                $dbm->next();
            }
            $str .= "</table>";
            $str .= "</td></tr>";
            $str .= "<tr><td><input id='Place' type='submit' name='Place' value='" . localize("Archiver dans le répertoire") . "' /></td></tr>";
            $str .= "<td><input name='select' type='hidden' value='" . $archive . "' /></td>";
            $str .= "<td><input name='send' type='hidden' value='" . $archivemsModulo . "' /></td>";
            $str .= "<tr><td class='mainbglabel'> Listes des id des message(s): " . $archive . "</td></tr>";
            $str .= "</table>";
        }  // --------------------------------------------------------- Fin traitement archivage -----------------------------------------------------------------
else {
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr>";
            $str .= "<td class='mainbglabel'>";
            $str .= "" . localize('Aucune recherche n\'a été effectué') . ".";
            $str .= "</td>";
            $str .= "</tr>";
            $str .= " </table>";
        }
        $str .= "</form>\n";
        
        return $str;
    }
    
    // Fonction pour archiver les messages ------------------------ Archive ---------------------------------------
    public function archiveMail($archive, $idbody, $playerid, $db)
    {
        if (isset($_POST["CHOOSE_REPERTORY_ID"])) {
            
            $repertoryId = $_POST["CHOOSE_REPERTORY_ID"];
            
            $msgToArchive = $archive;
            $msgToArchiveS = $idbody; // 1. on incère le nom du répertoire dans le messages sélectionner
            $dbmsd = new DBCollection("UPDATE MailSend SET id_repertory='$repertoryId' WHERE id_Player=" . $playerid . " AND (" . $msgToArchive . ") ", $db, 0, 0, false);
            // 2. on duplique le ou les message sélectionner dans la table MailArchive
            $dbmasd = new DBCollection(
                "INSERT INTO MailArchive (id_MailBody,title,important,id_repertory,date,id_Player) 
			SELECT id_MailBody,MailBody.title,important,id_repertory,MailBody.date," . $playerid . "
			FROM MailSend LEFT JOIN MailBody ON id_MailBody=(MailBody.id) WHERE " . $msgToArchive, $db, 0, 0, false);
            $dbmdsd = new DBCollection("DELETE FROM MailSend WHERE " . $msgToArchive, $db, 0, 0, false); // On évite la duplication en supprimant le message de sa table d'origine
            
            $dbmtd = new DBCollection("UPDATE MailTrade SET id_repertory='$repertoryId' WHERE id_Player\$receiver=" . $playerid . " AND " . $msgToArchive, $db, 0, 0, false);
            $dbmatd = new DBCollection(
                "INSERT INTO MailArchive (id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date,id_Player) 
			SELECT id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date," . $playerid . " FROM MailTrade 
			WHERE id_Player\$receiver=" . $playerid . " AND (" . $msgToArchive . ") ", $db, 0, 0, false);
            $dbmdtd = new DBCollection("DELETE FROM MailTrade WHERE id_Player\$receiver=" . $playerid . " AND (" . $msgToArchive . ") ", $db, 0, 0, false);
            
            $dbm = new DBCollection("UPDATE Mail SET id_Repertory='$repertoryId' WHERE " . $msgToArchive, $db, 0, 0, false);
            $dbma = new DBCollection(
                "INSERT INTO MailArchive (id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date,id_Player) 
			SELECT id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date," . $playerid . "
			FROM Mail WHERE id_Player\$receiver=" . $playerid . " AND (" . $msgToArchive . ") ", $db, 0, 0, false);
            $dbmd = new DBCollection("DELETE FROM Mail WHERE id_Player\$receiver=" . $playerid . " AND (" . $msgToArchive . ") ", $db, 0, 0, false);
            
            return localize("Message(s) archivé(s)");
        }
    }
    
    // Fonction pour déclarer un message comme important ---------------------------- Important ---------------------------------
    public function importantMail($search, $db)
    {
        $curplayer = $this->curplayer;
        $id = $curplayer->get("id");
        
        if (isset($_GET["priority"]))
            $msgToImportant = "id_MailBody=" . quote_smart($_GET["priority"]);
        else
            $msgToImportant = $this->getCheckedDelMailCond("id_MailBody");
        
        if ($msgToImportant != "") {
            
            $dbmTrd = new DBCollection("UPDATE MailTrade SET important=1  WHERE id_Player\$receiver=$id AND ($msgToImportant) ", $db, 0, 0, false);
            
            $dbmArch = new DBCollection("UPDATE MailArchive SET important=1  WHERE id_Player=$id AND ($msgToImportant) ", $db, 0, 0, false);
            
            $dbmSd = new DBCollection("UPDATE MailSend SET important=1 WHERE id_Player=$id AND ($msgToImportant) ", $db, 0, 0, false);
            
            $dbm = new DBCollection("UPDATE Mail SET important=1 WHERE id_Player\$receiver=$id AND ($msgToImportant) ", $db, 0, 0, false);
            
            return localize("Message(s) marqué(s) comme important"); // $msgToImportant
        } else {
            return localize("Aucun message marquer comme important");
        }
        $str .= "<div class='profile'>\n";
    }
    
    // Fonction pour déclarer un message comme lu ---------------------------- Marquer un ou des meesages comme lu ------------------------------------------
    public function FlagReadMail($db)
    {
        $curplayer = $this->curplayer;
        $id = $curplayer->get("id");
        $msgToRead = $this->getCheckedDelMailCond("id");
        
        $dbmTrd = new DBCollection("UPDATE MailTrade SET new=0  WHERE id_Player\$receiver=$id", $db, 0, 0, false);
        $dbmarch = new DBCollection("UPDATE MailArchive SET new=0 WHERE  id_Player\$receiver=$id", $db, 0, 0, false);
        $dbm = new DBCollection("UPDATE Mail SET new=0 WHERE id_Player\$receiver=$id", $db, 0, 0, false);
        return localize("Message(s) marquer comme lu");
        $str .= "<div class='profile'>\n";
    }
    
    // fonction pour supprimer les messages ----------------------------------- Delete ------------------------------------------
    public function deleteMail($search, $db)
    {
        if (isset($_GET["priority"])) {
            $msgToDelete = "id_MailBody=" . quote_smart($_GET["priority"]);
            $deleteMailBody = "id=" . quote_smart($_GET["priority"]);
        } else {
            $msgToDelete = $this->getCheckedDelMailCond("id_MailBody");
            $msgToDeleteSend = $this->getCheckedDelMailCond2("id");
            $deleteMailBody = $this->getCheckedDelMailCond("id");
        }
        
        if ($msgToDelete != "") {
            $curplayer = $this->curplayer;
            $db = $this->db;
            $id = $curplayer->get("id");
            $dbmb = new DBCollection("UPDATE MailBody SET cpt=cpt-1 WHERE " . $deleteMailBody . " OR " . $msgToDeleteSend . " ", $db, 0, 0, false);
            
            $dbm = new DBCollection("DELETE FROM Mail WHERE id_Player\$receiver=" . $id . " AND (" . $msgToDelete . ") ", $db, 0, 0, false);
            $dbmitd = new DBCollection("DELETE FROM MailTrade WHERE id_Player\$receiver=" . $id . " AND (" . $msgToDelete . ") ", $db, 0, 0, false);
            $dbmisd = new DBCollection("DELETE FROM MailSend WHERE id_Player=" . $id . " AND (" . $msgToDelete . ") ", $db, 0, 0, false);
            $dbmisarch = new DBCollection("DELETE FROM MailArchive WHERE id_Player=" . $id . " AND (" . $msgToDelete . ") ", $db, 0, 0, false); // AND id_Player\$receiver=".$id." AND (".$msgToDelete.")
            $dbmbd = new DBCollection("DELETE FROM MailBody WHERE cpt=0", $db, 0, 0, false);
            
            return localize("Message(s) effacé(s)");
        } else {
            return localize("Aucun message effacé");
        }
        $str .= "<div class='profile'>\n";
    }
    
    // fonction qui surveille les sélections ---------------------------------------- Checked Del Mail Cond ------------------------------------------------
    public function getCheckedDelMailCond($condName)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . $val;
                else
                    $msg .= " OR " . $condName . "=" . $val;
                $first = 0;
            }
        }
        return $msg;
    }

    public function getCheckedDelMailCond2($condName)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . ($val - 10);
                else
                    $msg .= " OR " . $condName . "=" . ($val - 10);
                $first = 0;
            }
        }
        return $msg;
    }
    
    // fonction qui surveille les sélections avec en paramètre la variable $title ------------------------------ Checked Mail Cond --------------------
    public function getCheckedMailCond($condName, &$title)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . quote_smart($val);
                else
                    $msg .= " OR " . $condName . "=" . quote_smart($val);
                $first = 0;
                $title = quote_smart($_POST["title"] . $val);
            }
        }
        return $msg;
    }
    
    // ----------------------------------------------------------- Reply -------------------------------------------------------
    public function reply($db)
    {
        $condToReply = $this->getCheckedMailCond("MailBody.id", $title);
        if ($condToReply != "") {
            $dbm = new DBCollection("SELECT Player.name,MailBody.id FROM MailBody LEFT JOIN Player ON MailBody.id_Player=Player.id WHERE " . $condToReply . " GROUP BY Player.name", 
                $db);
            $first = 1;
            $names = "";
            while (! $dbm->eof()) {
                if ($first)
                    $names = $dbm->get("name");
                else
                    $names .= "," . $dbm->get("name");
                
                $first = 0;
                $dbm->next();
            }
            
            redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
            return "";
        }
        return localize("Vous devez d'abord choisir un message");
    }
    // ------------------------------------------------------- Reply To All -------------------------------------------
    public function replyToAll($db)
    {
        global $Conquest;
        
        $condToReply = $this->getCheckedMailCond("MailBody.id", $title);
        if ($condToReply != "") {
            $dbm = new DBCollection("SELECT recipient,name FROM MailBody LEFT JOIN Player ON Player.id=MailBody.id_Player WHERE " . $condToReply, $db);
            $first = 1;
            $arr = array();
            while (! $dbm->eof()) {
                $splited = explode(",", $dbm->get("recipient"));
                foreach ($splited as $name)
                    $arr[$name] = 1;
                $arr[$dbm->get("name")] = 1;
                $dbm->next();
            }
            
            $player = $this->curplayer;
            
            unset($arr[$player->get("name")]);
            $names = "";
            foreach ($arr as $name => $val) {
                if ($first)
                    $names = $name;
                else
                    $names .= "," . $name;
                
                $first = 0;
            }
            redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
            return "";
        }
        return localize("Vous devez d'abord choisir un message");
    }
}

?>
