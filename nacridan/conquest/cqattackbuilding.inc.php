<?php
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");
require_once (HOMEPATH . "/class/DBStaticModifier.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");

class CQAttackbuilding extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQAttackbuilding($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        $this->err = "";
    }

    public function toString()
    {
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $xp = $curplayer->get("x");
        $yp = $curplayer->get("y");
        $map = $curplayer->get("map");
        $id = $curplayer->get("id");
        
        if ($curplayer->get("ap") < 7) {
            $str = "<table class='maintable bottomareawidth'><tr><td class='mainbgtitle'>";
            $str .= localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser une attaque sur un bâtiment.");
            $str .= "</td></tr></table>";
        } else {
            $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
            $str .= "<table class='maintable bottomareawidth'><tr class='mainbgtitle'><td  width='80px'>";
            $str .= localize("Attaquer") . " </td><td> <select id='selector_id' class='selector cqattackselectorsize' name='TARGET_ID'>";
            
            $gap = 1;
            $item = array();
            $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un batiment --") . "</option>";
            if ($curplayer->get("inbuilding"))
                $dbnpc = new DBCollection("SELECT * FROM Building WHERE id=" . $curplayer->get("inbuilding"), $db, 0, 0);
            else
                $dbnpc = new DBCollection(
                    "SELECT * FROM Building WHERE id_BasicBuilding!=14 AND currsp!= 0 AND progress!=0 AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" .
                         $xp . "-" . $yp . "))/2 =" . $gap, $db, 0, 0);
            
            while (! $dbnpc->eof()) {
                $item[] = array(
                    localize($dbnpc->get("name")) . " (" . $dbnpc->get("id") . ")" => $dbnpc->get("id")
                );
                $dbnpc->next();
            }
            
            foreach ($item as $arr) {
                foreach ($arr as $key => $value) {
                    if ($value == - 1)
                        $str .= "<optgroup class='group' label='" . $key . "' />";
                    else
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
            }
            
            $str .= "</select></td><td class='mainbgtitle' rowspan=2; align='center';>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' />";
            $str .= "<input name='action' type='hidden' value=" . ATTACK_BUILDING . " />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</tr>";
            $str .= "<tr class='mainbgtitle'>";
            
            $str .= "<td  >";
            $str .= "Avec : ";
            $str .= "</td ><td  >";
            
            $str .= "<select id='selector_id' class='selector' name='ATTACK_TYPE'>";
            $str .= "<option value='0' selected='selected'>" . localize("-- Type d'attaque --") . "</option>";
            $str .= "<option value='1'>Attaque normale</option>";
            $str .= "<option value='2'>Attaque magique</option>";
            $str .= "</select></td>";
            $str .= "</tr></table>";
            $str .= "</form>";
        }
        return $str;
    }
}
?>
>
