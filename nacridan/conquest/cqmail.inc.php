<?php
DEFINE("STEP", 8);

class CQMail extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function CQMail($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $id = $curplayer->get("id");
        $landmark = "";
        $typemsg = "";
        $err = "";
        
        if (isset($_GET["mail"])) {
            $mail = quote_smart($_GET["mail"]);
        } else {
            $mail = 1;
        }
        
        if (isset($_POST["Reply"])) {
            $err = $this->reply($mail, $db);
        }
        
        if (isset($_POST["ReplyToAll"])) {
            $err = $this->replyToAll($mail, $db);
        }
        
        if (isset($_POST["Important"])) {
            $err = $this->importantMail($mail, $db);
        }
        
        if (isset($_GET["priority"])) {
            $err = $this->importantMail($mail, $db);
        }
        
        if (isset($_GET["delete"])) {
            $err = $this->deleteMail($mail, $db);
        }
        
        if (isset($_POST["FlagRead"])) {
            $err = $this->FlagReadMail($mail, $db);
        }
        
        if (isset($_POST["Del"])) {
            $err = $this->deleteMail($mail, $db);
        }
        // on test le déplacemnet des messages
        if (isset($_POST["Place"])) {
            
            if ($_POST["CHOOSE_REPERTORY_ID"] == 0) // on vérifie qu'un répertoire à bien été sélectionner
                $err = localize("Aucun répertoire sélectionner. Veuillez en choisir un dans la liste.");
            else {
                $playerid = $id;
                $err = $this->archiveMail($mail, $_POST["select"], $playerid, $db);
            }
        }
        
        if (isset($_POST["Archive"])) {
            $dbr = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $id, $db);
            $norep = $dbr->count();
            $playerid = "";
            if ($norep == 0) { // on vérifie qu'un répertoire à bien été créer
                
                $err = localize("Aucun répertoire existant. Veuillez en créer un");
                $arch = 0;
            } else {
                $arch = 1;
                if ($mail == 2)
                    $archive = $this->getCheckedDelMailCond("MailSend.id");
                elseif ($mail == 3)
                    $archive = $this->getCheckedDelMailCond("MailTrade.id");
                else
                    $archive = $this->getCheckedDelMailCond("Mail.id");
            }
        } else {
            $arch = 0;
            $archive = "";
        }
        
        $target = "/conquest/conquest.php?center=mail&mail=" . $mail;
        
        if (isset($_POST["__stepEvt"])) {
            // print_r($_POST);
            $step = quote_smart($_POST["__stepEvt"]);
            if (isset($_POST["NextEvt"])) {
                $step += 1;
            }
            if (isset($_POST["PrevEvt"])) {
                $step -= 1;
            }
        }
        
        if (! isset($step) || $step < 0) {
            $step = 0;
        }
        
        if ($mail == 2) {
            $dbm = new DBCollection(
                "SELECT MailSend.id AS idMailSend,MailSend.important,MailSend.id_MailBody,MailBody.recipient,MailBody.recipientid,MailBody.type,MailBody.title,MailBody.date FROM MailSend LEFT JOIN MailBody ON MailSend.id_MailBody=(MailBody.id) WHERE MailSend.id_Player=" .
                     $id . " ORDER BY date DESC", $db, $step * STEP, STEP);
            $addr = localize("Destinataire");
        } elseif ($mail == 3) {
            $dbm = new DBCollection(
                "SELECT MailTrade.id AS idMailSend,MailTrade.id_MailBody,MailBody.title,MailTrade.new,MailTrade.important,MailTrade.id_Player\$receiver,MailTrade.date,Player.name, Player.racename, Player.id AS idplayer FROM MailTrade LEFT JOIN Player ON id_Player\$sender=Player.id LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE type='1' AND id_Player\$receiver =" .
                     $id . " ORDER BY date DESC", $db, $step * STEP, STEP);
            $landmark = "COMMERCIAUX";
            $addr = localize("Expéditeur");
        } /*
           * elseif($mail==4)
           * {
           * $dbm=new DBCollection("SELECT Mail.id AS idMailSend,Mail.id_MailBody,MailBody.title,Mail.new,Mail.id_Player\$receiver,Mail.date,Player.name, Player.racename, Player.id AS idplayer FROM Mail LEFT JOIN Player ON id_Player\$sender=Player.id LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE type='0' AND important='1' AND id_Player\$receiver =".$id." ORDER BY date DESC",$db);
           * $dbmitd=new DBCollection("SELECT MailTrade.id AS idMailSend,MailTrade.id_MailBody,MailBody.title,MailTrade.new,MailTrade.id_Player\$receiver,MailTrade.date,Player.name, Player.racename, Player.id AS idplayer FROM MailTrade LEFT JOIN Player ON id_Player\$sender=Player.id LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE type='1' AND important='1' AND id_Player\$receiver =".$id." ORDER BY date DESC",$db);
           * $dbmisd=new DBCollection("SELECT MailSend.id AS idMailSend,MailBody.* FROM MailSend LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE important=1 AND MailSend.id_Player=".$id." ORDER BY date DESC",$db);
           * $addr=localize("Expéditeur | Destinataire");
           * }
           */
elseif ($mail == 5) {
            $dbm = new DBCollection(
                "SELECT MailAlert.id AS idMailAlert,MailAlert.id_MailBody,MailBody.title,MailAlert.new,MailAlert.important,MailAlert.id_Player\$receiver,MailAlert.date,Player.name, Player.racename, Player.id AS idplayer FROM MailAlert LEFT JOIN Player ON id_Player\$sender=Player.id LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE type='2' AND id_Player\$receiver =" .
                     $id . " ORDER BY date DESC", $db, $step * STEP, STEP);
            $landmark = "ALERTES";
            $addr = localize("Expéditeur");
        } else {
            $dbm = new DBCollection(
                "SELECT Mail.id AS idMailSend,Mail.id_MailBody,MailBody.title,Mail.new,Mail.important,Mail.id_Player\$receiver,Mail.date,Player.name, Player.racename, Player.id AS idplayer FROM Mail LEFT JOIN Player ON id_Player\$sender=Player.id LEFT JOIN MailBody ON id_MailBody=MailBody.id WHERE type='0' AND id_Player\$receiver =" .
                     $id . " ORDER BY date DESC", $db, $step * STEP, STEP);
            $landmark = "STANDARDS";
            $addr = localize("Expéditeur");
        }
        
        $str = "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?center=search' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "<tr><td class='mainbgtitle tabmenu'>" . "<label>Rechercher :  <input type='text' name='query' size='20px'/> " . "</label>
	<input id='Search' type='submit' name='Search' value='" . localize("Recherche") . "' />
	<span class='mainbgtitle' style='font-size: 10px;'>&nbsp;&nbsp;&nbsp;&nbsp; Attention la recherche ne s'effectue pas dans les alertes.</span></td></tr>";
        $str .= "</table>";
        $str .= "</form>\n";
        
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . $target . "' target='_self'>\n";
        $str .= "<table class='maintable centerareawidth'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('M E S S A G E R I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span> \n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        $str .= "<tr>\n";
        
        $dbp = new DBCollection("SELECT authlevel FROM Player WHERE id_Member=" . $curplayer->get("id_Member"), $db);
        $anim = 0;
        while (! $dbp->eof()) {
            if ($dbp->get("authlevel") > 2)
                $anim = 1;
            $dbp->next();
        }
        
        if ($curplayer->get('status') == 'PC' || $anim) {
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'>\n";
            $str .= "<a href='../conquest/conquest.php?center=compose' class='tabmenu'>" . localize('Composer un Message') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Message(s) Reçu(s)') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=mail&mail=2' class='tabmenu'>" . localize('Message(s) Envoyé(s)') . "</a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=contact' class='tabmenu'>" . localize('Contacts') . " </a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=alias' class='tabmenu'>" . localize('Alias') . " </a> |\n";
            $str .= "<a href='../conquest/conquest.php?center=archive' class='tabmenu'>" . localize('Archive(s)') . " </a>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "<tr><td class='mainbgtitle'>";
            if ($mail != 2) {
                $typemsg = "MESSAGE(S) RECU(S)";
                $str .= "<b><h5>" . localize($typemsg) . "</h5></b></td>";
            } else {
                $typemsg = "MESSAGE(S) ENVOYE";
                $str .= "<b><h5>" . localize($typemsg) . "</h5></b></td>";
            }
            $str .= "</tr>";
            if ($mail == 1 || $mail == 3 || $mail == 5) {
                $dbmnew = new DBCollection("SELECT new FROM Mail WHERE id_Player\$receiver=" . $id . " AND new=1 ", $db);
                $dbmtd = new DBCollection("SELECT new FROM MailTrade WHERE id_Player\$receiver=" . $id . " AND new=1 ", $db);
                $dbmalrt = new DBCollection("SELECT new FROM MailAlert WHERE id_Player\$receiver=" . $id . " AND new=1 ", $db);
                
                $str .= "<tr>\n";
                $str .= "<td colspan='6' class='mainbgtitle'>\n";
                $str .= "<a href='../conquest/conquest.php?center=mail&mail=1' class='tabmenu'>" . localize('Standard') . "</a>&nbsp;(" . $dbmnew->count() . ") |\n";
                $str .= "<a href='../conquest/conquest.php?center=mail&mail=3' class='tabmenu'>" . localize('Commerce') . "</a>&nbsp;(" . $dbmtd->count() . ") |\n";
                $str .= "<a href='../conquest/conquest.php?center=mail&mail=5' class='tabmenu'>" . localize('Alerte') . "</a>&nbsp;(" . $dbmalrt->count() . ")\n";
                $str .= "</td>\n";
                $str .= "</tr>\n";
                $str .= "<tr><td class='mainbgtitle'>" . $landmark . "</td></tr>\n";
            }
            $str .= "</table>\n";
            $data = array();
            $cptMsg = $dbm->count();
            $rep = " ";
            if ($mail == 2) {
                while (! $dbm->eof()) {
                    $idarray = explode(",", $dbm->get("recipientid"));
                    $namearray = explode(",", $dbm->get("recipient"));
                    
                    $player = "";
                    $first = 1;
                    foreach ($idarray as $id) {
                        list ($key, $name) = each($namearray);
                        if (! $first) {
                            $player .= ", ";
                        }
                        $first = 0;
                        $player .= "<a href=\"../conquest/profile.php?id=" . $id . "\" class='stylepc popupify'>" . $name . "</a>";
                    }
                    $id = $dbm->get("idMailSend");
                    $idbody = ($dbm->get("id_MailBody"));
                    $title = "<a href=\"../conquest/mailbody.php?id=" . $idbody . "&mail=2\" class='none popupify'>" .
                         $dbm->get("title") . "</a>";
                    
                    if ($dbm->get("important") == 1)
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $img = "";
                    
                    $time = gmstrtotime($dbm->get("date"));
                    $date = date("Y-m-d H:i:s", $time);
                    
                    $data[] = array(
                        "date" => $date,
                        "title" => $title,
                        "name" => $player . $img,
                        "img" => "",
                        "imp" => $important,
                        "id" => $id
                    );
                    $dbm->next();
                }
            } elseif ($mail == 3) {
                while (! $dbm->eof()) {
                    $id = $dbm->get("idMailSend");
                    $player = "";
                    if ($dbm->get("name") != "")
                        $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("name") . "</a>";
                    else
                        $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("racename") . "</a>";
                    $title = "";
                    if ($dbm->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $dbm->get("id_MailBody") . "&mail=3\" class='none popupify'>" . $dbm->get("title") . "</a>";
                    
                    if ($dbm->get("new") == "1")
                        $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                    else
                        $img = "";
                    
                    if ($dbm->get("important") == 1)
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $time = gmstrtotime($dbm->get("date"));
                    $date = date("Y-m-d H:i:s", $time);
                    
                    $data[] = array(
                        "date" => $date,
                        "title" => $title,
                        "title2" => $dbm->get("title"),
                        "name" => $player,
                        "img" => $img,
                        "imp" => $important,
                        "id" => $id
                    );
                    $dbm->next();
                }
            } elseif ($mail == 4) {
                /*
                 * while(!$dbm->eof())
                 * {
                 * $sans_title2=0;
                 * $id=$dbm->get("idMailSend");
                 * $player="";
                 * if($dbm->get("name")!="")
                 * $player="<a href=javascript:profile(\"../conquest/profile.php?id=".$dbm->get("idplayer")."\") class='stylepc'>".$dbm->get("name")."</a>";
                 * else
                 * $player="<a href=javascript:profile(\"../conquest/profile.php?id=".$dbm->get("idplayer")."\") class='stylepc'>".$dbm->get("racename")."</a>";
                 * $title="";
                 * if($dbm->get("title")!="")
                 * $title="<a href=javascript:profile(\"../conquest/mailbody.php?id=".$dbm->get("id_MailBody")."&mail=3&".getGoogleKeywords()."=".$id."\") class='none'>".$dbm->get("title")."</a>";
                 *
                 * if($dbm->get("new")=="1")
                 * $img="<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                 * else
                 * $img="";
                 *
                 * $time=gmstrtotime ($dbm->get("date"));
                 * $date=date("Y-m-d H:i:s",$time);
                 *
                 * $data[]=array("date" => $date,"title" => $title,"title2" => $dbm->get("title"), "name" => $player, "img" => $img, "id" => $id);
                 * $dbm->next();
                 * }
                 * while(!$dbmitd->eof())
                 * {
                 * $sans_title2=0;
                 * $id=$dbmitd->get("idMailSend");
                 * $player="";
                 * if($dbmitd->get("name")!="")
                 * $player="<a href=javascript:profile(\"../conquest/profile.php?id=".$dbmitd->get("idplayer")."\") class='stylepc'>".$dbmitd->get("name")."</a>";
                 * else
                 * $player="<a href=javascript:profile(\"../conquest/profile.php?id=".$dbmitd->get("idplayer")."\") class='stylepc'>".$dbmitd->get("racename")."</a>";
                 * $title="";
                 * if($dbmitd->get("title")!="")
                 * $title="<a href=javascript:profile(\"../conquest/mailbody.php?id=".$dbmitd->get("id_MailBody")."&mail=3&".getGoogleKeywords()."=".$id."\") class='none'>".$dbmitd->get("title")."</a>";
                 *
                 * if($dbmitd->get("new")=="1")
                 * $img="<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                 * else
                 * $img="";
                 *
                 * $time=gmstrtotime ($dbmitd->get("date"));
                 * $date=date("Y-m-d H:i:s",$time);
                 *
                 * $data[]=array("date" => $date,"title" => $title,"title2" => $dbmitd->get("title"), "name" => $player, "img" => $img, "id" => $id);
                 * $dbmitd->next();
                 * }
                 *
                 * while(!$dbmisd->eof())
                 * {
                 * $idarray=explode(",",$dbmisd->get("recipientid"));
                 * $namearray=explode(",",$dbmisd->get("recipient"));
                 *
                 * $sans_title2=1;
                 * $player="";
                 * $first=1;
                 * foreach($idarray as $id)
                 * {
                 * list($key,$name)=each($namearray);
                 * if(!$first)
                 * {
                 * $player.=", ";
                 * }
                 * $first=0;
                 * $player.="<a href=javascript:profile(\"../conquest/profile.php?id=".$id."\") class='stylepc'>".$name."</a>";
                 * }
                 * $id=$dbmisd->get("idMailSend");
                 *
                 * $title="<a href=javascript:profile(\"../conquest/mailbody.php?id=".$dbmisd->get("id")."&mail=2&".getGoogleKeywords()."=".$id."\") class='none'>".$dbmisd->get("title")."</a>";
                 *
                 * $img="";
                 *
                 * $time=gmstrtotime ($dbmisd->get("date"));
                 * $date=date("Y-m-d H:i:s",$time);
                 *
                 * $data[]=array("date" => $date,"title" => $title, "name" => $player.$img, "img" => "", "id" => $id);
                 * $dbmisd->next();
                 * }
                 */
            } elseif ($mail == 5) {
                while (! $dbm->eof()) {
                    $id = $dbm->get("idMailAlert");
                    $player = "";
                    if ($dbm->get("name") != "")
                        $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("name") . "</a>";
                    else
                        $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("racename") . "</a>";
                    $title = "";
                    if ($dbm->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $dbm->get("id_MailBody") . "&mail=1\" class='none popupify'>" .
                            $dbm->get("title") . "</a>";
                    
                    if ($dbm->get("new") == "1")
                        $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                    else
                        $img = "";
                    
                    if ($dbm->get("important") == 1)
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $time = gmstrtotime($dbm->get("date"));
                    $date = date("Y-m-d H:i:s", $time);
                    
                    $data[] = array(
                        "date" => $date,
                        "title" => $title,
                        "title2" => $dbm->get("title"),
                        "name" => $player,
                        "img" => $img,
                        "imp" => $important,
                        "id" => $id
                    );
                    $dbm->next();
                }
            } else {
                while (! $dbm->eof()) {
                    $id = $dbm->get("idMailSend");
                    $player = "";
                    if ($dbm->get("name") != "")
                        $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("name") . "</a>";
                    else
                        $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("racename") . "</a>";
                    $title = "";
                    if ($dbm->get("title") != "")
                        $title = "<a href=\"../conquest/mailbody.php?id=" . $dbm->get("id_MailBody") . "&mail=1\" class='none popupify'>" .
                            $dbm->get("title") . "</a>";
                    
                    if ($dbm->get("new") == "1")
                        $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                    else
                        $img = "";
                    
                    if ($dbm->get("important") == 1)
                        $important = "<img src=\"../pics/misc/etoile_import.gif\" width=\"20px\">";
                    else
                        $important = "";
                    
                    $time = gmstrtotime($dbm->get("date"));
                    $date = date("Y-m-d H:i:s", $time);
                    
                    $data[] = array(
                        "date" => $date,
                        "title" => $title,
                        "title2" => $dbm->get("title"),
                        "name" => $player,
                        "img" => $img,
                        "imp" => $important,
                        "id" => $id
                    );
                    $dbm->next();
                }
            }
            if ($arch == 0) {
                $cpt = 0;
                // Gestion des boutons
                $str .= "<table><tr>";
                if ($mail != 5) {
                    $str .= "<td><input id='Important' type='submit' name='Important' value='" . localize("Important") . "' /></td>";
                }
                
                if ($mail != 2 && $mail != 5) {
                    $str .= "<td><input id='Reply' type='submit' name='Reply' value='" . localize("Répondre") . "' /></td>";
                    $str .= "<td><input id='ReplyToAll' type='submit' name='ReplyToAll' value='" . localize("Répondre à tous") . "' /></td>";
                }
                
                if ($step > 0) {
                    $str .= "<td width='80px'><input id='Previous' class='eventbtprevious' type='submit' name='PrevEvt' value='" . localize("Précédent") . "' /></td>";
                }
                
                if ($cptMsg == STEP) {
                    $str .= "<td width='80px'><input id='Previous' class='eventbtnext' type='submit' name='NextEvt' value='" . localize("Suivant") . "' /></td>";
                }
                if ($mail != 2)
                    $str .= "<td><input id='FlagRead' type='submit' name='FlagRead' value='" . localize("Marquer comme lu/non-lu") . "' /></td>";
                $str .= "<td><input id='Del' type='submit' name='Del' value='" . localize("Effacer") . "' /></td>";
                if ($mail != 5)
                    $str .= "<td><input id='Archive' type='submit' name='Archive' value='" . localize("Archiver") . "' /></td>";
                $str .= "</tr></table>";
                $str .= "<table class='maintable centerareawidth'>\n";
                $str .= "<tr>\n";
                $str .= "<td class='mainbglabel' width='30px' align='center'><a href='#' class='all' onclick=\"invertCheckboxes('form'); return false;\"><img src='../pics/misc/all.gif' style='border: 0' /></a></td>\n";
                $str .= "<td class='mainbglabel' width='60px'>" . localize('Non lu') . "</td>\n";
                $str .= "<td class='mainbglabel' width='75px' align='center'>" . localize('Important') . "</td>\n";
                $str .= "<td class='mainbglabel' width='240px'>" . localize('Titre') . "</td>\n";
                $str .= "<td class='mainbglabel' width='125px' align='center'>" . $addr . "</td>\n";
                $str .= "<td class='mainbglabel' width='140px' align='center'>" . localize('Date') . "</td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                $str .= "<table class='maintable'>\n";
                foreach ($data as $arr) {
                    $str .= "<tr><td class='mainbgtitle' width='40px' align='center'><input name='check[]' type='checkbox' value='" . $arr["id"] . "'/>\n";
                    if ($mail != 2 && $mail != 4)
                        $str .= "<input name='title" . $arr["id"] . "' type='hidden' value='" . localize("Re: ") . $arr["title2"] . "' />\n";
                    elseif ($mail == 5 && $sans_title2 == 0)
                        $str .= "<input name='title" . $arr["id"] . "' type='hidden' value='" . localize("Re: ") . $arr["title2"] . "' />\n";
                    $str .= "</td>";
                    $str .= "<td class='mainbgtitle' width='70px'  align='center'>" . $arr["img"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='80px'  align='center'>" . $arr["imp"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='270px'>" . $arr["title"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='138px' align='center'>" . $arr["name"] . "</td>\n";
                    $str .= "<td class='mainbgtitle' width='158px' align='center'>" . $arr["date"] . "</td></tr>\n";
                    $cpt ++;
                }
                $str .= "</table>";
            }
            
            // ---------------------------------------------------------------- Traitement de l'archivage --------------------------------------------------------
            $sans_title2 = "";
            $data = array();
            if ($arch == 1) {
                if ($archive != "") {
                    if ($mail == 2) {
                        $dbmisd = new DBCollection(
                            "SELECT MailSend.id AS idMailSend,MailSend.important,MailSend.id_MailBody,MailBody.recipient,MailBody.recipientid,MailBody.date,MailBody.title FROM MailSend LEFT JOIN MailBody ON id_MailBody=(MailBody.id) WHERE " .
                                 $archive . " ", $db);
                    } elseif ($mail == 3) {
                        $dbmitd = new DBCollection(
                            "SELECT MailTrade.id AS idMailSend,MailTrade.id_MailBody,MailTrade.title,MailTrade.new,MailTrade.id_Player\$receiver, MailTrade.date,Player.name,Player.racename, Player.id AS idplayer FROM MailTrade LEFT JOIN Player ON id_Player\$sender=Player.id WHERE " .
                                 $archive, $db);
                    } else {
                        $dbm = new DBCollection(
                            "SELECT Mail.id AS idMailSend,Mail.id_MailBody,Mail.title,Mail.new,Mail.id_Player\$receiver,Mail.date,Player.name, Player.racename, Player.id AS idplayer FROM Mail LEFT JOIN Player ON id_Player\$sender=Player.id WHERE " .
                                 $archive, $db);
                    }
                    
                    $addr = localize("Expéditeur |  Destinataire");
                    
                    if ($mail == 2) {
                        while (! $dbmisd->eof()) {
                            $idarray = explode(",", $dbmisd->get("recipientid"));
                            $namearray = explode(",", $dbmisd->get("recipient"));
                            
                            $sans_title2 = 1;
                            $player = "";
                            $first = 1;
                            foreach ($idarray as $id) {
                                list ($key, $name) = each($namearray);
                                if (! $first) {
                                    $player .= ", ";
                                }
                                $first = 0;
                                $player .= "<a href=\"../conquest/profile.php?id=" . $id . "\" class='stylepc popupify'>" . $name . "</a>";
                            }
                            $id = $dbmisd->get("idMailSend");
                            
                            $title = "<a href=\"../conquest/mailbody.php?id=" . $dbmisd->get("id_MailBody") . "&mail=2\" class='none popupify'>" .
                                $dbmisd->get("title") . "</a>";
                            
                            $img = "";
                            
                            $time = gmstrtotime($dbmisd->get("date"));
                            $date = date("Y-m-d H:i:s", $time);
                            
                            $data[] = array(
                                "date" => $date,
                                "title" => $title,
                                "name" => $player . $img,
                                "img" => "",
                                "id" => $id
                            );
                            $dbmisd->next();
                        }
                    } elseif ($mail == 3) {
                        while (! $dbmitd->eof()) {
                            $sans_title2 = 0;
                            $id = $dbmitd->get("idMailSend");
                            $player = "";
                            if ($dbmitd->get("name") != "")
                                $player = "<a href=\"../conquest/profile.php?id=" . $dbmitd->get("idplayer") . "\" class='stylepc popupify'>" . $dbmitd->get("name") .
                                     "</a>";
                            else
                                $player = "<a href=\"../conquest/profile.php?id=" . $dbmitd->get("idplayer") . "\" class='stylepc popupify'>" . $dbmitd->get("racename") .
                                     "</a>";
                            $title = "";
                            if ($dbmitd->get("title") != "")
                                $title = "<a href=\"../conquest/mailbody.php?id=" . $dbmitd->get("id_MailBody") . "&mail=3\" class='none popupify'>" .
                                    $dbmitd->get("title") . "</a>";
                            
                            if ($dbmitd->get("new") == "1")
                                $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                            else
                                $img = "";
                            
                            $time = gmstrtotime($dbmitd->get("date"));
                            $date = date("Y-m-d H:i:s", $time);
                            
                            $data[] = array(
                                "date" => $date,
                                "title" => $title,
                                "title2" => $dbmitd->get("title"),
                                "name" => $player,
                                "img" => $img,
                                "id" => $id
                            );
                            $dbmitd->next();
                        }
                    } else {
                        while (! $dbm->eof()) {
                            $sans_title2 = 0;
                            $id = $dbm->get("idMailSend");
                            $player = "";
                            if ($dbm->get("name") != "")
                                $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("name") . "</a>";
                            else
                                $player = "<a href=\"../conquest/profile.php?id=" . $dbm->get("idplayer") . "\" class='stylepc popupify'>" . $dbm->get("racename") .
                                     "</a>";
                            $title = "";
                            if ($dbm->get("title") != "")
                                $title = "<a href=\"../conquest/mailbody.php?id=" . $dbm->get("id_MailBody") . "&mail=3\" class='none popupify'>" .
                                    $dbm->get("title") . "</a>";
                            
                            if ($dbm->get("new") == "1")
                                $img = "<img src=\"../pics/misc/wrap.gif\" width=\"12px\">";
                            else
                                $img = "";
                            
                            $time = gmstrtotime($dbm->get("date"));
                            $date = date("Y-m-d H:i:s", $time);
                            
                            $data[] = array(
                                "date" => $date,
                                "title" => $title,
                                "title2" => $dbm->get("title"),
                                "name" => $player,
                                "img" => $img,
                                "id" => $id
                            );
                            $dbm->next();
                        }
                    }
                    
                    $str .= "<table class='maintable'>\n";
                    $playerid = $curplayer->get("id");
                    $str .= "<tr>\n";
                    $str .= "<tr><td class='mainbglabel' align='center'>Sélection:</td><td class='mainbglabel' width='270px'>Titre</td><td class='mainbglabel' width='170px' align='center'>" .
                         $addr . "</td><td class='mainbglabel' align='center'>Date</td></tr>\n";
                    $str .= "<tr><td class='mainbgtitle' align='center' rowspan='" . count($data) . "'><select class='selector cqattackselectorsize' name='CHOOSE_REPERTORY_ID'>";
                    $item = array();
                    $dbrep = new DBCollection("SELECT * FROM NameRepertory WHERE id_Player=" . $playerid, $db);
                    
                    $str .= "<option value='0' selected='selected'>" . localize("-- Choisissez un répertoire --") . "</option>";
                    while (! $dbrep->eof()) {
                        $item[] = array(
                            $dbrep->get("id") => localize($dbrep->get("name_Repertory"))
                        );
                        $dbrep->next();
                    }
                    foreach ($item as $arr) {
                        foreach ($arr as $key => $value)
                            $str .= "<option value='" . $key . "'>" . $value . "</option>";
                    }
                    $str .= "</select></td>";
                    $inc = 0;
                    foreach ($data as $arr) {
                        if ($inc > 0)
                            $str .= "<tr>";
                            // if ($mail != 2)
                            // $str .= "<input name='title" . $arr["id"] . "' type='hidden' value='" . localize("Re: ") . $arr["title2"] . "' />\n";
                            // $str .= "</td>";
                        $str .= "<td class='mainbgtitle' width='270px'>" . $arr["title"] . "</td>\n";
                        $str .= "<td class='mainbgtitle' width='125px' align='center'>" . $arr["name"] . "</td>\n";
                        $str .= "<td class='mainbgtitle' width='140px' align='center'>" . $arr["date"] . "</td></tr>\n";
                        $str .= "</tr>";
                        $inc = $inc + 1;
                    }
                    $str .= "<tr><td><input id='Place' type='submit' name='Place' value='" . localize("Archiver dans le répertoire") . "' /></td></tr>";
                    $str .= "<td><input name='select' type='hidden' value='" . $archive . "' /></td>";
                    // $str.="<tr><td class='mainbglabel'> Listes des id des message(s): ".$archive."</td></tr>";
                    $str .= "</table>";
                } else {
                    $str .= "<table class=\"maintable centerareawidth\">";
                    $str .= "<tr>\n";
                    $str .= "<td colspan='6' class='mainbgtitle'>\n";
                    $str .= "Vous n'avez pas sélectionné de message à archiver.";
                    $str .= "</td>\n";
                    $str .= "</tr>\n";
                    $str .= "</table>\n";
                }
            }
            // --------------------------------------------------------- Fin traitement archivage -----------------------------------------------------------------
            
            $str .= "<input name='__stepEvt' type='hidden' Value='" . $step . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        } else {
            $str .= "<table>";
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'>\n";
            $str .= localize("Ce joueur ne peut pas envoyer de message");
            $str .= "</td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
        }
        return $str;
    }

    /* -------------------------------------------------------------------- Public function ------------------------------------------------------------------ */
    
    // Fonction pour archiver les messages ------------------------ Archive ---------------------------------------
    public function archiveMail($mail, $archive, $playerid, $db)
    {
        if (isset($_POST["CHOOSE_REPERTORY_ID"])) {
            $repertoryId = $_POST["CHOOSE_REPERTORY_ID"];
            if ($mail == 2) // Send msg
{
                $msgToArchive = $archive;
                $dbm = new DBCollection("UPDATE MailSend SET id_Repertory='$repertoryId' WHERE " . $msgToArchive, $db, 0, 0, false);
                $dbma = new DBCollection(
                    "INSERT INTO MailArchive (id_MailBody,title,important,id_repertory,date) 
			SELECT id_MailBody,MailBody.title,important,id_Repertory,MailBody.date 
			FROM MailSend LEFT JOIN MailBody ON id_MailBody=(MailBody.id) WHERE " . $msgToArchive, $db, 0, 0, false);
                $dbma2 = new DBCollection("SELECT * FROM MailArchive WHERE id_Repertory='$repertoryId' AND id_Player=0 ", $db);
                while (! $dbma2->eof()) {
                    $dbu1 = new DBCollection(
                        "UPDATE MailArchive SET id_Player=" . $playerid . ", id_MailBody=(id_MailBody-10)
					WHERE id_Player=0 AND id_Repertory='" . $dbma2->get("id_repertory") . "' ", $db);
                    $dbma2->next();
                }
                $dbmd = new DBCollection("DELETE FROM MailSend WHERE " . $msgToArchive, $db, 0, 0, false);
                return localize("Message(s) archivé(s)");
            } elseif ($mail == 3) // Trade msg
{
                $curplayer = $this->curplayer;
                $id = $curplayer->get("id");
                $msgToArchive = $archive;
                $dbm = new DBCollection("UPDATE MailTrade SET id_repertory='$repertoryId' WHERE id_Player\$receiver=" . $id . " AND (" . $msgToArchive . ") ", $db, 0, 0, false);
                $dbma = new DBCollection(
                    "INSERT INTO MailArchive (id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_Repertory,date) 
			SELECT id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date FROM MailTrade WHERE " . $msgToArchive, $db, 
                    0, 0, false);
                $dbma2 = new DBCollection("SELECT * FROM MailArchive WHERE id_repertory='$repertoryId'", $db);
                while (! $dbma2->eof()) {
                    $dbu = new DBCollection("UPDATE MailArchive SET id_Player=" . $playerid . " WHERE id_Player=0 AND id_Repertory=" . $repertoryId . " ", $db, 0, 0, false);
                    $dbma2->next();
                }
                $dbmd = new DBCollection("DELETE FROM MailTrade WHERE " . $msgToArchive, $db, 0, 0, false);
                return localize("Message(s) archivé(s)");
            } else { // Standard msg
                $msgToArchive = $archive;
                $dbm = new DBCollection("UPDATE Mail SET id_Repertory='$repertoryId' WHERE " . $msgToArchive, $db, 0, 0, false);
                $dbma = new DBCollection(
                    "INSERT INTO MailArchive (id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date) 
			SELECT id_Player\$receiver,id_Player\$sender,id_MailBody,new,title,important,id_repertory,date FROM Mail WHERE " . $msgToArchive, $db, 0, 0, 
                    false);
                $dbma2 = new DBCollection("SELECT * FROM MailArchive WHERE id_repertory='$repertoryId'", $db);
                while (! $dbma2->eof()) {
                    $dbu = new DBCollection("UPDATE MailArchive SET id_Player=" . $playerid . " WHERE id_Player=0 AND id_repertory='" . $repertoryId . "' ", $db, 0, 0, false);
                    $dbma2->next();
                }
                $dbmd = new DBCollection("DELETE FROM Mail WHERE " . $msgToArchive, $db, 0, 0, false);
                return localize("Message(s) archivé(s)");
            }
        }
    }
    
    // Fonction pour déclarer un message comme important ---------------------------- Important ---------------------------------
    public function importantMail($mail, $db)
    {
        $important = 0;
        if (isset($_GET["priority"]))
            $msgToImportant = "id=" . quote_smart($_GET["priority"]);
        else
            $msgToImportant = $this->getCheckedDelMailCond("id");
        
        if ($msgToImportant != "") {
            if ($mail == 2) {
                $dbi = new DBCollection("SELECT important FROM MailSend WHERE " . $msgToImportant, $db);
                $important = $dbi->get("important");
                if ($important == 0) {
                    $dbm = new DBCollection("UPDATE MailSend SET important=1 WHERE " . $msgToImportant, $db, 0, 0, false);
                    return localize("Message(s) marqué(s) comme important");
                } else {
                    $dbm = new DBCollection("UPDATE MailSend SET important=0 WHERE " . $msgToImportant, $db, 0, 0, false);
                    return localize("Importance supprimé");
                }
            } elseif ($mail == 3) {
                $dbi = new DBCollection("SELECT important FROM MailTrade WHERE " . $msgToImportant, $db);
                $important = $dbi->get("important");
                if ($important == 0) {
                    $dbm = new DBCollection("UPDATE MailTrade SET important=1 WHERE " . $msgToImportant, $db, 0, 0, false);
                    return localize("Message(s) marqué(s) comme important");
                } else {
                    $dbm = new DBCollection("UPDATE MailTrade SET important=0 WHERE " . $msgToImportant, $db, 0, 0, false);
                    return localize("Importance supprimé");
                }
            } else {
                $dbi = new DBCollection("SELECT important FROM Mail WHERE " . $msgToImportant, $db);
                $important = $dbi->get("important");
                if ($important == 0) {
                    $dbm = new DBCollection("UPDATE Mail SET important=1 WHERE " . $msgToImportant, $db, 0, 0, false);
                    return localize("Message(s) marqué(s) comme important");
                } else {
                    $dbm = new DBCollection("UPDATE Mail SET important=0 WHERE " . $msgToImportant, $db, 0, 0, false);
                    return localize("Importance supprimé");
                }
            }
        } else {
            return localize("Aucun message marqué comme important");
        }
        $str .= "<div class='profile'>\n";
    }
    
    // Fonction pour déclarer un message comme lu ou non lu, selon son état antérieur-----------------
    public function FlagReadMail($mail, $db)
    {
        $msgToRead = $this->getCheckedDelMailCond("id");
        if ($msgToRead != "") {
            if ($mail == 3) {
                
                $dbm = new DBCollection("UPDATE MailTrade SET new=MOD((new + 1),2) WHERE " . $msgToRead, $db, 0, 0, false);
            } elseif ($mail == 5) {
                
                $dbm = new DBCollection("UPDATE MailAlert SET new=MOD((new + 1),2) WHERE " . $msgToRead, $db, 0, 0, false);
            } else {
                
                $dbm = new DBCollection("UPDATE Mail SET new=MOD((new + 1),2) WHERE " . $msgToRead, $db, 0, 0, false);
            }
        }
        return localize("Opération effectuée");
        $str .= "<div class='profile'>\n";
    }
    
    // fonction pour supprimer les messages ----------------------------------- Delete ------------------------------------------
    public function deleteMail($mail, $db)
    {
        if ($mail == 2) {
            if (isset($_GET["delete"]))
                $msgToDelete = "MailSend.id=" . quote_smart($_GET["delete"]);
            else
                $msgToDelete = $this->getCheckedDelMailCond("MailSend.id");
        } elseif ($mail == 3) {
            if (isset($_GET["delete"]))
                $msgToDelete = "MailTrade.id=" . quote_smart($_GET["delete"]);
            else
                $msgToDelete = $this->getCheckedDelMailCond("MailTrade.id");
        } elseif ($mail == 4) {
            if (isset($_GET["delete"])) {
                $msgToDelete = "Mail.id=" . quote_smart($_GET["delete"]);
                $msgToDeleteMtd = "MailTrade.id=" . quote_smart($_GET["delete"]);
                $msgToDeleteMsd = "MailSend.id=" . quote_smart($_GET["delete"]);
            } else {
                $msgToDelete = $this->getCheckedDelMailCond("Mail.id");
                $msgToDeleteMtd = $this->getCheckedDelMailCond("MailTrade.id");
                $msgToDeleteMsd = $this->getCheckedDelMailCond("MailSend.id");
            }
        } elseif ($mail == 5) {
            if (isset($_GET["delete"])) {
                $msgToDelete = "MailAlert.id=" . quote_smart($_GET["delete"]);
            } else {
                $msgToDelete = $this->getCheckedDelMailCond("MailAlert.id");
            }
        } else {
            if (isset($_GET["delete"])) {
                $msgToDelete = "Mail.id=" . quote_smart($_GET["delete"]);
            } else {
                $msgToDelete = $this->getCheckedDelMailCond("Mail.id");
            }
        }
        
        if ((isset($msgToDelete) && $msgToDelete != "") || (isset($msgToDeleteMtd) && $msgToDeleteMtd != "") || (isset($msgToDeleteMsd) && $msgToDeleteMsd != "")) {
            if ($mail == 2) {
                $dbm = new DBCollection("UPDATE MailBody LEFT JOIN MailSend ON MailBody.id=(MailSend.id_MailBody-10) SET cpt=cpt-1 WHERE " . $msgToDelete, $db, 0, 0, false);
                $dbm = new DBCollection("DELETE FROM MailSend WHERE " . $msgToDelete, $db, 0, 0, false);
            } elseif ($mail == 3) {
                $dbm = new DBCollection("UPDATE MailBody LEFT JOIN MailTrade ON MailBody.id=MailTrade.id_MailBody SET cpt=cpt-1 WHERE " . $msgToDelete, $db, 0, 0, false);
                $dbm = new DBCollection("DELETE FROM MailTrade WHERE " . $msgToDelete, $db, 0, 0, false);
            } /*
               * elseif($mail==4)
               * {
               * $dbm=new DBCollection("UPDATE MailBody LEFT JOIN Mail ON MailBody.id=Mail.id_MailBody SET cpt=cpt-1 WHERE ".$msgToDelete,$db,0,0,false);
               * $dbm=new DBCollection("DELETE FROM Mail WHERE ".$msgToDelete,$db,0,0,false);
               * $dbmitd=new DBCollection("UPDATE MailBody LEFT JOIN MailTrade ON MailBody.id=MailTrade.id_MailBody SET cpt=cpt-1 WHERE ".$msgToDeleteMtd,$db,0,0,false);
               * $dbmitd=new DBCollection("DELETE FROM MailTrade WHERE ".$msgToDeleteMtd,$db,0,0,false);
               * $dbmisd=new DBCollection("UPDATE MailBody LEFT JOIN MailSend ON MailBody.id=MailSend.id_MailBody SET cpt=cpt-1 WHERE ".$msgToDeleteMsd,$db,0,0,false);
               * $dbmisd=new DBCollection("DELETE FROM MailSend WHERE ".$msgToDeleteMsd,$db,0,0,false);
               * }
               */
elseif ($mail == 5) {
                $dbm = new DBCollection("UPDATE MailBody LEFT JOIN MailAlert ON MailBody.id=MailAlert.id_MailBody SET cpt=cpt-1 WHERE " . $msgToDelete, $db, 0, 0, false);
                $dbm = new DBCollection("DELETE FROM MailAlert WHERE " . $msgToDelete, $db, 0, 0, false);
            } else {
                $dbm = new DBCollection("UPDATE MailBody LEFT JOIN Mail ON MailBody.id=Mail.id_MailBody SET cpt=cpt-1 WHERE " . $msgToDelete, $db, 0, 0, false);
                $dbm = new DBCollection("DELETE FROM Mail WHERE " . $msgToDelete, $db, 0, 0, false);
            }
            $dbm = new DBCollection("DELETE FROM MailBody WHERE cpt=0", $db, 0, 0, false);
            
            return localize("Message(s) effacé(s)");
        } else {
            return localize("Aucun message effacé");
        }
        
        $str .= "<div class='profile'>\n";
    }
    
    // fonction qui surveille les sélections ---------------------------------------- Checked Del Mail Cond ------------------------------------------------
    public function getCheckedDelMailCond($condName)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . $val;
                else
                    $msg .= " OR " . $condName . "=" . $val;
                $first = 0;
            }
        }
        return $msg;
    }
    
    // fonction qui surveille les sélections avec en paramètre la variable $title ------------------------------ Checked Mail Cond --------------------
    public function getCheckedMailCond($condName, &$title)
    {
        $msg = "";
        $first = 1;
        
        if (isset($_POST["check"])) {
            foreach ($_POST["check"] as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . quote_smart($val);
                else
                    $msg .= " OR " . $condName . "=" . quote_smart($val);
                $first = 0;
                $title = quote_smart($_POST["title" . $val]);
            }
        }
        return $msg;
    }
    // ----------------------------------------------------------- Reply -------------------------------------------------------
    public function reply($mail, $db)
    {
        if ($mail == 3) {
            $condToReply = $this->getCheckedMailCond("MailTrade.id", $title);
            if ($condToReply != "") {
                $dbm = new DBCollection(
                    "SELECT Player.name,MailTrade.id FROM MailTrade LEFT JOIN Player ON MailTrade.id_Player\$sender=Player.id WHERE " . $condToReply . " GROUP BY Player.name", $db);
                $first = 1;
                $names = "";
                while (! $dbm->eof()) {
                    if ($first)
                        $names = $dbm->get("name");
                    else
                        $names .= "," . $dbm->get("name");
                    
                    $first = 0;
                    $dbm->next();
                }
                
                redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
                return "";
            }
        } else {
            $condToReply = $this->getCheckedMailCond("Mail.id", $title);
            if ($condToReply != "") {
                $dbm = new DBCollection("SELECT Player.name,Mail.id FROM Mail LEFT JOIN Player ON Mail.id_Player\$sender=Player.id WHERE " . $condToReply . " GROUP BY Player.name", 
                    $db);
                $first = 1;
                $names = "";
                while (! $dbm->eof()) {
                    if ($first)
                        $names = $dbm->get("name");
                    else
                        $names .= "," . $dbm->get("name");
                    
                    $first = 0;
                    $dbm->next();
                }
                
                redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
                return "";
            }
        }
        return localize("Vous devez d'abord choisir un message");
    }
    // ------------------------------------------------------- Reply To All -------------------------------------------
    public function replyToAll($mail, $db)
    {
        global $Conquest;
        
        if ($mail == 3) {
            $condToReply = $this->getCheckedMailCond("MailTrade.id", $title);
            if ($condToReply != "") {
                $dbm = new DBCollection(
                    "SELECT recipient,name FROM MailBody LEFT JOIN MailTrade ON MailBody.id=id_MailBody LEFT JOIN Player ON Player.id=MailBody.id_Player WHERE " . $condToReply, $db);
                $first = 1;
                $arr = array();
                while (! $dbm->eof()) {
                    $splited = explode(",", $dbm->get("recipient"));
                    foreach ($splited as $name)
                        $arr[$name] = 1;
                    $arr[$dbm->get("name")] = 1;
                    $dbm->next();
                }
                
                $player = $this->curplayer;
                
                unset($arr[$player->get("name")]);
                $names = "";
                foreach ($arr as $name => $val) {
                    if ($first)
                        $names = $name;
                    else
                        $names .= "," . $name;
                    
                    $first = 0;
                }
                redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
                return "";
            }
        } else {
            $condToReply = $this->getCheckedMailCond("Mail.id", $title);
            if ($condToReply != "") {
                $dbm = new DBCollection(
                    "SELECT recipient,name FROM MailBody LEFT JOIN Mail ON MailBody.id=id_MailBody LEFT JOIN Player ON Player.id=MailBody.id_Player WHERE " . $condToReply, $db);
                $first = 1;
                $arr = array();
                while (! $dbm->eof()) {
                    $splited = explode(",", $dbm->get("recipient"));
                    foreach ($splited as $name)
                        $arr[$name] = 1;
                    $arr[$dbm->get("name")] = 1;
                    $dbm->next();
                }
                
                $player = $this->curplayer;
                
                unset($arr[$player->get("name")]);
                $names = "";
                foreach ($arr as $name => $val) {
                    if ($first)
                        $names = $name;
                    else
                        $names .= "," . $name;
                    
                    $first = 0;
                }
                redirect(CONFIG_HOST . "/conquest/conquest.php?center=compose&to=" . urlencode($names) . "&re=" . urlencode($title));
                return "";
            }
        }
        return localize("Vous devez d'abord choisir un message");
    }
}
?>