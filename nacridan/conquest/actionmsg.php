<?php

/* ************************************************************** LES MESSAGES AUXILLIAIRES ******************************************************* */
class ErrorMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case ACTION_NOT_ENOUGH_AP:
                $msg = localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser cette Action.") . "<br/>";
                break;
            case ACTION_NOT_ALLOWED:
                $msg = localize("Vous n'êtes pas autorisé à effectuer cette action. <br/>");
                break;
            case ACTION_DISABLED_TARGET:
                $msg = localize("Ce joueur ne peut pas être attaqué. <br/>");
                break;
            case INV_FULL_ERROR:
                $msg = localize("Action impossible : l'inventaire cible est plein.<br/>");
                break;
            case NOT_YOUR_OBJECT_ERROR:
                $msg = localize("Erreur : un des objets ne vous appartient pas.<br/>");
                break;
            case ABILITY_ERROR:
                $msg = localize("Erreur ABILITY inconnue. <br/>");
                break;
            case MAGICSPELL_ERROR:
                $msg = localize("Erreur MAGICSPELL inconnue. <br/>");
                break;
            case TALENT_ERROR:
                $msg = localize("Erreur TALENT inconnue. <br/>");
                break;
            case ERROR_NOT_VALID_NAME:
                $msg = localize("Erreur : Aucun personnage de ce nom là. <br/>");
                break;
            case KNOWLEDGE_ALREADY_KNOWN:
                $msg = localize("Vous savez déjà confectionner ce type d'équipement. <br/>");
                break;
            case NOT_ENOUGH_MONEY_IN_BUILDING:
                $msg = localize("La caisse du bâtiment ne contient pas assez de PO. <br/>");
                break;
            case ERROR_TERRASSEMENT_NOT_DONE:
                $msg = "Vous devez terrasser votre village avant de fortifier ou <br/>d'améliorer les fortifications de votre village. <br/>";
                break;
            // Erreur de position
            case ACTION_TARGET_TOO_FAR:
                $msg = localize("Vous êtes trop éloigné de votre cible pour réaliser cette action. <br/>");
                break;
            case ACTION_BUILDING_TOO_FAR:
                $msg = localize("Vous êtes trop éloigné du bâtiment sélectionné pour réaliser cette action. <br/>");
                break;
            case ACTION_PLACE_TOO_FAR:
                $msg = localize("Un ou plusieurs emplacements séléctionnés sont trop loin de votre position pour réaliser cette action. <br/>");
                break;
            case ACTION_EXPLOITATION_TOO_FAR:
                $msg = localize("Vous êtes trop éloigné de l'exploitation séléctionnée pour réaliser cette action. <br/>");
                break;
            case ACTION_OBJECT_TOO_FAR:
                $msg = localize("Vous êtes trop éloigné de l'objet séléctionné pour réaliser cette action. <br/>");
                break;
            case ACTION_NOT_SAME_ROOM:
                $msg = localize("Action impossible : Vous devez être dans la même pièce que votre cible pour réaliser cette action. <br/>");
                break;
            case ERROR_PLACE_NOT_FREE:
                $msg = localize("Erreur : un ou plusieurs emplacement choisis ne sont pas libres.<br/>");
                break;
            case ERROR_NOT_IN_BUILDING:
                $msg = localize("Erreur : vous devez être dans un bâtiment pour pouvoir effectuer cette action.");
                break;
            case NO_PLACE_FOR_ESCORT:
                $msg = localize("Problème : il n'y a pas de place libre près de vous pour que le PNJ à escorter puisse venir vous voir. Nettoyez le coin et tentez à nouveau.");
                break;
            case NOT_IN_TURTLE:
                $msg = localize("Erreur: vous ne pouvez pas effectuer cette action à l'intérieur d'une tortue.");
                break;
            case ERROR_ENTREPOT_FULL:
                $msg = localize("Erreur: l'entrepôt est plein vous ne pouvez plus y déposer d'objet.");
                break;
            // Erreur de chargement
            case ERROR_TARGET_LOADING:
                $msg = localize("Erreur : La cible sélectionnée n'a pas pu être chargé. Réessayer. <br/>");
                break;
            case ERROR_OBJECT_LOADING:
                $msg = localize("Erreur : L'objet sélectionné n'a pas pu être chargé. Réessayer. <br/>");
                break;
            case ERROR_BUILDING_LOADING:
                $msg = localize("Erreur : Le batiment sélectionné n'a pas pu être chargé. Réessayer. <br/>");
                break;
            case ERROR_EXPLOITATION_LOADING:
                $msg = localize("Erreur : L'exploitation sélectionnée n'a pas pu être chargée. Réessayer. <br/>");
                break;
            
            // Champs manquants
            case PLACE_FIELD_INVALID:
                $msg = localize("Erreur : Un ou plusieurs emplacement n'ont pas été sélectionnés.<br/>");
                break;
            case TARGET_FIELD_INVALID:
                $msg = localize("Erreur : Une ou plusieurs cibles n'ont pas été séléctionnées.<br/>");
                break;
            case EXPLOITATION_FIELD_INVALID:
                $msg = localize("Erreur : L'exploitation n'a pas été sélectionnée.<br/>");
                break;
            case OBJECT_FIELD_INVALID:
                $msg = localize("Erreur : Un ou plusieurs objets n'ont pas été sélectionnés.<br/>");
                break;
            case CHARAC_FIELD_INVALID:
                $msg = localize("Erreur : Une ou plusieurs caractéristiques n'ont pas été sélectionnées.<br/>");
                break;
            case ARROW_FIELD_INVALID:
                $msg = localize("Erreur : Une ou plusierus flèches n'ont pas été sélectionnées.<br/>");
                break;
            case DIRECTION_FIELD_INVALID:
                $msg = localize("Erreur : Une ou plusieurs directions n'ont pas été sélectionnées.<br/>");
                break;
            case BUILDING_FIELD_INVALID:
                $msg = localize("Erreur : Un ou plusieurs bâtiments n'ont pas été sélectionnés.<br/>");
                break;
            case TYPEATTACK_FIELD_INVALID:
                $msg = localize("Vous n'avez pas sélectionné le type d'attaque. <br/>");
                break;
            case EXPLOITATIONTYPE_FIELD_INVALID:
                $msg = localize("Le type de ressource à détecter n'a pas été sélectionné. <br/>");
                break;
            case CHOICE_FIELD_INVALID:
                $msg = localize("Vous devez faire un choix à l'aide du sélecteur. <br/>");
                break;
            case MONEY_FIELD_INVALID:
                $msg = localize("Erreur : Vous devez entrer une quantité de PO valide.");
                break;
            
            // COMPETENCE
            case ACTION_WRONG_ARM_ERROR:
                $msg = localize("Erreur : vous ne pouvez pas utilisez cette compétence sur/avec ce type d'arme. <br/>");
                break;
            case ACTION_WRONG_SHIELD_ERROR:
                $msg = localize("Vous devez être équipé d'une griffe pour utiliser cette compétence.");
                break;
            case ABILITY_FORBIDDEN_IN_TURTLE:
                $msg = localize("Cette compétence est interdite lorsque vous chevauchez une tortue.");
                break;
            case ABILITY_EXORCISM_NOT_WELL_INJURED:
                $msg = localize("Il faut être gravement blessé pour utiliser cette compétence.");
                break;
            // EQUIPEMENT
            case WEARED_OBJECT_ERROR:
                $msg = localize("Erreur : vous portez cet objet. <br/>");
                break;
            case WRONG_BAG_ERROR:
                $msg = localize("Action impossible : cet objet ne peut pas être rangé dans ce sac.<br/>");
                break;
            case SAME_PLACE_ERROR:
                $msg = localize("Cet objet est déjà dans ce sac.<br/>");
                break;
            case ROOM_FULL_ERROR:
                $msg = localize("Erreur : il n'y a plus assez de place dans la pièce pour déposer l'objet.<br/>");
                break;
            
            // ECONOMIE
            case NOT_ENOUGH_MONEY_ERROR:
                $msg = localize("Action impossible : vous n'avez pas assez d'argent.<br/>");
                break;
            case NOT_ENOUGH_MONEY_ERROR2:
                $msg = localize("Action impossible : il n'y a pas assez d'argent dans les caisses du village.<br/>");
                break;
            case ERROR_OBJECT_ALREADY_SOLD:
                $msg = localize("Erreur : l'objet a déjà été vendu.");
                break;
            case OBJECT_NOT_NEW_ERROR:
                $msg = localize("Action impossible : l'object n'est pas dans un état impécable, veuillez le réparer.<br/>");
                break;
            case NOT_ENOUGH_SPACE_IN_VILLAGE:
                $msg = localize("Vous devez laisser au moins 3 espaces libres dans votre village.<br/>");
                break;
            // SPELL
            case SPELL_WALL_SAME_PLACE:
                $msg = localize("Erreur : vous avez selectionné plusieurs fois la même case.<br/>");
                break;
            case SPELL_WALL_DISCONNECT:
                $msg = localize("Erreur : certaines cases choisies sont trop loin de votre position.<br/>");
                break;
            case SPELL_WALL_TOO_MANY:
                $msg = localize("Vous ne pouvez pas invoquer plus de 10 murs de terre à la fois.<br/>");
                break;
            case SPELL_WALL_BUSY:
                $msg = localize("Vous ne pouvez pas invoquer un mur sur une case occupée.<br/>");
                break;
            case TARGET_ON_THE_GROUND_ERROR:
                $msg = localize("Erreur : cette cible est déjà au sol, le lancer de bolas n'aura aucun effet.<br/>");
                break;
            case PILLARS_INCORRECT_POSITION:
                $msg = localize("Erreur : Les piliers ne sont pas positionnés correctement.<br/>");
                break;
            case SPELL_FIRECALL_NO_PLACE:
                $msg = localize("Erreur : Aucune case adjacente n'est disponible pour invoquer le golem de feu.<br/>");
                break;
            case TOO_MANY_SPRING:
                $msg = localize("Vous ne pouvez pas invoquer plus de deux bassins divins à la fois. Vous devez d'abord en révoquer un. <br/>");
                break;
            case GOLEM_CONTROL_LOST:
                $msg = localize("Votre golem de feu se trouve trop loin. Vous ne pouvez pas le contrôler d'ici.");
                break;
            case DEMONPUNCH_NPC_ERROR:
                $msg = localize("Vous devez cibler le corps pour utiliser cette compétence sur un monstre.");
                break;
            case SPELL_FORBIDDEN_IN_TURTLE:
                $msg = localize("Ce sort est interdit lorsque vous chevauchez une tortue.");
                break;
            case FIRE_GOLEM_ALREADY_EXISTING:
                $msg = localize("Vous ne pouvez invoquer qu'un seul Golem de Feu à la fois.");
                break;
            
            // Talent
            case ERROR_WRONG_RAWMATERIAL:
                $msg = localize("Erreur : vous ne pouvez pas créer/enchanter cet objet avec ce type de matière première.<br/>");
                break;
            case ERROR_SAME_OBJECT:
                $msg = localize("Erreur : vous avez choisi le même objet.<br/>");
                break;
            case ERROR_NOT_SAME_TYPE_OBJECT:
                $msg = localize("Erreur : vous n'avez pas choisi le même type d'objet.<br/>");
                break;
            case TALENT_GEMCRAFT_TEMPLATE_IGNORED:
                $msg = localize("Le champ template doit être ignoré pour cet objet.");
                break;
            case TALENT_RAWMATERIAL_MISSED:
                $msg = localize("Erreur : un champ de matière première n'a pas été rempli.");
                break;
            case TALENT_RAWMATERIAL_IGNORED:
                $msg = localize("Erreur : vous devez sélectionner -ignorer- pour certain champs pour continuer cette action.");
                break;
            case TALENT_TEMPLATE_MISSED:
                $msg = localize("Erreur : l'enchantement n'a pas été sélectionné.");
                break;
            case TALENT_CLOSEGATE_TOO_FAR:
                $msg = localize("Vous êtes trop loin d'un temple pour recourir à une incantation de fermeture de portail");
                break;
            case ERROR_LEVEL_TOO_HIGH:
                $msg = localize("Erreur : Le niveau d'artisanat ne peut dépasser le niveau de la matière première.");
                break;
            case NO_GATE:
                $msg = localize("Vous n'avez aucune chance d'attirer un Kradjeck ferreux sans un portail dans votre vue.");
                break;
            case TERMINATED_OBJECT_ERROR:
                $msg = localize("Cet objet n'est pas en cours de fabrication.");
                break;
            case TALENT_GEMCRAFT_TEMPLATE_NO_BE_IGNORED:
                $msg = localize("Le champ template ne doit pas être ignoré pour réaliser cette action.");
                break;
            
            case TEMPLE_TELEPORT_NOT_ALLOWED:
                $msg = localize("Vous ne pouvez pas vous téléporter dans ce village car l'accès vous y est interdit.");
                break;
            case TEMPLE_RESURECTION_NOT_ALLOWED:
                $msg = localize("Le gouverneur du village ne vous a pas autorisé à placer votre lieu de réssurection dans ce village .");
                break;
            case TELEPORT_PRIEST_DEAD:
                $msg = localize("La téléporation dans ce village est impossible car le prêtre en charge du temple a été tué.");
                break;
            case RESURRECT_PRIEST_DEAD:
                $msg = localize("Vous ne pouvez pas fixer votre âme à ce temple car le prêtre en charge a été tué.");
                break;
            case ERROR_ALREADY_GOUVERNOR:
                $msg = localize("Impossible : ce personnage est déjà le gouverneur du village.");
                break;
            case ERROR_PUTSCH_ALREADY_EXIST:
                $msg = localize("Impossible : ce personnage est déjà à la tête d'un putsch.");
                break;
            case ERROR_LOYALTY_TOO_SMALL:
                $msg = localize("Impossible : la loyauté du village est inférieur à 70%.");
                break;
            default:
                {
                    trigger_error(localize("Résultat d'ACTION {error} inconnu.", array(
                            "error" => $error
                    )));
                }
        }
        return $msg;
    }
}

class GainXPMsg
{

    function msgBuilder ($param)
    {
        $msg = "<br/>";
        $msg = localize("Cette action vous a rapporté {xp} px ", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        
        if (isset($param["NOMBRE"])) {
            for ($i = 1; $i < $param["NOMBRE"]; $i ++) {
                $msg .= localize("{name} gagne {xp} px ", array(
                        "name" => $param["PLAYER" . $i],
                        "xp" => $param["GAIN" . $i]
                )) . "<br/>";
            }
        }
        return $msg;
    }
}

class FallMsg
{

    public function msgBuilder ($param)
    {
        $msg = "";
        if ((isset($param["MONEY_FALL"]) && $param["MONEY_FALL"] != 0) || (isset($param["EQUIPMENT_FALL"]) && $param["EQUIPMENT_FALL"] != 0)) {
            $msg .= localize("Il a de plus laissé tomber : ");
            
            if (isset($param["MONEY_FALL"]) && $param["MONEY_FALL"] != 0) {
                $msg .= localize("<b>{nb}</b> Pièce(s) d'Or.", array(
                        "nb" => $param["MONEY_FALL"]
                ));
                $msg .= "<br/>";
            }
            if (isset($param["EQUIPMENT_FALL"]) && $param["EQUIPMENT_FALL"] != 0) {
                $msg .= localize("{name}.", array(
                        "name" => $param["DROP_NAME"]
                ));
                $msg .= "<br/>";
            }
        }
        
        if (isset($param["LARCIN"]) && $param["LARCIN"] > 0) {
            if ($param["LARCIN"] == 1)
                $msg .= localize("Il a de plus laissé tomber <b>1</b> pièce d'Equipement car il était ciblé par un larcin");
            else
                $msg .= localize("Aucun équipement n'est tombé malgré le larcin car il n'en transportait pas.");
            $msg .= "<br/>";
        }
        
        return $msg;
    }
}

/* ************************************************************** LES ACTIONS DE BASE ************************************************************* */
class MoveMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case MOVE_NO_ERROR:
                {
                    $msg = localize("Déplacement réussi.") . "<br/>" . localize("Cette Action vous a coûté") . " " . $param["AP"] . " " . localize("PA") . ".<br/><br/>";
                    $msg .= localize("Votre nouvelle position est : ") . "X=" . $param["NEWX"] . " / Y=" . $param["NEWY"];
                }
                break;
            case MOVE_NOT_ENOUGH_AP:
                {
                    $msg = localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser cette Action.") . "<br/>";
                    // $msg=localize("Not enough AP").".<br/>";
                }
                break;
            case MOVE_POSITION_UNCHANGED:
                {
                    $msg = localize("Vous ne vous êtes pas déplacé, cette Action vous a coûté 0 PA !") . ".<br/>";
                }
                break;
            case MOVE_INVALID_POSITION:
                {
                    $msg = localize("Position non valide {x} {y}, cette Action vous a coûté 0 PA !", array(
                            "x" => $param["NEWX"],
                            "y" => $param["NEWY"]
                    )) . ".<br/>";
                }
                break;
            case MOVE_TOO_FAR:
                {
                    $msg = localize("Vous ne pouvez pas vous déplacer si loin, cette Action vous a coûté 0 PA !") . ".<br/>";
                }
                break;
            case MOVE_CASE_NOT_FREE:
                {
                    $msg = localize("Déplacement impossible, cette case est occupée, cette Action vous a coûté 0 PA !") . ".<br/>";
                }
                break;
            case GOLEM_MOVE_TOO_FAR:
                $msg = localize("Vous ne pouvez déplacer votre golem de feu aussi loin de vous.");
                break;
            case GOLEM_CONTROL_LOST:
                $msg = localize("Votre golem de feu se trouve trop loin. Vous ne pouvez pas le contrôler d'ici.");
                break;
            
            default:
                {
                    $msg = localize("Résultat de DEPLACEMENT {error} inconnu.", array(
                            "error" => $error
                    )) . "<br/>";
                }
        }
        return $msg;
    }
}

class EnterBuildingMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case MOVE_NO_ERROR:
                $msg = localize("Vous êtes entré dans un bâtiment.") . "<br/>" . localize("Cette Action vous a coûté") . " " . $param["AP"] . " " . localize("PA") . ".<br/><br/>";
                break;
            
            case MOVE_NOT_ENOUGH_AP:
                $msg = localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser cette Action.") . "<br/>";
                break;
            case GOLEM_CONTROL_LOST:
                $msg = localize("Votre golem de feu se trouve trop loin. Vous ne pouvez pas le contrôler d'ici.");
                break;
            case GOLEM_MOVE_TOO_FAR:
                $msg = localize("Vous ne pouvez déplacer votre golem de feu aussi loin de vous.");
                break;
            default:
                $msg = localize("Résultat de DEPLACEMENT {error} inconnu.", array(
                        "error" => $error
                )) . "<br/>";
        }
        return $msg;
    }
}

class LeaveBuildingMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case MOVE_NO_ERROR:
                $msg = localize("Vous êtes sorti d'un bâtiment.") . "<br/>" . localize("Cette Action vous a coûté") . " " . $param["AP"] . " " . localize("PA") . ".<br/><br/>";
                break;
            
            case MOVE_NOT_ENOUGH_AP:
                $msg = localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser cette Action.") . "<br/>";
                break;
            case GOLEM_CONTROL_LOST:
                $msg = localize("Votre golem de feu se trouve trop loin. Vous ne pouvez pas le contrôler d'ici.");
                break;
            case GOLEM_MOVE_TOO_FAR:
                $msg = localize("Vous ne pouvez déplacer votre golem de feu aussi loin de vous.");
                break;
            default:
                $msg = localize("Résultat de DEPLACEMENT {error} inconnu.", array(
                        "error" => $error
                )) . "<br/>";
        }
        return $msg;
    }
}

class HustleMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = "";
            if ($param["JET"]) {
                $msg .= localize("Vous tentez de bousculer " . $param["TARGET_NAME"]) . " (" . $param["TARGET_ID"] . ")<br>";
                $msg .= localize("Votre Jet de maitrise  est de ");
                $msg .= localize("............: " . $param["PLAYER_ATTACK"]) . "<br/>";
                $msg .= localize("Le Jet de maitrise de votre adversaire est de ");
                $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
            }
            if ($param["SUCCESS"]) {
                $msg .= localize("Action réussie : vous avez interverti vos places avec {target}" . " (" . $param["TARGET_ID"] . ")", array(
                        "target" => localize($param["TARGET_NAME"])
                )) . "<br/>";
                $msg .= localize("Votre nouvelle position est : ") . "X=" . $param["XNEW"] . " / Y=" . $param["YNEW"] . "<br/><br/>";
            } else
                $msg .= localize("Action ratée : vous n'avez pas réussi à intervertir vos places avec {target} ", array(
                        "target" => localize($param["TARGET_NAME"])
                )) . "<br/>";
            
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ConcentrationMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous vous concentrez sur les raisons de votre échec et refaites rapidement les gestes techniques de l'action " . $param["NAME"]) . "<br/><br/>";
            
            $msg .= localize("Votre pourcentage de maitrise est de {percent}%", array(
                    "percent" => $param["PERCENT"]
            )) . "<br/>";
            $msg .= localize("Votre jet d'amélioration est de ");
            $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["SCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <br/>";
            
            if ($param["SUCCESS"])
                $msg .= localize("Vous améliorez votre maîtrise de l'action " . $param["NAME"] . " de <b>{enhance}</b>% .", array(
                        "enhance" => $param["ENHANCE"]
                )) . "<br/>";
            else
                $msg .= " " . localize("Vous n'améliorez pas votre maîtrise.") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class FreeMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            if ($param["TYPE_ATTACK"] != FREE_TURTLE_EVENT) {
                if ($param["PC_STILL_CREEPING"] == "YES") {
                    $msg = localize("Action réussie : vous vous êtes libéré d'un bolas, malheureusement vous êtes toujours entravé.") . "<br/>";
                } else {
                    $msg = localize("Action réussie : vous vous êtes libéré puis relevé.") . "<br/>";
                }
            } else {
                if ($param["PC_STILL_CREEPING"] == "YES") {
                    $msg = localize("Action réussie : vous avez libéré votre tortue de son entrave, malheureusement elle est toujours entravée.") . "<br/>";
                } else {
                    $msg = localize("Action réussie : vous avez libéré votre tortue de son entrave.") . "<br/>";
                }
            }
            
            $msg .= localize("L'objet : {name} est resté au sol", array(
                    "name" => $param["NAME"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a couté 8 PA. ", array(
                    "name" => $param["NAME"]
            )) . "<br/>";
        } 

        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class DestroyMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Action réussie.") . "<br/>";
            $msg .= localize("Cette action vous a coûté " . DESTROY_AP . " points d'action") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ExitJailMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous êtes sorti de prison.") . "<br/>";
            $msg .= localize("Votre sursis en cas de récidive est de " . $param["PRISON"] . " heures") . "<br/>";
            $msg .= localize("Il réduira avec le temps si vous vous tenez à carreaux") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class PrisonGraffitiMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            if ($param["FINI"]) {
                $msg = localize("Vous avez écrit sur le mur de la prison et signé votre gravure.") . "<br/>";
            } else {
                $msg = localize("Vous avez écrit sur le mur de la prison.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ContinueGraffitiMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            if ($param["FINI"]) {
                $msg = localize("Vous avez terminé et signé votre texte.") . "<br/>";
            } else {
                $msg = localize("Vous avez continué votre texte.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ConsultGraffitiMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous avez lu les gravures sur les murs de la prison.<br /><table class='maintable bottomareawidth'>");
            $msg .= "<tr><td class='mainbgtitle' style='text-align:center;'>" . localize("<br />** Gravures terminées **") . "</td></tr>";
            for ($i = 0; $i < $param["NB1"]; $i ++) {
                $msg .= "<tr><td class='mainbgtitle'>" . localize($param["Text" . $i]) . "</td></tr>";
            }
            $msg .= "<tr><td class='mainbgtitle' style='text-align:center;'>" . localize("<br />** Gravures non-terminées **") . "</td></tr>";
            for ($i = $param["NB1"]; $i < $param["NB2"]; $i ++) {
                $msg .= "<tr><td class='mainbgtitle'>" . localize($param["Text" . $i]) . "</td></tr>";
            }
            $msg .= "</table>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class AppearMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Action réussie : vous êtes à nouveau visible pour tous.") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class GiveMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("L'objet : {name}({id}) a été donné avec succès.", array(
                    "name" => $param["OBJECT_NAME"],
                    "id" => $param["OBJECT_ID"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class GiveMoneyMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous avez donné {po} pièces d'or à {target}" . " (" . $param["TARGET_ID"] . ").", array(
                    "target" => $param["TARGET_NAME"],
                    "po" => $param["VALUE"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class RPActionMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous avez effectué une action personnalisée pour 1PA.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class NewATBMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case NEWATB_NO_ERROR:
                {
                    if (isset($param["AP"])) {
                        $msg = "";
                        
                        if ($param["DLA"] == 1) {
                            $pluriel = "s";
                            if ($param["PLAYER_REGEN"] == 1 || $param["PLAYER_REGEN"] == 1)
                                $pluriel = "";
                            $msg .= localize("Votre personnage <b>{name}</b> entre dans une nouvelle phase de jeu.", array(
                                    "name" => $param["PLAYER_NAME"]
                            )) . "<br/>";
                            if ($param["PLAYER_REGEN"] > 0)
                                $msg .= localize("Il régénère {regen} point{s} de vie", array(
                                        "s" => $pluriel,
                                        "regen" => $param["PLAYER_REGEN"]
                                )) . "<br/><br/>";
                            if ($param["PLAYER_REGEN"] < 0)
                                $msg .= localize("Il perd {regen} point{s} de vie", array(
                                        "s" => $pluriel,
                                        "regen" => - $param["PLAYER_REGEN"]
                                )) . "<br/><br/>";
                        }
                        
                        if ($param["RESURRECT"] == 1)
                            $msg .= localize("<b>{name}</b> est mort, il a été ressucité à l'endroit de votre lieu de résurrection.", array(
                                    "name" => $param["PLAYER_NAME"]
                            )) . "<br/><br/>";
                        if ($param["DLA"] == 1) {
                            if ($param["OVERLOAD"] == 1)
                                $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> est en surcharge. Il subit un malus de la moitié de ses Points d'Action.", array(
                                        "name" => $param["PLAYER_NAME"]
                                )) . "<br/><br/>";
                            elseif ($param["OVERLOAD"] == 2)
                                $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> est en lourde surcharge. Il subit un malus des 3/4 de ses Points d'Action.", array(
                                        "name" => $param["PLAYER_NAME"]
                                )) . "<br/><br/>";
                            if ($param["PLAYER_RECALL"])
                                $msg .= localize("Un sort de rappel est disponible. Vous devez l'accepter ou le refuser avant toute autre action") . "<br/><br/>";
                            if ($param["LOST_TURTLE"] == 1)
                                $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> était responsable d'une caravane mais la tortue a été tuée. La mission commerciale a été annulée. Votre réputation de marchand diminue.", array(
                                        "name" => $param["PLAYER_NAME"]
                                )) . "<br/><br/>";
                            if ($param["LATE_CARAVAN"] == 1)
                                $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> est responsable d'une caravane dont la date limite est dépassée. Tant que vous n'aurez pas atteint le comptoir commerciale de destination, votre réputation de marchand diminuera à chaque nouvelle dla. En complétant la mission vous pourrez récupérer votre mise et regagner un peu de réputation.", array(
                                        "name" => $param["PLAYER_NAME"]
                                )) . "<br/><br/>";
                            if ($param["NPC_ESCORT_DIED"] == 1)
                                $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> était responsable d'une mission d'escorte mais le personnage à escorter à été tué. La mission est annulée.", array(
                                        "name" => $param["PLAYER_NAME"]
                                )) . "<br/><br/>";
                            if ($param["LATE_ESCORT"] == 1)
                                $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> est responsable d'une mission d'escorte dont la date limite est dépassée. Le personnage à escorter vous a quitté, pensant qu'il arrivera plus vite sans vous. La mission est annulée.", array(
                                        "name" => $param["PLAYER_NAME"]
                                )) . "<br/><br/>";
                            if ($param["GOLEM_NOT_ENOUGH_MM"] == 1)
                                $msg .= localize("Le jet de sauvegarde de contrôle de golem a été insuffisant. Celui-ci s'est dissipé") . "<br/><br/>";
                        }
                        
                        if ($param["BOLAS"] == 1)
                            $msg .= localize("ATTENTION ! Votre personnage <b>{name}</b> est entravé par un bolas.", array(
                                    "name" => $param["PLAYER_NAME"]
                            )) . "<br/><br/>";
                        
                        if ($param["BOLAS_TURTLE"])
                            $msg .= localize("ATTENTION ! Votre tortue est entravée par un bolas.", array(
                                    "name" => $param["PLAYER_NAME"]
                            )) . "<br/><br/>";
                        
                        if ($param["DLA"] == 1) {
                            $msg .= localize("Il dispose de {ap} Points d'Action.", array(
                                    "ap" => $param["AP"]
                            )) . "<br/>";
                        } else {
                            $msg .= localize("Votre personnage <b>{name}</b> dispose de {ap} Points d'Action.", array(
                                    "name" => $param["PLAYER_NAME"],
                                    "ap" => $param["AP"]
                            )) . "<br/>";
                        }
                        if (isset($param["MSG"]) && $param["MSG"] > 0)
                            $msg .= localize("Vous avez {nb} message(s) non lu(s)", array(
                                    "nb" => $param["MSG"]
                            )) . "<br/><br/>";
                        if (isset($param["MSG_TRD"]) && $param["MSG_TRD"] > 0) {
                            if ($param["MSG_TRD"] == 1)
                                $msg .= localize("Vous avez {nb} message commercial non lu", array(
                                        "nb" => $param["MSG_TRD"]
                                )) . "<br/>";
                            else
                                $msg .= localize("Vous avez {nb} messages commerciaux non lus", array(
                                        "nb" => $param["MSG_TRD"]
                                )) . "<br/>";
                        }
                    } 

                    else {
                        $msg = localize("La DLA de <b>{name}</b> a été activée.<br/><br/>", array(
                                "name" => $param["PLAYER_NAME"]
                        ));
                    }
                }
                break;
            default:
                {
                    $msg = localize("Résultat de NEWDLA {error} inconnu.", array(
                            "error" => $error
                    )) . "<br/>";
                }
        }
        return $msg;
    }
}

class RecallMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case CHOICE_FIELD_INVALID:
                $msg = localize("Vous n'avez pas rempli le champ. <br/><br/>", array(
                        "name" => $param["TARGET_NAME"]
                ));
                break;
            case ROOM_FULL_ERROR:
                $msg = localize("La téléportation n'a pu se faire car il n'y a pas de case disponible. <br/><br/>", array(
                        "name" => $param["TARGET_NAME"]
                ));
                break;
            case ACTION_NO_ERROR:
                
                if ($param["CHOICE"]) {
                    $msg = localize("Vous avez accepté le rappel de {name}" . " (" . $param["TARGET_ID"] . "). <br/>", array(
                            "name" => $param["TARGET_NAME"]
                    ));
                    $msg .= localize("Vous avez été téléporté en X={x} et Y={y}. <br/>", array(
                            "x" => $param["X"],
                            "y" => $param["Y"]
                    ));
                    $msg .= localize("Cette action vous a coûté {ap} point d'action. <br/><br/>", array(
                            "ap" => $param["AP"]
                    ));
                } else
                    $msg = localize("Vous avez refusé le rappel de {name}" . " (" . $param["TARGET_ID"] . "). <br/><br/>", array(
                            "name" => $param["TARGET_NAME"]
                    ));
                
                break;
        }
        return $msg;
    }
}

class ArrestMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case CHOICE_FIELD_INVALID:
                $msg = localize("Vous devez faire un choix à l'aide du sélecteur.");
                break;
            case NOT_ENOUGH_MONEY_ERROR:
                $msg = localize("Vous n'avez pas suffisant d'argent pour payer l'amende.");
                break;
            case ACTION_NO_ERROR:
                
                if ($param["CHOICE"] == 1) {
                    $msg = localize("Vous avez accepter l'arrestation") . "<br/>";
                    $msg .= localize("Vous avez été conduit à la prison d'{ville} en X={x} et Y={y}. <br/>", array(
                            "ville" => $param["VILLE"],
                            "x" => $param["X"],
                            "y" => $param["Y"]
                    ));
                    $msg .= localize("Vous devez y purger votre peine de {nb} heures ({nb1} pour vos dernières actions + {nb2} de récidive) <br/>", array(
                            "nb" => $param["PEINETOT"],
                            "nb1" => $param["DER"],
                            "nb2" => $param["SURCIS"]
                    ));
                } elseif ($param["CHOICE"] == 3) {
                    $msg = localize("Vous avez rendu l'argent et payer l'amende demandée par le garde, vous êtes libre") . "<br/>";
                    // $msg.=localize("Vous avez été conduit à la prison d'{ville} en X={x} et Y={y}. <br/>",array("ville" => $param["VILLE"], "x"=>$param["X"], "y"=>$param["Y"]));
                    // $msg.=localize("Vous devez y purger votre peine de {nb} heures ({nb1} pour vos dernières actions + {nb2} de récidive) <br/>",array("nb" => $param["PEINETOT"], "nb1" => $param["DER"], "nb2" => $param["SURCIS"]));
                } 

                else
                    $msg = localize("Vous avez refusé l'arrestation. <br/><br/>");
                
                break;
        }
        return $msg;
    }
}

/* ************************************************************** MENU OBJETS ************************************************************* */
class DropMsg
{

    function msgBuilder ($error)
    {
        if (! $error)
            $msg = localize("Objet déposé.") . "<br/>" . localize("Cette Action vous a coûté") . " " . DROP_OBJECT_AP . " " . localize("PA") . ".";
        
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class PickMsg
{

    function msgBuilder ($error)
    {
        if (! $error)
            $msg = localize("Ramassage réussi.") . "<br/>" . localize("Cette Action vous a coûté") . " " . PICK_UP_OBJECT_AP . " " . localize("PA") . ".";
        
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class WearMsg
{

    function msgBuilder ($error, $nopa = 0)
    {
        if (! $error) {
            $msg = "";
            if (! $nopa)
                $msg = localize("Action réussie.") . "<br/>" . localize("Cette Action vous a coûté") . " " . WEAR_EQUIPMENT_AP . " " . localize("PA") . ".<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class UnWearMsg
{

    function msgBuilder ($error, $nopa = 0)
    {
        if (! $error)
            $msg = localize("Action réussie.") . "<br/>" . localize("Cette Action vous a coûté") . " " . UNWEAR_EQUIPMENT_AP . " " . localize("PA") . ".";
        
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class StoreMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = localize("L'objet a été rangé correctement.") . "<br/>" . localize("Cette Action vous a coûté") . " " . $param["AP"] . " " . localize("PA") . ".";
        
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class LevelUpMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                {
                    if ($param["CRABE"]) {
                        $str = localize("Vos actions n'ont pas été suffisantes pour franchir un level.", array(
                                "level" => $param["LEVEL"]
                        )) . "<br/><br/>";
                    } else {
                        $traduction = array();
                        $traduction["strength"] = "FORCE";
                        $traduction["dexterity"] = "DEXTERITE";
                        $traduction["speed"] = "VITESSE";
                        $traduction["magicSkill"] = "MAITRISE DE LA MAGIE";
                        
                        $str = localize("Félicitations ! Vous passez niveau {level}", array(
                                "level" => $param["LEVEL"]
                        )) . "<br/><br/>";
                        $str .= localize("Vous gagnez 3 points de vie.") . "<br/>";
                        
                        if ($param["FIELD1"] == $param["FIELD2"] && $param["FIELD1"] != "hp") {
                            $str .= localize("Vous progressez de 2D6 dans la caractéristique : {carac}", array(
                                    "carac" => $traduction[$param["FIELD1"]]
                            ));
                        } else {
                            if ($param["FIELD1"] == "hp")
                                $str .= localize("Vous gagnez 15 points de vie.") . "<br/>";
                            else
                                $str .= localize("Vous progressez de 1D6 dans la caractéristique : {carac}", array(
                                        "carac" => $traduction[$param["FIELD1"]]
                                )) . "<br/>";
                            
                            if ($param["FIELD2"] == "hp")
                                $str .= localize("Vous gagnez 15 points de vie.") . "<br/>";
                            else
                                $str .= localize("Vous progressez de 1D6 dans la caractéristique : {carac}", array(
                                        "carac" => $traduction[$param["FIELD2"]]
                                )) . "<br/>";
                        }
                    }
                }
                break;
            default:
                {
                    $str = localize("Résultat de Level Up {error} inconnu.", array(
                            "error" => $error
                    )) . "<br/>";
                }
        }
        return $str;
    }
}

class AnswerToPollMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                $str = "Merci d'avoir participé au sondage";
                break;
            case POLL_INACTIVE:
                $str = "Vous avez essayé de répondre à un sondage inactif";
                break;
            case POLL_ALREADY_PARTICIPATED:
                $str = "Vous avez déjà répondu à ce sondage";
                break;
            case POLL_MISSING_ANSWER:
                $str = "Vous avez oublié de répondre à l'une des questions. Veuillez réessayez, s'il vous plait.";
                break;
            default:
                {
                    $str = localize("Résultat de Level Up {error} inconnu.", array(
                            "error" => $error
                    )) . "<br/>";
                }
        }
        return $str;
    }
}

class UsePotionMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                {
                    $msg = localize("Vous avez utilisé {name} de niveau {level}  potion a été utilisé.", array(
                            "level" => $param["POTION_LEVEL"],
                            "name" => $param["POTION_NAME"]
                    )) . "<br/>";
                    if (isset($param["HP_POTENTIEL"]))
                        $msg = localize("Vous récupérez {pv} points de vie.", array(
                                "hp" => $param["HP_POTENTIEL"],
                                "pv" => $param["HP_HEAL"]
                        )) . "<br/>";
                    else {
                        $msg = localize("La potion a eu les effets suivants : ") . "<br/>";
                        $static = new StaticModifier();
                        foreach ($static->m_characLabel as $key) {
                            if (isset($param[$key])) {
                                $msg .= "- " . translateAtt($key) . localize(":") . $param[$key] . "<br/>";
                            }
                        }
                        $msg .= localize("pour 3 dla") . "<br/>";
                        
                        if (isset($param["DRUG"])) {
                            $msg .= localize("La potion est trop puissante pour vous et vous subissez un malus.") . "<br/>";
                        }
                    }
                }
                break;
                
                break;
            case ACTION_NOT_ENOUGH_AP:
                $msg = localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser cette Action.") . "<br/>";
                break;
            case INVALID_OBJECT_ERROR:
                $msg = localize("Erreur : cet objet n'existe pas. <br/>");
                break;
            default:
                {
                    $msg = localize("Résultat de Level Up {error} inconnu.", array(
                            "error" => $error
                    )) . "<br/>";
                }
        }
        return $msg;
    }
}

// ---------------------------------------------------------- Parchemin pour mission / Escorte ---------------------------------------------
class UseParchmentMsg
{

    function msgBuilder ($error, &$param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                {
                    
                    $msg = localize("Vous venez d'utiliser un parchemin pour " . USE_PARCHMENT_AP . " PA. <br/>");
                    switch ($param["POINT"]) {
                        case "Starting":
                            if ($param["MIS_ACTIV"] == 1)
                                $msg = localize("Votre mission est déjà active. Vous ne pouvez pas valider le parchemin une seconde fois au point de départ. 
						Vous devez maintenant escorter le PNJ au second point figurant sur votre parchemin.") . "<br/>";
                            
                            else
                                $msg .= localize("Vous devez vous rendre à la destination indiquée par le parchemin. ") . "<br/>";
                            break;
                        case "Stop":
                            $msg .= localize("Félicitation vous avez terminé la mission d'escorte avec succès. 
					Vous recevez votre salaire : " . $param["MONEY"] . " PO.") . "<br/>";
                            break;
                        case "NO":
                            $msg .= localize("Vous ne pouvez pas valider votre parchemin car le PNJ est trop éloigné de votre position.
					Rapprochez celui-ci près de vous afin de valider votre mission.") . "<br/>";
                            break;
                    }
                }
                break;
            
            case ALREADY_GOT_MISSION:
                $msg = localize("Vous avez actuellement une mission d'escorte en cours, vous ne pouvez pas en prendre une autre.");
                break;
            
            case ACTION_NOT_ENOUGH_AP:
                $msg = localize("Vous n'avez pas assez de Points d'Action (PA) pour réaliser cette Action.") . "<br/>";
                break;
            default:
                {
                    $msg = localize("Vous ne vous trouvez pas au lieu indiqué par le parchemin.") . "<br/>";
                }
        }
        return $msg;
    }
}
// --------------------------------------------------------------- Fin parcho mission -----------------------------------------------------------

/*
 * class UseParchmentMsg{
 *
 * function msgBuilder($error, &$param)
 * {
 * if(!$error)
 * {
 * $msg=localize("Vous avez étudié un parchemin de confection : {equip}.", array("equip"=>$param["EQUIP_NAME"]))."<br/>";
 * $msg.=localize("Vous pouvez désormais créer ce type d'équipement")."<br/>";
 * }
 * else
 * {
 * $obj = new ErrorMsg();
 * $msg = $obj->msgBuilder($error, $param);
 * }
 * return $msg;
 * }
 * }
 */

/* ************************************************************** INSIDE BUILDING ACTIONS ********************************************************** */
class TempleTeleportMsg
{

    function msgBuilder ($error, $param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                $msg = "Votre nouveau lieu est maintenant : X=" . $param["XNEW"] . ", Y=" . $param["YNEW"] . ".<br/>Cette action vous a coûté " . $param["PRICE"] . " PO (et " . TELEPORT_AP . " PA).";
                if ($param["TAXE"] != 0)
                    $msg .= " Le temple a reversé " . $param["TAXE"] . " PO de taxe au gouverneur. ";
                break;
            
            default:
                {
                    $obj = new ErrorMsg();
                    $msg = $obj->msgBuilder($error, $param);
                }
        }
        return $msg;
    }
}

class TempleHealMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "Le moine réalise une Larme de Vie de niveau " . $param["SPELL_LEVEL"] . ". Vous êtes soigné à hauteur de " . $param["HP_HEAL"] . " PV. <br/> Cela vous a coûté " . $param["PRICE"] . " PO.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TempleBlessingMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Le moine réalise un Souffle d'Athlan de niveau " . $param["SPELL_LEVEL"] . " pour " . $param["PRICE"] . " PO. Le niveau des malédictions dont vous êtes atteint diminue de " . $param["SPELL_LEVEL"] . ".");
            if ($param["TAXE"] != 0)
                $msg .= " Le temple a reversé " . $param["TAXE"] . " PO de taxe au gouverneur. ";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TempleResurrectMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Le moine a fixé votre âme à ce temple pour " . $param["PRICE"] . " PO. Vous y retournerez lorsque vous serez sur le point de mourir.");
            if ($param["TAXE"] != 0)
                $msg .= " Le temple a reversé " . $param["TAXE"] . " PO de taxe au gouverneur. ";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TemplePalaceMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez lancé la construction d un(e) {name} pour {prix} pièces d'or en ({x},{y}).", array(
                    "x" => $param["X"],
                    "y" => $param["Y"],
                    "name" => $param["NAME"],
                    "prix" => $param["PRICE"]
            )) . "$<br/><br/>";
            $msg .= localize("La construction durera entre {nb1} et {nb2} jours.", array(
                    "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                    "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
            )) . "$<br/><br/>";
            $msg .= localize("A son achèvement, rendez-vous y pour gérer le village.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class PalaceDepositMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez déposé " . $param["MONEYDEP"] . " PO dans les caisses du village.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class PalaceWithdrawMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = localize("Vous avez retiré " . $param["MONEY"] . " PO des caisse de la cité.");
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class PalaceTransferMoneyBatMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez transféré " . $param["MONEY"] . " PO de la caisse du palais vers le bâtiment " . $param["BAT_NAME"] . " (" . $param["BAT_ID"] . ")");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

// --------------- Décision sale du gouverneur -----------------------
class PalaceDecisionMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            switch ($param["DECISION"]) {
                case 2:
                    $msg = localize("La loyauté du village envers son protecteur actuel a augmenté de 10%.") . "<br/>";
                    break;
                case 1:
                    $msg = localize("La loyauté du village envers son protecteur actuel a baissé de 10%.") . "<br/>";
                    break;
                case 3:
                    $msg = localize("Vous avez engager un garde pour défendre le palais du gouverneur") . "<br/>";
                case 4:
                    break;
            }
            $msg .= localize("Cette action vous a coûté " . PALACE_DECISION_AP . " PA");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class CreatePutschMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez lancé un coup d'état.") . "<br/>";
            $msg .= localize("Si votre progresion atteint 100%, " . $param["LEADER_NAME"] . " deviendra le nouveau protecteur du village.") . "<br/>";
            $msg .= localize("Cette action vous a coûté " . PALACE_DECISION_AP . " PA");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class ActPutschMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            if ($param["BONUS"] == 10) {
                $msg = localize("Vous avez soutenu le putsch de " . $param["LEADER_NAME"]) . "<br/>";
                if ($param["EFFECT"] == 3)
                    $msg .= localize("Votre groupe a pris le contrôle du village. " . $param["LEADER_NAME"] . " en est le nouveau gouverneur") . "<br/>";
                else
                    $msg .= localize("Votre groupe progresse de 10% dans la prise de contrôle du village") . "<br/>";
            }
            if ($param["BONUS"] == - 10) {
                $msg = localize("Vous avez lutté contre le putsch de " . $param["LEADER_NAME"]) . "<br/>";
                if ($param["EFFECT"] == 4)
                    $msg .= localize("Son putsch a été annihilé.") . "<br/>";
                else
                    $msg .= localize("Vous avez réduit la prise de contrôle du village de leur groupe de 10%") . "<br/>";
            }
            
            $msg .= localize("Cette action vous a coûté " . PALACE_DECISION_AP . " PA");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class ControlTempleMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez bannis des aventuriers qui ressuscitaient dans le Temple de votre village");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* Message qui s'affiche lorsque l'on a valider le nom du village */
class NameVillageMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = localize("Vous avez appelé votre village " . $param["NAME_VILLAGE"] . " et cela vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO."); /* revenir */
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* Message qui s'affiche lorsque l'on a fortifié le village */
class FortifyVillageMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez fortifié votre village et cela vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO.") . "<br/>";
            if ($param["CONSTRUCTION_DURATION_MIN"] == $param["CONSTRUCTION_DURATION_MAX"])
                $msg .= localize("Les remparts seront entièrement construit dans {nb1} jours.", array(
                        "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                        "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                )) . "$<br/><br/>";
            else
                $msg .= localize("Les remparts seront entièrement construit dans {nb1} à {nb2} jours.", array(
                        "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                        "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                )) . "$<br/><br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* Message qui s'affiche lorsque l'on a terrassé le village */
class TerrassementMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "Vous avez terrassé votre village en " . $param["TYPE_TERRASSEMENT"] . ".<br/>";
            $msg .= "Cela vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TerraformationMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            if ($param["TERRAFORMATION_CASE"] == "") {
                $msg = "Vous avez terraformé le tour de votre village en " . $param["TYPE_TERRAFORMATION"] . ".<br/><br/>";
            } else {
                $msg = "Vous avez terraformé la case " . $param["TERRAFORMATION_CASE"] . " autour de votre village en " . $param["TYPE_TERRAFORMATION"] . ".<br/><br/>";
            }
            $msg .= "Cette action vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class UpgradeFortificationMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez amélioré les fortification de votre village et cela vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO.") . "<br/>";
            $msg .= localize("Les travaux dureront de {nb1} à {nb2} jours.", array(
                    "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                    "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
            ));
            $msg .= localize("A leur achèvement les fortications auront gagné " . FRAGILE . " points de structure.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* Gestion des portes du village */
class OperateVillageMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            switch ($param["DOOR"]) {
                case 1:
                    $msg = localize("Vous avez ouvert les portes du village à tout le monde");
                    break;
                
                case 2:
                    $msg = localize("Vous avez fermé les portes du village à tout le monde");
                    break;
                
                case 3:
                    $msg = localize("Vous avez ouvert les portes du village à vos alliés selon les critères suivants :") . "<br/>";
                    if ($param["OPTION_ALLY"] % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux membres de votre ordre.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 10) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert à vos alliés personnels.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 100) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux ordres de vos alliances personnelles.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 1000) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux membres alliés de votre ordre.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 10000) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux ordres alliés à votre ordre.") . "<br>";
                    break;
                case 4:
                    $msg = localize("Vous avez fermé les portes du village à tous vos ennemis");
                    break;
                
                default:
                    break;
            }
            $msg .= "<br/>" . localize("L'autorisation d'entrée à votre village par téléportation ainsi que la possibilité de fixer le temple du village comme lieu de résurrection n'ont pas été modifiée.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* Gestion des portes du temple */
class OperateVillageTempleMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            switch ($param["DOOR_TEMPLE"]) {
                case 1:
                    $msg = localize("Vous avez ouvert les portes du Temple à tout le monde");
                    break;
                
                case 2:
                    $msg = localize("Vous avez fermé les portes du Temple à tout le monde");
                    break;
                
                case 3:
                    $msg = localize("Vous avez ouvert les portes du Temple à vos alliés selon les critères suivants :") . "<br/>";
                    if ($param["OPTION_ALLY"] % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux membres de votre ordre.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 10) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert à vos alliés personnels.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 100) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux ordres de vos alliances personnelles.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 1000) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux membres alliés de votre ordre.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 10000) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux ordres alliés à votre ordre.") . "<br>";
                    break;
                case 4:
                    $msg = localize("Vous avez fermé les portes du Temple à tous vos ennemis");
                    break;
                
                default:
                    break;
            }
            $msg .= "<br/>" . localize("L'autorisation d'entrée à votre village par les portes n'a pas été modifiée.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class OperateVillageEntrepotMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            switch ($param["DOOR_TEMPLE"]) {
                case 1:
                    $msg = localize("Vous avez ouvert les portes de(s) Entrepôt(s) à tout le monde");
                    break;
                
                case 2:
                    $msg = localize("Vous avez fermé les portes de(s) Entrepôt(s) à tout le monde");
                    break;
                
                case 3:
                    $msg = localize("Vous avez ouvert les portes de(s) Entrepôt(s) à vos alliés selon les critères suivants :") . "<br/>";
                    if ($param["OPTION_ALLY"] % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux membres de votre ordre.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 10) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert à vos alliés personnels.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 100) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux ordres de vos alliances personnelles.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 1000) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux membres alliés de votre ordre.") . "<br>";
                    if (floor($param["OPTION_ALLY"] / 10000) % 10 == 3)
                        $msg .= localize("- Vous avez ouvert aux ordres alliés à votre ordre.") . "<br>";
                    break;
                case 4:
                    $msg = localize("Vous avez fermé les portes de(s) Entrepôt(s) à tous vos ennemis");
                    break;
                
                default:
                    break;
            }
            $msg .= "<br/>" . localize("L'autorisation d'entrée à votre village par les portes n'a pas été modifiée.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BedroomSleepingMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = localize("Vous avez dormi dans la chambre et gagné " . $param["HPGAIN"] . " PV.");
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BedroomDepositMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez déposé " . $param["MONEYDEP"] . " PO dans la caisse de votre maison.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BedroomWithdrawMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = localize("Vous avez retiré " . $param["MONEY"] . " PO de la caisse de votre maison.");
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class HostelDrinkMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez bu un verre.") . "<br/>";
            $msg .= localize("Le verre a eu l'effet suivant : {effet} ", array(
                    "effet" => $param["TEXT"]
            )) . "<br/><br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class HostelChatMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $msg = localize("Vous aviez " . $param["LIMIT"] . "% de chance d'obtenir une mission. Votre jet a été de " . $param["JET"] . ".");
            if ($param["MISSION"])
                $msg .= localize(" La conversation a été fructueuse, vous avez reçu un parchemin.");
            else
                $msg .= localize(" La conversation s'épuise sans avoir apporté d'information intéressante.");
        } 

        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class HostelRoundMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $msg = localize("Vous avez payé une tournée pour " . $param["PRICE"] . " PO. Vous avez " . $param["LIMIT"] . " pourcent de chance d'obtenir une quête. Vous faites un jet de " . $param["JET"] . ". ") . "<br/>";
            if ($param["TAXE"] != 0)
                $msg .= " L'auberge a reversé " . $param["TAXE"] . " PO de taxe au gouverneur. ";
            if ($param["MISSION"])
                $msg .= localize("La conversation a été fructueuse, en théorie vous auriez reçu un parcho (missions/quêtes pas encore codées)");
            else
                $msg .= localize("La conversation s'épuise sans avoir apporté d'information intéressante.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class HostelNewsMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez posté une annonce avec succès pour " . $param["PRICE"] . " PO, elle restera affichée un mois.");
            if ($param["TAXE"] != 0)
                $msg .= " L'auberge a reversé " . $param["TAXE"] . " PO de taxe au gouverneur.";
        } elseif ($error == 1) {
            $msg = localize("Vous n'avez pas posté l'annonce, cela ne vous a rien coûté.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class HostelTradeMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez posté une offre commerciale avec succès pour " . $param["PRICE"] . " PO et " . HOSTEL_TRADE_AP . " PA.");
        } elseif ($error == 1) {
            $msg = localize("Vous n'avez pas posté l'offre commercial, cela ne vous a rien coûté.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class LearnSpellMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez appris le sortilège " . $param["SPELL_NAME"] . " pour " . $param["PRICE"] . " PO.");
            if ($param["TAXE"] != 0)
                $msg .= " L'échoppe a reversé " . $param["TAXE"] . " PO de taxe au gouverneur.";
        } elseif ($error == NEED_MORE_PROGRESS_ERROR) {
            $msg = localize("Vous n'avez pas encore assez progressé dans la maîtrise de vos sorts pour apprendre ce sortilège.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class LearnAbilityMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez appris la compétence " . $param["ABILITY_NAME"] . " pour " . $param["PRICE"] . " PO.");
            if ($param["TAXE"] != 0)
                $msg .= " L'échoppe a reversé " . $param["TAXE"] . " PO de taxe au gouverneur.";
        } elseif ($error == NEED_MORE_PROGRESS_ERROR) {
            $msg = localize("Vous n'avez pas encore assez progressé dans la maîtrise de vos compétences pour apprendre celle-ci.");
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class LearnTalentMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            if (isset($param["NEW"]) && $param["NEW"] == 0) {
                $msg = localize("Vous avez appris à confectionner une nouvelle pièce de l'artisanat : " . $param["TALENT_NAME"] . " pour " . $param["PRICE"] . " PO.") . "<br/>";
                $msg .= localize("Votre pourcentage de maitrise d'artisanat a chuté de 2%");
            } 

            else
                $msg = localize("Vous avez appris le talent " . $param["TALENT_NAME"] . " pour " . $param["PRICE"] . " PO.");
        } elseif ($error == NEED_MORE_PROGRESS_ERROR) {
            $msg = "Vous ne respectez les conditions pour apprendre un nouveau artisanat: soit vous connaissez déjà 10 patrons, soit vous n'avez pas assez progressé pour en apprendre un nouveau.<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BankDepositMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez déposé " . $param["FINALBANK"] . " PO sur votre compte. Cela vous a coûté " . ($param["MONEYDEP"] - $param["FINALBANK"]) . " PO. ");
            // if($param["FINALTAXE"] != 0)
            // $msg.="La banque a payé ".$param["FINALTAXE"]." PO au gouverneur.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BankWithdrawMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = localize("Vous avez retiré " . $param["MONEY"] . " PO de votre compte.");
        else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BankTransfertMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $name = "";
            if ($param["TYPE"] == 1)
                $name .= " à " . $param["TARGET_NAME"];
            elseif ($param["TYPE"] == 2)
                $name .= " au village " . $param["TARGET_NAME"];
            
            $msg = localize("Vous avez transféré " . $param["MONEY_TRANSFERED"] . " PO " . $name . ". Cela vous a coûté " . $param["FINAL_COST"] . " PO. ");
            // if($param["FINALTAXE"] != 0)
            // $msg.="La banque a payé ".$param["FINALTAXE"]." PO au gouverneur.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BasicBuyMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $msg = localize("Vous avez acheté un(e) " . $param["EQ_NAME"] . " de niveau 1 pour " . $param["EQ_PRICE"] . " PO.");
            // if($param["TAXE"] != 0)
            // $msg.=" L'échoppe a reversé ".$param["TAXE"]." PO de taxe au gouverneur.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class MainBuyMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $msg = localize("Vous avez acheté un(e) " . $param["EQ_NAME"] . " de niveau " . $param["EQ_LEVEL"] . " pour " . $param["EQ_PRICE"] . " PO.");
            // if($param["TAXE"] != 0)
            // $msg.=" L'échoppe a reversé ".$param["TAXE"]." PO de taxe au gouverneur.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class ShopSellMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "";
            for ($i = 0; $i < $param["NOMBRE"]; $i ++) {
                $msg .= "Vous avez vendu un(e) " . $param["EQ_NAME"][$i] . ". Vous avez gagné " . $param["EQ_PRICE"][$i] . " PO." . "<br/>";
            }
            $msg .= "Vous avez gagné un total de " . $param["TOTAL_PRICE"] . " PO." . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class ShopRepairMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "";
            for ($i = 0; $i < $param["NOMBRE"]; $i ++) {
                $msg .= "Vous avez réparé un(e) " . $param["EQ_NAME"][$i] . " pour " . $param["EQ_PRICE"][$i] . " PO." . "<br/>";
            }
            $msg .= "Vous avez payé un total de " . $param["TOTAL_PRICE"] . " PO." . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class OrderEquipMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $msg = localize("Vous avez commandé un(e) " . $param["EQ_NAME"] . " de niveau " . $param["EQ_LEVEL"] . ", cela vous a coûté " . $param["EQ_PRICE"] . " PO. La fabrication prendra " . $param["EQ_LEVEL"] . " jour(s). ");
            // if($param["TAXE"] != 0)
            // $msg.=" La guilde des Artisan a reversé ".$param["TAXE"]." PO de taxe au gouverneur.";
        } elseif ($error < 4) {
            switch ($error) {
                case GUILD_MAX_ERROR:
                    $msg = "Le nombre maximum de commandes simultanées a déjà été atteint. Attendez qu'une commande en cours se termine.";
                    break;
                
                case GUILD_WRONG_LEVEL_ERROR:
                    $msg = "Le niveau sélectionné est incorrect";
                    break;
                
                case GUILD_WRONG_TYPE_ERROR:
                    $msg = "Hacker de ... tu pensais pouvoir fabriquer des matières premières en douce ? ";
                    break;
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TakeEquipMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "Vous avez pris l'objet commandé(e) : un(e) " . $param["EQ_NAME"] . " de niveau " . $param["EQ_LEVEL"] . " .";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class OrderEnchantMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            
            $msg = localize("Vous avez commandé un enchantement " . $param["EN_NAME"] . " de niveau " . $param["EN_LEVEL"] . " sur votre " . $param["EQ_NAME"] . ", cela vous a coûté " . $param["EN_PRICE"] . " PO. La fabrication prendra " . (2 * $param["EN_LEVEL"]) . " jour(s). ");
            // if($param["TAXE"] != 0)
            // $msg.=" La guilde des Artisan a reversé ".$param["TAXE"]." PO de taxe au gouverneur.";
        } elseif ($error < 6) {
            switch ($error) {
                case GUILD_MAX_ERROR:
                    $msg = "Le nombre maximum de commandes simultanées a déjà été atteint. Attendez qu'une commande en cours se termine.";
                    break;
                
                case GUILD_WRONG_LEVEL_ERROR:
                    $msg = "Le niveau sélectionné est incorrect";
                    break;
                
                case GUILD_WRONG_TYPE_ERROR:
                    $msg = "Hacker de ... tu pensais pouvoir fabriquer des matières premières en douce ? ";
                    break;
                
                case GUILD_ALREADY_ENCHANT_ERROR:
                    $msg = "Cet objet est déjà enchanté";
                    break;
                
                case GUILD_INVALID_EQUIP_ERROR:
                    $msg = "Le niveau de l'objet à enchanter est trop faible.";
                    break;
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TakeEnchantMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "Vous avez pris votre objet " . $param["EQ_NAME"] . "  qui a reçu un enchantement <span class='template'>" . $param["EN_NAME"] . "</span>.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class ResetGaugeMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            $msg = "Vous avez remis à zéro la jauge de " . $param["CARACT"];
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class CaravanCreateMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error)
            $msg = "Vous avez organisé une mission commerciale de niveau " . $param["CARAVAN_LEVEL"] . ". Cela vous a coûté " . $param["CARAVAN_PRICE"] . " PO. Vous avez reçu un parchemin avec les détails de la mission. La tortue géante qui transporte les marchandises se trouve dans les écuries.";
        
        elseif ($error == CARAVAN_LOAD_TOO_SMALL) {
            $msg = "Votre chargement ne contient pas assez d'objets, il doit en contenir au moins " . NB_MIN_LOAD_CARAVAN;
        } elseif ($error == CARAVAN_INVALID_MERCHANT_LEVEL) {
            $msg = "Votre niveau de marchand n'est pas suffisant pour avoir le droit d'organiser cette caravane.";
        } elseif ($error == CARAVAN_INVALID_DESTINATION_TWICE_START_KINGDOM) {
            $msg = "Il est interdit de faire un trajet entre 2 villes dans le même royaume à partir du niveau 6.";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class CaravanTerminateMsg
{

    function msgBuilder ($error, $param)
    {
        if (! $error) {
            if ($param["MISSING_LOAD"])
                $msg = localize("Le chargement de la tortue n'est pas conforme à la mission. Vous n'obtenez donc aucune récompense.");
            elseif ($param["CARAVAN_TOO_LATE"])
                $msg = localize("Vous êtes en retard sur le délai de livraison. Vous récupérez uniquement votre mise de départ soit " . ($param["PRICE"] / 2) . "PO et un peu de la réputation perdue");
            else {
                $msg = localize("Vous avez accompli avec succès une mission commerciale de niveau " . $param["CARAVAN_LEVEL"] . ". La vente des marchandises vous a rapporté " . $param["PRICE"] . " PO .");
                if ($param["LEVEL_PROGRESS"])
                    $msg .= " Votre réputation de marchand progresse de 25% vers le niveau suivant.";
            }
        } elseif ($error < 6) {
            switch ($error) {
                case CARAVAN_NOT_REAL:
                    $msg = "Vous n'avez pas de caravane en cours.";
                    break;
                
                case CARAVAN_NO_TURTLE:
                    $msg = "Vous n'avez pas de tortue géante.";
                    break;
                
                case CARAVAN_WRONG_BUILDING:
                    $msg = "vous n'êtes pas dans le bon bâtiment.";
                    break;
                
                case CARAVAN_TOO_LATE:
                    $msg = "Vous êtes arrivé trop tard, vous avez échoué.";
                    break;
                
                case CARAVAN_MISSING_LOAD:
                    $msg = "Vous avez perdu une partie de votre chargement en cours de route, vous avez échoué.";
                    break;
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class BuildingConstructMsg
{

    function msgBuilder ($error, $param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                $msg = localize("Vous avez lancé la construction d un(e) {name} pour {prix} pièce d'or en ({x},{y}).", array(
                        "x" => $param["X"],
                        "y" => $param["Y"],
                        "name" => $param["NAME"],
                        "prix" => $param["PRICE"]
                )) . "$<br/><br/>";
                if ($param["CONSTRUCTION_DURATION_MIN"] == $param["CONSTRUCTION_DURATION_MAX"])
                    $msg .= localize("Le bâtiment sera entièrement construit dans {nb1} jours.", array(
                            "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                            "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                    )) . "$<br/><br/>";
                else
                    $msg .= localize("La construction durera entre {nb1} et {nb2} jours.", array(
                            "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                            "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                    )) . "$<br/><br/>";
                break;
            
            default:
                {
                    $obj = new ErrorMsg();
                    $msg = $obj->msgBuilder($error, $param);
                }
        }
        return $msg;
    }
}

class BuildingRepairMsg
{

    function msgBuilder ($error, $param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                $msg = localize("Vous avez lancé la réparation de {name} pour {prix} pièce d'or.", array(
                        "name" => $param["NAME"],
                        "prix" => $param["PRICE"]
                )) . "$<br/><br/>";
                if ($param["CONSTRUCTION_DURATION_MIN"] == $param["CONSTRUCTION_DURATION_MAX"])
                    $msg .= localize("Le bâtiment sera entièrement réparé dans {nb1} jours.", array(
                            "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                            "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                    )) . "$<br/><br/>";
                else
                    $msg .= localize("La réparation durera entre {nb1} et {nb2} jours.", array(
                            "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                            "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                    )) . "$<br/><br/>";
                break;
            
            default:
                {
                    $obj = new ErrorMsg();
                    $msg = $obj->msgBuilder($error, $param);
                }
        }
        return $msg;
    }
}

class BuildingDestroyMsg
{

    function msgBuilder ($error, $param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                $msg = localize("Vous avez lancé la destruction de {name} pour {prix} po.", array(
                        "name" => $param["NAME"],
                        "prix" => $param["PRICE"]
                )) . "$<br/><br/>";
                if ($param["CONSTRUCTION_DURATION_MIN"] == $param["CONSTRUCTION_DURATION_MAX"])
                    $msg .= localize("Le bâtiment sera entièrement détruit dans {nb1} jours.", array(
                            "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                            "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                    )) . "$<br/><br/>";
                else
                    $msg .= localize("Le bâtiment sera entièrement détruit dans {nb1} à {nb2} jours.", array(
                            "nb1" => $param["CONSTRUCTION_DURATION_MIN"],
                            "nb2" => $param["CONSTRUCTION_DURATION_MAX"]
                    )) . "$<br/><br/>";
                
                break;
            
            default:
                {
                    $obj = new ErrorMsg();
                    $msg = $obj->msgBuilder($error, $param);
                }
        }
        return $msg;
    }
}

class SetTaxMsg
{

    function msgBuilder ($error, $param)
    {
        switch ($error) {
            case ACTION_NO_ERROR:
                if ($param["DEFAULT"])
                    $msg = localize("Les taxes du bâtiment {name} ont été changé aux valeurs par défaut.", array(
                            "name" => $param["BUILDING_NAME"]
                    )) . "$<br/><br/>";
                else
                    $msg = localize("Les taxes du bâtiment {name} ont été changé avec succès.", array(
                            "name" => $param["BUILDING_NAME"]
                    )) . "$<br/><br/>";
                    // $msg.=localize("Le bâtiment sera entièrement détruit dans {nb1} à {nb2} jours.", array("nb1" => $param["CONSTRUCTION_DURATION_MIN"], "nb2" => $param["CONSTRUCTION_DURATION_MAX"]))."$<br/><br/>";
                break;
            
            default:
                {
                    $obj = new ErrorMsg();
                    $msg = $obj->msgBuilder($error, $param);
                }
        }
        return $msg;
    }
}

/* **************************************************************** TALENT, ABILITY & MAGICSPELL ******************************************************* */
class AbilityMsg
{

    function msgBuilder (&$param)
    {
        $str = "";
        if (isset($param["MALUS"]))
            $str = "(" . $param["MALUS"] . "% bonus)";
        else
            $param["MALUS"] = 0;
        $msg = localize("Vous avez {percent}% de chance de réussir cette compétence " . $str, array(
                "percent" => $param["PERCENT"]
        )) . " $<br/><br/>";
        $msg .= localize("Votre score est de ");
        $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["SCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
        
        if ($param["SCORE"] <= $param["PERCENT"]) {
            $msg .= localize("<b>Compétence réussie.</b>$$") . "<br/>";
        } else {
            $msg .= localize("<b>Vous avez raté votre compétence.</b>$$") . "<br/>";
        }
        if ($param["SCORE"] <= $param["PERCENT"] && ($param["PERCENT"] - $param["MALUS"]) <= 90) {
            
            if ($param["SCOREB"] > 0) {
                
                $msg .= localize("Votre jet d'amélioration est de {scoreb},", array(
                        "scoreb" => $param["SCOREB"]
                ));
                if ($param["ENHANCE"] > 0)
                    $msg .= " " . localize("vous améliorez votre compétence de <b>{enhance}</b>%.", array(
                            "enhance" => $param["ENHANCE"]
                    )) . "<br/>";
                else
                    $msg .= " " . localize("vous n'améliorez pas votre compétence.") . "<br/>";
            }
        }
        $msg .= "<hr/><br/>";
        
        return $msg;
    }
}

class TalentMsg
{

    function msgBuilder (&$param)
    {
        $str = "";
        if (isset($param["MALUS"]))
            $str = "(" . $param["MALUS"] . "% bonus)";
        else
            $param["MALUS"] = 0;
        $msg = localize("Vous avez {percent}% de chance de réussir ce savoir-faire " . $str, array(
                "percent" => $param["PERCENT"]
        )) . " $<br/><br/>";
        $msg .= localize("Votre score est de ");
        $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["SCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
        
        if ($param["SCORE"] <= $param["PERCENT"]) {
            $msg .= localize("<b>Savoir-faire réussi.</b>$$") . "<br/>";
        } else {
            $msg .= localize("<b>Vous avez raté votre savoir-faire.</b>$$") . "<br/>";
        }
        if ($param["SCORE"] <= $param["PERCENT"] && $param["PERCENT"] - $param["MALUS"] <= 90) {
            if ($param["SCOREB"] > 0) {
                $msg .= localize("Votre jet d'amélioration est de {scoreb},", array(
                        "scoreb" => $param["SCOREB"]
                ));
                if ($param["ENHANCE"] > 0)
                    $msg .= " " . localize("vous améliorez votre savoir-faire de <b>{enhance}</b>%.", array(
                            "enhance" => $param["ENHANCE"]
                    )) . "<br/>";
                else
                    $msg .= " " . localize("vous n'améliorez pas votre savoir-faire.") . "<br/>";
            }
        }
        $msg .= "<hr/><br/>";
        
        return $msg;
    }
}

class MagicSpellMsg
{

    function msgBuilder (&$param)
    {
        $str = "";
        if (isset($param["MALUS"]))
            $str = "(" . $param["MALUS"] . "% bonus)";
        else
            $param["MALUS"] = 0;
        
        $msg = localize("Vous avez {percent}% de chance de réussir ce sortilège " . $str, array(
                "percent" => $param["PERCENT"]
        )) . " $<br/><br/>";
        
        $msg .= localize("Votre score est de ");
        $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["SCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
        
        if ($param["SCORE"] <= $param["PERCENT"]) {
            $msg .= localize("<b>Sortilège réussi.</b>$$") . "<br/>";
        } else {
            $msg .= localize("<b>Vous avez raté votre sortilège.</b>$$") . "<br/>";
        }
        if ($param["SCORE"] <= $param["PERCENT"] && $param["PERCENT"] - $param["MALUS"] <= 90 && $param["NO_ENHANCE"] == 0) {
            if ($param["SCOREB"] > 0) {
                $msg .= localize("Votre jet d'amélioration est de {scoreb},", array(
                        "scoreb" => $param["SCOREB"]
                ));
                if ($param["ENHANCE"] > 0)
                    $msg .= " " . localize("vous améliorez votre sortilège de <b>{enhance}</b>%.", array(
                            "enhance" => $param["ENHANCE"]
                    )) . "<br/>";
                else
                    $msg .= " " . localize("vous n'améliorez pas votre sortilège.") . "<br/>";
            }
        }
        $msg .= "<hr/><br/>";
        return $msg;
    }
}

/* **************************************************************** LES COMPETENCES D'ATTAQUE de corps à corps **************************************************** */
class PassiveBarrierMsg
{

    function msgBuilder (&$param)
    {
        $msg = "";
        
        if (isset($param["BARRIER_LEVEL"])) {
            if (isset($param["PIEGE"]) && $param["PIEGE"]) {
                $msg = " <br/>";
                $msg .= localize("Vous êtes protegé par une Barrière enflammée de niveau {nb}", array(
                        "nb" => $param["BARRIER_LEVEL"]
                )) . "<br/>";
                $msg .= "Araignée Piègeuse Géante perd $.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_DAMAGE"] . " points de vie $<br/>";
                if ($param["PLAYER_KILLED"]) {
                    $msg .= "Araignée Piègeuse Géante a été tuée" . "<br/>";
                }
            } else {
                $msg = " <br/>";
                $msg .= localize("Votre adversaire est protégé par une Barrière enflammée de niveau {nb}", array(
                        "nb" => $param["BARRIER_LEVEL"]
                )) . "<br/>";
                $msg .= "Elle vous inflige $.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_DAMAGE"] . " points de vie $<br/>";
                if ($param["PLAYER_KILLED"]) {
                    $msg .= "Vous avez été tué(e)" . "<br/>";
                }
            }
            $msg .= " <br/>";
        }
        return $msg;
    }
}

class PassiveBubbleMsg
{

    function msgBuilder (&$param)
    {
        $msg = "";
        if (isset($param["BUBBLE_LIFE"])) {
            $msg .= "<br/>";
            if (! isset($param["PIEGE"]) or ! $param["PIEGE"]) {
                if (! $param["BUBBLE_CANCEL"]) {
                    $msg .= localize("Votre adversaire est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                            "damage" => $param["BUBBLE_LIFE"],
                            "level" => $param["BUBBLE_LEVEL"]
                    )) . "<br/>";
                } else {
                    $msg .= localize("Votre adversaire est protégé par une bulle de vie qui absorbe la totalité du coup.") . "<br/>";
                }
            } else {
                if (! $param["BUBBLE_CANCEL"]) {
                    $msg .= localize("Vous êtes protégé(e) par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                            "damage" => $param["BUBBLE_LIFE"],
                            "level" => $param["BUBBLE_LEVEL"]
                    )) . "<br/>";
                } else {
                    $msg .= localize("Vous êtes est protégé(e) par une bulle de vie qui absorbe la totalité du coup.") . "<br/>";
                }
            }
            $msg .= "<br/>";
        }
        
        return $msg;
    }
}

class AttackBuildingMsg
{

    function msgBuilder (&$error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous attaquez {name} niveau {level}", array(
                    "name" => $param["BUILDING_NAME"],
                    "level" => $param["BUILDING_LEVEL"]
            )) . "<br/>";
            
            if ($param["ATTACK"] == 1) {
                $msg .= localize("Votre Jet de dégât est de ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["BUILDING_DAMAGE"]) . "$<br/>";
            } else {
                $msg .= localize("Votre Jet de Maitrise de la magie est de : ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"]) . "$<br/>";
                $msg .= localize("Votre Jet de Dextérité est de : ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_DEX"]) . "$<br/>";
                $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
            }
            
            $msg .= localize("Vous infligez au bâtiment une perte de {nb} points de structure", array(
                    "nb" => $param["BUILDING_DAMAGE"]
            )) . "<br/>";
            if ($param["BUILDING_DEATH"])
                $msg .= localize("Vous avez <b> DETRUIT</b> le bâtiment") . "<br/>";
            
            $msg .= "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class AbilityAttackMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $obj = new AttackMsg();
                $msg .= $obj->msgBuilder($error, $param);
            } else
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class PiegeMsg
{

    function msgBuilder ($error, &$param)
    {
        $pluriel = "s";
        $msg = "Vous avez été piégé par Araignée piégeuse géante" . "<br/>";
        $msg .= "Votre attaque a été inefficace et Araignée piégeuse géante riposte" . "<br/><br/>";
        $msg .= localize("Araignée Piégeuse Géante vous attaque.") . "<br/>";
        
        $obj = new PassiveBarrierMsg();
        $msg .= $obj->msgBuilder($param);
        
        if (! $param["PLAYER_KILLED"]) {
            $msg .= localize("Son Jet d'Attaque est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
            $msg .= localize("Votre Jet de défense est de ");
            $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
            
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                $msg .= localize("Araignée Piégeuse vous a donc <b>TOUCHÉ</b> (Bonus de dégâts : {bonus})", array(
                        "bonus" => $param["BONUS_DAMAGE"]
                )) . "<br/>";
                $msg .= localize("Son Jet de Dégâts est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DAMAGE"] . " $<br/>";
                $msg .= localize("Elle vous inflige donc {damage} points de dégâts.", array(
                        "damage" => $param["TARGET_DAMAGETOT"]
                )) . "<br/>";
                if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                    $pluriel = "";
                
                $obj = new PassiveBubbleMsg();
                $msg .= $obj->msgBuilder($param);
                
                $msg .= localize("Votre armure vous protège et vous ne perdez que <b>{hp} point" . $pluriel . " de vie </b>", array(
                        "hp" => $param["TARGET_HP"]
                )) . "<br/>";
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= "<br/>";
                    $msg .= localize("Vous avez été <b>TUÉ</b>.");
                    $msg .= "<br/>";
                }
            } else {
                $msg .= localize("Araignée Piègeuse Géante ne parvient pas à vous toucher.") . "<br/><br/>";
            }
        }
        return $msg;
    }
}

class AttackMsg
{

    function msgBuilder ($error, &$param)
    {
        $pluriel = "s";
        
        if (! $error) {
            if ($param["PROTECTION"]) {
                $obj = new InterpositionMsg();
                $msg = $obj->msgBuilder($error, $param);
            } elseif ($param["PIEGE"]) {
                $obj = new PiegeMsg();
                $msg = $obj->msgBuilder($error, $param);
            } else {
                $msg = localize("Vous attaquez {target} ({targetId}) ", array(
                        "target" => localize($param["TARGET_NAME"]),
                        "targetId" => $param["TARGET_ID"]
                )) . "<br/><br/>";
                
                $obj = new PassiveBarrierMsg();
                $msg .= $obj->msgBuilder($param);
                
                if (! $param["PLAYER_KILLED"]) {
                    if ($param["IDENT"] == ABILITY_LIGHT)
                        $msg .= localize("Votre Jet de Maitrise de la magie est de : ");
                    else
                        $msg .= localize("Votre Jet d'Attaque est de ");
                    $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                    
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                        
                        $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire (Bonus de dégâts : {bonus})", array(
                                "bonus" => $param["BONUS_DAMAGE2"]
                        )) . "<br/>";
                        if ($param["IDENT"] == ABILITY_TREACHEROUS) {
                            $msg .= "<br/>" . localize("Votre Jet de vitesse est de ");
                            $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                            $msg .= localize("Bonus de la griffe : {percent}% ", array(
                                    "percent" => min(120, (50 + $param["PLAYER_SPEED"]))
                            )) . "<br/>";
                            $msg .= localize("Votre bonus de dégât est donc de {percent}", array(
                                    "percent" => $param["BONUS_DAMAGE"]
                            )) . "<br/><br/>";
                        }
                        if (isset($param["UNDEAD"]) && $param["UNDEAD"] == 1)
                            $msg .= localize("La compétence Appel de la Lumière vous confère 30% de bonus de dégât sur les morts-vivant.") . "<br>";
                        
                        $msg .= localize("Votre Jet de Dégâts est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DAMAGE"] . " $<br/>";
                        $msg .= localize("Vous lui infligez donc {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_DAMAGETOT"]
                        )) . "<br/>";
                        if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                            $pluriel = "";
                        
                        $obj = new PassiveBubbleMsg();
                        $msg .= $obj->msgBuilder($param);
                        
                        $knockout = "";
                        if ($param["IDENT"] == ABILITY_KNOCKOUT && $param["TARGET_KILLED"] == 0) {
                            if (isset($param["LEVEL_BENE"])) {
                                $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                        "level" => $param["LEVEL_BENE"]
                                )) . "<br/>";
                                if ($param["CANCEL"])
                                    $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                                else
                                    $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} points de blessure mortelle.", array(
                                            "level" => floor($param["TARGET_HP"] / 10) * 10,
                                            "niveau" => ($param["CURSE"] * 10)
                                    )) . "<br/>";
                            } else
                                $knockout = "dont " . ($param["CURSE"] * 10) . " PV de blessure mortelle.";
                        }
                        if ($param["IDENT"] == ABILITY_THRUST) {
                            $msg .= localize("Vous parvenez à trouver une faille dans l'armure de votre adversaire qui est reduit de 80%") . "<br/>";
                            $msg .= localize("Il perd <b>{hp} point" . $pluriel . " de vie </b>", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                        } else
                            $msg .= localize("Son armure le protège et il ne perd que <b>{hp} point" . $pluriel . " de vie </b>" . $knockout, array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                        
                        if ($param["TARGET_KILLED"] == 1) {
                            $msg .= "<br/>";
                            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                            $msg .= "<br/>";
                            $obj = new FallMsg();
                            $msg .= $obj->msgBuilder($param);
                        } else {
                            if ($param["IDENT"] == ABILITY_STUNNED) {
                                if (floor($param["TARGET_HP"] / 10) > 0) {
                                    if (isset($param["LEVEL_BENE"])) {
                                        $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                                "level" => $param["LEVEL_BENE"]
                                        )) . "<br/>";
                                        if ($param["CANCEL"])
                                            $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                                        else
                                            $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} dés de malus de vitesse.", array(
                                                    "level" => floor($param["TARGET_HP"] / 10),
                                                    "niveau" => ($param["CURSE"])
                                            )) . "<br/>";
                                    } else
                                        $msg .= localize("Vous avez étourdi votre adversaire. Il subit un malus de {vit}D6 de vitesse pour les 2 prochains tours.", array(
                                                "vit" => floor($param["TARGET_HP"] / 10)
                                        )) . "<br/>";
                                } else
                                    $msg .= localize("Vos dégâts sont trop faibles et vous ne parvenez pas à étourdir votre adversaire") . "<br/>";
                            }
                            if ($param["IDENT"] == ABILITY_THRUST) {
                                if (floor($param["TARGET_HP"] / 5) > 0) {
                                    if (isset($param["LEVEL_BENE"])) {
                                        $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                                "level" => $param["LEVEL_BENE"]
                                        )) . "<br/>";
                                        if ($param["CANCEL"])
                                            $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                                        else
                                            $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} point(s) de vie pour les 3 prochains tours.", array(
                                                    "level" => floor($param["TARGET_HP"] / 5),
                                                    "niveau" => (floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"])
                                            )) . "<br/>";
                                    } else
                                        $msg .= localize("Vous infligez à votre adversaire une blessure profonde de {hp} point(s) de vie pour les 3 prochains tours.", array(
                                                "hp" => floor($param["TARGET_HP"] / 5)
                                        )) . "<br/>";
                                } else
                                    $msg .= localize("Vos dégâts sont trop faibles et vous ne parvenez pas à infliger une blessure profonde à votre adversaire") . "<br/>";
                            }
                            $msg .= "<br/>";
                            
                            if ($param["IDENT"] == ABILITY_PROJECTION) {
                                if (! isset($param["MOVE1"])) {
                                    $msg .= localize("Vous n'avez pas eu assez de force pour projeter votre adversaire.") . "<br/>";
                                } else {
                                    if ($param["MOVE2"])
                                        $msg .= localize("Votre adversaire est projeté en arrière et recule de 2 cases") . "<br/>";
                                    if ($param["MOVE1"] && ! $param["MOVE2"])
                                        $msg .= localize("Votre adversaire est projeté en arrière mais ne recule que d'une case car il heurte un obstacle.") . "<br/>";
                                    if (! $param["MOVE1"])
                                        $msg .= localize("Votre adversaire est projeté en arrière mais heurte un obstacle et ne recule pas.") . "<br/>";
                                    if (isset($param["NAME_PJO"])) {
                                        $msg .= "<br/>L'obstacle est le personnage " . $param["NAME_PJO"] . "(" . $param["PJO_ID"] . ") qui perd <b>" . $param["PJO_EXTRA_DAMAGE"] . "</b> point(s) de vie <br/>";
                                        if (isset($param["TARGET_KILLED_OBS"])) {
                                            $msg .= "L'obstacle a été tué.<br/>";
                                            if ((isset($param["MONEY_FALL_OBS"]) && $param["MONEY_FALL_OBS"] != 0) || (isset($param["EQUIPMENT_FALL_OBS"]) && $param["EQUIPMENT_FALL_OBS"] != 0)) {
                                                $msg .= localize("Il a de plus laissé tomber : ");
                                                
                                                if (isset($param["MONEY_FALL_OBS"]) && $param["MONEY_FALL_OBS"] != 0) {
                                                    $msg .= localize("<b>{nb}</b> Pièce(s) d'Or.", array(
                                                            "nb" => $param["MONEY_FALL_OBS"]
                                                    ));
                                                    $msg .= "<br/>";
                                                }
                                                if (isset($param["EQUIPMENT_FALL_OBS"]) && $param["EQUIPMENT_FALL_OBS"] != 0) {
                                                    $msg .= localize("{name}.", array(
                                                            "name" => $param["DROP_NAME_OBS"]
                                                    ));
                                                    $msg .= "<br/>";
                                                }
                                            }
                                            
                                            if (isset($param["LARCIN_OBS"]) && $param["LARCIN_OBS"] > 0) {
                                                if ($param["LARCIN_OBS"] == 1)
                                                    $msg .= localize("Il a de plus laissé tomber <b>1</b> pièce d'Equipement car il était ciblé par un larcin");
                                                else
                                                    $msg .= localize("Aucun équipement n'est tombé malgré le larcin car il n'en transportait pas.");
                                                $msg .= "<br/>";
                                            }
                                        }
                                        
                                        $msg .= "Votre adversaire subit des dégâts supplémentaires et perd <b>" . $param["OPP_EXTRA_DAMAGE"] . "</b> point(s) de vie.<br/><br/>";
                                    }
                                    if (isset($param["NAME_BUILDING"])) {
                                        $msg .= "<br/>L'obstacle est le bâtiment " . $param["NAME_BUILDING"] . "(" . $param["BUILDING_ID"] . ") qui perd <b>" . $param["BUILDING_EXTRA_DAMAGE"] . "</b> point(s) de structure. <br/>";
                                        $msg .= "Votre adversaire a subit des dégâts supplémentaires et perd <b>" . $param["OPP_EXTRA_DAMAGE"] . "</b> point(s) de vie.<br/><br/>";
                                    }
                                }
                            }
                        }
                    } else {
                        $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                    }
                }
                
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $param["AP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* **************************************************************** LES COMPETENCES D'ARCHER **************************************************** */
class AbilityArcheryMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $obj = new ArcheryMsg();
                $msg .= $obj->msgBuilder($error, $param);
            } else
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ArcheryMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $pluriel = "s";
            if ($param["PROTECTION"]) {
                $obj = new InterpositionArcheryMsg();
                $msg = $obj->msgBuilder($error, $param);
            } else {
                $msg = localize("Vous attaquez {target} ({targetId}) (malus distance = {malus}%)", array(
                        "target" => localize($param["TARGET_NAME"]),
                        "targetId" => $param["TARGET_ID"],
                        "malus" => $param["MALUS_GAP"]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet d'Attaque est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
                $msg .= localize("Le Jet de Défense de votre adversaire est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                
                if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                    $msg .= localize("Votre bonus aux dégâts est de {bonus} point(s)", array(
                            "bonus" => $param["BONUS_DAMAGE"]
                    )) . "<br/>";
                    $msg .= localize("Vous lui infligez {damage} points de dégâts.", array(
                            "damage" => $param["TARGET_DAMAGE"]
                    )) . "<br/>";
                    
                    $obj = new PassiveBubbleMsg();
                    $msg .= $obj->msgBuilder($param);
                    if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                        $pluriel = "";
                    $msg .= localize("Son armure le protège et il ne perd que <b>{hp} point" . $pluriel . " de vie </b>", array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                    
                    // $msg.=localize("Vous lui avez infligé {damage} points de dégâts.",array("damage"=>$param["TARGET_HP"]))."<br/>";
                    if ($param["TARGET_KILLED"] == 1) {
                        $msg .= "<br/>";
                        $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                        $msg .= "<br/>";
                        $obj = new FallMsg();
                        $msg .= $obj->msgBuilder($param);
                    } else {
                        if ($param["IDENT"] == ABILITY_DISABLING) {
                            if ($param["TARGET_HP"] > 0)
                                $msg .= localize("Vous avez blessé votre adversaire à la jambe.<br/> Ses déplacements lui couteront 1PA supplémentaire pour le tour en cours et le suivant.") . "<br/>";
                            else
                                $msg .= "<br/>" . localize("Vos dégâts ne sont pas suffisants pour infliger un malus de déplacement à votre adversaire") . "<br/>";
                        }
                        
                        if ($param["IDENT"] == ABILITY_NEGATION) {
                            if ($param["TARGET_HP"] > 0) {
                                $msg .= "<br/>";
                                $msg .= localize("Votre Jet de maitrise de la magie est de ");
                                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["MAGICSKILL"] . " $<br/>";
                                if (isset($param["LEVEL_BENE"])) {
                                    $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                            "level" => $param["LEVEL_BENE"]
                                    )) . "<br/>";
                                    if ($param["CANCEL"])
                                        $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                                    else
                                        $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} dés de malus de magie.", array(
                                                "level" => $param["LEVEL_SPELL"],
                                                "niveau" => ($param["CURSE"])
                                        )) . "<br/>";
                                } else
                                    $msg .= localize("Vous infligez un malus de {nb} D6 de MM à votre adversaire pour le tour en cours et le suivant.", array(
                                            "nb" => $param["CURSE"]
                                    )) . "<br/>";
                            } else
                                $msg .= localize("Vos dégâts ne sont pas suffisants pour infliger un malus de magie à votre adversaire.", array(
                                        "nb" => $param["CURSE"]
                                )) . "<br/>";
                        }
                    }
                } else {
                    $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                }
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $param["AP"]
                )) . "<br/>";
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* **************************************************************** LES COMPETENCES SPECIALES **************************************************** */
class AbilitySpecialMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                switch ($param["IDENT"]) {
                    case ABILITY_LARCENY:
                        $obj = new AbilityLarcenyMsg();
                        break;
                    case ABILITY_DISARM:
                        $obj = new AbilityDisarmMsg();
                        break;
                    case ABILITY_STEAL:
                        $obj = new AbilityStealMsg();
                        break;
                    case ABILITY_PROTECTION:
                        $obj = new AbilityProtectionMsg();
                        break;
                    case ABILITY_FIRSTAID:
                        $obj = new AbilityFirstAidMsg();
                        break;
                }
                $msg .= $obj->msgBuilder($error, $param);
            } else
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class AbilityFirstAidMsg
{

    function msgBuilder ($error, &$param)
    {
        if ($param["SELF"])
            $msg = localize("Vous utilisez Premiers Soins sur vous-même.") . "<br/>";
        else
            $msg = localize("Vous utilisez Premiers Soins sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                    "target" => $param["TARGET_NAME"]
            )) . "<br/>";
        $msg .= localize("Votre Jet de Dexterité est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_DEXTERITY"] . " <br/>";
        $msg .= localize("Vous réalisez une maitrise de compétence de niveau {niv} ({nb}D6 pv).", array(
                "niv" => $param["LEVEL_COMP"],
                "nb" => $param["LEVEL_COMP"] / 2
        )) . "<br/>";
        $msg .= localize("Vous apportez {PV} points de vie à votre cible.", array(
                "PV" => $param["HP_POTENTIEL"]
        )) . "<br/>";
        $msg .= localize("{target}" . " (" . $param["TARGET_ID"] . ") est soigné de {hp} points de vie.", array(
                "target" => $param["TARGET_NAME"],
                "hp" => $param["HP_HEAL"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityDisarmMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Désarmer sur {name}" . " (" . $param["TARGET_ID"] . ")", array(
                "name" => $param["TARGET_NAME"]
        )) . "<br/>";
        $obj = new PassiveBarrierMsg();
        $msg .= $obj->msgBuilder($param);
        if (! $param["PLAYER_KILLED"]) {
            
            $msg .= localize("Votre Jet de compétence est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
            $msg .= localize("Le Jet de Défense de votre adversaire est de ");
            $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
            
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                if (isset($param["STUPID"]))
                    $msg .= localize("Félicitaion, vous auriez réussi à faire tomber l'arme de votre adversaire s'il en avait eu une ! :D.") . "<br/>";
                else
                    $msg .= localize("Vous réussissez à désarmer votre adversaire, son arme est tombée au sol.") . "<br/>";
            } else
                $msg .= localize("Vous ne parvenez pas à désarmer votre adversaire.") . "<br/>";
        }
        
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        if (isset($param["WEAPON_BROKEN"]))
            $msg .= localize("Votre arme s'est cassée !") . "<br/>";
        if (isset($param["MAIL"]))
            $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        return $msg;
    }
}

class AbilityFlamingMsg
{

    function msgBuilder (&$error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous tirez une flèche enflammée sur {name} niveau {level}", array(
                        "name" => $param["BUILDING_NAME"],
                        "level" => $param["BUILDING_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Votre Jet d'Attaque est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Vous infligez au bâtiment une perte de {nb} points de structure", array(
                        "nb" => $param["BUILDING_DAMAGE"]
                )) . "<br/>";
                if ($param["BUILDING_DEATH"])
                    $msg .= localize("Vous avez <b> DETRUIT</b> le bâtiment") . "<br/>";
                $msg .= "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $param["AP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class AbilityStealMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Vol à la Tire sur {name}" . " (" . $param["TARGET_ID"] . ")", array(
                "name" => $param["TARGET_NAME"]
        )) . "<br/>";
        $msg .= localize("Votre Jet de maitrise est de ");
        $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
        $msg .= localize("Le Jet de maitrise de votre adversaire est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
        
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
            $msg .= localize("Vous réussissez à voler votre adversaire. Vous lui dérobez {nb} pièces d'or.", array(
                    "nb" => $param["MONEY"]
            )) . "<br/>";
            if ($param["AUTH"] == 2)
                $msg .= localize("Vous n'avez pas été repéré par votre victime") . "<br/>";
            else
                $msg .= localize("Vous avez été repéré par votre victime") . "<br/>";
        } 

        else
            $msg .= localize("Vous ne parvenez pas à voler votre adversaire.") . "<br/>";
        
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/><br/>";
        
        if (isset($param["MAIL"]))
            $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        return $msg;
    }
}

class AbilityLarcenyMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Larçin sur {name}" . " (" . $param["TARGET_ID"] . ")", array(
                "name" => $param["TARGET_NAME"]
        )) . "<br/>";
        $msg .= localize("Votre Jet de maitrise est de ");
        $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
        $msg .= localize("Le Jet de défense de votre adversaire est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
        
        if (floor($param["PLAYER_ATTACK"]) >= $param["TARGET_DEFENSE"] + 1)
            $msg .= localize("Vous réussissez à toucher votre adversaire. S'il meurt dans le tour en cours, il perdra un objet de son inventaire.") . "<br/>";
        else
            $msg .= localize("Vous ne parvenez pas à cibler votre adversaire.") . "<br/>";
        
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityProtectionMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Protection sur {name}" . " (" . $param["TARGET_ID"] . ")", array(
                "name" => $param["TARGET_NAME"]
        )) . "<br/>";
        $msg .= localize("Tant que la compétence sera activée, vous protégerez {name}" . " (" . $param["TARGET_ID"] . ")", array(
                "name" => $param["TARGET_NAME"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

// Message quand on attaque quelq'un qui est défendu
class InterpositionMsg
{

    function msgBuilder ($error, &$param)
    {
        $pluriel = "s";
        $msg = localize("Vous attaquez {name}({nameId}) mais {opp}({oppId}) s'interpose.", array(
                "name" => $param["NAME"],
                "opp" => $param["TARGET_NAME"],
                "nameId" => localize($param["INTER_ID"]),
                "oppId" => localize($param["TARGET_ID"])
        )) . "<br/>";
        
        $obj = new PassiveBarrierMsg();
        $msg .= $obj->msgBuilder($param);
        
        if (! $param["PLAYER_KILLED"]) {
            $msg .= localize("Votre Jet d'Attaque est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]);
            $msg .= localize(" (Bonus de dégâts : {bonus})", array(
                    "bonus" => $param["BONUS_DAMAGE"]
            )) . "<br/>";
            if ($param["IDENT"] == ABILITY_TREACHEROUS) {
                $msg .= "<br/>" . localize("Votre Jet de vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Bonus de la griffe : {percent}% ", array(
                        "percent" => min(120, 50 + $param["PLAYER_SPEED"])
                )) . "<br/>";
                $msg .= localize("Vous bonus dégât sont donc de {percent}", array(
                        "percent" => $param["BONUS_DAMAGE"]
                )) . "<br/><br/>";
            }
            $msg .= localize("Votre Jet de Dégâts est de ");
            $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DAMAGE"] . " $<br/>";
            $msg .= localize("Vous lui infligez donc {damage} points de dégâts.", array(
                    "damage" => $param["TARGET_DAMAGETOT"]
            )) . "<br/>";
            
            $obj = new PassiveBubbleMsg();
            $msg .= $obj->msgBuilder($param);
            
            $msg .= "<br/>";
            $msg .= localize("Le Jet de maitrise de magie de votre adversaire est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"]) . "$<br/>";
            $msg .= localize("Son bonus d'armure est donc de {bonus}%", array(
                    "bonus" => $param["ARMOR_BONUS"]
            )) . "<br/>";
            
            if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                $pluriel = "";
            
            $knockout = "";
            if ($param["IDENT"] == ABILITY_KNOCKOUT && $param["TARGET_KILLED"] == 0) {
                if (isset($param["LEVEL_BENE"])) {
                    $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                            "level" => $param["LEVEL_BENE"]
                    )) . "<br/>";
                    if ($param["CANCEL"])
                        $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                    else
                        $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} points de blessure mortelle.", array(
                                "level" => floor($param["TARGET_HP"] / 10) * 10,
                                "niveau" => ($param["CURSE"] * 10)
                        )) . "<br/>";
                } else
                    $knockout = "dont " . (floor($param["HP"] / 10) * 10) . " PV de blessure mortelle.";
            }
            
            if ($param["IDENT"] == ABILITY_THRUST) {
                $msg .= localize("Vous parvenez à trouver une faille dans l'armure de votre adversaire qui est reduite de 80%") . "<br/>";
                $msg .= localize("Il perd <b>{hp} point" . $pluriel . " de vie </b>", array(
                        "hp" => $param["TARGET_HP"]
                )) . "<br/>";
            } else
                $msg .= localize("Son armure le protège et il ne perd que <b>{hp} point" . $pluriel . " de vie </b>" . $knockout, array(
                        "hp" => $param["TARGET_HP"]
                )) . "<br/>";
            
            if ($param["TARGET_KILLED"] == 1) {
                $msg .= "<br/>";
                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                $msg .= "<br/>";
                $obj = new FallMsg();
                $msg .= $obj->msgBuilder($param);
            } else {
                if ($param["IDENT"] == ABILITY_STUNNED) {
                    if (floor($param["TARGET_HP"] / 10) > 0) {
                        if (isset($param["LEVEL_BENE"])) {
                            $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                    "level" => $param["LEVEL_BENE"]
                            )) . "<br/>";
                            if ($param["CANCEL"])
                                $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                            else
                                $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} dés de malus de vitesse.", array(
                                        "level" => floor($param["TARGET_HP"] / 10),
                                        "niveau" => ($param["CURSE"])
                                )) . "<br/>";
                        } else
                            $msg .= localize("Vous avez étourdi votre adversaire. Il subira un malus de {vit}D6 de vitesse pour 2 DLA.", array(
                                    "vit" => floor($param["TARGET_HP"] / 10)
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vos dégâts sont trop faibles et vous ne parvenez pas à étourdir votre adversaire") . "<br/>";
                }
                if ($param["IDENT"] == ABILITY_THRUST) {
                    if (floor($param["TARGET_HP"] / 5) > 0) {
                        if (isset($param["LEVEL_BENE"])) {
                            $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                    "level" => $param["LEVEL_BENE"]
                            )) . "<br/>";
                            if ($param["CANCEL"])
                                $msg .= localize("Sa protection était plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                            else
                                $msg .= localize("Les effets de votre compétence ont été réduit de {level} à {niveau} point(s) de vie pour les 3 prochains tours.", array(
                                        "level" => floor($param["TARGET_HP"] / 5),
                                        "niveau" => (floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"])
                                )) . "<br/>";
                        } else
                            $msg .= localize("Vous avez infligez à votre adversaire ({id}) une blessure profonde de {hp} point(s) de vie pour les 3 prochains tours.", array(
                                    "hp" => floor($param["TARGET_HP"] / 5),
                                    "id" => $param["TARGET_ID"]
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vos dégâts ont été trop faibles et vous n'êtes pas parvenu pas à infliger une blessure profonde à votre adversaire") . "<br/>";
                }
            }
            
            $msg .= "<br/>";
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
            $obj = new GainXPmsg();
            $msg .= $obj->msgBuilder($param);
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        }
        
        return $msg;
    }
}

// Message quand on attaque quelq'un qui est défendu
class InterpositionArcheryMsg
{

    function msgBuilder ($error, &$param)
    {
        $pluriel = "s";
        $msg = localize("Vous attaquez {name}({nameId}) (malus distance = {malus}%) mais {opp}({oppId}) s'interpose.", array(
                "name" => $param["NAME"],
                "malus" => $param["MALUS_GAP"],
                "opp" => $param["TARGET_NAME"],
                "nameId" => localize($param["INTER_ID"]),
                "oppId" => localize($param["TARGET_ID"])
        )) . "<br/><br/>";
        $msg .= localize("Votre Jet d'Attaque est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
        $msg .= localize("Votre bonus aux dégâts est de {bonus} point(s)", array(
                "bonus" => $param["BONUS_DAMAGE"]
        )) . "<br/>";
        $msg .= localize("Vous lui infligez {damage} points de dégâts.", array(
                "damage" => $param["TARGET_DAMAGE"]
        )) . "<br/><br/>";
        
        $obj = new PassiveBubbleMsg();
        $msg .= $obj->msgBuilder($param);
        
        if ($param["TARGET_DAMAGETOT2"] > 0) {
            $msg .= localize("Le Jet de maitrise de magie de votre adversaire est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"]) . "$<br/>";
            $msg .= localize("Il bénéficie donc d'une armure de {bonus}", array(
                    "bonus" => $param["ARMOR_BONUS"]
            )) . "<br/>";
        }
        $msg .= localize("Il perd donc {damage} points de de vie.", array(
                "damage" => $param["TARGET_HP"]
        )) . "<br/>";
        if ($param["TARGET_KILLED"] == 1) {
            $msg .= "<br/>";
            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
            $msg .= "<br/>";
            $obj = new FallMsg();
            $msg .= $obj->msgBuilder($param);
        } else {
            if ($param["IDENT"] == ABILITY_DISABLING) {
                if ($param["TARGET_HP"] > 0)
                    $msg .= localize("Vous avez blessé votre adversaire à la jambe. Ses déplacements lui couteront 1PA supplémentaire pour la DLA en cours et la suivante.") . "<br/>";
                else
                    $msg .= localize("Vos dégâts ne sont pas suffisant pour infliger un malus de déplacement à votre adversaire") . "<br/>";
            }
            
            if ($param["IDENT"] == ABILITY_NEGATION) {
                if ($param["TARGET_HP"] > 0) {
                    $msg .= "<br/>";
                    $msg .= localize("Votre Jet de maitrise de la magie est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["MAGICSKILL"] . " $<br/>";
                    if (isset($param["LEVEL_BENE"])) {
                        $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                "level" => $param["LEVEL_BENE"]
                        )) . "<br/>";
                        if ($param["CANCEL"])
                            $msg .= localize("Sa protection est plus puissante que votre niveau de compétence et en annule les effets") . "<br/>";
                        else
                            $msg .= localize("Les effets de votre compétence sont réduit de {level} à {niveau} dés de malus de magie.", array(
                                    "level" => $param["LEVEL_SPELL"],
                                    "niveau" => ($param["CURSE"])
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vous infligez un malus de {nb} D6 de MM à votre adversaire pour la DLA en cours et la suivante.", array(
                                "nb" => $param["CURSE"]
                        )) . "<br/>";
                } else
                    $msg .= localize("Vos dégâts ne sont pas suffisant pour infliger un malus de magie à votre adversaire.", array(
                            "nb" => $param["CURSE"]
                    )) . "<br/>";
            }
        }
        
        $msg .= "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        $obj = new GainXPmsg();
        $msg .= $obj->msgBuilder($param);
        if (isset($param["WEAPON_BROKEN"]))
            $msg .= localize("Votre arme s'est cassée !") . "<br/>";
        if (isset($param["MAIL"]))
            $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        return $msg;
    }
}

class AbilityFlyArrowMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= "Vous utilisez Volée de Flèches" . " $<br/>";
                
                for ($i = 1; $i < $param["NB"]; $i ++) {
                    $msg .= " ------- FLECHE " . $i . "-----------" . "<br/>";
                    if (($i == 2 && $param["ARROW2"]) || ($i == 3 && $param["ARROW3"]))
                        $msg .= localize("Vous ne décochez pas cette flèche, car votre cible est déjà morte") . "<br/><br/>";
                    elseif ($param["PROTECTION"][$i]) {
                        $msg .= localize("Vous attaquez {name} ({nameId}) mais {target} {name}({targetId}) s'interpose (malus distance = {nb}%)", array(
                                "target" => localize($param["TARGET_NAME"][$i]),
                                "targetId" => localize($param["TARGET_ID_FA"][$i]),
                                "name" => localize($param["NAME"][$i]),
                                "nameId" => localize($param["INTER_ID"][$i]),
                                "nb" => $param["MALUS_GAP"][$i]
                        )) . "<br/><br/>";
                        $msg .= localize("Votre Jet d'Attaque est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"][$i] . " $<br/>";
                        $msg .= localize("Vous lui avez infligé donc {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_DAMAGETOT2"][$i]
                        )) . "<br/>";
                        if (isset($param["BUBBLE_LIFE2"][$i])) {
                            if (! $param["BUBBLE_CANCEL2"][$i])
                                $msg .= localize("Votre adversaire est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE2"][$i],
                                        "level" => $param["BUBBLE_LEVEL2"][$i]
                                )) . "<br/>";
                            else
                                $msg .= localize("Votre adversaire est protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["TARGET_DAMAGE"][$i]
                                )) . "<br/>";
                        }
                        if ($param["TARGET_DAMAGE"] != 0) {
                            $msg .= localize("Le Jet de maitrise de magie de votre adversaire est de ");
                            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"][$i]) . "$<br/>";
                            $msg .= localize("Il bénéficie donc d'une armure de {bonus}", array(
                                    "bonus" => $param["ARMOR_BONUS"][$i]
                            )) . "<br/>";
                            $msg .= localize("Votre adversaire perd {damage} points de vie.", array(
                                    "damage" => $param["TARGET_HP"][$i]
                            )) . "<br/>";
                        }
                    } else {
                        $msg .= localize("Vous attaquez {target} ({targetId}) (malus distance = {nb}%)", array(
                                "target" => localize($param["TARGET_NAME"][$i]),
                                "targetId" => $param["TARGET_ID_FA"][$i],
                                "nb" => $param["MALUS_GAP"][$i]
                        )) . "<br/><br/>";
                        $msg .= localize("Votre Jet d'Attaque est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"][$i] . " $<br/>";
                        $msg .= localize("Le Jet de Défense de votre adversaire est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"][$i] . " $<br/>";
                        
                        if ($param["PLAYER_ATTACK"][$i] >= $param["TARGET_DEFENSE"][$i] + 1) {
                            $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                            $msg .= localize("Votre bonus aux dégâts est de {bonus} point(s)", array(
                                    "bonus" => $param["BONUS_DAMAGE"][$i]
                            )) . "<br/>";
                            if (isset($param["BUBBLE_LIFE2"][$i])) {
                                if (! $param["BUBBLE_CANCEL2"][$i])
                                    $msg .= localize("Votre adversaire est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                            "damage" => $param["BUBBLE_LIFE2"][$i],
                                            "level" => $param["BUBBLE_LEVEL2"][$i]
                                    )) . "<br/>";
                                else
                                    $msg .= localize("Votre adversaire est protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                            "nb" => $param["TARGET_DAMAGE"][$i]
                                    )) . "<br/>";
                            }
                            $pluriel = "";
                            if ($param["TARGET_HP"][$i] > 1)
                                $pluriel = "s";
                            
                            $msg .= localize("Son armure le protège et il ne perd que <b>{hp} point" . $pluriel . " de vie </b>", array(
                                    "hp" => $param["TARGET_HP"][$i]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"][$i] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $msg .= "<br/>";
                                $param2 = array();
                                if (isset($param["EQUIPMENT_FALL"][$i]))
                                    $param2["EQUIPMENT_FALL"] = $param["EQUIPMENT_FALL"][$i];
                                if (isset($param["MONEY_FALL"][$i]))
                                    $param2["MONEY_FALL"] = $param["MONEY_FALL"][$i];
                                if (isset($param["DROP_NAME"][$i]))
                                    $param2["DROP_NAME"] = $param["DROP_NAME"][$i];
                                if (isset($param["LARCIN"][$i]))
                                    $param2["LARCIN"] = $param["LARCIN"][$i];
                                
                                $obj = new FallMsg();
                                $msg .= $obj->msgBuilder($param2);
                                unset($param2);
                            }
                            $msg .= "<br/>";
                        } else {
                            $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                        }
                    }
                }
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $param["AP"]
                )) . "<br/>";
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            } else
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* **************************************************************** LES COMPETENCES SANS OPPOSANT **************************************************** */
class AbilityWithoutTargetMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                switch ($param["IDENT"]) {
                    case ABILITY_GUARD:
                        $obj = new AbilityGuardMsg();
                        break;
                    case ABILITY_RUN:
                        $obj = new AbilityRunMsg();
                        break;
                    case ABILITY_AUTOREGEN:
                        $obj = new AbilityAutoRegenMsg();
                        break;
                    case ABILITY_BRAVERY:
                        $obj = new AbilityBraveryMsg();
                        break;
                    case ABILITY_RESISTANCE:
                        $obj = new AbilityResistanceMsg();
                        break;
                    case ABILITY_EXORCISM:
                        $obj = new AbilityExorcismMsg();
                        break;
                    case ABILITY_AMBUSH:
                        $obj = new AbilityAmbushMsg();
                        break;
                }
                $msg .= $obj->msgBuilder($error, $param);
            } else
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class AbilityGuardMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Garde") . "<br/>";
        $msg .= localize("Votre Jet de Défense sans bonus est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_DEFENSE"] . " <br/>";
        $msg .= localize("Votre défense est donc augmentée de {def}D6 pour ce tour et le suivant", array(
                "def" => $param["GAIN_DEFENSE"]
        )) . "<br/><br/>";
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityAmbushMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Embuscade.") . "<br/>";
        if (isset($param["AMBUSH_INBUILDING"]) && $param["AMBUSH_INBUILDING"] == 1) {
            $msg .= localize("Vous êtes devenus invisible pour tous ceux qui n'était pas dans la même salle que vous ou moins au moment de l'utilisation de la compétence.") . "<br/>";
        } else {
            $msg .= localize("Vous êtes devenus invisible pour tous ceux qui n'était pas à 5 cases de vous ou moins au moment de l'utilisation de la compétence.") . "<br/>";
            $msg .= localize("N'oubliez pas ceux qui se trouvaient en salle de garde, dans le clocher ou dans l'observatoire des bâtiments alentours.") . "<br/>";
            $msg .= localize("Ceux qui étaient dans la salle d'accueil des bâtiments à 2 cases de vous ou moins, vous ont vu aussi.") . "<br/>";
        }
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityAutoRegenMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Auto-Régénération") . "<br/>";
        $msg .= localize("Votre Jet de Force est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " <br/>";
        $msg .= localize("Vous vous régénérez donc de {nb} points de vie", array(
                "nb" => $param["HP_HEAL"]
        )) . "<br/>";
        $msg .= localize("Vous subissez un malus de {nb} dés de force pour le tour en cours et les 2 prochains", array(
                "nb" => $param["MALUS_STRENGTH"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityRunMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Course Celeste") . "<br/>";
        $msg .= localize("Tous vos déplacement ne vous couteront qu'un seul PA quel que soit le type de terrain pour cette DLA et la suivante.") . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityBraveryMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous activez Aura de courage") . "<br/>";
        $msg .= localize("L'ensemble de votre groupe de chasse reçoit un bonus de {nb}% en d'attaque", array(
                "nb" => $param["BONUS_ATTACK"]
        )) . "<br/>";
        $msg .= localize("Tant que l'Aura sera activée vous subirez une perte de {nb} points de vie à chaque nouveau tour", array(
                "nb" => $param["MALUS_LIFE"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityResistanceMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous activez Aura de resistance") . "<br/>";
        $msg .= localize("Votre Jet de Maitrise de la magie est de ");
        $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"]) . "$<br/>";
        $msg .= localize("L'ensemble de votre groupe de chasse reçoit un bonus de {nb}% en armure", array(
                "nb" => $param["BONUS_ARMOR"]
        )) . "<br/>";
        $msg .= localize("Tant que l'Aura sera activée vous subirez un malus de 20% de force") . "<br/>";
        $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class AbilityExorcismMsg
{

    function msgBuilder ($error, &$param)
    {
        $msg = localize("Vous utilisez Exorcisme de l'ombre.") . "<br/>";
        $msg .= localize("Vous libérez l'énergie noire accumulée au cours des trois derniers tours") . "<br/><br/>";
        if ($param["NUMBER_OPPONENT"] == 0)
            $msg .= localize("Aucune de vos cibles n'est à portée.");
        else {
            $msg .= localize("Votre Jet de Maitrise de la magie est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"]) . "$<br/><br/>";
            $msg .= localize($param["PLAYER_MM"] . "% de l'énergie noire accumulée sera transformée en dégât.") . "$<br/>";
            
            for ($i = 1; $i < $param["NUMBER_OPPONENT"] + 1; $i ++) {
                if ($param["FAR"][$i])
                    $msg .= localize("La cible {name}({id}) n'est pas à portée ou est déjà morte.", array(
                            "name" => $param["TARGET_NAME"][$i],
                            "id" => $param["TARGET_VALUE_ID"][$i]
                    )) . "<br/><br/>";
                else {
                    $msg .= localize("{name}({id}) est ciblé par l'exorcisme de l'ombre.", array(
                            "name" => $param["TARGET_NAME"][$i],
                            "id" => $param["TARGET_VALUE_ID"][$i]
                    )) . "<br/>";
                    $msg .= localize("Il reçoit donc {dmg} points de dégât.", array(
                            "dmg" => $param["TARGET_DAMAGE"][$i]
                    )) . "<br/>";
                    
                    if ($param["BUBBLE"][$i]) {
                        if (! $param["BUBBLE_CANCEL2"][$i])
                            $msg .= localize("Il est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                    "damage" => $param["BUBBLE_LIFE2"][$i],
                                    "level" => $param["BUBBLE_LEVEL2"][$i]
                            )) . "<br/>";
                        else
                            $msg .= localize("Il est protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                    "nb" => $param["PLAYER_DAMAGE"][$i]
                            )) . "<br/>";
                    }
                    $msg .= localize("Il perd {nb} points de vie.", array(
                            "nb" => $param["TARGET_HP"][$i]
                    )) . "<br/>";
                    if ($param["TARGET_KILLED"][$i] == 1) {
                        $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.") . "<br/><br/>";
                        $param2 = array();
                        if (isset($param["EQUIPMENT_FALL"][$i]))
                            $param2["EQUIPMENT_FALL"] = $param["EQUIPMENT_FALL"][$i];
                        if (isset($param["MONEY_FALL"][$i]))
                            $param2["MONEY_FALL"] = $param["MONEY_FALL"][$i];
                        if (isset($param["DROP_NAME"][$i]))
                            $param2["DROP_NAME"] = $param["DROP_NAME"][$i];
                        if (isset($param["LARCIN"][$i]))
                            $param2["LARCIN"] = $param["LARCIN"][$i];
                        $obj = new FallMsg();
                        $msg .= $obj->msgBuilder($param2);
                        unset($param2);
                    }
                    $msg .= "<br/>";
                }
            }
        }
        $obj = new GainXPmsg();
        $msg .= $obj->msgBuilder($param) . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }
}

class SharpenMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous aiguisez votre " . $param["OBJECT_NAME"]) . "<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"]) . "$<br/>";
                $msg .= localize("Les dégâts de l'arme sont augmentées de " . ceil($param["PLAYER_SPEED"] / 5) . " point(s).") . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class TwirlMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Tournoiement") . "<br/><br/>";
                $msg .= localize("Votre Jet d'Attaque est de ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
                $msg .= localize("Votre Jet de dégâts est de ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DAMAGE_T"]) . "<br/><br/>";
                
                for ($i = 1; $i < $param["NUMBER_OPPONENT"] + 1; $i ++) {
                    
                    if (isset($param["BARRIER_LEVEL"][$i])) {
                        $param2 = array();
                        $param2["BARRIER_LEVEL"] = $param["BARRIER_LEVEL"][$i];
                        if (isset($param2["PIEGE"]))
                            $param2["PIEGE"] = $param["PIEGE"][$i];
                        $param2["PLAYER_DAMAGE"] = $param["PLAYER_DAMAGE"][$i];
                        $param2["PLAYER_KILLED"] = $param["PLAYER_KILLED"][$i];
                        $obj = new PassiveBarrierMsg();
                        $msg .= $obj->msgBuilder($param2);
                        unset($param2);
                    }
                    
                    if (! $param["PLAYER_KILLED"][$i]) {
                        
                        if ($param["PROTECTION"][$i]) {
                            $msg .= localize("{name}" . " (" . $param["INTER_ID"][$i] . ") s'est interposé pour défendre {target}" . " (" . $param["TARGET_ID_TWIRL"][$i] . ")", array(
                                    "name" => $param["TARGET_NAME"][$i],
                                    "target" => $param["INTER_NAME"][$i]
                            )) . "<br/>";
                            if (isset($param["BUBBLE_LIFE2"][$i])) {
                                if (! $param["BUBBLE_CANCEL2"][$i]) {
                                    $msg .= localize("Votre adversaire est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                            "damage" => $param["BUBBLE_LIFE2"][$i],
                                            "level" => $param["BUBBLE_LEVEL2"][$i]
                                    )) . "<br/>";
                                    $msg .= localize("Son jet de maitrise de magie est de ");
                                    $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"][$i]) . "$<br/>";
                                    $msg .= localize("Son bonus d'armure est donc de {bonus}%", array(
                                            "bonus" => $param["ARMOR_BONUS"][$i]
                                    )) . "<br/>";
                                } else
                                    $msg .= localize("Votre adversaire est protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                            "nb" => $param["TARGET_DAMAGE_T"]
                                    )) . "<br/>";
                            } else {
                                $msg .= localize("Son jet de maitrise de magie est de ");
                                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"][$i]) . "$<br/>";
                                $msg .= localize("Son bonus d'armure est donc de {bonus}%", array(
                                        "bonus" => $param["ARMOR_BONUS"][$i]
                                )) . "<br/>";
                            }
                            $msg .= localize("Il perd <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"][$i]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"][$i] == 1) {
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $param2 = array();
                                if (isset($param["EQUIPMENT_FALL"][$i]))
                                    $param2["EQUIPMENT_FALL"] = $param["EQUIPMENT_FALL"][$i];
                                if (isset($param["MONEY_FALL"][$i]))
                                    $param2["MONEY_FALL"] = $param["MONEY_FALL"][$i];
                                if (isset($param["DROP_NAME"][$i]))
                                    $param2["DROP_NAME"] = $param["DROP_NAME"][$i];
                                if (isset($param["LARCIN"][$i]))
                                    $param2["LARCIN"] = $param["LARCIN"][$i];
                                
                                $obj = new FallMsg();
                                $msg .= $obj->msgBuilder($param2);
                                unset($param2);
                            }
                            $msg .= "<br/><br/>";
                        } else {
                            $msg .= localize("Le Jet de défense de {name}" . " (" . $param["TARGET_ID_TWIRL"][$i] . ") est de ", array(
                                    "name" => $param["TARGET_NAME"][$i]
                            ));
                            $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"][$i] . " $<br/>";
                            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$i] + 1) {
                                if (isset($param["BUBBLE_LIFE2"][$i])) {
                                    if (! $param["BUBBLE_CANCEL2"][$i])
                                        $msg .= localize("Votre adversaire est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                                "damage" => $param["BUBBLE_LIFE2"][$i],
                                                "level" => $param["BUBBLE_LEVEL2"][$i]
                                        )) . "<br/>";
                                    else
                                        $msg .= localize("Votre adversaire est protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                                "nb" => $param["TARGET_DAMAGE_T"]
                                        )) . "<br/>";
                                }
                                $msg .= localize("Vous l'avez touché. Il perd <b>{hp} points de vie</b>.", array(
                                        "hp" => $param["TARGET_HP"][$i]
                                )) . "<br/>";
                                if ($param["TARGET_KILLED"][$i] == 1) {
                                    $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.") . "<br/>";
                                    $param2 = array();
                                    if (isset($param["EQUIPMENT_FALL"][$i]))
                                        $param2["EQUIPMENT_FALL"] = $param["EQUIPMENT_FALL"][$i];
                                    if (isset($param["MONEY_FALL"][$i]))
                                        $param2["MONEY_FALL"] = $param["MONEY_FALL"][$i];
                                    if (isset($param["DROP_NAME"][$i]))
                                        $param2["DROP_NAME"] = $param["DROP_NAME"][$i];
                                    if (isset($param["LARCIN"][$i]))
                                        $param2["LARCIN"] = $param["LARCIN"][$i];
                                    
                                    $obj = new FallMsg();
                                    $msg .= $obj->msgBuilder($param2);
                                    unset($param2);
                                }
                            } else {
                                $msg .= localize("{name}" . " (" . $param["TARGET_ID_TWIRL"][$i] . ") a esquivé votre attaque", array(
                                        "name" => $param["TARGET_NAME"][$i]
                                )) . "<br/>";
                            }
                            $msg .= "<br/><br/>";
                        }
                    }
                }
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
            
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class BolasMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new AbilityMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous lancez votre bolas niveau {niv} sur {name}" . " (" . $param["TARGET_ID"] . ")", array(
                        "niv" => $param["BOLAS_LEVEL"],
                        "name" => $param["TARGET_NAME"]
                )) . "<br/>";
                
                if (isset($param["NB_BOLAS"]) && $param["NB_BOLAS"] >= 2) {
                    $msg .= localize("Vous n'êtes pas parvenu à toucher votre adversaire car il est ficelé par deux bolas.") . "<br/>";
                } else {
                    $msg .= localize("Votre Jet d'Attaque est de ");
                    $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
                    $msg .= localize("Le Jet de Défense de votre adversaire est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                        $msg .= localize("Vous avez touché votre adversaire, il est dorénavant entravé par le bolas.") . "<br/>";
                    } else {
                        $msg .= localize("Vous ne parvenez pas à touché votre adversaire.") . "<br/>";
                    }
                }
                
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            }
            
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* *************************************************************** LES SORTILEGES DE BASES **************************************************** */
class SpellMsg
{

    function msgBuilder ($error, $ident, &$paramability, &$param)
    {
        switch ($ident) {
            case SPELL_ARMOR:
                $obj = new ArmorMsg();
                break;
            case SPELL_TEARS:
                $obj = new TearsMsg();
                break;
            case SPELL_BURNING:
                $obj = new BurningMsg();
                break;
            case SPELL_REGEN:
                $obj = new RegenMsg();
                break;
            case SPELL_CURE:
                $obj = new CureMsg();
                break;
            case SPELL_SPRING:
                $obj = new SpringMsg();
                break;
            case SPELL_WALL:
                $obj = new WallMsg();
                break;
            case SPELL_FIREBALL:
                $obj = new FireBallMsg();
                break;
            case SPELL_DEMONPUNCH:
                $obj = new DemonpunchMsg();
                break;
            case SPELL_FIRE:
                $obj = new FireMsg();
                break;
            case SPELL_BLOOD:
                $obj = new BloodMsg();
                break;
            case SPELL_SHIELD:
                $obj = new ShieldMsg();
                break;
            case SPELL_NEGATION:
                $obj = new NegationMsg();
                break;
            case SPELL_RAIN:
                $obj = new RainMsg();
                break;
            case SPELL_RELOAD:
                $obj = new ReloadMsg();
                break;
            case SPELL_SUN:
                $obj = new SunMsg();
                break;
            case SPELL_BLESS:
                $obj = new BlessMsg();
                break;
            case SPELL_BARRIER:
                $obj = new BarrierMsg();
                break;
            case SPELL_BUBBLE:
                $obj = new BubbleMsg();
                break;
            case SPELL_PILLARS:
                $obj = new PillarsMsg();
                break;
            case SPELL_ANGER:
                $obj = new AngerMsg();
                break;
            case SPELL_CURSE:
                $obj = new CurseMsg();
                break;
            case SPELL_TELEPORT:
                $obj = new SpellTeleportMsg();
                break;
            case S_MASSIVEINSTANTTP:
                $obj = new SpellMassiveTeleportMsg();
                break;
            case SPELL_LIGHTTOUCH:
                $obj = new LightTouchMsg();
                break;
            case SPELL_RECALL:
                $obj = new SpellRecallMsg();
                break;
            case SPELL_SOUL:
                $obj = new SpellSoulMsg();
                break;
            case M_INSTANTBLOOD:
                $obj = new SpellInstantBloodMsg();
                break;
            default:
                break;
        }
        $msg = $obj->msgBuilder($error, $paramability, $param);
        return $msg;
    }
}

class interBurningMsg
{

    function msgBuilder (&$param)
    {
        $msg = localize("Vous attaquez {name}({nameId}) mais {opp}({oppId}) s'interpose.", array(
                "name" => $param["NAME"],
                "opp" => $param["TARGET_NAME"],
                "nameId" => localize($param["INTER_ID"]),
                "oppId" => localize($param["TARGET_ID"])
        )) . "<br/>";
        $obj = new PassiveBarrierMsg();
        $msg .= $obj->msgBuilder($param);
        
        if (! $param["PLAYER_KILLED"]) {
            $msg .= localize("Votre Jet d'attaque magique est de ");
            $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
            
            // $msg.=localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire")."<br/>";
            $msg .= localize("Votre jet de MM est de : {mm} ", array(
                    "mm" => $param["PLAYER_MM"]
            )) . "<br/>";
            $msg .= localize("Vous réalisez un sort de niveau {level} ({nb}D6 de dégâts) ", array(
                    "level" => $param["SPELL_LEVEL"],
                    "nb" => $param["SPELL_LEVEL"] * 1.7
            )) . "<br/>";
            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                    "damage" => $param["TARGET_DAMAGE"]
            )) . "<br/>";
            
            $obj = new PassiveBubbleMsg();
            $msg .= $obj->msgBuilder($param);
            
            $msg .= localize("Le Jet de maitrise de magie de votre adversaire est de ");
            $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"]) . "$<br/>";
            $msg .= localize("Son bonus d'armure est donc de {bonus}%", array(
                    "bonus" => $param["ARMOR_BONUS"]
            )) . "<br/>";
            
            $msg .= localize("Son armure le protège et il ne perdra que <b>{hp} point(s) de vie</b>.", array(
                    "hp" => $param["TARGET_HP"]
            )) . "<br/>";
            if ($param["TARGET_KILLED"] == 1) {
                $msg .= "<br/>";
                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                $msg .= "<br/>";
                $obj = new FallMsg();
                $msg .= $obj->msgBuilder($param);
            }
            $msg .= "<br/>";
        }
        
        $obj = new GainXPmsg();
        $msg .= $obj->msgBuilder($param);
        if (isset($param["WEAPON_BROKEN"]))
            $msg .= localize("Votre arme s'est cassée !") . "<br/>";
        if (isset($param["MAIL"]))
            $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        
        return $msg;
    }
}

class BurningMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                if ($param["PROTECTION"] == 1) {
                    $obj = new interBurningMsg();
                    $msg .= $obj->msgBuilder($param);
                } else {
                    $msg .= localize("Vous attaquez {target} ({targetId}) ", array(
                            "target" => localize($param["TARGET_NAME"]),
                            "targetId" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                    $obj = new PassiveBarrierMsg();
                    $msg .= $obj->msgBuilder($param);
                    
                    if (! $param["PLAYER_KILLED"]) {
                        $msg .= localize("Votre Jet d'attaque magique est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
                        $msg .= localize("Le Jet de défense de votre adversaire est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                        
                        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                            $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                            $msg .= localize("Votre jet de MM est de : {mm} ", array(
                                    "mm" => $param["PLAYER_MM"]
                            )) . "<br/>";
                            $msg .= localize("Vous réalisez un sort de niveau {level} ({nb}D6 de dégâts) ", array(
                                    "level" => $param["SPELL_LEVEL"],
                                    "nb" => $param["SPELL_LEVEL"] * 1.7
                            )) . "<br/>";
                            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGE"]
                            )) . "<br/>";
                            
                            $obj = new PassiveBubbleMsg();
                            $msg .= $obj->msgBuilder($param);
                            
                            $msg .= localize("Son armure le protège et il ne perdra que <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $msg .= "<br/>";
                                $obj = new FallMsg();
                                $msg .= $obj->msgBuilder($param);
                            }
                            $msg .= "<br/>";
                        } 

                        else {
                            $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                        }
                        
                        $obj = new GainXPmsg();
                        $msg .= $obj->msgBuilder($param);
                    }
                }
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class TearsMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Larmes de Vie sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Larmes de Vie sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b> ({nb}D6 Points de Vie)", array(
                        "nb" => $param["SPELL_LEVEL"],
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                // $msg.=localize("Nombre de point potentiel de guérison : {PV}",array("PV"=>$param["HP_POTENTIEL"]))."<br/>";
                if ($param["SELF"])
                    $msg .= localize("vous regagnez {hp} des {pv} points de vie que le sortilège aurait pu vous rendre", array(
                            "pv" => $param["HP_POTENTIEL"],
                            "target" => $param["TARGET_NAME"],
                            "hp" => $param["HP_HEAL"]
                    )) . "<br/>";
                else
                    $msg .= localize("{target}" . " (" . $param["TARGET_ID"] . ") est soigné de {hp} des {pv} points de vie que le sortilège aurait pu rendre ", array(
                            "pv" => $param["HP_POTENTIEL"],
                            "target" => $param["TARGET_NAME"],
                            "hp" => $param["HP_HEAL"]
                    )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        }
        return $msg;
    }
}

class ArmorMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Armure d'Athlan sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Armure d'Athlan sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Force est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " $<br/>";
                // $msg.=localize("Vous façonnez une armure magique conférant <b>{armure}</b> points d'amure",array("armure"=>2+$param["PLAYER_STRENGTH"]/10)));
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "nb" => $param["SPELL_LEVEL"],
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                if ($param["SELF"])
                    $msg .= localize("Votre armure est augmentée de {armor} points pour ce tour et le suivant. ", array(
                            "target" => $param["TARGET_NAME"],
                            "armor" => $param["TARGET_ARMOR"]
                    )) . "<br/>";
                else
                    $msg .= localize("L'armure de {target}" . " (" . $param["TARGET_ID"] . ") est augmentée de {armor} points pour ce tour  et le suivant.", array(
                            "target" => $param["TARGET_NAME"],
                            "armor" => $param["TARGET_ARMOR"]
                    )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class KineMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Force d'Aether sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                        "target" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/><br/>";
                $msg .= localize("Vous réalisez un sort de niveau " . $param["SPELL_LEVEL"]) . "<br/><br/>";
                
                $msg .= localize("Le résistance de votre adversaire est de niveau : ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"] . " $<br/><br/>";
                if ($param["SPELL_LEVEL"] > $param["TARGET_MM"]) {
                    if ($param["MOVE"])
                        $msg .= localize("Vous surpassez la résistance magique de votre adversaire et vous parvenez à le déplacer en X={x} Y={y}", array(
                                "x" => $param["X"],
                                "y" => $param["Y"]
                        ));
                    else
                        $msg .= localize("Vous n'êtes pas  parvenu à déplacer votre adversaire car la case est occupée.", array(
                                "x" => $param["X"],
                                "y" => $param["Y"]
                        ));
                } else
                    $msg .= localize("Votre adversaire a contré votre sortilège et vous n'êtes pas parvenu à le déplacer");
                
                $msg .= "$$<br/><br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* *************************************************************** LES SORTILEGES DE L'ECOLE DE GUERISON **************************************************** */
class RegenMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Charme de Vitalité sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Charme de Vitalité sur {target} ({targetId}).", array(
                            "target" => $param["TARGET_NAME"],
                            "targetId" => $param["TARGET_ID"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                if ($param["SELF"])
                    $msg .= localize("Votre gagnez {nb} points de régénération pour les trois prochains tours. ", array(
                            "nb" => $param["TARGET_REGEN"]
                    )) . "<br/>";
                else
                    $msg .= localize("{target}" . " (" . $param["TARGET_ID"] . ") gagne {nb} points de régénération pour les trois prochains tours.", array(
                            "target" => $param["TARGET_NAME"],
                            "nb" => $param["TARGET_REGEN"]
                    )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class CureMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Souffle d'Athlan sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Souffle d'Athlan sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                if ($param["SELF"])
                    $msg .= localize("Tous vos maux ont été réduit de {nb} niveau(x). ", array(
                            "nb" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                else
                    $msg .= localize("Tous les maux de {target}" . " (" . $param["TARGET_ID"] . ") ont été réduit de {nb} niveau(x).", array(
                            "target" => $param["TARGET_NAME"],
                            "nb" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        }
        return $msg;
    }
}

class SpringMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Bassin Divin en (x={xb},y={yb}).", array(
                        "xb" => $param["XB"],
                        "yb" => $param["YB"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Vous avez créé un Bassin Divin de niveau {niv}", array(
                        "niv" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class DrinkSpringMsg
{

    function msgBuilder ($error, &$param)
    {
        if (! $error) {
            $msg = localize("Vous buvez l'eau d'un Bassin Divin de niveau {niv}.", array(
                    "niv" => $param["SPRING_LEVEL"]
            )) . "<br/>";
            $msg .= localize("Vous récupérez {hp} points de vie", array(
                    "hp" => $param["HP_HEAL"]
            )) . "<br/><br/>";
            
            // $msg.=localize("Cette action vous a coûté {ap} PA",array("ap"=>$param["AP"]))."<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class RainMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez pluie sacrée en {x}-{y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/><br/>";
                $msg .= localize("Votre jet de MM est de : {mm} ", array(
                        "mm" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Vous réalisez un sort de niveau {level} ", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Nombre de points potentiels de guérison : {PV}", array(
                        "PV" => $param["HP_POTENTIEL"]
                )) . "<br/><br/>";
                
                if ($param["NB"] == 0)
                    $msg .= localize("Personne n'est atteint par la pluie sacrée") . "<br/><br/>";
                
                for ($i = 1; $i < $param["NB"] + 1; $i ++) {
                    
                    if ($param["HP_HEAL"][$i] > 1)
                        $pluriel = "s";
                    else
                        $pluriel = "";
                    if ($param["SELF"][$i]) {
                        $msg .= localize("Vous êtes atteint par la pluie", array(
                                "target" => $param[$i]["TARGET_NAME"]
                        )) . "<br/>";
                        $msg .= localize("vous regagnez {hp} point" . $pluriel . " de vie", array(
                                "target" => $param[$i]["TARGET_NAME"],
                                "hp" => $param["HP_HEAL"][$i]
                        )) . "<br/><br/>";
                    } else {
                        $msg .= localize("{target}" . " (" . $param[$i]["TARGET_ID"] . ") est atteint par la pluie", array(
                                "target" => $param[$i]["TARGET_NAME"]
                        )) . "<br/>";
                        $msg .= localize("{target} ({targetId}) est soigné de {hp} point" . $pluriel . " de vie", array(
                                "target" => $param[$i]["TARGET_NAME"],
                                "targetId" => $param[$i]["TARGET_ID"],
                                "hp" => $param["HP_HEAL"][$i]
                        )) . "<br/><br/>";
                    }
                }
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ReloadMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                if (isset($param["SPELL_LEVEL"]))
                    $msg .= "Vous réalisez un sort de niveau " . $param["SPELL_LEVEL"] . ".<br/>";
                if ($param["TYPE_ATTACK"] == SPELL_RELOAD_EVENT_FAIL) {
                    $msg .= "Le Bassin Divin est de niveau " . $param["BUILDING_LEVEL"] . ".<br/>";
                    $msg .= "Il n'a pas pu être rechargé.<br/><br/>";
                } else {
                    // $msg.=localize("Vous utilisez recharge du bassin en {x}-{y}",array("x"=>$param["X"], "y" => $param["Y"]))."<br/><br/>";
                    $msg .= localize("Le bassin ciblé a été rechargé avec succès") . "<br/><br/>";
                }
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class SunMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Soleil de guérison") . "<br/>";
                $msg .= localize("Votre Jet de maitrise de la magie est de");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                )) . "<br/>";
                $msg .= localize("Chacun de vos alliés sous l'effet du soleil sera automatiquement guérit de {level}% de la perte subite en cas d'attaque et ce pendant ce tour et les deux suivantes.", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* *************************************************************** LES SORTILEGES DE L'ECOLE DES LIMBES **************************************************** */
class DemonpunchMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez poing du démon sur {target} ({targetId}) ", array(
                        "target" => localize($param["TARGET_NAME"]),
                        "targetId" => $param["TARGET_ID"]
                )) . "<br/><br/>";
                
                $obj = new PassiveBarrierMsg();
                $msg .= $obj->msgBuilder($param);
                if (! $param["PLAYER_KILLED"]) {
                    $msg .= localize("Votre Jet d'Attaque magique est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                    
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                        $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                        $msg .= localize("Votre jet de MM est de : {mm} ", array(
                                "mm" => $param["PLAYER_MM"]
                        )) . "<br/>";
                        if ($param["TARGET_IS_NPC"]) {
                            $msg .= localize("Vous réalisez un sort de niveau {level}.", array(
                                    "level" => $param["SPELL_LEVEL"]
                            )) . "<br/>";
                            $msg .= localize("La résistance du monstre a été diminuée. Son armure baisse de {pt} points. ", array(
                                    "pt" => $param["SPELL_LEVEL"] * 1.2
                            )) . "<br/>";
                        } else {
                            $msg .= localize("Vous réalisez un sort de niveau {level} ({nb} points de durabilité) ", array(
                                    "level" => $param["SPELL_LEVEL"],
                                    "nb" => $param["POINT"]
                            )) . "<br/><br/>";
                            if ($param["NB_OBJECT"] == 1 && $param["NAME_OBJECT"][0] == 0)
                                $msg .= localize("{target}" . " (" . $param["TARGET_ID"] . ") ne porte aucune pièce d'équipement qui ne soit pas déjà cassée.", array(
                                        "target" => $param["TARGET_NAME"]
                                )) . "<br/>";
                            else {
                                $dep = 0;
                                if ($param["NAME_OBJECT"][0] == "none")
                                    $dep = 1;
                                for ($i = $dep; $i < $param["NB_OBJECT"]; $i ++)
                                    $msg .= localize("{objet} est touché et sa durabilité tombe à {nb}", array(
                                            "objet" => $param["NAME_OBJECT"][$i],
                                            "nb" => $param["DUR_OBJECT"][$i]
                                    )) . "<br/>";
                            }
                        }
                        
                        $msg .= "<br/>";
                    } 

                    else {
                        $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                    }
                    
                    $obj = new GainXPmsg();
                    $msg .= $obj->msgBuilder($param);
                    if (isset($param["WEAPON_BROKEN"]))
                        $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                    if (isset($param["MAIL"]))
                        $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
                }
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class interFireBallMsg
{

    function msgBuilder (&$param)
    {
        $msg = localize("Vous attaquez {name} ({nameId}) mais {opp} ({oppId}) s'interpose.", array(
                "name" => $param["NAME"],
                "opp" => $param["TARGET_NAME"],
                "nameId" => localize($param["INTER_ID"]),
                "oppId" => localize($param["TARGET_ID"])
        )) . "<br/>";
        $obj = new PassiveBarrierMsg();
        $msg .= $obj->msgBuilder($param);
        
        $msg .= localize("Votre Jet d'attaque magique est de ");
        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
        
        // $msg.=localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire")."<br/>";
        $msg .= localize("Votre jet de MM est de : {mm} ", array(
                "mm" => $param["PLAYER_MM"]
        )) . "<br/>";
        $msg .= localize("Vous réalisez un sort de niveau {level} ({nb}D6 de dégâts) ", array(
                "level" => $param["SPELL_LEVEL"],
                "nb" => 1.2 * $param["SPELL_LEVEL"]
        )) . "<br/>";
        $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                "damage" => $param["TARGET_DAMAGE"]
        )) . "<br/>";
        
        $obj = new PassiveBubbleMsg();
        $msg .= $obj->msgBuilder($param);
        
        $msg .= localize("Le Jet de maitrise de magie de votre adversaire est de ");
        $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"]) . "$<br/>";
        $msg .= localize("Son bonus d'armure est donc de {bonus}%", array(
                "bonus" => $param["ARMOR_BONUS"]
        )) . "<br/>";
        
        $msg .= localize("Son armure le protège et il ne perdra que <b>{hp} point(s) de vie</b>.", array(
                "hp" => $param["TARGET_HP"]
        )) . "<br/>";
        if ($param["TARGET_KILLED"] == 1) {
            $msg .= "<br/>";
            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
            $msg .= "<br/>";
            $obj = new FallMsg();
            $msg .= $obj->msgBuilder($param);
        }
        $msg .= "<br/>";
        
        $obj = new GainXPmsg();
        $msg .= $obj->msgBuilder($param);
        if (isset($param["WEAPON_BROKEN"]))
            $msg .= localize("Votre arme s'est cassée !") . "<br/>";
        if (isset($param["MAIL"]))
            $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        
        return $msg;
    }
}

class FireMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez brasier en {x}-{y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet d'Attaque magique est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
                $msg .= localize("Votre jet de MM est de : {mm} ", array(
                        "mm" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Vous réalisez un sort de niveau {level} ", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                
                if ($param["NB"] == 0)
                    $msg .= localize("Personne n'est atteint par le brasier") . "<br/><br/>";
                
                for ($i = 1; $i < $param["NB"] + 1; $i ++) {
                    if ($param["SELF"][$i]) {
                        $msg .= localize("Vous êtes atteint par le brasier", array(
                                "target" => $param[$i]["TARGET_NAME"]
                        )) . "<br/>";
                        $msg .= localize("Votre jet de défense est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"][$i] . " $<br/>";
                        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$i]) {
                            if (isset($param["BUBBLE_LIFE2"][$i])) {
                                if (! $param["BUBBLE_CANCEL2"][$i])
                                    $msg .= localize("Vous êtes protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                            "damage" => $param["BUBBLE_LIFE2"][$i],
                                            "level" => $param["BUBBLE_LEVEL2"][$i]
                                    )) . "<br/>";
                                else
                                    $msg .= localize("Vous êtes protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                            "nb" => $param["TARGET_DAMAGE"][$i]
                                    )) . "<br/>";
                            }
                            
                            $msg .= localize("Vous perdez {damage} points de vie.", array(
                                    "damage" => $param["TARGET_DAMAGE2"][$i]
                            )) . "<br/>";
                            if ($param["KILLED"][$i] == 1)
                                $msg .= localize("Vous avez été <b>TUÉ</b>.");
                            
                            $msg .= "<br/><br/>";
                        } else
                            $msg .= localize("Le brasier ne parvient pas à vous toucher.") . "<br/><br/>";
                    } else {
                        $msg .= localize("{target} ({targetId}) est atteint par le brasier", array(
                                "target" => $param[$i]["TARGET_NAME"],
                                "targetId" => $param[$i]["TARGET_ID"]
                        )) . "<br/>";
                        $msg .= localize("Son Jet de défense est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"][$i] . " $<br/>";
                        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$i]) {
                            if (isset($param["BUBBLE_LIFE2"][$i])) {
                                if (! $param["BUBBLE_CANCEL2"][$i])
                                    $msg .= localize("Votre adversaire est protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                            "damage" => $param["BUBBLE_LIFE2"][$i],
                                            "level" => $param["BUBBLE_LEVEL2"][$i]
                                    )) . "<br/>";
                                else
                                    $msg .= localize("Votre adversaire est protégé par une bulle de vie qui absorbe la totalité du coup ({nb} points de damage).", array(
                                            "nb" => $param["TARGET_DAMAGE"][$i]
                                    )) . "<br/>";
                            }
                            
                            $msg .= localize("Il perd {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGE2"][$i]
                            )) . "<br/>";
                            if ($param["KILLED"][$i] == 1) {
                                $msg .= localize("{target}" . " (" . $param[$i]["TARGET_ID"] . ") a été <b>TUÉ</b>.", array(
                                        "target" => $param[$i]["TARGET_NAME"]
                                )) . "<br/>";
                                $param2 = array();
                                if (isset($param["EQUIPMENT_FALL"][$i]))
                                    $param2["EQUIPMENT_FALL"] = $param["EQUIPMENT_FALL"][$i];
                                if (isset($param["MONEY_FALL"][$i]))
                                    $param2["MONEY_FALL"] = $param["MONEY_FALL"][$i];
                                if (isset($param["DROP_NAME"][$i]))
                                    $param2["DROP_NAME"] = $param["DROP_NAME"][$i];
                                if (isset($param["LARCIN"][$i]))
                                    $param2["LARCIN"] = $param["LARCIN"][$i];
                                
                                $obj = new FallMsg();
                                $msg .= $obj->msgBuilder($param2);
                                unset($param2);
                            }
                            $msg .= "<br/><br/>";
                        } else
                            $msg .= localize("Le brasier ne parvient pas à le toucher.") . "<br/><br/>";
                    }
                }
                
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class BloodMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous attaquez {target} ({targetId})", array(
                        "target" => localize($param["TARGET_NAME"]),
                        "targetId" => $param["TARGET_ID"]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet de maitrise de la magie est de");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
                $msg .= localize("Le Jet de maitrise de la magie de votre adversaire est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                    )) . "<br/>";
                    if (isset($param["LEVEL_BENE"])) {
                        $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau {level}.", array(
                                "level" => $param["LEVEL_BENE"]
                        )) . "<br/>";
                        if ($param["CANCEL"])
                            $msg .= localize("Sa protection est plus puissante que votre sortilège et en annule les effets") . "<br/>";
                        else
                            $msg .= localize("Les effets de votre sortilège sont réduit de {level} à {niveau} points de dégât pour les 5 prochains tours.", array(
                                    "level" => $param["TARGET_DAMAGE"],
                                    "niveau" => $param["VALUE"]
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vous infligez à votre adversaire une malédiction de {damage} points de dégâts pour les 5 prochains tours.", array(
                                "damage" => $param["TARGET_DAMAGE"]
                        )) . "<br/>";
                    
                    $msg .= "<br/>";
                } 

                else {
                    $msg .= localize("Votre adversaire a contré votre sort.") . "<br/><br/>";
                }
                
                $obj = new GainXPmsg();
                $msg .= $obj->msgBuilder($param);
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class FireBallMsg
{

    function msgBuilder ($error, $paramability, $param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                if ($param["PROTECTION"] == 1) {
                    $obj = new interFireballMsg();
                    $msg .= $obj->msgBuilder($param);
                } else {
                    $msg .= localize("Vous lancez une boule de feu sur {target} ({targetId}) ", array(
                            "target" => localize($param["TARGET_NAME"]),
                            "targetId" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                    $msg .= localize("Votre Jet d'Attaque magique est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"] . " $<br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire est de ");
                    $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_DEFENSE"] . " $<br/>";
                    
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                        if ($param["GOLEM"])
                            $msg .= localize("Le Golem de Feu absorde l'énergie de votre boule de feu et devient plus fort") . "<br/>";
                        
                        else {
                            $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                            $msg .= localize("Votre jet de MM est de : {mm} ", array(
                                    "mm" => $param["PLAYER_MM"]
                            )) . "<br/>";
                            $msg .= localize("Vous réalisez un sort de niveau {level} ({nb}D6 de dégâts) ", array(
                                    "level" => $param["SPELL_LEVEL"],
                                    "nb" => (1.2 * $param["SPELL_LEVEL"])
                            )) . "<br/>";
                            $msg .= localize("Vous lui infligez {damage} points de dégât.", array(
                                    "damage" => $param["TARGET_DAMAGE"]
                            )) . "<br/>";
                            
                            $obj = new PassiveBubbleMsg();
                            $msg .= $obj->msgBuilder($param);
                            
                            $msg .= localize("Son armure le protège et il ne perdra que <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $msg .= "<br/>";
                                $obj = new FallMsg();
                                $msg .= $obj->msgBuilder($param);
                            }
                        }
                        $msg .= "<br/>";
                    } 

                    else {
                        $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                    }
                    
                    $obj = new GainXPmsg();
                    $msg .= $obj->msgBuilder($param);
                    if (isset($param["WEAPON_BROKEN"]))
                        $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                    if (isset($param["MAIL"]))
                        $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
                }
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class PillarsMsg
{

    function msgBuilder ($error, $paramability, $param)
    {
        $msg = "";
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Piliers Infernaux en {x1}-{y1} et {x2}-{y2} et {x3}-{y3}", array(
                        "x1" => $param["X"][1],
                        "y1" => $param["Y"][1],
                        "x2" => $param["X"][2],
                        "y2" => $param["Y"][2],
                        "x3" => $param["X"][3],
                        "y3" => $param["Y"][3]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet de Maitrise de la magie est de : ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"]) . "$<br/>";
                $msg .= localize("Votre Jet d'attaque magique est de : ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "$<br/>";
                $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                for ($i = 1; $i < 4; $i ++) {
                    if (isset($param["BUILDING_LEVEL"][$i])) {
                        $msg .= localize("Vous attaquez {name} niveau {level}", array(
                                "name" => $param["BUILDING_NAME"][$i],
                                "level" => $param["BUILDING_LEVEL"][$i]
                        )) . "<br/>";
                        $msg .= localize("Vous infligez au bâtiment une perte de {nb} points de structure", array(
                                "nb" => $param["BUILDING_DAMAGE"]
                        )) . "<br/>";
                        if ($param["BUILDING_DEATH"][$i])
                            $msg .= localize("Vous avez <b> DETRUIT</b> le bâtiment") . "<br/>";
                    }
                    $msg .= "<br/>";
                }
                
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $param["AP"]
                )) . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        }
        return $msg;
    }
}

class FireCallMsg
{

    function msgBuilder ($error, $paramability, $param)
    {
        $msg = "";
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Appel du Feu") . "<br/><br/>";
                $msg .= localize("Votre Jet de Maitrise de la magie est de : ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"]) . "<br/>";
                $msg .= localize("Votre Jet d'attaque magique est de : ");
                $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_ATTACK"]) . "<br/><br/>";
                if (isset($param["ID_FEU_FOL_TARGET"])) {
                    $msg .= localize("Le Jet de Maitrise de la magie du Feu Fol (" . $param["ID_FEU_FOL_TARGET"] . ") est de : ");
                    $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["FEU_FOL_MM"]) . "<br/>";
                    $msg .= localize("Le Jet d'attaque magique du Feu Fol (" . $param["ID_FEU_FOL_TARGET"] . ") est de : ");
                    $msg .= localize("$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["FEU_FOL_ATTACK"]) . "<br/><br/>";
                    
                    if ($param["FEU_FOL_CAPTURE"] == 1) {
                        $msg .= localize("Vous avez réussi à apprivoiser le Feu Fol.") . "<br/><br/>";
                    } else {
                        $msg .= localize("Vous avez échoué à apprivoiser le Feu Fol.") . "<br/><br/>";
                    }
                } else {
                    $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                            "level" => $param["SPELL_LEVEL"]
                    )) . "<br/><br/>";
                    $msg .= localize("Vous avez réussi l'invocation d'un Feu Fol de niveau {niv}", array(
                            "niv" => ceil($param["SPELL_LEVEL"] / 1.5)
                    )) . "<br/>";
                }
                $msg .= "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            }
            
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* *************************************************************** LES SORTILEGES DE L'ECOLE DE PROTECTION **************************************************** */
class BarrierMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Barrière enflammée sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Barrière enflammée sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Force est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                if ($param["SELF"])
                    $msg .= localize("Vous avez créé une barrière enflammée qui infligera {nb}D6 de dégâts à tout adversaire vous attaquant au corps à corps pour le tour en cours et les deux suivants. ", array(
                            "target" => $param["TARGET_NAME"],
                            "nb" => $param["SPELL_LEVEL"] / 2
                    )) . "<br/>";
                else
                    $msg .= localize("Vous avez créé une barrière enflammée qui infligera {nb}D6 de dégâts à tout adversaire attaquant {target}" . " (" . $param["TARGET_ID"] . ") au corps à corps pour le tour en cours et les deux suivants. ", array(
                            "target" => $param["TARGET_NAME"],
                            "nb" => $param["SPELL_LEVEL"] / 2
                    )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class NegationMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Souffle de négation sur {target} ({targetId}).", array(
                        "target" => $param["TARGET_NAME"],
                        "targetId" => $param["TARGET_ID"]
                )) . "<br/>";
                
                /*
                 * $msg.=localize("Votre Jet de Maîtrise de la Magie est de ");
                 * $msg.="$.$.$.$.$.$.$.$.$.$.$.$.: $$$".$param["PLAYER_MM"]." $<br/>";
                 * $msg.=localize("Votre Jet de Force est de ");
                 * $msg.="$.$.$.$.$.$.$.$.$.$.$.$.: $$$".$param["PLAYER_STRENGTH"]." $<br/>";
                 *
                 * $msg.=localize("Vous réalisez un sort de niveau : <b>{level}</b>",array("level"=>$param["SPELL_LEVEL"],"nb"=> ceil($param["SPELL_LEVEL"]/3)));
                 * $msg.="$$<br/>";
                 */
                $msg .= localize("{target}" . " (" . $param["TARGET_ID"] . ") est désormais invisible et ne peut plus être ciblé. ", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["SPELL_LEVEL"] / 2
                )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class BubbleMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Bulle de vie sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Bulle de vie sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Force est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/>";
                $msg .= localize("Vous avez créé une bulle de vie capable d'encaisser {nb} points de dégât. ", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["SPELL_LEVEL"] * 3
                )) . "<br/>";
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class ShieldMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Bouclier magique sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Bouclier magique sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Force est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                if ($param["SELF"])
                    $msg .= localize("Votre défense est augmentée de {armor} %. ", array(
                            "target" => $param["TARGET_NAME"],
                            "armor" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                else
                    $msg .= localize("La défense de {target}" . " (" . $param["TARGET_ID"] . ") est augmenté de {armor} %.", array(
                            "target" => $param["TARGET_NAME"],
                            "armor" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class BlessMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Bénédiction sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Bénédiction sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Force est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "$$<br/>";
                $msg .= localize("Vous créer une protection de niveau {level} contre les maux et malédictions pour le tour en cours de la cible et les 2 suivants. ", array(
                        "target" => $param["TARGET_NAME"],
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class WallMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Réveil de la terre") . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Force est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_STRENGTH"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Vous avez créé {nb} mur(s) de terre de niveau {niv}", array(
                        "nb" => $param["NB_WALL"],
                        "niv" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* *************************************************************** LES SORTILEGES DE L'ECOLE D'ALTERATION **************************************************** */
class AngerMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["SELF"])
                    $msg .= localize("Vous utilisez Ailes de colère sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous utilisez Ailes de colère sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/>";
                $msg .= localize("Vous avez créé un enchantement qui augmente la caractéristique : <b> {carac} </b> de {nb}D6 pour le tour en cours et les deux suivants. ", array(
                        "carac" => $param["CHARAC_NAME"],
                        "nb" => $param["SPELL_LEVEL"] / 2
                )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class CurseMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Malédiction d'Arcxos sur {target}" . " (" . $param["TARGET_ID"] . ").", array(
                        "target" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Le Jet de Maîtrise de la Magie de votre adversaire  est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TARGET_MM"] . " $<br/>";
                if ($param["SUCCESS"]) {
                    $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] / 3)
                    ));
                    $msg .= "$$<br/>";
                    
                    if (isset($param["LEVEL_BENEDICTION"])) {
                        $msg .= localize("Votre adversaire est protégé par une bénédiction de niveau <b>{level}</b>", array(
                                "level" => $param["LEVEL_BENEDICTION"]
                        )) . "<br/>";
                        if ($param["CANCEL"] == 1)
                            $msg .= localize("Sa protection est plus puissante que votre malédiction et en annule les effets") . "<br/>";
                        else 
                            if ($param["CHARAC_NAME"] == "ENCHANTEMENTS") {
                                $msg .= localize("Les effets de votre malédiction sur les enchantements de {target}" . " (" . $param["TARGET_ID"] . ") ont été réduit de {nb} à {nb2} niveau(x).", array(
                                        "target" => $param["TARGET_NAME"],
                                        "nb" => $param["SPELL_LEVEL"],
                                        "nb2" => $param["SPELL_LEVEL_FINAL"]
                                )) . "<br/>";
                                $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
                            } else {
                                $msg .= localize("Les effets de votre malédiction sont réduit de {nb} à {nb2}D6 de malus sur la caractéristique : <b> {carac} </b> pour le tour en cours de la cible et le suivant.", array(
                                        "carac" => $param["CHARAC_NAME"],
                                        "nb" => $param["LEVEL_BM"] / 2.5,
                                        "nb2" => $param["LEVEL_BM_FINAL"] / 2.5
                                )) . "<br/>";
                            }
                    } else {
                        if ($param["CHARAC_NAME"] == "ENCHANTEMENTS") {
                            $msg .= localize("Tous les enchantements de {target}" . " (" . $param["TARGET_ID"] . ") ont été réduit de {nb} niveau(x).", array(
                                    "target" => $param["TARGET_NAME"],
                                    "nb" => $param["SPELL_LEVEL_FINAL"]
                            )) . "<br/>";
                            $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
                        } else {
                            $msg .= localize("Vous avez créé une malédiction qui reduit la caractéristique : <b> {carac} </b> de {nb}D6 pour le tour en cours de la cible et le suivant. ", array(
                                    "carac" => $param["CHARAC_NAME"],
                                    "nb" => $param["SPELL_LEVEL"] / 2.5
                            )) . "<br/>";
                        }
                    }
                } else
                    $msg .= localize("Vous avez été contré par votre adversaire et vous n'êtes pas parvenu à lui infligé une malédiction") . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class SpellMassiveTeleportMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous tentez une téléportation en  x={x} y={y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Le temple ou vous vous trouvez est de niveau ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["TEMPLE_LEVEL"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/><br/>";
                if ($param["SUCCESS"]) {
                    $msg .= localize("Vous avez téléporté avec succès aux environ de la destination souhaitée les personnages de votre groupe de chasse suivants. Les autres sont restés où ils étaient.") . "<br/>";
                    if ($param["NB"] > 0) {
                        for ($i = 1; $i < $param["NB"] + 1; $i ++) {
                            $msg .= "  - " . $param["RESULT_PJ"][$i]["name"] . " (" . $param["RESULT_PJ"][$i]["id"] . ") a été téléporté en (" . $param["RESULT_PJ"][$i]["x"] . "/" . $param["RESULT_PJ"][$i]["y"] . ").<br/>";
                        }
                    } else {
                        $msg .= "  - Personne n'a été téléporté.<br/>";
                    }
                } else
                    $msg .= localize("Votre jet de MM n'est pas suffisant pour atteindre la destination souhaitée. Vous n'avez téléporté personne.") . "<br/>";
            }
            
            $msg .= localize("<br/>Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class SpellTeleportMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous tentez une téléportation en  x={x} y={y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/>";
                if ($param["CASE_BUSY"]) {
                    $msg .= localize("Vous vous téléportez jusqu'à la place sélectionnée mais celle-ci est occupée") . "<br/>";
                    $msg .= localize("Vous restez donc coincé dans une dimension d'espace-temps parallèle jusqu'à votre MORT") . "<br/>";
                } else {
                    if ($param["SUCCESS"])
                        $msg .= localize("Vous vous téléportez avec succès jusqu'à la destination souhaitée") . "<br/>";
                    else
                        $msg .= localize("Votre jet de MM n'est pas suffisant pour atteindre la destination souhaitée. Vous n'avez pas bougé.") . "<br/>";
                }
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class SpellInstantBloodMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        $msg = "";
        
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            
            if ($param["PLAYER_IS_NPC"]) {
                if (isset($param["PLAYER_GENDER"])) {
                    if ($param["PLAYER_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg .= localize("Vous avez utilisé Rayon de lumière noire sur {target}" . " (" . $param["TARGET_ID"] . ")", array(
                        "target" => $art . " " . localize($param["TARGET_NAME"])
                )) . "<br/><br/>";
            } else
                $msg .= localize("Vous avez utilisé Rayon de lumière noire sur {target}" . " (" . $param["TARGET_ID"] . ") ", array(
                        "target" => $param["TARGET_NAME"]
                )) . "<br/><br/>";
            
            $msg .= localize("Le Jet de maitrise de la magie de votre adversaire a été de");
            $msg .= "........................: " . $param["PLAYER_ATTACK"] . "<br/>";
            $msg .= localize("Votre Jet de maitrise de la magie a été de ");
            $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
            
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                $msg .= localize("Votre adversaire a réalisé un sort de niveau {level}", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                )) . "<br/>";
                $msg .= localize("Vous lui avez infligé {damage} points de dégât.", array(
                        "damage" => $param["TARGET_DAMAGE"]
                )) . "<br/>";
                $obj = new PassiveBubbleMsg();
                $msg .= $obj->msgBuilder($param);
                $msg .= localize("Son armure le protège et il ne perdra que <b>{hp} point(s) de vie</b>.", array(
                        "hp" => $param["TARGET_HP"]
                )) . "<br/>";
                
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= "<br/>";
                    $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                    $msg .= "<br/>";
                    $obj = new FallMsg();
                    $msg .= $obj->msgBuilder($param);
                }
            } else {
                $msg .= localize("Vous avez contré le sort de votre adversaire.") . "<br/><br/>";
            }
            $msg .= "<br/>";
            $msg .= "<br/>";
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
            $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                    "xp" => $param["XP"]
            ));
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class SpellSoulMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous tentez une projection de l'âme en  x={x} y={y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/>";
                if ($param["SUCCESS"])
                    $msg .= localize("Vous avez réussi. Votre âme se trouve actuellement à la destination souhaitée à plus ou mois 3 cases.") . "<br/>";
                else
                    $msg .= localize("Votre jet de MM n'est pas suffisant ou les cases autour n'étaient pas libres pour projeter votre âme à la destination souhaitée.") . "<br/>";
            }
            
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
            if (isset($param["WEAPON_BROKEN"]))
                $msg .= localize("Votre arme s'est cassée !") . "<br/>";
            if (isset($param["MAIL"]))
                $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class LightTouchMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Touché de lumière sur {target}" . " (" . $param["TARGET_ID"] . ")", array(
                        "target" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/>";
                
                $msg .= localize("{target}" . " (" . $param["TARGET_ID"] . ") regagne {nb} points d'action.", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["AP_GAIN"]
                )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class SpellRecallMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new MagicSpellMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Rappel sur {target}" . " (" . $param["TARGET_ID"] . ")", array(
                        "target" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_MM"] . " $<br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SPEED"] . " $<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "$$<br/>";
                if ($param["SUCCESS"])
                    $msg .= localize("Vous avez réussi votre sort de rappel. Votre cible aura la possibilité de se téléporter à côté de vous à son prochain tour.", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                else
                    $msg .= localize("Votre niveau de sort n'est pas suffisant pour rappeler une cible aussi éloignée.", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
                if (isset($param["WEAPON_BROKEN"]))
                    $msg .= localize("Votre arme s'est cassée !") . "<br/>";
                if (isset($param["MAIL"]))
                    $msg .= localize("Suite à cette action, vous avez reçu un message.") . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

/* *************************************************************** LES TALENT d'EXTRACTION **************************************************** */
class ExtractionMsg
{

    function msgBuilder ($error, $ident, &$paramability, &$param)
    {
        switch ($ident) {
            case TALENT_MINE:
                $obj = new MineMsg();
                break;
            case TALENT_DISMEMBER:
                $obj = new DismemberMsg();
                break;
            case TALENT_SCUTCH:
                $obj = new ScutchMsg();
                break;
            case TALENT_HARVEST:
                $obj = new HarvestMsg();
                break;
            default:
                break;
        }
        $msg = $obj->msgBuilder($error, $paramability, $param);
        return $msg;
    }
}

class MineMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $fem = "e";
                if ($param["GNAME"] == "Rubis")
                    $fem = "";
                
                if ($param["INV"] == 1)
                    $sac = "sac de gemmes";
                else
                    $sac = "sac d'équipement";
                
                if ($param["TOOL"]) {
                    $msg .= localize("Vous exploitez un gisement de niveau " . $param["LEVELG"]) . "<br/>";
                    $msg .= localize("Vous utilisez votre pioche pour extraire un" . $fem . " " . $param["GNAME"]) . "<br/>";
                    $msg .= localize("Vos chances d'obtenir une gemme de niveau supérieur sont de {percent}%", array(
                            "percent" => $param["TPERCENT"]
                    )) . "<br/>";
                    $msg .= localize("Votre score est de ");
                    $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["TSCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . "<br/>";
                    if ($param["TSUCCESS"])
                        $msg .= localize("Félicitations, vous parvenez à extraire un" . $fem . " {pierre} de niveau {niv}", array(
                                "pierre" => $param["GNAME"],
                                "niv" => $param["LEVEL"]
                        )) . "<br/>";
                    else
                        $msg .= localize("Dommage, vous avez extrait un" . $fem . " {pierre} de niveau {niv}", array(
                                "pierre" => $param["GNAME"],
                                "niv" => $param["LEVEL"]
                        )) . "<br/>";
                    
                    $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                            "sac" => $sac
                    )) . "<br/>";
                    if ($param["BROKEN"])
                        $msg .= localize("Votre pioche de mineur était trop usée, elle s'est cassée.") . "<br/><br/>";
                } else {
                    $msg .= localize("Vous exploitez un gisement de niveau " . $param["LEVELG"]) . "<br/>";
                    $msg .= localize("Votre travail a été fructueux, vous obtenez un" . $fem . " {pierre} de niveau {niv}", array(
                            "pierre" => $param["GNAME"],
                            "niv" => $param["LEVEL"]
                    )) . "<br/>";
                    $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                            "sac" => $sac
                    )) . "<br/>";
                }
                $msg .= localize("Cette action vous a coûté {ap} PA", array(
                        "ap" => $paramability["AP"]
                )) . "<br/>";
            }
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        return $msg;
    }
}

class DismemberMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $fem = "e";
                if ($param["ELEMENT"] == "cuir")
                    $fem = "";
                
                if ($param["INV"] == 2)
                    $sac = "sac de matières premières";
                else
                    $sac = "sac d'équipement";
                
                $msg .= localize("Votre dépecez une dépouille de niveau " . $param["LEVELD"]) . "<br/>";
                if ($param["TOOL"])
                    $msg .= localize("Vous utilisez votre pince à dépecer pour récupérer un" . $fem . " " . $param["ELEMENT"]) . "<br/>";
                $msg .= localize("Félicitations, vous parvenez à récupérer un" . $fem . " {peau} de niveau {niv}", array(
                        "peau" => $param["ELEMENT"],
                        "niv" => $param["ELEMENT_LEVEL"]
                )) . "<br/>";
                $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                        "sac" => $sac
                )) . "<br/>";
                if ($param["BROKEN"])
                    $msg .= localize("Votre pince à dépecer était trop usée, elle s'est cassée.") . "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class ScutchMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["INV"] == 2)
                    $sac = "sac de matières premières";
                else
                    $sac = "sac d'équipement";
                
                $msg .= localize("Vous teillez un champs de niveau " . $param["LEVELC"]) . "<br/>";
                if ($param["TOOL"]) {
                    $msg .= localize("Vous utilisez votre teilleuse pour récolter du lin") . "<br/>";
                    $msg .= localize("Vos chances d'obtenir un lin de niveau supérieur sont de {percent}%", array(
                            "percent" => $param["TPERCENT"]
                    )) . "<br/>";
                    $msg .= localize("Votre score est de ");
                    $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["TSCORE"] . " $$$$$$";
                    if ($param["TSUCCESS"])
                        $msg .= localize("Félicitations, vous parvenez à extraire un lin de niveau {niv}", array(
                                "niv" => $param["ELEMENT_LEVEL"]
                        )) . "<br/>";
                    else
                        $msg .= localize("Dommage, vous avez récolté un lin de niveau {niv}", array(
                                "niv" => $param["ELEMENT_LEVEL"]
                        )) . "<br/>";
                } else
                    $msg .= localize("Félicitations, vous parvenez à récupérer un lin de niveau {niv}", array(
                            "niv" => $param["ELEMENT_LEVEL"]
                    )) . "<br/>";
                
                $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                        "sac" => $sac
                )) . "<br/>";
                if ($param["BROKEN"])
                    $msg .= localize("Votre teilleuse était trop usée, elle s'est cassée.") . "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class CutMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["ELEMENT_LEVEL"] == 0)
                    $msg .= localize("Malgré une longue recherche, vous ne trouvez malheureusement aucun bois à couper sur ce type de terrain") . "<br/>";
                else {
                    if ($param["INV"] == 2)
                        $sac = "sac de matières premières";
                    else
                        $sac = "sac d'équipement";
                    
                    $msg .= localize("Vous recherchez du bois à couper") . "<br/>";
                    if ($param["TOOL"]) {
                        $msg .= localize("Vous utilisez votre cauchoir pour couper du bois") . "<br/>";
                        $msg .= localize("Vos chances d'obtenir un bois de niveau supérieur sont de {percent}%", array(
                                "percent" => $param["TPERCENT"]
                        )) . "<br/>";
                        $msg .= localize("Votre score est de ");
                        $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["TSCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . "<br/>";
                        if ($param["TSUCCESS"])
                            $msg .= localize("Félicitations, vous parvenez à couper du bois de niveau {niv}", array(
                                    "niv" => $param["ELEMENT_LEVEL"]
                            )) . "<br/>";
                        else
                            $msg .= localize("Dommage, vous avez coupé un bois de niveau {niv}", array(
                                    "niv" => $param["ELEMENT_LEVEL"]
                            )) . "<br/>";
                    } else
                        $msg .= localize("Félicitations, vous parvenez à couper un bois de niveau {niv}", array(
                                "niv" => $param["ELEMENT_LEVEL"]
                        )) . "<br/>";
                    
                    $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                            "sac" => $sac
                    )) . "<br/>";
                    if ($param["BROKEN"])
                        $msg .= localize("Votre cauchoir était trop usé, il s'est cassé.") . "<br/><br/>";
                }
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class HarvestMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["INV"] == 3)
                    $sac = "sac d'herbes";
                else
                    $sac = "sac d'équipement";
                
                $msg .= localize("Vous récoltez un buisson de niveau " . $param["LEVELB"]) . "<br/>";
                if ($param["TOOL"]) {
                    $msg .= localize("Vous utilisez votre pressoir pour récolter une {herbe}", array(
                            "herbe" => $param["ELEMENT"]
                    )) . "<br/>";
                    $msg .= localize("Vos chances d'obtenir une {herbe} de niveau supérieur sont de {percent}%", array(
                            "herbe" => $param["ELEMENT"],
                            "percent" => $param["TPERCENT"]
                    )) . "<br/>";
                    $msg .= localize("Votre score est de ");
                    $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["TSCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
                    if ($param["TSUCCESS"])
                        $msg .= localize("Félicitations, vous parvenez à extraire une {herbe} de niveau {niv}", array(
                                "herbe" => $param["ELEMENT"],
                                "niv" => $param["ELEMENT_LEVEL"]
                        )) . "<br/>";
                    else
                        $msg .= localize("Dommage, vous avez récolté une {herbe} de niveau {niv}", array(
                                "herbe" => $param["ELEMENT"],
                                "niv" => $param["ELEMENT_LEVEL"]
                        )) . "<br/>";
                } else
                    $msg .= localize("Félicitations, vous parvenez à récupérer une {herbe} de niveau {niv}", array(
                            "herbe" => $param["ELEMENT"],
                            "niv" => $param["ELEMENT_LEVEL"]
                    )) . "<br/>";
                
                $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                        "sac" => $sac
                )) . "<br/>";
                if ($param["BROKEN"])
                    $msg .= localize("Votre pressoir était trop usé, elle s'est cassé.") . "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class SpeakMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                if ($param["INV"] == 2)
                    $sac = "sac de matières premières";
                else
                    $sac = "sac d'équipement";
                
                $msg .= localize("Vous négociez avec un Kradjeck ferreux de niveau " . $param["LEVELK"]) . "<br/>";
                if ($param["TOOL"]) {
                    $msg .= "Vous utilisez votre gemme à conviction. Vos chances d'obtenir une fer de niveau supérieur sont de " . $param["TPERCENT"] . "%.<br/>";
                }
                
                $msg .= localize("Félicitations, vous parvenez à obtenir un fer de niveau {niv}", array(
                        "niv" => $param["ELEMENT_LEVEL"]
                )) . "<br/>";
                
                $msg .= localize("L'objet a été rangé dans votre {sac}", array(
                        "sac" => $sac
                )) . "<br/>";
                if ($param["BROKEN"])
                    $msg .= localize("Votre gemme de conviction était trop usée, elle s'est cassée.") . "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* *************************************************************** LES TALENT NATURE **************************************************** */
class TalentKradjeckCallMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez {name}", array(
                        "name" => $param["TALENT_NAME"]
                )) . "<br/><br/>";
                $msg .= localize("Le niveau du portail est de {level}.", array(
                        "level" => $param["GATE_LEVEL"]
                )) . " $$<br/>";
                $msg .= localize("Le nombre de Kradjeck Ferreux dans votre vue est de {level}.", array(
                        "level" => $param["KRADJ_NB"]
                )) . " $$<br/>";
                
                $msg .= localize("Vos chances de réussite sont donc de {percent}%.", array(
                        "percent" => $param["PERCENT"]
                )) . " $$<br/><br/>";
                $msg .= localize("Votre score est de ");
                $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["SCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . "<br/>";
                if ($param["SCORE"] <= $param["PERCENT"])
                    $msg .= localize("Vous parvenez à attirer un kradjeck Ferreux.") . " $<br/><br/>";
                else
                    $msg .= localize("Vous ne parvenez pas à attirer un Kradjeck Ferreux. ") . " $<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TalentMonsterMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez {name}", array(
                        "name" => $param["TALENT_NAME"]
                )) . "<br/><br/>";
                $levelMonster = "";
                if (isset($param["MONSTER_LEVEL"])) {
                    $levelMonster = " de niveau " . $param["MONSTER_LEVEL"];
                }
                $msg .= localize("Vous estimez les caractéristiques de {target} ({id})" . $levelMonster . ": ", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . " $$<br/>";
                $msg .= localize("Vie :");
                $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["TARGET_HP"] . "<br/>";
                $msg .= localize("{name1} : ", array(
                        "name1" => $param["CHARAC1_NAME"]
                ));
                $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["CHARAC1_VALUE"] . "<br/>";
                $msg .= localize("{name2} : ", array(
                        "name2" => $param["CHARAC2_NAME"]
                ));
                $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["CHARAC2_VALUE"] . "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TalentDetectionMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez {name}", array(
                        "name" => $param["TALENT_NAME"]
                )) . "<br/>";
                
                if ($param["DIRECTION"] == 99)
                    $msg .= localize("Il ne semble pas y avoir de {name} de niveau {niveau} dans les parages", array(
                            "name" => $param["NAME"],
                            "niveau" => $param["NIVEAU"]
                    ));
                elseif ($param["DIST"] > 30)
                    $msg .= localize("Le {name} de niveau {niveau} le plus proche se trouve à environ {dist} lieues d'ici ", array(
                            "name" => $param["NAME"],
                            "niveau" => $param["NIVEAU"],
                            "dist" => $param["DIST"]
                    ));
                elseif ($param["DIRECTION"] == "")
                    $msg .= localize("Le {name} de niveau {niveau} le plus proche est en vue.", array(
                            "name" => $param["NAME"],
                            "niveau" => $param["NIVEAU"]
                    ));
                else
                    $msg .= localize("Le {name} de niveau {niveau} le plus proche se situe à environ <b> {dist} </b> lieues en suivant la direction : <b> {dir} </b>.", array(
                            "name" => $param["NAME"],
                            "niveau" => $param["NIVEAU"],
                            "dist" => $param["DIST"],
                            "dir" => $param["DIRECTION"]
                    ));
                
                $msg .= "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TalentCloseGateMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous avez réussi votre incantation") . "<br/>";
                $msg .= localize("Le portail démoniaque de niveau {level} ({id}) en x={xp},y={yp} a été fermé avec succès.", array(
                        "level" => $param["GATE_LEVEL"],
                        "id" => $param["GATE_ID"],
                        "xp" => $param["GATE_X"],
                        "yp" => $param["GATE_Y"]
                )) . "<br/>";
                
                $msg .= "<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TalentRefineMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez {name}", array(
                        "name" => $param["TALENT_NAME"]
                )) . "<br/><br/>";
                $msg .= localize("Votre première pièce est de niveau {niv1}, votre seconde pièce est de niveau {niv2}.", array(
                        "niv1" => $param["LEVEL_OBJECT1"],
                        "niv2" => $param["LEVEL_OBJECT2"]
                )) . " $<br/>";
                if ($param["TOOL"])
                    $msg .= localize("Bonus outil niveau {niv}: {bonus}%", array(
                            "niv" => $param["TOOL_LEVEL"],
                            "bonus" => $param["BONUS"]
                    )) . " $<br/>";
                if ($param["GUILD_BONUS"])
                    $msg .= "Vous avez payé " . GUILD_BONUS_PRICE . " PO pour utiliser les outils de la guilde des artisans qui vous donne un bonus de " . $param["GUILD_BONUS"] . "%.<br/>";
                $msg .= localize("Vos chances de réussite sont donc de {percent}%.", array(
                        "percent" => $param["PERCENT"]
                )) . " $$<br/><br/>";
                $msg .= localize("Votre score est de ");
                $msg .= "$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.$$.: $$$" . $param["SCORE"] . " $$$$$$&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . "<br/>";
                if ($param["SCORE"] <= $param["PERCENT"])
                    $msg .= localize("Vous parvenez à confectionner une pièce de niveau {niv} ", array(
                            "niv" => $param["FINAL_LEVEL"]
                    )) . " $<br/><br/>";
                else
                    $msg .= localize("Vous ne parvenez pas à confectionner une pièce de niveau supérieur ") . " $<br/><br/>";
                
                if ($param["BROKEN"])
                    $msg .= localize("Vous outil niveau {niv} était trop usé, il s'est cassé", array(
                            "niv" => $param["TOOL_LEVEL"]
                    )) . " $<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

/* *************************************************************** LES TALENT d'ARTISANAT **************************************************** */
class TalentCraftMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez {ident}", array(
                        "ident" => $param["TALENT_NAME"]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet de {carac} est de ", array(
                        "carac" => $param["TALENT_CARAC"]
                ));
                $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SKILL"] . " $<br/>";
                if ($param["GUILD_BONUS"] != 0)
                    $msg .= " dont un bonus de +" . $param["GUILD_BONUS"] . " car vous avez payé " . $param["PRICE"] . " PO pour utiliser les facilités de la guilde des artisans. " . "<br/>";
                $msg .= localize("Vous réalisez donc une maitrise d'artisanat de niveau {niv}", array(
                        "niv" => $param["LEVEL_CRAFT"]
                )) . " $<br/>";
                
                if ($param["NEW"]) {
                    if ($param["EQUIP_ID"] < 207 || $param["EQUIP_ID"] > 212)
                        $msg .= localize("La matière première utilisée est de niveau {niv} ", array(
                                "niv" => $param["LEVEL_RAWMATERIAL"]
                        )) . " $<br/>";
                    
                    if ($param["EQUIP_LEVEL"] < $param["LEVEL_RAWMATERIAL"])
                        $msg .= localize("Mais vous avez préféré la travailler comme une matière première de niveau {niv} ", array(
                                "niv" => $param["EQUIP_LEVEL"]
                        )) . " $<br/>";
                } else
                    $msg .= localize("La pièce que vous travaillez est de niveau {niv}", array(
                            "niv" => $param["LEVEL_RAWMATERIAL"]
                    )) . " $<br/>";
                
                if ($param["SUCCESS"] == 0)
                    $msg .= localize("Votre maitrise d'artisanat n'est pas suffisante pour travailler sur une matière première de ce niveau") . " $<br/>";
                else {
                    
                    if ($param["END"]) {
                        if ($param["TOOL"]) {
                            if ($param["EQUIP_SUP"])
                                $msg .= localize("Votre outil vous a permis de confectionner {nb} objet(s) supplémentaire(s)", array(
                                        "nb" => $param["EQUIP_SUP"]
                                )) . "$<br/>";
                            else
                                $msg .= localize("Votre outil ne vous confère pas suffisamment de bonus pour confectionner un objet supplémentaire") . "$<br/>";
                        }
                        if ($param["FULL"])
                            $msg .= localize("Vous n'avez confectionné que {nb} objet(s) : '{name}' de niveau {niv} car votre inventaire est plein", array(
                                    "nb" => $param["NB_EQUIP"],
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . " $<br/><br/>";
                        else {
                            if ($param["NB_EQUIP"] == 0)
                                $msg .= localize("Votre pourcentage de maitrise d'artisanat est trop faible pour confectionner un objet de ce niveau", array(
                                        "nb" => $param["NB_EQUIP"],
                                        "niv" => $param["EQUIP_LEVEL"],
                                        "name" => $param["EQUIP_NAME"]
                                )) . " $<br/><br/>";
                            else
                                $msg .= localize("Vous avez confectionné {nb} objet(s) : '{name}' de niveau {niv} ", array(
                                        "nb" => $param["NB_EQUIP"],
                                        "niv" => $param["EQUIP_LEVEL"],
                                        "name" => $param["EQUIP_NAME"]
                                )) . " $<br/><br/>";
                        }
                    } else {
                        if ($param["TOOL"])
                            $msg .= localize("Votre outil de niveau {niv} vous confère un bonus de {bonus}% d'avancement dans la confection", array(
                                    "niv" => $param["TOOL_LEVEL"],
                                    "bonus" => $param["BONUS"]
                            )) . " $<br/>";
                        
                        if ($param["PROGRESS"] == 100)
                            $msg .= localize("Félicitations ! Vous avez terminé la confection d'un(e) {name} de niveau {niv} ", array(
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . " $<br/><br/>";
                        else
                            $msg .= localize("Vous avancez de {vit}% dans la confection d'un(e) {name} de niveau {niv} ", array(
                                    "vit" => $param["SPEED"],
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . " $<br/><br/>";
                    }
                }
                if ($param["BROKEN"])
                    $msg .= localize("Votre outil niveau {niv} était trop usé, il s'est cassé", array(
                            "niv" => $param["TOOL_LEVEL"]
                    )) . " $<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

class TalentEnchantMsg
{

    function msgBuilder ($error, &$paramability, &$param)
    {
        if (! $error) {
            $obj = new TalentMsg();
            $msg = $obj->msgBuilder($paramability);
            if ($paramability["SUCCESS"]) {
                $msg .= localize("Vous utilisez Artisanat des gemmes") . "<br/>";
                switch ($param["CRAFT_TYPE"]) {
                    // Création d'un bâton ou d'un sceptre
                    case 1:
                        if ($param["EQUIP_ID"] != 220) {
                            $msg .= localize("L'émeraude utilisée est de niveau {niv} ", array(
                                    "niv" => $param["GEM_LEVEL"]
                            )) . " $<br/>";
                            $msg .= localize("Le bois utilisé est de niveau {niv} ", array(
                                    "niv" => $param["WOOD_LEVEL"]
                            )) . " $<br/>";
                        }
                        $msg .= localize("Vous souhaitez réaliser un artisanat de niveau {niv} ", array(
                                "niv" => $param["EQUIP_LEVEL"]
                        )) . " $<br/>";
                        
                        $msg .= localize("Votre Jet de Maitrise de la magie est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SKILL"] . " $<br/>";
                        if ($param["GUILD_BONUS"] != 0)
                            $msg .= " dont un bonus de +" . $param["GUILD_BONUS"] . " car vous avez payé " . $param["PRICE"] . " PO pour utiliser les facilités de la guilde des artisans. " . "<br/>";
                        
                        $msg .= localize("Vous réalisez donc une maitrise d'artisanat de niveau {niv}", array(
                                "niv" => $param["LEVEL_CRAFT"]
                        )) . " $<br/>";
                        
                        if ($param["SUCCESS"] == 0)
                            $msg .= localize("Votre maitrise d'artisanat n'est pas suffisante pour travailler sur une matière première de ce niveau") . " $<br/><br/>";
                        else {
                            if ($param["TOOL"])
                                $msg .= localize("Votre outil de niveau {niv} vous confère un bonus de {bonus}% d'avancement dans la confection", array(
                                        "niv" => $param["TOOL_LEVEL"],
                                        "bonus" => $param["BONUS"]
                                )) . " $<br/>";
                            
                            if ($param["PROGRESS"] == 100)
                                $msg .= localize("Félicitations ! Vous avez terminé la confection d'un(e) {name} de niveau {niv} ", array(
                                        "niv" => $param["EQUIP_LEVEL"],
                                        "name" => $param["EQUIP_NAME"]
                                )) . " $<br/><br/>";
                            else
                                $msg .= localize("Vous avancez de {vit}% dans la confection d'un(e) {name} de niveau {niv} ", array(
                                        "vit" => $param["SPEED"],
                                        "niv" => $param["EQUIP_LEVEL"],
                                        "name" => $param["EQUIP_NAME"]
                                )) . " $<br/><br/>";
                        }
                        break;
                    // Continuation de la confection d'un sceptre ou d'un bâton ou d'un enchantement
                    case 2:
                        $msg .= localize("L'équipement à travailler est de niveau {niv} ", array(
                                "niv" => $param["EQUIP_LEVEL"]
                        )) . " $<br/>";
                        $msg .= localize("Votre Jet de Maitrise de la magie est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SKILL"] . " $<br/>";
                        if ($param["GUILD_BONUS"] != 0)
                            $msg .= " dont un bonus de +" . $param["GUILD_BONUS"] . " car vous avez payé " . $param["PRICE"] . " PO pour utiliser les facilités de la guilde des artisans. " . "<br/>";
                        
                        $msg .= localize("Vous réalisez une maitrise d'artisanat de niveau {niv}", array(
                                "niv" => $param["LEVEL_CRAFT"]
                        )) . " $<br/>";
                        if ($param["SUCCESS"] == 0)
                            $msg .= localize("Votre maitrise d'artisanat n'est pas suffisante pour travailler sur une matière première de ce niveau") . " $<br/><br/>";
                        else {
                            if ($param["TOOL"])
                                $msg .= localize("Votre outil de niveau {niv} vous confère un bonus de {bonus}% d'avancement dans la confection", array(
                                        "niv" => $param["TOOL_LEVEL"],
                                        "bonus" => $param["BONUS"]
                                )) . " $<br/>";
                            
                            if ($param["PROGRESS"] == 100)
                                $msg .= localize("Félicitations ! Vous avez terminé la confection d'un(e) {name} de niveau {niv} ", array(
                                        "niv" => $param["EQUIP_LEVEL"],
                                        "name" => $param["EQUIP_NAME"]
                                )) . " $<br/><br/>";
                            else {
                                $msg .= localize("Vous avancez de {vit}% dans la confection d'un(e) {name} de niveau {niv} ", array(
                                        "vit" => $param["SPEED"],
                                        "niv" => $param["EQUIP_LEVEL"],
                                        "name" => $param["EQUIP_NAME"]
                                )) . " $<br/>";
                                $msg .= localize("L'objet est maintenant à {vit}% d'avancement ", array(
                                        "vit" => $param["PROGRESS"]
                                )) . " $<br/><br/>";
                            }
                        }
                        break;
                    // Enchantement mineur et majeur
                    case 3:
                    case 4:
                        $msg .= localize("La gemme utilisée est de niveau {niv} ", array(
                                "niv" => $param["GEM_LEVEL"]
                        )) . " $<br/>";
                        $msg .= localize("L'équipement à enchanter est de niveau {niv} ", array(
                                "niv" => $param["EQUIP_LEVEL"]
                        )) . " $<br/>";
                        $msg .= localize("Vous souhaitez réaliser un artisanat de niveau {niv} ", array(
                                "niv" => $param["TEMPLATE_LEVEL"]
                        )) . " $<br/>";
                        $msg .= localize("Votre Jet de Maitrise de la magie est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SKILL"] . " $<br/>";
                        if ($param["GUILD_BONUS"] != 0)
                            $msg .= " dont un bonus de +" . $param["GUILD_BONUS"] . " car vous avez payé " . $param["PRICE"] . " PO pour utiliser les facilités de la guilde des artisans. " . "<br/>";
                        $msg .= localize("Vous réalisez une maitrise d'artisanat de niveau {niv}", array(
                                "niv" => $param["LEVEL_CRAFT"]
                        )) . " $<br/>";
                        if ($param["SUCCESS"] == 0)
                            $msg .= localize("Votre maitrise d'artisanat n'est pas suffisante pour travailler sur une matière première de ce niveau") . " $<br/><br/>";
                        else {
                            if ($param["TOOL"])
                                $msg .= localize("Votre outil de niveau {niv} vous confère un bonus de {bonus}% d'avancement dans la confection", array(
                                        "niv" => $param["TOOL_LEVEL"],
                                        "bonus" => $param["BONUS"]
                                )) . " $<br/>";
                            
                            if ($param["PROGRESS"] == 100)
                                $msg .= localize("Félicitations ! Vous avez terminé d'enchanter {equip} {lev} {name} ", array(
                                        "lev" => $param["EQUIP_LEVEL"],
                                        "equip" => $param["EQUIP_NAME"],
                                        "name" => $param["TEMPLATE_NAME"]
                                )) . " $<br/><br/>";
                            else
                                $msg .= localize("Vous avancez de {vit}% dans l'enchantement de {equip} {lev} {name} ", array(
                                        "vit" => $param["SPEED"],
                                        "lev" => $param["EQUIP_LEVEL"],
                                        "equip" => $param["EQUIP_NAME"],
                                        "name" => $param["TEMPLATE_NAME"]
                                )) . " $<br/>";
                        }
                        break;
                    // On continue un template mineur on majeur
                    case 5:
                        $msg .= localize("L'enchantement à poursuivre est de niveau {niv} ", array(
                                "niv" => $param["TEMPLATE_LEVEL"]
                        )) . " $<br/>";
                        $msg .= localize("Votre Jet de Maitrise de la magie est de ");
                        $msg .= "$.$.$.$.$.$.$.$.$.$.$.$.: $$$" . $param["PLAYER_SKILL"] . " $<br/>";
                        if ($param["GUILD_BONUS"] != 0)
                            $msg .= " dont un bonus de +" . $param["GUILD_BONUS"] . " car vous avez payé " . $param["PRICE"] . " PO pour utiliser les facilités de la guilde des artisans. " . "<br/>";
                        $msg .= localize("Vous réalisez une maitrise d'artisanat de niveau {niv}", array(
                                "niv" => $param["LEVEL_CRAFT"]
                        )) . " $<br/>";
                        if ($param["SUCCESS"] == 0)
                            $msg .= localize("Votre maitrise d'artisanat n'est pas suffisante pour travailler sur une matière première de ce niveau") . " $<br/><br/>";
                        else {
                            if ($param["TOOL"])
                                $msg .= localize("Votre outil de niveau {niv} vous confère un bonus de {bonus}% d'avancement dans la confection", array(
                                        "niv" => $param["TOOL_LEVEL"],
                                        "bonus" => $param["BONUS"]
                                )) . " $<br/>";
                            
                            if ($param["PROGRESS"] == 100)
                                $msg .= localize("Félicitations ! Vous avez terminé d'enchanter {equip} {lev}", array(
                                        "lev" => $param["EQUIP_LEVEL"],
                                        "equip" => $param["EQUIP_NAME"]
                                )) . " $<br/><br/>";
                            else {
                                $msg .= localize("Vous avancez de {vit}% dans l'enchantement {equip} {lev}", array(
                                        "vit" => $param["SPEED"],
                                        "lev" => $param["EQUIP_LEVEL"],
                                        "equip" => $param["EQUIP_NAME"]
                                )) . " $<br/>";
                                $msg .= localize("L'enchantement est maintenant à {vit}% d'avancement ", array(
                                        "vit" => $param["PROGRESS"]
                                )) . " $<br/><br/>";
                            }
                        }
                }
                
                if ($param["BROKEN"])
                    $msg .= localize("Votre arcane d'enchantement niveau {niv} était trop usée, il s'est cassé", array(
                            "niv" => $param["TOOL_LEVEL"]
                    )) . " $<br/><br/>";
            }
            $msg .= localize("Cette action vous a coûté {ap} PA", array(
                    "ap" => $paramability["AP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
        } else {
            $obj = new ErrorMsg();
            $msg = $obj->msgBuilder($error, $param);
        }
        
        return $msg;
    }
}

?>
