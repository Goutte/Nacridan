<?php
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
require_once (HOMEPATH . "/lib/utils.inc.php");

class CQInfo extends HTMLObject
{

    public $nacridan;

    public $curplayer;

    public $db;

    public function CQInfo($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function getAskJoin($curplayer, $db)
    {
        $dbm = new DBCollection("SELECT count(id) AS count FROM TeamAskJoin WHERE id_Team=" . $curplayer->get("id_Team"), $db);
        
        $str = "";
        $add = "";
        if ($dbm->get("count") > 0) {
            $add = "!!!";
            $str .= localize('Nouveau(x) Demandeur(s) voulant rejoindre votre Ordre') . " : <b>" . $dbm->get("count") . " " . $add . "</b><br/>";
        }
        return $str;
    }

    public function toString()
    {
        $db = $this->db;
        $sess = $this->nacridan->sess;
        $auth = $this->nacridan->auth;
        
        $dbm = new DBCollection("SELECT Player.id_BasicRace, Player.id,Player.ap, Player.nextatb, Player.state, Player.inbuilding, Player.overload, count(Mail.id) AS count FROM Player LEFT JOIN Mail ON id_Player\$receiver = Player.id AND Mail.new=1 WHERE id_Member =" . $auth->auth["uid"] . " GROUP BY Player.id ORDER BY Player.id", $db);
        $dbmch = new DBCollection("SELECT Player.id,count(MailTrade.id) AS count FROM Player LEFT JOIN MailTrade ON id_Player\$receiver = Player.id AND MailTrade.new=1 WHERE id_Member =" . $auth->auth["uid"] . " GROUP BY Player.id ORDER BY Player.id", $db);
        $dbm2 = new DBCollection("SELECT Player.id,count(TeamAskJoin.id) AS count FROM Player LEFT JOIN Team ON Team.id_Player=Player.id LEFT JOIN TeamAskJoin ON TeamAskJoin.id_Team=Team.id WHERE id_Member =" . $auth->auth["uid"] . " GROUP BY Player.id ORDER BY Player.id", $db);
        $dbmalrt = new DBCollection("SELECT Player.id,count(MailAlert.id) AS count FROM Player LEFT JOIN MailAlert ON id_Player\$receiver = Player.id AND MailAlert.new=1 WHERE id_Member =" . $auth->auth["uid"] . " GROUP BY Player.id ORDER BY Player.id", $db);
        
        $players = $this->nacridan->loadSessPlayers();
        
        $i = 0;
        
		$str = displayActivePoll($auth->auth["uid"],$db);
		
        $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act" . "' target='_self'>";
        $button = 0;
        
        while (! $dbm->eof()) {
            $time = gmstrtotime($dbm->get("nextatb")) - date("I") * 3600;
            $curDate = (time());
            $activer = "";
            if ($sess->has("DLA" . $dbm->get("id"))) {
                $ap = $dbm->get("ap");
                if ($curDate > $time) {
                    if ($this->curplayer->get("id") == $dbm->get("id")) {
                        $activer = localize("Activer?&nbsp;&nbsp;&nbsp;&nbsp;") . "<input name='check" . $i . "' type='checkbox' value=" . $dbm->get("id") . ">";
                        $button = 1;
                    } else {
                        $activer = localize("DLA activable");
                    }
                }
            } else {
                $ap = localize("(session restreinte)");
                $activer = localize("Activer? &nbsp;&nbsp;&nbsp;&nbsp;") . "<input name='check" . $i . "' type='checkbox' value=" . $dbm->get("id") . ">";
                $button = 1;
            }
            $i ++;
            $str .= "<br/>\n";
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr><td colspan='3' class='mainbgtitle'><b>" . $players[$dbm->get("id")] . "</b></td></tr>";
            
            $day = localizedday(date("j M Y", $time));
            
            $hourstr = date("Y-m-d H:i:s", $time);
            
            // list($day,$hour)=preg_split(" ",$str);
            list ($daytmp, $hour) = explode(" ", $hourstr);
            
            if ($dbm->get("id_BasicRace") != 263 && $dbm->get("id_BasicRace") != 112) {
                $str .= "<tr><td class='mainbglabel cqinfotablecol1size'>&nbsp; Prochaine DLA  </b></td><td class='mainbgbody'><b>" . $day . ", " . $hour . "</b></td><td>&nbsp;</td></tr>";
                $str .= "<tr><td class='mainbglabel cqinfotablecol1size'> &nbsp; " . localize('Nombre de PA disponible(s)') . "</td><td class='mainbgbody' width='40%'><b>" . $ap . "</b></td><td>" . $activer . "</td></tr>";
            }
            $dbv = new DBCollection("SELECT * FROM Equipment WHERE id_Player=" . $dbm->get("id") . " AND weared='YES' AND durability=0", $db);
            if ($dbv->count() == 1)
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention ce personnage porte un équipement cassé !') . "</b></td></tr>";
            if ($dbv->count() > 1)
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention ce personnage porte plusieurs équipements cassés !') . "</b></td></tr>";
            
            if ($dbm->get("state") == "creeping")
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention ce personnage est entravé par un bolas !!!') . "</b></td>";
            if ($dbm->get("overload") == 1)
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention ce personnage est en surcharge ! Il subira un malus de la moitié de ses PAs à la prochaine dla') . "</b></td></tr>";
            
            if ($dbm->get("overload") == 2)
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention ce personnage est en lourde surcharge ! Il subira un malus des 3/4 de ses PAs à la prochaine dla') . "</b></td></tr>";
            
            $dbt = new DBCollection("SELECT Player.state FROM Player LEFT JOIN Caravan ON Caravan.id=Player.id_Caravan WHERE Caravan.id_Player=" . $dbm->get("id"), $db);
            if ($dbt->count() > 0 && $dbt->get("state") == "creeping")
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention votre tortue est entravée par un bolas !') . " </b></td></tr>";
            
            $playerSrc = new Player();
            $playerSrc->load($dbm->get("id"), $db);
            if ((PlayerFactory::getIdArm($playerSrc, $db, 1) != 0) && ($dbm->get("inbuilding") != 0))
                $str .= "<tr><td  colspan='3' class='mainbgbody cqinfotablecol1size'><b>&nbsp; " . localize('Attention, ce personnage se trouve dans un bâtiment l\'arme à la main.') . " </b></td></tr>";
            
            $add = "";
            
            if ($dbm->get("count") > 0) {
                $add = "!!!";
                if ($dbm->get("count") == 1)
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Message non lu') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbm->get("count") . " " . $add . "</b></td></tr>";
                else
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Messages non lus') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbm->get("count") . " " . $add . "</b></td></tr>";
            }
            
            if ($dbmalrt->get("count") > 0) {
                $add = "!!!";
                if ($dbmalrt->get("count") == 1)
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Vous avez une alerte en attente') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbmalrt->get("count") . " " . $add . "</b></td></tr>";
                else
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Vous avez des alertes en attentes') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbmalrt->get("count") . " " . $add . "</b></td></tr>";
            }
            
            if ($dbmch->get("count") > 0) {
                $add = "!!!";
                if ($dbmch->get("count") == 1)
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Message commercial non lu') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbmch->get("count") . " " . $add . "</b></td></tr>";
                else
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Messages commerciaux non lus') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbmch->get("count") . " " . $add . "</b></td></tr>";
            }
            
            if ($dbm2->get("count") > 0) {
                $add = "!!!";
                if ($dbm2->get("count") == 1)
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Nouveau Demandeur voulant rejoindre votre Ordre') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbm2->get("count") . " " . $add . "</b></tr>";
                else
                    $str .= "<tr><td class='mainbglabel'><b>&nbsp; " . localize('Nouveau(x) Demandeur(s) voulant rejoindre votre Ordre') . "</b></td><td colspan='2' class='mainbgbody'><b>" . $dbm2->get("count") . " " . $add . "</b></tr>";
            }
            
            $str .= "</table>";
            $dbm->next();
            $dbmch->next();
            $dbmalrt->next();
            $dbm2->next();
        }
        
        if ($button) {
            $str .= "<table class='maintable centerareasize'>";
            $str .= "<tr><td colspan='3' align='right'><input name='submitbt' type='submit' value='" . localize("Activer") . "'><input name='action' type='hidden' value='" . ATBON . "'></td></tr>";
            $str .= "</table>";
        }
        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        $str .= "</form>";
        
        $dbplayer = new DBCollection("SELECT Player.id,racename FROM Player WHERE status='PC' AND id_Member =" . $auth->auth["uid"], $db);
        $dbmax = new DBCollection("SELECT maxNbPlayers FROM Member WHERE id =" . $auth->auth["uid"], $db);
        if ($dbplayer->count() < $dbmax->get("maxNbPlayers")) {
            $str .= "<form name='form'  method='POST' action='" . CONFIG_HOST . "/main/newcharacter.php' target='_self'>";
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr><td class='mainbglabel'><input name='submitbt' type='submit' value='Créer un personnage supplémentaire'>";
            $str .= "<input name='step' type='hidden' value='0' />\n";
            $str .= "</td></tr>";
            $str .= "</table>";
            $str .= "</form>";
        } elseif ($dbmax->get("maxNbPlayers") < 2) {
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr><td class='mainbglabel cqinfotablecol1size'><b>Soutenez l'association :</b> ";
            $str .= "</td> <td class='mainbgbody'> Obtenez un <a href='" . CONFIG_HOST . "/gg/ggsite/index.php?page=shop.ShopHome' class='stylepc' onclick='window.open(this.href); return false;'>personnage supplémentaire</a> ! </td></tr>";
            $str .= "</table>";
        }
        
        $dboption = new DBCollection("SELECT music FROM MemberOption WHERE id_Member =" . $auth->auth["uid"], $db);
        $music = $dboption->get("music");
        if ($music == 1) {
            $race = $this->curplayer->get("racename");
            $city = "Artasse";
            $playlist = getPlaylist();
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr><td class='mainbglabel cqinfotablecol1size'>Lecteur de musique <b>activé</b> !";
            $str .= "</td></tr></table>";
            $str .= "<script type='text/javascript' src='" . CONFIG_HOST . "/musicplayer/script.js' 
data-config='{\"skin\":\"skins/tunes/skin.css\",\"volume\":57,\"autoplay\":true,\"shuffle\":false,\"repeat\":1,\"placement\":\"top\",\"showplaylist\":false,\"playlist\":" . $playlist . "}' ></script>";
        } else {
            $str .= "<table class='maintable centerareawidth'>";
            $str .= "<tr><td class='mainbglabel cqinfotablecol1size'>Lecteur de musique <b>désactivé</b> !";
            $str .= "</td></tr></table>";
        }
        
        return $str;
    }

    public function render()
    {
        echo $this->toString();
    }
}
?>
