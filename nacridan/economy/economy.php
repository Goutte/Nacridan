<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/include/game.inc.php");
require_once (HOMEPATH . '/economy/einfo.inc.php');

require_once (HOMEPATH . '/main/mmenu.inc.php');
require_once (HOMEPATH . '/diplomacy/dtribune.inc.php');
$MAIN_BODY->add(new CQMMenu($nacridan, $db, $lang));

if ($curplayer->get("disabled") == 0) {
    if ($curplayer->get("authlevel") < 2) {
        require_once ('emenu.inc.php');
        $MAIN_BODY->add(new EMenu($nacridan, $db));
    }
    
    require_once (HOMEPATH . '/conquest/cqmenubt.inc.php');
    $MAIN_BODY->add(new CQMenuBT());
    
    $MAIN_BOTTOM = $MAIN_BODY->addNewHTMLObject("div", "", "class='bottomarea'");
    $btobj = "";
    
    if (isset($bottom)) {
        switch ($bottom) {
            case "sale":
                require_once (HOMEPATH . "/economy/esale.inc.php");
                $btobj = new ESale($nacridan, $db);
                break;
            case "buy":
                require_once (HOMEPATH . "/economy/ebuy.inc.php");
                $btobj = new EBuy($nacridan, $db);
                break;
            case "salehorse":
                require_once (HOMEPATH . "/economy/esalehorse.inc.php");
                $btobj = new ESaleHorse($nacridan, $db);
                break;
            case "buyhorse":
                require_once (HOMEPATH . "/economy/ebuyhorse.inc.php");
                $btobj = new EBuyHorse($nacridan, $db);
                break;
            case "ownbuilding":
                require_once (HOMEPATH . "/economy/eownbuilding.inc.php");
                $btobj = new EOwnBuilding($nacridan, $db);
                break;
            
            default:
                break;
        }
    }
    
    $MAIN_BOTTOM->add($btobj);
}
require_once ('../conquest/cqright.inc.php');
$MAIN_BODY->add(new CQRight($nacridan, $db));

$str = "</div>";
$MAIN_BODY->add($str);

if ($curplayer->get("disabled") == 0) {
    $centerobj = new EInfo($nacridan, $db);
} else {
    require_once (HOMEPATH . '/conquest/cqbanned.inc.php');
    $centerobj = new CQBanned($nacridan, $curplayer->get("id"), $db);
}

$MAIN_CENTER = $MAIN_BODY->addNewHTMLObject("div", "", "class='centerarea'");
$MAIN_CENTER->add($centerobj);

/*
 * $google="<script src='http://www.google-analytics.com/urchin.js' type='text/javascript'>
 * </script>
 * <script type='text/javascript'>
 * _uacct = 'UA-1166023-1';
 * urchinTracker();
 * </script>";
 * //$MAIN_BODY->add($pub);
 *
 * $MAIN_BODY->add($google);
 *
 *
 * if($curplayer->get("incity"))
 * {
 * $MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='".CONFIG_HOST."/css/nacridanCity".$lang.".css'>\n");
 * }
 * else
 * {
 * $MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='".CONFIG_HOST."/css/nacridan".$lang.".css'>\n");
 * }
 *
 * //$MAIN_BODY->add("<script src='".CONFIG_HOST."/javascript/astrack.js' type='text/javascript'></script>\n");
 * $MAIN_BODY->add("<script type=\"text/javascript\">
 * <!--
 * clickaider_cid = \"900685fa-497\";
 * // -->
 * </script>
 * <script id=\"clickaiderscript\" type=\"text/javascript\" src=\"http://hit.clickaider.com/clickaider.js\"></script>
 * <noscript>
 * <img src=\"http://hit.clickaider.com/pv?c=900685fa-497\" alt=\"ClickAider\" border=\"0\" width=\"1\" height=\"1\" />
 * </noscript>");
 *
 */

$MAIN_PAGE->render();
Translation::saveMessages();

// profiler_stop("start");
// $prof->printTimers(PROFILER_MODE);
?>
