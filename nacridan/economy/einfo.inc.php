<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");

class EInfo extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function EInfo($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $err = "";
        
        $str = "<table class='maintable centerareawidth'>";
        $str .= "<tr><td class='mainbgtitle'>";
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>";
        $str .= "</tr>";
        $str .= "</table>";
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize('É C O N O M I E') . "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='mainerror'>" . $err . "</span>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "<tr class='mainbgtitle'><td height='40px' valign='center'><b>Banque</b>  : votre compte contient " . $curplayer->get('moneyBank') .
             " PO. Vous devez être dans une banque pour le gérer.</td></tr>";
        
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'>";
        $str .= "<a href='../economy/economy.php?view=1' class='tabmenu'>" . localize('Les Objets en Vente') . "</a> |";
        $str .= "<a href='../economy/economy.php?view=2' class='tabmenu'>" . localize('Votre Liste de Vente') . "</a> |";
        $str .= "<a href='../economy/economy.php?view=3' class='tabmenu'>" . localize('Vos bâtiments') . "</a> |";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= " </table>";
        
        if (isset($_POST["remove"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                if (isset($_POST["check"]))
                    EquipFactory::removeEquipFromForSale($curplayer, quote_smart($_POST["check"]), $err, $db);
            }
        } 

        elseif (isset($_POST["submitbt2"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                EquipFactory::buySeveralEquipment($curplayer, $_POST["ID_EQUIP"], $_POST["ID_SELLER"], $err, $db);
            }
        } 

        elseif (isset($_POST["submitbt3"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
                EquipFactory::revokeBuilding($curplayer, $_POST["ID_BUILDING"], $err, $db);
            }
        }
        
        if ($err == "") {
            if (isset($_POST["buy"]) && isset($_POST["check"])) {
                $str .= "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='600px'>";
                
                if ($curplayer->get("ap") < BUY_AP) {
                    $str .= "<tr><td> Vous n'avez pas assez de Points d'Action (PA) pour acheter un objet.";
                    $str .= "</td></tr></table>";
                } else {
                    $where = EquipFactory::getCondFromArray($_POST["check"], "Equipment.id", "OR");
                    $dbe = new DBCollection("SELECT * FROM Equipment WHERE " . $where, $db);
                    $price = 0;
                    $id_Player = $dbe->get("id_Player");
                    $samePlayer = 1;
                    while (! $dbe->eof()) {
                        echo $dbe->get("id") . "-";
                        $price += $dbe->get("sell");
                        if ($id_Player != $dbe->get("id_Player"))
                            $samePlayer = 0;
                        $dbe->next();
                    }
                    
                    if (! $samePlayer)
                        $str .= "<tr><td class='mainbglabel'>Vous ne pouvez pas acheter des objets appartement à de différentes personnes en même temps.</td></tr>";
                    
                    elseif ($price > $curplayer->get("money"))
                        $str .= "<tr><td class='mainbglabel'>Vous ne possédez pas suffisament d'argent pour acheter tous ces objets.</td></tr>";
                    
                    else {
                        $dbe->first();
                        if ($dbe->count() > 1)
                            $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point d'acheter ces objets :<b></td></tr>";
                        else
                            $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point d'acheter cet objet :<b></td></tr>";
                        
                        while (! $dbe->eof()) {
                            $str .= "<tr><td class='mainbglabel'>" . $dbe->get("name") . " " . $dbe->get("extraname") . " </b>pour " . $dbe->get("sell") . " PO ?";
                            $str .= "</tr></td>";
                            // $idEquip.=$dbe->get("id").",";
                            $dbe->next();
                        }
                        
                        if ($dbe->count() > 1)
                            $str .= "<tr><td class='mainbglabel'>Il vous en coutera un total de " . $price . " PO";
                        $str .= "<tr><td><input id='submitbt2' type='submit' name='submitbt2' value='Continuer' /> </td></tr>";
                        $dbe->first();
                        $str .= "<input name='ID_SELLER' type='hidden' value='" . $dbe->get("id_Player") . "' />";
                        $str .= "<input name='ID_EQUIP' type='hidden' value='" . $where . "' />";
                    }
                }
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</table></form>";
            } elseif (isset($_POST["revoke"]) && isset($_POST["check"])) {
                $str .= "<form name='form'  method='POST' target='_self'>";
                $str .= "<table class='maintable'><tr><td class='mainbgtitle' width='600px'>";
                
                if ($curplayer->get("ap") < 0) {
                    $str .= "<tr><td> Vous n'avez pas assez de Points d'Action (PA) pour révoquer un bâtiment.";
                    $str .= "</td></tr></table>";
                } else {
                    $where = EquipFactory::getCondFromArray($_POST["check"], "Building.id", "OR");
                    $dbe = new DBCollection("SELECT * FROM Building WHERE " . $where, $db);
                    $id_Player = $dbe->get("id_Player");
                    if ($dbe->count() > 1)
                        $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point de révoquer ces bâtiments :<b></td></tr>";
                    else
                        $str .= "<tr><td class='mainbglabel'>Vous êtes sur le point de révoquer ce bâtiment :<b></td></tr>";
                    
                    while (! $dbe->eof()) {
                        $str .= "<tr><td class='mainbglabel'>" . $dbe->get("name") . " de niveau " . $dbe->get("level") . " (" . $dbe->get("x") . "," . $dbe->get("y") . ") ?";
                        $str .= "</tr></td>";
                        $dbe->next();
                    }
                    
                    $str .= "<tr><td><input id='submitbt3' type='submit' name='submitbt3' value='Continuer' /> </td></tr>";
                    $dbe->first();
                    $str .= "<input name='ID_BUILDING' type='hidden' value='" . $where . "' />";
                }
                
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</table></form>";
            } else {
                
                $str .= "<div class='centerareawidth' >";
                
                // ------------------------------------- VOTRE LISTE DE VENTE
                
                if (isset($_GET["view"]) && $_GET["view"] == 2) {
                    $str .= "<form method='POST' action='" . CONFIG_HOST . '/economy/economy.php?view=2' . "' target='_self'>\n";
                    $eq = new Equipment();
                    $eqmodifier = new Modifier();
                    $modifier = new Modifier();
                    $mission = new Mission();
                    
                    $xp = $curplayer->get("x");
                    $yp = $curplayer->get("y");
                    $id = $curplayer->get("id");
                    
                    $dbe = new DBCollection(
                        "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                             $mission->getASRenamer("Mission", "MISS") .
                             ",mask,wearable,BasicEquipment.durability AS dur  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE Equipment.sell!=0 AND Equipment.id_Player=" .
                             $id, $db);
                    
                    $data = array();
                    $dist = array();
                    
                    while (! $dbe->eof()) {
                        $eqmodifier = new Modifier();
                        $eq->DBLoad($dbe, "EQ");
                        $eqmodifier->DBLoad($dbe, "EQM");
                        $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                        
                        if ($eq->get("extraname") != "") {
                            $tmodifier = new Modifier();
                            $template = new Template();
                            $dbt = new DBCollection(
                                "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                                     $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                                     " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                                     $eq->get("id") . " order by Template.pos asc", $db);
                            while (! $dbt->eof()) {
                                $template->DBLoad($dbt, "TP");
                                $tmodifier->DBLoad($dbt, "MD");
                                $tmodifier->updateFromTemplateLevel($template->get("level"));
                                $eqmodifier->addModifObj($tmodifier);
                                $dbt->next();
                            }
                        }
                        $eqmodifier->initCharacStr();
                        
                        $id_BasicEquipment = $eq->get("id_BasicEquipment");
                        
                        if ($eq->get("id_EquipmentType") == 45) {
                            $missname = localize($dbe->get("MISSname"));
                            $eq->set("name", 
                                "<a href=\"../conquest/parchment.php?id=" . $eq->get("id") . "\" class='parchment popupify'>" . localize($eq->get("name")) . "</a>");
                        } else {
                            $missname = "";
                        }
                        
                        $data[] = array(
                            "id_BasicEquipment" => $id_BasicEquipment,
                            "equip" => clone ($eq),
                            "eqmodif" => clone ($eqmodifier),
                            "eqtpl" => clone ($modifier),
                            "missname" => $missname
                        );
                        $dbe->next();
                    }
                    
                    $nbequip = count($data);
                    $forsell = array();
                    $forsell2 = array();
                    
                    for ($i = 0; $i < $nbequip; $i ++) {
                        $col1 = "<input type='checkbox' name='check[]' value='" . $data[$i]["equip"]->get("id") . "'>";
                        $col2 = $data[$i]["equip"]->get("id");
                        $col3 = $data[$i]["equip"]->get("level");
                        /*
                         * if($data[$i]["equip"]->get("id_BasicEquipment") == 600)
                         * {
                         * $col4.="<a href=javascript:profile(\"../conquest/parchment.php?type=caravan&id=".$data[$i]["equip"]->get("id")."\") class='parchment'>".localize($data[$i]["equip"]->get("name"))."</a>";
                         *
                         * if($data[$i]["missname"] != "")
                         * $col4.="<br/>".localize($data[$i]["missname"]);
                         * //elseif($dbe->get("CARid") != "")
                         * //$str.="<br/> Mission commerciale";
                         * }
                         */
                        
                        $col4 = "<span class='" . ($data[$i]["equip"]->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($data[$i]["equip"]->get("name")) . "</span>" . " " .
                             $data[$i]["missname"];
                        if ($data[$i]["equip"]->get("extraname") != "")
                            $col4 .= " <span class='template'>" . localize($data[$i]["equip"]->get("extraname")) . "</span>";
                            
                            // $col3.="<br/>";
                        if (($data[$i]["equip"]->get("templateProgress") < 100 && $data[$i]["equip"]->get("templateProgress") > 0) or $data[$i]["equip"]->get("progress") < 100) {
                            if ($data[$i]["equip"]->get("templateProgress") < 100 && $data[$i]["equip"]->get("templateProgress") > 0)
                                $col4 .= " - Confection : " . $data[$i]["equip"]->get("templateProgress") . "%";
                            
                            if ($data[$i]["equip"]->get("progress") < 100)
                                $col4 .= " - Confection : " . $data[$i]["equip"]->get("progress") . "%";
                        } else {
                            if ($data[$i]["equip"]->get("id_EquipmentType") < 28 or
                                 ($data[$i]["equip"]->get("id_EquipmentType") >= 40 and $data[$i]["equip"]->get("id_EquipmentType") <= 44) or
                                 $data[$i]["equip"]->get("id_EquipmentType") == 46)
                                $col4 .= " - Etat : " . EquipFactory::getDurability($data[$i]["equip"], $db);
                            
                            $col4 .= "<br/>";
                            if ($data[$i]["equip"]->get("id_EquipmentType") < 30) {
                                $modstr = "";
                                foreach ($data[$i]["eqmodif"]->m_characLabel as $key) {
                                    $tmp = $data[$i]["eqmodif"]->getModifStr($key);
                                    if ($tmp != "0")
                                        $modstr .= translateAttshort($key) . " : " . $tmp . " | ";
                                }
                                $col4 .= "(" . substr($modstr, 0, - 3) . ")";
                            }
                        }
                        $col5 = $data[$i]["equip"]->get("sell");
                        
                        $forsell[] = array(
                            "0" => array(
                                $col1,
                                "class='mainbgbody' align='left'"
                            ),
                            "1" => array(
                                $col2,
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $col3,
                                "class='mainbgbody' align='left'"
                            ),
                            "3" => array(
                                $col4,
                                "class='mainbgbody' align='left'"
                            ),
                            "4" => array(
                                $col5,
                                "class='mainbgbody' align='right'"
                            )
                        );
                    }
                    
                    if (count($forsell) > 0) {
                        $str .= createTable(5, $forsell, 
                            array(
                                array(
                                    localize(
                                        "Remarque : votre liste de vente est affichée dans les panneaux d'affichage de toutes les auberges de Nacridan, pour que les acheteurs potentiels puissent vous contacter facilement. En cas de vente à distance, pensez au service de transport d'objets dans les comptoirs commerciaux. <br/><br/> Votre liste de vente :"),
                                    "class='mainbgtitle' colspan=5 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    "",
                                    "class='mainbglabel' width='5%' align='center'"
                                ),
                                array(
                                    localize("Id"),
                                    "class='mainbglabel' width='10%' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='10%' align='left'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel'  align='left'"
                                ),
                                array(
                                    localize("Prix"),
                                    "class='mainbglabel' width='15%' align='right'"
                                )
                            ), "class='maintable centerareawidth'");
                        
                        $str .= "<input id='remove' type='submit' name='remove' value='" . localize("Supprimer de la liste") . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    }
                    
                    $str .= "</form>";
                }
                
                // -------------------------------------- LISTE DES OBJETS EN VENTE
                
                if (isset($_GET["view"]) && $_GET["view"] == 1) {
                    $str .= "<form method='POST' action='" . CONFIG_HOST . '/economy/economy.php?view=2' . "' target='_self'>\n";
                    $eq = new Equipment();
                    $modifier = new Modifier();
                    $eqmodifier = new Modifier();
                    $mission = new Mission();
                    $xp = $curplayer->get("x");
                    $yp = $curplayer->get("y");
                    $map = $curplayer->get("map");
                    $id = $curplayer->get("id");
                    
                    $dbe = new DBCollection(
                        "SELECT Player.id, Player.name, Player.x, Player.y, Player.map, " . $eq->getASRenamer("Equipment", "EQ") . "," .
                             $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," . $mission->getASRenamer("Mission", "MISS") .
                             ",mask,wearable,BasicEquipment.durability AS dur  FROM Player LEFT JOIN Equipment ON Equipment.id_Player=Player.id LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE Equipment.sell!=0 AND Player.map=" .
                             $map . " AND Player.x<=" . SIZEAREA . "+" . $xp . " AND Player.x>=-" . SIZEAREA . "+" . $xp . " AND Player.y<=" . SIZEAREA . "+" . $yp .
                             " AND Player.y>=-" . SIZEAREA . "+" . $yp . " AND Player.id!=" . $id, $db);
                    
                    $data = array();
                    $dist = array();
                    while (! $dbe->eof()) {
                        $eqmodifier = new Modifier();
                        $eq->DBLoad($dbe, "EQ");
                        $eqmodifier->DBLoad($dbe, "EQM");
                        $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                        
                        $xx = $dbe->get("x");
                        $yy = $dbe->get("y");
                        
                        $distVal = distHexa($xp, $yp, $xx, $yy);
                        $dist[] = $distVal;
                        
                        $eqmodifier->initCharacStr();
                        
                        $id_BasicEquipment = $eq->get("id_BasicEquipment");
                        
                        if ($eq->get("id_EquipmentType") == 45) {
                            $missname = localize($dbe->get("MISSname"));
                            $eq->set("name", 
                                "<a href=\"../conquest/parchment.php?id=" . $eq->get("id") . "\" class='parchment popupify'>" . localize($eq->get("name")) . "</a>");
                        } else {
                            $missname = "";
                        }
                        
                        $data[] = array(
                            "id_BasicEquipment" => $id_BasicEquipment,
                            "xx" => $xx,
                            "yy" => $yy,
                            "dist" => $distVal,
                            "name" => $dbe->get("name"),
                            "id" => $dbe->get("id"),
                            "equip" => clone ($eq),
                            "eqmodif" => clone ($eqmodifier),
                            "eqtpl" => clone ($modifier),
                            "missname" => $missname
                        );
                        $dbe->next();
                    }
                    
                    array_multisort($dist, SORT_ASC, $data);
                    
                    $nbequip = count($data);
                    $inzone = array();
                    $outzone = array();
                    
                    for ($i = 0; $i < $nbequip; $i ++) {
                        // $col1="<input type='checkbox' name='check[]' value='".$data[$i]["equip"]->get("id")."'>";
                        $checkbox = "<input type='checkbox' name='check[]' value='" . $data[$i]["equip"]->get("id") . "'>";
                        $col1 = $data[$i]["equip"]->get("id");
                        $col2 = $data[$i]["equip"]->get("level");
                        /*
                         * if($data[$i]["equip"]->get("id_BasicEquipment") == 600)
                         * {
                         * $col3="<a href=javascript:profile(\"../conquest/parchment.php?type=caravan&id=".$data[$i]["equip"]->get("id")."\") class='parchment'>".localize($data[$i]["equip"]->get("name"))."</a>";
                         *
                         * if($data[$i]["missname"] != "")
                         * $col3.="<br/>".localize($data[$i]["missname"]);
                         * //elseif($dbe->get("CARid") != "")
                         * //$str.="<br/> Mission commerciale";
                         * }
                         */
                        $col3 = "<span class='" . ($data[$i]["equip"]->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($data[$i]["equip"]->get("name")) . "</span>" . " " .
                             $data[$i]["missname"];
                        if ($data[$i]["equip"]->get("extraname") != "")
                            $col3 .= " <span class='template'>" . localize($data[$i]["equip"]->get("extraname")) . "</span>";
                            
                            // $col3.="<br/>";
                        if (($data[$i]["equip"]->get("templateProgress") < 100 && $data[$i]["equip"]->get("templateProgress") > 0) or $data[$i]["equip"]->get("progress") < 100) {
                            if ($data[$i]["equip"]->get("templateProgress") < 100 && $data[$i]["equip"]->get("templateProgress") > 0)
                                $col3 .= " - Confection : " . $data[$i]["equip"]->get("templateProgress") . "%";
                            
                            if ($data[$i]["equip"]->get("progress") < 100)
                                $col3 .= " - Confection : " . $data[$i]["equip"]->get("progress") . "%";
                        } else {
                            if ($data[$i]["equip"]->get("id_EquipmentType") < 28 or
                                 ($data[$i]["equip"]->get("id_EquipmentType") >= 40 and $data[$i]["equip"]->get("id_EquipmentType") <= 44) or
                                 $data[$i]["equip"]->get("id_EquipmentType") == 46)
                                $col3 .= " - Etat : " . EquipFactory::getDurability($data[$i]["equip"], $db);
                            
                            $col3 .= "<br/>";
                            if ($data[$i]["equip"]->get("id_EquipmentType") < 30) {
                                $modstr = "";
                                foreach ($data[$i]["eqmodif"]->m_characLabel as $key) {
                                    $tmp = $data[$i]["eqmodif"]->getModifStr($key);
                                    if ($tmp != "0")
                                        $modstr .= translateAttshort($key) . " : " . $tmp . " | ";
                                }
                                $col3 .= "(" . substr($modstr, 0, - 3) . ")";
                            }
                        }
                        
                        $col4 = "<a href=\"../conquest/profile.php?id=" . $data[$i]["id"] . "\" class='stylepc popupify'>" . $data[$i]["name"] . "</a>";
                        $col5 = $data[$i]["equip"]->get("sell");
                        
                        if ($data[$i]["dist"] < 2) {
                            $inzone[] = array(
                                "0" => array(
                                    $checkbox,
                                    "class='mainbgbody' align='left'"
                                ),
                                "1" => array(
                                    $col1,
                                    "class='mainbgbody' align='left'"
                                ),
                                "2" => array(
                                    $col2,
                                    "class='mainbgbody' align='left'"
                                ),
                                "3" => array(
                                    $col3,
                                    "class='mainbgbody' align='left'"
                                ),
                                "4" => array(
                                    $col4,
                                    "class='mainbgbody' align='left'"
                                ),
                                "5" => array(
                                    $col5,
                                    "class='mainbgbody' align='right'"
                                )
                            );
                        } else {
                            $outzone[] = array(
                                "0" => array(
                                    $col1,
                                    "class='mainbgbody' align='left'"
                                ),
                                "1" => array(
                                    $col2,
                                    "class='mainbgbody' align='left'"
                                ),
                                "2" => array(
                                    $col3,
                                    "class='mainbgbody' align='left'"
                                ),
                                "3" => array(
                                    $col4,
                                    "class='mainbgbody' align='left'"
                                ),
                                "4" => array(
                                    $col5,
                                    "class='mainbgbody' align='right'"
                                )
                            );
                        }
                    }
                    
                    if (count($inzone) > 0) {
                        $str .= createTable(6, $inzone, 
                            array(
                                array(
                                    localize("En vente à côté de vous"),
                                    "class='mainbgtitle' colspan=6 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    localize(""),
                                    "class='mainbglabel' width='60px' align='left'"
                                ),
                                array(
                                    localize("Id"),
                                    "class='mainbglabel' width='60px' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='50px' align='left'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel'  align='left'"
                                ),
                                array(
                                    localize("Proriétaire"),
                                    "class='mainbglabel' width='180px' align='left'"
                                ),
                                array(
                                    localize("Prix"),
                                    "class='mainbglabel' width='50px' align='right'"
                                )
                            ), "class='maintable centerareawidth'");
                    }
                    if (count($outzone) > 0) {
                        $str .= createTable(5, $outzone, 
                            array(
                                array(
                                    localize("En vente dans les alentours"),
                                    "class='mainbgtitle' colspan=5 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    localize("Id"),
                                    "class='mainbglabel' width='60px' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='50px' align='left'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel'  align='left'"
                                ),
                                array(
                                    localize("Proriétaire"),
                                    "class='mainbglabel' width='180px' align='left'"
                                ),
                                array(
                                    localize("Prix"),
                                    "class='mainbglabel' width='50px' align='right'"
                                )
                            ), "class='maintable centerareawidth'");
                    }
                    
                    if (count($inzone) == 0 && count($outzone) == 0) {
                        $err = localize("Aucun objet en vente à proximité.");
                    } elseif (count($inzone) != 0) {
                        $str .= "<input id='buy' type='submit' name='buy' value='" . localize("Acheter") . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    }
                    $str .= "</form>";
                }
                
                // ---------------------------------- LISTE DES BATIMENTS
                
                if (isset($_GET["view"]) && $_GET["view"] == 3) {
                    $str .= "<form method='POST' action='" . CONFIG_HOST . '/economy/economy.php?view=3' . "' target='_self'>\n";
                    $xp = $curplayer->get("x");
                    $yp = $curplayer->get("y");
                    $map = $curplayer->get("map");
                    $id = $curplayer->get("id");
                    
                    $dbe = new DBCollection("SELECT * FROM Building WHERE (id_BasicBuilding=27 or id_BasicBuilding=15) AND id_Player=" . $id, $db);
                    $data = array();
                    $dist = array();
                    while (! $dbe->eof()) {
                        $xx = $dbe->get("x");
                        $yy = $dbe->get("y");
                        
                        $distVal = distHexa($xp, $yp, $xx, $yy);
                        $dist[] = $distVal;
                        
                        $data[] = array(
                            "name" => $dbe->get("name"),
                            "level" => $dbe->get("level"),
                            "currsp" => $dbe->get("currsp"),
                            "sp" => $dbe->get("sp"),
                            "xx" => $xx,
                            "yy" => $yy,
                            "dist" => $distVal,
                            "id" => $dbe->get("id")
                        );
                        $dbe->next();
                    }
                    
                    array_multisort($dist, SORT_ASC, $data);
                    
                    $nbb = count($data);
                    $final = array();
                    
                    for ($i = 0; $i < $nbb; $i ++) {
                        
                        $checkbox = "<input type='checkbox' name='check[]' value='" . $data[$i]["id"] . "'>";
                        $col1 = $checkbox;
                        $col2 = $data[$i]["name"];
                        $col3 = $data[$i]["level"];
                        $col4 = $data[$i]["currsp"] . "/" . $data[$i]["sp"];
                        $col5 = $data[$i]["xx"];
                        $col6 = $data[$i]["yy"];
                        
                        $final[] = array(
                            "0" => array(
                                $col1,
                                "class='mainbgbody' align='center'"
                            ),
                            "1" => array(
                                $col2,
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $col3,
                                "class='mainbgbody' align='center'"
                            ),
                            "3" => array(
                                $col4,
                                "class='mainbgbody' align='center'"
                            ),
                            "4" => array(
                                $col5,
                                "class='mainbgbody' align='center'"
                            ),
                            "5" => array(
                                $col6,
                                "class='mainbgbody' align='center'"
                            )
                        );
                    }
                    
                    if (count($final) > 0) {
                        $str .= createTable(6, $final, 
                            array(
                                array(
                                    localize("Bâtiments invoqués"),
                                    "class='mainbgtitle' colspan=6 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    localize("Id"),
                                    "class='mainbglabel' width='20px' align='center'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel' width='200px' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='50px' align='center'"
                                ),
                                array(
                                    localize("Structure"),
                                    "class='mainbglabel' width='50px' align='center'"
                                ),
                                array(
                                    localize("x"),
                                    "class='mainbglabel' width='30px' align='center'"
                                ),
                                array(
                                    localize("y"),
                                    "class='mainbglabel' width='30px' align='center'"
                                )
                            ), "class='maintable centerareawidth'");
                        
                        $str .= "<input id='buy' type='submit' name='revoke' value='" . localize("Révoquer") . "' />";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    }
                    
                    $dbhouse = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=10 AND id_Player=" . $id, $db);
                    while (! $dbhouse->eof()) {
                        
                        $data[] = array(
                            "name" => $dbhouse->get("name"),
                            "level" => $dbhouse->get("level"),
                            "currsp" => $dbhouse->get("currsp"),
                            "sp" => $dbhouse->get("sp"),
                            "xx" => $dbhouse->get("x"),
                            "yy" => $dbhouse->get("y"),
                            "id" => $dbhouse->get("id")
                        );
                        $dbhouse->next();
                    }
                    
                    $nbb = count($data);
                    $final = array();
                    
                    for ($i = 0; $i < $nbb; $i ++) {
                        
                        $col1 = $data[$i]["id"];
                        $col2 = $data[$i]["name"];
                        $col3 = $data[$i]["level"];
                        $col4 = $data[$i]["currsp"] . "/" . $data[$i]["sp"];
                        $col5 = $data[$i]["xx"];
                        $col6 = $data[$i]["yy"];
                        
                        $final[] = array(
                            "0" => array(
                                $col1,
                                "class='mainbgbody' align='left'"
                            ),
                            "1" => array(
                                $col2,
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $col3,
                                "class='mainbgbody' align='center'"
                            ),
                            "3" => array(
                                $col4,
                                "class='mainbgbody' align='center'"
                            ),
                            "4" => array(
                                $col5,
                                "class='mainbgbody' align='center'"
                            ),
                            "5" => array(
                                $col6,
                                "class='mainbgbody' align='center'"
                            )
                        );
                    }
                    
                    if (count($final) > 0) {
                        $str .= createTable(6, $final, 
                            array(
                                array(
                                    localize("Vos maisons"),
                                    "class='mainbgtitle' colspan=6 align='left'"
                                )
                            ), 
                            array(
                                array(
                                    localize("Id"),
                                    "class='mainbglabel' width='30px' align='center'"
                                ),
                                array(
                                    localize("Nom"),
                                    "class='mainbglabel' width='200px' align='left'"
                                ),
                                array(
                                    localize("Niveau"),
                                    "class='mainbglabel' width='50px' align='center'"
                                ),
                                array(
                                    localize("Structure"),
                                    "class='mainbglabel' width='50px' align='center'"
                                ),
                                array(
                                    localize("x"),
                                    "class='mainbglabel' width='30px' align='center'"
                                ),
                                array(
                                    localize("y"),
                                    "class='mainbglabel' width='30px' align='center'"
                                )
                            ), "class='maintable centerareawidth'");
                    } else
                        $err = localize("Aucun bâtiment.");
                    
                    $str .= "</form>";
                }
            }
        }
        
        if ($err != "") {
            $str .= "<table class='maintable centerareawidth' >\n";
            $str .= "<tr><td class='mainbgtitle' >" . $err . "</td></tr></table>";
        }
        
        // $str.=" </div>";
        return $str;
    }
}
?>
