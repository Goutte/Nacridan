$(document).ready(function() {

    // Params
    // Time in seconds to check new messages
    var checkNewMessagesTime = 30;

    // Default scroll to bottom or last read message
    var content = $('#scrollable_content')[0];
    var unread = $('.messageUnread');
    if (unread.length) {
        $(content).animate({
            scrollTop: unread.position().top
        }, 450);
    } else {
        $(content).scrollTop(content.scrollHeight);
    }

    // Default focus to textarea
    var textarea = $('textarea#Body');
    textarea.focus();

    // Submit form on CTRL+ENTER
    textarea.keydown(function(event){
        if (event.which == 13 && event.ctrlKey) {
            event.preventDefault();
            $('#tribune_form').submit();
        }
    });

    // Check regularly if new messages are there
    setInterval(function(){
        $.getJSON('../ajax/unreadCheckFighterGroup.php', function( data ) {
            var newMessages = $('#newMessages');
            if (data.unreads > 0) {
                $(newMessages).removeClass('hidden');
                $('#countMessages').text(data.unreads);
                $('.mmenu .tr').removeClass('tr').addClass('trnewmsg');
                if (data.unreads > 1) {
                    newMessages.find('.plural').removeClass('hidden');
                } else {
                    newMessages.find('.plural').addClass('hidden');
                }
            } else {
                $(newMessages).addClass('hidden');
            }
        });
    }, checkNewMessagesTime*1000);

    // Watch for the author's change form
    $('a.changePers').on('click', function(event) {
        event.preventDefault();
        $(this).siblings('form.changePersForm').removeClass('hidden');
        $(this).addClass('hidden');
        $(this).siblings('a.name').addClass('hidden');
        $(this).siblings('a.message').addClass('hidden');
    });
    $('a.cancel').on('click', function(event) {
        event.preventDefault();
        var parent = $(this).parents('td.mainbglabel');
        $(parent).find('form.changePersForm').addClass('hidden');
        $(parent).find('a.changePers').removeClass('hidden');
        $(parent).find('a.name').removeClass('hidden');
        $(parent).find('a.message').removeClass('hidden');
    });


});