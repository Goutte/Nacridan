var current_panel; // dernier panel affiché
var overlay_panel; // panel overlay

var current_tile; 
var current_tile_classname;

function invertGridState(){
	var d = document.getElementById("minimap")
	if(d){
		if(d.style.borderSpacing == "0px")
			d.style.borderSpacing = "1px";
		else
			d.style.borderSpacing = "0px";
	}else{
		alert("No minimap found !!");
	}
}

//Gestion du rollover
/* function func(obj,tile,classname){
    var boites = document.getElementsByClassName(classname);
    for (i=0;i<boites.length;i++) {boites[i].className=classname;}//style.backgroundColor="white";
    document.getElementById(tile).className=classname+"l";//className=classname+"l"
    
}
function mover(obj,tile,classname){
    if (document.getElementById(tile).className!=classname+"l";)
        {document.getElementById(tile).className=classname+"l";}//;className=classname+"l"
}
function mout(obj,tile,classname){
    if (document.getElementById(tile).className==classname+"l")//className==classname+"l"
        {document.getElementById(tile).className=classname;}//className=classname
    
}
 */

function highlightCurrentTile(obj,classname){
        if(current_tile)
           current_tile.className=current_tile_classname;

        current_tile=obj;
        current_tile_classname=classname;
        current_tile.className=classname+"l";
}

function highlightManager(obj,classname)
{
       if(current_tile!=obj)
	  obj.className=classname;
}

function showPanel(id){
	var d = document.getElementById(id);
	var actionmsg = document.getElementById("cqaction");

   if(actionmsg){
        actionmsg.style.display = 'none';
   }

	if(d && d !== current_panel){
		hidePanel();
		d.style.display = 'block';
		current_panel = d;
	}
}


function hidePanel(){
	hideOverlay();
	if(current_panel){
		current_panel.style.display = 'none';
		current_panel = null;
	}
}

function hideOverlay(){
	if(overlay_panel){
		if (current_panel != overlay_panel){
			overlay_panel.style.display = 'none';
			if(current_panel)
				current_panel.style.display = 'block';
		}
		overlay_panel = null;
	}
}

function overlayPanel(id){
	var d = document.getElementById(id);
	hideOverlay();
	if(d && d !== current_panel){
		d.style.display = 'block';
		if(current_panel)
			current_panel.style.display = 'none';
		overlay_panel = d;
	}      
}

function minimapRefresh(){
	location.href=location.pathname+(current_panel ? ("?current_panel="+current_panel.id) : "");
}

function minimapRestoreRefresh(id){
	showPanel(id)
	setTimeout('minimapRefresh()',300000);
}
