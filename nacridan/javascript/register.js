var idtimeout;

function sethour(id, zone)
{
     var ut=new Date();
     var offset = ut.getTimezoneOffset();
     dts=isDayLightSaving(ut);

     shift_array=zone.split(";");
     if(dts)
     {
        shift=shift_array[2];
     }
     else
     {
        shift=shift_array[1];
     }

     utc=ut.getTime();
     utc=utc+(offset*60000);
     ut.setTime(utc+shift*60000);

     var h,m,s;
     var time="      ";
     h=ut.getHours();
     m=ut.getMinutes();
     s=ut.getSeconds();
     if(s<=9) s="0"+s;
     if(m<=9) m="0"+m;
     if(h<=9) h="0"+h;
     time+=h+":"+m+":"+s;
     var obj=document.getElementById(id);
     obj.innerHTML=time;
     clearTimeout(idtimeout);
     idtimeout=setTimeout("sethour(\""+id+"\",\""+zone+"\")",1000);
}

function isDayLightSaving(gmt)
{
  var lsm = new Date;
  var lso = new Date;
  lsm.setMonth(2); // March
  lsm.setDate(31);
  var day = lsm.getDay();// day of week of 31st
  lsm.setDate(31-day); // last Sunday
  lso.setMonth(9); // October
  lso.setDate(31);
  day = lso.getDay();
  lso.setDate(31-day);

  lsm.setSeconds(0);
  lsm.setMinutes(0);
  lsm.setHours(0);
  gmtlsm=lsm.getTime();
  gmtlsm+=3600*2*1000-1000;
	
  lso.setSeconds(0);
  lso.setMinutes(0);
  lso.setHours(0);
  gmtlso=lso.getTime();
  gmtlso+=3600*2*1000-1000;

  gmtgmt=gmt.getTime();

  if (gmtgmt > gmtlsm && gmtgmt < gmtlso)
      return 1;
   else
      return 0;
}
