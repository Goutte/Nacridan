var currentmenu;
var timeout=0; //ne pas toucher, c'est pour d�clarer la variable
var delay = 300; // en milliseconde
var outmenu;

function isdefined( variable)
{
    return (typeof(window[variable]) == "undefined")?  false: true;
}

function showSubMenu(id) {
   var d = document.getElementById(id);
   if (d) 
   {

      if (d.style.display=='none') 
      {
          d.style.display='block';
          d.style.width='20em';       
      }
      else
      {
         d.style.display='none';
      }
   }
}

function trim(string)
{
return string.replace(/(^\s*)|(\s*$)/g,'');
} 

function undoublecoma(string)
{
return string.replace(/,(,| )*/,',');
}

function updateDiv(id,response){
  document.getElementById(id).innerHTML = response;
}

function setStyleObj(obj, style, value){
  obj.style[style]= value;
}

function setStyle(id, style, value){
	
	var obj=document.getElementById(id);
  obj.style[style]= value;
}


function setStyleCity(style,value){
	var vmap=document.getElementById('map01'); 

	if(vmap.style['visibility']=='visible')
	{
		var obj1=document.getElementById('smallcity01');
  	obj1.style[style]= value;
  	
  	var obj2=document.getElementById('smallcity02');
  	obj2.style[style]= 'hidden';
	}
	else if(vmap.style['visibility']=='hidden')
	{
		var obj1=document.getElementById('smallcity01');
  	obj1.style[style]= 'hidden';
  	
  	var obj2=document.getElementById('smallcity02');
  	obj2.style[style]= value;
	}  
}

function setStyleCityCaptured(style,value){
	var vmap=document.getElementById('map01'); 

	if(vmap.style['visibility']=='visible')
	{
		var obj1=document.getElementById('smallcitycaptured01');
		obj1.style[style]= value;
  	
		/* var obj2=document.getElementById('smallcity02');
		obj2.style[style]= 'hidden'; */
	}
	else if(vmap.style['visibility']=='hidden')
	{
		var obj1=document.getElementById('smallcitycaptured01');
		obj1.style[style]= 'hidden';
		
		/* var obj2=document.getElementById('smallcity02');
		obj2.style[style]= value; */
	}  
}

function setStyleLevelMonster(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('levelmonstermap01');
	var obj2=document.getElementById('levelmonstermap02');
	var obj3=document.getElementById('maincity');
  var obj4=document.getElementById('maincity2');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	obj2.style[style]= 'hidden';
  	
  	if(value=='hidden')
  	{  	
  		obj3.style[style]= 'visible';
  		obj4.style[style]= 'hidden';
  	}
  	else
  	{
  		obj3.style[style]= 'hidden';
  		obj4.style[style]= 'visible';
  	}
  	
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	obj2.style[style]= value;
  	obj3.style[style]= 'hidden';
  	obj4.style[style]= 'hidden';
	}  
}

function setStyleMapTopo(style,value){
	document.getElementById('maptopo').style[style]= value;
}

function setStyleKingdom(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('kingdommap01');
	//var obj2=document.getElementById('levelmonstermap02');
	var obj3=document.getElementById('maincity');
  var obj4=document.getElementById('maincity2');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	//obj2.style[style]= 'hidden';
  	
  	if(value=='hidden')
  	{  	
  		obj3.style[style]= 'visible';
  		obj4.style[style]= 'hidden';
  	}
  	else
  	{
  		obj3.style[style]= 'hidden';
  		obj4.style[style]= 'visible';
  	}
  	
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	//obj2.style[style]= value;
  	obj3.style[style]= 'hidden';
  	obj4.style[style]= 'hidden';
	}  
}

function setStyleRoad(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('roadmap01');
	//var obj2=document.getElementById('levelmonstermap02');
	var obj3=document.getElementById('maincity');
  var obj4=document.getElementById('maincity2');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	//obj2.style[style]= 'hidden';
  	
  	if(value=='hidden')
  	{  	
  		obj3.style[style]= 'visible';
  		obj4.style[style]= 'hidden';
  	}
  	else
  	{
  		obj3.style[style]= 'hidden';
  		obj4.style[style]= 'visible';
  	}
  	
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	//obj2.style[style]= value;
  	obj3.style[style]= 'hidden';
  	obj4.style[style]= 'hidden';
	}  
}

function setStylePlayers(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('player01');
	var obj2=document.getElementById('player02');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	obj2.style[style]= 'hidden';
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	obj2.style[style]= value;
	}  
}

function setStyleTeam(style,value){
	var vmap=document.getElementById('map01'); 
	var obj1=document.getElementById('teammember01');
	var obj2=document.getElementById('teammember02');

	if(vmap.style['visibility']=='visible')
	{
  	obj1.style[style]= value;
  	obj2.style[style]= 'hidden';
	}
	else if(vmap.style['visibility']=='hidden')
	{
  	obj1.style[style]= 'hidden';
  	obj2.style[style]= value;
	}  
}

function loadMap(idmap,currmap){
	var obj1=document.getElementById('map01');
	var obj2=document.getElementById('map02');
	var obj3=document.getElementById('maincity');
	var obj4=document.getElementById('redcross');
	
	if(idmap=='map01')
	{
  	obj1.style['visibility']= 'visible';
  	obj2.style['visibility']= 'hidden';
  	obj3.style['visibility']= 'visible';
  	
  	if(currmap==1)
  	{ 		
  		obj4.style['visibility']= 'visible';
  	}
  	else if(currmap==2)
  	{  		
  		obj4.style['visibility']= 'hidden';
  	}
  }
  else if(idmap=='map02')
  {
  	obj1.style['visibility']= 'hidden';
  	obj2.style['visibility']= 'visible';
  	obj3.style['visibility']= 'hidden';
  	
  	if(currmap==1)
  	{  		
  		obj4.style['visibility']= 'hidden';
  	}
  	else if(currmap==2)
  	{  		
  		obj4.style['visibility']= 'visible';
  	}  
  }
  var obj5=document.getElementById('levelmonstermap01');
  obj5.style['visibility']= 'hidden';
  var obj6=document.getElementById('levelmonstermap02');
  obj6.style['visibility']= 'hidden';
  var obj7=document.getElementById('maincity2');
  obj7.style['visibility']= 'hidden';
}



function updateInput(id, value){
  document.getElementById(id).value = value;
}

function loadPage(id,page)
{
   eval("window.parent."+id+".location.href = page");
   showSubMenu();
}

function setInnerHTML(id,value)
{
   document.getElementById(id).innerHTML=value;
}

function setClassName(id, classname) {
   document.getElementById(id).className = classname;
}

function profile(url) { 
     nameWindow = "profile"+Math.round(Math.random()*100000);  
     window.open (url, nameWindow, "height=850px,width=832px,left=0,top=0,scrollbars=yes,titlebar=yes,status=yes,toolbar=yes,resizable=1,menubar=yes,location=yes"); 
}              

function detailedRules(url) { 
     nameWindow = "profile"+Math.round(Math.random()*100000);  
     window.open (url, nameWindow, "height=750px,width=930px,left=0,top=0,scrollbars=yes,titlebar=yes,status=yes,toolbar=yes,resizable=1,menubar=yes,location=yes"); 
}

function openOpener(url) 
{ 
   window.opener.location=url;
   return false;
}

var tooltype;

function showTip(name)
{ 
  tooltype= document.getElementById(name).style;
  tooltype.visibility = "visible";
}


function hideTip() 
{
  tooltype.visibility = "hidden";	
}

function invertCheckboxes(formname)
{
  var elts = document.getElementsByName('check[]');

 
  if(elts != null)
  {
    var nb  =  elts.length;                 
    if (nb) {
      for (var i = 0; i < nb; i++) {
	elts[i].checked = !elts[i].checked;
      } // end for
    }
    else
      {
        elts.checked = !elts.checked;
      }
  }
  return true;
}

// AJAX

var tooltype;

function checkAlliance(name,destid)
{ 
  var param;
  param="name="+name;
  param+="&destid="+destid;
  var response=RPCPost("../ajax/team.php",param);
  updateDiv('ajaxResp',response);
  if(response.length>0)
    setStyle('ajaxResp', 'visibility', 'visible');
  else
    setStyle('ajaxResp', 'visibility', 'hidden');
  return true;
}

function checkAlliancePJ(name,destid)
{ 
  var param;
  param="name="+name;
  param+="&destid="+destid;
  var response=RPCPost("../ajax/pj.php",param);
  updateDiv('ajaxResp',response);
  if(response.length>0)
    setStyle('ajaxResp', 'visibility', 'visible');
  else
    setStyle('ajaxResp', 'visibility', 'hidden');
  return true;
}

function checkAllegiance(name,destid)
{
 checkAlliance(name,destid);	 
}


function submitTheForm(formid,hiddenid,val)
{
  obj=document.getElementById(hiddenid);
  obj.value=val;
  obj=document.getElementById(formid);
  obj.submit();
}

var detect = navigator.userAgent.toLowerCase();
var browser;
function checkIt(string)
{
	place = detect.indexOf(string) + 1;
	thestring = string;
	return place;
}

if (checkIt('konqueror'))
{
  browser = "Konqueror"
}
else if (checkIt('safari')) browser = "Safari";
else if (checkIt('omniweb')) browser = "OmniWeb";
else if (checkIt('opera')) browser = "Opera";
else if (checkIt('webtv')) browser = "WebTV";
else if (checkIt('msie')) browser = "Internet Explorer";
else if (checkIt('icab')) browser = "iCab";
else if (!checkIt('compatible'))
{
	browser = "Netscape Navigator";
	version = detect.charAt(8);
}
else browser = "An unknown browser";



var addLimit=10;
var nbadd=0;
    
function addDest(name,dest,msg)
{
	if (dest)
	{
		index=dest.selectedIndex;
		if(dest.value>0)
		{
			curDest=document.getElementById(name);
			curDest.value=trim(curDest.value);    
			value= dest.options[index].text;
			if ((pos = curDest.value.indexOf(value,0)) < 0)
			{
				if(nbadd<addLimit)
				{
					if(curDest.value.length!=0 && curDest.value.charAt(curDest.value.length)!=',')
   						curDest.value+=',';
 					curDest.value=curDest.value+value;
 					curDest.value=undoublecoma(curDest.value);
 					nbadd=nbadd+1;
				}
				else
				{
 					alert(msg);
				}
			}
		}
		dest.selectedIndex=0;
 	}
}


$( document ).ready(function() {

    /**
     * Links to profile which are going to be open in a popup
     */
    $('a.popupify').on('click', function(event){
        event.preventDefault();
        profile($(this).attr('href'));
    });

    /**
     * Links from a popup which are going to be open in the opener's window if it has one.
     */
    $('a.toOpener').on('click', function(event){
        if (window.opener != null) {
            event.preventDefault();
            openOpener($(this).attr('href'));
        }
    })
});
