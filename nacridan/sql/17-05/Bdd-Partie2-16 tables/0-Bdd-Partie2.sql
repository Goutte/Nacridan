-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Ven 14 Juin 2013 à 14:41
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicAbility`
--

CREATE TABLE IF NOT EXISTS `BasicAbility` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `ident` varchar(30) NOT NULL DEFAULT '',
  `timeAttack` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `modifierPA` int(11) NOT NULL DEFAULT '8',
  `school` tinyint(4) NOT NULL DEFAULT '1',
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `usable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `BasicAbility`
--

INSERT INTO `BasicAbility` (`id`, `name`, `ident`, `timeAttack`, `modifierPA`, `school`, `level`, `usable`) VALUES
(1, 'Attaque puissante', 'ABILITY_POWERFUL', 'YES', 0, 1, 1, 1),
(2, 'Garde', 'ABILITY_GUARD', 'NO', 5, 9, 0, 0),
(3, 'Premiers soins', 'ABILITY_FIRSTAID', 'NO', 5, 9, 0, 0),
(4, 'Dégâts accrus', 'ABILITY_DAMAGE', 'YES', 0, 9, 0, 1),
(5, 'Tournoiement', 'ABILITY_TWIRL', 'YES', 1, 1, 1, 11),
(6, 'Aiguise Lame', 'ABILITY_SHARPEN', 'NO', 5, 1, 1, 1),
(7, 'Coup traitre', 'ABILITY_THRUST', 'YES', 0, 3, 1, 1),
(8, 'Lancer de bolas', 'ABILITY_BOLAS', 'NO', 8, 3, 1, 2),
(9, 'Coup assomant', 'ABILITY_STUNNED', 'YES', 0, 1, 2, 1),
(10, 'Coup de grâce', 'ABILITY_KNOCKOUT', 'YES', 0, 1, 2, 1),
(11, 'Désarmer', 'ABILITY_DISARM', 'YES', 0, 3, 2, 1),
(12, 'Autorégénération', 'ABILITY_AUTOREGEN', 'NO', 10, 4, 1, 0),
(13, 'Appel de la lumière', 'ABILITY_LIGHT', 'YES', 0, 4, 1, 1),
(14, 'Aura de courage', 'ABILITY_BRAVERY', 'NO', 5, 4, 2, 0),
(15, 'Aura de résistance', 'ABILITY_RESISTANCE', 'NO', 5, 4, 2, 0),
(16, 'Exorcisme de l''ombre', 'ABILITY_EXORCISM', 'NO', 8, 4, 2, 0),
(17, 'Vol à la tire', 'ABILITY_STEAL', 'NO', 5, 3, 1, 0),
(18, 'Tir lointain', 'ABILITY_FAR', 'YES', 1, 2, 1, 13),
(19, 'Tir gênant', 'ABILITY_DISABLING', 'YES', 0, 2, 1, 3),
(20, 'Flèche de négation', 'ABILITY_NEGATION', 'YES', 0, 2, 1, 3),
(21, 'Course celeste', 'ABILITY_RUN', 'NO', 2, 2, 2, 0),
(22, 'Flèche enflammée', 'ABILITY_FLAMING', 'YES', 0, 2, 2, 3),
(23, 'Larçin', 'ABILITY_LARCENY', 'NO', 5, 3, 2, 0),
(24, 'Protection', 'ABILITY_PROTECTION', 'NO', 8, 4, 1, 0),
(25, 'Projection', 'ABILITY_PROJECTION', 'YES', 0, 1, 2, 11),
(26, 'Volée de flèche', 'ABILITY_FLYARROW', 'YES', 0, 2, 2, 3),
(27, 'Embuscade', 'ABILITY_AMBUSH', 'NO', 5, 3, 2, 0),
(28, 'Botte d''estoc', 'ABILITY_TREACHEROUS', 'YES', 0, 9, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `BasicBuilding`
--

CREATE TABLE IF NOT EXISTS `BasicBuilding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Maison',
  `pic` varchar(32) NOT NULL DEFAULT '',
  `price` int(11) NOT NULL DEFAULT '0',
  `solidity` enum('Fragile','Medium','Solid') NOT NULL DEFAULT 'Fragile',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=163 ;

--
-- Contenu de la table `BasicBuilding`
--

INSERT INTO `BasicBuilding` (`id`, `name`, `pic`, `price`, `solidity`) VALUES
(1, 'Auberge', 'auberge.png', 200, 'Fragile'),
(2, 'Banque', 'banque.png', 1500, 'Solid'),
(3, 'Caserne', 'caserne.png', 10000, 'Fragile'),
(4, 'Comptoir Commercial', 'comptoir-commercial.png', 200, 'Medium'),
(5, 'Échoppe', 'echoppe.png', 200, 'Fragile'),
(6, 'École de Combat', 'ecole-combat.png', 400, 'Medium'),
(7, 'École de Magie', 'ecole-magie.png', 400, 'Medium'),
(8, 'École des Métiers', 'ecole-metiers.png', 400, 'Medium'),
(9, 'Guilde des Artisans', 'guilde-artisans.png', 500, 'Medium'),
(10, 'Maison', 'maison.png', 200, 'Fragile'),
(11, 'Palais du Gouverneur', 'palais-gouverneur.png', 10000, 'Fragile'),
(12, 'Palais Royal', 'palais-royal.png', 10000, 'Fragile'),
(13, 'Prison', 'prison.png', 10000, 'Fragile'),
(14, 'Temple', 'temple.png', 500, 'Solid'),
(15, 'Bassin Divin', 'bassin-divin.png', 0, 'Fragile'),
(16, 'Rempart', 'mur-pierre-NS.png', 50, 'Solid'),
(17, 'Rempart', 'mur-pierre-NE.png', 50, 'Solid'),
(18, 'Rempart', 'mur-pierre-EO.png', 50, 'Solid'),
(19, 'Rempart', 'mur-pierre-coin-SE.png', 50, 'Solid'),
(20, 'Rempart', 'mur-pierre-coin-NE.png', 50, 'Solid'),
(21, 'Porte Fermée', 'mur-pierre-porte-N.png', 50, 'Solid'),
(22, 'Rempart', 'mur-pierre-coin-E.png', 50, 'Solid'),
(23, 'Rempart', 'mur-pierre-coin-NO.png', 50, 'Solid'),
(24, 'Rempart', 'mur-pierre-coin-O.png', 50, 'Solid'),
(25, 'Rempart', 'mur-pierre-coin-SO.png', 50, 'Solid'),
(26, 'Porte Ouverte', 'mur-pierre-porte-N-ouverte.png', 50, 'Solid'),
(27, 'Mur de Terre', 'mur-de-terre.png', 50, 'Medium');

-- --------------------------------------------------------

--
-- Structure de la table `BasicEquipment`
--

CREATE TABLE IF NOT EXISTS `BasicEquipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Modifier_BasicEquipment` bigint(20) NOT NULL DEFAULT '0',
  `id_EquipmentType` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicMaterial` smallint(7) NOT NULL DEFAULT '1',
  `name` varchar(32) NOT NULL DEFAULT '',
  `frequency` tinyint(4) NOT NULL DEFAULT '0',
  `durability` smallint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_listTypeEquipement` (`id_EquipmentType`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=601 ;

--
-- Contenu de la table `BasicEquipment`
--

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(1, 1, 1, 1, 'Dague', 48, 60),
(2, 2, 1, 1, 'Epée Courte', 48, 60),
(3, 3, 2, 1, 'Lance', 48, 60),
(4, 4, 2, 1, 'Epée longue', 48, 60),
(5, 5, 2, 1, 'Hache de bataille', 48, 60),
(6, 6, 3, 1, 'Fléau d''armes', 48, 60),
(7, 7, 3, 1, 'Masse d''armes', 48, 60),
(8, 8, 4, 1, 'Epée à deux mains', 56, 60),
(9, 9, 4, 1, 'Hallebarde', 56, 60),
(10, 10, 4, 1, 'Marteau de guerre', 56, 60),
(11, 11, 6, 9, 'Sceptre', 48, 60),
(12, 12, 7, 9, 'Bâton magique', 56, 60),
(13, 13, 5, 2, 'Arc court', 48, 60),
(14, 14, 5, 2, 'Arc à double courbure', 48, 60),
(15, 15, 5, 2, 'Arc long', 48, 60),
(16, 16, 8, 2, 'Petit bouclier', 56, 60),
(17, 17, 9, 3, 'Bouclier moyen', 56, 60),
(18, 18, 10, 1, 'Bouclier large', 56, 60),
(19, 19, 11, 4, 'Cuirasse de cuir', 48, 60),
(20, 20, 11, 3, 'Cuirasse d''écailles', 48, 60),
(21, 21, 11, 1, 'Cuirasse de plate', 48, 60),
(22, 22, 11, 5, 'Toge magique', 48, 60),
(23, 23, 12, 4, 'Casque de cuir', 32, 60),
(24, 24, 12, 3, 'Capuchon d''écailles', 32, 60),
(25, 25, 12, 1, 'Heaume de plate', 32, 60),
(26, 26, 12, 5, 'Diadème magique', 32, 60),
(27, 27, 13, 4, 'Bottes de cuir', 32, 60),
(28, 28, 13, 3, 'Bottes d''écailles', 32, 60),
(29, 29, 13, 1, 'Bottes de plate', 32, 60),
(30, 30, 13, 5, 'Bottes magiques', 32, 60),
(31, 31, 14, 4, 'Cuissarde de cuir', 32, 60),
(32, 32, 14, 3, 'Cuissarde d''écailles', 32, 60),
(33, 33, 14, 1, 'Cuissarde de plate', 32, 60),
(34, 34, 14, 5, 'Cuissarde magique', 32, 60),
(35, 35, 15, 4, 'Gant droit de cuir', 32, 60),
(36, 36, 15, 3, 'Gant droit d''écailles', 32, 60),
(37, 37, 15, 1, 'Gant droit de maille', 32, 60),
(38, 38, 15, 5, 'Gant droit magique', 32, 60),
(39, 39, 17, 4, 'Epaulière droite de cuir', 24, 60),
(40, 40, 17, 3, 'Epaulière droite d''écailles', 24, 60),
(41, 41, 17, 1, 'Epaulière droite de plate', 24, 60),
(42, 42, 17, 5, 'Epaulière droite magique', 24, 60),
(43, 39, 18, 4, 'Epaulière gauche de cuir', 24, 60),
(44, 40, 18, 3, 'Epaulière gauche d''écailles', 24, 60),
(45, 41, 18, 1, 'Epaulière gauche de plate', 24, 60),
(46, 42, 18, 5, 'Epaulière gauche magique', 24, 60),
(47, 47, 20, 4, 'Ceinture de cuir', 40, 60),
(48, 48, 20, 3, 'Ceinture d''écailles', 40, 60),
(49, 49, 20, 1, 'Ceinture de plate', 40, 60),
(50, 50, 20, 5, 'Ceinture magique', 40, 60),
(55, 55, 28, 2, 'Flèche en bois', 6, 0),
(56, 56, 28, 1, 'Flèche en fer', 6, 0),
(100, 999, 30, 9, 'Emeraude', 20, 0),
(101, 999, 30, 10, 'Rubis', 40, 0),
(102, 999, 30, 11, 'Diamant', 40, 0),
(103, 999, 31, 4, 'Cuir', 8, 0),
(104, 999, 31, 3, 'Ecailles', 8, 0),
(105, 999, 31, 5, 'Lin', 8, 0),
(106, 999, 31, 1, 'Fer', 8, 0),
(107, 999, 32, 6, 'Racine de Pismouss', 4, 0),
(108, 999, 32, 7, 'Graine de garnach', 4, 0),
(109, 999, 32, 8, 'Feuille de Neptune', 4, 0),
(110, 999, 31, 2, 'Bois', 4, 0),
(200, 999, 40, 1, 'Pioche de mineur', 40, 15),
(201, 999, 40, 1, 'Pince à dépecer', 40, 15),
(202, 999, 40, 2, 'Teilleuse', 40, 15),
(203, 999, 40, 2, 'Pressoir', 40, 15),
(204, 999, 40, 2, 'Dictionnaire Kradjeck', 40, 15),
(205, 205, 1, 1, 'Bolas', 40, 60),
(206, 999, 40, 1, 'Cauchoir de bucheron', 40, 15),
(207, 999, 40, 1, 'Tenailles d''artisan', 40, 15),
(208, 999, 40, 1, 'Alène d''artisan', 40, 15),
(209, 999, 40, 1, 'Plioir d''artisan', 40, 15),
(210, 999, 40, 1, 'Ciseaux d''artisan', 40, 15),
(211, 999, 40, 1, 'Alambic d''artisan', 40, 15),
(212, 999, 40, 1, 'Râpes d''artisan', 40, 15),
(213, 999, 40, 1, 'Marteau de forgeron', 40, 15),
(214, 999, 40, 1, 'Griffe à lacer de tanneur', 40, 15),
(215, 999, 40, 1, 'Abat-care d''écailleur', 40, 15),
(216, 999, 40, 1, 'Roue à molette d''effileur', 40, 15),
(217, 999, 40, 1, 'Filtre d''alchimiste', 40, 15),
(218, 999, 40, 1, 'Chignole de charpentier', 40, 15),
(219, 999, 40, 1, 'Burin de tailleur', 40, 15),
(300, 999, 41, 4, 'Sac de gemmes', 40, 60),
(301, 999, 42, 4, 'Sac de matières premières', 40, 60),
(302, 999, 43, 4, 'Sac d''herbes', 40, 60),
(400, 400, 29, 6, 'Potion de vie', 12, 0),
(303, 999, 44, 2, 'Carquois', 24, 60),
(401, 401, 29, 7, 'Potion de force', 12, 0),
(402, 402, 29, 7, 'Potion de dextérité', 12, 0),
(403, 403, 29, 8, 'Potion de vitesse', 12, 0),
(404, 404, 29, 8, 'Potion de magie', 12, 0),
(500, 999, 33, 11, 'Moustache de rat', 4, 0),
(501, 999, 33, 11, 'Crocs de chauve-souris', 4, 0),
(502, 999, 33, 11, 'Langue de crapaud', 4, 0),
(503, 999, 33, 11, 'Antenne de fourmi reine', 10, 0),
(504, 999, 33, 11, 'Yeux d''araignée éclipsante', 4, 0),
(600, 999, 45, 2, 'Parchemin', 0, 0);

-- --------------------------------------------------------
-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 09 Juillet 2013 à 11:55
-- Version du serveur: 5.1.32
-- Version de PHP: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `NacridanV2B`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicEvent`
--

CREATE TABLE IF NOT EXISTS `BasicEvent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ident` varchar(30) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=611 ;

--
-- Contenu de la table `BasicEvent`
--

INSERT INTO `BasicEvent` (`id`, `ident`, `description`) VALUES
(1, 'MOVE_EVENT', '{player} s''est déplacé{gendersrc}.'),
(2, 'ATTACK_EVENT_FAIL', '{player} a frappé {target} qui a survécu.'),
(3, 'ATTACK_EVENT_FAIL', '{player} a raté son attaque sur {target}.'),
(4, 'ARCHERY_EVENT_SUCCESS', '{player} a tiré une flèche sur {target} qui a survécu.'),
(5, 'ARCHERY_EVENT_FAIL', '{player} a raté son tir sur {target}.'),
(6, 'ACTION_EVENT_DEATH_NPC', '{player} a tué {target}.'),
(7, 'ACTION_EVENT_DEATH_PC', '{player} a tué {target} qui sera ressuscité{genderdest} dans un temple.'),
(10, 'PICK_UP_EVENT', '{player} a ramassé un objet.'),
(11, 'DROP_EVENT', '{player} a déposé un objet.'),
(12, 'GIVE_MONEY_EVENT', '{player} a donné des PO à {target}.'),
(13, 'GIVE_OBJECT_EVENT', '{player} a donné un objet à {target}.'),
(14, 'HUSTLE_EVENT', '{player} a bousculé {target}.'),
(15, 'DRINK_SPRING_EVENT', '{player} a bu l''eau d''un bassin divin.'),
(16, 'ACTION_DEATH_EVENT', '{player} est mort à la suite de ses blessures.'),
(17, 'DISABLED_BRAVERY_EVENT', '{player} a désactivé son aura de courage.'),
(18, 'DISABLED_RESISTANCE_EVENT', '{player} a désactivé son aura de résistance'),
(19, 'DISABLED_PROTECTION_EVENT', '{player} a desactivé sa protection'),
(20, 'ATTACK_INTER_EVENT_SUCCESS', '{player} a frappé {target} qui s''est interposé.'),
(21, 'ATTACK_INTER_EVENT_DEATH', '{player} a tué {target} qui s''est interposé.'),
(23, 'ATTACK_INTER_EVENT_FAIL', '{player} a raté son attaque sur {target} qui s''est interposé.'),
(24, 'ATTACK_BUILDING_EVENT_SUCCESS', '{player} a attaqué un bâtiment.'),
(25, 'ATTACK_BUILDING_EVENT_DEATH', '{player} a détruit un bâtiment'),
(28, 'ARCHERY_INTER_EVENT_SUCCESS', '{player} a tiré une flèche sur {target} qui s''est interposé.'),
(29, 'USE_POTION_EVENT', '{player} a utilisé une potion.'),
(30, 'RECALL_EVENT_ACCEPTED', '{player} a accepté d''être téléporté auprès de {target}.'),
(31, 'RECALL_EVENT_REFUSED', '{player} a refusé d''être téléporté auprès de {target}.'),
(32, 'REVOKE_GOLEM_EVENT', '{player} a révoqué son golem de feu.'),
(33, 'RIDE_TORTLE_EVENT', '{player} a chevauché une tortue'),
(34, 'TORTLE_UNMOUNT', '{player} est descendu d''une tortue'),
(35, 'HUSTEL_EVEN_FAIL', '{player} n''a pas réussi à bousculer {target}'),
(36, 'DESTROY_EVENT', '{player} a détruit une ressource.'),
(37, 'ARREST_EVENT_ACCEPTED', '{player} a été conduit en prison sans violence.'),
(38, 'ARREST_EVENT_REFUSED', '{player} a refusé de se rendre aux gardes de la ville.'),
(39, 'EXIT_JAIL_EVENT', '{player} est sorti de prison après avoir purgé sa peine.'),
(40, 'LEVEL_UP_EVENT', '{player} a gagné un niveau.'),
(41, 'GAIN_XP_EVENT', '{player} a gagné des points d''expérience.'),
(42, 'ARREST_EVENT_FORCED', '{player} a été conduit en prison de force.'),
(43, 'BUY_PJ_EVENT', '{player} a acheté des objets à {target}'),
(100, 'ABILITY_FIRSTAID_EVENT', '{player} a apporté les premiers soins à {target}'),
(101, 'ABILITY_GUARD_EVENT', '{player} se met en position de défense'),
(103, 'ABILITY_TWIRL_EVENT', '{player} a réalisé un tournoiement.'),
(104, 'ABILITY_SHARPEN_EVENT', '{player} a aiguisé une arme.'),
(105, 'ABILITY_BOLAS_EVENT_SUCCESS', '{player} a réussi à lancer un bolas sur {target}'),
(106, 'ABILITY_BOLAS_EVENT_FAIL', '{player} a lancé un bolas sur {target} qui l''a esquivé.'),
(111, 'ABILITY_RESISTANCE_EVENT', '{player} a activé une aura de résistance'),
(107, 'ABILITY_DISARM_FAIL', '{player} a tenté de désarmer {target} qui a esquivé l''attaque.'),
(108, 'ABILITY_DISARM_SUCCESS', '{player} a désarmé {target}'),
(109, 'ABILITY_AUTOREGEN_EVENT', '{player} a utilisé auto-régénération.'),
(110, 'ABILITY_BRAVERY_EVENT', '{player} a activé une aura de courage'),
(112, 'ABILITY_EXORCISM_EVENT', '{player} a utilisé Exorcisme de l''ombre'),
(113, 'PASSIVE_EXORCISM_EVENT', '{target} a subit l’exorcisme de {player} \r\n'),
(114, 'ABILITY_STEAL_EVENT_SUCCESS', '{player} a réussi à voler {target}'),
(115, 'ABILTIY_STEAL_EVENT_FAIL', '{player} n''a pas réussi à voler {target}'),
(116, 'ABILITY_RUN', '{player} a utilisé course celeste'),
(117, 'ABILITY_FLAMING_EVENT_SUCCESS', '{player} a tiré une flèche enflammée sur un bâtiment.'),
(118, 'ABILITY_LARCENY_EVENT_SUCCESS', '{player} a réussi un larçin sur {target}'),
(119, 'ABILITY_LARCENY_EVENT_FAIL', '{player} a raté son larçin sur {target}'),
(120, 'ABILITY_PROTECTION_EVENT', '{player} défend {target}'),
(121, 'ABILITY_FLYARROW_EVENT', '{player} a décoché une volée de flèche.'),
(122, 'ABILITY_FLAMING_EVENT_DEATH', '{player} a détruit un bâtiment en tirant une flèche enflammée.'),
(123, 'ABILITY_AMBUSH_EVENT', '{player} a utilisé Ambuscade.'),
(201, 'MAGICAL_ATTACK_EVENT_SUCCESS', '{player} a réussi une attaque magique sur {target}'),
(202, 'MAGICAL_ATTACK_EVENT_FAIL', '{player} a raté une attaque magique sur {target}'),
(203, 'SPELL_TEARS_EVENT', '{player} a réussi à soigner {target}'),
(204, 'SPELL_ARMOR_EVENT', '{player} a créé une armure magique sur {target}'),
(205, 'SPELL_CURE', '{player} a utilisé Souffle d''Athlan sur {target}'),
(206, 'SPELL_REGEN', '{player} a lancé Régénération sur {target}'),
(207, 'SPELL_SPRING_EVENT', '{player} a créé un Bassin Divin'),
(208, 'SPELL_BLESS_EVENT', '{player} a utilisé Bénédiction sur {target}'),
(209, 'SPELL_WALL_EVENT', '{player} a créé un mur de terre.\r\n'),
(210, 'SPELL_BARRIER', '{player} a lancé barrière enflammée sur {target}'),
(213, 'SPELL_FIRE_EVENT', '{player} a réussi un brasier.'),
(214, 'SPELL_FIRE_EVENT_SUCCESS', '{target} a été touché par le brasier de {player}'),
(215, 'SPELL_FIRE_EVENT_FAIL', '{target} a esquivé le brasier de {player}'),
(217, 'SPELL_SHIELD_EVENT', '{player} a utilisé bouclier magique sur {target}'),
(218, 'SPELL_NEGATION_EVENT', '{player} a utilisé souffle de négation sur {target}'),
(219, 'SPELL_RAIN_EVENT', '{player} a utilisé pluie guérissante.'),
(220, 'PASSIVE_RAIN_EVENT', '{target} a été régénéré par la pluie guerissante de {player}'),
(221, 'SPELL_RELOAD_EVENT', '{player} a rechargé un bassin divin.'),
(222, 'SPELL_SUN_EVENT', '{player} a invoqué un soleil de guérison.'),
(223, 'PASSIVE_SUN_EVENT', '{target} a été guérit automatiquement par le soleil de guérison de {player}.'),
(224, 'SPELL_BUBBLE_EVENT', '{player} a créé une bulle de vie sur {target}'),
(226, 'SPELL_PILLARS_EVENT', '{player} a utilisé Piliers infernaux.'),
(227, 'SPELL_CURSE_EVENT_SUCCESS', '{player} a réussi une malédiction d''Arcxos sur {target}'),
(228, 'SPELL_CURSE_EVENT_FAIL', '{player} a raté une malédiction d''Arcxos sur {target}'),
(229, 'SPELL_ANGER_EVENT', '{player} a lancé Ailes de Colère sur {target}'),
(230, 'SPELL_TELEPORT_EVENT_SUCCESS', '{player} s''est téléporté.'),
(231, 'SPELL_TELEPORT_EVENT_DEATH', '{player} est mort en voulant de se téléporter sur une place occupée.'),
(232, 'SPELL_TELEPORT_EVENT_FAIL', '{player} n''a pas réussi à se téléporté.\r\n'),
(233, 'SPELL_LIGHTTOUCH', '{player} a utlisé touché de lumière sur {target}'),
(234, 'SPELL_SOUL_EVENT_SUCCESS', '{player} a utilisé Projection de l''âme.'),
(235, 'SPELL_RECALL_EVENT_SUCCESS', '{player} a réussi un sort de rappel sur {target}.'),
(236, 'SPELL_RECALL_EVENT_FAIL', '{player} a raté un sort de rappel sur {target}.\r\n'),
(237, 'SPELL_FIRECALL_EVENT', '{player} a invoqué un golem de feu.'),
(238, 'SPELL_SOUL_EVENT_FAIL', '{player} a raté une projection de l''âme.'),
(240, 'SPELL_KINE_EVENT_SUCCESS', '{player} a réussi Force d''Aether sur {target}'),
(241, 'SPELL_KINE_EVENT_FAIL', '{player} a raté Force d''Aether sur {target}'),
(301, 'TALENT_MINE_EVENT', '{player} a exploité un gisement'),
(302, 'TALENT_DISMEMBER_EVENT', '{player} a dépecé une  dépouille'),
(303, 'TALENT_SCUTCH_EVENT', '{player} a moissonné un champs'),
(304, 'TALENT_HARVEST_EVENT', '{player} a récolté un buisson'),
(305, 'TALENT_SPEAK_EVENT', '{player} a obtenu un fer auprès d''un Kradjeck ferreux.'),
(306, 'TALENT_CUT', '{player} a coupé du bois'),
(307, 'TALENT_IRONCRAFT_EVENT', '{player} a utilisé artisanat du fer.'),
(308, 'TALENT_WOODCRAFT_EVENT', '{player} a utilisé artisanat du bois'),
(309, 'TALENT_LEATHERCRAFT_EVENT', '{player} a utilisé artisanat du cuir.'),
(310, 'TALENT_SCALECRAFT_EVENT', '{player} a utilisé artisanat d''écaille.'),
(311, 'TALENT_LINENCRAFT_EVENT', '{player} a utilisé artisanat du lin'),
(312, 'TALENT_PLANTCRAFT_EVENT', '{player} a utilisé artisanat des plantes.'),
(313, 'TALENT_GEMCRAFT_EVENT', '{player} a utilisé artisanat des gemmes.'),
(314, 'TALENT_REFINE_EVENT_SUCCESS', '{player} a raffiné une matière première.'),
(315, 'TALENT_REFINE_EVENT_FAIL', '{player} a raté le raffinage d''une matière première. '),
(316, 'TALENT_SCALEREFINE_EVENT', '{player} a écaillé deux écailles.'),
(317, 'TALENT_LINENREFINE_EVENT', '{player} a effilé deux lins.'),
(318, 'TALENT_GEMREFINE_EVENT', '{player} a taillé deu gemmes.'),
(319, 'TALENT_PLANTREFINE_EVENT', '{player} a filtré deux plantes.'),
(320, 'TALENT_WOODREFINE_EVENT', '{player} a charpenté deux bois.'),
(321, 'TALENT_KRADJECKCALL', '{player} a utilisé Appel de Kradjeck.'),
(322, 'TALENT_DETECTION', '{player} a utilisé Détection des Ressources'),
(323, 'TALENT_MONSTER', '{player} a utilisé Connaissance des monstres sur {target}.'),
(401, 'TELEPORT_EVENT', '{player} a utilisé un temple de téléportation.'),
(402, 'TEMPLE_HEAL_EVENT', '{player} s''est fait soigner dans un temple.'),
(403, 'TEMPLE_BLESSING_EVENT', '{player} a utilisé les services d''un prêtre.'),
(404, 'TEMPLE_RESURRECT_EVENT', '{player} a désigné un nouveau temple comme lieu de résurrection.'),
(405, 'BEDROOM_SLEEPING_EVENT', '{player} s''est reposé dans une auberge.'),
(406, 'HOSTEL_DRINK_EVENT', '{player} a bu un verre dans une auberge.'),
(407, 'HOSTEL_CHAT_EVENT', '{player} a discuté avec les clients d''une auberge.'),
(408, 'HOSTEL_ROUND_EVENT', '{player} a payé une tournée aux clients d''une auberge.'),
(409, 'HOSTEL_NEWS_EVENT', '{player} a posté une annonce sur le panneau d''affichage d''une auberge.'),
(410, 'SCHOOL_LEARN_SPELL_EVENT', '{player} a appris un nouveau sortilège dans une école de magie.'),
(411, 'SCHOOL_LEARN_ABILITY_EVENT', '{player} a appris une nouvelle compétence.'),
(412, 'SCHOOL_LEARN_TALENT_EVENT', '{player} a appris un nouveau savoir-faire dans une école de métier.'),
(413, 'BANK_DEPOSIT_EVENT', '{player} a effectué un dépôt auprès d''une banque.'),
(414, 'BANK_WITHDRAW_EVENT', '{player} a retiré de l''argent dans une banque.'),
(415, 'BANK_TRANSFERT_EVENT', '{player} a effectué un transfert d''argent.'),
(416, 'BASIC_SHOP_BUY_EVENT', '{player} a effectué un achat dans une échoppe.'),
(417, 'SHOP_SELL_EVENT', '{player} a réalisé une vente dans une échoppe.'),
(418, 'MAIN_SHOP_BUY_EVENT', '{player} a effectué un achat dans une échoppe.'),
(419, 'GUILD_ORDER_EQUIP_EVENT', '{player} a commandé un équipement auprès de l''artisan d''un village.'),
(420, 'GUILD_TAKE_EQUIP_EVENT', '{player} a retiré sa commande auprès de l''artisan d''un village.'),
(421, 'GUILD_ORDER_ENCHANT_EVENT', '{player} a commandé l''enchantement d''une pièce.'),
(422, 'GUILD_ORDER_ENCHANT_EVENT', '{player} a retiré sa commande auprès de l''enchanteur d''un village.'),
(423, 'CARAVAN_CREATE_EVENT', '{player} a obtenu une mission commerciale.'),
(424, 'CARAVAN_TERMINATE_EVENT', '{player} a fini une mission commerciale.'),
(425, 'HOUSE_SLEEPING_EVENT', '{player} s''est reposé dans la chambre d''une maison.'),
(426, 'SHOP_REPAIR_EVENT', '{player} a fait réparer quelques équipements.'),
(427, 'COLLECT_PACKAGE_EVENT', '{player} a réceptionné un colis.'),
(428, 'SEND_PACKAGE_EVENT', '{player} a envoyé un colis.'),
(429, 'TEMPLE_PALACE_EVENT', '{player} a lancé la construction d''un palais du gouverneur.'),
(430, 'BUILDING_CONSTRUCT_EVENT', '{player} a ordonné la construction d''un bâtiment.'),
(431, 'BUILDING_REPAIR_EVENT', '{player} a ordonné la réparation d''un bâtiment.'),
(432, 'BUILDING_DESTROY_EVENT', '{player} a ordonné la démolition d''un bâtiment d''un village.'),
(433, 'TREASURY_SET_TAX_EVENT', '{player} a modifié les taxes d''un village'),
(434, 'NAME_VILLAGE_EVENT', '{player} a renommé un village'),
(435, 'PALACE_DEPOSIT_EVENT', '{player} a déposé de l''argent dans les caisses d''un village'),
(436, 'PALACE_WITHDRAW_EVENT', '{player} a retiré de l''argent des caisses d''un village'),
(437, 'FORTIFY_VILLAGE_EVENT', '{player} a construit des fortifications'),
(438, 'OPERATE_VILLAGE_EVENT', '{player} a modifier les règles d''accès à son village'),
(439, 'PALACE_DECISION_EVENT', '{player} a agit sur la loyauté d''un village envers son protecteur.'),
(440, 'HOSTEL_TRADE_EVENT', '{player} a envoyé un message commercial.'),
(441, 'CONTROL_TEMPLE_EVENT', '{player} a bannis des joueurs du Temple de son village.'),
(442, 'UPGRADE_FORTIFICATION', '{player} a fait améliorer les fortifications de son village.'),
(444, 'BUILDING_COLLAPSE_EVENT', 'Le bâtiment dans lequel se trouvait {player} z a été détruit. Il a été blessé.'),
(445, 'BUILDING_COLLAPSE_DEATH_EVENT', 'Le bâtiment dans lequel se trouvait {player} a été détruit. Il a été tué.'),
(446, 'CREATE_PUTSCH_EVENT', '{player} a lancé un coup d''état sur un village au profit de {target}.'),
(447, 'SUPPORT_PUTSCH_EVENT', '{player} a soutenu le putsch de {target}.\r\n'),
(448, 'STRUGGLE_PUTSCH_EVENT', '{player} a lutté contre le putsch de {target}\r\n'),
(449, 'PUTSCH_EVENT_SUCCESS', '{player} a reversé un gouvernement au proft de {target}.'),
(500, 'PASSIVE_BRAVERY_DEATH_EVENT', '{player} est mort suite à la perte de vie subit pour créer une aura de courage.'),
(501, 'PASSIVE_INFEST_DEATH_EVENT', '{target} est mort{gendersrc}, le cerveau rongé par {player}'),
(502, 'PASSIVE_POISON_DEATH_EVENT', '{player} est mort suite l''empoisonnement par {target}'),
(503, 'PASSIVE_BLOOD_DEATH', '{target} est mort suite à la malédiction de {player}'),
(504, 'PASSIVE_BARRIER_DEATH_EVENT', '{target} a été tué dans la barrière enflammée de {player}'),
(600, 'JUMP_EVENT', '{player} a réalisé un bond.'),
(601, 'M_TELEPORT', '{player} s''est téléportée.'),
(602, 'M_KOBOLD_CRAP_EVENT', '{player} s''est enflammé avec sa propre boule de feu.'),
(603, 'M_HEAL_EVENT', '{player} a guéri {target}'),
(604, 'M_MORPH_EVENT', '{player} a pris la forme de {target}.'),
(605, 'M_BACKWORLD', '{player} a traversé un portail pour repartir dans son monde.'),
(606, 'M_STUN_EVENT', '{target} a été assomé par {player}'),
(610, 'M_INFEST_EVENT', '{target} a été infesté par {player}');
-- --------------------------------------------------------

--
-- Structure de la table `BasicMagicSpell`
--

CREATE TABLE IF NOT EXISTS `BasicMagicSpell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `ident` varchar(30) NOT NULL DEFAULT '',
  `school` tinyint(4) NOT NULL DEFAULT '1',
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `PA` int(11) NOT NULL DEFAULT '0',
  `usable` int(7) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `BasicMagicSpell`
--

INSERT INTO `BasicMagicSpell` (`id`, `name`, `ident`, `school`, `level`, `PA`, `usable`) VALUES
(1, 'Toucher brulant', 'SPELL_BURNING', 10, 0, 8, 0),
(2, 'Armure d''Athlan', 'SPELL_ARMOR', 10, 0, 5, 0),
(3, 'Larmes de vie', 'SPELL_TEARS', 10, 0, 7, 0),
(4, 'Soufle d''Athlan', 'SPELL_CURE', 5, 1, 5, 0),
(5, 'Charme de vitalité', 'SPELL_REGEN', 5, 1, 8, 0),
(7, 'Bénédiction', 'SPELL_BLESS', 6, 1, 5, 0),
(8, 'Réveil de la terre', 'SPELL_WALL', 6, 1, 8, 10),
(9, 'Barrière enflammée', 'SPELL_BARRIER', 6, 1, 5, 0),
(12, 'Poing du démon', 'SPELL_DEMONPUNCH', 7, 1, 6, 0),
(6, 'Bassin Divin', 'SPELL_SPRING', 5, 1, 9, 10),
(11, 'Boule de feu', 'SPELL_FIREBALL', 7, 1, 8, 0),
(13, 'Brasier', 'SPELL_FIRE', 7, 2, 10, 10),
(14, 'Sang de lave', 'SPELL_BLOOD', 7, 2, 8, 0),
(10, 'Bouclier magique', 'SPELL_SHIELD', 6, 2, 5, 0),
(15, 'Souffle de négation', 'SPELL_NEGATION', 6, 2, 8, 0),
(16, 'Pluie sacrée', 'SPELL_RAIN', 5, 2, 9, 10),
(17, 'Recharge du bassin', 'SPELL_RELOAD', 5, 2, 5, 10),
(18, 'Soleil de guérison', 'SPELL_SUN', 5, 2, 10, 10),
(19, 'Bulle de vie', 'SPELL_BUBBLE', 6, 2, 8, 0),
(20, 'Piliers infernaux', 'SPELL_PILLARS', 7, 2, 9, 10),
(21, 'Malédiction d''Arcxos', 'SPELL_CURSE', 8, 1, 8, 0),
(22, 'Ailes de colère', 'SPELL_ANGER', 8, 1, 8, 0),
(23, 'Téléportation', 'SPELL_TELEPORT', 8, 2, 8, 0),
(24, 'Toucher de lumière', 'SPELL_LIGHTTOUCH', 8, 2, 5, 0),
(25, 'Projection de l''âme', 'SPELL_SOUL', 8, 1, 5, 0),
(26, 'Rappel', 'SPELL_RECALL', 8, 2, 12, 0),
(27, 'Appel du feu', 'SPELL_FIRECALL', 7, 1, 10, 0);

-- --------------------------------------------------------

--
-- Structure de la table `BasicMaterial`
--

CREATE TABLE IF NOT EXISTS `BasicMaterial` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT 'Fer',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `BasicMaterial`
--

INSERT INTO `BasicMaterial` (`id`, `name`) VALUES
(1, 'iron'),
(2, 'wood'),
(3, 'scale'),
(4, 'leather'),
(5, 'linen'),
(6, 'root'),
(7, 'seed'),
(8, 'leaf'),
(9, 'emerald'),
(10, 'ruby');

-- --------------------------------------------------------

--
-- Structure de la table `BasicMission`
--

CREATE TABLE IF NOT EXISTS `BasicMission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL DEFAULT '',
  `frequency` float NOT NULL DEFAULT '0',
  `success` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `BasicMission`
--

INSERT INTO `BasicMission` (`id`, `name`, `frequency`, `success`) VALUES
(1, 'Mission : Escorter', 10, ''),
(2, 'Mission : Libérer une Ville', 10, ''),
(3, 'Mission : Formule', 0, ''),
(4, 'Mission : Trésor', 10, ''),
(5, 'Mission : Tuer des Monstres', 0, ''),
(6, 'Mission : Transporter un Objet', 0, ''),
(7, 'Mission : Énigme', 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `BasicProfil`
--

CREATE TABLE IF NOT EXISTS `BasicProfil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `hp` tinyint(4) NOT NULL DEFAULT '0',
  `strength` tinyint(4) NOT NULL DEFAULT '0',
  `dexterity` tinyint(4) NOT NULL DEFAULT '0',
  `speed` tinyint(4) NOT NULL DEFAULT '0',
  `magicSkill` tinyint(4) NOT NULL DEFAULT '0',
  `armor` tinyint(4) NOT NULL DEFAULT '0',
  `attack_bm` tinyint(4) NOT NULL DEFAULT '0',
  `defense_bm` tinyint(4) NOT NULL DEFAULT '0',
  `damage_bm` tinyint(4) NOT NULL DEFAULT '0',
  `magicSkill_bm` tinyint(4) NOT NULL DEFAULT '0',
  `fightStyle` int(7) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=264 ;

--
-- Contenu de la table `BasicProfil`
--

INSERT INTO `BasicProfil` (`id`, `name`, `hp`, `strength`, `dexterity`, `speed`, `magicSkill`, `armor`, `attack_bm`, `defense_bm`, `damage_bm`, `magicSkill_bm`, `fightStyle`) VALUES
(100, 'Rat géant', 5, 5, 4, 1, 0, 2, 2, 0, 2, 0, 3),
(101, 'Crapaud géant', 7, 5, 3, 0, 0, 2, 0, 0, 4, 0, 3),
(102, 'Chauve-souris géante', 2, 2, 4, 7, 0, 1, 1, 4, 0, 0, 1),
(103, 'Kobold', 3, 0, 5, 2, 5, 1, 1, 0, 0, 4, 2),
(104, 'Loup', 2, 3, 5, 5, 0, 1, 3, 0, 2, 0, 2),
(105, 'Araignée éclipsante', 3, 2, 4, 6, 0, 1, 1, 4, 0, 0, 1),
(106, 'Fourmi guerrière géante', 3, 6, 5, 1, 0, 1, 4, 0, 1, 0, 3),
(107, 'Fourmi Reine', 6, 5, 4, 0, 0, 3, 0, 0, 3, 0, 3),
(108, 'Homme Lézard', 3, 5, 6, 1, 0, 3, 2, 0, 1, 0, 3),
(109, 'Mange-Coeur', 3, 6, 4, 2, 0, 2, 0, 0, 4, 0, 3),
(110, 'Araignée Sabre', 2, 1, 5, 7, 0, 1, 1, 4, 0, 0, 1),
(111, 'Araignée piègeuse géante', 4, 3, 7, 1, 0, 1, 4, 1, 0, 0, 2),
(112, 'Ame en peine', 4, 6, 4, 1, 0, 2, 1, 0, 3, 0, 1),
(113, 'Fantôme', 2, 0, 3, 4, 6, 1, 0, 1, 0, 4, 1),
(114, 'Esprit terrifiant', 3, 6, 4, 0, 2, 1, 3, 2, 0, 0, 3),
(115, 'DoppleGanger', 2, 4, 2, 2, 5, 1, 0, 0, 2, 3, 2),
(116, 'Assassin Runique', 3, 1, 8, 3, 1, 2, 2, 0, 2, 0, 2),
(117, 'Ange noir', 3, 0, 0, 5, 7, 0, 0, 2, 0, 4, 4),
(118, 'Gobelin', 3, 3, 5, 4, 0, 1, 3, 0, 2, 0, 2),
(119, 'Shaman Gobelin', 2, 0, 4, 2, 7, 0, 0, 0, 0, 5, 4),
(120, 'Gobelin Archer', 2, 0, 9, 4, 0, 0, 3, 2, 0, 0, 2),
(121, 'Goule', 4, 6, 4, 1, 0, 2, 0, 0, 4, 0, 3),
(122, 'Ombre', 2, 0, 0, 5, 8, 0, 0, 1, 0, 5, 4),
(123, 'Blême', 3, 0, 0, 6, 6, 0, 0, 0, 4, 2, 4),
(124, 'Golem de chair', 7, 5, 3, 0, 0, 3, 0, 0, 3, 0, 3),
(125, 'Momie', 3, 0, 4, 2, 6, 1, 0, 0, 0, 5, 4),
(126, 'Squelette', 4, 5, 5, 1, 0, 2, 2, 0, 2, 0, 2),
(127, 'Gobelours', 7, 5, 3, 0, 0, 3, 1, 0, 2, 0, 3),
(128, 'Orc', 4, 4, 6, 1, 0, 2, 3, 0, 1, 0, 2),
(129, 'Horreur des sous bois', 5, 0, 5, 5, 0, 0, 4, 0, 0, 0, 1),
(130, 'Fée des bois', 1, 0, 2, 4, 8, 0, 0, 2, 0, 4, 4),
(131, 'Mantes prêtresse', 3, 0, 9, 3, 0, 1, 1, 0, 4, 0, 2),
(138, 'Feu Fol', 3, 0, 4, 2, 6, 0, 0, 2, 0, 4, 4),
(253, 'Kradjeck Ferreux', 5, 2, 2, 5, 1, 5, 0, 0, 0, 0, 1),
(263, 'Tortue Géante', 8, 4, 2, 1, 0, 5, 0, 0, 0, 0, 1),
(132, 'Hobgobelin', 5, 5, 5, 0, 0, 4, 1, 0, 2, 0, 3),
(133, 'Troll', 5, 2, 8, 0, 0, 2, 2, 0, 2, 0, 2),
(134, 'Necromancien', 2, 0, 5, 0, 8, 0, 2, 0, 0, 4, 4),
(135, 'Zombi', 3, 3, 5, 1, 2, 1, 2, 0, 1, 0, 2),
(136, 'Combattant squelette', 7, 2, 2, 2, 2, 4, 2, 0, 2, 0, 2),
(144, 'Serpent Aqualien', 4, 6, 5, 0, 0, 1, 2, 0, 3, 0, 3),
(145, 'Abobination des marais', 5, 4, 4, 2, 0, 2, 2, 0, 2, 0, 2),
(146, 'Basilic Majeur', 3, 0, 5, 2, 5, 1, 2, 0, 0, 4, 4),
(147, 'Basilic mineur', 3, 2, 4, 6, 0, 0, 4, 0, 2, 0, 1),
(148, 'Manticore', 5, 5, 5, 0, 0, 1, 4, 0, 2, 0, 2),
(154, 'Etranglesaule', 7, 6, 3, 0, 0, 4, 4, 0, 0, 0, 3),
(156, 'Gargouille', 4, 2, 5, 4, 0, 4, 1, 0, 4, 0, 2),
(155, 'Sauteur Fauchard', 4, 7, 4, 0, 0, 1, 2, 0, 4, 0, 3),
(157, 'Golem d''Argile', 7, 5, 3, 0, 0, 3, 4, 0, 1, 0, 3),
(158, 'Dopplegangeur majeur', 2, 2, 2, 2, 7, 0, 0, 4, 0, 0, 1),
(159, 'Araignée titanesque', 7, 4, 4, 0, 0, 2, 4, 0, 1, 0, 2),
(160, 'Golem de bière', 4, 2, 6, 3, 0, 0, 2, 0, 2, 0, 2),
(161, 'Dragon de ven', 5, 0, 2, 4, 4, 0, 0, 3, 0, 3, 1),
(170, 'Araignée Colossale', 6, 4, 4, 1, 0, 2, 2, 0, 3, 0, 2),
(171, 'Gnoll', 3, 3, 4, 3, 2, 1, 3, 2, 0, 0, 2);

-- --------------------------------------------------------

--
-- Structure de la table `BasicRace`
--

CREATE TABLE IF NOT EXISTS `BasicRace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_BasicProfil` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '0',
  `gender` enum('M','F','N') NOT NULL DEFAULT 'M',
  `pic` varchar(32) NOT NULL DEFAULT '',
  `PC` enum('Yes','No') NOT NULL DEFAULT 'No',
  `alignment` varchar(50) NOT NULL DEFAULT '',
  `class` varchar(32) NOT NULL DEFAULT '',
  `state` enum('walking','flying') NOT NULL DEFAULT 'walking',
  `dropItem` int(11) NOT NULL DEFAULT '0',
  `percent` int(7) NOT NULL DEFAULT '100',
  `frequency` tinyint(4) NOT NULL DEFAULT '0',
  `land` enum('forest','plain','mountain','march','artic','desert') NOT NULL DEFAULT 'plain',
  `band` smallint(7) NOT NULL DEFAULT '0',
  `behaviour` int(11) NOT NULL DEFAULT '1',
  `levelMin` smallint(6) NOT NULL DEFAULT '0',
  `levelMax` smallint(6) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=264 ;

--
-- Contenu de la table `BasicRace`
--

INSERT INTO `BasicRace` (`id`, `id_BasicProfil`, `name`, `gender`, `pic`, `PC`, `alignment`, `class`, `state`, `dropItem`, `percent`, `frequency`, `land`, `band`, `behaviour`, `levelMin`, `levelMax`, `description`) VALUES
(1, 1, 'Humain', 'M', 'human.jpg', 'Yes', '', 'humanoïde', 'walking', 0, 100, 3, 'plain', 0, 1, 1, 150, 'De loin les plus nombreux parmi les quatre Races Ainées, les hommes forment également la population la plus métissée, et la plus - et de loin - diverse. Présent dans toutes les régions de l''Île, des sables du désert Safran jusqu''aux plaines glacées du grand nord, ils semblent pouvoir s''adapter aisément à tous les environnements et à toutes les situations. Leur puissance s''exprime par le nombre mais aussi par une prodigieuse soif de connaissance et de savoir, qu''ils partagent semblent-ils avec un don pour l''apprentissage. De carrure plus robuste que les elfes et les doranes, ils peuvent aussi bien être d''excellents guerriers, bretteurs habiles, comme de puissants magiciens. Souvent très endurants, ils aiment à parcourir les vastes plaines centrales de Nacridan...'),
(2, 2, 'Elfe', 'M', 'elf.jpg', 'Yes', '', 'humanoïde', 'walking', 0, 100, 3, 'plain', 0, 1, 1, 150, 'Fins et graciles, les elfes aux cheveux teints et aux longues oreilles dépourvues de lobes se considèrent comme le Beau Peuple, et il est vrai que peu parmi les races ainées peuvent prétendre les égaler en charisme. Peuple de nature et de magie, les elfes révèrent la Mère Nature et les esprits de l''air, auquel ils rendent grâce pour leur œil infaillible ainsi que leur habilité proprement démoniaque à l''arc. Les elfes accordent une grande importance aux arts, dont ils tirent grand plaisir et satisfaction, et au nombre desquels ils comptent musique, poésie, théâtre ainsi que magie, dont ils sont souvent des pratiquants redoutablement doués, comme en témoigne le Centre elfique de magie, en leur ville de Krima.'),
(3, 3, 'Nain', 'M', 'dwarf.jpg', 'Yes', '', 'humanoïde', 'walking', 0, 100, 3, 'plain', 0, 1, 1, 150, 'Presque aussi larges qu''ils sont hauts, les fiers aventuriers nains à la barbe fournie et aux sourcils broussailleux sont en général des combattants féroces et tenaces, mus par un grand sens de l''honneur et du clan - ainsi, disent les mauvaises langues, que par une atavique soif de l''or. Ils portent le plus souvent de lourdes armures de plate forgées par leur soin, et gardent leurs haches de guerres aiguisées avec un soin jaloux. Qu''ils viennent des profondes mines de leur cité d''Octobian ou qu''ils aient franchi les Mers jusqu''aux rivages de l''Île, ce sont des artisans plus que compétents, et bien qu''ils soient dur en affaire, ils se révèlent souvent des marchands honnêtes. Courageux, les nains affectionnent la bagarre, surtout après quelques bières, dont ils font une consommation importante.'),
(4, 4, 'Dorane', 'M', 'dorane.jpg', 'Yes', '', 'humanoïde', 'walking', 0, 100, 3, 'plain', 0, 1, 0, 150, 'Venus dans leurs nefs aux flancs nacrés des lointains Archipels de Dol''Onoran et Sul''Mandar, les doranes forment un peuple mystérieux et farouche, apparenté aux elfes plutôt qu''aux hommes, lesquels ont longtemps craint ces sorciers à la peau violette et aux longs cheveux blancs, dont les arts mystiques surpassaient en puissance les autres magies. Car les doranes sont un peuple de magie par excellence, et celle-ci prend place dans chaque aspect de leur vie, étant une véritable religion. Généralement grands et fins, le port altier, les doranes ne vivent guère plus longtemps que les hommes, et même souvent moins, ce qui est considéré comme la juste rétribution des dieux pour tant de puissance, de même que l''on considère que leur présence en Izandar amena de grands malheurs sur la ville.'),
(100, 100, 'Rat géant', 'M', 'rat.png', 'No', 'neutre', 'mammifère', 'walking', 500, 100, 3, 'plain', 1, 3, 1, 6, 'Les Rats Géants déferlent en horde dans la campagne et constituent un véritable fléau depuis la nuit des temps. Car ces mutations monstrueuses du rongeur commun ne se contentent pas de grignoter les réserves de blé, ni même de saccager les chaumières des paysans : ils sont volontiers carnivores et charognards et véhiculent des maladies pestilentielles. En outre, ils sont dotés d’une intelligence nocive et n’hésitent pas à s’attaquer aux villes. Leur technique procède alors en deux temps : une première vague de ces bêtes assaille les barricades jusqu’à ce que l’une d’elles arrive à se faufiler dans les rangs des défenseurs et à y propager les infections par le biais de ses parasites ; une fois les lignes ennemies ainsi affaiblies, les pernicieuses créatures envahissent en nombre la citadelle et en investissent le moindre recoin. Elles restent alors tapies jusqu’à ce qu’une proie affaiblie par les fièvres passe à leur portée et se repaissent de sa dépouille avant d’assurer la multiplication des générations futures. Qui bientôt tenteront de dévaster d’autres régions.\r\n\r\n[i]« Autrefois le rat des villes[/i]\r\n[i]Invita le rat géant[/i]\r\n[i]D’une façon fort hostile[/i]\r\n[i]Celui-ci s’en vint séant.[/i]\r\n\r\n[i]Sur un tapis de cadavres[/i]\r\n[i]Le couvert se trouva mis.[/i]\r\n[i]Mais soudain dans les charpies[/i]\r\n[i]Le gros rat du p’tit se gave… »[/i] [i]Comptines des Grandes Pestes[/i] de Khaâ-Mû, éleveur d''orques depuis 25 ans.'),
(101, 101, 'Crapaud géant', 'M', 'crapaud.jpg', 'No', 'neutre', 'batracien', 'walking', 502, 100, 3, 'plain', 1, 4, 1, 6, 'Cet énorme crapaud est le produit d’une manipulation magique incontrôlée. Censée servir d’arme sonore en faisant exploser les cervelles de ses victimes par l’effet de ses coassements, cette expérimentation fut abandonnée à la fin de l’Hegemon Artassien. Cependant, l’espèce se montra étonnamment viable. Son habitude agaçante de se placer à proximité d’un campement pour pousser ses hurlements sadiques toute la nuit la rend dangereuse. \r\n\r\nEn effet, ce bruit abominable et incessant peut rapidement provoquer des crises de nerfs chez les aventuriers les moins endurcis, susceptibles de se lever en hurlant au milieu de la nuit pour charger aveuglément, épée en main, sans même penser à enlever leur pyjama ou à passer une côte de maille. Le venin que suppure cette bête est toutefois recherché, et de nombreux jeunes aventuriers voient dans sa récolte un moyen rapide de faire fortune. Nombreux sont ceux qui meurent dans d’atroces souffrances, les tympans percés par un coassement tueur.\r\n\r\n[i]« Avec les bouchons d’oreille Joyeux Corsaire ™, pars à la chasse au crapaud parfaitement couvert ! Satisfait ou remboursé (sur preuve) »[/i] Réclame parue dans le [i]Barbare déchaîné[/i] d’Earok.'),
(102, 102, 'Chauve-souris géante', 'F', 'bat-online-version.png', 'No', 'neutre', 'mammifère', 'flying', 501, 100, 3, 'plain', 1, 2, 1, 6, 'Le battement obscur d’ailes de cuir sombre dans la nuit. Un corps massif fendant les airs nocturnes sous la hâve lueur d’Aryn. Le chant lancinant d’un orgue dans un château au sein du Scercz''biec. Le vol noir de la Chauve-ouris Géante sur les plaines de Nacridan provoque la terreur sous sa large ombre maléfique ; quand le crépuscule vient, c’est en vastes groupes que ce féroce carnivore n’hésite nullement à s’en prendre à ses victimes : monstres, bêtes, Races Aînées, tout lui est bon pour nourrir son abominable appétit. Tombant de tout son poids considérable sur le dos de sa proie, elle le fouette avec ses lourdes ailes de cuir aux griffes proéminentes, tout en lui plongeant ses crocs effrayants dans la nuque, pour aspirer son sang. Contrairement aux rumeurs, il ne s’agit pas, néanmoins, d’un véritable vampire, et si elle ne brûle pas au soleil, un bon coup d’un froid acier bien affûté saura tranquilliser définitivement ce monstre. Domestiquée, elle sert efficacement de messager ou de Familier, et procure un important bonus au charisme pour les nécromanciens et autres êtres de la nuit.\r\n\r\n[i]« … c’était par une nuit obscure, m’sieur Zvountz. Mon camarade Wanderbegen était sorti fumer une pipe d’herbe-qui-fait-sourire-bêtement… c’est Adah qui lui avait refilée, moi j’y ai pas touché, ça me fait tousser, je vous le jure… Et puis il y a eu un bruit comme un gros truc qui ferait flap-flap, et… et quand je suis sortie, il était partiiiiiiii-i-i-i… sans… SANS MÊME ME DIRE AU REVOIR ! »[/i] Mes traumatismes, par Llyn.'),
(103, 103, 'Kobold', 'M', 'kobold.jpg', 'No', 'chaotique,mauvais', 'humanoïde', 'walking', 0, 100, 3, 'plain', 2, 7, 4, 6, 'Ces petites créatures à la fourrure fauve et à tête de chien, rappelant celle d’un cocker mais en plus laid, sont assez pathétiques. D''une force très peu développée, à peine capable de brandir un coutelas, elles sont en outre d''une rare stupidité. Ce n''est pas pour rien qu''un proverbe izandrin dit : [i]« Aussi bête qu''un kobold dans une colonne de paladins. »[/i] En vérité, leur seule chance de parvenir à mettre un terme à l''existence d''un aventurier est de le prendre par surprise pendant son sommeil ou de lui jeter une boule de feu. Par une espèce d''humour assez saumâtre de Dame Nature, les kobolds comptent en effet au nombre de leurs très rares aptitudes celles de produire à volonté de petites sphères incandescentes, dont ils se servent en général pour organiser de complexes parades enflammées pour trouver une partenaire, ou pour chasser les papillons et les oiseaux dont ils font une consommation quasi exclusive, bien que ne dédaignant pas à l''occasion fruits sauvages et racines, voire une charogne ou deux. \r\n\r\nContrairement à son lointain parent le Gnoll, toutefois, le kobold prend toujours soin de bien faire cuire la viande qu''il consomme, au risque néanmoins de s''enflammer lui-même, ce qui arrive hélas bien plus souvent qu''on ne le croit. Il y a peu de mérite à affronter pareille aberration de la nature, mais les plus jeunes aventuriers ne manquent pas de les massacrer pour peu qu''ils en trouvent. En plus de pourvoir à leur éducation, cela leur procure un divertissant spectacle de sons et lumières.\r\n\r\n[i]«[/i] - [i] Bon, les gens, y a plus d''ambiance, là, s''écria Balin à la cantonade. Et si on jouait à Paf le kobold ?»[/i] Les folles soirées de la Taverne de la Tortue Boiteuse, par Dame Amlika.'),
(104, 104, 'Loup', 'M', 'loup.jpg', 'No', 'neutre', 'mammifère', 'walking', 1, 100, 3, 'plain', 2, 5, 4, 6, 'Ce loup est d''une taille assez imposante. Sa riche fourrure grise et noire est très convoitée par les habitants des villes, mais, hélas, il rechigne bien trop souvent à s''en séparer au profit des aventuriers qui s''en viennent là lui prendre. Muni de crocs impressionnants, il n''hésitera jamais à tailler une couenne ou deux à ceux qui s''approchent trop de lui, et ses puissantes mâchoires peuvent parfaitement suffire à arracher une gorge à découvert, par exemple. Néanmoins, il craint en général les races aînées et n''ira pas les attaquer, à moins qu''il ne soit en nombre ou affamé et désespéré. On cite alors bien souvent la fois où les loups entrèrent dans Earok, durant ce terrible hiver de 3E555. La population en fut d''ailleurs bien heureuse, car on n''avait plus de viande comestible depuis bien longtemps. \r\n\r\n[i]«… Faites bien attention ma petite dame, ce n''est pas un chien que vous avez là, c''est un fauve ! [/i]– [i]Je suis certaine qu''il me fera aucun mal, répliqua Amlika»[/i] Croc Jaune, d''Amlika'),
(105, 105, 'Araignée éclipsante', 'F', 'araignee-eclipsante.jpg', 'No', 'Neutre', 'Arachnide', 'walking', 504, 70, 3, 'plain', 2, 6, 4, 6, 'Dans une île où chaque espèce d''araignée semble être gagnée d''une aérophagie proportionnelle à son avidité pour la chair humaine, l''araignée éclipsante apparaît presque comme un soulagement. Bien que son venin soit mortel, cet arachnide au corps noir et luisant est de taille modeste, à peine plus d''un mètre de haut, et n''est même pas très agressif. A vrai dire, elle ferait une proie facile pour tout jeune aventurier en quête d’exercice et, dans un monde où chaque village est habitué à recevoir la visite d''armées mortes-vivantes, de dragons enragés ou d''Alicia de Lamb tous les cinq à sept, l''araignée éclipsante aurait bien du mal à gagner le respect de ses monstrueux collègues, si elle n''avait la capacité de se téléporter en un clin d’œil à n''importe quel endroit de son champ de vision. Loin de ton épée. Hors de portée de ton arc. Làààà !! derrière, dans ton dos… ah, criss, trop tard… \r\n\r\n[i]« Encore une putain d''araignée !! J''en ai marre des araignées !! AAAAAAAAAH !! [i]« Pourquoi j''ai mangé mon pied avant de me taper la tête contre un arbre en chantant »[/i], de Suma Dartson '),
(106, 106, 'Fourmi guerrière géante', 'F', 'fourmi.jpg', 'No', 'neutre', 'insecte géant', 'walking', 2, 100, 3, 'plain', 3, 8, 4, 6, 'Malgré sa taille qui peut atteindre plusieurs coudées de haut, la Fourmi Guerrière Géante ne semble pas extrêmement menaçante sauf pour les plus frêles combattants. Cependant, cette mignonne bestiole couverte d’une carapace d’un noir profond compte bien plus sur la vie en société que sur sa force propre. Les Fourmis Guerrières ont en effet développé une technique redoutable de chasse en groupe afin de protéger leur nid, et surtout leur reine pour laquelle elles se battront jusqu’à la mort. En peloton d’une douzaine d’individus, ces insectes défendent leur tanière avec acharnement et ne sont pas dénués d’intelligence, ils ont même développé une certaine faculté magique assimilable aux Arcanes Mineures; chacun, lorsqu’il se sent menacé, a bien vite fait d’appeler ses compagnes à la rescousse qui ne tardent pas à l’épauler dans le combat.\r\n\r\n[i]« Flûte alors ! Encore des fourmis qui viennent boulotter mon cheval ! »[/i] De la Nature et des Joies du Pique[/i] - [i]nique, de Luc de Retz'),
(107, 107, 'Fourmi Reine', 'F', '', 'No', 'Neutre', 'insecte', 'walking', 503, 50, 1, 'plain', 3, 9, 4, 6, 'La Reine Fourmi est la première cause des infestations d''insectes géants sur les terres de Nacridan. D''une grande intelligence, ces créatures sont heureusement rares : capables de pondre des centaines de combattantes en quelques jours, elles peuvent réellement faire apparaître des armées entières de fourmis guerrières, qui les défendront jusqu''à la mort, voire, si un nécromancien passe par-là, après celle-ci. Fondamentalement, la fourmi, fût-elle géante, n''est pas très dure à tuer, mais, hélas, elles sont fichtrement nombreuses. En outre, leur reine est capable d''émettre des phéromones d''une grande puissance, qui rendent littéralement folles furieuses les guerrières. Handicapée par son gigantisme et le poids de sa lourde cuirasse de chitine aux tons d''ébène et d''argent, la Reine Fourmi est parfois vulgairement appelée [i]« outre à expérience »[/i] par les jeunes aventuriers, mais de toute façon, ces gens-là ne respectent plus rien, ça n''était pas pareil de mon temps, hein mâme Durant.\r\n\r\n[i]« Avec la masse d''arme Tonfa, réglés, tes problèmes de fourmis ! Approuvé par les services de sécurité de la Fédération ! Également utile pour le contrôle des foules »[/i] Réclame parue dans La Vraie Vérité, magazine de la Fédération des Peuples Libres.'),
(108, 108, 'Homme-lézard', 'M', 'homme-lezard.jpg', 'No', 'neutre', 'humanoïde', 'walking', 0, 100, 3, 'plain', 4, 10, 7, 14, 'Les Hommes-Lézards forment un peuple étrange. Des légendes racontent qu’ils proviendraient de lointaines îles à l’est de Nacridan mais hormis les corsaires de Dust, rares sont ceux qui parviennent à en revenir, malgré les multiples expéditions menées en vue de se saisir des fabuleuses réserves de perles que recéleraient leurs atolls d’origine. Bien qu’inhumaine, leur civilisation est pourtant avérée : ils possèdent  leur propre langage sacré, mais parlent aussi la langue des races aînées, bâtissent des constructions gigantesques dans les marais et les jungles qu’ils affectionnent, vénèrent des dieux terribles et sont prêts à dépecer leur prochain pour quelques piécettes d’or. \r\n\r\nBien que bipède, l’Homme-Lézard ressemble plutôt à un varan qu’à un homme, avec sa peau écailleuse verdâtre et ses grands yeux glauques, ses mains palmées et sa gueule proéminente munie d’une multitude de fines dents acérées. Lorsque, poussés par la surpopulation, par leur goût du lucre ou afin d’honorer dignement leurs puissances divines, ils sortent en troupes effrénées de leur repaire, montant dans leurs longs bateaux-serpents pour fondre sur les côtes de Nacridan, ils constituent une réelle menace pour les villages côtiers, tant leur cruauté est grande. \r\n\r\nEn dépit des efforts des corsaires de Dust et des seigneurs de la Grève Lointaine, ils prélèvent trop souvent dans les chaumières de pauvres victimes afin de les offrir en sacrifice à Rastapoulatepec, leur sombre et sanglante divinité tutélaire. Seuls, ils se montrent non moins perfides et abominables. Leur langue fourchue darde des sarcasmes persifleurs tandis qu’ils comptent sur leur haute stature et sur leurs puissants membres griffus pour leur assurer l’avantage sur leur victime, qu’ils dépouilleront consciencieusement de tous ses biens une fois leur forfait accompli.\r\n\r\n[i]« Mon habitude n’est pas de faire d’amalgames, mais il faut bien reconnaître que ces hordes d’hommes[/i] - [i]lézards posent un sérieux problème de sécurité. Un Homme[/i] - [i]Lézard, ça va. C''est quand il y en a plusieurs que ça pose des problèmes !»[/i] Discours d’investiture d''El Zvountz comme Maître de Chasse à la Fédération des Peuples Libres.'),
(109, 109, 'Mange-Coeur', 'M', '', 'No', 'Neutre', 'Humanoïde', 'walking', 201904, 35, 1, 'plain', 4, 11, 7, 14, 'À la fois prêtres, guerriers et stratège, les Mange-Cœurs à l''éclatante collerette sont les commandants des hommes-lézards, désignés par ces guerriers pour mener le raid sur les terres adverses, et oints par les grands prêtres de Rastapoulatepec, le dieu au cœur noir. Tout homme-lézard peut en droit être Mange-Cœur, mais dans les faits, l''admiration de ces guerriers devant être obtenue, ce sont en général les plus coriaces et les plus forts des leurs qui obtiennent cet honneur. Une certaine aisance est également nécessaire pour payer les provisions et le bateau serpent qui mènera le raid. \r\n\r\nPortant des semi-armures somptueusement décorées aux couleurs sanglantes du dieu lézard, les Mange-Cœurs affectionnent en général des armes tarabiscotées aux lames étranges, dont le poids et la taille symbolisent leur force et leur ascendant psychologique sur l''ennemi. Rares sont ceux en effet parmi les races aînées qui ne peuvent se cacher d''un léger frémissement – voire d''une légère humidité au niveau des chausses – en voyant débouler soudain du sous-bois une horde mugissante et sifflante de monstres plus grands que des hommes, brandissant sabres et haches, dans le but de massacrer tout le monde, et, détail cocasse, de dévorer leur cœur. Assez littéralement : le cœur des ennemis morts – voire encore vivants – étant considéré comme un mets de choix. Il n''est pas rare, d''ailleurs, de voir un Mange-Cœur accomplir ce fait au beau milieu d''un champ de bataille. C''est même le meilleur moment pour mettre fin à ses méfaits d''un bon coup de lance bien placé, mais curieusement, la majorité des conscrits ne sont pas pressés de l''approcher. \r\n\r\n[i]« - Ssss''… Je vaisss manger votre cœur !! Mouhahahaaaaargll.[/i]\r\n[i] - Se vanter ou tuer, il faut choisir »[/i], Apocryphes de Bataille, de Jorel Tapeau.'),
(110, 110, 'Araignée Sabre', 'F', 'araignee-sabre.jpg', 'No', 'Neutre', 'Arachnide', 'walking', 2, 100, 3, 'plain', 5, 12, 7, 24, 'Pour être dépourvue de venin, cette araignée aussi grande qu''un homme n''en est pas moins redoutable. Outre son apparence absolument répugnante, qui est susceptible de faire flancher les cœurs les plus solides, cette créature massive au torse trapu est un adversaire coriace : recouverte d''épaisses plaques de chitine brunâtre, elle est capable de se déplacer à une vitesse foudroyante, usant de ses pattes antérieures comme de véritables armes de guerre dont les bords sont plus tranchants que des rasoirs et aussi solides que de l''acier. Dressée sur ses pattes postérieures, si elle ne vous a pas déjà sournoisement poignardé dans le dos, l''Araignée Sabre utilise ses lames pour combattre ses rivales comme pour tuer ses proies.\r\n\r\nQuant à la protection d’une armure pour se garantir de ses assauts, il ne faut pas y compter : ses coups transpercent le cuir bouilli et la maille comme du lin. Seule une épaisse plate pourra avoir quelque chance d’y résister. Si les jeunes araignées sont faciles à défaire, les plus âgées sont d''une taille colossale et leur science du corps à corps est immense. Dans tous les cas, il est vivement recommandé de se tenir à distance et de s''en servir comme cible de tir à l''arc.\r\n\r\n[i]« Messire, vite, messire ! Elle arrive !!![/i]\r\n[i]– Roh, deux secondes, la corde de l''arbalète vient de lâcher… »[/i]\r\nDernières paroles de Godefroy de Noirmarée et de son écuyer, Hughes de Père inconnu, in [i]Vie inutiles[/i], de Plute Orque.'),
(111, 111, 'Araignée piègeuse géante', 'F', 'araignee-piegeuse.jpg', 'No', 'Neutre', 'Arachnide', 'walking', 1, 100, 3, 'plain', 5, 13, 7, 24, 'Une immense araignée aussi haute qu’un nain et deux fois plus large, noire et velue, sur laquelle se dessine d’un jaune tranchant l’image morbide d’une tête de mort, telle est l’Araignée Piégeuse. Elle se tient bien souvent tapie dans l’ombre d’une grotte ou dans de vastes terriers qu’elle creuse à même le sol. Mais son ouïe est si fine qu’elle est capable de repérer sa proie à des lieues à la ronde. Ses yeux aux multiples facettes luisent d’un regard maléfique lorsqu’elle s’élance sur sa cible, brillant d’un rouge vif et signifiant à sa future victime [i]« il est trop tard »[/i]. \r\n\r\nMaligne en diable, furtive et insidieuse, l’araignée piégeuse s’y entend à isoler les groupes de voyageurs avant d’envoyer chaque individu dans un trou garni de ses toiles où elle pourra les dévorer tranquillement. Perpétuellement affamée, cette araignée est toujours si avide de chair fraîche que seuls les elfes des profondeurs, dit-on, sont parvenus à en dresser comme montures pour la chasse et la guerre. Malheur à qui croise alors leur chemin ! Ses griffes sont aussi féroces que son appétit, et son intelligence maléfique en fait une chasseresse redoutable.\r\n\r\n[i]« J’étais coincé au fond de son puits, et je voyais ses grands yeux rouges surmontant des mandibules terribles qui se rapprochaient. Heureusement, j’avais dû renoncer à mon bain annuel à cause de l’enterrement de la tante Gudrun, sinon elle m’aurait boulotté tout cru. L’araignée, pas la tante, quoique… »[/i]\r\nLe Testament des Héros Perdus de Balin le Brave'),
(112, 112, 'Ame en peine', 'F', 'ame-en-peine.jpg', 'No', 'Neutre', '', 'walking', 505, 20, 3, 'plain', 6, 14, 7, 14, 'Il est des rages si grandes. Il est des âmes si noires. Il est des êtres si torturés, des colères si terribles, que la Mort même ne parvient pas à apaiser ces malheureux. Frappés d''un anathème au-delà de la compréhension des hommes, leurs âmes torturées sont condamnées à errer par-delà les frontières des Plaines Ombreuses, bien après que leurs corps sont revenus à la terre, et leurs os dispersés. Quelle que soit la raison de leur éternelle errance, les âmes en peine hantent les lieux qui les ont vues périr, brûlant d''une haine inextinguible pour les êtres de chair et de sang sur lesquelles elles ne manquent pas d''exercer une vengeance sanglante sitôt qu''elles le peuvent. Ces formes éthérées semblent constituées d''une brume grisâtre à la phosphorescence malsaine, et rampent dans les airs accompagnées d''un murmure aussi inaudible qu''atroce. Si elles ne représentent pas, stricto sensu, un très grand danger du fait de leur débilité physique, leur manie de traverser les obstacles et de surgir sans crier gare, attaquant à outrance et sans aucun discernement tous ceux qui croisent leur chemin, les rendent redoutables pour les nerfs – et les vessies – les moins solides. \r\n\r\nMaître Rodroc, un vieux routard, se souvient : [i]« C''était atroce ; j''étais en train d''inventorier les possessions matérielles de feu un marchand qui venait de faire une mauvaise rencontre, le pauvre, quand j''ai entendu des cris de rage déments et que j''ai vu deux yeux injectés de sang qui me fondaient dessus. Mais en fait, ce n''était qu''Alicia de Lamb. »[/i]'),
(113, 113, 'Fantôme', 'M', 'fantome.jpg', 'No', 'variable', 'morts-vivant', 'flying', 505, 20, 3, 'plain', 6, 15, 7, 14, 'Cet esprit inapaisé a trouvé une mort en général aussi brutale que sanglante dans quelque coin perdu de l''Ile, de préférence les ruines romantiques d''un vieux château sur une falaise au bord de la mer ou une vieille auberge qui servait de repère à des brigands sanguinaires. \r\n\r\nDepuis sa mort, il hante les lieux qui l''ont vu périr, se réjouissant de l''arrivée de quelques insouciants inconnus, en les effrayant par ses gémissements, cris de souffrances, et autres piaulements infâmes les humains qui s''en approchent trop. Il peut même se révéler dangereux, en descellant sciemment des pierres. Les modèles un peu supérieurs sont même capables de manier avec une violence redoutable de lourdes chaînes rouillées dont ils se servent comme d''un fléau d''arme, utilisant les capacités de leur ectoplasme pour esquiver les coups reçus en échanges...\r\n \r\n[i]«Essentiellement, toute l’agressivité d''un revenant réside dans ce que je pourrais appeler l''habitus du trauma, vous voyez ?… [/i]\r\n[i]–Hou hou ! [/i]\r\n[i]–Alors, parlez[/i] [i]–moi donc un peu de votre mort, M. Jean sans Corps, vous voulez bien ?"»[/i] Introduction à la psychanalyse ectoplasmique, Siggi Lakhan'),
(114, 114, 'Esprit terrifiant', 'M', 'esprit-terrifiant.jpg', 'No', 'neutre,mauvais', 'morts-vivant', 'flying', 505, 20, 3, 'plain', 6, 16, 7, 14, 'Bien que peu le sachent, l’Esprit Terrifiant est un mélomane. Avant tout adepte du Sombre Barde Oggy Obbourne, il se plaît à accompagner son arrivée de musiques lancinantes et sataniques jouées sur des gammes impossibles. Mais cette mélopée funeste n’est rien comparée à son apparence. Évanescent dans les ténèbres, enveloppé de miasmes d’une couleur noirâtre digne du plus profond d’Outre-Styx, il répand un froid glacial autour de lui; et seuls ses yeux mortifères apparaissent parfois parmi les sournoises volutes de ce qui fut jadis un corps. \r\n\r\nIl s’aventure de nuit près des arbres desséchés ou dans les demeures qui ont vu son trépas, profitant des ténèbres pour insuffler d’horribles cauchemars aux occupants en leur chantant ses complaintes sinistres. Quasiment invisible dans la pénombre, ses yeux se repaissent alors du pauvre hère égaré entre le sommeil et la veille, entre la vie et la mort. Une panique incontrôlable accompagne son apparition, et plus d’un valeureux guerrier a lâché son arme et mouillé ses chausses en s’enfuyant devant l’Esprit Terrifiant.\r\n\r\n[i]« Un esprit terrifié est déjà à moitié mort. Pour qui se soucie du travail bien fait, il ne reste plus qu’à achever la tâche.»[/i] Ethique au Nécromant de Dame Mordsith.'),
(115, 112, 'Doppleganger', 'M', 'doppleganger.jpg', 'No', 'chaotique,mauvais', 'métamorphe', 'walking', 0, 100, 3, 'plain', 7, 17, 15, 24, 'Le Doppleganger est une créature décidément très étrange… Nul ne sait d’où il provient et nul ne saurait non plus prédire son comportement. Est-il la fabrication d’un mage dément ou l’incarnation d’un esprit de la forêt particulièrement perfide ? Sous sa forme originelle, le Doppleganger se présente comme une frêle créature à la peau grisâtre. Son visage dépourvu de traits n’est adouci que par deux grands yeux pâles comme la lune où semblent luire des questions enfantines. Mais gare à qui se laisserait aller aux sentiments ! Le Doppleganger se montre un redoutable étrangleur, enserrant dans ses mains noueuses le cou de ses proies dans une étreinte fatale. Il se plaît alors à changer son apparence en celle de sa victime, parfois durant des mois, imitant à la perfection son caractère et ses habitudes, jusqu’au jour où il jette son dévolu sur une nouvelle cible. Celle-ci, souvent abusée par les talents de métamorphe de la créature, se retrouve ainsi un beau jour rapidement étranglée à son tour. De nombreux témoignages rapportent qu’ainsi des familles et des guildes entières ont été décimées.\r\n\r\n[i]« Tiens, mademoiselle des Eaux d’Hêt, c’est fooormidable comme vous me rappelez quelqu’un, mais qui ? »[/i] \r\nSouvenirs de Magda Layne, de P. Rouste'),
(116, 116, 'Assassin runique', 'M', 'assassin_runique.jpg', 'No', 'neutre,mauvais', 'extra-planaire', 'walking', 0, 100, 3, 'plain', 7, 18, 15, 24, 'Produit de conjurations antiques et de sacrifices humains plus ou moins sanglants, l''Assassin Runique est un humanoïde élancé, au teint pâle comme le lait, à la tête étrangement chauve et boursouflée, dépourvue de bouche et de nez, et aux longs doigts graciles. Ses yeux noirs sont ternes et immobiles comme le néant des Contrées de l''Ombre. Il a une dague dans la main. Aucune porte ne le retiendra, aucune muraille ne ralentira son pas coulé et gracieux. Aucun volet ne détournera son regard fixe. Il est venu pour vous. \r\n\r\nCensés disparaître proprement une fois leur méfait accompli, les Assassins Runiques étaient autrefois très à la mode : on se les arrachait, depuis les milieux nécromanciens de Néphy, qui s''en servaient comme carton d''invitation, jusqu''aux riches nobles d''Artasse, qui ne voyaient pas d''objection à en lancer un sur un rival, une fois convenablement vêtu bien sûr. Toutefois, l''usage tend à se perdre aujourd’hui, et c''est bien regrettable… En effet, notoirement imparfaite, la conjuration qui donne naissance à l''Assassin Runique ne parvient pas dans tous les cas à l’ assujettir à sa mission, et un nombre important y échappe et erre éternellement dans les terres de Nacridan. Dépourvus de réel esprit, ils accomplissent alors dûment ce pourquoi ils sont venus à la [i]« vie »[/i], jusqu''à ce qu''une épée ou un sort ne vienne finalement mettre un terme à cette triste existence.\r\n\r\n[i]« Des volutes bleutées du brasero surgit la pâle forme de l’Assassin. Le Grand Mage l’aspergea copieusement de son mystérieux mélange soigneusement broyé en une fine poudre mais la chose eut tôt fait de s’enfuir par la fenêtre [/i]– [i]Mince, encore raté, j’ai dû mettre trop de sel ! »[/i], extrait de La prise de risque dans l''art des soins magiques, d''Aé Li.'),
(117, 117, 'Ange noir', 'M', 'ange-noir.jpg', 'No', '', '', 'walking', 0, 100, 3, 'plain', 7, 19, 15, 24, 'Au temps de la grandeur de Landar, nombreux étaient les prêtres-sorciers habiles à manier les puissances des ténèbres, créant et soumettant à leur pouvoir de nombreux êtres. Parmi les plus craints de leurs esclaves étaient les sinistres Anges Noirs, créatures impalpables faites de fumée obscure, et de malheur. Ombres parmi les ombres, les Ages Noirs portaient sans bruits ni murmures la sentence mortelle, frappant l''impie sans que pierre, fer ou flamme ne puisse l''arrêter. Difficile et coûteuse était leur création, et plus dur encore de les maintenir sous contrôle. \r\n\r\nIl n''était pas rare de voir une créature se retourner contre son maître et fuir dans les ténèbres en dessous du monde, se nourrissant de la peur et du désespoir. On dit que, par les nuits sans lune, certains des Anges remontent par des chemins noirs et cachés, inconnus des mortels, en quête de victimes pour apaiser leur soif inextinguible.'),
(118, 118, 'Gobelin', 'M', 'gobelin.png', 'No', '', '', 'walking', 0, 100, 4, 'plain', 8, 20, 15, 24, 'Petit, nerveux, et méchant, le gobelin aurait tout pour faire un excellent président de la République (de Tonak, bien sûr), s''il n''était aussi vert que faible et pleutre. Ne se déplaçant jamais qu''en vastes bandes, les gobelins sont méprisés sans vergogne par toutes les autres peuplades de Nacridan. Toutefois, ils sont loin d''être bêtes ; et nombreux sont ceux qui survivent dans l''ombre de grands seigneurs trolls ou orques en assurant des tâches vitales, telles que comprendre les mots longs de plus de deux syllabes ou compter les pièces d''or du dernier butin. Ce sont également des voleurs assez compétents, et des archers potables, qui peuvent profiter de leur grand nombre pour ensevelir l''ennemi sous une pluie de flèches.\r\n\r\n[i]«Sha''llaback hi hi hikk Shologlogoobb !»[/i] Aria du premier acte de Sosi fanglobglob Tutigalbrr, opéra gobelin en V actes et trois interludes. '),
(119, 119, 'Shaman Gobelin', 'M', 'gobelin-shaman.jpg', 'No', 'loyal,mauvais', 'humanoïde', 'walking', 111226, 35, 2, 'plain', 8, 21, 15, 24, 'Ossature de la société gobeline, les Chamans forment un ordre informel de prêtres, médecins, sorciers, au service d''un clan. S''ils se prétendent capables de tirer leurs pouvoirs des esprits de la nature ou des ancêtres, il s''agit plus probablement d''élémentaristes de faible niveau, tout au plus capable de générer une petite illusion ou de projeter quelques boules de feu. Toutefois, les plus puissants parmi les Chamans peuvent être assez redoutables pour des aventuriers mal préparés ou affaiblis. En outre, leur présence renforce considérablement la détermination des guerriers en combat, et rend ceux-ci plus dangereux, en soignant les blessés.\r\nUne rumeur de taverne prétend que les Chamans Gobelins se réunissent à la lune noire dans une caverne de Néphy pour dominer le monde. C''est assez improbable.\r\n\r\n[i]«Il y a des chamans gobelins puissants et il y a des chamans gobelins vieux. Mais pas les deux à la fois ! Alors, à l''attaque !!»[/i] Tueur d''orque, mémoires d''un guerrier, par Salazar'),
(120, 120, 'Gobelin Archer', 'M', '', 'No', '', '', 'walking', 131415, 35, 3, 'plain', 8, 22, 15, 24, 'Petits, verts et sournois, alternant entre la bassesse désagréable et la grandeur repoussante, les gobelins sont unanimement les créatures les plus méprisées de Nacridan. Néanmoins, Dame Nature ne conçoit pas qu''une créature puisse survivre en étant dépourvue du moindre talent, ne serait-ce que celui de s''enflammer soi-même et tous ces adversaires en cas de peur, comme le Kobold. Les gobelins sont donc, comparativement à la plus grande partie de leurs congénères peau-verte, intelligents. Un terme qu''il est bon de prendre avec toute la rigueur et les pincettes nécessaires, de peur qu''il ne vous agresse. \r\n\r\nLes archers sont les meilleurs combattants des forces gobelines. Ces guerriers d''élite relative arborent fièrement des marques tribales marron et jaunes, de nombreuses décorations dont ils tirent grand honneur et de larges coiffes décorées de plumes censées être arrachées à des phénix et autres griffons. Cependant, le possesseur de la coiffe dûment expédié à coups de masse dans un au-delà meilleur, ces fameuses plumes se  révéleront souvent provenir du poulailler voisin. Il faut craindre toutefois leurs flèches : un certain esprit sadique – et des siècles de frustration sanguinaire – s''exprimant par le biais de poisons extrêmement rares et aux effets aussi mortels qu''intensément douloureux. \r\n\r\n[i]«Recevoir la dernière flèche du gobelin»[/i]. Proverbe populaire d''Earok, désignant un individu condamné à subir une grande souffrance alors qu''il ne s''y attendait pas. S''emploie également lors de la réception de la feuille d''imposition.'),
(121, 121, 'Goule', 'F', 'goule.jpg', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 505, 20, 3, 'plain', 9, 23, 15, 24, 'Peau froide et grise, yeux pâles et vides, fins crocs, et une odeur déplaisante de décomposition, telle est la goule, qui se terre au fond des caveaux oubliés, pour n''en ressortir qu''à la lune éclose. Presque dépourvue de conscience, la goule est un mort-vivant assez agressif, en général asservi par un Bléme ou hantant simplement un cimetière mal entretenu, dans l''attente d''une nouvelle livraison de cadavres frais, qu''ils soient déjà morts ou en passe de l''être, quand ses crocs glacés se refermeront sur une gorge vulnérable. Dans la mesure où une pareille créature peut encore éprouver des sentiments, on suppose qu''elle se réjouit de pousser des piaulements terrifiants avant de passer brutalement à l''attaque. Assez résistante, la goule souffre malgré tout d''une grande vulnérabilité à la lumière du jour, et doit impérativement retrouver l''abri de l''obscurité quand se lève la brillante face du soleil, au grand dépit du Bléme qui voit ses plans d''invasion du village humain échouer encore une fois…\r\n\r\n[i]«Quoi ma goule ? Qu''est[/i] - [i]ce qu''elle a, ma goule ? J''exige un avocat !»[/i] Blème tentant de faire l''innocent après son arrestation par les villageois en colère in Scènes bucoliques de la vie rurale d''Adah.'),
(122, 122, 'Ombre', 'F', '', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 505, 20, 3, 'plain', 9, 24, 15, 24, 'Créatures mortes-vivantes et impalpables, les Ombres se terrent dans les entrailles de la terre quand brille le soleil, pour sortir des ténèbres quand ce dernier voile son sein. Dégageant un froid glacial, ces silhouettes floues, brumeuses, sont quasiment impossibles à distinguer dans la nuit. Seuls leurs yeux brûlants permettent de les distinguer. Inconsistantes, le fer et l''acier glissent sur elles sans les blesser, et seul le feu, ou la magie, pourra les détruire. Être saisi dans l’étreinte d’une Ombre est abominable. Aspirant votre énergie vitale comme un nain siffle une pinte, l’Ombre vous vole votre vie, ne laissant derrière elle qu’un cadavre épuisé. Si elle craint le feu et la lumière, elle se complaît à hanter les lieux perdus et sombres, les cavernes désolées, se nourrissant des rares vivants assez fous pour venir les chercher. Ceux qui sont morts dans les bras d''une Ombre peuvent revenir des morts sous la même forme.\r\n\r\n[i]« En Ombreterre, ne te découvre pas d''un fil ! »[/i] Proverbe de l''Izandar. '),
(123, 123, 'Blême', 'M', '', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 505, 20, 3, 'plain', 9, 25, 15, 24, 'Un vent glacial secoue dans la nuit blafarde les branchages décharnés des quelques arbres alentour. Perçant le silence moribond de ces ténèbres, un engoulevent fait retentir son cri sinistre. C’est alors, sur ces plaines dévastées, hantées du souvenir d’anciens massacres, que surgit le Blême. Son esprit égaré réside jusqu’à la fin des temps dans la mémoire de sa mort atroce. Cette créature non vivante, pâle comme la Mort qu’elle côtoie à tout instant, est affublée d’un corps décharné, déjà rongé par les vers. Ses yeux verts et luisants brillent de la haine inextinguible envers les vivants qui la tenaille constamment. Lorsqu’un voyageur est perdu dans les parages, elle s’approche sans bruit et tente de lui offrir son baiser d’outre-tombe. Le malheureux, paralysé par la terreur, ne peut alors plus réagir lorsque le Blême l’emporte dans son charnier. Il en fera certainement une Goule qui viendra compléter sa troupe macabre afin  de se venger, en de vastes invasions, sur les villages avoisinants. \r\n\r\n[i]« Cunégonde ? Mais elle est où ? Cela fait quand même quatre heures qu’elle est partie chercher du bois ! Cunégonde ! [/i]\r\n[i]– Euh, tu crois que c’était une bonne idée de passer la nuit dans le cimetière ?[/i]\r\n[i]– Ah, te voilà Cunégonde, mais qu''est[/i] - [i]ce qu''y a, t’es toute pâle ! »[/i] Des Conduites à Risque chez les Aventuriers de Francis d’Holt-Haut'),
(124, 124, 'Golem de chair', 'M', 'golem-de-chair.jpg', 'No', 'neutre', 'créatures magique', 'walking', 0, 100, 3, 'plain', 10, 26, 15, 24, 'Répugnante créature sortie des machinations démentes d’un mage à la santé mentale discutable, le Golem de Chair se présente comme un amas de chairs plus ou moins bien cousues entre elles. D’une stature impressionnante, le Golem de Chair est doté d’une force phénoménale qui rend ses coups particulièrement dangereux. En outre, dénué d’esprit propre, il est parfaitement dévoué à son créateur. Ces deux caractéristiques en font un membre de choix parmi les Créatures Robustes et Serviles (disponible en catalogue sur simple demande par mouette postale)… Du moins lorsque le magicien maîtrise un minimum ses compétences traumaturgiques. Trop souvent, on a eu à déplorer la perte prématurée de jeunes étudiants en sorcellerie qui tentaient de s’essayer avant l’heure à la chirurgie inesthétique. Livré à lui-même, le Golem de Chair se jette alors sans aucun discernement sur toute chose possédant une peau dépourvue de poils et, une fois la pauvre créature dépecée, il se recouvre compulsivement le corps des morceaux sanguinolents de sa victime.\r\n \r\n[i]«– Bonsoir mon brave, je viens voir le maître de séant[/i]\r\n[i]–Y gn’sra po là ‘vant l''nuit.[/i]\r\n[i]–Fort bien, fort bien, j’attendrai donc. Mais, dites[/i] - [i]moi, vous n’auriez pas eu quelque accident de monture récemment ? Vous m’avez l’air d’avoir le visage légèrement tuméfié… »[/i] Ambassadeur chez les Vampires de Tangorodrim'),
(125, 125, 'Momie', 'F', 'momie.jpg', 'No', 'neutre,mauvais', 'morts-vivant', 'walking', 505, 20, 3, 'plain', 10, 27, 15, 24, 'C''est toujours pareil, ici, sur Nacridan. On ne peut rien enterrer sans que ça ressorte du tombeau en titubant avec acrimonie dès qu''on tourne le dos. Les morts-vivants, vraiment, c''est une vraie malédiction, ne serait-ce que pour les caisses de retraite. Quant aux aventuriers, n''en parlons pas : de nos jours, la plus perdue des nécropoles est aussi animée qu''une fête chez Adah, quand on commence à ouvrir les tonneaux d''alcool (surtout quand il s''agit de ceux sans étiquettes dans la petite cave avec l''alambic bizarre). Allez donc piller une sépulture, quand tout, jusqu''à la tombe du plus humble des paysans, semble grouiller de défunts dont les gestes ostensiblement agressifs jurent sans fard avec leur certificat de décès! Entre les nécromanciens qui font des tests et réveillent des régions entières par inadvertance, les créatures mortes-vivantes qui lèvent une armée, les concerts de Heavy Metal et les cadavres qui décident naturellement que la position allongée ne leur convient plus, la situation devient vite intenable. Les prêtres de Landar, aux rites mystérieux et aux idées bizarres, souscrivaient alors à la politique consistant à attendre d''un grand prêtre enfin mort qu''il le reste. \r\n\r\nDans le but d''éviter le retour à la vie de quelques individus qui auraient pu, assez littéralement du reste, parler des squelettes dans les placards, ils mirent au point la technique de la momification, consistant essentiellement à enlever l''ensemble des organes vitaux, plus quelques autres subsidiaires, et à entourer l’enveloppe externe d''un assemblage de bandelettes en lin très serrées et couvertes de signes cabalistiques. En vain : à peine la dalle funéraire des nécropoles fermées, voilà que ça gratte déjà aux parois…\r\n\r\n[i]« Momie ? C''est toi ? »[/i] in Pilleur de tombe, un métier d''avenir ?, de Grumpy Len. '),
(126, 126, 'Squelette', 'M', 'squelette.jpg', 'No', '', '', 'walking', 505, 20, 3, 'plain', 10, 28, 15, 24, 'L’animation des morts est la première chose que tout aspirant nécromancien apprend en première année. Animés par la volonté du magicien et quelques signes cabalistiques tracés à coups de pinceau, les cadavres se redressent et serrent leurs armes dans leurs mains osseuses, obéissant fidèlement aux ordres de leur maître. Ne nécessitant que des ossements en bon état, qu’il est facile de se procurer dans n’importe quel cimetière bien achalandé, la constitution d’une armée de squelettes est chose très courante. \r\n\r\nOn peut même trouver des armées mortes vendues clef en main par certains nécromanciens pour les apprentis maîtres du monde pressés, à des prix très modiques. Plus esthétiques que les zombies, et moins désagréables à fréquenter pour les nécromanciens pourvus d’un odorat, les squelettes ont un certain succès ces dernières années. Cependant, en dépit de leur fidélité à toute épreuve et de leur grande efficacité face aux archers ou épéistes, ce modèle de base pêche par sa relative fragilité face aux coups de masses ou face aux chiens. \r\n\r\nEn outre, comme tout mort-vivant, ils sont parfaitement stupides, et pour toute tâche plus complexe que patrouiller dans les couloirs, se faire tuer par un héros ou monter la garde devant les portes, ils se révéleront tristement ineptes.\r\n\r\n[i]«Pour une légion des damnés achetée, la compagnie de Guerriers Squelette offerte ! Satisfait ou empalé. »[/i] Réclame passée dans la Nécropole illustrée.'),
(127, 127, 'Gobelours', 'M', 'gobelours.jpg', 'No', 'chaotique,mauvais', 'humanoïdes', 'walking', 182510, 100, 3, 'plain', 11, 29, 25, 36, 'Ces colosses parmi les peaux-vertes sont de loin apparentés aux gobelins. Toutefois, ils n''ont rien de chétifs ni de pleutres. Aussi hauts qu''un homme et bien souvent plus robustes, ils peuvent courir des journées entières sous un lourd harnois de maille et de fer sans gémir, et combattre avec vigueur dans l''heure qui suit. Affectionnant les armes courbes aux cruelles dentelures, ce sont des combattants farouches et brutaux à l''instar des orques, mais pourtant capables d''une certaine discipline. \r\n\r\nLeur aspect quelque peu velu et les lourdes griffes qui ornent leurs mains puissantes sont la cause des rumeurs qui imputeraient leur existence aux manipulations thaumaturgiques de Saroulemaal, un mage qui cherchait jadis à améliorer le potentiel offensif de ses troupes de gobelins monteurs d''ours… mais il vaut mieux éviter de le dire en face d''un Gobelours, toujours un peu chatouilleux sur ces aspects-là. \r\n\r\nS''ils ne forment pas les troupes de choc ou les cadres commandants d''une armée de peaux-vertes au service d''un seigneur du Mal quelconque, on retrouve en général les Gobelours à la tête de troupes de leurs cousins Gobelins ou orques, voire parfois en compagnie de hors-la-loi des Races Aînées. On raconte même qu''un Gobelours socialement très bien intégré et particulièrement entreprenant avait jadis fondé une compagnie d''import-export d''esclaves elfiques basée à Earok, jusqu''à ce qu''une descente de paladins ne vienne mettre un terme à son fructueux commerce, le contraignant à devenir quartier-maître à bord d''un vaisseau corsaire de Dust.\r\n\r\n[i]«Yo ho ho ils étaient six sur le coffre du gobelours mort, et une bouteille de rhum !»[/i] Vieille chanson corsaire de Dust interprétée a capella par Neander, Salazar &amp; les Joyeux Pirates du Pavillon Noir.'),
(128, 128, 'Orc', 'M', 'orc.png', 'No', 'loyal,mauvais', 'humanoïde', 'walking', 291906, 100, 3, 'plain', 11, 30, 25, 36, 'D''où viennent les orques ? Ces sauvages humanoïdes à la peau verte ne vivent que pour le combat et la guerre, qu''ils pratiquent avec une absence de scrupules réjouissante contre tous ceux qui croisent leur chemin. Regroupés en tribus nomades régies par un système complexe de lois coutumières, les orques parcourent les terres connues en semant la dévastation sur leur passage, ou se rangent sous la bannière de quiconque les paye suffisamment. Ce sont des fantassins brutaux, qui affectionnent les lourdes armures et les armes pesantes, et qui s''avèrent bien souvent redoutables au corps à corps, mais ils sont rétifs à toute idée de discipline, en quoi ils restent dangereusement imprévisibles. Susceptibles de tourner casaque aussi bien que de se faire tailler en pièces jusqu''au dernier, selon le jour. Ils détestent en général la magie, et ceux qui la pratiquent. \r\n\r\n[i]«Une pour les trouver[/i]\r\n[i]Une pour les rassembler et dans les ténèbres les mener[/i]\r\n[i]Et par mon feu leurs crânes faire exploser[/i]\r\n[i]On achève bien les fédéraux»[/i] Dame Mordsith.');
INSERT INTO `BasicRace` (`id`, `id_BasicProfil`, `name`, `gender`, `pic`, `PC`, `alignment`, `class`, `state`, `dropItem`, `percent`, `frequency`, `land`, `band`, `behaviour`, `levelMin`, `levelMax`, `description`) VALUES
(129, 129, 'Horreur des sous bois', 'F', '', 'No', '', '', 'walking', 0, 100, 1, 'forest', 19, 31, 15, 24, 'Ce répugnant lichen carnivore est probablement l''une des premières causes de mortalité chez les jeunes aventuriers néophytes, avec Judas le Fourbe, Divin Scorpion et le chant mortifère du Castor Iustine. \r\n\r\nSe développant sur de vastes surfaces, en prenant l''apparence d''innocentes plaques de mousse, ou en se tapissant sur les branches des arbres, il attend, dans une horrible semi-vie végétative, qu''un individu soit assez crétin pour s''endormir à proximité. Alors il rampera par des spasmes obscènes jusqu''à sa proie, et, chose abominable, pénétrera dans son crâne par les oreilles pour digérer très lentement son cerveau, processus fort peu ragoûtant s''il en est. Outre que cela inflige d''atroces douleurs à celui qui en est victime, la miséricorde de la mort sera souvent trop longtemps refusée au malheureux, condamné à perdre très lentement toutes ses facultés mentales, à mesure que son cerveau est dévoré de l''intérieur. On cite ainsi le cas d''un individu qui aurait survécu pendant plus de trois ans à sa contamination, même si dans le dernier tiers, il tenait plus du légume que de l''être conscient. \r\n\r\nContrairement à ce que des rebouteux sans scrupule pourraient vous faire croire, jeunes aventuriers, il est extrêmement difficile – bien que possible – de soigner une infestation par une Horreur. Cependant, seul un très grand thaumathérapeute peut sauver la victime, et ces derniers n''ont pas fait vingt ans d''études dans des bibliothèques poussiéreuses plutôt que de courir la gueuse dans les clubs branchouilles d''Earok pour sauver bénévolement un jeune crétin sans le sou dans votre genre. D''autant plus que, si l''on excepte la douleur insoutenable des premiers jours, les symptômes d''une infestation sont assez proches des affres de l''adolescence ou de la consommation de certains produits disponibles sur simple demande à la Taverne de la Fédé – yeux dans le vide, boutons, rires gras et stupides, tendance à baver, etc., ce qui ne facilite pas la détection précoce du monstre, ses victimes étant souvent dans la mauvaise tranche d''âge.\r\n\r\n[i]« En conclusion, en forêt, sortez couverts ! Les bouchons d''oreilles Joyeux Corsaire (tm) sont très utiles pour tous les petits ennuis du quotidien… »[/i] Réclame parue dans le Barbare Déchaîné.'),
(130, 130, 'Fée des bois', 'F', 'fee-des-bois.jpg', 'No', '', '', 'walking', 506, 20, 1, 'forest', 19, 32, 15, 24, 'Ces minuscules esprits des bois aux couleurs vives, dépassant à peine la dizaine de centimètres, virevoltent joyeusement sur des libellules mordorées aux côtés des aventuriers qui s''aventurent dans les sous-bois de la Forêt d''Yeuse, pépiant d''incompréhensibles sottises de leurs voix ténues. Gare cependant ! Si elles ne sont pas mauvaises, le ballet électrique de leurs corps en mouvement peut hypnotiser l''individu peu préparé, ou distrait, au risque de subir des avanies plus ou moins graves : rentrer dans un arbre, tomber dans une ravine, bousculer Lord Thergal. \r\n\r\nA l''aide de certaines poudres et phéromones, elles peuvent provoquer des hallucinations, des mouvements confus, endormir ou surexciter ceux qui en sont victimes, à leurs risques et périls. Depuis qu''il est possible de s''en prémunir avec un masque de gaz imprégné de raifort, les fées des bois sont malheureusement pour elles devenues l''objet d''un commerce extrêmement lucratif ; en effet, en capturant une fée et en la pressurant, n''importe quel alchimiste un peu talentueux parvient à en extraire la poudre de fée, tant recherchée par les aventuriers pour ses vertus, disons, festives. \r\n\r\n[i]«… En fait, ce fut très simple. Usant de mon poste de Commerçant pour le compte de la Fédération, j''étais en charge de la Taverne. Avec Adah, qui elle[/i] - [i]même s''occupait de former les jeunes. De là, il était très facile de vendre à ceux[/i] - [i]ci de grosses quantités de poudre de fée, herbe[/i] - [i]qui[/i] - [i]fait[/i] - [i]sourire, écorces de ronces d''elfe, et autre feuilles de Neptune séchée. C''était sans danger. Enfin, pour nous : on graissait la patte à El Zvountz, et tout se passait très bien. Évidemment, à mesure que le temps passait, certaines recrues avaient bien le regard un peu vide et une tendance à baver en public, mais, après tout, c''était des jeunes en pleine croissance…»[/i] Narcorsaire, un métier d''avenir, d''Anraud [i]«Bicornu»[/i] de Greif, hérault.'),
(131, 131, 'Mante prêtresse', 'F', 'mante.jpg', 'No', '', '', 'walking', 0, 100, 3, 'forest', 19, 33, 15, 24, 'Qu''il est paisible ce sous-bois joli. Qu''il est doux, le murmure du ruisseau. Qu''il est vert, le frais cresson bleu. Quelle est bonne la cuvée [i]« Ne dit pas à Aé Li que je t''en ai refilé »[/i] de la Taverne ! Rien ne pourrait vous déranger dans votre somnolence avinée au creux douillet de cette clairière paisible de la forêt d''Yeuse.\r\nVoire.\r\nDans l''ombre verte, deux yeux à facettes luisent d''une impitoyable lueur. N''entendez-vous pas comme un froissement soyeux dans les herbes agitées par le vent ? Ce n''est pas le vent. C''est la Mante Prêtresse, et vous êtes sa proie. Haute de deux mètres, le corps cuirassé d''une épaisse chitine verte, tirant sur le jaune pâle au printemps, la Mante se déplace avec la grâce du tigre blanc et frappe à la vitesse foudroyante de l''araignée sabre. Ses puissantes faux avant découpent l''air, l''acier, la chair de même. Heureusement, elle n''est pas très résistante : quelques coups de lance ou de flèches bien placées auront tôt fait de mettre hors de combat ce monstre qui n''opposera qu''une fort relative résistance, à condition qu''il ne vous prenne pas par surprise.'),
(132, 132, 'Hobgobelin', 'M', 'hobgobelin.jpg', 'No', 'loyal,mauvais', 'humanoïde', 'walking', 211006, 25, 3, 'plain', 12, 36, 37, 50, 'Très proche cousin du Gobelours, le Hobgobelin fut obtenu pour les besoins de l''Izandar lors de la colonisation des contrées nordiques. Pour faire face à la profusion de menaces et de monstres qui hantaient les steppes gelées d''au-delà les Crocs du Monde, et pour permettre de meilleures capacités de combat, des orques furent croisées traumaturgiquement avec des humains, d''une part; tandis qu''une autre branche de l''Université de l''Invisible, la faculté de Zôonomaturgie, travaillait sur un projet similaire par des méthodes qui se passent de tout commentaire. \r\n\r\nBien que les archives restantes soient difficiles à déchiffrer et fassent mention de nombreux problèmes liés à l''agressivité et à la violence de ces créatures dénaturées, les hobgobelins ont bel et bien une existence. La destruction pure et simple de l''Empire izandrin par les démons du nord ayant conduit à la libération de vastes régiments de Hobgobelins, ceux-ci, après avoir massacré leurs officiers, tentèrent de survivre dans le Nord. \r\n\r\nParfaitement adaptés au froid, leurs descendants ont le teint jaunâtre et un faciès grossier. Professant une adoration malsaine pour tout ce qui se réfère de près ou de loin à la mort ou à la violence, leur culture, essentiellement basée sur la recherche du combat au corps à corps, et leur puissance musculaire en font des combattants dangereux. Leur intelligence est supérieure toutefois à celle des peaux-vertes, et ils parcourent régulièrement les Steppes des contrées du Nord, comme les Plaines Centrales, à la tête de vastes régiments de trolls ou d''orques, voire de gobelours, assaillant et massacrant les populations des villages qui ont le malheur de succomber à leurs attaques.\r\n\r\n[i]«Ca commence à faire beaucoup»[/i] Commentaire du juge Renard de Ruine-Bec au procès de MM. Pipindrelin, Dominique de Vilbaguette et Mémé du Court-Etang, respectivement Archichancelier et Recteurs Adjoints de l''Université de l''Invisible, pour crimes de guerre et actes contre-naturels, in [i]Utopie d''un monde meilleur[/i], d''Houle-Douce d’Axley.'),
(133, 133, 'Troll', 'M', '', 'No', 'chaotique,mauvais', 'humanoïde', 'walking', 1, 100, 3, 'plain', 12, 37, 37, 50, 'Ces grandes créatures de la parentèle des orques forment de vastes hardes agressives. Demeurant en général dans les grottes et les lieux obscurs, ils n''hésitent pas à attaquer les villages des races aînées pour y trouver leur pitance. Leur constitution exceptionnelle et leur force remarquable en fait, même isolés, des adversaires particulièrement dangereux. Acculés, ils peuvent faire preuve d''une férocité plus grande encore. Toutefois, l''épaisseur de leur cuir et les propriétés rares de leur sang vert, qui entre dans la formule des plus puissantes potions de régénération, font de la profession de Chasseur de troll un métier aussi lucratif que risqué. Certains mages n''hésitent pas à capturer des trolls et à les dresser dans le but d''en faire des guerriers redoutables. Ainsi la Garde Verte de l''Empereur d''Artasse était-elle coutumiérement constituée de trolls. Ils se firent tailler en pièces à la bataille des Chants Sanglants pour permettre la retraite de l''armée impériale (ce qu''il en restait). Mélomanes, les trolls apprécient également d''écouter de la bonne musique de chez eux : ils se regroupent alors par centaines autour de ceux des leurs qui tapent sur des cailloux en faisant du bruit. \r\n\r\n[i]«- ... Ugh ? Zog zogarth Ugluzotgaatha !! URH URH URH !!!»[/i] Bonne blague troll, in Ma Vie chez la Garde Verte, d''Evarrglh Johlih.'),
(134, 134, 'Nécromancien', 'M', 'necromancien.jpg', 'No', 'neutre,mauvais', 'morts-vivant', 'walking', 0, 100, 2, 'plain', 13, 38, 37, 50, 'La pratique de la nécromancie a longtemps fait beaucoup débat dans les milieux magico-druidiques. De l''avis général, on considère que, si porter une longue robe noire chargée de symboles kabbalistiques, des bagues à tête de mort, un teint pâle et maladif, des cheveux longs et gras sont choses acceptables pour un jeune mage, les cohortes d''esclaves putréfiés font généralement tache dans les réceptions de l''Université de l''Invisible, dont les nécromants sont régulièrement expulsés. \r\n\r\nAlors s''ils maudissent des contrées entières, génèrent des pestes zombifiantes ou balancent des rayons de la mort, c''est parce qu''ils sont jaloux, c''est tout. Ils n''ont pas un grand sens de l''humour, hélas. Toujours est-il que, en dépit des lénifiants discours de tolérance des recteurs de l''Université de l''Invisible, le métier de Nécromancien reste socialement peu valorisé, contraignant les pratiquants de cet art de la mort à rester dans leurs grottes et tours isolées, et à suivre des cours par correspondance, de même si l''essor remarquable l''Université de l''Invisible à Néphy (UIAN) permet de s''assurer du goût toujours intact des jeunes mages pour le tripatouillage d''ossements. Le relatif rejet dont ils font quand même l’objet provoque néanmoins bien trop souvent un ressentiment à l''égard de la société, qu''ils expurgent par une thérapie de groupe, en envoyant des légions des morts raser un village ou deux, à la grande fierté de leurs parents. Évidemment, il reste l''explication que seul un dangereux sociopathe préfère passer ses nuits à réveiller des cadavres plutôt qu''à boire dans les bars d''Izandar comme les étudiants en Zôonomathurgie…\r\n\r\n[i]« AAH AHA HA HA !! Avec ma légion des morts, je serai le Maître du Monde et PERSONNE ne m’en empêchera ! Ça leur apprendra à piquer mes petits[/i] - [i]fours ! »[/i] Jean-Eude de Nuit-Saint-Anraud, alias [i]Astrubaal le Seigneur des Ténèbres[/i], in [i]Mémoires d’un zombie[/i], par Lha’khontess de Ssg’Ur'),
(135, 135, 'Zombi', 'M', 'zombi.jpg', 'No', 'neutre', 'morts-vivant', 'walking', 505, 20, 3, 'plain', 13, 39, 37, 50, 'Ce cadavre ambulant est dépourvu de toute intelligence, et n''est qu''un pantin au service du nécromancien qui l''anime. Résistant, toujours affamé et ne réclamant jamais de congés maladie ou d''augmentation, le zombie est fiable, assez efficace pour semer la terreur, et reste par-dessus tout économique, facile à créer et ne requérant qu''une dépense d''énergie somme toute minime. En conséquence de quoi, le zombie est un pilier pour toute armée de nécromant aspirant maître du monde qui se respecte, bien que le processus de décomposition ne puisse être interrompu. Il faut donc accepter de devoir renouveler ses effectifs régulièrement, supporter de marcher dans des membres pourris qui traînent dans les couloirs et, bien entendu, être totalement dépourvu d''odorat.\r\n\r\n[i]« [/i]– [i]Par le damné caractère d''Alicia de Lamb !! Qui donc a encore laissé trainer des bouts dans la soupe ? Qu''il se dénonce immédiatement, ou je vous passe tous au souffre et au sel !»[/i] Le journal d''une nécromante, par Dame Shaan Yythaenath'),
(136, 136, 'Combattant squelette', 'M', 'combattant-squelette.jpg', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 505, 20, 3, 'plain', 13, 40, 37, 50, 'Version améliorée du squelette guerrier de base, le Combattant Squelette est également plus difficile à produire. Il nécessite non plus un squelette trouvé n’importe où, mais bien le cadavre d’un soldat tombé au combat. S’il est vrai qu’on ne manque pas de ces derniers en Nacridan, il est par contre beaucoup plus dur d’en trouver en parfait état. \r\n\r\nPar le biais d’une incantation assez complexe, il est possible de conserver les compétences militaires du mort. On peut même leur apprendre, au prix de quelques efforts, à combattre de façon plus efficace, notamment en phalanges, ou à manier des armes de jet. Inépuisables, insensibles aux blessures, ils peuvent représenter des adversaires redoutables, si tant est que leur maître ait consenti à leur procurer des armes correctes : bien trop souvent, des Combattants Squelettes connaissent une nouvelle mort humiliante parce qu’un nécromancien trop près de ses pièces d’or a préféré leur laisser l’arme rouillée et les lambeaux d’armure avec lesquels ils s’étaient relevés, soit-disant pour des raisons de charisme… \r\n \r\n[i]« … Morts[/i] - [i]vivants de tous les tombeaux ! Camarades ! On vous spolie, on vous exploite ! Cessez de servir des maîtres du monde qui vous envoient sans état d’âme à la re[/i] - [i]mort avec des équipements insuffisants ! Mettons[/i] - [i]nous en grève, ici et maintenant ! Exigeons une assurance non[/i] - [i]vie avant de reprendre le combat… »[/i] Discours d’ouverture de Jean Cubitès, à la IIe Assemblée générale de la Confédération générale des Trépassés. '),
(137, 137, 'Gardien squelette', 'M', 'gardien-squelette.jpg', 'No', 'neutre', 'morts-vivant', 'walking', 0, 100, 3, 'plain', 14, 41, 51, 66, 'Muni d’un plastron d’acier rouillé et d’une pesante hallebarde, n’importe quel squelette peut remplir indéfiniment sa mission, pour peu que le magicien prenne la précaution de l’asservir éternellement. Contrairement aux autres types de squelettes guerriers, qui seront détruits à la mort de leur maître, le Gardien Squelette tire sa force de sa propre énergie ; des siècles durant il restera prostré, immobile, dans les ténèbres poussiéreuses d’un tombeau, jusqu’au jour où un groupe d’aventuriers pénétrera dans le temple perdu pour y voler les trésors et les revendre au marché d’Earok. La Contrainte invoquée par le nécromancien obligera alors le Gardien à se relever dans un cliquetis de mauvais augure pour défendre jusqu’à la fin le lieu auquel il est attaché. Lourdement armé et protégé par de nombreux sorts de défense, le Gardien Squelette forme un adversaire redoutable, d’autant plus qu’il coûte assez peu à créer. Les nécromanciens peu partageurs en recrutent des régiments entiers à grand coût d''incantations et de sacrifices. On ne compte plus les fois où un groupe d’aventuriers a dû battre en retraite devant la muraille d’acier qu’opposait une horde cliquetante de squelettes gardiens. Gare toutefois à ceux qui parviennent malgré tout à profaner le sanctuaire défendu par les gardiens squelettes : ceux qui ne seront pas détruits seront libres alors de parcourir les terres en semant terreur et désolation sur leur chemin.\r\n\r\n[i]« Que d''os ! Que d''os ! »[/i] Encourager ses troupes pour les nuls, Lord Thergal, duc de Mac''ahron'),
(138, 138, 'Feu Fol', 'M', '', 'No', 'Neutre', 'Elémentaire', 'walking', 0, 100, 3, 'plain', 14, 42, 51, 66, 'La légende dit qu''Aram Oeil-Braise, le grand nécromancien, fut le père des Feux Fols qui hantent désormais les Marais du Désespoir. D''autres prétendent qu''il s''agit simplement d''émanations de méthane embrasé. Ils ont tous deux raison : dans la mesure où cette créature n''est rien d''autre qu''un golem façonné par la volonté d''un mage depuis longtemps disparu à partir de gaz enflammé rejeté par le marais. \r\n\r\nLe Feu Fol se présente à l''origine comme une petite flamme humanoïde dansant dans les brumes méphitiques des Marais, chuchotant d''une voix ténue des choses inaudibles. Bien souvent, l''aventurier imprudent, le voyageur fatigué par une longue journée de marche manque de chance et tombe dans le piège : croyant voir la lueur lointaine de la torche de son guide ou d''un abri, il sort du sentier presque invisible et aura tôt fait de s''égarer dans les boues mortelles, au plus grand plaisir des Gnolls de Fange et des abominations. On ne sait pas vraiment pourquoi les Feux Fols agissent ainsi ; on suppose que la mission que leur avait donnée leur créateur consistait à égarer d''éventuels visiteurs indésirables, à moins qu''il ne s''agisse simplement d''un système un peu tape-à-l’œil pour orner son jardin de fleurs carnivores. \r\n\r\nLe Golem de Feu est assez difficile à combattre de par sa nature ignée, en particulier pour les mages : les boules de feu ne font que renforcer son corps. Prenez garde ! Si le feu fol parvient à trouver une vaste nappe de gaz en suspens dans le marais, il l''utilisera pour renforcer son corps, atteignant aussitôt des proportions inquiétantes. Ainsi renforcé, c''est un véritable monstre déchaîné qui avancera aveuglément droit devant lui, allant jusqu''à sortir de son marais natal pour dévaster les plaines et les forêts. De telles créatures, en particulier dans la chaleur de l''été, deviennent aussitôt de terribles dangers pour les communautés des plaines centrales qui vivent dans la terreur de telles attaques ; et lorsque le tocsin retentit, qu''un golem de feu est repéré, les miliciens troquent aussitôt leurs hallebardes contre des seaux d''eau et tentent d''éloigner la créature : un acte courageux, car de nombreux hommes peuvent se retrouver piégés dans les bras enflammés du monstre furieux et livrés à une mort atroce.\r\n\r\n– [i] « Excusez-moi, pardon, vous auriez un golem de feu ? demanda l''aventurier nain en sortant sa pipe. [/i]\r\n– [i] Oui, bien sûr, répondit Albus, attendez que je l''invoque. » [/i]'),
(139, 139, 'Ogre bicéphales', 'M', '', 'No', 'chaotique,mauvais', 'humanoïde', 'walking', 1, 100, 3, 'plain', 15, 88, 67, 84, 'Cette immense masse de chair et de muscles qui s’approche de vous à grands pas en brandissant une énorme massue de bois, de pierre et de fer entremêlés vous ramènera certainement à vos pires cauchemars d’enfance car ses deux bouches avides ne se tendent vers vous que pour vous dévorer ! Son imposante stature ainsi que sa maîtrise du combat au corps à corps en font un redoutable adversaire. Mais il faut bien reconnaître que, malgré ses deux têtes volumineuses et difformes, l’Ogre Bicéphale ne brille pas par l’action de ses deux cervelets. Seule sa faim semble décuplée par le nombre de bouches à nourrir. Certains pourtant possèdent des dons magiques qu’ils mettent au service de leur estomac, répandant une terreur sans nom dans les contrées sur lesquelles ils ont élu territoire. Ils considèrent alors hommes et bêtes comme du bétail qu’ils prélèvent pour se nourrir et défient fièrement les aventuriers qui tenteraient de s’opposer à leur domination.\r\n\r\n[i]« Gzorg s’avance d’un pas rapide et se jette dans la mêlée ! Il fait face au numéro 9 des Paladins, qu’il domine de deux têtes, enfin, de quatre en l’occurrence vu qu’il en a deux ! Et même, peut[/i] - [i]être même… oui !!! Plus de quatre têtes maintenant qu’il vient d''arracher celle du numéro 9 adversaire… d’un splendide coup de massue sur le revers du cou. Oh là là ! Une action foooooormidable ! Quel talent, quelle force et quelle précision dans le geste technique ! »[/i] Bataille Ogres-Paladins en demi-finale de la [i]Ligue des Héros[/i], rapportée par Zéon l’Histrione.'),
(140, 140, 'Garde Noir', 'M', 'garde-noir.jpg', 'No', 'neutre', 'extra-planaire', 'walking', 0, 100, 3, 'plain', 15, 1, 67, 84, 'Entièrement revêtu d’une armure d’acier noirci et masqué par un heaume de ténèbres insondables, le Garde Noir est invoqué par un sorcier dans le but de se procurer une sentinelle parfaite. Généralement muni d’un fléau ou d’une lourde et massive hallebarde, le Garde Noir peut rester des journées entières à son poste ; il n’a pas besoin de manger, de dormir, ou d’aller aux filles de joie. Aucune partie de dés ou de poker ne peut le distraire de sa tâche, et l’ombre comme la lumière du jour ne peut masquer sa vision acérée. Son ouïe est parfaite, et on aura du mal à trouver un défaut de son armure où placer un poignard pour lui trancher une gorge apparemment inexistante. Ajouter à cela une totale incorruptibilité, une absence parfaite de sentiments, et un grand talent pour le combat, et vous comprendrez pourquoi les Gardes Noirs sont aussi courus. Cependant, leur utilisation comprend certains fardeaux, notamment le fait qu’ils sont liés à leur invocateur et tirent leur énergie de la leur, obligeant les mages qui ne souhaitent pas voir leur poids chuter considérablement et leur vie ressembler à celle d’une larve pour peu que leur créature se mette à combattre, à consentir à des sacrifices plus ou moins importants. Moyennant un sacrifice humain, il devient possible d’attacher le Garde Noir à une mission bien spécifique, qu’il remplira jusqu’à sa destruction. Ainsi certaines ziggourats perdues sont-elles défendues par de véritables garnisons de Gardes Noirs, rendant leur violation particulièrement ardue.\r\n\r\n[i]«Peur du noir[/i]\r\n[i]Peur du noir[/i]\r\n[i]J’ai une constante peur du Garde Noir[/i]\r\n[i]Qui se tient là caché dans le noir…»[/i]\r\nExtrait de Peur du Garde Noir, du groupe de barde La vierge d’acier.'),
(141, 141, 'Guerrier Possédé', 'M', 'guerrier-possede.jpg', 'No', 'chaotique,mauvais', 'humanoïde', 'walking', 0, 100, 3, 'plain', 17, 1, 85, 104, 'De même qu''il est possible à un homme d''enfermer un démon dans un pentacle d''invocation pour le réduire à sa volonté, il est loisible aux plus puissants parmi les Seigneurs des Cercles Infernaux de soumettre un individu, en attirant un démon de rang inférieur dans le corps du possédé. L''opération est toutefois loin d''être aisée et demande une débauche considérable d''énergie pour réussir, d''autant plus si la volonté et l''expérience de l''individu sont grandes. La force du possédé se voit décuplée, de même que sa vitesse et sa malignité, tandis que son être, sa conscience et une bonne partie de ses souvenirs seront soufflés. Il devient alors un guerrier terrifiant, insensible aux maux qui affligent les simples mortels, et face auquel seul un champion exceptionnel peut prétendre résister. L''exemple le plus célèbre d''une possession reste encore celle du Capitaine Anraud de Greif, qui sous l''influence d''un démon précipita le Vieux Monde vers sa fin. Il aurait d''ailleurs prétendu que ça n''était qu''un vaste complot orchestré par les dieux, mais du fait de la fin du monde, cela n''a jamais pu être établi. \r\n\r\n[i]«Être ou ne pas être possédé, telle est la question»[/i] in Mémoires d''Outre[/i] - [i]Mer, d''Anraud [i]«Bicornu»[/i] de Greif.'),
(142, 142, 'Balor', 'M', 'balor.jpg', 'No', 'chaotique,mauvais', 'démon', 'walking', 0, 100, 3, 'plain', 17, 1, 85, 104, 'Dans les entrailles de la terre, là où règne la lave en maîtresse, vivent depuis les temps immémoriaux les Balors, oubliés des hommes comme des dieux. Démons des enfers, gardiens de l’impur, ils étaient jadis les seigneurs et maîtres du Cinquième Cercle de l''Enfer, possesseurs des âmes et tyrans impitoyables pour leurs sujets, capitaines des grandes légions infernales. Et, à ce titre, ont-ils traversé jadis les limbes entre les mondes pour envahir le nôtre. Mais ils furent trahis, le portail derrière eux refermé, les coupant de la source de leurs pouvoirs. Réduits au rang de vassaux des grandes puissances de l''ancien temps, ils sont les détenteurs de la Flamme Noire depuis des millénaires, bien avant l’émergence de Nacridan. Lors du Grand Bouleversement qui accompagna la fin de l''Age des Dieux, nombreux furent ceux qui périrent. Les autres se réfugièrent dans les entrailles de la terre, et sortirent de la mémoire des dieux comme de celle des hommes… \r\n\r\nDe sourds battements de tambour, lancinants et sataniques, accompagnent leur apparition, ainsi qu’une meute de créatures aussi serviles que corrompues par leur pouvoir. Ils sont passés maîtres dans l’art de tordre les esprits aussi bien que les corps, et leur hurlement seul suffit à réduire le nain le plus brave à l’état d’un Dorane nouveau-né, à obscurcir l’esprit du plus sage magicien jusqu’à le faire ressembler à un gobelin stupide. Peu connaissent sa véritable apparence mais on le dit enveloppé de flammes sombres, gigantesque et armé d’une hache terrible et d’un fléau d’arme sanguinolent. Certains grimoires relatent des tentatives d’invocation par des magiciens de ce démon redoutable ; jamais on n’a retrouvé d’autre trace d’eux qu’un résidu de cendres au milieu de leur laboratoire… Maître des Enfers, Disciple des Chaînes, Grand Manipulateur du Feu Sombre, Haut Asservisseur des Ames, mieux vaut tout compte fait ne jamais les rencontrer. Alors, si vous entendez Ses tambours, inutile même de fuir car sa vélocité égale sa férocité… Il ne vous reste qu’à prier !\r\n\r\n[i]« Tiens, c’est sympa ce djembé qu’on entend au loin ! Tu crois qu’y a une teuf au fond de la mine ? »[/i] Dernières Paroles d’Adah'),
(143, 143, 'Gardes des ténèbres', 'M', '', 'No', 'neutre', 'extra-planaire', 'walking', 0, 100, 3, 'plain', 17, 1, 84, 104, 'Cette immense silhouette qui s’avance dans les brumes de l’aube peu à peu dissipées par les brises matinales, ce reflet d’obsidienne qui luit sur son armure alors que les premiers rayons du soleil percent le brouillard, cette épée menaçante qui jette des éclats de flamme alors que son pas lourd et serein fait retentir le sol, c’est le message spécial qu’apporte le Garde des Ténèbres. Bienheureux les analphabètes, sa missive est en général très brève ! Mutique autant qu’inépuisable, le Garde des Ténèbres parcourt monts et vaux sans relâche et est même capable de se déplacer au fond des eaux jusqu’à ce qu’il trouve la cible désignée par son seigneur et maître. \r\n\r\nAdepte d’une arithmétique infernale, il se déplace en droite ligne d’un pas sûr vers sa proie, et ni bâtisse, ni homme, ni démon ne peuvent arrêter sa progression funeste. D’aucuns ont ainsi vainement fui durant des lunes, sans trêve ni repos, avant de se retrouver hagards devant leur Destin : le Garde des Ténèbres assène des coups impitoyables et son armure sombre ainsi que son écu de nuit le protègent de la plupart des attaques. Parfois, échappant au contrôle du mage téméraire qui l’invoqua, il erre sans but parmi les landes dans l’espoir de retrouver le chemin des plans nocturnes qui l’ont enfanté. Mais jamais la terrible nature de cet assassin de premier ordre ne se dément…\r\n\r\n[i]« Voilà des mois qu’il me court après. Je suis à bout, je craque… Jour et nuit j’entends son pas incessant, je ne peux penser à autre chose qu’à cette créature qui me talonne. Jamais je n’aurais dû refuser les petits gâteaux salés de ma belle[/i] - [i]mère Alicia ! Oh, comme je regrette à présent, mais il est trop tard maintenant ! Dieux, ayez pitié de moi ! »[/i] Parchemin anonyme retrouvé sur les rivages de Tonak.'),
(144, 144, 'Serpent Aqualien', 'M', 'serpent-aqualien.jpg', 'No', 'neutre', 'élémentaire', 'walking', 2, 100, 3, 'march', 23, 43, 37, 50, 'Surgissant sans bruit des mystérieux abîmes océaniques aux verts reflets, le serpent aqualien est le cauchemar des marins dont il fait sa provende, qui voient en lui le messager de la colère de Makero, l''ombrageux seigneur des mers. Pour l''homme perdu dans les flots agités, c''est la mort reptilienne qui dresse son long corps froid couvert d''écailles glauques, ses centaines de crocs effilés comme autant de poignards blafards constellés de gouttes scintillantes de poison, ses yeux fendus au regard fixe terrifiant, alors sa langue bifide s''agite fébrilement, goûtant la peur dans l''air salé tandis qu''il fond sur sa proie hurlante, ses courts bras griffus agrippant les plus solides coques pour s''y maintenir. Parfaitement amphibie et redoutable même sur terre.\r\n\r\n[i]«Ssss ? Comment ça, il est pas frais mon marin ? ! Répète un peu pour voir !»[/i] in La vie rêvée des Aqualiens, de Suma Dartson.'),
(145, 145, 'Abomination des marais', 'F', 'abomination-marais.jpg', 'No', '', '', 'walking', 0, 100, 3, 'march', 23, 44, 37, 50, 'Pour beaucoup, l''abomination des marais n''est qu''un conte de [i]«vieille femme propagé par les Fangeux du Désespoir»[/i], ou un moyen discret de soutirer un petit pourcentage en plus pour les guides qui vous mènent à Néphy par leurs chemins d''eau et de boue. Pour certains autres, ce n''était qu''un tas de mousse verdâtre et de bouillie vénéneuse de plus dans un marais qui en compte tant, quand le tas s''est dressé brutalement avant de se jeter sur eux pour les étouffer de sa masse nauséabonde et froide. Indiscernables, les abominations rampent parmi les canaux mystérieux des marais du Désespoir, pour surgir sans crier gare, frappant et disparaissant selon des motifs que nul ne connaît ni ne comprend. Pour les Fangeux qui habitent le marais, elles sont l''émanation du marais lui-même, qu''ils déifient. Pour les habitants de Néphy, il s''agit d''un moyen commode de tenir à l''écart les individus aux intentions malvenues ; les paladins aux lourdes armures font des proies rêvées pour les abominations.\r\n\r\n[i]« OUUUUUIIII ! En vérité je vous le dis, l’''AAAAAABOMINATION viendraaaaaah ![/i]\r\n[i] –Encore un bourbeux qui a perdu son cerveau dans une fondrière»[/i], songea Judas le Fourbe, avant de l''éventrer pour lui prendre son or. Tiré des [i]Petits meurtres entre amis[/i] de dame Petra.'),
(146, 146, 'Basilic majeur', 'M', 'basilic-majeur.jpg', 'No', 'neutre,mauvais', 'reptile', 'walking', 2, 100, 3, 'march', 23, 45, 37, 50, 'Être mystérieux s''il en est, le Basilic fait l''objet de nombreuses rumeurs et croyances parmi le petit peuple des aventuriers. On dit ainsi qu''il naîtrait d''un œuf de serpent couvé par un poulet, et que le chant d''un coq enroué le jour de la saint Aé Li le condamnerait à une mort aussi rapide que miséricordieuse. On dit aussi que son regard est mortel et que son venin ne connaît d''antidote. Tout ceci est très folklorique, et fort improbable. \r\n\r\nSelon des témoins dignes de foi, le basilic est un énorme serpent, pouvant mesurer plusieurs dizaines de mètres de long, aux écailles couleur vase et tourbe, qui demeure de longues heures immobile tapi en attente dans les profondeurs inondées des Marais du Désespoir, avant de surgir telle une foudre mortelle pour refermer ses crocs acérés sur sa victime. Si celle-ci parvient à échapper par extraordinaire à la fureur de l''attaque, le Basilic use alors du pouvoir hypnotique de ses immenses yeux et de sa voix au sifflement charmeur pour fasciner sa proie qui abandonne alors toute volonté de se battre et marche tel un somnambule vers une mort certaine.\r\n\r\n[i]« T''as de beaux yeux, tu sais ? »[/i] Les Interrogations, de Bael le Barde'),
(147, 147, 'Basilic mineur', 'M', '', 'No', 'neutre', 'reptile', 'walking', 2, 100, 3, 'desert', 21, 46, 37, 50, 'Ce gros lézard à huit pattes qui semble dormir sur les rochers n’est pas aussi inoffensif qu’il y paraît : il s’agit d’un basilic (Basilic mineur, ou encore Basilic commun, selon les régions). S’il est adepte du savoir-vivre méridional, se complaisant à de longues siestes au soleil et adoptant volontiers un train nonchalant, ses yeux mi-clos sont toujours à l’affût de tout ce qui bouge, homme ou bête. Au moindre mouvement, il saute sur sa proie d’un bond et lacère de ses griffes sa victime alors que sa bouche garnie de dents acérées tente de se saisir de son cou. Le pire est alors le regard qu’il vous jette de ses énormes yeux glauques et qui a le don de vous mettre le moral dans les chausses tellement il a l’air triste. L’on raconte qu’une fois séchée sa chair est très appréciée des gourmets, mais, trop âcre pour être consommée directement, elle est surtout utilisée comme condiment.\r\n\r\n[i]« Splotch ! Une pizza basilic pour la cinq, une ! »[/i] Les Chemins de Taverne de Gustavus'),
(148, 148, 'Manticore', 'F', '', 'No', '', '', 'walking', 0, 100, 3, 'desert', 21, 47, 37, 50, 'Pour le voyageur isolé, perdu sous l''impitoyable lumière qu''inflige un soleil brûlant aux vagues de sable calciné du désert Safran, la mort vient parfois de façon plus miséricordieuse en un bref moment d''agonie, plutôt que dans une lente dessiccation et la folie des fantasmagories et autres mirages. Il est possible toutefois que cette perspective ne console pas les victimes de la Manticore, dans les quelques brefs instants de panique folle qui seront sans doute leurs derniers sentiments sur cette terre de souffrance. \r\n\r\nCréature chimérique, la manticore se présente sous la forme d''un lion géant à la face grimaçante, tapie à l''ombre des dunes. Mais crocs et griffes ne sont pas tant dangereux que la plus terrible arme de la bête : modèle de symbiose animale, la Manticore vit en effet en étroite collaboration avec le serpent-flagelle, au poison foudroyant, dont elle use à la manière d''un fouet pour capturer et abattre ses proies qu''elle pourra ensuite dévorer à loisir. Par contre la raison pour laquelle les Manticores portent souvent des chapeaux de cuir reste néanmoins encore aujourd''hui l''un des grands mystères de notre belle Ile...\r\n\r\n[i]« Ta ta taaa ta taaaa. »[/i] Cri très particulier de la Manticore lorsqu''elle use de son serpent - flagelle, selon l''Almanac des Bêtes et Créatures Dangereuses, Exotiques et Féroces de Nacridan, édition de 789, aux Presses Corsaires d''Earok (dir. Commodore Neander)'),
(149, 149, 'Dragon des dunes', 'M', 'dragon-des-dunes.jpg', 'No', '', '', 'walking', 0, 100, 3, 'desert', 22, 60, 51, 66, 'D''une taille modeste pour un noble dragon, le dragon des dunes n''en est pas moins terriblement dangereux. D''une dizaine de mètres d''envergure, il étale volontiers ses écailles safran sur le sommet des dunes, profitant à l''envi de la chaleur brûlante qui écrase le Désert. Extrêmement rapide, il est capable de cracher du feu, et de voler sans difficulté au travers des pires tempêtes de sable, ses yeux étant parfaitement protégés par une paupière transparente semblable à celle des crocodiles. Paupières d''ailleurs très recherchées par les puissants de ce monde : l''une de ses propriétés étant notamment de corriger la vue défaillante de celui qui les porte en sautoir sur le nez. \r\n\r\nSi le Dragon des Dunes est un adversaire redoutable le jour, il est fort heureusement possible de le tuer la nuit : son sang-froid se glace quand la nuit tombe et que vient le froid, et le rend incapable de bouger quand surgissent les hommes de Dust et leurs longues piques à dragon ; il est cependant difficile de trouver la tanière du monstre, qui s''enfouit souvent sous le sable pour la nuit, obligeant ceux qui le traquent à fouiller au hasard les dunes. Et malheur à ceux que surprendrait la levée du jour, si le dragon se réveille ! \r\n\r\n[i]« Un écu d''or à celui qui repère le dragon des dunes blanc ! »[/i] in Chasseur de dragon, du Capitaine Macab''.'),
(150, 150, 'Scorpion géant', 'M', 'scorpion-geant.jpg', 'No', '', '', 'walking', 0, 100, 3, 'desert', 22, 61, 51, 66, 'Cette créature endémique du Désert Safran mesure plus d''une dizaine de mètres de long. Avec sa lourde cuirasse de chitine toute d''or et de sable, il est le cauchemar des marcheurs du désert lorsqu''il fond sur eux, énormes pinces tranchantes comme des lames et queue au dard suintant au venin si foudroyant qu''une Manticore peut en mourir en quelques instants. Lorsque les facettes cruelles de ses nombreux yeux vous ont repéré, il est déjà bien trop tard pour fuir : ses pattes solides comme l''acier et couvertes de pointes aiguës tricotent dans le sable mou comme les échasses d''un troupeau de hérons infernaux et sont capables d''accélérations foudroyantes. \r\n\r\nMachines de guerre accomplies et terrifiantes, les scorpions, recueillis en leur jeune âge au prix de grands risques, ont souvent été dressés et utilisés au combat par les tribus nomades du désert qui s''en servent ainsi que d''une monture infatigable pour traverser les vastes étendues sablonneuses. Après les pactes d''alliance entre les tribus et la cité blanche, ils furent employés comme cavalerie lourde par les corsaires de Dust, contribuant à forger le mythe de l''imprenable cité. Comment décrire l''effet que peut provoquer sur la vessie du jeune conscrit ou du paladin qui rôtit depuis plusieurs heures dans son armure la vision d''une dune se couvrant soudain de monstrueux scorpions recouverts d''acier et chevauchés par des semi-déments en burnous écarlate ? Arme de guerre comme de siège, puisque les griffes des scorpions leur permettent de grimper aisément sur les remparts de la plus puissante des forteresses. \r\n\r\nLa fameuse charge de la Gorge Noire, au pied des montagnes du même nom, où cinq cents de ces créatures recouvertes d''épaisses plaques d''acier et montées par des chevaucheurs de scorpions brisèrent l''avancée des piques d''Izandar, et du même coup, les ambitions hégémoniques de la ville aux toits d''or, est l''un des derniers exemples de bataille où les scorpions furent déployés. En effet, leur utilisation est malheureusement difficile en dehors du désert Safran : extrêmement sensible aux changements de température du fait de leur sang-froid, le moindre soubresaut du climat signe la mort rapide des grands arachnides.\r\n\r\n[i]« Pourquoi croyez[/i] - [i]vous que nous nous sommes installés au pied d''un glacier ? Bon, après moi j''ai pas peur d''eux, hein. Faut pas croire. »[/i] Nidaime de Nidascon, de Nidaime. '),
(152, 152, 'Basilic roi', 'M', 'basilic-roi.jpg', 'No', 'neutre,mauvais', 'reptile', 'walking', 2, 100, 3, 'march', 24, 62, 51, 66, 'A l’instar des autres ophidiens, les Basilics Majeurs ne cessent jamais de grandir au fur et à mesure que s’écoulent les années de leur vie. Rares toutefois sont ceux qui parviennent à atteindre le nombre de mues suffisantes pour être qualifiés de [i]« royaux »[/i]. \r\n\r\nAtteignant, et dépassant bien souvent, la trentaine de mètres de long, les Basilics Rois sont définitivement les seigneurs des Marais du Désespoir. Outre leur taille terrifiante, dont ils usent pour écraser leurs proies, ils peuvent projeter leur venin foudroyant à de grandes distances avec une étonnante précision, en contractant les puissants muscles de leur œsophage démesuré. Et quand bien même cela ne suffirait point, leur terrifiant regard est capable d’arracher l’âme même du corps, plongeant celui qui en est victime sans y être préparé dans un coma mortel. \r\n\r\n[i]« … et Carne descendit dans la grotte, et les Yeux du basilic royal étaient là qui le regardaient… »[/i] in La légende des mois, Hictor Vugeaux. '),
(153, 153, 'Vouivre', 'F', 'vouivre.jpg', 'No', '', '', 'walking', 0, 100, 3, 'march', 24, 63, 51, 66, 'Cette créature farouche au torse de femme (note pour les aventuriers : nue, la femme) demeure tapie dans les rivières et les lacs isolés, dont l''eau claire et les herbes folles sont autant de caches. Aisément reconnaissables, les vouivres évitent en général la confrontation : toutefois, lointaines parentes des dragons, elles veillent jalousement sur leurs trésors, et peuvent se montrer particulièrement cruelles et vicieuses quand il s''agit de les défendre face à une bande d''aventuriers avides, n''hésitant pas à user de crocs au venin foudroyant, ainsi que de leurs talents latents, capables d''égarer l''esprit et de troubler les sens. \r\n\r\nOn raconte dans les tavernes que ces pouvoirs magiques sont liés directement à l''escarboucle, gros rubis que la vouivre arbore au front, et qui fait l''objet d''un trafic rentable : de nombreux mages, généralement mineurs, croient en effet que la possession d''une escarboucle véritable augmentera considérablement leur pouvoir…'),
(154, 154, 'Etranglesaule', 'M', '', 'No', '', '', 'walking', 0, 100, 3, 'mountain', 25, 48, 25, 36, 'Parfois, même les plus tordus des magiciens doivent s''avouer vaincus par l''humour bien particulier de Dame Nature. L''étranglesaule, cousin du carnetremble, en est probablement l''un des meilleurs exemples tant il semble sortir de nos cauchemars d''enfant : quand à la lumière crue d''Arryn, les arbres tordus qui s''agitent dans le vent semblent autant de mains cruellement crochues tendues vers soi. L''étranglesaule est ce cauchemar devenu réalité ; un saule massif au tronc argenté, empli d''une vigueur mystérieuse, une sombre volonté se tendant dans ses branches tordues et ces racines profondes. \r\n\r\nCet arbre sournois ne hante guère que les lieux isolés où il se meut à une vitesse inquiétante dans les ténèbres, même si l''on peut en trouver n''importe où, en particulier dans les montagnes grises, où il semble se plaire. L''important est qu''il y ait un peu de passage : malheur à l''imprudent qui piquerait un somme à ses pieds. Une branche souple aura tôt fait d''enserrer son cou ainsi qu''un collet d''acier, avant de hisser le malheureux jusqu''au sommet du tronc, où de fines vibrisses se déploient, pénétrant la peau pour y injecter une sève atrocement corrosive, réduisant rapidement la chair et les organes internes en une bouillie infâme - l''arbre s’en nourrira tel Aé Li sifflant un pomme-sureau du chef avant de laisser mollement choir la dépouille vidée de son contenu. La légende veut que ce goût inhabituel pour les protéines animales provienne de la pratique ancestrale des habitants de Krima de déposer les corps de ceux qui sont morts au sein d''un vaste champ de saules nommé la Sauleraie.'),
(155, 155, 'Sauteur fauchard', 'M', 'sauteur-fauchard.jpg', 'No', '', '', 'walking', 0, 100, 3, 'mountain', 25, 49, 25, 36, 'Cet oiseau géant, très volontiers anthropophage, est l''une des terreurs maudites qu''adoraient les Prêtres Noirs de la Cité de Landar. Se déplaçant dressé sur ses pattes arrières, le Sauteur Fauchard darde son bec cruel à hauteur des yeux d''un Elfe. Aussi large d''épaule qu''un homme, c''est un adversaire terrifiant à la force disproportionnée. Extrêmement mobile et rapide, c''est une véritable machine à tuer : affronter un Sauteur est comme affronter une tempête hurlante, munie d''un bec ravageur et d''ergots empoisonnés. \r\n\r\nS''il peut sauter à des distances incroyables, l''oiseau est toutefois incapable de voler, et pour cause : ses ailes se sont peu à peu atrophiées au cours des âges pour se transformer en bras aux muscles puissants, capables de brandir les faux acérées qu''ils affectionnent tant. Son plumage incroyablement riche aux couleurs scintillantes est le signe de son rang dans la société des Sauteurs – plus celui-ci est somptueux, plus l''oiseau tueur est haut placé. Bien qu''intelligents – suffisamment en tout cas pour manier de manière redoutable leurs armes meurtrières –, les Sauteurs Fauchards vacillent toujours au bord de la folie sanguinaire, et n''aiment rien tant que répandre carnage et dévastation autour d''eux, n''hésitant pas alors à s''entre-tuer dans leur folie si aucune autre victime ne se présente. Seul un culte aussi malsain que celui de Sareco pouvait s''en servir comme milice urbaine. Il est vrai que leur efficacité dans le domaine du maintien de l''ordre était indéniable, si l''on exceptait leur habitude un peu salissante de dévorer sur place les contrevenants. Après un bête accident avec la fille unique d''un Grand Inquisiteur, les Sauteurs furent uniquement utilisés comme troupe de choc par Landar, jusqu''à leur tentative de soulèvement en 3E879, à la suite de quoi une majeure partie des régiments réguliers fut soigneusement massacrée sur ordre du Grand Dôme et le reste prit la fuite dans les Montagnes Noires et le Bois-La-Pluie, que leurs descendants hantent encore actuellement.\r\n\r\n[i]« - Vous remarquerez tout de même que les chiffres de la récidive dans notre Cité sont les plus faibles jamais enregistrés dans notre belle Ile depuis plus de six siècles ![/i]\r\n[i]– En effet, je note que lâcher des oiseaux anthropophages géants sur les mendiants fait diminuer en quelques nuits la paupérisation criminelle, c''est remarquable ! Et dire que certains voudraient encore lancer des plans d''aide sociale inutilement coûteux… je pense que nous allons vous acheter une brigade d''œufs pour ma forteresse [/i]\r\n[i]– pour commencer… »[/i] La Justice n''est qu''une question de bon goût, par Skato, de l''UMN.'),
(156, 156, 'Gargouille', 'F', 'gargouille.jpg', 'No', 'chaotique,mauvais', 'chimère', 'walking', 0, 100, 3, 'mountain', 26, 50, 37, 50, 'La Gargouille se présente comme une hideuse créature aux formes tourmentées. Ses grandes ailes repliées, ses membres griffus ramenés sous son corps difforme, son horrible tête ornée d’une corne torsadée et arborant un sourire marmoréen, tout cela, ainsi que son immobilité légendaire et la couleur uniformément grisâtre de tout son corps fait qu’on la prend souvent pour une simple statue. Mais malheur à qui se laisse abuser par son mutisme ! Elle est généralement placée là par quelque enchanteur à la vocation artistique douteuse et attaque quiconque tente de dépasser le seuil interdit qu’elle est chargée de garder. Sans-Peur de Dhorîn, fut dit-on, le premier à développer les incantations magiques permettant de créer ces êtres fantastiques et son Grand Œuvre a depuis fait nombre d’adeptes chez les mages un tant soit peu cossus et soucieux de préserver des pillards leur résidence secondaire durant leurs longues absences.\r\n\r\n[i]« 8 de la Lune Blême : Froid Glacial. Avons dégagé un immense rocher et découvert deux splendides statues, datant sans doute de l’Age du Ver.[/i]\r\n[i]9 de la Lune Blême : Les statues ont bougé ! Avons été obligés de les abattre. C’est maintenant sûr, nous sommes sur le chemin de l’ancien palais du Prince Fou»[/i] Cités Perdues et Ruines Célèbres du Grand Nord de Zaza l’Usineur.');
INSERT INTO `BasicRace` (`id`, `id_BasicProfil`, `name`, `gender`, `pic`, `PC`, `alignment`, `class`, `state`, `dropItem`, `percent`, `frequency`, `land`, `band`, `behaviour`, `levelMin`, `levelMax`, `description`) VALUES
(157, 157, 'Golem d''argile', 'M', 'golem-de-boue.jpg', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 26, 51, 25, 36, 'Selon la légende transmise depuis l''Aube des Temps par les Scaldes d''Octobian, le premier Golem fut créé dans le ghetto nain d''Earok, par l''Aoksclade Kloew Beersteinson pour protéger son peuple des attaques incessantes de l''UMN. Et il est vrai qu''un colosse aux tendances homicides d''une tonne d''argile, capable de traverser un mur de taille moyenne d''un coup de poing sans même le remarquer forme une protection plus qu''acceptable. \r\n\r\nIls sont cependant assez rarement employés au combat, y compris dans les armées naines. En effet, en dépit de leur indéniable talent en tant que machine de guerre, il reste néanmoins indubitable qu''ils sont terriblement coûteux en énergie  lors de leur création, et qu''il est en outre fort difficile de les contrôler ; bien qu''insensibles à la magie, d''une vigilance à toute épreuve, et d''une force colossale, les Golems sont proverbialement instables mentalement et prompts à échapper à la surveillance de leur créateur, provoquant alors d''incommensurables dégâts avant de disparaître dans les montagnes. De nombreux cas ayant échaudé les stratèges nains, seul le clan Beerstein est encore autorisé à produire des golems pour la guerre, selon des procédés anciens et soigneusement contrôlés. Il existe cependant de nombreuses contrefaçons, les autres branches de magie n''ayant pas tardé à tenter de mettre au point leurs propres golems. (voir Bière, golem de, et Chair, golem de).\r\n\r\n[i]«GO LEM GO !»[/i] Ordre de combat pour l''infanterie Golem.'),
(158, 158, 'Doppleganger majeur', 'M', '', 'No', 'chaotique,mauvais', 'métamorphe', 'walking', 0, 100, 3, 'plain', 26, 52, 25, 36, 'Plus pernicieux encore que le doppelganger commun, le doppelganger d’élite est caractérisé par une propension plus marquée à la violence et à la sournoiserie. D’une haute intelligente, il est capable d’aspirer la mémoire même de sa victime, ce qui lui permet de jouer son rôle d’une façon incroyablement réaliste, jusqu’au moment où il sombre dans une frénésie de mort, assassinant de manière particulièrement ingénieuse et violente ceux dont il a fait ses proies. Depuis la déclassification des archives secrètes de l’Empire Izandrin, des preuves de plus en plus marquées tendent à relier l’apparition et l’existence de ces créatures métamorphes, capables de prendre n’importe quelle apparence et de se glisser n’importe où, au mystérieux [i]« Projet MAN attend »[/i] mené dans le secret le plus total par le département de Traumaturgie de l’Université de l’Invisible, et brutalement stoppé sans explications.\r\n \r\n[i]«  … Le projet MAN attend est une fiction de piètre journalisme ! Nous n’en avons jamais entendu parler ! Et puis c’est du passé tout ça ! Du passé, vous m’entendez ! Nous avons tous fait des choses dont nous ne sommes pas fiers ! C’était la guerre !…»[/i] Extrait de l’entrevue accordée par Mistrim Rudculle, Uber Archichancelier en chef de l’Université de l’Invisible, au Barbare déchaîné, et jamais publiée par suite de la disparition prématurée du journaliste. '),
(159, 159, 'Araignée titanesque', 'M', 'araignee-titanesque.jpg', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 27, 53, 37, 50, 'Un vacarme énorme provoqué par des arbres tombant brutalement à terre, une odeur putride qui se répand alentour, de gigantesques blocs de rocher qui choient soudainement, tels sont les signes précurseurs de l’arrivée imminente de l’Araignée Titanesque. Seuls les plus braves et les plus aguerris resteront pour contempler en face ce mastodonte de poils et de piques : son corps couleur rouille dépasse aisément la dizaine de mètres et ses immenses yeux sans fond sont comme le présage du néant qui attend celui qui la croise. Armée de terribles mandibules, elle est aussi dotée d’une rapidité foudroyante dont elle se sert pour écraser sa proie d’un bond. \r\n\r\nAlors commence le supplice pour la pauvre victime : utilisant simultanément les deux dards situés à l’arrière de son abdomen, elle lui inocule des poisons qui tour à tour la paralysent et attaquent irrémédiablement son système nerveux central. Le venin qui s’écoule de ses dards est d’ailleurs fort réputé parmi les apothicaires, mires, rebouteux et autres assassins mais rares sont ceux qui osent aller prélever la précieuse substance !\r\n\r\n[i]« Là, là ! Je vois une grosse bête ! Mais si, regarde ! »[/i] in [i]Les Derniers Jours et les Choses[/i], d’Aé Ziode'),
(160, 160, 'Golem de bière', 'M', '', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 27, 54, 37, 50, 'Originellement armes de guerre, ces homoncules hauts comme trois hommes furent utilisés pour la première fois au combat en 745 par les scaldes d''Octobian, pour briser le siège de la cité troglodyte par les troupes de l''Hegemon Artassien, lequel durait depuis six ans déjà. Selon l''Histoire, ces créatures dépourvues d''intelligence se seraient tout simplement écoulées par-dessus les remparts de la Cité Naine pour submerger littéralement les camps retranchés des assiégeants, noyant ceux-ci sous des hectolitres de liquide malté. Profitant de l''état d''ébriété proche du coma éthylique dans lequel ces derniers se retrouvèrent par la suite, les guerriers nains n''eurent plus qu''à sortir pour faire prisonnier l''intégralité des troupes ennemies, des tambours aux généraux, brisant pour longtemps les vues impérialistes d''Artasse sur le Nord. La victoire est d''ailleurs fêtée chaque annnée depuis lors par les nains lors de la fameuse Golemenbier''Festung, qui attire toujours un nombre considérable d''aventuriers et d''étudiants en médecine, désireux de combattre les Golems de Bière qui sont lâchés dans les rues de la cité, à charge pour les combattants de les boire entièrement –  à leurs risques et périls… Aisément convocable par quiconque dispose de très larges stocks de bière, existant en de nombreuses déclinaisons (lager, stout, pilster, ale…) ce Golem original se distingue par une très grande résistance aux armes conventionnelles, incapables de le blesser, et, depuis la Loi sur la Pureté des Golems de 867, une excellente résistance aux méfaits de l''évaporation, qui jusque-là, étaient son talon de Lord Thergal. \r\n\r\nAprès leurs premiers succès à la Bataille de la Gueule de Bois, les golems de bière connurent un succès foudroyant auprès des étudiants en magie, comme en témoignent chaque dimanche matin les violentes migraines de nombre d''entre eux, signe particulièrement frappant qu''un nouveau golem a échappé au contrôle d''un étudiant et erre dans les montagnes d''Izandar, furieux et déboussolé.\r\n\r\n [i]«[/i] - [i]Ach ! Chai une zuper idée pour faincre les nains ! Nous zallons faire ein Qollem de Pière empoisonné, ja ! Eurégâ !»[/i] Tiscours triomphant du Herr Doktor Krolenberrg à la neuvième convention des Brasseurs Indépendants Artassiens de 747.'),
(161, 161, 'Dragon de vent', 'M', 'dragon-de-vent.jpg', 'No', 'neutre', 'élémentaire', 'walking', 0, 100, 3, 'mountain', 27, 55, 37, 50, 'Surgi des tourbillons tournoyant amenés depuis les lointains éthers glacés, le Dragon des Vents se présente aux regards terrifiés des passants sous la forme d’un immense lézard à la couleur grisâtre. Démuni d’ailes, les plus savants de Nacridan ne le considèrent pas comme un véritable dragon mais plutôt comme un élémentaire tirant son essence et sa force des puissances du Corps d’Hiolis. Seuls les plus puissants mages savent le dompter au moyen de flûtiaux et maîtriser ainsi sa fougue légendaire. Les autres affronteront en vain son corps élancé et mobile comme l’air, aussi insaisissable que le vent et aussi terrible qu’un cyclone lorsqu’il se met en tête de renforcer sa substance d’une matière plus tangible. Planant au-dessus de ses proies, porté par les masses d’air déchaînées qui le soutiennent, il décrit de larges cercles avec une rapidité surnaturelle avant de fondre sur ses victimes comme la tempête s’abat sur le pauvre peuple. Sa nature éthérée laisse alors place à de terribles mâchoires qui se saisissent de quiconque l’aura contrarié dans ses desseins étranges, et qui l’amèneront vers un septième ciel aérien et fatal.\r\n\r\n[i]« Avis de tempête sur Ouest[/i] - [i]Izandar, je répète, avis de tempête sur Ouest[/i] - [i]Izandar. Ne sortez pas de chez vous, ne prenez pas la mer, un Dragon des Vents est signalé sur la zone ! Tiens, il y a comme un courant d’air dans la pièce, quelqu’un aurait laissé une fenêtre ouverte ? »[/i] Les Bulletins de la Météorologie Nacridane'),
(162, 162, 'Grande mère Wyverne', 'F', 'grand-mere-wyverne.jpg', 'No', 'neutre', 'créatures draconique', 'walking', 2, 100, 3, 'mountain', 28, 58, 51, 66, 'Si la Grand-mère Wyverne possède le regard furibard de la pire belle-mère découvrant sa progéniture dans les bras de son petit ami, elle ne fait pas partie pour autant de la race des dragons auxquels elle ressemble pourtant par plusieurs aspects. Son corps de reptile couvert d’épaisses écailles d’un vert luisant est doté d’ailes opalescentes et se prolonge par une queue gracile munie de piques et d’un dard dont la vivacité n’égale que la létalité. Dressée sur ses deux pattes de lézard, la gueule ouverte en un sifflement perfide et sa queue tournoyant dans une danse inquiétante, elle n’a rien d’une mamie-gâteau et fera tout pour défendre sa tanière face aux aventureux qui s’en approchent un peu trop. Sa technique favorite est de s’élever quelques instants dans les airs avant de fondre sur les jambes de sa proie et, profitant du déséquilibre de son adversaire, de lui injecter son venin mortel. Très recherchée par les plus téméraires, sa peau constitue une pièce de choix pour confectionner des armures de première qualité.\r\n[i]« J’vais m’la faire, la grosse allude ! Bizarre quand même qu’elle ressemble autant à ma maman… Euh, Mamie, c’est toi ? »[/i] Paroles Illustres des Champs de Bataille, de Nidaïme.'),
(163, 163, 'Wyverne', 'F', '', 'No', 'neutre', 'créatures draconique', 'walking', 2, 100, 3, 'mountain', 28, 59, 51, 66, 'Aussi appelé vouivre, ce monstre reptilien est parent des dragons.  Néanmoins, bien que redoutable, une wyverne n''a rien de comparable avec ses grands cousins. La wyverne a deux ailes, deux pattes et une queue terminée par un dard semblable à ceux des scorpions. '),
(164, 164, 'Dragon rouge', 'M', 'dragon-rouge.jpg', 'No', 'chaotique,mauvais', 'dragon', 'walking', 2, 100, 3, 'mountain', 29, 1, 67, 84, 'Le Dragon Rouge est l''une des créatures les plus redoutables parmi celles qui hantent les cieux de Nacridan. De stature moindre que son royal cousin, il est cependant beaucoup plus mobile, n''hésitant jamais à accomplir de grandes distances pour trouver de nouvelles proies. C''est l''adversaire traditionnel des paladins en quête de notoriété, bien que les rumeurs concernant la prétendue préférence de ce dragon à l''écarlate armure pour la chair des jeunes elfettes vierges ne soit pas avérée : ils mangent en réalité n''importe quoi, voire n''importe qui, même s''il est vrai que les chevaliers, fussent-ils convenablement rôtis, tiennent en général plus de la boîte de conserve que de la vraie nourriture. \r\n\r\nAu-delà de son appétit démesuré, c''est surtout pour son souffle embrasé qu''il faut craindre le dragon rouge, tant il en use avec libéralité; à tel point que de nombreuses villes ont établi des règlements anti-draconiques dans les nouvelles constructions, D''ailleurs, après le dramatique accident lors des Ténèbres de Cendre d''Earok, la ville des plaines du centre a cessé de construire des demeures en bois.\r\n\r\n[i]« N''oubliez pas les enfants, quand le dragon est rouge, ne traversez surtout pas sans regarder à gauche ET à droite !! »[/i] in Conseils de dernière minute aux enfants surnuméraires, par dame Adah, aux Presses facultatives de Krima (PFK).'),
(165, 165, 'Phénix', 'M', 'phenix.jpg', 'No', 'neutre', 'élémentaire', 'walking', 0, 100, 3, 'mountain', 29, 1, 67, 84, 'Noble et sage créature du feu, le Phénix se présente sous la forme d''un immense rapace de flamme à la voix splendide, chantant des mélodies de sang et de feu, tandis qu''il danse parmi les nuages, éclairant les nuits de l''Ile d''une lueur pourpre. Rares, mais dépositaires d''une connaissance qui remonte aux premiers âges du monde, les phénix sont les plus anciennes créatures de l''Ile, plus âgées même que les vieux dragons qui se tapissent dans les cols perdus au cœur des Crocs du Monde. Leur vol les porte par-delà tous les monts, toutes les rivières, depuis le désert Safran où ils naquirent, et toujours ils doivent regarder sans agir, car tel est le destin des oiseaux de flamme qui ne peuvent mourir qu''en se jetant dans la mer qui éteint à jamais leur essence ignée. Il est dommage que certains aventuriers sans foi ni loi s''en servent comme monture, mais heureusement, les inconvénients de chevaucher une monture constituée de pures flammes, comme les brûlures au douzième degré, apparaissent bien vite à ces téméraires individus, qui en général préfèrent laisser aux magiciens de l''école du Feu le soin de s''occuper de cette créature.\r\n\r\n[i]« [/i]– [i]Un oiseau de feu qui s''élève d''une coupe… c''est marrant, ça m''évoque quelque chose, mais quoi ? »[/i] Bien vendre sa bière pour les nains, par Zildjan, directeur mercantique de la Banque Maritime.'),
(166, 166, 'Griffon', 'M', 'griffon.jpg', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 29, 1, 67, 84, 'D''apparence noble, cette créature mi-aigle mi-lion au plumage mordoré affectionne particulièrement les hauteurs désolées et inaccessibles au commun des mortels, en particulier les pics qui parsèment les Monts Gris ainsi que les rochers entourés par la mer froide de la Grève Lointaine. Pareil goût pour la solitude n''étonne guère chez ces créatures farouchement territoriales, qui n''hésitent jamais à attaquer avec la plus extrême violence les étrangers qui violent ce qu''elles estiment être leur territoire. Les griffons n''ont ainsi jamais pu tolérer les dragons, ce qui explique leur rareté parmi les hauteurs des Crocs du Monde pourtant bien faites pour les attirer. \r\n\r\nEn dépit de la ténacité avec laquelle les griffons défendent leurs aires et de leur dangerosité, ils font l''objet d''une véritable traque de la part des aventuriers, qui prennent tous les risques pour parvenir à en capturer un : leur plumage merveilleusement doux et chaud coûte en effet une véritable fortune, assez pour enrichir un homme jusqu''à la fin de ses jours. Quant aux œufs de l''animal, ils se vendent à des prix faramineux : très intelligent, le griffon fait une monture de choix pour les puissants de ce monde. Il est toutefois interdit de les chasser de quelque manière que ce soit sur l''île de la Lumière Étoilée, domaine de la maison de Greif, dont il est l''emblème depuis l''aube des temps. Selon la légende, reconnaissants, en cas de grands périls, les griffons viendraient spontanément à la rescousse du Greifroc.\r\n\r\n[i]« De la mer comme le griffon l''éther »[/i] Devise de la Maison de Greif.'),
(167, 167, 'Kraken terrestre', 'M', 'kraken.png', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 29, 1, 67, 84, 'Pour les savants mages de l''Université de l''Invisible, c''est un merveilleux exemple de l''adaptation des formes de vie à la magie. Pour les éminents professeurs de la Faculté de Pseudozôologie de Krima, c''est un mollusque carnivore de type giga absolument fascinant. Pour toi, aventurier, c''est une saloperie de pieuvre large comme trois Balors, avide et visqueuse, qui a décidé de t''ajouter très prochainement à son menu, catégorie plat principal. \r\n\r\nParfaitement amphibie grâce au mucus protecteur que sécrète en continu son épaisse peau, le Kraken Terrestre, pour lourd et pataud qu''il semble lorsqu''il se traîne à la force des tentacules sur la terre ferme, est capable d''accomplir en une seule nuit de très longs trajets d''une cachette à l''autre. Il affectionne en effet, en son jeune âge, les lieux sombres et humides, dans lesquels il se terre, guettant l''arrivée d''une proie à portée de ses pseudopodes fureteurs. Ne cessant jamais de grossir, les plus vieux s''enterrent ou se glissent dans les ruines désertées de civilisations disparues, étant assurés dans ce cas de ne jamais manquer d''une ample provende d''aventuriers en quête de trésor, voire dans certains cas bien précis, d''adorateurs un peu naïfs. \r\n\r\nAdversaire redoutable, il est très dur à tuer : sa peau aux multiples couleurs est particulièrement résistante et ses tentacules colossaux frappent l''air et les corps à la vitesse d''un fouet. Et en plus, il crache de l''acide !\r\n\r\n[i]«Oh zut, on dirait qu''ils bougent»[/i]\r\n Chturlulu &amp; Cie, 2000 ans d''expertise dans le domaine du tentacule.'),
(168, 168, 'Dragon royal', 'M', '', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 30, 1, 85, 104, 'Vieux de centaines d''années, antiques gardiens de leurs fabuleux trésors, rares et puissants sont les grands Dracosires des Crocs du Monde. Pères de la race perdue des Dragons, survivants d''une époque révolue, leur intelligence et leur ruse maligne dépassent de beaucoup celles des autres mortels, qui ne sont pour eux que des insectes agaçants, que leur souffle ardent aura tôt embrasés. Les seigneurs dragons demeurent au fond de leurs grottes immenses, dans leurs cavernes brillantes, leurs écailles écarlates reflétant leurs feux internes. Ces gigantesques reptiles, doués de parole, dont les plus grands spécimens dépassent souvent les vingt mètres, sont en effet très casaniers. \r\n\r\nLovés sur leurs trésors, ils veillent sans trêve, pensant des idées au-delà de l''intellect des faibles mortels, ne quittant leurs abris confortables que lorsqu''un perturbateur vient profaner leur domaine, car ils ne craignent nulle force, confiants dans la puissance de leur feu, de leurs griffes, et de la magie qu''ils dominent tous à des degrés divers. Affronter un sire dragon et survivre est plus qu''un exploit. Sauf, évidemment, si ce dragon mesure quelques mètres et se cache sous une pierre. Nombreux sont les tueurs de dragons auto-proclamé qui n''ont en fait qu''abattu froidement un bébé dragon pour lui voler ses écailles.\r\n\r\n[i]« Mais que fait Brigit Bordeau ? »[/i]\r\nAdah, [i]Manifeste d''une aventurière choquée[/i]'),
(169, 169, 'Griffon argenté', 'M', '', 'No', '', '', 'walking', 0, 100, 0, 'mountain', 30, 1, 85, 104, 'Encore plus, rare, si possible, que le Griffon, le Griffon Argenté se distingue bien entendu par son plumage splendide, dont la couleur rappelle invinciblement celle du métal précieux d''où il tire son nom. Il se distingue cependant de son petit-cousin par une envergure deux fois supérieure et une agressivité sans commune mesure. Considérablement plus lourd, en effet, le griffon argenté peut se targuer d''être l''une des plus impressionnantes créatures volantes : les dragons les plus modestes eux-mêmes évitent en général de prendre le risque de déranger le griffon argenté dans son aire ; celui-ci la défendra avec la plus grande ténacité. D''autant plus que, contrairement au griffon, l''argenté n''est pas tout à fait le produit d''une évolution naturelle : l''espèce fut en effet élevée parmi les sables de Dust par des nomades du désert qui désiraient disposer de montures plus rapides que les traditionnels scorpions des sables. Ceux-ci sont certes impressionnants mais quelque peu ridicules à une époque où le moindre guerrier dispose d''un dragon des neiges pour monture. \r\n\r\nParmi les remarquables aptitudes du Griffon Argenté, on observe particulièrement sa capacité à repérer à des kilomètres de distance les ennemis de son maître, mais également les ressources, telles que les points d''eau, et plus curieusement, les mines de diamant. Au combat, il dispose d''un atout imparable dans la queue de scorpion qu''il arbore, ainsi que dans les ergots empoisonnés qui ornent ses griffes, mais c''est également son cri particulièrement perçant qui le rend redoutable : il est capable de crever les tympans d''un elfe à quelques dizaines de mètres de distance, et de faire vomir un nain sobre. Du temps de la gloire de l''Union Obscure, l''on raconte que deux griffons argentés doués d''une voix humaine montaient jour et nuit la garde devant la Porte des Enfers à Néphy, et qu''ils interrogeaient chacun des visiteurs pour démasquer les espions.\r\n\r\n[i]« ''''Avoir un cri de griffon argenté.'''' Parler très fort, avec une voix désagréable, et généralement en faisant des commentaires acerbes qui dérangent les personnes présentes. Se dit généralement de sa belle[/i] - [i]mère ou d''un leader syndical. »[/i] in Dictionnaire des Proverbes et Synonymes abscons de Nacridan, par Robert [i]« Larouste »[/i] Lepetit.'),
(170, 170, 'Araignée colossales', 'M', 'araignee-colossale.jpg', 'No', '', '', 'walking', 0, 100, 0, 'forest', 20, 56, 25, 36, 'D’énormes pattes velues armées de griffes, un corps flasque et gigantesque atteignant aisément au garrot la taille d’un homme, l’araignée colossale est terrifiante pour quiconque s’avance près de son nid. La teinte brun-roux de son pelage lui procure un camouflage efficace parmi les branchages et les feuilles mortes à la saison automnale. Bien qu’arboricole de nature, elle ne déteste pas d’assaisonner son menu quotidien de protéines animales, surtout lorsqu’elle juge son nid attaqué. Elle chasse alors en groupe, chacune sautant sur la proie à tour de rôle afin de planter son terrible dard au poison mortel dans le corps de la pauvre victime. \r\n\r\nCertains racontent aussi qu’elles tissent des pièges de toile en vue de piéger les voyageurs au détour des forêts peu fréquentées, mais peu nombreux sont ceux qui peuvent en témoigner. Une fois l’imprudent immobilisé dans leurs rets, elles le farcissent de pommes sauvages avant de le déguster tout à loisir. D’autres rapportent qu’elles se hissent dans les arbres afin d’attaquer par surprise tout être vivant qui passe à leur portée.\r\n\r\n[i]« Si tu es perdu dans la forêt, regarde bien où tu mets les pieds, observe les buissons, les branchages…  Une araignée peut bien manger qui n’est pas sage ! »[/i] Les Chroniques de la Survie d’Anraud [i]« Bicornu »[/i] de Greif.'),
(171, 171, 'Gnoll', 'M', 'gnoll.jpg', 'No', 'chaotique,mauvais', 'humanoïde', 'walking', 0, 100, 3, 'forest', 20, 57, 25, 36, 'La nuit est le domaine de prédilection du Gnoll, parfaitement nyctalope grâce à ses yeux immenses (ne dit-on pas "[i]«y voir comme un gnoll par Noire Arryn ?»[/i]). C''est à l''abri des plus sombres ténèbres que les charognards au corps difforme et bossu se rassemblent autour d''une proie inconsciente, par exemple un aventurier qui n''a pas eu l''élémentaire précaution de tisonner son feu avant de s''endormir. Leur pelage souillé de boue et de terre rend les Gnolls invisibles à l’œil humain, et seul une bourrasque de vent pourra les dénoncer, en portant jusqu''au guetteur l''odeur de pourriture qu''exhalent ces monstres. Mais il est bien souvent trop tard pour fuir, et quand monte dans le noir le gloussement hideux et obscène du gnoll et que la lueur du feu se reflète sur des yeux avides, bien rares sont ceux qui brandissent encore leur fer d''un air ferme.\r\n\r\n[i]«[/i] - [i] Tu te moques de moi, c''est ça, Gnoll ? Attend, tu vas voir !… euh quelqu''un aurait un briquet ?»[/i] Un chemin de feu, par Albus'),
(172, 172, 'Gandrazc', 'M', 'garde-demon.jpg', 'No', '', '', 'walking', 0, 100, 0, 'plain', 18, 1, 85, 104, 'Au cœur de la société strictement ordonnée de l''Empire des Sept Enfers, l''indépendance et l''action solitaire sont relativement peu encouragées à la guerre. Les démons considèrent que le combat ne se conçoit qu''au sein d''un tissu de relations, clientèles et liens d''âme extrêmement stricts, reflets de la vie de la démoncratie. \r\n\r\nToutefois, le cas des Gandrenzcs, que l''on traduit bien souvent par [i]« guerrier »[/i] ou [i]« gardien »[/i], est extrêmement particulier : ce sont en effet des âmes anciennement réduites en esclavage par les Démons qui, à force d''efforts, sont parvenues à gagner de leur maître – s''il ne les a pas dévorées avant – une forme de semi-liberté particulière que l''on désigne sous le nom d''Affiliation ; à charge pour elles de dévorer la force et l''énergie d''autres âmes de même rang afin d’évoluer jusqu''à devenir un être démoniaque, suffisamment fort pour être capable d''intégrer les cercles les plus inférieurs du Socuzsc démoniaque. \r\n\r\nCes nouveaux jeunes démons, vibrant d''une toute nouvelle énergie, débordent bien souvent de force et d''ambition, gonflés des âmes qu''ils ont fraîchement soumises. Ils seront peut-être remarqués par un puissant Mandrazsc ou même un simple Meelkagar cherchant à constituer ses Sicentries qui les intégreront dans leurs armées comme auxiliaires, ne combattant pas dans le rang des légionnaires, citoyens de plein droit, mais de façon solitaire et indépendante. Période risquée, mais qui ouvre aux âmes devenues démons la voie vers des rangs supérieurs de citoyenneté à mesure de leurs exploits et des créatures dont ils parviennent à voler l''âme. \r\n\r\nColosses rouge feu, s''entourant volontiers de flammes, ils font de terrifiants opposants, prêts à tout pour parvenir à dévorer votre âme, qui sera peut-être celle qui les fera passer au rang de Legazscranz…'),
(173, 173, 'Legaszcranz', 'M', '', 'No', '', '', 'walking', 0, 100, 0, 'plain', 18, 1, 85, 104, 'Plus connu sous le nom de légionnaire démon, le Légazs forme l''ossature des légions démoniaques depuis les grandes réformes militaires de l''ère Gracczcienne. Conscrits pour 2 500 000 années, ils sont recrutés parmi les Cercles mineurs de la Société Démoniaques, sur décision du Seigneur des Sept Enfers, ou engagés volontaires, et placé au bout d''une chaîne de commandement extrêmement stricte, chaque légionnaire étant partie d''une manipulecz (66 démons) commandée par un sectumanarzck issu du rang, les manipulecdzas étant réunies dans des cohortes de 666 légionnaires, elles-mêmes étant partie d''une légion de 6 666 soldats démons, sous le commandement d''un Meelkagar. \r\n\r\nPourvus d''un armement standardisé, les légionnaires sont lourdement armés : pesante cuirasse en lamelles de sang séché, lourde lance à crocs qu''ils peuvent lancer à de grandes distances sur l''ennemi, mais aussi le terrible gzrarliuz, ce terrifiant glaive à deux mains à la lame redoutablement acérée, avec laquelle leur sauvagerie naturelle et leur force colossale trouvent toute sa mesure. Toutefois, c''est leur discipline, inculquée dans le sang et la violence, qui les rend particulièrement dangereux, sans compter leur nombre, apparemment infini. Capables de manœuvrer de concert avec les troupes autour d''eux, les légionnaires démons forment l''ossature centrale des troupes démoniaques et sont redoutés en tant que tels. On considère, selon les informations rapportées par le Capitaine, qu''il y a environ six légions démoniaques stationnées au Nord de Nacridan. [i]«Vous comprenez dans quel pétrin nous sommes ?»[/i] \r\n\r\n[i]« Alectazcs Zsceta ezsct »[/i], c''est[/i] - [i]à[/i] - [i]dire : [i]« Tiens, voilà du boudin. »[/i] Paroles prononcées traditionnellement par les légionnaires démons lorsqu''ils découvrent leurs épées avant d''attaquer des êtres vivants pour leur arracher leur âme.'),
(175, 175, 'Mandrazc', 'M', '', 'No', '', '', 'walking', 0, 100, 0, 'plain', 18, 1, 85, 104, 'Nul n''ignore que les entités nommées par le vulgaire [i]«daemons»[/i] sont en réalité des esprits du feu, constitués de pure énergie ayant pris forme tangible dans notre monde, et ne pouvant survivre qu''au prix d''une alimentation constante en énergie vitale, ou essence, justifiant leur éternelle avidité pour la chair des êtres vivants. \r\n\r\nLes Mandrazcs, ou Traqueurs d’Âmes, sont les grands maîtres d''œuvre de cette quête sans fin ; généraux des armées démoniaques émanant du Sixième Cercle, les Mandrazcs sont des êtres terrifiants, même pour les leurs, avec leurs corps gigantesques, recouverts d''une cuirasse de leur propre sang noir séché, aussi solide que l''acier, et aux yeux multiples dont les facettes brillent d''une froide intelligence. \r\n\r\nCe n''est pas sans raison en effet que les Mandrazcs passent pour les plus redoutables des démons : outre leur ascendant sans partage sur les légions démoniaques dont ils savent se faire obéir par une cruauté et la promesse de grands butins d''âmes, même les plus modestes d''entre eux, au début de leur conception dans les Limbes, disposent de dons d''illusionnistes et de déguisement, voyageant dans les royaumes vivants pour y apprendre les arts de la guerre et de la politique. Ils en rapportent sagesse, ruse et goût du lucre. Plus d''un Seigneur des Sept Enfers s''étant vu défier et vaincre par un membre de la caste des Mandrazcs, ceux-ci font généralement preuve de défiance envers leurs généraux, répugnant souvent à les laisser aller loin de leur surveillance étroite. \r\n\r\nToutefois, ils sont incapables, selon tous les témoignages, de pratiquer la magie, ce qui peut représenter une maigre consolation pour qui a le malheur de croiser leur chemin. \r\n\r\n[i]«Dztuzsc quozcsque, mizsc fiszli ? !»[/i] Dernier rugissement de Dzules Dszèzar, premier seigneur démon à avoir régné sur les Sept Enfers Unifiés, lors de son assassinat au Sénarszc Démoniaque.'),
(176, 176, 'Meelkagarszc', 'M', '', 'No', '', '', 'walking', 0, 100, 0, 'plain', 18, 1, 85, 104, 'La création du corps des Meelkagarszc remonte aux grandes réformes militaires de l''ère Drazglienne, lorsque les hordes démoniaques furent organisées sous la forme de légions aux effectifs contrôlés de 6 666 démons, répartis en 666 cohortes, équipées de griffes et d''armure de sang standardisées, et formées à la guerre de façon disciplinée, dans l''optique [i]« d''en finir définitivement avec le chaos et la chienlit ! »[/i], et accessoirement d''envahir définitivement les royaumes vivants, qui s''obstinaient à ne pas se laisser réduire en nourriture pour démon. \r\n\r\nToutefois, il apparut rapidement aux démoniaques membres du Sénarszc que pour diriger et garder dans le rang les légions nouvellement créées, un corps intermédiaire de dignitaires devenait nécessaire, les Balors du Cinquième étant par nature trop indépendants, et les généraux du Sixième trop importants. Aussi fut créé parallèlement à la fondation des nouvelles légions le rang et le grade de Meelkagar, plus communément appelés Pourvoyeurs d''effroi. Grade militaire, ce titre est également le premier rang du cursus déshonorant qui organise toute la société démoniaque. \r\n\r\nCeux qui le détiennent ambitionnent rarement de rester à ce stade, et visent vers les rangs supérieurs, ne reculant pas, de ce fait, devant une action d''exploit, comme le massacre en solitaire d''un groupe d''aventuriers. Guerriers accomplis, ils sont aisément reconnaissables de loin aux grandes cornes à crinière qui ornent leurs crânes, symbole de leur dignité, et en outre, ce sont parfois des mages puissants. Il est vivement déconseillé de les provoquer sans une bonne préparation.'),
(260, 260, 'Roi Liche', 'M', '', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 0, 100, 3, 'plain', 99, 1, 140, 180, 'Seigneur de la Géhenne, Maître de la Corruption, Prince des nécromants et commandeur des légions des morts, le sombre roi des Terres Décomposées demeure en son castel vicié au cœur le plus noir de la Vieille Forêt du Nord, où règne omniprésente l''odeur lourde de la putréfaction entre les arbres tordus par des arts maléfiques, et entre lesquels errent sans fin les Loups-garous et les morts avides de la chaleur du sang des vivants. Petite silhouette décharnée blottie dans les soies corrompues d''un costume antique, son front d''os est ceint d''une haute couronne de fer, et sa main d''ivoire jauni tient une baguette d''épine au rubis sanglant, quand il siège en sa cour de nuit éternelle parmi ceux qui sont morts. S''il fut jadis un homme de grand renom et sagesse ancienne, son nom s''est perdu dans les ténèbres des temps obscurs, et seul demeure dans les rêves et les contes la crainte de la folie qui brûle toujours férocement dans ses orbites mortes, creusées par les siècles passés à accroître sa puissance, lançant ses légions en putréfaction contre les hordes de son ennemi ancestral, le Seigneur des Sept Enfers, auquel il voue une haine par-delà les siècles.'),
(261, 161, 'Zombi Démoniaque', 'M', '', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 0, 100, 3, 'plain', 99, 1, 140, 180, 'Cette créature est le résultat d''une lutte entre un démon et un roi liche. Ce dernier ayant finalement réussi à emprisonner le démon dans un corps de zombi, en créant ainsi un serviteur personnel particulièrement puissant.'),
(262, 162, 'Géant d''Outre-Tombe', 'M', '', 'No', 'chaotique,mauvais', 'morts-vivant', 'walking', 0, 100, 3, 'plain', 99, 1, 140, 180, 'Pourquoi un nécromancien ne ranimerait-il que des êtres normaux ? Dans ce cas précis il s''est essayé avec succès sur un géant quelconque. Les aventuriers ont tendance à sous-estimer l''intelligence de ce mort-vivant.'),
(236, 136, 'Leithan', 'M', '', 'No', 'neutre,bon', 'divinité', 'walking', 0, 100, 3, 'plain', 99, 1, 150, 150, 'Dans les déserts neigeux hantés par les blizzards, se dresse au milieu d’un lac aux eaux translucides un croc de glace finement ciselé, somptueux autant que menaçant parmi les flots parsemés de banquises mouvantes. En ce palais transi d’un silence immuable réside Leithan en maîtresse divine du froid et des eaux, aussi belle et scintillante que le cristal le plus pur, aussi diaphane que l’onde la plus claire. C’est elle qui accompagne d’un dernier sourire le malheureux marin qui se noie, elle aussi qui enveloppe d’un dernier souffle glacial l’aventurier perdu dans les immensités nordiques, mais elle encore qui envoie les pluies fines des derniers frimas qui assureront la récolte abondante des terres du Sud, Antique déesse adorée depuis les premiers temps de l’Ere des Dieux, fille de l’Ether Aeda’ll et de l’Eau Ekaero, Leithan tient une place de premier ordre dans le panthéon des races Nacridanes. \r\n\r\nRares sont ceux qui ont pu contempler son fin visage en dehors du fugace instant de leur trépas, et nul autre qu’un dieu ne pourrait attaquer cette vivante statue de givre opalin. Pourtant, de nombreux téméraires, aguerris parmi les plus aguerris, ont été attirés par la foi ou par sa légendaire beauté et ont tenté de trouver sa demeure glacée. Multiples sont ceux qui ont ainsi disparu dans les Crocs du Monde ou dans les dangereuses mers des Contrées Nordiques à la recherche de sa demeure. Seul Firelabar, dit la légende, ce héros des anciens temps au courage et à la vision si puissante, a pu pénétrer dans son étincelant manoir de glace et contempler en face l’ondine déesse. Mais même lui, si prompt à la fanfaronnade, a toujours tu le contenu de leur entretien et l’emplacement de sa résidence.\r\n\r\n[i]« ”Contempler le sourire de Leithan” Locution proverbiale en usage dans de nombreuses contrées et spécialement répandue dans les Grèves Lointaines et à la Baie des Phoques : périr noyé et, par extension, mourir dans d’atroces souffrances en espérant un trépas le plus miséricordieux possible. »[/i]'),
(246, 146, 'Bébé Wyverne', 'M', '', 'No', 'neutre', 'créature draconique', 'walking', 2, 100, 3, 'plain', 99, 1, 70, 150, 'Vous avez déjà vu une Grand Mère Wyverne ? Un bébé, c''est pareil, sauf qu''ils sont beaucoup plus petits. Toutefois, ces bestioles sont très agressives, et ce, dès la naissance. Il est vivement déconseillé de s''attarder à leur gratouiller le menton. Plus d''un elfe nouvellement arrivé en terre nacridanienne a connu une mort atrocement douloureuse pour avoir commis cette bévue. '),
(247, 147, 'Succube', 'F', 'succube.jpg', 'No', 'neutre,mauvais', 'démon', 'walking', 0, 100, 3, 'plain', 99, 1, 50, 150, 'La succube est un démon perfide à forme féminine dont la principale occupation consiste à séduire les hommes pour les anéantir. Elle a des ailes dans le dos qui lui permettent de voler. Elle possède une arme éthérée qui transmet son pouvoir d''absorption d''énergie. '),
(253, 253, 'Kradjeck ferreux', 'M', 'kradjeck.png', 'No', 'Loyal, Neutre', 'Extra-planaire', 'walking', 0, 100, 0, 'plain', 100, 253, 200, 200, 'Le Kradjeck est une créature robuste et massive, pourvue de quatre pseudopodes moteurs, et d''une solide carapace, d''où seuls des yeux folliculés dépassent à intervalles irréguliers. Apparemment constituée de fer, cette carapace permet de donner l''âge du Kradjeck ; en effet, plus celui-ci est vieux, plus sa carapace est rouillée, martelée par les marques d''une vie inconnue. On connaît mal ces créatures pacifiques et intelligentes, parlant un langage compréhensible des habitants de Nacridan, aux motivations inconnues. Sont-elles des monstres, comme en témoigne leur apparition par les portails ? Une réponse à l''absence de minerai de fer sur l''Ile ? Seuls le temps et l''observation pourront peut-être apporter une réponse à ce grand mystère…\r\n\r\n[i]« Stail Yroun Sthâll ub Ridick ?[/i]\r\n[i]–… hein ?[/i]\r\n[i]– Stail Yrou…[/i]\r\n[i]–… oh et pis meRde ! S''exclama PetRa en incinéRant le KRadjeck pour lui pRendre son feRr. La cRéatuRe agonisante laissa échappeR un meuglement d''incompRéhension. En même temps, songea l''aventuRièRe, si on n’est pas capable de paRleR une langue coRRecte, hein… »[/i] Belles Rencontres sur l''Ile de NacRidan, par Jorel Tapeau.'),
(254, 154, 'Géant des Glaces', 'M', '', 'No', 'neutre,chaotique', 'humanoïde', 'walking', 0, 100, 3, 'plain', 99, 1, 120, 250, 'On rencontre ce grand humanoïde dans les terres éternellement gelées du Nord. Vêtu d''un simple pagne de fourrure, il semble insensible au froid et les coups qu''il assène du haut de ses quatre mètres sont aussi vifs que puissants.'),
(255, 155, 'Dragon Neigeux', 'M', '', 'No', 'chaotique,mauvais', 'élémentaire', 'walking', 0, 100, 3, 'plain', 99, 1, 120, 250, 'Comme le dragon de vent, cette créature n''est pas exactement un dragon. C''est une entité élémentaire constituée de neige et de glace ayant la forme d''un dragon sans aile. Ceux qui résistent à son souffle glacé se font déchiqueter par ses griffes de glace.'),
(257, 157, 'Seigneur des Sept Enfers', 'M', '', 'No', 'chaotique,mauvais', 'démon', 'walking', 0, 100, 3, 'plain', 99, 1, 180, 250, 'Cette haute figure marmoréenne est assise sur un trône de marbre froid au pied duquel se tordent pour l''éternité les âmes perdues sur lesquelles il règne. Son  visage de terreur disparaît dans l''ombre de dimensions inconnues des mortels, alors que ses yeux de feu brillent d''une lueur sombre et maléfique comme celle d''un rubis sanglant, et que sa voix profère des malédictions perdues, tirant la substance de la trame même de l''enfer pour maintenir le grand portail-racine du Chaos dans le Monde. \r\n\r\nLe Seigneur des Sept Enfers est le premier et le plus puissant des démons qui hantent encore la surface de Nacridan. Terrible est sa faim, colossale sa force et sans limites ses connaissances impies. Ses yeux de braise brûlent de la fureur de celui qui se veut maître et possesseur du monde, et il n''est pourtant maître que d''une portion infime du territoire de la nuit écarlate, lieu de folies et de mort brutale. Contrairement aux croyances de nombreux aventuriers peu éduqués comme Jorel Tapeau, le Seigneur des Sept Enfers n''est pas un fou furieux démoniaque : le poste qu''il occupe, et qui est une très mauvaise traduction de Czéznarzsc Impérial, est l''aboutissement d''un long cursus déshonorant au sein de la société des Enfers. \r\n\r\n[i]«Fuyez ! Pauvres Fous !!»[/i] Derniers Conseils, de Illuminati'),
(259, 159, 'Tigre Blanc', 'M', '', 'No', 'chaotique,neutre', 'mammifère', 'walking', 0, 100, 3, 'plain', 99, 1, 120, 250, 'Ce gros félin de deux mètres au garrot hante les terres désolées des confins du monde, au cœur des contrées nordiques. Son épaisse fourrure le garantit du froid, et ses muscles puissants lui permettent de fuir les ennemis trop redoutables comme de terrasser de nombreuses proies. Malheureusement pour l''animal, sa magnifique peau est particulièrement recherchée. On dit que la fameuse Impératrice d''Artasse n''acceptait de dormir que sur les peaux de cet animal, dont elle avait un stock prodigieux. C''est d''ailleurs étroitement enroulée dans l''une de ces dernières qu''elle fut jetée vivante dans son bûcher mortuaire lorsque l''empereur revient prendre son trône. En sale égoïste qu''il est, le tigre blanc refuse en général de se laisser dépouiller de sa fourrure au profit de riches bourgeoises de la Fédération et se défendra jusqu''à la mort, que ce soit la sienne ou celle de ses bourreaux.'),
(226, 126, 'Gargouille majeure', 'F', '', 'No', 'chaotique,mauvais', 'chimère', 'walking', 0, 100, 3, 'plain', 99, 1, 70, 70, 'Une gargouille a une peau grise qui fait qu''on peut facilement la prendre pour une statue.  \r\nElle a une tête hideuse ornée d''une corne, des membres griffus, deux ailes dans le dos et une longue queue. Celle-ci est énorme et beaucoup plus puissante que la moyenne. '),
(263, 263, 'Tortue Géante', 'F', '', 'No', '', '', 'walking', 0, 100, 0, 'plain', 0, 0, 0, 0, 'Cet énorme animal placide est la principale raison du triomphe du mercantilisme en Nacridan. Ne cessant jamais de grandir et pouvant vivre des siècles si elle est bien nourrie, la Tortue Géante de Néphy, endémique des Marais du Désespoir, est élevée avec soin selon des procédés anciens et quasiment oubliés par les quelques familles de concessionnaires en Tortues qui fournissent la Guilde des Caravanes. Elle est prisée pour sa capacité à transporter des cargaisons faramineuses dans l''espace creux à l''intérieur de sa carapace gigantesque. De nombreux marchands utilisent avec profit les Tortues Géantes sur les routes pour transporter de volumineuses cargaisons, même si la relative rareté de ces créatures justifie le prix fort dispendieux que les éleveurs de Néphy se plaisent à faire payer à ceux qui désirent en acquérir une. Néanmoins, c''est un compagnon fidèle et aussi affectueux que peut l''être une grosse tortue. Tout terrain, puisque ni les marais ni les cours d''eau ne l''arrêtent, elle protège des intempéries comme des insectes et autres inconvénients du voyage ceux qui la chevauchent, mais n''attendez quand même pas d''elle des pointes de vitesse. C''est une tortue : elle peut porter beaucoup, certes, mais lentement. Il est en outre indispensable de la nourrir très régulièrement, aux nombreux relais [i]Totesso ou Nacrid[/i] - [i]Batavia[/i], qui vendent à prix scandaleux les précieuses laitues que consomment les Tortues Géantes, pour le plus grand bonheur de la République de Tonak, qui contrôle les vastes champs laituifères du Proche-Est et n''hésite jamais à faire grimper les prix dès que Djinn menace d''attaquer.\r\n\r\n«…[i] C''est pourquoi j''ai l''immense honneur, messeigneurs, de vous présenter notre nouvelle race de tortue, la Titine, qui ne consomme, tenez - vous bien que 10L/100, c''est - à - dire, eh oui, à peine 10 laitues aux 100 lieues ! Non polluantes, et donc parfaitement écologiques, nos Tortues next - gen, comme on dit en elfique…[/i] » Discours de Kharlos Geauchsn, Directeur de Peunault&amp;Cie-Troêne, au 22ê Salon annuel de la Tortue de Néphy. ');

-- --------------------------------------------------------

--
-- Structure de la table `BasicTalent`
--

CREATE TABLE IF NOT EXISTS `BasicTalent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `ident` varchar(20) NOT NULL,
  `PA` smallint(7) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `BasicTalent`
--

INSERT INTO `BasicTalent` (`id`, `name`, `ident`, `PA`, `type`) VALUES
(1, 'Miner', 'TALENT_MINE', 8, 1),
(2, 'Dépecer', 'TALENT_DISMEMBER', 4, 1),
(3, 'Teiller', 'TALENT_SCUTCH', 8, 1),
(4, 'Récolter', 'TALENT_HARVEST', 5, 1),
(5, 'Dialecte Kradjeckien', 'TALENT_SPEAK', 7, 1),
(6, 'Couper du bois', 'TALENT_CUT', 5, 1),
(7, 'Artisanat du fer', 'TALENT_IRONCRAFT', 8, 3),
(8, 'Artisanat du bois', 'TALENT_WOODCRAFT', 8, 3),
(9, 'Artisanat du cuir', 'TALENT_LEATHERCRAFT', 8, 3),
(10, 'Artisanat des écailles', 'TALENT_SCALECRAFT', 8, 3),
(11, 'Artisanat du lin', 'TALENT_LINENCRAFT', 8, 3),
(12, 'Artisanat des plantes', 'TALENT_PLANTCRAFT', 8, 3),
(13, 'Artisanat des gemmes', 'TALENT_GEMCRAFT', 8, 3),
(14, 'Ferronner', 'TALENT_IRONREFINE', 5, 2),
(15, 'Tanner', 'TALENT_LEATHERREFINE', 5, 2),
(16, 'Ecailler', 'TALENT_SCALEREFINE', 5, 2),
(17, 'Effiler', 'TALENT_LINENREFINE', 5, 2),
(18, 'Tailler', 'TALENT_GEMREFINE', 5, 2),
(19, 'Filtrer', 'TALENT_PLANTREFINE', 5, 2),
(20, 'Charpenterie', 'TALENT_WOODREFINE', 5, 2),
(21, 'Appel du Kradjeck', 'TALENT_KRADJECKCALL', 2, 4),
(22, 'Détection des ressources', 'TALENT_DETECTION', 2, 4),
(23, 'Connaissance des monstres', 'TALENT_MONSTER', 2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `BasicTemplate`
--

CREATE TABLE IF NOT EXISTS `BasicTemplate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Modifier_BasicTemplate` bigint(20) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  `frequency` tinyint(4) NOT NULL DEFAULT '0',
  `numset` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `name2` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `BasicTemplate`
--

INSERT INTO `BasicTemplate` (`id`, `id_Modifier_BasicTemplate`, `rank`, `frequency`, `numset`, `name`, `name2`) VALUES
(1, 1, 50, 8, 0, 'du Kraken', 'Colossal(e)'),
(2, 2, 70, 3, 1, 'du Griffon', 'Flamboyant(e)'),
(3, 3, 80, 3, 2, 'de la Manticore', 'Foudroyant(e)'),
(4, 4, 60, 8, 0, 'du Phénix', 'Ethéré(e)'),
(5, 5, 90, 3, 3, 'du Dragon', 'Eternel(le)');

-- --------------------------------------------------------

--
-- Structure de la table `BuildingAction`
--

CREATE TABLE IF NOT EXISTS `BuildingAction` (
  `id` bigint(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `ident` varchar(30) NOT NULL,
  `price` bigint(15) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT '0',
  `ap` int(7) NOT NULL,
  `id_BasicBuilding` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `BuildingAction`
--

INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`) VALUES
(401, 'Téléportation', 'TELEPORT', 0, 5, 12, 14),
(402, 'Soin', 'TEMPLE_HEAL', 4, 1, 2, 14),
(403, 'Bénédiction', 'TEMPLE_BLESSING', 9, 3, 2, 14),
(404, 'Fixation de l''âme', 'TEMPLE_RESURRECT', 15, 5, 0, 14),
(405, 'Dormir une nuit', 'BEDROOM_SLEEPING', 4, 1, 12, 1),
(406, 'Boire un verre', 'HOSTEL_DRINK', 0, 1, 1, 1),
(408, 'Payer une tournée', 'HOSTEL_ROUND', 5, 5, 5, 1),
(409, 'Poster une annonce', 'HOSTEL_NEWS', 2, 3, 3, 1),
(413, 'Déposer de l''argent', 'BANK_DEPOSIT', 9, 1, 0, 4),
(414, 'Retirer de l''argent', 'BANK_WITHDRAW', 0, 0, 0, 4),
(415, 'Transfert d''argent', 'BANK_TRANSFERT', 9, 1, 0, 4),
(416, 'Achat echoppe basique', 'BASIC_SHOP_BUY', 90, 10, 0, 5),
(418, 'Achat échoppe principale', 'MAIN_SHOP_BUY', 90, 10, 0, 5),
(419, 'Commander équipment', 'GUILD_ORDER_EQUIP', 70, 30, 0, 9),
(421, 'Commander enchantement', 'GUILD_ORDER_ENCHANT', 70, 30, 0, 9),
(425, 'Réparation', 'SHOP_REPAIR', 70, 30, 0, 5),
(427, 'Envoi colis', 'SEND_PACKAGE', 2, 3, 0, 4),
(450, 'Sortilège élémentaire', 'SCHOOL_LEARN_SPELL_0', 50, 30, 12, 7),
(451, 'Sortillège aspirant', 'SCHOOL_LEARN_SPELL_1', 400, 100, 12, 7),
(452, 'Sortilège adepte', 'SCHOOL_LEARN_SPELL_2', 1000, 500, 12, 7),
(453, 'Compétence élémentaire', 'SCHOOL_LEARN_ABILITY_0', 50, 30, 12, 6),
(454, 'Compétence aspirant', 'SCHOOL_LEARN_ABILITY_1', 400, 100, 12, 6),
(455, 'Compétence adepte', 'SCHOOL_LEARN_ABILITY_2', 1000, 500, 12, 6),
(457, 'Savoir-faire raffinage', 'SCHOOL_LEARN_TALENT_1', 30, 20, 12, 8),
(458, 'Savoir-faire artisanat', 'SCHOOL_LEARN_TALENT_2', 60, 40, 12, 8),
(459, 'Savoir faire nature', 'SCHOOL_LEARN_TALENT_3', 10, 5, 10, 8),
(456, 'Talent extraction', 'SCHOOL_LEARN_TALENT_0', 15, 10, 12, 8),
(441, 'Message commercial', 'HOSTEL_TRADE', 5, 5, 2, 1);

-- --------------------------------------------------------
-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Sam 01 Juin 2013 à 12:28
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `EquipmentType`
--

CREATE TABLE IF NOT EXISTS `EquipmentType` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `wearable` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `small` enum('Yes','No') NOT NULL DEFAULT 'No',
  `mask` bigint(20) NOT NULL DEFAULT '0',
  `zone` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Contenu de la table `EquipmentType`
--

INSERT INTO `EquipmentType` (`id`, `name`, `wearable`, `small`, `mask`, `zone`) VALUES
(1, 'ligth_weapon', 'Yes', 'No', 1, 1),
(2, 'intermediate_weapon', 'Yes', 'No', 1, 1),
(3, 'heavy_weapon', 'Yes', 'No', 1, 1),
(4, 'arm_two_hand', 'Yes', 'No', 3, 2),
(5, 'short bow', 'Yes', 'No', 3, 2),
(6, 'magical_one_hand_arm', 'Yes', 'No', 1, 1),
(7, 'magical_two_hand_arm', 'Yes', 'No', 3, 2),
(8, 'ligth_shield', 'Yes', 'No', 2, 1),
(9, 'intermediate_shield', 'Yes', 'No', 2, 1),
(10, 'heavy_shield', 'Yes', 'No', 2, 1),
(11, 'cuirass', 'Yes', 'No', 4, 1),
(12, 'helmet', 'Yes', 'No', 32, 1),
(13, 'boots', 'Yes', 'No', 64, 2),
(14, 'cuisse', 'Yes', 'No', 512, 1),
(15, 'gauntlet_right', 'Yes', 'No', 4096, 1),
(16, 'gauntlet_left', 'Yes', 'No', 8192, 1),
(17, 'spaulder_right', 'Yes', 'No', 8, 1),
(18, 'spaulder_left', 'Yes', 'No', 16, 1),
(19, 'talisman', 'Yes', 'No', 128, 1),
(20, 'belt', 'Yes', 'No', 256, 1),
(21, 'claw', 'Yes', 'No', 2, 1),
(22, 'long bow', 'Yes', 'No', 3, 2),
(28, 'arrow', 'No', 'No', 0, 0),
(29, 'potion', 'No', 'No', 0, 0),
(30, 'gem', 'No', 'No', 0, 0),
(31, 'raw_materials', 'No', 'No', 0, 0),
(32, 'herbs', 'No', 'No', 0, 0),
(40, 'tools', 'Yes', 'No', 1, 1),
(41, 'bag of gems', 'Yes', 'No', 16384, 1),
(42, 'bag of materials', 'Yes', 'No', 32768, 1),
(43, 'bag of herbs', 'Yes', 'No', 65536, 1),
(44, 'quiver', 'Yes', 'No', 131072, 1),
(45, 'scroll', 'No', 'No', 0, 0),
(33, 'factice', 'No', 'No', 0, 0);


-- --------------------------------------------------------

--
-- Structure de la table `Modifier_BasicEquipment`
--

CREATE TABLE IF NOT EXISTS `Modifier_BasicEquipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hp` varchar(32) NOT NULL DEFAULT '////////',
  `hp_bm` varchar(32) NOT NULL DEFAULT '////////',
  `strength` varchar(32) NOT NULL DEFAULT '////////',
  `strength_bm` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity_bm` varchar(32) NOT NULL DEFAULT '////////',
  `speed` varchar(32) NOT NULL DEFAULT '////////',
  `speed_bm` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill_bm` varchar(32) NOT NULL DEFAULT '////////',
  `attack` varchar(32) NOT NULL DEFAULT '////////',
  `attack_bm` varchar(32) NOT NULL DEFAULT '////////',
  `defense` varchar(32) NOT NULL DEFAULT '////////',
  `defense_bm` varchar(32) NOT NULL DEFAULT '////////',
  `damage` varchar(32) NOT NULL DEFAULT '////////',
  `damage_bm` varchar(32) NOT NULL DEFAULT '////////',
  `armor` varchar(32) NOT NULL DEFAULT '////////',
  `armor_bm` varchar(32) NOT NULL DEFAULT '////////',
  `timeAttack` varchar(32) NOT NULL DEFAULT '////////',
  `timeAttack_bm` varchar(32) NOT NULL DEFAULT '////////',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=1000 ;

--
-- Contenu de la table `Modifier_BasicEquipment`
--

INSERT INTO `Modifier_BasicEquipment` (`id`, `hp`, `hp_bm`, `strength`, `strength_bm`, `dexterity`, `dexterity_bm`, `speed`, `speed_bm`, `magicSkill`, `magicSkill_bm`, `attack`, `attack_bm`, `defense`, `defense_bm`, `damage`, `damage_bm`, `armor`, `armor_bm`, `timeAttack`, `timeAttack_bm`) VALUES
(1, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////'),
(2, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////'),
(3, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////'),
(4, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////'),
(5, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////3/', '////////', '////////', '////////', '////////', '////////'),
(6, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////3/', '////////', '////////', '////////', '////////', '////////'),
(7, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////4/', '////////', '////////', '////////', '////////', '////////'),
(8, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '///////3/', '////////', '////////', '////////', '////////', '////////'),
(9, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////4/', '////////', '////////', '////////', '////////', '////////'),
(10, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////5/', '////////', '////////', '////////', '////////', '////////'),
(11, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////3/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(12, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////5/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(13, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(14, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////'),
(15, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////'),
(16, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(17, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////'),
(18, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////3/', '////////', '////////', '////////'),
(19, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(20, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(21, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////'),
(22, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(23, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(24, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(25, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(26, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(27, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(28, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(29, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(30, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(31, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(32, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(33, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(62, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(35, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(36, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(37, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(38, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(39, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////0.5/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(40, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////0.5/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(41, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////0.5/', '////////', '////////', '////////'),
(42, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////0.5/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(47, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(999, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(55, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(56, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////'),
(400, '/2///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(48, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(49, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////'),
(50, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(401, '////////', '////////', '/2///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(402, '////////', '////////', '////////', '////////', '/2///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(403, '////////', '////////', '////////', '////////', '////////', '////////', '/2///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(404, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '/2///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(51, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////', '///////2/', '////////', '////////', '////////', '////////', '////////'),
(34, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(52, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(205, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////');

-- --------------------------------------------------------

--
-- Structure de la table `Modifier_BasicTemplate`
--

CREATE TABLE IF NOT EXISTS `Modifier_BasicTemplate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hp` varchar(32) NOT NULL DEFAULT '////////',
  `hp_bm` varchar(32) NOT NULL DEFAULT '////////',
  `strength` varchar(32) NOT NULL DEFAULT '////////',
  `strength_bm` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity_bm` varchar(32) NOT NULL DEFAULT '////////',
  `speed` varchar(32) NOT NULL DEFAULT '////////',
  `speed_bm` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill_bm` varchar(32) NOT NULL DEFAULT '////////',
  `armor` varchar(32) NOT NULL DEFAULT '////////',
  `armor_bm` varchar(32) NOT NULL DEFAULT '////////',
  `attack` varchar(32) NOT NULL DEFAULT '////////',
  `attack_bm` varchar(32) NOT NULL DEFAULT '////////',
  `defense` varchar(32) NOT NULL DEFAULT '////////',
  `defense_bm` varchar(32) NOT NULL DEFAULT '////////',
  `damage` varchar(32) NOT NULL DEFAULT '////////',
  `damage_bm` varchar(32) NOT NULL,
  `timeAttack` varchar(32) NOT NULL DEFAULT '////////',
  `timeAttack_bm` varchar(32) NOT NULL DEFAULT '////////',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `Modifier_BasicTemplate`
--

INSERT INTO `Modifier_BasicTemplate` (`id`, `hp`, `hp_bm`, `strength`, `strength_bm`, `dexterity`, `dexterity_bm`, `speed`, `speed_bm`, `magicSkill`, `magicSkill_bm`, `armor`, `armor_bm`, `attack`, `attack_bm`, `defense`, `defense_bm`, `damage`, `damage_bm`, `timeAttack`, `timeAttack_bm`) VALUES
(1, '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '', '////////', '////////'),
(2, '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '', '////////', '////////'),
(3, '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '', '////////', '////////'),
(4, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '', '////////', '////////'),
(5, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////1/', '////////', '////////', '////////', '////////', '////////', '////////', '', '////////', '////////');

-- --------------------------------------------------------

--
-- Structure de la table `StaticModifier_BM`
--

CREATE TABLE IF NOT EXISTS `StaticModifier_BM` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hp` varchar(32) NOT NULL DEFAULT '////////',
  `strength` varchar(32) NOT NULL DEFAULT '////////',
  `dexterity` varchar(32) NOT NULL,
  `speed` varchar(32) NOT NULL,
  `magicSkill` varchar(32) NOT NULL DEFAULT '////////',
  `attack` varchar(32) NOT NULL DEFAULT '////////',
  `defense` varchar(32) NOT NULL DEFAULT '////////',
  `damage` varchar(32) NOT NULL DEFAULT '////////',
  `armor` varchar(32) NOT NULL DEFAULT '////////',
  `timeAttack` varchar(32) NOT NULL DEFAULT '////////',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `StaticModifier_BM`
--

INSERT INTO `StaticModifier_BM` (`id`, `hp`, `strength`, `dexterity`, `speed`, `magicSkill`, `attack`, `defense`, `damage`, `armor`, `timeAttack`) VALUES
(1, '////////', '////////', '////////', '/-0.5///////', '////////', '////////', '////////', '////////', '////////', '////////'),
(2, '///////-10/', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(3, '////////', '////////', '////////', '////////', '/-0.5///////', '////////', '////////', '////////', '////////', '////////'),
(4, '////////', '////////', '////////', '////////', '////////', '/0.5///////', '////////', '////////', '////////', '////////'),
(5, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '/0.5///////', '////////', '////////'),
(6, '////////', '////////', '/-0.5///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(7, '////////', '/-0.5///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(8, '////////', '////////', '////////', '/-0.5///////', '////////', '////////', '////////', '////////', '////////', '////////'),
(9, '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '///////0.8/', '////////'),
(10, '////////', '////////', '////////', '////////', '////////', '////////', '/0.5///////', '////////', '////////', '////////'),
(11, '////////', '////////', '////////', '////////', '////////', '////////', '////////1', '////////', '////////', '////////'),
(12, '////////', '////////', '////////', '////////', '////////', '////////13', '////////', '////////', '////////', '////////'),
(13, '////////', '/0.5///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(14, '////////', '////////', '/0.5///////', '////////', '////////', '////////', '////////', '////////', '////////', '////////'),
(15, '////////', '////////', '////////', '/0.5///////', '////////', '////////', '////////', '////////', '////////', '////////'),
(16, '////////', '////////', '////////', '////////', '/0.5///////', '////////', '////////', '////////', '////////', '////////'),
(17, '////////', '////////', '////////', '////////', '////////-10', '////////', '////////', '////////', '////////', '////////'),
(18, '////////', '////////', '////////', '////////-50', '////////-50', '////////', '////////', '////////', '////////', '////////');
