﻿-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 08 Août 2011 à 23:02
-- Version du serveur: 5.1.30
-- Version de PHP: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `nacridanv2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Basicmaterial`
--
DROP TABLE IF EXISTS `BasicMaterial`;
CREATE TABLE IF NOT EXISTS `BasicMaterial` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT 'Fer',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `BasicMaterial`
--

INSERT INTO `BasicMaterial` (`id`, `name`) VALUES
(1, 'iron'),
(2, 'wood'),
(3, 'scale'),
(4, 'leather'),
(5, 'linen'),
(6, 'root'),
(7, 'seed'),
(8, 'leaf'),
(9, 'emerald'),
(10, 'ruby');
