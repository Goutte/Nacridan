-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 09 Octobre 2013 à 12:47
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicEquipment`
--

CREATE TABLE IF NOT EXISTS `BasicEquipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Modifier_BasicEquipment` bigint(20) NOT NULL DEFAULT '0',
  `id_EquipmentType` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicMaterial` smallint(7) NOT NULL DEFAULT '1',
  `name` varchar(32) NOT NULL DEFAULT '',
  `frequency` tinyint(4) NOT NULL DEFAULT '0',
  `durability` smallint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_listTypeEquipement` (`id_EquipmentType`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=601 ;

--
-- Contenu de la table `BasicEquipment`
--

INSERT INTO `BasicEquipment` (`id`, `id_Modifier_BasicEquipment`, `id_EquipmentType`, `id_BasicMaterial`, `name`, `frequency`, `durability`) VALUES
(1, 1, 1, 1, 'Dague', 48, 60),
(2, 2, 1, 1, 'Epée Courte', 48, 60),
(3, 3, 2, 1, 'Lance', 48, 60),
(4, 4, 2, 1, 'Epée longue', 48, 60),
(5, 5, 2, 1, 'Hache de bataille', 48, 60),
(6, 6, 3, 1, 'Fléau d''armes', 48, 60),
(7, 7, 3, 1, 'Masse d''armes', 48, 60),
(8, 8, 4, 1, 'Epée à deux mains', 56, 60),
(9, 9, 4, 1, 'Hallebarde', 56, 60),
(10, 10, 4, 1, 'Marteau de guerre', 56, 60),
(11, 11, 6, 9, 'Sceptre', 48, 60),
(12, 12, 7, 9, 'Bâton magique', 56, 60),
(13, 13, 5, 2, 'Arc court', 48, 60),
(505, 999, 33, 11, 'Poudre de Mort-vivant', 20, 0),
(15, 15, 22, 2, 'Arc long', 48, 60),
(16, 16, 8, 3, 'Petit bouclier', 56, 60),
(17, 17, 9, 2, 'Bouclier moyen', 56, 60),
(18, 18, 10, 1, 'Bouclier large', 56, 60),
(19, 19, 11, 4, 'Cuirasse de cuir', 48, 60),
(20, 20, 11, 3, 'Cuirasse d''écailles', 48, 60),
(21, 21, 11, 1, 'Cuirasse de plate', 48, 60),
(22, 22, 11, 5, 'Toge magique', 48, 60),
(23, 23, 12, 4, 'Casque de cuir', 40, 60),
(24, 24, 12, 3, 'Capuchon d''écailles', 40, 60),
(25, 25, 12, 1, 'Heaume de plate', 40, 60),
(26, 26, 12, 5, 'Diadème magique', 40, 60),
(27, 27, 13, 4, 'Bottes de cuir', 32, 60),
(28, 28, 13, 3, 'Bottes d''écailles', 32, 60),
(29, 29, 13, 1, 'Bottes de plate', 32, 60),
(30, 30, 13, 5, 'Bottes magiques', 32, 60),
(31, 31, 14, 4, 'Cuissarde de cuir', 40, 60),
(32, 32, 14, 3, 'Cuissarde d''écailles', 40, 60),
(33, 33, 14, 1, 'Cuissarde de plate', 40, 60),
(34, 34, 14, 5, 'Cuissarde magique', 40, 60),
(35, 35, 15, 4, 'Gant droit de cuir', 32, 60),
(36, 36, 15, 3, 'Gant droit d''écailles', 32, 60),
(37, 37, 15, 1, 'Gant droit de maille', 32, 60),
(38, 38, 15, 5, 'Gant droit magique', 32, 60),
(39, 39, 17, 4, 'Epaulière droite de cuir', 24, 60),
(40, 40, 17, 3, 'Epaulière droite d''écailles', 24, 60),
(41, 41, 17, 1, 'Epaulière droite de plate', 24, 60),
(42, 42, 17, 5, 'Epaulière droite magique', 24, 60),
(43, 39, 18, 4, 'Epaulière gauche de cuir', 24, 60),
(44, 40, 18, 3, 'Epaulière gauche d''écailles', 24, 60),
(45, 41, 18, 1, 'Epaulière gauche de plate', 24, 60),
(46, 42, 18, 5, 'Epaulière gauche magique', 24, 60),
(47, 47, 20, 4, 'Ceinture de cuir', 40, 60),
(48, 48, 20, 3, 'Ceinture d''écailles', 40, 60),
(49, 49, 20, 1, 'Ceinture de plate', 40, 60),
(50, 50, 20, 5, 'Ceinture magique', 40, 60),
(55, 55, 28, 2, 'Flèche torsadée', 2, 0),
(56, 56, 28, 2, 'Flèche barbelée', 2, 0),
(100, 999, 30, 9, 'Emeraude', 20, 0),
(101, 999, 30, 10, 'Rubis', 40, 0),
(506, 999, 33, 11, 'Ailes de fée', 20, 0),
(103, 999, 31, 4, 'Cuir', 8, 0),
(104, 999, 31, 3, 'Ecailles', 8, 0),
(105, 999, 31, 5, 'Lin', 8, 0),
(106, 999, 31, 1, 'Fer', 8, 0),
(107, 999, 32, 6, 'Racine de Razhot', 4, 0),
(108, 999, 32, 7, 'Graine de garnach', 4, 0),
(109, 999, 32, 8, 'Feuille de Folianne', 4, 0),
(110, 999, 31, 2, 'Bois', 4, 0),
(200, 999, 40, 1, 'Pioche de mineur', 16, 10),
(201, 999, 40, 1, 'Pince à dépecer', 12, 15),
(202, 999, 40, 2, 'Teilleuse', 12, 15),
(203, 999, 40, 2, 'Pressoir', 8, 20),
(204, 999, 40, 9, 'Gemme de conviction', 12, 15),
(205, 205, 1, 1, 'Bolas', 40, 10),
(206, 999, 40, 1, 'Cauchoir de bucheron', 8, 20),
(207, 999, 40, 1, 'Tenailles d''artisan', 20, 10),
(208, 999, 40, 4, 'Alène d''artisan', 20, 10),
(209, 999, 40, 3, 'Plioir d''artisan', 20, 10),
(210, 999, 40, 5, 'Ciseaux d''artisan', 20, 10),
(211, 999, 40, 6, 'Alambic d''artisan', 20, 10),
(212, 999, 40, 2, 'Râpes d''artisan', 20, 10),
(213, 999, 40, 1, 'Marteau de ferronier', 20, 20),
(214, 999, 40, 1, 'Griffe à lacer de tanneur', 20, 20),
(215, 999, 40, 1, 'Abat-care d''écailleur', 20, 20),
(216, 999, 40, 1, 'Roue à molette d''effileur', 20, 20),
(217, 999, 40, 1, 'Filtre d''alchimiste', 20, 20),
(218, 999, 40, 1, 'Chignole de charpentier', 20, 20),
(219, 999, 40, 1, 'Burin de tailleur', 40, 60),
(300, 999, 41, 4, 'Sac de gemmes', 12, 60),
(301, 999, 42, 4, 'Sac de matières premières', 12, 60),
(302, 999, 43, 4, 'Sac d''herbes', 12, 60),
(400, 400, 29, 6, 'Potion de vie', 8, 0),
(303, 999, 44, 2, 'Carquois', 16, 60),
(401, 401, 29, 7, 'Potion de force', 8, 0),
(402, 402, 29, 7, 'Potion de dextérité', 8, 0),
(403, 403, 29, 8, 'Potion de vitesse', 8, 0),
(404, 404, 29, 8, 'Potion de magie', 8, 0),
(500, 999, 33, 11, 'Moustache de rat', 6, 0),
(501, 999, 33, 11, 'Crocs de chauve-souris', 6, 0),
(502, 999, 33, 11, 'Langue de crapaud', 6, 0),
(503, 999, 33, 11, 'Antenne de fourmi reine', 10, 0),
(504, 999, 33, 11, 'Yeux d''araignée éclipsante', 8, 0),
(600, 999, 45, 11, 'Parchemin', 0, 0),
(51, 51, 3, 1, 'Masse Sacrée', 48, 60),
(220, 999, 40, 9, 'Cristal d''enchantement', 40, 60),
(52, 52, 21, 1, 'Griffe', 24, 60),
(507, 999, 33, 11, 'Branche d''Etranglesaule', 15, 0),
(57, 57, 7, 9, 'Bâton d''Aether', 56, 60),
(58, 58, 7, 9, 'Bâton d''Athlan', 56, 60);
