-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Sam 01 Juin 2013 à 12:28
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `EquipmentType`
--

CREATE TABLE IF NOT EXISTS `EquipmentType` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `wearable` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `small` enum('Yes','No') NOT NULL DEFAULT 'No',
  `mask` bigint(20) NOT NULL DEFAULT '0',
  `zone` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Contenu de la table `EquipmentType`
--

INSERT INTO `EquipmentType` (`id`, `name`, `wearable`, `small`, `mask`, `zone`) VALUES
(1, 'ligth_weapon', 'Yes', 'No', 1, 1),
(2, 'intermediate_weapon', 'Yes', 'No', 1, 1),
(3, 'heavy_weapon', 'Yes', 'No', 1, 1),
(4, 'arm_two_hand', 'Yes', 'No', 3, 2),
(5, 'short bow', 'Yes', 'No', 3, 2),
(6, 'magical_one_hand_arm', 'Yes', 'No', 1, 1),
(7, 'magical_two_hand_arm', 'Yes', 'No', 3, 2),
(8, 'ligth_shield', 'Yes', 'No', 2, 1),
(9, 'intermediate_shield', 'Yes', 'No', 2, 1),
(10, 'heavy_shield', 'Yes', 'No', 2, 1),
(11, 'cuirass', 'Yes', 'No', 4, 1),
(12, 'helmet', 'Yes', 'No', 32, 1),
(13, 'boots', 'Yes', 'No', 64, 2),
(14, 'cuisse', 'Yes', 'No', 512, 1),
(15, 'gauntlet_right', 'Yes', 'No', 4096, 1),
(16, 'gauntlet_left', 'Yes', 'No', 8192, 1),
(17, 'spaulder_right', 'Yes', 'No', 8, 1),
(18, 'spaulder_left', 'Yes', 'No', 16, 1),
(19, 'talisman', 'Yes', 'No', 128, 1),
(20, 'belt', 'Yes', 'No', 256, 1),
(21, 'claw', 'Yes', 'No', 2, 1),
(22, 'long bow', 'Yes', 'No', 3, 2),
(28, 'arrow', 'No', 'No', 0, 0),
(29, 'potion', 'No', 'No', 0, 0),
(30, 'gem', 'No', 'No', 0, 0),
(31, 'raw_materials', 'No', 'No', 0, 0),
(32, 'herbs', 'No', 'No', 0, 0),
(40, 'tools', 'Yes', 'No', 1, 1),
(41, 'bag of gems', 'Yes', 'No', 16384, 1),
(42, 'bag of materials', 'Yes', 'No', 32768, 1),
(43, 'bag of herbs', 'Yes', 'No', 65536, 1),
(44, 'quiver', 'Yes', 'No', 131072, 1),
(45, 'scroll', 'No', 'No', 0, 0),
(33, 'factice', 'No', 'No', 0, 0);
