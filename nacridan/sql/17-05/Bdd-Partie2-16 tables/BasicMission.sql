-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 20:47
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicMission`
--

CREATE TABLE IF NOT EXISTS `BasicMission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL DEFAULT '',
  `frequency` float NOT NULL DEFAULT '0',
  `success` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `BasicMission`
--

INSERT INTO `BasicMission` (`id`, `name`, `frequency`, `success`) VALUES
(1, 'Mission : Escorter', 10, ''),
(2, 'Mission : Libérer une Ville', 10, ''),
(3, 'Mission : Formule', 0, ''),
(4, 'Mission : Trésor', 10, ''),
(5, 'Mission : Tuer des Monstres', 0, ''),
(6, 'Mission : Transporter un Objet', 0, ''),
(7, 'Mission : Énigme', 0, '');
