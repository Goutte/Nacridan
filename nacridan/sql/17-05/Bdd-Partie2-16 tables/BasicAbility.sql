-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 23 Avril 2013 à 16:59
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `BasicAbility`
--

CREATE TABLE IF NOT EXISTS `BasicAbility` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `ident` varchar(30) NOT NULL DEFAULT '',
  `timeAttack` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `modifierPA` int(11) NOT NULL DEFAULT '8',
  `school` tinyint(4) NOT NULL DEFAULT '1',
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `usable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `BasicAbility`
--

INSERT INTO `BasicAbility` (`id`, `name`, `ident`, `timeAttack`, `modifierPA`, `school`, `level`, `usable`) VALUES
(1, 'Attaque puissante', 'ABILITY_POWERFUL', 'YES', 0, 1, 1, 1),
(2, 'Garde', 'ABILITY_GUARD', 'NO', 5, 9, 0, 0),
(3, 'Premiers soins', 'ABILITY_FIRSTAID', 'NO', 5, 9, 0, 0),
(4, 'Dégâts accrus', 'ABILITY_DAMAGE', 'YES', 0, 9, 0, 1),
(5, 'Tournoiement', 'ABILITY_TWIRL', 'YES', 1, 1, 1, 11),
(6, 'Aiguise Lame', 'ABILITY_SHARPEN', 'NO', 5, 1, 1, 1),
(7, 'Coup traitre', 'ABILITY_THRUST', 'YES', 0, 3, 1, 1),
(8, 'Lancer de bolas', 'ABILITY_BOLAS', 'NO', 8, 3, 1, 2),
(9, 'Coup assomant', 'ABILITY_STUNNED', 'YES', 0, 1, 2, 1),
(10, 'Coup de grâce', 'ABILITY_KNOCKOUT', 'YES', 0, 1, 2, 1),
(11, 'Désarmer', 'ABILITY_DISARM', 'YES', 0, 3, 2, 1),
(12, 'Autorégénération', 'ABILITY_AUTOREGEN', 'NO', 10, 4, 1, 0),
(13, 'Appel de la lumière', 'ABILITY_LIGHT', 'YES', 0, 4, 1, 1),
(14, 'Aura de courage', 'ABILITY_BRAVERY', 'NO', 5, 4, 2, 0),
(15, 'Aura de résistance', 'ABILITY_RESISTANCE', 'NO', 5, 4, 2, 0),
(16, 'Exorcisme de l''ombre', 'ABILITY_EXORCISM', 'NO', 8, 4, 2, 0),
(17, 'Vol à la tire', 'ABILITY_STEAL', 'NO', 5, 3, 1, 0),
(18, 'Tir lointain', 'ABILITY_FAR', 'YES', 1, 2, 1, 13),
(19, 'Tir gênant', 'ABILITY_DISABLING', 'YES', 0, 2, 1, 3),
(20, 'Flèche de négation', 'ABILITY_NEGATION', 'YES', 0, 2, 1, 3),
(21, 'Course celeste', 'ABILITY_RUN', 'NO', 2, 2, 2, 0),
(22, 'Flèche enflammée', 'ABILITY_FLAMING', 'YES', 0, 2, 2, 3),
(23, 'Larçin', 'ABILITY_LARCENY', 'NO', 5, 3, 2, 0),
(24, 'Protection', 'ABILITY_PROTECTION', 'NO', 8, 4, 1, 0),
(25, 'Projection', 'ABILITY_PROJECTION', 'YES', 0, 1, 2, 11),
(26, 'Volée de flèche', 'ABILITY_FLYARROW', 'YES', 0, 2, 2, 3),
(27, 'Embuscade', 'ABILITY_AMBUSH', 'NO', 5, 3, 2, 0),
(28, 'Botte d''estoc', 'ABILITY_TREACHEROUS', 'YES', 0, 9, 0, 1);
