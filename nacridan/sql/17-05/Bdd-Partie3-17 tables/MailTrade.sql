-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 04 Juin 2013 à 09:18
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `MailTrade`
--

CREATE TABLE IF NOT EXISTS `MailTrade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `title` tinytext CHARACTER SET utf8 NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_receiver` (`id_Player$receiver`),
  KEY `id_sender` (`id_Player$sender`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `MailTrade`
--

INSERT INTO `MailTrade` (`id`, `id_Player$receiver`, `id_Player$sender`, `id_MailBody`, `new`, `title`, `important`, `name_Repertory`, `date`) VALUES
(2, 2010, 11277, 1, '1', '', 0, '', '2013-05-27 13:26:34'),
(3, 11277, 11277, 1, '1', '', 1, '', '2013-05-27 13:26:34'),
(5, 2010, 11277, 4, '1', '', 1, '', '2013-05-27 14:02:31'),
(6, 11277, 11277, 4, '1', '', 1, '', '2013-05-27 14:02:31'),
(8, 2010, 11277, 5, '1', '', 0, '', '2013-05-27 14:16:21'),
(13, 2010, 11277, 10, '1', 'A 1 ou pas', 0, '', '2013-05-29 09:59:33'),
(21, 2010, 11277, 14, '1', 'E 1 ou pas', 0, '', '2013-05-29 10:00:39'),
(23, 2010, 11277, 15, '1', 'F count msg', 0, '', '2013-05-29 10:00:55');
