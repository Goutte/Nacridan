-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 04 Juin 2013 à 09:17
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `MailArchive`
--

CREATE TABLE IF NOT EXISTS `MailArchive` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') CHARACTER SET utf8 NOT NULL DEFAULT '1',
  `title` tinytext CHARACTER SET utf8 NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `MailArchive`
--

INSERT INTO `MailArchive` (`id`, `id_Player`, `id_Player$receiver`, `id_Player$sender`, `id_MailBody`, `new`, `title`, `important`, `name_Repertory`, `date`) VALUES
(1, 0, 2006, 2006, 22, '1', 'Enchantement épée', 0, ' Test1', '2013-05-30 09:30:35'),
(2, 0, 2006, 2006, 23, '1', ' Achat épée deux main', 0, ' Test1', '2013-05-30 09:31:56'),
(4, 0, 2006, 11277, 21, '1', 'Jeudi, juste prix', 0, ' Test2', '2013-05-30 07:48:04'),
(5, 0, 2006, 2006, 24, '', 'les épées légendaire', 0, ' Test2', '2013-05-30 09:32:44');
