-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 10 Mars 2014 à 19:38
-- Version du serveur: 5.1.32
-- Version de PHP: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `NacridanV2B`
--

-- --------------------------------------------------------

--
-- Structure de la table `TeamRankInfo`
--

CREATE TABLE IF NOT EXISTS `TeamRankInfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Team` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `num` tinyint(4) NOT NULL DEFAULT '0',
  `auth1` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth2` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth3` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth4` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth5` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth6` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth7` enum('Yes','No') NOT NULL DEFAULT 'No',
  `auth8` enum('Yes','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`),
  KEY `id_Team` (`id_Team`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7201 ;
