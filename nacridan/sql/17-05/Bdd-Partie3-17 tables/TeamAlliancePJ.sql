-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 21:21
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `TeamAlliancePJ`
--

CREATE TABLE IF NOT EXISTS `TeamAlliancePJ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Team$src` int(11) NOT NULL DEFAULT '0',
  `id_Player` int(11) NOT NULL DEFAULT '0',
  `type` enum('Allié','Ennemi') NOT NULL DEFAULT 'Allié',
  `body` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `datestart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datestop` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_Team$src_2` (`id_Team$src`,`id_Player`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=553 ;
