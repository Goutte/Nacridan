-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 04 Juin 2013 à 09:17
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `Mail`
--

CREATE TABLE IF NOT EXISTS `Mail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player$receiver` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$sender` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `new` enum('1','0') NOT NULL DEFAULT '1',
  `title` tinytext NOT NULL,
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_receiver` (`id_Player$receiver`),
  KEY `id_sender` (`id_Player$sender`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `Mail`
--

INSERT INTO `Mail` (`id`, `id_Player$receiver`, `id_Player$sender`, `id_MailBody`, `new`, `title`, `important`, `name_Repertory`, `date`) VALUES
(2, 11286, 11277, 2, '1', '', 0, '', '2013-05-27 13:27:19'),
(4, 11286, 11277, 3, '1', '', 0, '', '2013-05-27 13:29:48'),
(6, 11286, 11277, 7, '1', '', 0, '', '2013-05-28 09:44:08'),
(8, 11286, 11277, 8, '1', ' Vente', 0, '', '2013-05-29 08:34:56'),
(20, 11286, 11277, 20, '1', ' Achat épée deux main', 0, '', '2013-05-29 11:46:01'),
(26, 2010, 2006, 23, '1', ' Achat épée deux main', 0, '', '2013-05-30 09:31:56'),
(10, 11286, 11277, 9, '1', 'F count msg', 0, '', '2013-05-29 09:55:12'),
(24, 2010, 2006, 22, '1', 'Enchantement épée', 0, '', '2013-05-30 09:30:35'),
(25, 2006, 2006, 23, '1', ' Achat épée deux main', 0, ' Test1', '2013-05-30 09:31:56'),
(23, 2006, 2006, 22, '1', 'Enchantement épée', 0, ' Test1', '2013-05-30 09:30:35'),
(22, 11286, 11277, 21, '1', 'Jeudi, juste prix', 0, '', '2013-05-30 07:48:04'),
(21, 2006, 11277, 21, '1', 'Jeudi, juste prix', 0, ' Test2', '2013-05-30 07:48:04'),
(27, 2006, 2006, 24, '', 'les épées légendaire', 0, ' Test2', '2013-05-30 09:32:44'),
(28, 2010, 2006, 24, '1', 'les épées légendaire', 0, '', '2013-05-30 09:32:44');
