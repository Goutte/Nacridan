-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 04 Juin 2013 à 09:18
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanv2stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `MailSend`
--

CREATE TABLE IF NOT EXISTS `MailSend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_MailBody` bigint(20) NOT NULL DEFAULT '0',
  `important` tinyint(1) NOT NULL DEFAULT '0',
  `name_Repertory` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_MailBody` (`id_MailBody`),
  KEY `id_Player` (`id_Player`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `MailSend`
--

INSERT INTO `MailSend` (`id`, `id_Player`, `id_MailBody`, `important`, `name_Repertory`) VALUES
(1, 11277, 21, 1, ''),
(2, 2006, 22, 0, ''),
(3, 2006, 23, 0, ''),
(4, 2006, 24, 0, '');
