-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 21:16
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `Paypal`
--

CREATE TABLE IF NOT EXISTS `Paypal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(50) NOT NULL DEFAULT '',
  `item_name` varchar(100) NOT NULL DEFAULT '',
  `item_number` int(11) NOT NULL DEFAULT '0',
  `payment_status` varchar(100) NOT NULL DEFAULT '',
  `mc_gross` float NOT NULL DEFAULT '0',
  `mc_fee` float NOT NULL DEFAULT '0',
  `mc_currency` varchar(10) NOT NULL DEFAULT '',
  `receiver_email` varchar(50) NOT NULL DEFAULT '',
  `payer_email` varchar(50) NOT NULL DEFAULT '',
  `post_value` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `txn_id` (`txn_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;
