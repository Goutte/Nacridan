-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 20:40
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `Banned`
--

CREATE TABLE IF NOT EXISTS `Banned` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$target` bigint(20) NOT NULL DEFAULT '0',
  `body` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;


--
-- Structure de la table `PaypalProduct`
--

CREATE TABLE IF NOT EXISTS `PaypalProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `PaypalProduct`
--

INSERT INTO `PaypalProduct` (`id`, `label`, `description`) VALUES
(1, 'Nacridan-RemoveBanner', 'Enlève Les pubs de Nacridan.\r\n');


-- --------------------------------------------------------

--
-- Structure de la table `Paypal`
--

CREATE TABLE IF NOT EXISTS `Paypal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(50) NOT NULL DEFAULT '',
  `item_name` varchar(100) NOT NULL DEFAULT '',
  `item_number` int(11) NOT NULL DEFAULT '0',
  `payment_status` varchar(100) NOT NULL DEFAULT '',
  `mc_gross` float NOT NULL DEFAULT '0',
  `mc_fee` float NOT NULL DEFAULT '0',
  `mc_currency` varchar(10) NOT NULL DEFAULT '',
  `receiver_email` varchar(50) NOT NULL DEFAULT '',
  `payer_email` varchar(50) NOT NULL DEFAULT '',
  `post_value` mediumtext NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `txn_id` (`txn_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;


-- --------------------------------------------------------

--
-- Structure de la table `Timezone`
--

CREATE TABLE IF NOT EXISTS `Timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `utcOffset` int(11) NOT NULL DEFAULT '0',
  `utcOffsetDLS` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=456 ;

--
-- Contenu de la table `Timezone`
--

INSERT INTO `Timezone` (`id`, `name`, `utcOffset`, `utcOffsetDLS`) VALUES
(1, 'Africa/Abidjan', 0, 0),
(2, 'Africa/Accra', 0, 0),
(3, 'Africa/Addis_Ababa', 180, 180),
(4, 'Africa/Algiers', 60, 60),
(5, 'Africa/Asmera', 180, 180),
(6, 'Africa/Bamako', 0, 0),
(7, 'Africa/Bangui', 60, 60),
(8, 'Africa/Banjul', 0, 0),
(9, 'Africa/Bissau', 0, 0),
(10, 'Africa/Blantyre', 120, 120),
(11, 'Africa/Brazzaville', 60, 60),
(12, 'Africa/Bujumbura', 120, 120),
(13, 'Africa/Cairo', 120, 120),
(14, 'Africa/Casablanca', 0, 0),
(15, 'Africa/Ceuta', 60, 120),
(16, 'Africa/Conakry', 0, 0),
(17, 'Africa/Dakar', 0, 0),
(18, 'Africa/Dar_es_Salaam', 180, 180),
(19, 'Africa/Djibouti', 180, 180),
(20, 'Africa/Douala', 60, 60),
(21, 'Africa/El_Aaiun', 0, 0),
(22, 'Africa/Freetown', 0, 0),
(23, 'Africa/Gaborone', 120, 120),
(24, 'Africa/Harare', 120, 120),
(25, 'Africa/Johannesburg', 120, 120),
(26, 'Africa/Kampala', 180, 180),
(27, 'Africa/Khartoum', 180, 180),
(28, 'Africa/Kigali', 120, 120),
(29, 'Africa/Kinshasa', 60, 60),
(30, 'Africa/Lagos', 60, 60),
(31, 'Africa/Libreville', 60, 60),
(32, 'Africa/Lome', 0, 0),
(33, 'Africa/Luanda', 60, 60),
(34, 'Africa/Lubumbashi', 120, 120),
(35, 'Africa/Lusaka', 120, 120),
(36, 'Africa/Malabo', 60, 60),
(37, 'Africa/Maputo', 120, 120),
(38, 'Africa/Maseru', 120, 120),
(39, 'Africa/Mbabane', 120, 120),
(40, 'Africa/Mogadishu', 180, 180),
(41, 'Africa/Monrovia', 0, 0),
(42, 'Africa/Nairobi', 180, 180),
(43, 'Africa/Ndjamena', 60, 60),
(44, 'Africa/Niamey', 60, 60),
(45, 'Africa/Nouakchott', 0, 0),
(46, 'Africa/Ouagadougou', 0, 0),
(47, 'Africa/Porto-Novo', 60, 60),
(48, 'Africa/Sao_Tome', 0, 0),
(49, 'Africa/Timbuktu', 0, 0),
(50, 'Africa/Tripoli', 120, 120),
(51, 'Africa/Tunis', 60, 60),
(52, 'Africa/Windhoek', 120, 60),
(53, 'America/Adak', -600, -540),
(54, 'America/Anchorage', -540, -480),
(55, 'America/Anguilla', -240, -240),
(56, 'America/Antigua', -240, -240),
(57, 'America/Araguaina', -180, -180),
(58, 'America/Argentina/Buenos_Aires', -180, -180),
(59, 'America/Argentina/Catamarca', -180, -180),
(60, 'America/Argentina/ComodRivadavia', -180, -180),
(61, 'America/Argentina/Cordoba', -180, -180),
(62, 'America/Argentina/Jujuy', -180, -180),
(63, 'America/Argentina/La_Rioja', -180, -180),
(64, 'America/Argentina/Mendoza', -180, -180),
(65, 'America/Argentina/Rio_Gallegos', -180, -180),
(66, 'America/Argentina/San_Juan', -180, -180),
(67, 'America/Argentina/Tucuman', -180, -180),
(68, 'America/Argentina/Ushuaia', -180, -180),
(69, 'America/Aruba', -240, -240),
(70, 'America/Asuncion', -180, -240),
(71, 'America/Atka', -600, -540),
(72, 'America/Bahia', -180, -180),
(73, 'America/Barbados', -240, -240),
(74, 'America/Belem', -180, -180),
(75, 'America/Belize', -360, -360),
(76, 'America/Boa_Vista', -240, -240),
(77, 'America/Bogota', -300, -300),
(78, 'America/Boise', -420, -360),
(79, 'America/Buenos_Aires', -180, -180),
(80, 'America/Cambridge_Bay', -420, -360),
(81, 'America/Campo_Grande', -180, -240),
(82, 'America/Cancun', -360, -300),
(83, 'America/Caracas', -240, -240),
(84, 'America/Catamarca', -180, -180),
(85, 'America/Cayenne', -180, -180),
(86, 'America/Cayman', -300, -300),
(87, 'America/Chicago', -360, -300),
(88, 'America/Chihuahua', -420, -360),
(89, 'America/Coral_Harbour', -300, -300),
(90, 'America/Cordoba', -180, -180),
(91, 'America/Costa_Rica', -360, -360),
(92, 'America/Cuiaba', -180, -240),
(93, 'America/Curacao', -240, -240),
(94, 'America/Danmarkshavn', 0, 0),
(95, 'America/Dawson', -480, -420),
(96, 'America/Dawson_Creek', -420, -420),
(97, 'America/Denver', -420, -360),
(98, 'America/Detroit', -300, -240),
(99, 'America/Dominica', -240, -240),
(100, 'America/Edmonton', -420, -360),
(101, 'America/Eirunepe', -300, -300),
(102, 'America/El_Salvador', -360, -360),
(103, 'America/Ensenada', -480, -420),
(104, 'America/Fort_Wayne', -300, -240),
(105, 'America/Fortaleza', -180, -180),
(106, 'America/Glace_Bay', -240, -180),
(107, 'America/Godthab', -180, -120),
(108, 'America/Goose_Bay', -240, -180),
(109, 'America/Grand_Turk', -300, -240),
(110, 'America/Grenada', -240, -240),
(111, 'America/Guadeloupe', -240, -240),
(112, 'America/Guatemala', -360, -360),
(113, 'America/Guayaquil', -300, -300),
(114, 'America/Guyana', -240, -240),
(115, 'America/Halifax', -240, -180),
(116, 'America/Havana', -240, -240),
(117, 'America/Hermosillo', -420, -420),
(118, 'America/Indiana/Indianapolis', -300, -240),
(119, 'America/Indiana/Knox', -300, -240),
(120, 'America/Indiana/Marengo', -300, -240),
(121, 'America/Indiana/Vevay', -300, -240),
(122, 'America/Indianapolis', -300, -240),
(123, 'America/Inuvik', -420, -360),
(124, 'America/Iqaluit', -300, -240),
(125, 'America/Jamaica', -300, -300),
(126, 'America/Jujuy', -180, -180),
(127, 'America/Juneau', -540, -480),
(128, 'America/Kentucky/Louisville', -300, -240),
(129, 'America/Kentucky/Monticello', -300, -240),
(130, 'America/Knox_IN', -300, -240),
(131, 'America/La_Paz', -240, -240),
(132, 'America/Lima', -300, -300),
(133, 'America/Los_Angeles', -480, -420),
(134, 'America/Louisville', -300, -240),
(135, 'America/Maceio', -180, -180),
(136, 'America/Managua', -360, -360),
(137, 'America/Manaus', -240, -240),
(138, 'America/Martinique', -240, -240),
(139, 'America/Mazatlan', -420, -360),
(140, 'America/Mendoza', -180, -180),
(141, 'America/Menominee', -360, -300),
(142, 'America/Merida', -360, -300),
(143, 'America/Mexico_City', -360, -300),
(144, 'America/Miquelon', -180, -120),
(145, 'America/Monterrey', -360, -300),
(146, 'America/Montevideo', -120, -180),
(147, 'America/Montreal', -300, -240),
(148, 'America/Montserrat', -240, -240),
(149, 'America/Nassau', -300, -240),
(150, 'America/New_York', -300, -240),
(151, 'America/Nipigon', -300, -240),
(152, 'America/Nome', -540, -480),
(153, 'America/Noronha', -120, -120),
(154, 'America/North_Dakota/Center', -360, -300),
(155, 'America/Panama', -300, -300),
(156, 'America/Pangnirtung', -300, -240),
(157, 'America/Paramaribo', -180, -180),
(158, 'America/Phoenix', -420, -420),
(159, 'America/Port-au-Prince', -300, -300),
(160, 'America/Port_of_Spain', -240, -240),
(161, 'America/Porto_Acre', -300, -300),
(162, 'America/Porto_Velho', -240, -240),
(163, 'America/Puerto_Rico', -240, -240),
(164, 'America/Rainy_River', -360, -300),
(165, 'America/Rankin_Inlet', -360, -300),
(166, 'America/Recife', -180, -180),
(167, 'America/Regina', -360, -360),
(168, 'America/Rio_Branco', -300, -300),
(169, 'America/Rosario', -180, -180),
(170, 'America/Santiago', -180, -240),
(171, 'America/Santo_Domingo', -240, -240),
(172, 'America/Sao_Paulo', -120, -180),
(173, 'America/Scoresbysund', -60, 0),
(174, 'America/Shiprock', -420, -360),
(175, 'America/St_Johns', -210, -150),
(176, 'America/St_Kitts', -240, -240),
(177, 'America/St_Lucia', -240, -240),
(178, 'America/St_Thomas', -240, -240),
(179, 'America/St_Vincent', -240, -240),
(180, 'America/Swift_Current', -360, -360),
(181, 'America/Tegucigalpa', -360, -360),
(182, 'America/Thule', -240, -180),
(183, 'America/Thunder_Bay', -300, -240),
(184, 'America/Tijuana', -480, -420),
(185, 'America/Toronto', -300, -240),
(186, 'America/Tortola', -240, -240),
(187, 'America/Vancouver', -480, -420),
(188, 'America/Virgin', -240, -240),
(189, 'America/Whitehorse', -480, -420),
(190, 'America/Winnipeg', -360, -300),
(191, 'America/Yakutat', -540, -480),
(192, 'America/Yellowknife', -420, -360),
(193, 'Brazil/Acre', -300, -300),
(194, 'Brazil/DeNoronha', -120, -120),
(195, 'Brazil/East', -120, -180),
(196, 'Brazil/West', -240, -240),
(197, 'Canada/Atlantic', -240, -180),
(198, 'Canada/Central', -360, -300),
(199, 'Canada/East-Saskatchewan', -360, -360),
(200, 'Canada/Eastern', -300, -240),
(201, 'Canada/Mountain', -420, -360),
(202, 'Canada/Newfoundland', -210, -150),
(203, 'Canada/Pacific', -480, -420),
(204, 'Canada/Saskatchewan', -360, -360),
(205, 'Canada/Yukon', -480, -420),
(206, 'Chile/Continental', -180, -240),
(207, 'Chile/EasterIsland', -300, -360),
(208, 'Mexico/BajaNorte', -480, -420),
(209, 'Mexico/BajaSur', -420, -360),
(210, 'Mexico/General', -360, -300),
(211, 'US/Alaska', -540, -480),
(212, 'US/Aleutian', -600, -540),
(213, 'US/Arizona', -420, -420),
(214, 'US/Central', -360, -300),
(215, 'US/East-Indiana', -300, -240),
(216, 'US/Eastern', -300, -240),
(217, 'US/Hawaii', -600, -600),
(218, 'US/Indiana-Starke', -300, -240),
(219, 'US/Michigan', -300, -240),
(220, 'US/Mountain', -420, -360),
(221, 'US/Pacific', -480, -420),
(222, 'US/Pacific-New', -480, -420),
(223, 'US/Samoa', -660, -660),
(224, 'Antarctica/Casey', 480, 480),
(225, 'Antarctica/Davis', 420, 420),
(226, 'Antarctica/DumontDUrville', 600, 600),
(227, 'Antarctica/Mawson', 360, 360),
(228, 'Antarctica/McMurdo', 780, 720),
(229, 'Antarctica/Palmer', -180, -240),
(230, 'Antarctica/Rothera', -180, -180),
(231, 'Antarctica/South_Pole', 780, 720),
(232, 'Antarctica/Syowa', 180, 180),
(233, 'Antarctica/Vostok', 360, 360),
(234, 'Arctic/Longyearbyen', 60, 120),
(235, 'Asia/Aden', 180, 180),
(236, 'Asia/Almaty', 360, 360),
(237, 'Asia/Amman', 120, 180),
(238, 'Asia/Anadyr', 720, 780),
(239, 'Asia/Aqtau', 300, 300),
(240, 'Asia/Aqtobe', 300, 300),
(241, 'Asia/Ashgabat', 300, 300),
(242, 'Asia/Ashkhabad', 300, 300),
(243, 'Asia/Baghdad', 180, 240),
(244, 'Asia/Bahrain', 180, 180),
(245, 'Asia/Baku', 240, 300),
(246, 'Asia/Bangkok', 420, 420),
(247, 'Asia/Beirut', 120, 180),
(248, 'Asia/Bishkek', 360, 360),
(249, 'Asia/Brunei', 480, 480),
(250, 'Asia/Calcutta', 330, 330),
(251, 'Asia/Choibalsan', 540, 600),
(252, 'Asia/Chongqing', 480, 480),
(253, 'Asia/Chungking', 480, 480),
(254, 'Asia/Colombo', 360, 360),
(255, 'Asia/Dacca', 360, 360),
(256, 'Asia/Damascus', 120, 180),
(257, 'Asia/Dhaka', 360, 360),
(258, 'Asia/Dili', 540, 540),
(259, 'Asia/Dubai', 240, 240),
(260, 'Asia/Dushanbe', 300, 300),
(261, 'Asia/Gaza', 120, 180),
(262, 'Asia/Harbin', 480, 480),
(263, 'Asia/Hong_Kong', 480, 480),
(264, 'Asia/Hovd', 420, 480),
(265, 'Asia/Irkutsk', 480, 540),
(266, 'Asia/Istanbul', 120, 180),
(267, 'Asia/Jakarta', 420, 420),
(268, 'Asia/Jayapura', 540, 540),
(269, 'Asia/Jerusalem', 120, 180),
(270, 'Asia/Kabul', 270, 270),
(271, 'Asia/Kamchatka', 720, 780),
(272, 'Asia/Karachi', 300, 300),
(273, 'Asia/Kashgar', 480, 480),
(274, 'Asia/Katmandu', 345, 345),
(275, 'Asia/Krasnoyarsk', 420, 480),
(276, 'Asia/Kuala_Lumpur', 480, 480),
(277, 'Asia/Kuching', 480, 480),
(278, 'Asia/Kuwait', 180, 180),
(279, 'Asia/Macao', 480, 480),
(280, 'Asia/Macau', 480, 480),
(281, 'Asia/Magadan', 660, 720),
(282, 'Asia/Makassar', 480, 480),
(283, 'Asia/Manila', 480, 480),
(284, 'Asia/Muscat', 240, 240),
(285, 'Asia/Nicosia', 120, 180),
(286, 'Asia/Novosibirsk', 360, 420),
(287, 'Asia/Omsk', 360, 420),
(288, 'Asia/Oral', 300, 300),
(289, 'Asia/Phnom_Penh', 420, 420),
(290, 'Asia/Pontianak', 420, 420),
(291, 'Asia/Pyongyang', 540, 540),
(292, 'Asia/Qatar', 180, 180),
(293, 'Asia/Qyzylorda', 360, 360),
(294, 'Asia/Rangoon', 390, 390),
(295, 'Asia/Riyadh', 180, 180),
(296, 'Asia/Saigon', 420, 420),
(297, 'Asia/Sakhalin', 600, 660),
(298, 'Asia/Samarkand', 300, 300),
(299, 'Asia/Seoul', 540, 540),
(300, 'Asia/Shanghai', 480, 480),
(301, 'Asia/Singapore', 480, 480),
(302, 'Asia/Taipei', 480, 480),
(303, 'Asia/Tashkent', 300, 300),
(304, 'Asia/Tbilisi', 240, 240),
(305, 'Asia/Tehran', 210, 270),
(306, 'Asia/Tel_Aviv', 120, 180),
(307, 'Asia/Thimbu', 360, 360),
(308, 'Asia/Thimphu', 360, 360),
(309, 'Asia/Tokyo', 540, 540),
(310, 'Asia/Ujung_Pandang', 480, 480),
(311, 'Asia/Ulaanbaatar', 480, 540),
(312, 'Asia/Ulan_Bator', 480, 540),
(313, 'Asia/Urumqi', 480, 480),
(314, 'Asia/Vientiane', 420, 420),
(315, 'Asia/Vladivostok', 600, 660),
(316, 'Asia/Yakutsk', 540, 600),
(317, 'Asia/Yekaterinburg', 300, 360),
(318, 'Asia/Yerevan', 240, 300),
(319, 'Indian/Antananarivo', 180, 180),
(320, 'Indian/Chagos', 360, 360),
(321, 'Indian/Christmas', 420, 420),
(322, 'Indian/Cocos', 390, 390),
(323, 'Indian/Comoro', 180, 180),
(324, 'Indian/Kerguelen', 300, 300),
(325, 'Indian/Mahe', 240, 240),
(326, 'Indian/Maldives', 300, 300),
(327, 'Indian/Mauritius', 240, 240),
(328, 'Indian/Mayotte', 180, 180),
(329, 'Indian/Reunion', 240, 240),
(330, 'Atlantic/Azores', -60, 0),
(331, 'Atlantic/Bermuda', -240, -180),
(332, 'Atlantic/Canary', 0, 60),
(333, 'Atlantic/Cape_Verde', -60, -60),
(334, 'Atlantic/Faeroe', 0, 60),
(335, 'Atlantic/Jan_Mayen', 60, 120),
(336, 'Atlantic/Madeira', 0, 60),
(337, 'Atlantic/Reykjavik', 0, 0),
(338, 'Atlantic/South_Georgia', -120, -120),
(339, 'Atlantic/St_Helena', 0, 0),
(340, 'Atlantic/Stanley', -180, -240),
(341, 'Australia/ACT', 660, 600),
(342, 'Australia/Adelaide', 630, 570),
(343, 'Australia/Brisbane', 600, 600),
(344, 'Australia/Broken_Hill', 630, 570),
(345, 'Australia/Canberra', 660, 600),
(346, 'Australia/Currie', 660, 600),
(347, 'Australia/Darwin', 570, 570),
(348, 'Australia/Hobart', 660, 600),
(349, 'Australia/LHI', 660, 630),
(350, 'Australia/Lindeman', 600, 600),
(351, 'Australia/Lord_Howe', 660, 630),
(352, 'Australia/Melbourne', 660, 600),
(353, 'Australia/North', 570, 570),
(354, 'Australia/NSW', 660, 600),
(355, 'Australia/Perth', 480, 480),
(356, 'Australia/Queensland', 600, 600),
(357, 'Australia/South', 630, 570),
(358, 'Australia/Sydney', 660, 600),
(359, 'Australia/Tasmania', 660, 600),
(360, 'Australia/Victoria', 660, 600),
(361, 'Australia/West', 480, 480),
(362, 'Australia/Yancowinna', 630, 570),
(363, 'Europe/Amsterdam', 60, 120),
(364, 'Europe/Andorra', 60, 120),
(365, 'Europe/Athens', 120, 180),
(366, 'Europe/Belfast', 0, 60),
(367, 'Europe/Belgrade', 60, 120),
(368, 'Europe/Berlin', 60, 120),
(369, 'Europe/Bratislava', 60, 120),
(370, 'Europe/Brussels', 60, 120),
(371, 'Europe/Bucharest', 120, 180),
(372, 'Europe/Budapest', 60, 120),
(373, 'Europe/Chisinau', 120, 180),
(374, 'Europe/Copenhagen', 60, 120),
(375, 'Europe/Dublin', 0, 60),
(376, 'Europe/Gibraltar', 60, 120),
(377, 'Europe/Helsinki', 120, 180),
(378, 'Europe/Istanbul', 120, 180),
(379, 'Europe/Kaliningrad', 120, 180),
(380, 'Europe/Kiev', 120, 180),
(381, 'Europe/Lisbon', 0, 60),
(382, 'Europe/Ljubljana', 60, 120),
(383, 'Europe/London', 0, 60),
(384, 'Europe/Luxembourg', 60, 120),
(385, 'Europe/Madrid', 60, 120),
(386, 'Europe/Malta', 60, 120),
(387, 'Europe/Mariehamn', 120, 180),
(388, 'Europe/Minsk', 120, 180),
(389, 'Europe/Monaco', 60, 120),
(390, 'Europe/Moscow', 180, 240),
(391, 'Europe/Nicosia', 120, 180),
(392, 'Europe/Oslo', 60, 120),
(393, 'Europe/Paris', 60, 120),
(394, 'Europe/Prague', 60, 120),
(395, 'Europe/Riga', 120, 180),
(396, 'Europe/Rome', 60, 120),
(397, 'Europe/Samara', 240, 300),
(398, 'Europe/San_Marino', 60, 120),
(399, 'Europe/Sarajevo', 60, 120),
(400, 'Europe/Simferopol', 120, 180),
(401, 'Europe/Skopje', 60, 120),
(402, 'Europe/Sofia', 120, 180),
(403, 'Europe/Stockholm', 60, 120),
(404, 'Europe/Tallinn', 120, 180),
(405, 'Europe/Tirane', 60, 120),
(406, 'Europe/Tiraspol', 120, 180),
(407, 'Europe/Uzhgorod', 120, 180),
(408, 'Europe/Vaduz', 60, 120),
(409, 'Europe/Vatican', 60, 120),
(410, 'Europe/Vienna', 60, 120),
(411, 'Europe/Vilnius', 120, 180),
(412, 'Europe/Warsaw', 60, 120),
(413, 'Europe/Zagreb', 60, 120),
(414, 'Europe/Zaporozhye', 120, 180),
(415, 'Europe/Zurich', 60, 120),
(416, 'Pacific/Apia', -660, -660),
(417, 'Pacific/Auckland', 780, 720),
(418, 'Pacific/Chatham', 825, 765),
(419, 'Pacific/Easter', -300, -360),
(420, 'Pacific/Efate', 660, 660),
(421, 'Pacific/Enderbury', 780, 780),
(422, 'Pacific/Fakaofo', -600, -600),
(423, 'Pacific/Fiji', 720, 720),
(424, 'Pacific/Funafuti', 720, 720),
(425, 'Pacific/Galapagos', -360, -360),
(426, 'Pacific/Gambier', -540, -540),
(427, 'Pacific/Guadalcanal', 660, 660),
(428, 'Pacific/Guam', 600, 600),
(429, 'Pacific/Honolulu', -600, -600),
(430, 'Pacific/Johnston', -600, -600),
(431, 'Pacific/Kiritimati', 840, 840),
(432, 'Pacific/Kosrae', 660, 660),
(433, 'Pacific/Kwajalein', 720, 720),
(434, 'Pacific/Majuro', 720, 720),
(435, 'Pacific/Marquesas', -570, -570),
(436, 'Pacific/Midway', -660, -660),
(437, 'Pacific/Nauru', 720, 720),
(438, 'Pacific/Niue', -660, -660),
(439, 'Pacific/Norfolk', 690, 690),
(440, 'Pacific/Noumea', 660, 660),
(441, 'Pacific/Pago_Pago', -660, -660),
(442, 'Pacific/Palau', 540, 540),
(443, 'Pacific/Pitcairn', -480, -480),
(444, 'Pacific/Ponape', 660, 660),
(445, 'Pacific/Port_Moresby', 600, 600),
(446, 'Pacific/Rarotonga', -600, -600),
(447, 'Pacific/Saipan', 600, 600),
(448, 'Pacific/Samoa', -660, -660),
(449, 'Pacific/Tahiti', -600, -600),
(450, 'Pacific/Tarawa', 720, 720),
(451, 'Pacific/Tongatapu', 780, 780),
(452, 'Pacific/Truk', 600, 600),
(453, 'Pacific/Wake', 720, 720),
(454, 'Pacific/Wallis', 720, 720),
(455, 'Pacific/Yap', 600, 600);


-- --------------------------------------------------------

--
-- Structure de la table `World`
--

CREATE TABLE IF NOT EXISTS `World` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `World`
--

INSERT INTO `World` (`id`, `name`) VALUES
(1, 'BabylonX');


--
-- Structure de la table `StaticModifier_BasicRace`
--

CREATE TABLE IF NOT EXISTS `StaticModifier_BasicRace` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attack` varchar(32) NOT NULL DEFAULT '////////',
  `dodge` varchar(32) NOT NULL DEFAULT '////////',
  `damage` varchar(32) NOT NULL DEFAULT '////////',
  `armor` varchar(32) NOT NULL DEFAULT '////////',
  `regen` varchar(32) NOT NULL DEFAULT '////////',
  `hp` varchar(32) NOT NULL DEFAULT '////////',
  `magicSkill` varchar(32) NOT NULL DEFAULT '////////',
  `magicResist` varchar(32) NOT NULL DEFAULT '////////',
  `time` varchar(32) NOT NULL DEFAULT '////////',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `StaticModifier_BasicRace`
--

INSERT INTO `StaticModifier_BasicRace` (`id`, `attack`, `dodge`, `damage`, `armor`, `regen`, `hp`, `magicSkill`, `magicResist`, `time`) VALUES
(1, '/4///////', '/3///////', '3////////', '///////0/', '1////////', '///////30/', '/3///////', '////////', '///////720/'),
(2, '/3///////', '/4///////', '3////////', '///////0/', '1////////', '///////30/', '/3///////', '////////', '///////720/'),
(3, '/3///////', '/3///////', '4////////', '///////0/', '1////////', '///////30/', '/3///////', '////////', '///////720/'),
(4, '/3///////', '/3///////', '3////////', '///////0/', '1////////', '///////30/', '/4///////', '////////', '///////720/');