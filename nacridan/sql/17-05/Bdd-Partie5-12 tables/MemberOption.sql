-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:28
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `MemberOption`
--

CREATE TABLE IF NOT EXISTS `MemberOption` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Member` bigint(20) NOT NULL DEFAULT '0',
  `fightMsg` int(11) NOT NULL DEFAULT '0',
  `mailMsg` int(11) NOT NULL DEFAULT '0',
  `deadMsg` int(11) NOT NULL DEFAULT '1',
  `view2d` tinyint(4) NOT NULL DEFAULT '1',
  `windRose` tinyint(1) NOT NULL DEFAULT '0',
  `music` tinyint(4) NOT NULL DEFAULT '0',
  KEY `id` (`id`),
  KEY `id_Member` (`id_Member`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=6565 ;

--
-- Contenu de la table `MemberOption`
--

