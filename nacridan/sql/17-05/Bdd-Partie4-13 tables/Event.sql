-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mar 23 Avril 2013 à 17:06
-- Version du serveur: 5.1.32
-- Version de PHP: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `nacridanv2`
--

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE IF NOT EXISTS `Event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Member` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$src` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$dest` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicRace$src` int(11) NOT NULL DEFAULT '0',
  `id_BasicRace$dest` int(11) NOT NULL DEFAULT '0',
  `type` varchar(32) NOT NULL DEFAULT '',
  `idtype` bigint(20) NOT NULL DEFAULT '0',
  `typeEvent` int(11) NOT NULL DEFAULT '0',
  `typeAction` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(39) NOT NULL DEFAULT '',
  `ip_forward` varchar(39) NOT NULL DEFAULT '',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `auth` tinyint(4) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_Player$src` (`id_Player$src`),
  KEY `id_Player$dest` (`id_Player$dest`),
  KEY `typeAction` (`typeAction`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `event`
--

