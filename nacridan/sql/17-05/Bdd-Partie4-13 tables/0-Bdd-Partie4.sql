-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 23 Juillet 2012 à 16:42
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Ability`
--

CREATE TABLE IF NOT EXISTS `Ability` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicAbility` bigint(20) NOT NULL DEFAULT '0',
  `skill` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_player` (`id_Player`),
  KEY `id_BasicAbility` (`id_BasicAbility`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `Ability`
--


--
-- Structure de la table `BM`
--

CREATE TABLE IF NOT EXISTS `BM` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$src` bigint(20) NOT NULL DEFAULT '0',
  `id_StaticModifier_BM` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `value` tinyint(4) NOT NULL DEFAULT '0',
  `effect` enum('POSITIVE','NEGATIVE') NOT NULL DEFAULT 'POSITIVE',
  `level` tinyint(7) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `life` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_player` (`id_Player`),
  KEY `id_Payer$src` (`id_Player$src`),
  KEY `life` (`life`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `BM`
--


--
-- Structure de la table `Caravan`
--

CREATE TABLE IF NOT EXISTS `Caravan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL DEFAULT '1',
  `id_Player` bigint(30) NOT NULL DEFAULT '0',
  `finalprice` bigint(20) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `id_startbuilding` int(11) NOT NULL,
  `id_endbuilding` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19560 ;

--
-- Structure de la table `event`
--

CREATE TABLE IF NOT EXISTS `Event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Member` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$src` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$dest` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicRace$src` int(11) NOT NULL DEFAULT '0',
  `id_BasicRace$dest` int(11) NOT NULL DEFAULT '0',
  `type` varchar(32) NOT NULL DEFAULT '',
  `idtype` bigint(20) NOT NULL DEFAULT '0',
  `typeEvent` int(11) NOT NULL DEFAULT '0',
  `typeAction` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(39) NOT NULL DEFAULT '',
  `ip_forward` varchar(39) NOT NULL DEFAULT '',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `auth` tinyint(4) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_Player$src` (`id_Player$src`),
  KEY `id_Player$dest` (`id_Player$dest`),
  KEY `typeAction` (`typeAction`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Structure de la table `gate`
--
DROP TABLE IF EXISTS `Gate`;
CREATE TABLE IF NOT EXISTS `Gate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `npcBand` int(7) NOT NULL DEFAULT '1',
  `nbNPC` tinyint(4) NOT NULL DEFAULT '0',
  `activation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `x` (`x`,`y`),
  KEY `activation` (`activation`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2668 ;


--
-- Structure de la table `HFightMsg`
--

CREATE TABLE IF NOT EXISTS `HActionMsg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player$src` bigint(20) NOT NULL DEFAULT '0',
  `id_Player$dest` bigint(20) NOT NULL DEFAULT '0',
  `body` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1065103 ;

--
-- Structure de la table `MagicSpell`
--

DROP TABLE IF EXISTS `MagicSpell`;
CREATE TABLE IF NOT EXISTS `MagicSpell` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL DEFAULT '0',
  `id_BasicMagicSpell` int(11) NOT NULL DEFAULT '0',
  `skill` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_Player` (`id_Player`),
  KEY `id_BasicMagicSpell` (`id_BasicMagicSpell`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;


--
-- Structure de la table `Talent`
--

DROP TABLE IF EXISTS `Talent`;
CREATE TABLE IF NOT EXISTS `Talent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Player` bigint(20) NOT NULL,
  `id_BasicTalent` int(11) NOT NULL,
  `skill` smallint(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Structure de la table `Template`
--

DROP TABLE IF EXISTS `Template`;
CREATE TABLE `Template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Equipment` bigint(20) NOT NULL,
  `id_BasicTemplate` int(7) NOT NULL,
  `level` int(7) NOT NULL DEFAULT '1',
  `pos` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;


--
-- Structure de la table `Mission`
--

CREATE TABLE IF NOT EXISTS `Mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_BasicMission` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `Mission_level` int(11) NOT NULL DEFAULT '0',
  `Player_levelMin` int(11) NOT NULL DEFAULT '1',
  `Player_levelMax` int(11) NOT NULL DEFAULT '1',
  `In_List` int(11) NOT NULL DEFAULT '0',
  `RP_Mission` longtext NOT NULL,
  `Is_Standard` int(11) NOT NULL DEFAULT '0',
  `id_Quest` int(11) NOT NULL DEFAULT '0',
  `id_NextMission` int(11) NOT NULL DEFAULT '0',
  `NumMission` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19524 ;


--
-- Structure de la table `MissionCondition`
--

CREATE TABLE IF NOT EXISTS `MissionCondition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idMission` int(11) NOT NULL DEFAULT '0',
  `positionXstart` int(11) NOT NULL DEFAULT '0',
  `positionYstart` int(11) NOT NULL DEFAULT '0',
  `positionXstop` int(11) NOT NULL DEFAULT '0',
  `positionYstop` int(11) NOT NULL DEFAULT '0',
  `distanceX` int(11) NOT NULL DEFAULT '0',
  `distanceY` int(11) NOT NULL DEFAULT '0',
  `EscortPositionX` int(11) NOT NULL DEFAULT '0',
  `EscortPositionY` int(11) NOT NULL DEFAULT '0',
  `idEscort` int(11) NOT NULL DEFAULT '0',
  `EquipPositionX` int(11) NOT NULL DEFAULT '0',
  `EquipPositionY` int(11) NOT NULL DEFAULT '0',
  `idEquip` int(11) NOT NULL DEFAULT '0',
  `idPNJtoKill` int(11) NOT NULL DEFAULT '0',
  `EnigmAnswer` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1833 ;


--
-- Structure de la table `MissionReward`
--

CREATE TABLE IF NOT EXISTS `MissionReward` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idMission` int(11) NOT NULL DEFAULT '0',
  `moneyStart` int(11) NOT NULL DEFAULT '0',
  `xpStart` int(11) NOT NULL DEFAULT '0',
  `idEscort` int(11) NOT NULL DEFAULT '0',
  `idEquipStart` int(11) NOT NULL DEFAULT '0',
  `moneyStop` int(11) NOT NULL DEFAULT '0',
  `xpStop` int(11) NOT NULL DEFAULT '0',
  `idEquipStop` int(11) NOT NULL DEFAULT '0',
  `NameEquipStop` varchar(30) NOT NULL DEFAULT '',
  `ExtranameEquipStop` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1833 ;


--
-- Structure de la table `Quest`
--

CREATE TABLE IF NOT EXISTS `Quest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nbMission` int(11) NOT NULL DEFAULT '0',
  `idFirstMission` int(11) NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '',
  `goal` longtext NOT NULL,
  `In_List` int(11) NOT NULL DEFAULT '0',
  `Player_levelMin` int(11) NOT NULL DEFAULT '0',
  `Player_levelMax` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=530 ;

