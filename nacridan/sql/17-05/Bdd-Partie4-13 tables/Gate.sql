﻿-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 25 Juin 2012 à 12:51
-- Version du serveur: 5.1.30
-- Version de PHP: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `nacridanv2`
--

-- --------------------------------------------------------

--
-- Structure de la table `gate`
--
DROP TABLE IF EXISTS `Gate`;
CREATE TABLE IF NOT EXISTS `Gate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `npcBand` int(7) NOT NULL DEFAULT '1',
  `nbNPC` tinyint(4) NOT NULL DEFAULT '0',
  `activation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `x` (`x`,`y`),
  KEY `activation` (`activation`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2668 ;

--
-- Contenu de la table `gate`
--

