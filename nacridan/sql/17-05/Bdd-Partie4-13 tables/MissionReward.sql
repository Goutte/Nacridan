-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Mer 02 Février 2011 à 21:06
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridan`
--

-- --------------------------------------------------------

--
-- Structure de la table `MissionReward`
--

CREATE TABLE IF NOT EXISTS `MissionReward` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idMission` int(11) NOT NULL DEFAULT '0',
  `moneyStart` int(11) NOT NULL DEFAULT '0',
  `xpStart` int(11) NOT NULL DEFAULT '0',
  `idEscort` int(11) NOT NULL DEFAULT '0',
  `idEquipStart` int(11) NOT NULL DEFAULT '0',
  `moneyStop` int(11) NOT NULL DEFAULT '0',
  `xpStop` int(11) NOT NULL DEFAULT '0',
  `idEquipStop` int(11) NOT NULL DEFAULT '0',
  `NameEquipStop` varchar(30) NOT NULL DEFAULT '',
  `ExtranameEquipStop` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1833 ;
