
CREATE TABLE IF NOT EXISTS GroundEvolution (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_GroundInit` int(11) NOT NULL,
  `startCount` int(11) NOT NULL,
  `endCount` int(11) NOT NULL,
  `id_GroundReplace` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_GroundInit` (`id_GroundInit`,`id_GroundReplace`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO GroundEvolution (`id`, `id_GroundInit`, `startCount`, `endCount`, `id_GroundReplace`) 
VALUES (NULL, '3', '0', '29', '3'), (NULL, '3', '30', '46', '6');
INSERT INTO GroundEvolution (`id`, `id_GroundInit`, `startCount`, `endCount`, `id_GroundReplace`)
VALUES (NULL, '3', '47', '60', '2'), (NULL, '6', '0', '17', '6');
INSERT INTO GroundEvolution (`id`, `id_GroundInit`, `startCount`, `endCount`, `id_GroundReplace`)
VALUES (NULL, '6', '18', '60', '2'), (NULL, '4', '0', '39', '4');
INSERT INTO GroundEvolution (`id`, `id_GroundInit`, `startCount`, `endCount`, `id_GroundReplace`)
VALUES (NULL, '4', '40', '60', '2');

ALTER TABLE MapGround ADD wayCount INT NOT NULL DEFAULT '0' ;