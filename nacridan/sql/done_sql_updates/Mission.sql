-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 07 Juillet 2014 à 13:52
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--

-- --------------------------------------------------------

--
-- Structure de la table `Mission`
--

CREATE TABLE IF NOT EXISTS `Mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_BasicMission` int(11) NOT NULL DEFAULT '0',
  `id_EscortMissionRolePlay` int(11) NOT NULL,
  `id_Player` bigint(30) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `Mission_level` int(11) NOT NULL DEFAULT '0',
  `Player_levelMin` int(11) NOT NULL DEFAULT '1',
  `Player_levelMax` int(11) NOT NULL DEFAULT '1',
  `In_List` int(11) NOT NULL DEFAULT '0',
  `RP_Mission` longtext NOT NULL,
  `Is_Standard` int(11) NOT NULL DEFAULT '0',
  `id_Quest` int(11) NOT NULL DEFAULT '0',
  `id_NextMission` int(11) NOT NULL DEFAULT '0',
  `Mission_Active` int(20) NOT NULL DEFAULT '0',
  `NumMission` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;
