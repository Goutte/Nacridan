-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 02 Juillet 2014 à 15:31
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `nacridanv2stage2`
--

-- --------------------------------------------------------

--
-- Structure de la table `basicevent`
--

CREATE TABLE IF NOT EXISTS `BasicEvent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ident` varchar(30) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1007 ;

--
-- Contenu de la table `basicevent`
--

INSERT INTO `BasicEvent` (`id`, `ident`, `description`) VALUES
(50, 'GRAFFITI_EVENT', '{player} a écrit un graffiti en prison.'),
(52, 'CONTINUE_GRAFFITI_EVENT', '{player} a continué l''un de ses graffitis.'),
(452, 'GUILD_RESET_GAUGE_EVENT', '{player} est allé voir l''enchanteur pour retrouver son équilibre.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
