UPDATE `BasicEvent` SET `description` = '{target} a été touché par l''éclair électrique de {player}.' WHERE `BasicEvent`.`id` = 1005;

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES (1007, '31', 'SPELL_FIRECALL_PASSIVE_RELEASE', '{player} a retrouvé sa liberté suite à la mort de {target}.');

update `MagicSpell` set skill = 90 where `id_BasicMagicSpell` in (11,14) and skill = 100;

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES ('242', '31', 'SPELL_RELOAD_EVENT_FAIL', '{player} n''a pas réussi à recharger un bassin divin.');