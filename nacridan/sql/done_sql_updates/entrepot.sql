INSERT INTO `BasicBuilding` (`id`, `name`, `pic`, `price`, `solidity`) VALUES
(28, 'Entrepôt', 'entrepot.png', 250, 'Solid');

INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) 
VALUES
(470, 'Déposer un objet', 'EXCHANGE_OBJECT', 100, 75, 0, 28, 'Yes', 'Yes'),
(471, 'Réparation', 'SHOP_REPAIR_EX', 70, 30, 0, 28, 'Yes', 'Yes'),
(472, 'Récupérer un objet', 'EXCHANGE_GET_OBJECT', 100, 85, 0, 28, 'Yes', 'Yes');

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(460, 13, 'EXCHANGE_OBJECT_EVENT', '{player} a déposé un objet dans un entrepôt.');

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(461,13, 'SHOP_REPAIR_EX_EVENT', '{player} a réparé un objet dans un entrepôt.');

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(462, 13, 'EXCHANGE_GET_OBJECT_EVENT', '{player} a pris un objet dans un entrepôt.');

INSERT INTO `BuildingAction` (`id` ,`name` ,`ident` ,`price` ,`profit` ,`ap` ,`id_BasicBuilding` ,`taxable`,`treasureAccount`)
VALUES (
'410', 'Apprentissage d''un sortilège', 'SCHOOL_LEARN_SPELL', '0', '0', '0', '12', 'No','Yes'
);

INSERT INTO `BuildingAction` (`id` ,`name` ,`ident` ,`price` ,`profit` ,`ap` ,`id_BasicBuilding` ,`taxable`,`treasureAccount`)
VALUES (
'411', 'Apprentissage d''une compétence', 'SCHOOL_LEARN_ABILITY', '0', '0', '0', '12', 'No','Yes'
);

INSERT INTO `BuildingAction` (`id` ,`name` ,`ident` ,`price` ,`profit` ,`ap` ,`id_BasicBuilding` ,`taxable`,`treasureAccount`)
VALUES (
'412', 'Apprentissage d''un savoir-faire', 'SCHOOL_LEARN_TALENT', '0', '0', '0', '12', 'No','Yes'
);

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(463, 13, 'PALACE_TRANSFER_MONEY_EVENT', '{player} a alimenté la caisse d''un bâtiment depuis la caisse d''un village.');

INSERT INTO `BasicEventType` (`id`, `code`) VALUES ('35', 'DESARMER');

UPDATE `BasicEvent` SET `idBasicEventType` = '35' WHERE `BasicEvent`.`id` =107;

UPDATE `BasicEvent` SET `idBasicEventType` = '35' WHERE `BasicEvent`.`id` =108;

INSERT INTO `BasicEventType` (`id`, `code`) VALUES ('36', 'RECUPERATION DE COLIS');

INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) VALUES
(464, 36, 'RETRIEVE_PACKAGE_EVENT', '{player} a récupéré un colis oublié par son destinataire.');