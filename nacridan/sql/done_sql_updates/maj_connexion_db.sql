ALTER TABLE `BuildingAction`
  DROP PRIMARY KEY,
   ADD PRIMARY KEY(
     `id`,
     `id_BasicBuilding`);
	 
	 INSERT INTO `BuildingAction` (`id`, `name`, `ident`, `price`, `profit`, `ap`, `id_BasicBuilding`, `taxable`, `treasureAccount`) VALUES (413, 'Déposer de l''argent', 'BANK_DEPOSIT', 0, 0, 0, 2, 'Yes', 'Yes'), (414, 'Retirer de l''argent', 'BANK_WITHDRAW', 0, 0, 0, 2, 'No', 'Yes'), (415, 'Transfert d''argent', 'BANK_TRANSFERT', 0, 0, 0, 2, 'Yes', 'Yes');
	 
	 UPDATE `BasicRace` SET `dropItem` = '133515' WHERE `BasicRace`.`id` = 120;