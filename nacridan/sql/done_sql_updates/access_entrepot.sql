
ALTER TABLE `City` ADD `accessEntrepot` INT NOT NULL AFTER `accessTemple`;
UPDATE `City` SET accessEntrepot=door WHERE 1;
INSERT INTO `BasicEvent` (`id`, `idBasicEventType`, `ident`, `description`) 
VALUES ('465', '13', 'OPERATE_VILLAGE_ENTREPOT_EVENT', '{player} a modifié les règles d''accès à ses entrepôts');