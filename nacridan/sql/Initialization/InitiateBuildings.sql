-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 12 Février 2012 à 12:31
-- Version du serveur: 5.1.41
-- Version de PHP: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2test`
--







-- VILLES

-- Auberge niv 6

UPDATE `Building` SET sp=300,currsp=300 WHERE id_BasicBuilding=1;

-- Banque

UPDATE `Building` SET sp=150,currsp=150 WHERE id_BasicBuilding=2;

-- Comptoir

UPDATE `Building` SET sp=100,currsp=100 WHERE id_BasicBuilding=4;

-- Echoppe niv 6

UPDATE `Building` SET sp=300,currsp=300,level=6 WHERE id_BasicBuilding=5;

-- ****  Ecole de magie et combat, spécialisation par cité ****
-- Les villes n'ont pas l'école  de l'ombre sauf Nephy. Elle sera dispo dans certain bourg (repère de voleurs)

-- Artasse

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=159;
UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=159;

-- Earok

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=135;
UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=135;

-- Dust

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=158;
UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:2;}' WHERE id_BasicBuilding=7 AND id_City=158;

-- Tonak

UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:2;}' WHERE id_BasicBuilding=6 AND id_City=151;
UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=151;

-- Djin

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=103;
UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:2;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=103;

-- Krima

UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:2;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=82;
UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=82;

-- Landar

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=65;
UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:2;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=65;

-- Nephy

UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:3;i:2;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=11;
UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=11;

-- Izandar

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:9;i:1;i:1;i:1;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=25;
UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:10;i:1;i:5;i:2;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=25;

-- Octobian

UPDATE `Building` SET sp=600,currsp=600,level=6,school='a:5:{i:9;i:1;i:1;i:2;i:2;i:1;i:4;i:1;}' WHERE id_BasicBuilding=6 AND id_City=23;
UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:5:{i:10;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;}' WHERE id_BasicBuilding=7 AND id_City=23;

-- ****  Fin des écoles de magie et combat ***

-- Ecole métiers niv 5

UPDATE `Building` SET sp=420,currsp=420,level=5,school='a:20:{i:1;s:1:"1";i:2;s:1:"1";i:3;s:1:"1";i:4;s:1:"1";i:5;s:1:"1";i:6;s:1:"1";i:7;s:1:"3";i:8;s:1:"3";i:9;s:1:"3";i:10;s:1:"3";i:11;s:1:"3";i:12;s:1:"3";i:13;s:1:"3";i:14;s:1:"2";i:15;s:1:"2";i:16;s:1:"2";i:17;s:1:"2";i:18;s:1:"2";i:19;s:1:"2";i:20;s:1:"2";}' WHERE id_BasicBuilding=8;

-- Guild niv 4

UPDATE `Building` SET sp=280,currsp=280,level=4 WHERE id_BasicBuilding=9;

-- Maisons

UPDATE `Building` SET sp=50,currsp=50 WHERE id_BasicBuilding=10;

-- Palais prison caserne

UPDATE `Building` SET sp=150,currsp=150 WHERE id_BasicBuilding=3;
UPDATE `Building` SET sp=150,currsp=150 WHERE id_BasicBuilding=11;
UPDATE `Building` SET sp=150,currsp=150 WHERE id_BasicBuilding=12;
UPDATE `Building` SET sp=150,currsp=150 WHERE id_BasicBuilding=13;

-- Temple niv 2

UPDATE `Building` SET sp=180,currsp=180,level=2 WHERE id_BasicBuilding=14;

-- Murs

UPDATE `Building` SET sp=150,currsp=150 WHERE id_BasicBuilding>15 AND id_BasicBuilding<27; 

