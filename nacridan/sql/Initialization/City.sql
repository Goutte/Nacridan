﻿-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 12 Juillet 2012 à 16:41
-- Version du serveur: 5.1.30
-- Version de PHP: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `nacridanv2`
--

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

CREATE TABLE IF NOT EXISTS `City` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `id_Player` bigint(20) NOT NULL,
  `type` enum('Village','Bourg','Ville') NOT NULL DEFAULT 'Village',
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `money` bigint(30) NOT NULL,
  `captured` int(11) NOT NULL DEFAULT '0',
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  `map` int(11) NOT NULL DEFAULT '1',
  `bigpic` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `emerald` smallint(7) NOT NULL DEFAULT '0',
  `ruby` tinyint(7) NOT NULL DEFAULT '0',
  `wood` tinyint(7) NOT NULL DEFAULT '0',
  `linen` tinyint(7) NOT NULL DEFAULT '0',
  `iron` tinyint(7) NOT NULL DEFAULT '0',
  `leather` tinyint(7) NOT NULL DEFAULT '0',
  `scale` tinyint(7) NOT NULL DEFAULT '0',
  `root` tinyint(7) NOT NULL DEFAULT '0',
  `seed` tinyint(7) NOT NULL DEFAULT '0',
  `leaf` smallint(7) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `xy` (`x`,`y`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=1 AUTO_INCREMENT=236 ;

--
-- Contenu de la table `city`
--

INSERT INTO `City` (`id`, `name`, `id_Player`, `type`, `level`, `money`, `captured`, `x`, `y`, `map`, `bigpic`, `description`, `emerald`, `ruby`, `wood`, `linen`, `iron`, `leather`, `scale`, `root`, `seed`, `leaf`) VALUES
(1, 'Village', 0, 'Village', 0, 0, 0, 377, 8, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'Village', 0, 'Village', 0, 0, 0, 427, 9, 1, '', '', 0, 0, 0, 0, 0, 6, 3, 0, 1, 0),
(3, 'Village', 0, 'Village', 0, 0, 0, 212, 11, 1, '', '', 4, 0, 0, 0, 0, 0, 0, 2, 1, 3),
(4, 'Village', 0, 'Village', 0, 0, 0, 315, 11, 1, '', '', 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'Village', 0, 'Village', 0, 0, 0, 131, 12, 1, '', '', 0, 0, 0, 9, 1, 0, 0, 0, 0, 0),
(6, 'Village', 0, 'Village', 0, 0, 0, 571, 13, 1, '', '', 3, 0, 0, 2, 0, 3, 0, 2, 0, 0),
(7, 'Village', 0, 'Village', 0, 0, 0, 278, 14, 1, '', '', 1, 7, 0, 0, 0, 0, 0, 0, 2, 0),
(8, 'Village', 0, 'Village', 0, 0, 0, 484, 14, 1, '', '', 0, 0, 8, 0, 0, 0, 0, 0, 0, 2),
(9, 'Village', 0, 'Village', 0, 0, 0, 525, 15, 1, '', '', 0, 0, 9, 1, 0, 0, 0, 0, 0, 0),
(10, 'Village', 0, 'Village', 0, 0, 0, 613, 15, 1, '', '', 0, 0, 2, 0, 0, 0, 0, 0, 6, 2),
(11, 'Nephy', 0, 'Ville', 0, 0, 0, 166, 21, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(12, 'Village', 0, 'Village', 0, 0, 0, 235, 29, 1, '', '', 0, 0, 6, 1, 1, 2, 0, 0, 0, 0),
(13, 'Village', 0, 'Village', 0, 0, 0, 351, 29, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10),
(14, 'Village', 0, 'Village', 0, 0, 0, 86, 32, 1, '', '', 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 'Village', 0, 'Village', 0, 0, 0, 531, 33, 1, '', '', 0, 0, 0, 0, 10, 0, 0, 0, 0, 0),
(16, 'Village', 0, 'Village', 0, 0, 0, 302, 35, 1, '', '', 1, 0, 0, 0, 0, 5, 0, 3, 1, 0),
(17, 'Village', 0, 'Village', 0, 0, 0, 412, 35, 1, '', '', 0, 0, 0, 0, 1, 0, 0, 9, 0, 0),
(18, 'Village', 0, 'Village', 0, 0, 0, 189, 40, 1, '', '', 0, 1, 0, 1, 0, 0, 3, 0, 0, 5),
(19, 'Village', 0, 'Village', 0, 0, 0, 133, 42, 1, '', '', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 'Village', 0, 'Village', 0, 0, 0, 269, 42, 1, '', '', 0, 1, 7, 0, 1, 1, 0, 0, 0, 0),
(21, 'Village', 0, 'Village', 0, 0, 0, 448, 42, 1, '', '', 0, 0, 0, 0, 0, 10, 0, 0, 0, 0),
(22, 'Village', 0, 'Village', 0, 0, 0, 484, 45, 1, '', '', 0, 4, 0, 0, 2, 3, 0, 0, 1, 0),
(23, 'Octobian', 0, 'Ville', 0, 0, 0, 508, 54, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(24, 'Village', 0, 'Village', 0, 0, 0, 600, 55, 1, '', '', 0, 0, 0, 0, 0, 0, 5, 0, 0, 5),
(25, 'Izandar', 0, 'Ville', 0, 0, 0, 375, 56, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(26, 'Bourg', 0, 'Bourg', 0, 0, 0, 552, 57, 1, '', '', 0, 2, 0, 0, 2, 0, 6, 0, 0, 0),
(27, 'Village', 0, 'Village', 0, 0, 0, 100, 58, 1, '', '', 5, 1, 1, 0, 0, 0, 0, 0, 3, 0),
(28, 'Village', 0, 'Village', 0, 0, 0, 175, 60, 1, '', '', 0, 2, 0, 0, 0, 0, 8, 0, 0, 0),
(29, 'Village', 0, 'Village', 0, 0, 0, 56, 62, 1, '', '', 3, 0, 0, 0, 1, 1, 0, 0, 5, 0),
(30, 'Village', 0, 'Village', 0, 0, 0, 324, 63, 1, '', '', 0, 0, 1, 0, 0, 5, 0, 0, 3, 1),
(31, 'Bourg', 0, 'Bourg', 0, 0, 0, 385, 64, 1, '', '', 4, 0, 0, 0, 4, 0, 2, 0, 0, 0),
(32, 'Village', 0, 'Village', 0, 0, 0, 226, 65, 1, '', '', 0, 0, 5, 0, 3, 0, 0, 0, 0, 2),
(33, 'Village', 0, 'Village', 0, 0, 0, 351, 70, 1, '', '', 3, 0, 6, 0, 0, 1, 0, 0, 0, 0),
(34, 'Village', 0, 'Village', 0, 0, 0, 438, 70, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 6, 4),
(35, 'Village', 0, 'Village', 0, 0, 0, 279, 73, 1, '', '', 7, 0, 3, 0, 0, 0, 0, 0, 0, 0),
(36, 'Bourg', 0, 'Bourg', 0, 0, 0, 486, 76, 1, '', '', 0, 8, 0, 0, 1, 0, 2, 0, 0, 0),
(37, 'Village', 0, 'Village', 0, 0, 0, 526, 77, 1, '', '', 0, 0, 0, 1, 0, 2, 0, 7, 0, 0),
(38, 'Village', 0, 'Village', 0, 0, 0, 551, 79, 1, '', '', 0, 1, 0, 4, 5, 0, 0, 0, 0, 0),
(39, 'Bourg', 0, 'Bourg', 0, 0, 0, 112, 84, 1, '', '', 3, 4, 2, 1, 1, 0, 0, 0, 0, 0),
(40, 'Village', 0, 'Village', 0, 0, 0, 158, 86, 1, '', '', 1, 0, 0, 0, 0, 8, 0, 0, 0, 1),
(41, 'Village', 0, 'Village', 0, 0, 0, 191, 88, 1, '', '', 1, 1, 0, 6, 0, 0, 0, 1, 0, 1),
(42, 'Village', 0, 'Village', 0, 0, 0, 236, 92, 1, '', '', 0, 0, 0, 1, 0, 0, 0, 9, 0, 0),
(43, 'Village', 0, 'Village', 0, 0, 0, 310, 93, 1, '', '', 0, 0, 6, 0, 0, 0, 0, 0, 0, 4),
(44, 'Village', 0, 'Village', 0, 0, 0, 26, 95, 1, '', '', 0, 4, 0, 0, 0, 0, 1, 0, 0, 5),
(45, 'Bourg', 0, 'Bourg', 0, 0, 0, 610, 95, 1, '', '', 0, 0, 0, 8, 1, 0, 0, 0, 2, 0),
(46, 'Bourg', 0, 'Bourg', 0, 0, 0, 389, 98, 1, '', '', 0, 0, 5, 1, 1, 0, 0, 4, 0, 0),
(47, 'Village', 0, 'Village', 0, 0, 0, 424, 98, 1, '', '', 0, 3, 1, 1, 5, 0, 0, 0, 0, 0),
(48, 'Village', 0, 'Village', 0, 0, 0, 85, 101, 1, '', '', 0, 1, 0, 1, 0, 0, 5, 0, 0, 3),
(49, 'Village', 0, 'Village', 0, 0, 0, 349, 101, 1, '', '', 0, 0, 0, 1, 0, 0, 0, 9, 0, 0),
(50, 'Village', 0, 'Village', 0, 0, 0, 467, 101, 1, '', '', 0, 0, 0, 9, 0, 1, 0, 0, 0, 0),
(51, 'Village', 0, 'Village', 0, 0, 0, 561, 101, 1, '', '', 0, 0, 0, 5, 2, 0, 3, 0, 0, 0),
(52, 'Village', 0, 'Village', 0, 0, 0, 286, 114, 1, '', '', 0, 0, 0, 4, 0, 4, 0, 0, 2, 0),
(53, 'Village', 0, 'Village', 0, 0, 0, 121, 115, 1, '', '', 0, 0, 0, 0, 4, 0, 6, 0, 0, 0),
(54, 'Village', 0, 'Village', 0, 0, 0, 174, 115, 1, '', '', 0, 0, 0, 0, 4, 0, 6, 0, 0, 0),
(55, 'Village', 0, 'Village', 0, 0, 0, 51, 116, 1, '', '', 0, 0, 8, 0, 0, 0, 1, 0, 0, 1),
(56, 'Village', 0, 'Village', 0, 0, 0, 335, 120, 1, '', '', 3, 0, 0, 3, 1, 0, 3, 0, 0, 0),
(57, 'Village', 0, 'Village', 0, 0, 0, 206, 123, 1, '', '', 0, 9, 0, 1, 0, 0, 0, 0, 0, 0),
(58, 'Village', 0, 'Village', 0, 0, 0, 513, 123, 1, '', '', 0, 7, 0, 0, 3, 0, 0, 0, 0, 0),
(59, 'Village', 0, 'Village', 0, 0, 0, 385, 125, 1, '', '', 0, 0, 0, 0, 1, 0, 6, 3, 0, 0),
(60, 'Village', 0, 'Village', 0, 0, 0, 577, 125, 1, '', '', 4, 0, 6, 0, 0, 0, 0, 0, 0, 0),
(61, 'Village', 0, 'Village', 0, 0, 0, 618, 129, 1, '', '', 0, 0, 0, 3, 0, 4, 0, 0, 3, 0),
(62, 'Village', 0, 'Village', 0, 0, 0, 454, 132, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 10, 0),
(63, 'Bourg', 0, 'Bourg', 0, 0, 0, 252, 133, 1, '', '', 0, 0, 0, 0, 1, 3, 0, 0, 0, 7),
(64, 'Village', 0, 'Village', 0, 0, 0, 407, 135, 1, '', '', 0, 1, 4, 0, 0, 2, 0, 0, 0, 3),
(65, 'Landar', 0, 'Ville', 0, 0, 0, 100, 141, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(66, 'Village', 0, 'Village', 0, 0, 0, 135, 143, 1, '', '', 0, 0, 0, 1, 2, 0, 2, 5, 0, 0),
(67, 'Village', 0, 'Village', 0, 0, 0, 171, 146, 1, '', '', 0, 0, 0, 0, 0, 0, 10, 0, 0, 0),
(68, 'Village', 0, 'Village', 0, 0, 0, 298, 147, 1, '', '', 7, 0, 0, 3, 0, 0, 0, 0, 0, 0),
(69, 'Bourg', 0, 'Bourg', 0, 0, 0, 589, 150, 1, '', '', 0, 0, 0, 0, 1, 0, 0, 0, 8, 2),
(70, 'Bourg', 0, 'Bourg', 0, 0, 0, 532, 151, 1, '', '', 0, 1, 0, 0, 1, 0, 0, 1, 0, 8),
(71, 'Village', 0, 'Village', 0, 0, 0, 50, 153, 1, '', '', 0, 0, 10, 0, 0, 0, 0, 0, 0, 0),
(72, 'Village', 0, 'Village', 0, 0, 0, 215, 153, 1, '', '', 6, 0, 0, 4, 0, 0, 0, 0, 0, 0),
(73, 'Village', 0, 'Village', 0, 0, 0, 68, 156, 1, '', '', 0, 0, 0, 7, 0, 0, 3, 0, 0, 0),
(74, 'Village', 0, 'Village', 0, 0, 0, 433, 156, 1, '', '', 6, 0, 0, 0, 1, 0, 0, 2, 1, 0),
(75, 'Village', 0, 'Village', 0, 0, 0, 245, 157, 1, '', '', 6, 0, 0, 0, 0, 4, 0, 0, 0, 0),
(76, 'Village', 0, 'Village', 0, 0, 0, 383, 157, 1, '', '', 7, 0, 0, 0, 0, 0, 0, 0, 3, 0),
(77, 'Bourg', 0, 'Bourg', 0, 0, 0, 324, 164, 1, '', '', 0, 7, 3, 0, 1, 0, 0, 0, 0, 0),
(78, 'Village', 0, 'Village', 0, 0, 0, 120, 169, 1, '', '', 0, 0, 0, 0, 0, 4, 1, 0, 0, 5),
(79, 'Village', 0, 'Village', 0, 0, 0, 183, 171, 1, '', '', 0, 0, 0, 10, 0, 0, 0, 0, 0, 0),
(80, 'Village', 0, 'Village', 0, 0, 0, 480, 171, 1, '', '', 3, 4, 0, 0, 1, 0, 0, 2, 0, 0),
(81, 'Village', 0, 'Village', 0, 0, 0, 143, 179, 1, '', '', 2, 0, 0, 0, 0, 0, 0, 0, 0, 8),
(82, 'Krima', 0, 'Ville', 0, 0, 0, 291, 179, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(83, 'Village', 0, 'Village', 0, 0, 0, 353, 179, 1, '', '', 0, 4, 0, 6, 0, 0, 0, 0, 0, 0),
(84, 'Village', 0, 'Village', 0, 0, 0, 551, 180, 1, '', '', 0, 0, 0, 8, 1, 0, 0, 0, 0, 1),
(85, 'Bourg', 0, 'Bourg', 0, 0, 0, 276, 189, 1, '', '', 5, 0, 5, 0, 1, 0, 0, 0, 0, 0),
(86, 'Village', 0, 'Village', 0, 0, 0, 516, 189, 1, '', '', 1, 0, 0, 0, 0, 0, 6, 3, 0, 0),
(87, 'Village', 0, 'Village', 0, 0, 0, 395, 190, 1, '', '', 9, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(88, 'Village', 0, 'Village', 0, 0, 0, 166, 194, 1, '', '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 3),
(89, 'Bourg', 0, 'Bourg', 0, 0, 0, 90, 197, 1, '', '', 0, 0, 0, 2, 1, 0, 2, 1, 5, 0),
(90, 'Village', 0, 'Village', 0, 0, 0, 459, 197, 1, '', '', 0, 0, 0, 5, 0, 0, 0, 2, 1, 2),
(91, 'Village', 0, 'Village', 0, 0, 0, 201, 202, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 10, 0),
(92, 'Village', 0, 'Village', 0, 0, 0, 238, 203, 1, '', '', 0, 5, 2, 0, 0, 0, 0, 0, 3, 0),
(93, 'Village', 0, 'Village', 0, 0, 0, 126, 206, 1, '', '', 0, 8, 0, 0, 1, 0, 0, 0, 0, 1),
(94, 'Village', 0, 'Village', 0, 0, 0, 328, 208, 1, '', '', 1, 0, 0, 0, 5, 0, 1, 3, 0, 0),
(95, 'Village', 0, 'Village', 0, 0, 0, 292, 212, 1, '', '', 1, 0, 0, 0, 0, 0, 0, 7, 2, 0),
(96, 'Bourg', 0, 'Bourg', 0, 0, 0, 403, 215, 1, '', '', 0, 0, 8, 0, 1, 0, 2, 0, 0, 0),
(97, 'Village', 0, 'Village', 0, 0, 0, 524, 219, 1, '', '', 0, 1, 2, 7, 0, 0, 0, 0, 0, 0),
(98, 'Village', 0, 'Village', 0, 0, 0, 554, 221, 1, '', '', 0, 0, 2, 4, 0, 0, 0, 0, 0, 4),
(99, 'Village', 0, 'Village', 0, 0, 0, 474, 223, 1, '', '', 0, 0, 0, 5, 5, 0, 0, 0, 0, 0),
(100, 'Village', 0, 'Village', 0, 0, 0, 174, 225, 1, '', '', 0, 2, 0, 3, 5, 0, 0, 0, 0, 0),
(101, 'Village', 0, 'Village', 0, 0, 0, 363, 225, 1, '', '', 0, 0, 0, 0, 6, 0, 0, 4, 0, 0),
(102, 'Village', 0, 'Village', 0, 0, 0, 126, 230, 1, '', '', 2, 6, 1, 0, 0, 1, 0, 0, 0, 0),
(103, 'Djin', 0, 'Ville', 0, 0, 0, 447, 230, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(104, 'Village', 0, 'Village', 0, 0, 0, 85, 240, 1, '', '', 0, 0, 8, 0, 0, 0, 2, 0, 0, 0),
(105, 'Bourg', 0, 'Bourg', 0, 0, 0, 253, 240, 1, '', '', 0, 0, 0, 0, 1, 4, 5, 0, 1, 0),
(106, 'Bourg', 0, 'Bourg', 0, 0, 0, 512, 240, 1, '', '', 0, 5, 0, 0, 1, 0, 0, 0, 0, 5),
(107, 'Pilier', 0, '', 0, 0, 0, 617, 241, 1, '', '', 0, 0, 0, 2, 7, 0, 0, 0, 1, 0),
(108, 'Village', 0, 'Village', 0, 0, 0, 214, 244, 1, '', '', 0, 0, 2, 1, 0, 0, 7, 0, 0, 0),
(109, 'Village', 0, 'Village', 0, 0, 0, 297, 248, 1, '', '', 0, 0, 0, 1, 0, 0, 0, 4, 0, 5),
(110, 'Village', 0, 'Village', 0, 0, 0, 340, 249, 1, '', '', 0, 9, 0, 1, 0, 0, 0, 0, 0, 0),
(111, 'Pilier', 0, '', 0, 0, 0, 610, 249, 1, '', '', 0, 9, 0, 0, 0, 0, 1, 0, 0, 0),
(112, 'Pilier', 0, '', 0, 0, 0, 617, 249, 1, '', '', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(113, 'Bourg', 0, 'Bourg', 0, 0, 0, 155, 251, 1, '', '', 0, 0, 3, 0, 1, 0, 7, 0, 0, 0),
(114, 'Village', 0, 'Village', 0, 0, 0, 381, 256, 1, '', '', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(115, 'Bourg', 0, 'Bourg', 0, 0, 0, 444, 259, 1, '', '', 0, 0, 0, 0, 1, 2, 0, 4, 0, 4),
(116, 'Village', 0, 'Village', 0, 0, 0, 186, 267, 1, '', '', 0, 1, 0, 0, 0, 9, 0, 0, 0, 0),
(117, 'Pilier', 0, '', 0, 0, 0, 603, 267, 1, '', '', 0, 0, 0, 0, 2, 0, 0, 3, 5, 0),
(118, 'Pilier', 0, '', 0, 0, 0, 612, 267, 1, '', '', 0, 0, 9, 0, 0, 0, 0, 1, 0, 0),
(119, 'Village', 0, 'Village', 0, 0, 0, 58, 268, 1, '', '', 0, 0, 5, 0, 0, 0, 1, 0, 0, 4),
(120, 'Village', 0, 'Village', 0, 0, 0, 99, 271, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 2, 6, 2),
(121, 'Village', 0, 'Village', 0, 0, 0, 503, 273, 1, '', '', 0, 8, 0, 0, 0, 0, 1, 0, 1, 0),
(122, 'Pilier', 0, '', 0, 0, 0, 603, 276, 1, '', '', 0, 0, 2, 0, 0, 0, 0, 0, 0, 8),
(123, 'Pilier', 0, '', 0, 0, 0, 612, 276, 1, '', '', 0, 0, 0, 0, 3, 7, 0, 0, 0, 0),
(124, 'Bourg', 0, 'Bourg', 0, 0, 0, 255, 277, 1, '', '', 0, 0, 1, 0, 1, 0, 9, 0, 0, 0),
(125, 'Village', 0, 'Village', 0, 0, 0, 367, 277, 1, '', '', 0, 1, 8, 0, 0, 0, 1, 0, 0, 0),
(126, 'Village', 0, 'Village', 0, 0, 0, 419, 281, 1, '', '', 0, 0, 1, 8, 0, 0, 0, 0, 1, 0),
(127, 'Village', 0, 'Village', 0, 0, 0, 149, 283, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 3, 0, 7),
(128, 'Village', 0, 'Village', 0, 0, 0, 559, 283, 1, '', '', 7, 0, 1, 0, 0, 2, 0, 0, 0, 0),
(129, 'Village', 0, 'Village', 0, 0, 0, 281, 291, 1, '', '', 0, 1, 0, 0, 0, 0, 4, 5, 0, 0),
(130, 'Village', 0, 'Village', 0, 0, 0, 307, 295, 1, '', '', 0, 0, 0, 5, 0, 4, 0, 0, 1, 0),
(131, 'Village', 0, 'Village', 0, 0, 0, 64, 296, 1, '', '', 0, 0, 0, 0, 2, 7, 0, 0, 0, 1),
(132, 'Village', 0, 'Village', 0, 0, 0, 529, 297, 1, '', '', 0, 7, 3, 0, 0, 0, 0, 0, 0, 0),
(133, 'Village', 0, 'Village', 0, 0, 0, 115, 300, 1, '', '', 0, 0, 0, 0, 0, 3, 2, 4, 0, 1),
(134, 'Village', 0, 'Village', 0, 0, 0, 212, 300, 1, '', '', 3, 0, 1, 0, 0, 6, 0, 0, 0, 0),
(135, 'Earok', 0, 'Ville', 0, 0, 0, 252, 301, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(136, 'Village', 0, 'Village', 0, 0, 0, 350, 310, 1, '', '', 6, 0, 0, 0, 0, 0, 0, 0, 0, 4),
(137, 'Village', 0, 'Village', 0, 0, 0, 455, 310, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 6, 4, 0),
(138, 'Village', 0, 'Village', 0, 0, 0, 156, 316, 1, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10),
(139, 'Village', 0, 'Village', 0, 0, 0, 387, 316, 1, '', '', 9, 0, 0, 0, 0, 0, 1, 0, 0, 0),
(140, 'Bourg', 0, 'Bourg', 0, 0, 0, 553, 317, 1, '', '', 0, 3, 0, 0, 7, 0, 0, 0, 0, 0),
(141, 'Village', 0, 'Village', 0, 0, 0, 185, 320, 1, '', '', 2, 0, 0, 0, 0, 8, 0, 0, 0, 0),
(142, 'Village', 0, 'Village', 0, 0, 0, 259, 329, 1, '', '', 0, 2, 0, 0, 0, 7, 0, 1, 0, 0),
(143, 'Bourg', 0, 'Bourg', 0, 0, 0, 303, 329, 1, '', '', 0, 0, 0, 9, 1, 1, 0, 0, 0, 0),
(144, 'Village', 0, 'Village', 0, 0, 0, 35, 330, 1, '', '', 4, 0, 2, 0, 4, 0, 0, 0, 0, 0),
(145, 'Village', 0, 'Village', 0, 0, 0, 424, 330, 1, '', '', 0, 3, 6, 1, 0, 0, 0, 0, 0, 0),
(146, 'Bourg', 0, 'Bourg', 0, 0, 0, 131, 332, 1, '', '', 0, 0, 0, 0, 1, 3, 0, 0, 0, 7),
(147, 'Bourg', 0, 'Bourg', 0, 0, 0, 358, 336, 1, '', '', 1, 5, 0, 0, 1, 4, 0, 0, 0, 0),
(148, 'Village', 0, 'Village', 0, 0, 0, 395, 338, 1, '', '', 0, 0, 3, 2, 0, 3, 0, 2, 0, 0),
(149, 'Bourg', 0, 'Bourg', 0, 0, 0, 224, 344, 1, '', '', 0, 0, 1, 0, 1, 1, 8, 0, 0, 0),
(150, 'Bourg', 0, 'Bourg', 0, 0, 0, 453, 345, 1, '', '', 1, 0, 0, 1, 1, 0, 0, 8, 0, 0),
(151, 'Tonak', 0, 'Ville', 0, 0, 0, 565, 347, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(152, 'Village', 0, 'Village', 0, 0, 0, 279, 354, 1, '', '', 0, 0, 4, 0, 0, 0, 0, 5, 0, 1),
(153, 'Village', 0, 'Village', 0, 0, 0, 42, 363, 1, '', '', 0, 0, 1, 0, 3, 1, 3, 0, 0, 2),
(154, 'Village', 0, 'Village', 0, 0, 0, 391, 364, 1, '', '', 0, 1, 0, 0, 3, 0, 0, 6, 0, 0),
(155, 'Village', 0, 'Village', 0, 0, 0, 327, 365, 1, '', '', 4, 1, 0, 3, 0, 1, 0, 0, 1, 0),
(156, 'Village', 0, 'Village', 0, 0, 0, 178, 369, 1, '', '', 0, 7, 2, 0, 0, 0, 0, 0, 0, 1),
(157, 'Village', 0, 'Village', 0, 0, 0, 239, 372, 1, '', '', 0, 0, 0, 0, 0, 0, 10, 0, 0, 0),
(158, 'Dust', 0, 'Ville', 0, 0, 0, 130, 377, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(159, 'Artasse', 0, 'Ville', 0, 0, 0, 468, 375, 1, '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(160, 'Village', 0, 'Village', 0, 0, 0, 278, 380, 1, '', '', 0, 0, 0, 6, 0, 0, 0, 0, 2, 2),
(161, 'Bourg', 0, 'Bourg', 0, 0, 0, 355, 382, 1, '', '', 6, 0, 3, 0, 1, 0, 0, 0, 0, 1),
(162, 'Village', 0, 'Village', 0, 0, 0, 423, 385, 1, '', '', 0, 0, 0, 0, 0, 1, 0, 0, 8, 1),
(163, 'Village', 0, 'Village', 0, 0, 0, 157, 386, 1, '', '', 0, 9, 0, 0, 1, 0, 0, 0, 0, 0),
(164, 'Village', 0, 'Village', 0, 0, 0, 304, 389, 1, '', '', 0, 0, 0, 0, 0, 3, 0, 0, 0, 7);

