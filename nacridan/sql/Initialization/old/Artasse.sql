-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:06
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(66, 'Temple', 14, 159, 1, 1, 0, '', 100, 100, 468, 375, 1),
(67, 'Échoppe', 5, 159, 1, 1, 0, '', 100, 100, 469, 376, 1),
(68, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 469, 377, 1),
(69, 'École des Métiers', 8, 159, 1, 1, 0, '', 100, 100, 468, 372, 1),
(70, 'École de Magie', 7, 159, 1, 1, 0, '', 100, 100, 468, 373, 1),
(71, 'École de Combat', 6, 159, 1, 1, 0, '', 100, 100, 467, 373, 1),
(72, 'Guilde des Artisans', 9, 159, 1, 1, 0, '', 100, 100, 470, 374, 1),
(73, 'Banque', 2, 159, 1, 1, 0, '', 100, 100, 466, 377, 1),
(74, 'Comptoir Commercial', 4, 159, 1, 1, 0, '', 100, 100, 465, 377, 1),
(75, 'Caserne', 3, 159, 1, 1, 0, '', 100, 100, 465, 375, 1),
(76, 'Prison', 13, 159, 1, 1, 0, '', 100, 100, 464, 375, 1),
(77, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 471, 373, 1),
(78, 'Palais Royal', 12, 159, 1, 1, 0, '', 100, 100, 466, 375, 1),
(79, 'Auberge', 1, 159, 1, 1, 0, '', 100, 100, 465, 378, 1),
(80, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 468, 377, 1),
(81, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 470, 373, 1),
(82, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 464, 377, 1),
(83, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 466, 373, 1),
(84, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 467, 372, 1),
(85, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 468, 371, 1),
(86, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 470, 376, 1),
(87, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 464, 378, 1),
(88, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 467, 379, 1),
(89, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 471, 374, 1),
(90, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 470, 371, 1),
(91, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 471, 371, 1),
(92, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 472, 371, 1),
(93, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 472, 373, 1),
(94, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 472, 374, 1),
(95, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 471, 376, 1),
(96, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 470, 377, 1),
(97, 'Maison', 10, 159, 1, 1, 0, '', 100, 100, 468, 379, 1),
(98, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 473, 372, 1),
(99, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 473, 371, 1),
(100, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 473, 373, 1),
(101, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 465, 373, 1),
(102, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 466, 372, 1),
(103, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 464, 374, 1),
(104, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 467, 371, 1),
(105, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 463, 376, 1),
(106, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 463, 377, 1),
(107, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 463, 378, 1),
(108, 'Rempart', 16, 159, 1, 1, 0, '', 150, 150, 473, 374, 1),
(109, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 469, 379, 1),
(110, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 470, 378, 1),
(111, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 471, 377, 1),
(112, 'Rempart', 17, 159, 1, 1, 0, '', 150, 150, 472, 376, 1),
(113, 'Rempart', 18, 159, 1, 1, 0, '', 150, 150, 470, 370, 1),
(114, 'Rempart', 18, 159, 1, 1, 0, '', 150, 150, 471, 370, 1),
(115, 'Rempart', 18, 159, 1, 1, 0, '', 150, 150, 472, 370, 1),
(116, 'Rempart', 19, 159, 1, 1, 0, '', 150, 150, 468, 380, 1),
(117, 'Rempart', 20, 159, 1, 1, 0, '', 150, 150, 473, 370, 1),
(118, 'Porte Ouverte', 26, 159, 1, 1, 0, '', 150, 150, 469, 370, 1),
(119, 'Rempart', 22, 159, 1, 1, 0, '', 150, 150, 473, 375, 1),
(120, 'Rempart', 23, 159, 1, 1, 0, '', 150, 150, 468, 370, 1),
(121, 'Rempart', 24, 159, 1, 1, 0, '', 150, 150, 463, 375, 1);
