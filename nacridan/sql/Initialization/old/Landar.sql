-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 01 Juillet 2012 à 18:22
-- Version du serveur: 5.1.63
-- Version de PHP: 5.3.2-1ubuntu4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `nacridanV2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Building`
--

CREATE TABLE IF NOT EXISTS `Building` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT 'Mur',
  `id_BasicBuilding` int(11) NOT NULL,
  `id_City` bigint(20) NOT NULL,
  `id_Player` bigint(20) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `value` smallint(4) NOT NULL DEFAULT '0',
  `school` text NOT NULL,
  `sp` smallint(6) NOT NULL,
  `currsp` smallint(6) NOT NULL,
  `x` smallint(6) NOT NULL,
  `y` smallint(6) NOT NULL,
  `map` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1607 ;

--
-- Contenu de la table `Building`
--

INSERT INTO `Building` (`id`, `name`, `id_BasicBuilding`, `id_City`, `id_Player`, `level`, `value`, `school`, `sp`, `currsp`, `x`, `y`, `map`) VALUES
(373, 'Temple', 14, 65, 1, 1, 0, '', 100, 100, 100, 141, 1),
(374, 'Échoppe', 5, 65, 1, 1, 0, '', 100, 100, 96, 143, 1),
(375, 'Auberge', 1, 65, 1, 1, 0, '', 100, 100, 97, 144, 1),
(376, 'Auberge', 1, 65, 1, 1, 0, '', 100, 100, 98, 143, 1),
(377, 'Guilde des Artisans', 9, 65, 1, 1, 0, '', 100, 100, 98, 141, 1),
(378, 'École de Magie', 7, 65, 1, 1, 0, '', 100, 100, 98, 139, 1),
(379, 'École de Combat', 6, 65, 1, 1, 0, '', 100, 100, 102, 139, 1),
(380, 'École des Métiers', 8, 65, 1, 1, 0, '', 100, 100, 103, 139, 1),
(381, 'Comptoir Commercial', 4, 65, 1, 1, 0, '', 100, 100, 102, 141, 1),
(382, 'Banque', 2, 65, 1, 1, 0, '', 100, 100, 103, 141, 1),
(383, 'Caserne', 3, 65, 1, 1, 0, '', 100, 100, 100, 138, 1),
(384, 'Prison', 13, 65, 1, 1, 0, '', 100, 100, 101, 138, 1),
(385, 'Palais Royal', 12, 65, 1, 1, 0, '', 100, 100, 100, 139, 1),
(386, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 100, 143, 1),
(387, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 99, 144, 1),
(388, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 97, 142, 1),
(389, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 97, 141, 1),
(390, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 103, 138, 1),
(391, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 96, 142, 1),
(392, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 96, 141, 1),
(393, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 99, 145, 1),
(394, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 100, 144, 1),
(395, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 100, 145, 1),
(396, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 102, 142, 1),
(397, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 102, 143, 1),
(398, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 103, 142, 1),
(399, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 104, 141, 1),
(400, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 100, 137, 1),
(401, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 101, 137, 1),
(402, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 102, 137, 1),
(403, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 104, 138, 1),
(404, 'Maison', 10, 65, 1, 1, 0, '', 100, 100, 104, 139, 1),
(405, 'Porte Ouverte', 26, 65, 1, 1, 0, '', 150, 150, 97, 146, 1),
(406, 'Rempart', 18, 65, 1, 1, 0, '', 150, 150, 96, 146, 1),
(407, 'Rempart', 18, 65, 1, 1, 0, '', 150, 150, 98, 146, 1),
(408, 'Rempart', 18, 65, 1, 1, 0, '', 150, 150, 99, 146, 1),
(409, 'Rempart', 19, 65, 1, 1, 0, '', 150, 150, 100, 146, 1),
(410, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 101, 145, 1),
(411, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 102, 144, 1),
(412, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 103, 143, 1),
(413, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 104, 142, 1),
(414, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 104, 142, 1),
(415, 'Rempart', 22, 65, 1, 1, 0, '', 150, 150, 105, 141, 1),
(416, 'Rempart', 16, 65, 1, 1, 0, '', 150, 150, 105, 140, 1),
(417, 'Rempart', 16, 65, 1, 1, 0, '', 150, 150, 105, 139, 1),
(418, 'Rempart', 16, 65, 1, 1, 0, '', 150, 150, 105, 138, 1),
(419, 'Rempart', 16, 65, 1, 1, 0, '', 150, 150, 105, 137, 1),
(420, 'Rempart', 20, 65, 1, 1, 0, '', 150, 150, 105, 136, 1),
(421, 'Porte Ouverte', 26, 65, 1, 1, 0, '', 150, 150, 104, 136, 1),
(422, 'Rempart', 18, 65, 1, 1, 0, '', 150, 150, 103, 136, 1),
(423, 'Rempart', 18, 65, 1, 1, 0, '', 150, 150, 102, 136, 1),
(424, 'Rempart', 18, 65, 1, 1, 0, '', 150, 150, 101, 136, 1),
(425, 'Rempart', 23, 65, 1, 1, 0, '', 150, 150, 100, 136, 1),
(426, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 99, 137, 1),
(427, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 98, 138, 1),
(428, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 97, 139, 1),
(429, 'Rempart', 17, 65, 1, 1, 0, '', 150, 150, 96, 140, 1),
(430, 'Rempart', 24, 65, 1, 1, 0, '', 150, 150, 95, 141, 1),
(431, 'Rempart', 16, 65, 1, 1, 0, '', 150, 150, 95, 142, 1),
(432, 'Rempart', 16, 65, 1, 1, 0, '', 150, 150, 95, 143, 1);
