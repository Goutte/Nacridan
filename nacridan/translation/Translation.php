<?php

/**
 * Translation, static.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the BSD License.
 *
 * Copyright(c) 2004 by Xiang Wei Zhuo. 
 *
 * To contact the author write to {@link mailto:qiang.xue@gmail.com Qiang Xue}
 * The latest version of PRADO can be obtained from:
 * {@link http://prado.sourceforge.net/}
 *
 * @author Xiang Wei Zhuo <weizhuo[at]gmail[dot]com>
 * @version $Revision: 1.8 $  $Date: 2005/12/15 07:14:49 $
 * @package System.I18N
 */

/**
 * Get the MessageFormat class.
 */
require_once (dirname(__FILE__) . '/MessageFormat.php');

/**
 * Translation class.
 *
 * Provides translation using a static MessageFormatter.
 *
 * @author Xiang Wei Zhuo <weizhuo[at]gmail[dot]com>
 * @version v1.0, last update on Tue Dec 28 11:54:48 EST 2004
 * @package System.I18N
 */
class Translation
{

    /**
     * The string formatter.
     * This is a class static variable.
     *
     * @var MessageFormat
     */
    private static $autosave;

    private static $catalogue;

    private static $charset;

    protected static $formatter;

    /**
     * Initialize the TTranslate translation components
     */
    public static function init($type, $source, $culture, $charset, $catalogue, $autosave, $cache = "", $filename = "")
    {
        // initialized the default class wide formatter
        if (is_null(self::$formatter)) {
            
            $source = MessageSource::factory($type, $source, $filename);
            
            $source->setCulture($culture);
            
            if ($cache != "")
                $source->setCache(new MessageCache($cache));
            
            self::$autosave = $autosave;
            self::$catalogue = $catalogue;
            self::$charset = $charset;
            self::$formatter = new MessageFormat($source, $charset);
        }
    }

    public static function getCatalogue()
    {
        if (is_null(self::$catalogue))
            return null;
        else
            return self::$catalogue;
    }

    public static function getCharset()
    {
        if (is_null(self::$charset))
            return null;
        else
            return self::$charset;
    }

    public static function isExist()
    {
        if (is_null(self::$formatter))
            return 0;
        else
            return 1;
    }

    /**
     * Get the static formatter from this component.
     *
     * @return MessageFormat formattter.
     * @see localize()
     */
    public static function formatter()
    {
        return self::$formatter;
    }

    /**
     * Save untranslated messages to the catalogue.
     */
    public static function saveMessages()
    {
        static $onceonly = true;
        
        if (! is_null(self::$formatter)) {
            if ($onceonly) {
                if (isset(self::$autosave)) {
                    $catalogue = null;
                    
                    if (isset(self::$catalogue))
                        $catalogue = self::$catalogue;
                    
                    $auto = self::$autosave;
                    if ($auto == 'true')
                        self::$formatter->getSource()->save($catalogue);
                    else {
                        self::$formatter->getSource()->setCulture('fr');
                        self::$formatter->getSource()->save($auto);
                    }
                }
            }
            $onceonly = false;
        }
    }
}

/**
 * Localize a text to the locale/culture specified in the globalization handler.
 *
 * @param
 *            string text to be localized.
 * @param
 *            array a set of parameters to substitute.
 * @param
 *            string a different catalogue to find the localize text.
 * @param
 *            string the input AND output charset.
 * @return string localized text.
 * @see TTranslate::formatter()
 * @see TTranslate::init()
 */
function localize($text, $parameters = array(), $catalogue = null, $charset = null)
{
    if ($text == "") {
        return "";
    }
    
    // $text=I18N_toUTF8($text, "ISO-8859-1");
    $params = array();
    foreach ($parameters as $key => $value)
        $params['{' . $key . '}'] = $value;
        
        // no translation handler provided
    if (! Translation::isExist()) {
        return strtr($text, $params);
    }
    
    // Translation::init();
    
    if (empty($catalogue) && Translation::getCatalogue() != "")
        $catalogue = Translation::getCatalogue();
        
        // globalization charset
    $appCharset = is_null(Translation::getCharset()) ? '' : Translation::getCharset();
    
    // default charset
    $defaultCharset = (is_null(Translation::getCharset())) ? 'UTF-8' : Translation::getCharset();
    
    // fall back
    if (empty($charset))
        $charset = $appCharset;
    if (empty($charset))
        $charset = $defaultCharset;
    
    return Translation::formatter()->format($text, $params, $catalogue, $charset);
}

?>