<?php

class DCreate extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DCreate($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        // $head->add("<script language='javascript' type='text/javascript' src='../javascript/ajax.js'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        
        if (isset($_POST["newallegiance"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                TeamFactory::createTeam($curplayer, $_POST["newallegiance"], $err, $db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        } else {
            if ($curplayer->get("id_Team") <= 0) {
                $str = "<table class='maintable centerareawidth'>\n";
                $str .= "<tr><td class='mainbgtitle'>\n";
                $str .= "<b><h1>" . localize("Créer un Ordre") . "</h1></b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                
                $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=create' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='150px'>" . localize("Entrez le nom de l'ordre :") . "</td>";
                $str .= "<td class='mainbgtitle'><input name='newallegiance' autocomplete='off' maxlength=40 size=40 name=q value=''></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
                $str .= "</table>";
                $str .= "<table width='620px'>\n";
                $str .= "<tr><td width='150px'>&nbsp;</td><td class='mainerr'><div class='mainerror'>" . $err . "</div></td></tr>";
                $str .= "</table>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $err = localize("Tant que vous ferez partie de l'Ordre : <br/> <b>{name}</b> <br/> vous ne pourrez ni créer ni rejoindre un autre Ordre.", array(
                    "name" => $curplayer->getSub("Team", "name")
                ));
            }
        }
        
        if ($err != "") {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
        }
        
        return $str;
    }
}
?>

