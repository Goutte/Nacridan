<?php

class DInvite extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DInvite($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        // $head->add("<script language='javascript' type='text/javascript' src='../javascript/ajax.js'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        $err = "";
        $type = 2;
        
        if (isset($_POST["invite"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                GdcFactory::askJoinGdc($curplayer, $_POST["invite"], $type, $err, $db);
            } else
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
        } else {
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            $str .= "<b><h1>" . localize("Inviter un joueur dans votre groupe de chasse") . "</h1></b></td>\n";
            $str .= "</tr>\n";
            $str .= "</table>\n";
            $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=invite' . "' target='_self'>\n";
            $str .= "<table class='maintable' width='620px'>\n";
            $str .= "<tr><td class='mainbgtitle' width='150px'>" . localize("Entrez le nom du joueur ou son id:") . "</td>";
            $str .= "<td class='mainbgtitle'><input name='invite' id='invite' autocomplete='off' maxlength=40 size=40 value='' onKeyUp=\"checkAlliancePJ(this.value,'alliance')\"></td><td>";
            $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
            $str .= "</table>";
            $str .= "<table width='620px'>\n";
            $str .= "<tr><td width='150px'>&nbsp;</td><td class='maintextbody'><div class='ajaxarea' id='ajaxResp'></div></td></tr>";
            $str .= "</table>";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        }
        
        if ($err != "") {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
        }
        
        return $str;
    }
}
?>

