<?php

class DTransfert extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DTransfert($nacridan, $head, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);

        $head->add("<script language='javascript' type='text/javascript' src='". Cache::get_cached_file('../javascript/ajax.js') . "'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        $err = "";
        
        if (isset($_POST["player"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                TeamFactory::transfertTeam($curplayer, $_POST["player"], $err, $db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        } else {
            if ($curplayer->get("id_Team") > 0) {
                $str .= "<table class='maintable centerareawidth'>\n";
                $str .= "<tr><td class='mainbgtitle'>\n";
                $str .= "<b><h1>" . localize("Transférer le commandement de l'Ordre") . "</h1></b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=transfert' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='150px'>" . localize("Entrez le nom du successeur :") . "</td>";
                $str .= "<td class='mainbgtitle'><input name='player' id='player' autocomplete='off' maxlength=40 size=40 value='' onKeyUp=\"checkAlliancePJ(this.value,'player')\"></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
                $str .= "</table>";
                $str .= "<table width='620px'>\n";
                $str .= "<tr><td width='150px'>&nbsp;</td><td class='maintextbody'><div class='ajaxarea' id='ajaxResp'></div></td></tr>";
                $str .= "</table>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $err = localize("Vous ne faites partie d'aucun Ordre.", array(
                    "name" => $curplayer->getSub("Team", "name")
                ));
            }
        }
        
        if ($err != "") {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
        }
        
        return $str;
    }
}
?>

