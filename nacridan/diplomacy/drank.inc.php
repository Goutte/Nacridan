<?php

class DRank extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DRank($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        // $head->add("<script language='javascript' type='text/javascript' src='../javascript/ajax.js'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        if (isset($_POST["Valid"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                TeamFactory::changeTeamGrade($curplayer, quote_smart($_POST), "rank", "check", $err, $db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($err == "") {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            
            $team = new Team();
            if (($idteam = $curplayer->get("id_Team")) == 0) {
                $str .= "<b><h1>" . localize("Vous ne faites partie d'aucun Ordre.") . "</h1></b></td></tr>\n";
            } else {
                $team->load($idteam, $db);
                $str .= "<b><h1>" . $team->get("name") . "</h1></b></td></tr>";
            }
            $str .= "</tr></table>\n";
            
            $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=rank' . "' target='_self'>\n";
            
            if ($team->get("name") != "") {
                $dbt = new DBCollection("SELECT TeamRankInfo.name, TeamRankInfo.num, Team.id_Player,TeamRankInfo.auth1,TeamRankInfo.auth2,TeamRankInfo.auth3,TeamRankInfo.auth4,TeamRankInfo.auth5,TeamRankInfo.auth6,TeamRankInfo.auth7 FROM TeamRankInfo LEFT JOIN Team ON TeamRankInfo.id_Team = Team.id LEFT JOIN TeamRank ON TeamRank.id_Player=" . $curplayer->get("id") . " WHERE Team.id=" . $curplayer->get("id_Team") . " ORDER BY TeamRankInfo.num ASC", $db);
                $arrayMember = array();
                
                $modify = 0;
                if ($dbt->get("id_Player") == $curplayer->get("id"))
                    $modify = 1;
                
                while (! $dbt->eof()) {
                    $col1 = $dbt->get("num");
                    $col2 = $dbt->get("name");
                    if ($modify)
                        $col2 = "<input name='rank" . $col1 . "' type='textbox' value=\"" . $col2 . "\"/>";
                    
                    for ($i = 1; $i <= 7; $i ++) {
                        
                        if ($dbt->get("auth" . $i) == 'Yes')
                            $extra = "checked";
                        else
                            $extra = "";
                        
                        if (! $modify)
                            $extra .= " disabled";
                        
                        $authtmp[$i - 1] = "<input name='check" . $col1 . "[]' type='checkbox' " . $extra . " value=\"" . $i . "\"/>";
                    }
                    
                    $arrayMember[] = array(
                        "0" => array(
                            $col1,
                            "class='mainbgbody' align='left'"
                        ),
                        "1" => array(
                            $col2,
                            "class='mainbgbody' align='left'"
                        ),
                        "2" => array(
                            $authtmp[0],
                            "class='mainbgbody' align='left'"
                        ),
                        "3" => array(
                            $authtmp[1],
                            "class='mainbgbody' align='left'"
                        ),
                        "4" => array(
                            $authtmp[2],
                            "class='mainbgbody' align='left'"
                        ),
                        "5" => array(
                            $authtmp[3],
                            "class='mainbgbody' align='left'"
                        ),
                        "6" => array(
                            $authtmp[4],
                            "class='mainbgbody' align='left'"
                        ),
                        "7" => array(
                            $authtmp[5],
                            "class='mainbgbody' align='left'"
                        ),
                        "8" => array(
                            $authtmp[6],
                            "class='mainbgbody' align='left'"
                        )
                    );
                    $dbt->next();
                }
                $str .= createTable(9, $arrayMember, array(
                    array(
                        localize("Les Rangs"),
                        "class='mainbgtitle' colspan=2 align='center'"
                    ),
                    array(
                        localize("Gestion de l'ordre"),
                        "class='mainbgtitle' colspan=4 align='center'"
                    ),
                    array(
                        localize("Gestion des cités (accès au palais)"),
                        "class='mainbgtitle' colspan=3 align='center'"
                    )
                ), array(
                    array(
                        localize("Num"),
                        "class='mainbglabel' width='5%' align='left'"
                    ),
                    array(
                        localize("Nom"),
                        "class='mainbglabel' width='15%' align='left'"
                    ),
                    array(
                        localize("Accepter un Demandeur"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Rejeter un Demandeur"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Bannir un Membre"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Modifier le Rang d'un Membre"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Salle de l'architecte"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Trésorerie"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Salle du gouverneur"),
                        "class='mainbglabel' width='10%' align='left'"
                    )
                ), 
                    // array(localize("Gérer l'accès à la ville"),"class='mainbglabel' width='10%' align='left'"),
                    // array(localize("Construire et améliorer un bâtiment"),"class='mainbglabel' width='10%' align='left'")),
                    "class='maintable'");
                if ($modify)
                    $str .= "<input id='Valid' type='submit' name='Valid' value='" . localize("Valider les Changements") . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        } else {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=rank' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
            $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
            $str .= "</form>";
        }
        
        return $str;
    }
}
?>