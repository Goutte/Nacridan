<?php

class DTribune extends HTMLObject
{

    public $db;
    public $curplayer;
    public $nacridan;

    public function __construct(NacridanModule $nacridan, PDO $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($this->db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $lastId = 0;
        $str = '';
        $sessPlayers = $this->nacridan->loadSessPlayers();

        // Envois du message si besoin
        if (!empty($_POST["Body"])) {
            $lastId = $this->sendMessage($_POST["Body"]);
        }

        // Changement d'informatikons
        $this->checkToChangeAuthor($sessPlayers);

        $str .= '<div id="newMessages" class="hidden"><a href="">Vous avez <span id="countMessages">0</span> nouveau<span class="plural">x</span> message<span class="plural">s</span>.</a></div>';
        $str .= '<div id="scrollable_content">';
        $str .= '<h1><b>La Tribune</b></h1>';
        $str .= '<p id="irc_explained">Un <a class="stylepc" href="http://webchat.quakenet.org/">chat</a> IRC est également à votre disposition. ';
        $str .= 'Plus de détails pour vous connecter dans ';
        $str .= '<a class="stylepc" href="' . CONFIG_HOST . '/i18n/rules/fr/rules.php?page=step2#comtools">cette section</a> ';
        $str .= 'des règles.</p>';


        $idPlayer = $this->getIdPlayer($curplayer);
        $dbinfo = new DBCollection('SELECT p.time_FighterGroupMsg_read as lastReadMsg, p.id_FighterGroup FROM Player p WHERE p.id='.$idPlayer, $db);
        $lastReadTime = gmstrtotime($dbinfo->get('lastReadMsg'));
        $lastReadDate = date("Y-m-d H:i:s", $lastReadTime);
        $idFighterGroup = $dbinfo->get('id_FighterGroup');

        // On prend tous les messages vieux de plus de `TRIBUNE_TIME_READABLE` secondes a partir de la date de dernier message non lu.
        // Maximum TRIBUNE_MAX_TIME_READABLE secondes
        $time = max($lastReadTime - TRIBUNE_TIME_READABLE, time() - TRIBUNE_MAX_TIME_READABLE);
        $cal = gmdate("Y-m-d H:i:s", $time);

        if ($idFighterGroup > 0) {
            $dbm = new DBCollection(
                "SELECT f.*, p1.id as idPlayer, p1.id_BasicRace, case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                     " then concat(p1.racename,' de ',p2.name) else p1.name end as nameModified FROM FighterGroupMsg f left outer join Player p1 on p1.id = f.id_Player left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                     ID_BASIC_RACE_FEU_FOL . ") WHERE f.date > '" . $cal . "' AND f.id_FighterGroup=" . $idFighterGroup . ' ORDER BY f.date ASC ', $db);

            new DBCollection(
                'UPDATE Player SET time_FighterGroupMsg_read="'.gmdate('Y-m-d H:i:s').'" WHERE id_FighterGroup=' . $idFighterGroup . ' and id_Member=' . $curplayer->get("id_Member") . ' ', $db,
                0, 0, false);

            $listSessionPlayersOfSameFightingGroup = $this->getListOfSessionPlayersOfSameFightingGroup($sessPlayers, $idFighterGroup);
            $tableHeaderInfos = array();
            while (! $dbm->eof()) {

                $body = $dbm->get("body");
                $date = date("Y-m-d H:i:s", gmstrtotime($dbm->get("date")));

                // To know if the message is unread
                $readClass = '';
                if ($date > $lastReadDate && $dbm->get('id') != $lastId) {
                    $readClass = ' messageUnread';
                }

                // We get all the infos of the message to render
                $infos = $this->getAuthorInfos($dbm, $listSessionPlayersOfSameFightingGroup, $date);

                $tableHeaderInfos[] = array(
                    '0' => array(
                        $infos,
                        'class="mainbglabel'.$readClass.'" align="center"'
                    ),
                    '1' => array(
                        bbcode($body),
                        'class="maintable'.$readClass.'" align="left"'
                    )
                );

                $dbm->next();
            }

            $str .= createTable(2, $tableHeaderInfos, array(),
                array(
                    array(
                        '<b>Nom</b>',
                        'class="mainbglabel" align="center"'
                    ),
                    array(
                        '<b>'.localize("Message").'</b>',
                        'class="mainbglabel" id="messageLabel" align="center"'
                    )
                ), "class='maintable centerareawidth'", "formid", "order");

            $str .= '</td></tr></table>'."\n";
            $str .= '</div>';

            $str .= '<form method="POST" action="' . CONFIG_HOST . '/diplomacy/diplomacy.php?center=tribune' . '" target="_self" id="tribune_form">'."\n";
            $str .= '<div class="inputs">';
            $str .= '<textarea id="Body" name="Body"></textarea>';
            $str .= '<button id="Send" type="submit" name="Send">' . localize('Envoyer') . '<br/><span class="shortcut">(CTRL+ENTER)</span></button>';
            $str .= '</div>';
            $str .= "</form>";
        } else {

            $str .= '<p class="important">Ceci est un chat persistant dédié aux membres d\'un même groupe de chasse.<br/>';
            $str .= 'Vous devez faire partie d\'un GdC (groupe de chasse) pour en profiter.</p>';
            $str .= '<p class="important"><b>Rappel</b> : <i>Diplomatie &#8212;> GdC &#8212;> Inviter</i></p>';
            $str .= '</div>';
        }

        return $str;
    }

    /**
     * Add the message to DB
     *
     * @param $message
     * @return array
     */
    public function sendMessage($message)
    {
        $dbtri = new DBCollection(
            "SELECT case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
            " then p2.id_FighterGroup else p1.id_FighterGroup end as id_FighterGroup FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
            ID_BASIC_RACE_FEU_FOL . ") WHERE p1.id=" . $this->curplayer->get("id"), $this->db);
        $msg = new FighterGroupMsg();
        $msg->set("id_FighterGroup", $dbtri->get("id_FighterGroup"));
        $msg->set("id_Player", $this->curplayer->get("id"));
        $msg->set("body", $message);
        $date = gmdate('Y-m-d H:i:s');
        $msg->set("date", $date);
        $lastId = $msg->addDB($this->db);
        return $lastId;
    }

    /**
     * Change author of message if authorized and if it is needed
     *
     * @param $sessPlayers
     * @return string
     */
    public function checkToChangeAuthor($sessPlayers)
    {
        $idPostToChange = filter_input(INPUT_POST, 'idPostToChange', FILTER_VALIDATE_INT);
        $changePlayer = filter_input(INPUT_POST, 'changePlayer', FILTER_VALIDATE_INT);
        if (!empty($idPostToChange) && !empty($changePlayer)) {
            $r = 'UPDATE FighterGroupMsg SET id_Player=' . $changePlayer . ' WHERE id=' . $idPostToChange . ' AND id_Player in (';
            $r .= implode(',', array_keys($sessPlayers));
            $r .= ')';
            new DBCollection($r, $this->db);
        }
    }

    /**
     * Get the player ID, or it's owner if he has one
     *
     * @return mixed
     */
    public function getIdPlayer()
    {
        if ($this->curplayer->get('id_Owner') == "0") {
            $idPlayer = $this->curplayer->get('id');
        } else {
            $idPlayer = $this->curplayer->get('id_Owner');
        }
        return $idPlayer;
    }

    /**
     * Get the infos of unreads messages
     *
     * @return array(
     *  'success' => bool,
     *  'id_FighterGroup' => int,
     *  'unreads' => int
     * )
     */
    public function getUnreadMessagesInfos() {
        // On compte tous les messages vieux de plus de `TRIBUNE_TIME_READABLE` secondes a partir de la date de dernier message non lu.
        // Maximum TRIBUNE_MAX_TIME_READABLE secondes
        $maxTime = time() - TRIBUNE_MAX_TIME_READABLE;
        $maxDate = gmdate("Y-m-d H:i:s", $maxTime);

        $idPlayer = $this->getIdPlayer();

        $request = 'SELECT p.id_FighterGroup, SUM(p.time_FighterGroupMsg_read < m.date) as unreads FROM Player p LEFT JOIN FighterGroup g ON p.id_FighterGroup=g.id
         LEFT JOIN FighterGroupMsg m ON g.id=m.id_FighterGroup
         WHERE (m.date > DATE_SUB(p.time_FighterGroupMsg_read, INTERVAL ' . TRIBUNE_TIME_READABLE . ' SECOND)
         AND m.date > "'.$maxDate.'")
         AND p.id=' . $idPlayer;
        $dbtri = new DBCollection($request, $this->db);

        $return = array(
            'success' => $dbtri->count() > 0,
            'id_FighterGroup' => $dbtri->get('id_FighterGroup'),
            'unreads' => (is_null($dbtri->get("unreads")) ? 0 : $dbtri->get("unreads"))
        );
        return $return;
    }

    /**
     * Render the HTML string for the message infos.
     *
     * @param $dbm
     * @param $listSessionPlayersOfSameFightingGroup
     * @param $date
     * @return string
     */
    public function getAuthorInfos($dbm, $listSessionPlayersOfSameFightingGroup, $date)
    {
        $posteur = $dbm->get("nameModified");
        $idPlayer = $this->getIdPlayer();

        $infos = '';
        // If the speaking player is not a monster and neither is the current player, we add a link to write to him.
        if ($dbm->get('id_BasicRace') < 100 && $this->curplayer->get('id_Owner') == "0" && $idPlayer != $dbm->get('idPlayer')) {
            $infos .= Envelope::render($posteur);
        }

        if ($dbm->get('idPlayer') == null) {
            $infos .= '<i>Un Disparu</i>';
        } else {
            $infos .= '<a href="../conquest/profile.php?id=' . $dbm->get('idPlayer') . '" class="popupify name">' . $posteur . '</a>';
        }
        if (count($listSessionPlayersOfSameFightingGroup) > 1
            && array_key_exists($dbm->get('idPlayer'), $listSessionPlayersOfSameFightingGroup)
        ) {
            // Si le message à été écrit il y a moins de 5minutes
            $infos .= '<a href="#" class="changePers" title="Changer l\'auteur de ce message" alt="Changer l\'auteur de ce message">';
            $infos .= '<img src="../pics/misc/change.png"/></a>';
            $infos .= '<form class="changePersForm hidden" method="POST" action="" target="_self">';
            $infos .= '<select name="changePlayer">';
            foreach ($listSessionPlayersOfSameFightingGroup as $id => $name) {
                $infos .= '<option value="' . $id . '">' . $name . '</option>';
            }
            $infos .= '</select>';
            $infos .= '<input type="hidden" name="idPostToChange" value="' . $dbm->get("id") . '">';
            $infos .= '<button type="submit" class="changePlayer">Changer</button>';
            $infos .= '<a href="#" class="cancel"><img src="../pics/misc/redcross.gif"/></a>';
            $infos .= '</form>';
        }
        $infos .= '<br/>';
        $infos .= '<i class="date">' . $date . '</i>';
        return $infos;
    }

    /**
     * @param $sessPlayers
     * @param $db
     * @param $idFighterGroup
     * @return array
     */
    public function getListOfSessionPlayersOfSameFightingGroup($sessPlayers, $idFighterGroup)
    {
        $listSessionPlayersOfSameFightingGroup = array();
        $r = 'SELECT p.id, p.name, p.id_FighterGroup, p.id_Owner, p.id, p.racename FROM Player p
                        WHERE p.id IN (' . implode(',', array_keys($sessPlayers)) . ')
                        AND (p.id_BasicRace < 100 OR p.id_BasicRace=' . ID_BASIC_RACE_FEU_FOL . ')';
        $dbplayers = new DBCollection($r, $this->db);
        while (!$dbplayers->eof()) {
            if ($dbplayers->get('id_FighterGroup') == $idFighterGroup || $dbplayers->get('id_Owner') != "0") {
                if ($dbplayers->get('name') != '') {
                    $name = $dbplayers->get('name');
                } else {
                    $name = $dbplayers->get('racename') . ' de ' . $sessPlayers[$dbplayers->get('id_Owner')];
                }
                $listSessionPlayersOfSameFightingGroup[$dbplayers->get('id')] = $name;
            }
            $dbplayers->next();
        }
        return $listSessionPlayersOfSameFightingGroup;
    }
}

?>