<?php

class DMemberGdC extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DMemberGdC($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        $str = "";
        
        if (isset($_POST["validate"]) || isset($_POST["Exc"])) {
            if (isset($_POST["validate"])) {
                require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                $type = 2;
                GdCFactory::banGdCMember($curplayer, explode("|", $_POST["player"]), $type, $err, $db);
            }
            
            if (isset($_POST["Exc"])) {
                if (isset($_POST["check"])) {
                    $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=memberGdC' . "' target='_self'>\n";
                    $str .= "<table class='maintable' width='620px'>\n";
                    $error = 0;
                    $where = getSQLCondFromArray(quote_smart($_POST["check"]), "id", "OR");
                    $dbt = new DBCollection("SELECT * FROM Player WHERE " . $where, $db);
                    while (! $dbt->eof()) {
                        if ($dbt->get("id") == $curplayer->getSub("FighterGroup", "id_Player")) {
                            $str .= "<tr><td class='mainbgtitle'>" . localize("Erreur : vous ne pouvez pas bannir le chef du groupe.") . "</td></tr></table>";
                            $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                            $error = 1;
                        }
                        $dbt->next();
                    }
                    
                    if (! $error) {
                        $str .= "<tr><td class='mainbgtitle' width='620px'>" . localize("Êtes vous sur de vouloir exclure les personnes sélectionnés ?") . "</td></tr></table>";
                        $str .= "<input type='submit' name='back' value='" . localize("Annuler") . "' />";
                        $str .= "<input type='submit' name='validate' value='" . localize("Valider") . "' />";
                        $str .= "<input name='player' type='hidden' value='" . implode("|", $_POST["check"]) . "' />\n";
                        $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                    }
                    $str .= "</form>";
                } else
                    $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
            }
        }
        
        if ($err == "" && $str == "") {
            $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=memberGdC' . "' target='_self'>";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            
            $curentIdFigherGroup = $curplayer->get("id_FighterGroup");
            
            if ($curplayer->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) { // si le perso est un feu fol ses droits sont vérifiés avec le propriétaire du feu fol
                $id = $curplayer->get("id_Owner");
                $playerControlingFF = new Player();
                $playerControlingFF->externDBObj("Modifier");
                $playerControlingFF->load($id, $db);
                $curentIdFigherGroup = $playerControlingFF->get("id_FighterGroup");
            }
            
            if ($curentIdFigherGroup == 0)
                $str .= "<b>" . localize("Vous ne faites partie d'aucun groupe de chasse.") . "</b></td></tr></table>\n";
            
            else {
                
                $str .= "<b><h1>" . localize("Votre groupe de chasse") . "</h1></b></td></tr>\n";
                $str .= "</tr></table>\n";
                $dbt = new DBCollection(
                    "SELECT p1.*,case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                         " then concat(p1.racename,' de ',p2.name) else p1.name end as nameModified FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                         ID_BASIC_RACE_FEU_FOL . ") WHERE p1.id_FighterGroup=" . $curentIdFigherGroup . " or p2.id_FighterGroup=" . $curentIdFigherGroup, $db);
                $arrayMember = array();
                $player = new Player();
                while (! $dbt->eof()) {
                    $sppercent = $dbt->get("currhp") * 100 / $dbt->get("hp");
                    $vie = "<table class='hp'><tr ><td align='left'  class='hpcell'><img class='hpimg'  src='" . CONFIG_HOST . "/pics/misc/pxred.jpg' width='" . $sppercent .
                         "%'></td/></tr></table>";
                    $name = '';
                    if ($dbt->get('id_BasicRace') != ID_BASIC_RACE_FEU_FOL) {
                        $name  = Envelope::render($dbt->get('nameModified'));
                    }
                    $name .= "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("nameModified") . "</a> (" .
                         $dbt->get("racename") . ")";
                    if ($dbt->get("id_BasicRace") != ID_BASIC_RACE_FEU_FOL) {
                        $checkbox = "<input type='checkbox' name='check[]' value='" . $dbt->get("id") . "'>";
                    } else {
                        $checkbox = "";
                    }
                    if ($curplayer->getObj("FighterGroup")->get("id_Player") != $curplayer->get("id"))
                        $checkbox = "&nbsp;";
                    
                    if ($dbt->get("id") == $curplayer->getObj("FighterGroup")->get("id_Player"))
                        $name .= " - Chef";
                    
                    $arrayMember[] = array(
                        "0" => array(
                            $checkbox,
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $dbt->get("level"),
                            "class='mainbgbody' align='center'"
                        ),
                        "2" => array(
                            $name,
                            "class='mainbgbody' align='left'"
                        ),
                        "3" => array(
                            $vie,
                            "class='mainbgbody' align='center'"
                        ),
                        "4" => array(
                            $dbt->get("x"),
                            "class='mainbgbody' align='right'"
                        ),
                        "5" => array(
                            $dbt->get("y"),
                            "class='mainbgbody' align='right'"
                        ),
                        "6" => array(
                            distHexa($curplayer->get("x"), $curplayer->get("y"), $dbt->get("x"), $dbt->get("y")),
                            "class='mainbgbody' align='center'"
                        )
                    );
                    $dbt->next();
                }
                $str .= createTable(7, $arrayMember, 
                    array(
                        array(
                            localize("Membres"),
                            "class='mainbgtitle' colspan=7 align='left'"
                        )
                    ), 
                    array(
                        array(
                            "",
                            "class='mainbglabel' width='4%' align='center'"
                        ),
                        array(
                            localize("Niveau"),
                            "class='mainbglabel' width='9%' align='center'"
                        ),
                        array(
                            localize("Nom"),
                            "class='mainbglabel' width='30%' align='center'"
                        ),
                        array(
                            localize("Vie"),
                            "class='mainbglabel' width='30%' align='center'"
                        ),
                        array(
                            localize("X"),
                            "class='mainbglabel' width='9%' align='center'"
                        ),
                        array(
                            localize("Y"),
                            "class='mainbglabel' width='9%' align='center'"
                        ),
                        array(
                            localize("Distance"),
                            "class='mainbglabel' width='9%' align='center'"
                        )
                    ), "class='maintable centerareawidth'");
                
                if ($curplayer->getObj("FighterGroup")->get("id_Player") == $curplayer->get("id"))
                    $str .= "<input id='Exc' type='submit' name='Exc' value='" . localize("Exclure") . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        } else {
            
            if ($str == "") {
                echo "putain3";
                $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?' . "'target='_self'>\n";
                $str .= "<table class='maintable' width='620px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='620px'>" . $err . "</td></tr></table>";
                $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            }
        }
        return $str;
    }
}
?>
