<?php

class DNewMember extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DNewMember($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
        
        // $head->add("<script language='javascript' type='text/javascript' src='../javascript/ajax.js'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        if (isset($_POST["Add"]) || isset($_POST["Del"])) {
            if (! $this->nacridan->isRepostForm()) {
                if (isset($_POST["Add"])) {
                    require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                    if (isset($_POST["check"]))
                        TeamFactory::validateJoinTeam($curplayer, quote_smart($_POST["check"]), $err, $db);
                }
                
                if (isset($_POST["Del"])) {
                    require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                    if (isset($_POST["check"]))
                        TeamFactory::refuseJoinTeam($curplayer, quote_smart($_POST["check"]), $err, $db);
                }
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        }
        
        if ($err == "") {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=newmember' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            
            $team = $curplayer->getObj("Team");
            if (($idteam = $curplayer->get("id_Team")) == 0) {
                $str .= "<b><h1>" . localize("Vous ne faites partie d'aucun Ordre.") . "</h1></b></td></tr>\n";
            } else {
                $str .= "<b><h1>" . $team->get("name") . "</h1></b></td></tr>";
            }
            $str .= "</tr></table>\n";
            
            if ($team->get("name") != "") {
                
                $dbrank = new DBCollection("SELECT TeamRankInfo.id_Team,num,auth1,auth2,auth3,auth4 FROM TeamRank LEFT JOIN TeamRankInfo ON id_TeamRankInfo=TeamRankInfo.id WHERE TeamRank.id_Player=" . $curplayer->get("id"), $db);
                
                $modify1 = 0;
                $modify2 = 0;
                if (! $dbrank->eof()) {
                    if ($dbrank->get("auth1") == "Yes") {
                        $modify1 = 1;
                    }
                    if ($dbrank->get("auth2") == "Yes") {
                        $modify2 = 1;
                    }
                }
                
                $dbt = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.level FROM Player LEFT JOIN TeamAskJoin ON TeamAskJoin.id_Player=Player.id WHERE TeamAskJoin.id_Team=" . $team->get("id"), $db);
                $arrayMember = array();
                while (! $dbt->eof()) {
                    $name = "<a href=\"../conquest/profile.php?id=" . $dbt->get("id") . "\" class='stylepc popupify'>" . $dbt->get("name") . "</a>";
                    if ($modify1 || $modify2)
                        $arrayMember[] = array(
                            "0" => array(
                                "<input type='checkbox' name='check[]' value='" . $dbt->get("id") . "'>",
                                "class='mainbgbody' align='center'"
                            ),
                            "1" => array(
                                $dbt->get("id"),
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $name,
                                "left"
                            ),
                            "3" => array(
                                $dbt->get("racename"),
                                "class='mainbgbody' align='left'"
                            ),
                            "4" => array(
                                $dbt->get("level"),
                                "class='mainbgbody' align='center'"
                            )
                        );
                    else
                        $arrayMember[] = array(
                            "0" => array(
                                " ",
                                "class='mainbgbody' align='center'"
                            ),
                            "1" => array(
                                $dbt->get("id"),
                                "class='mainbgbody' align='left'"
                            ),
                            "2" => array(
                                $name,
                                "class='mainbgbody' align='left'"
                            ),
                            "3" => array(
                                $dbt->get("racename"),
                                "class='mainbgbody' align='left'"
                            ),
                            "4" => array(
                                $dbt->get("level"),
                                "class='mainbgbody' align='center'"
                            )
                        );
                    $dbt->next();
                }
                $str .= createTable(5, $arrayMember, array(
                    array(
                        localize("Les Nouvelles Demandes"),
                        "class='mainbgtitle' colspan=5 align='left'"
                    )
                ), array(
                    array(
                        "",
                        "class='mainbglabel' width='5%' align='center'"
                    ),
                    array(
                        localize("id"),
                        "class='mainbglabel' width='10%' align='left'"
                    ),
                    array(
                        localize("Nom"),
                        "class='mainbglabel' width='40%' align='left'"
                    ),
                    array(
                        localize("Race"),
                        "class='mainbglabel' width='30%' align='left'"
                    ),
                    array(
                        localize("Niveau"),
                        "class='mainbglabel' width='15%' align='center'"
                    )
                ), "class='maintable  centerareawidth'");
                
                if ($modify1)
                    $str .= "<input id='Add' type='submit' name='Add' value='" . localize("Accepter") . "' />";
                if ($modify2)
                    $str .= "<input id='Del' type='submit' name='Del' value='" . localize("Refuser") . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
        } else {
            $str = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=newmember' . "' target='_self'>\n";
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
            $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
            $str .= "</form>";
        }
        
        return $str;
    }
}

?>