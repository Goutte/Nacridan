<?php
$parameters = "";

if (isset($_GET["center"])) {
    $parameters = "?center=" . $_GET["center"];
}

if (isset($_GET["bottom"])) {
    if (isset($_GET["center"]))
        $parameters .= "&";
    else
        $parameters .= "?";
    $parameters .= "bottom=" . $_GET["bottom"];
}

printf("<form name='form'  method='POST' action='" . CONFIG_HOST . "/diplomacy/diplomacy.php%s' target='_self'>\n", $parameters);

if (! ($cache->start($auth->auth["uid"] . $curplayer->get("id") . "CQRIGHT"))) {
    
    $players = $Conquest->loadSessPlayers();
    $playerid = $curplayer->get("id");
    $modif = $curplayer->getObj("Modifier");

    function getATBDay($curplayer)
    {
        global $Conquest;
        $time = gmstrtotime($curplayer->get("nextatb"));
        $str = date("Y-m-d H:i:s", $time);
        
        list ($day, $hour) = explode(" ", $str);
        
        return localizedday($day);
    }

    function getATBHour($curplayer)
    {
        global $Conquest;
        $time = gmstrtotime($curplayer->get("nextatb"));
        $str = date("Y-m-d H:i:s", $time);
        
        list ($day, $hour) = explode(" ", $str);
        return $hour;
    }

    function localizedday($date)
    {
        $time = gmstrtotime($date);
        
        $eng_date = date("j M Y", $time);
        $eng_date_array = explode(" ", $eng_date);
        
        $eng_date_array['num'] = $eng_date_array[0];
        unset($eng_date_array[0]);
        $eng_date_array['month'] = $eng_date_array[1];
        unset($eng_date_array[1]);
        $eng_date_array['year'] = $eng_date_array[2];
        unset($eng_date_array[2]);
        
        $local_date_array_db = array(
            'month' => array(
                'Jan' => localize("jan"),
                'Feb' => localize("feb"),
                'Mar' => localize("mar"),
                'Apr' => localize("apr"),
                'May' => localize("may"),
                'Jun' => localize("jun"),
                'Jul' => localize("jul"),
                'Aug' => localize("aug"),
                'Sep' => localize("sep"),
                'Oct' => localize("oct"),
                'Nov' => localize("nov"),
                'Dec' => localize("dec")
            )
        );
        
        $local_date_array['num'] = $eng_date_array['num'] . " ";
        
        foreach ($local_date_array_db['month'] as $eng_month => $local_month)
            if ($eng_month == $eng_date_array['month'])
                $local_date_array['month'] = $local_month . " ";
        
        $local_date_array['year'] = $eng_date_array['year'] . " ";
        
        $local_date = implode("", $local_date_array);
        return $local_date;
    }
    
    $str = "<div class='cqright'>";
    $str .= "<div class='profil'>";
    
    $str .= "<select class='cqselector' style='width: 130px;' name='__idCurPlayer' size='1' onchange='submit();'>";
    foreach ($players as $key => $val) {
        if ($key != $playerid) {
            $str .= "<option value='$key'>" . $val . "</option>";
        } else {
            $str .= "<option value='$key' selected>" . $val . "</option>";
        }
    }
    $str .= "</select>";
    
    $bm = array();
    
    foreach ($modif->m_characLabel as $label) {
        $bm[$label] = $modif->getModifStr($label . '_bm');
        if ($bm[$label] == "0") {
            $bm[$label] = "";
        }
    }
    
    $str .= "<div class='level'>&nbsp;" . localize('Niveau') . ": " . $curplayer->get('level') . "</div>\n";
    $str .= "<b>&nbsp;" . localize('PV') . ": " . $curplayer->get('currhp') . "/" . $curplayer->getModif('hp', DICE_ADD) . "</b>\n";
    
    $str .= "<table class='hp'>\n";
    $str .= "<tr>";
    $str .= "<td class='hpcell'><img class='hpimg'  src='../pics/misc/pxred.jpg' width='" . $curplayer->getHPPercent() . "%'></td>\n";
    $str .= "</tr>\n";
    $str .= "</table>\n";
    $str .= "<div class='charac1'>\n";
    $str .= "<table><tr><td>\n";
    $str .= "</td><td></td><td>&nbsp;<span class='bonus'>B</span>/<span class='malus'>M</span><tr><td>\n";
    $str .= "<b>" . localize('Att') . "</b></td><td><b>:&nbsp</b>" . $modif->getModifStr('attack') . "</td><td>&nbsp;" . $bm['attack'] . "</td></tr><tr><td>\n";
    $str .= "<b>" . localize('Esq') . "</b></td><td><b>:&nbsp</b>" . $modif->getModifStr('dodge') . "</td><td>&nbsp;" . $bm['dodge'] . "</td></tr><tr><td>\n";
    $str .= "<b>" . localize('Dég') . "</b></td><td><b>:&nbsp</b>" . $modif->getModifStr('damage') . "</td><td>&nbsp;" . $bm['damage'] . "</td></tr><tr><td>\n";
    $str .= "<b>" . localize('Rég') . "</b></td><td><b>:&nbsp</b>" . $modif->getModifStr('regen') . "</td><td>&nbsp;" . $bm['regen'] . "</td></tr><tr><td>\n";
    $str .= "<b>" . localize('Arm') . "</b></td><td><b>:&nbsp</b>" . $modif->getModifStr('armor') . "</td><td>&nbsp;" . $bm['armor'] . "</td></tr><tr><td>\n";
    $str .= "<b>" . localize('MM') . "</b></td><td><b>:&nbsp</b>" . $modif->getModifStr('magicSkill') . "</td><td>&nbsp;" . $bm['magicSkill'] . "</td></tr>\n";
    // $str.="<b>".localize('RM')."</b></td><td><b>:&nbsp</b>".$modif->getModifStr('magicResist')."</td><td>&nbsp;".$bm['magicResist']."</td></tr>\n";
    $str .= "</table>\n";
    $str .= "</div>\n";
    $str .= "</div>\n";
    
    $str .= "<div id='infozone' class='infozone'><b>" . localize('Coordonnées') . "</b>:<br/>&nbsp;<b>X</b>=" . $curplayer->get('x') . " &nbsp;/&nbsp;  <b>Y</b>=" . $curplayer->get('y') . "</div>\n";
    $str .= "<div>\n";
    $str .= "<table id='infos' class='infos'>\n";
    $str .= "<tr><td><b>\n";
    $str .= localize('PA') . "</b></td><td>:</td><td>" . $curplayer->get('ap') . "<br/></td></tr><tr><td><b>\n";
    $str .= localize('PX') . "</b></td><td>:</td><td>" . $curplayer->get('xp') . "<br/></td></tr><tr><td><b>\n";
    // $str.=localize('PI')."</b></td><td>:</td><td>".$curplayer->get('ip')."<br/></td></tr><tr><td><b>\n";
    $str .= localize('PO') . "</b></td><td>:</td><td>" . $curplayer->get('money') . "</td></tr>\n";
    $str .= "</table>\n";
    $str .= "</div>\n";
    
    $str .= "<div class='right'>" . localize('Prochaine <b>DLA</b>') . "<br/>" . getATBDay($curplayer) . "</b><br/>\n";
    $str .= "<b>" . getATBHour($curplayer) . "</b>\n";
    $str .= "</div>\n";
    
    $str .= "<a class='logout' href='../main/logout.php'>" . localize('Déconnexion') . "</a><br/>\n";
    
    $str .= "</div></form>\n";
    echo $str;
    
    $cache->end();
}
