<?php

class DTransfertGdC extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DTransfertGdC($nacridan, $head, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);

        $head->add("<script language='javascript' type='text/javascript' src='". Cache::get_cached_file('../javascript/ajax.js') . "'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        $err = "";
        
        if (isset($_POST["transfer"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/GdCFactory.inc.php");
                $type = 2;
                GdCFactory::transfertGdC($curplayer, $_POST["TARGET_ID"], $type, $err, $db);
            } else
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
        } else {
            if ($curplayer->get("id_FighterGroup") > 0) {
                $str .= "<table class='maintable centerareawidth'>\n";
                $str .= "<tr><td class='mainbgtitle'>\n";
                $str .= "<b><h1>" . localize("Transférer le commandement du groupe de chasse") . "</h1></b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=transfertgdc' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='780px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='150px'>" . localize("Choississez le nom du successeur :") . "</td></tr>";
                $str .= "<ty><td><select class='selector cqattackselectorsize' name='TARGET_ID'>";
                $str .= "<option value='0' selected='selected'>" . localize("-- Membre du GdC --") . "</option>";
                $dbe = new DBCollection("SELECT * FROM Player WHERE id_FighterGroup=" . $curplayer->get("id_FighterGroup"), $this->db, 0, 0);
                while (! $dbe->eof()) {
                    if ($dbe->get("id") != $curplayer->get("id"))
                        $item[] = array(
                            localize($dbe->get("name")) . " (" . $dbe->get("id") . ")" => $dbe->get("id")
                        );
                    $dbe->next();
                }
                
                foreach ($item as $arr) {
                    foreach ($arr as $key => $value)
                        $str .= "<option value='" . $value . "'>" . $key . "</option>";
                }
                $str .= "</select>";
                
                // $str.="<td class='mainbgtitle'><input name='player' id='player' autocomplete='off' maxlength=40 size=40 value='' onKeyUp=\"checkAlliancePJ(this.value,'player')\"></td><td>";
                $str .= "<input id='transfer' type='submit' name='transfer' value='Action' /></td></tr>";
                $str .= "</table>";
                $str .= "<table width='620px'>\n";
                $str .= "<tr><td width='150px'>&nbsp;</td><td class='maintextbody'><div class='ajaxarea' id='ajaxResp'></div></td></tr>";
                $str .= "</table>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else
                $err = localize("Vous ne faites partie d'aucun groupe de chasse.");
        }
        
        if ($err != "") {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
        }
        
        return $str;
    }
}
?>

