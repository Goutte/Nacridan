<?php

class DFortress extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DFortress($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        
        $dbt = new DBCollection("SELECT name FROM Team WHERE id=" . $curplayer->get("id_Team"), $this->db);
        if ($dbt->count() > 0) {
            $str = "<table class='maintable centerareawidth'>";
            $str .= "<tr><td class='mainbgtitle'>";
            $str .= "<b><h1>" . $dbt->get('name') . "</h1></b></td>";
            $str .= "</tr>";
            $str .= "</table>";
        }
        $str .= "<table class='maintable centerareawidth'>";
        $str .= "<tr>";
        $str .= "<td class='mainbgtitle'><b>" . localize(' F O R T E R E S S E S') . "</b>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "</table>";
        
        if ($dbt->count() > 0) {
            $dbtr = new DBCollection("SELECT * FROM City WHERE id_Player IN (SELECT id FROM Player WHERE id_Team=" . $curplayer->get("id_Team") . ")", $this->db);
        } else {
            $dbtr = new DBCollection("SELECT * FROM City WHERE id_Player =" . $curplayer->get("id") . "", $this->db);
        }
        $array = array();
        while (! $dbtr->eof()) {
            
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbtr->get("id_Player"), $db);
            $name = "<a href='../conquest/profilecity.php?id=" . $dbtr->get("id") . "' class='stylepc popupify'>" . $dbtr->get("name") . "</a>";
            $x = $dbtr->get("x");
            $y = $dbtr->get("y");
            $gouverneur = "<a href='../conquest/profile.php?id=" . $dbp->get("id") . "' class='stylepc popupify'>" . $dbp->get("name") . "</a>";
            $fortification = "non";
            if ($dbtr->get("captured") == 4)
                $fortification = "oui";
            $acces = "<u>Village:</u> ";
            switch ($dbtr->get("door")) {
                case 0:
                case 1:
                    $acces .= "Ouvert à tous";
                    break;
                case 2:
                    $acces .= "Fermé à tous";
                    break;
                case 4:
                    $acces .= "Fermé aux ennemis";
                    break;
                default:
                    $acces .= "Ouvert aux alliés";
                    break;
            }
            $acces .= "<br/><u>Temple:</u> ";
            switch ($dbtr->get("accessTemple")) {
                case 0:
                case 1:
                    $acces .= "Ouvert à tous";
                    break;
                case 2:
                    $acces .= "Fermé à tous";
                    break;
                case 4:
                    $acces .= "Fermé aux ennemis";
                    break;
                default:
                    $acces .= "Ouvert aux alliés";
                    break;
            }
            $acces .= "<br/><u>Entrepôts:</u> ";
            switch ($dbtr->get("accessEntrepot")) {
                case 0:
                case 1:
                    $acces .= "Ouvert à tous";
                    break;
                case 2:
                    $acces .= "Fermé à tous";
                    break;
                case 4:
                    $acces .= "Fermé aux ennemis";
                    break;
                default:
                    $acces .= "Ouvert aux alliés";
                    break;
            }
            
            $array[] = array(
                "0" => array(
                    $name,
                    "class='mainbgbody' align='center'"
                ),
                "1" => array(
                    $x . "-" . $y,
                    "class='mainbgbody' align='center'"
                ),
                "2" => array(
                    $gouverneur,
                    "class='mainbgbody' align='center'"
                ),
                "3" => array(
                    $fortification,
                    "class='mainbgbody' align='center'"
                ),
                "4" => array(
                    $acces,
                    "class='mainbgbody' align='left'"
                ),
                "5" => array(
                    "<a href='../conquest/inbuilding/treasuryaccount.php?id=" . $dbtr->get("id") . "' class='stylepc popupify'>" . $dbtr->get("loyalty") . "</a>",
                        "class='mainbgbody' align='center'"
                )
            );
            $dbtr->next();
        }
        
        $str .= createTable(6, $array, array(), 
            array(
                array(
                    "Village contrôlé",
                    "class='mainbglabel' width='23%' align='center'"
                ),
                array(
                    localize("Coordonnées"),
                    "class='mainbglabel' width='20%' align='center'",
                    "EQ.name",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Gouverneur"),
                    "class='mainbglabel' width='20%' align='center'",
                    "price",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Fortification"),
                    "class='mainbglabel' width='5%' align='center'",
                    "price",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Acces"),
                    "class='mainbglabel' width='27%' align='center'",
                    "price",
                    "mainbglabelhover",
                    "mainbglabel"
                ),
                array(
                    localize("Loyautée/Comptes"),
                    "class='mainbglabel' width='5%' align='center'",
                    "price",
                    "mainbglabelhover",
                    "mainbglabel"
                )
            ), "class='maintable' width='780px' ", "formid", "order");
        
        return $str;
    }
}
?>
