<?php
require_once ("../config.ini.php");
require_once (HOMEPATH . "/class/main.inc.php");
ob_start();
page_open(array(
    "sess" => "SessionMgt",
    "auth" => "AuthMgt"
));
$Conquest = new ConquestSession($sess, $auth, getDB());
$timer->setMarker('END PAGE OPEN');
require_once (HOMEPATH . "/translation/Translation.php");
$timer->setMarker('END TRANSLATION');
Translation::init('gettext', '../i18n/messages', 'fr', 'UTF-8', null, true, '../i18n/messages/cache', $filename = "");
$timer->setMarker('END LOADING DICO');
require_once (HOMEPATH . "/class/accessors.inc.php");
$timer->setMarker('END ACCESSORS');

require_once (HOMEPATH . "/class/ddef.inc.php");
$timer->setMarker('END MAIN');

require_once (HOMEPATH . "/include/header.inc.php");
$timer->setMarker('END HEADER');

$db = getDB();
$curplayer = $Conquest->loadCurSessPlayer($db);
$timer->setMarker('END LOAD CURRENT PLAYER');

require_once ('Cache/Lite/Output.php');

$options = array(
    'cacheDir' => '/tmp/',
    'lifeTime' => 600
);

$cache = new Cache_Lite_Output($options);

echo "<body>";
echo "<div class='mainarea'>";
echo "<a href='http://execbase.no-ip.org/forum/' onclick=\"window.open(this.href,'_blank');return false;\" style='color: #888888;'>Le Forum de Nacridan</a>";

if (isset($_GET["center"]))
    $center = $_GET["center"];

if (isset($_GET["bottom"]))
    $bottom = $_GET["bottom"];

echo "<div class=\"content\">";

if (isset($center)) {
    
    switch ($center) {
        case "member":
            include "../diplomacy/dmember.inc.php";
            break;
        case "join":
            include "../diplomacy/djoin.inc.php";
            break;
        case "create":
            include "../diplomacy/dcreate.inc.php";
            break;
        case "disavow":
            include "../diplomacy/ddisavow.inc.php";
            break;
        case "dissol":
            include "../diplomacy/ddissol.inc.php";
            break;
        case "newmember":
            include "../diplomacy/dnewmember.inc.php";
            break;
        case "rank":
            include "../diplomacy/drank.inc.php";
            break;
        case "alliance":
            include "../diplomacy/dalliance.inc.php";
            break;
        default:
            break;
    }
} else {
    include 'dinfo.inc.php';
}

echo "</div>";

$timer->setMarker('END CENTER/ACTION');

// if (!($cache->start($auth->auth["uid"].$curplayer->get("id")."CQHEADER"))) {
include '../main/mmenu.inc.php';
include 'dmenu.inc.php';
include 'dmenubt.inc.php';
// $cache->end();
// }
$timer->setMarker('END CQMENU');

if (isset($bottom)) {
    echo "<div class=\"contentbt\">";
    
    switch ($bottom) {
        default:
            break;
    }
    echo "</div>";
}
$timer->setMarker('END BOTTOM');

$cache->remove($auth->auth["uid"] . $curplayer->get("id") . "CQRIGHT");
include 'dright.inc.php';

$timer->setMarker('END RIGHT');

ob_end_flush();
$timer->setMarker('END FLUSH');

Translation::saveMessages();
$timer->setMarker('END SAVE MESSAGES');

page_close();
$timer->display();

echo "</div>";
echo "</body>";
echo "</html>";

