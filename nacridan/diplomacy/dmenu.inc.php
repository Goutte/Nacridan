<?php

class DMenu extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DMenu($nacridan, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        $sess = $this->nacridan->sess;
        $id = $sess->get("cq_playerid");
        
        $str = "<div class='menu'>";
        $str .= "<dl class='menuright'>";
        $limited = $curplayer->get("status") == 'NPC' ? 1 : 0;
        $dbp = new DBCollection("SELECT authlevel FROM Player WHERE id_Member=" . $curplayer->get("id_Member"), $db);
        while (! $dbp->eof()) {
            if ($dbp->get("authlevel") > 2)
                $limited = 0;
            $dbp->next();
        }
        
        if (! $limited) {
            
            if ($curplayer->get("id_Team") == 0) {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Ordre') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=create'>" . localize("Créer un Ordre ({ap} PA)", 
                    array(
                        "ap" => CREATE_TEAM_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=join'>" . localize("Rejoindre un Ordre ({ap} PA)", 
                    array(
                        "ap" => JOIN_TEAM_AP
                    )) . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=fortress'>" . localize("Les Villages Fortifiés") . "</a></li>\n";
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            } else {
                $str .= "<dt onClick=\"showSubMenu('smenu1');\">" . localize('Ordre') . "</dt>\n";
                $str .= "<dd style='display: none;' id='smenu1'>\n";
                $str .= "<ul>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=member'>" . localize("Les Membres") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=fortress'>" . localize("Les Villages Fortifiés") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=alliance'>" . localize("Les Alliances (Ordre)") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=alliance&pj=1'>" . localize("Les Alliances (PJ)") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=rank'>" . localize("Rang & Autorisation") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=newmember'>" . localize("Les Nouvelles Demandes") . "</a></li>\n";
                
                if ($curplayer->getObj("Team")->get("id_Player") == $curplayer->get("id")) {
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=desc'>" . localize("Changer la Description") . "</a></li>\n";
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=transfert'>" . localize("Transférer le Commandement") . "</a></li>\n";
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=dissol'>" . localize("Dissolution de l'Ordre ({ap} PA)", 
                        array(
                            "ap" => JOIN_TEAM_AP
                        )) . "</a></li>\n";
                } else
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=disavow'>" . localize("Renier votre Ordre ({ap} PA)", 
                        array(
                            "ap" => DISAVOW_TEAM_AP
                        )) . "</a></li>\n";
                
                $str .= "</ul>\n";
                $str .= "</dd>\n";
            }
            
            $str .= "<dt onClick=\"showSubMenu('smenu2');\">" . localize('Alliances <br/>Personnelles') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu2'>\n";
            $str .= "<ul>\n";
            $str .= "<li><a href='../diplomacy/diplomacy.php?center=alliancepj'>" . localize("Les Alliances (Ordre)") . "</a></li>\n";
            $str .= "<li><a href='../diplomacy/diplomacy.php?center=alliancepj&pj=1'>" . localize("Les Alliances (PJ)") . "</a></li>\n";
            $str .= "</ul>\n";
            $str .= "</dd>\n";
            
            $str .= "<dt onClick=\"showSubMenu('smenu3');\">" . localize('Groupe de Chasse') . "</dt>\n";
            $str .= "<dd style='display: none;' id='smenu3'>\n";
            $str .= "<ul>\n";
            if ($curplayer->get("id_FighterGroup") != 0) {
                $str .= "<ul>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=memberGdC'>" . localize("Les Membres") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=tribune'>" . localize("La Tribune") . "</a></li>\n";
                
                if ($curplayer->getObj("FighterGroup")->get("id_Player") == $curplayer->get("id")) {
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=transfertgdc'>" . localize("Transférer le Commandement") . "</a></li>\n";
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=dissolgdc'>" . localize("Dissolution du GdC") . "</a></li>\n";
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=invite'>" . localize("Inviter") . "</a></li>\n";
                } else
                    $str .= "<li><a href='../diplomacy/diplomacy.php?center=quitgdc'>" . localize("Quitter le groupe") . "</a></li>\n";
                
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=newfigther'>" . localize("Vos invitations") . "</a></li>\n";
            } else {
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=newfigther'>" . localize("Vos invitations") . "</a></li>\n";
                $str .= "<li><a href='../diplomacy/diplomacy.php?center=invite'>" . localize("Inviter") . "</a></li>\n";
            }
            $str .= "</ul>\n";
            $str .= "</dd>\n";
            
            $str .= "</dl>\n";
        }
        $str .= "</div>\n";
        return $str;
    }
}
?>
