<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/diplomacy/ddef.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

// Javascript supplémentaire
$js_include = array();
$css_include = array();
if (isset($_GET["center"]) && ($_GET["center"] == 'tribune' || $_GET["center"] == 'notribune')) {
    $js_include[] = '/javascript/page/tribune.js';
    $css_include[] = '/css/page/tribune.css';
}

require_once (HOMEPATH . "/include/game.inc.php");

$centerobj = "";
$MAIN_CENTER = $MAIN_BODY->addNewHTMLObject("div", "", "class='centerarea'");
if (isset($center)) {
    $curplayer = $nacridan->loadCurSessPlayer($db);
    if ($curplayer->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
        switch ($center) {
            case "member":
            case "notribune":
            case "tribune":
            case "memberGdC":
                break;
            default:
                $center = "member";
        }
    }
    switch ($center) {
        case "member":
            include "../diplomacy/dmember.inc.php";
            $centerobj = new DMember($nacridan, $db);
            break;
        case "fortress":
            include "../diplomacy/dfortress.inc.php";
            $centerobj = new DFortress($nacridan, $db);
            break;
        case "join":
            include "../diplomacy/djoin.inc.php";
            $centerobj = new DJoin($nacridan, $MAIN_HEAD, $db);
            break;
        case "create":
            include "../diplomacy/dcreate.inc.php";
            $centerobj = new DCreate($nacridan, $db);
            break;
        case "disavow":
            include "../diplomacy/ddisavow.inc.php";
            $centerobj = new DDisavow($nacridan, $db);
            break;
        case "transfert":
            include "../diplomacy/dtransfert.inc.php";
            $centerobj = new DTransfert($nacridan, $MAIN_HEAD, $db);
            break;
        case "dissol":
            include "../diplomacy/ddissol.inc.php";
            $centerobj = new DDissol($nacridan, $db);
            break;
        case "newmember":
            include "../diplomacy/dnewmember.inc.php";
            $centerobj = new DNewMember($nacridan, $db);
            break;
        case "rank":
            include "../diplomacy/drank.inc.php";
            $centerobj = new DRank($nacridan, $db);
            break;
        case "alliance":
            include "../diplomacy/dalliance.inc.php";
            $centerobj = new DAlliance($nacridan, $MAIN_HEAD, $db);
            break;
        case "alliancepj":
            include "../diplomacy/dalliancepj.inc.php";
            $centerobj = new DAlliancePJ($nacridan, $MAIN_HEAD, $db);
            break;
        case "desc":
            include "../diplomacy/ddesc.inc.php";
            $centerobj = new DDesc($nacridan, $db);
            break;
        
        // Classe pour les GdC
        case "invite":
            include "../diplomacy/dinvite.inc.php";
            $centerobj = new DInvite($nacridan, $db);
            break;
        case "memberGdC":
            include "../diplomacy/dmembergdc.inc.php";
            $centerobj = new DMemberGdC($nacridan, $db);
            break;
        case "newfigther":
            include "../diplomacy/dnewfighter.inc.php";
            $centerobj = new DNewFighter($nacridan, $db);
            break;
        case "dissolgdc":
            include "../diplomacy/ddissolgdc.inc.php";
            $centerobj = new DDissolGdC($nacridan, $db);
            break;
        case "transfertgdc":
            include "../diplomacy/dtransfertgdc.inc.php";
            $centerobj = new DTransfertGdC($nacridan, $MAIN_HEAD, $db);
            break;
        case "quitgdc":
            include "../diplomacy/dquitgdc.inc.php";
            $centerobj = new DQuitGdC($nacridan, $db);
            break;
        case "notribune":
        case "tribune":
            include "../diplomacy/dtribune.inc.php";
            $centerobj = new DTribune($nacridan, $db);
            break;
    }
    $MAIN_CENTER->add($centerobj);
} else {
    require_once ('../diplomacy/dinfo.inc.php');
    $centerobj1 = new DInfo($nacridan, $db);
    include "../diplomacy/dmembergdc.inc.php";
    $centerobj2 = new DMemberGdC($nacridan, $db);
    $MAIN_CENTER->add($centerobj2);
    $MAIN_CENTER->add($centerobj1);
}

require_once (HOMEPATH . '/main/mmenu.inc.php');
require_once (HOMEPATH . '/diplomacy/dtribune.inc.php');
$MAIN_BODY->add(new CQMMenu($nacridan, $db, $lang));

if ($curplayer->get("authlevel") < 10) {
    require_once ('dmenu.inc.php');
    $MAIN_BODY->add(new DMenu($nacridan, $db));
}

require_once (HOMEPATH . '/conquest/cqmenubt.inc.php');
$MAIN_BODY->add(new CQMenuBT());

$MAIN_BOTTOM = $MAIN_BODY->addNewHTMLObject("div", "", "class='bottomarea'");

require_once ('../conquest/cqright.inc.php');
$MAIN_BODY->add(new CQRight($nacridan, $db));

$str = "</div>";
$MAIN_BODY->add($str);

$google = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68594660-1', 'auto');
  ga('send', 'pageview');

</script>";
$MAIN_BODY->add($google);

$MAIN_PAGE->render();

Translation::saveMessages();

// profiler_stop("start");
// $prof->printTimers(PROFILER_MODE);
?>
