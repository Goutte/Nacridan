<?php

class DAlliancePJ extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DAlliancePJ($nacridan, $head, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);

        $head->add("<script language='javascript' type='text/javascript' src='". Cache::get_cached_file('../javascript/ajax.js') . "'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $err = "";
        
        if (isset($_GET["pj"]) && $_GET["pj"] == 1) {
            $mode = "Player";
            $tablename = "PJAlliancePJ";
            $fieldname = "id_Player\$dest";
            $formtarget = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=alliancepj&pj=1' . "' target='_self'>";
        } else {
            $mode = "Team";
            $tablename = "PJAlliance";
            $fieldname = "id_Team";
            $formtarget = "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=alliancepj' . "' target='_self'>";
        }
        
        if (isset($_POST["alliance"]) || isset($_POST["allied"]) || isset($_POST["ennemy"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                
                if (isset($_POST["alliance"]))
                    TeamFactory::addPlayerAlliance($curplayer, $_POST["alliance"], quote_smart($_POST["type"]), $err, $mode, $db);
                
                if (isset($_POST["allied"])) {
                    if (isset($_POST["checkA"]))
                        TeamFactory::removePlayerAlliance($curplayer, quote_smart($_POST["checkA"]), $err, $mode, $db);
                    else
                        $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
                }
                
                if (isset($_POST["ennemy"])) {
                    if (isset($_POST["checkE"]))
                        TeamFactory::removePlayerAlliance($curplayer, quote_smart($_POST["checkE"]), $err, $mode, $db);
                    else
                        $err = localize("Erreur: Vous devez faire une sélection en cliquant sur les cases à cocher avant.");
                }
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        } else {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>\n";
            
            $str .= "<b><h1>" . localize("Vos Alliances Personnelles") . "</h1></b></td></tr>\n";
            
            $str .= "</tr></table>\n";
            
            $modify = 1;
            
            if ($modify) {
                $str .= $formtarget;
                if ($mode == "Player") {
                    $str .= "<table class='maintable centerareawidth'>\n";
                    $str .= "<tr><td class='mainbgtitle' width='160px'>" . localize("Nom du Personnage (PJ) :") . "</td>";
                    $str .= "<td class='mainbgtitle'><input name='alliance' id='alliance' autocomplete='off' maxlength=32 size=32 value='' onKeyUp=\"checkAlliancePJ(this.value,'alliance')\"></td><td class='mainbgtitle'>";
                } else {
                    $str .= "<table class='maintable centerareawidth'>\n";
                    $str .= "<tr><td class='mainbgtitle' width='110px'>" . localize("Nom de l'ordre :") . "</td>";
                    $str .= "<td class='mainbgtitle'><input name='alliance' id='alliance' autocomplete='off' maxlength=40 size=40 value='' onKeyUp=\"checkAlliance(this.value,'alliance')\"></td><td class='mainbgtitle'>";
                }
                
                $str .= "<select class='cqselector' name='type'>";
                $str .= "<option value='1' selected='selected'>" . localize("Allié") . "</option>";
                $str .= "<option value='0'>" . localize("Ennemi") . "</option>";
                $str .= "</select></td><td><input id='submitbt' type='submit' name='submitbt' value='Ajouter' /></td></tr>";
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</table></form>";
                
                $str .= "<div style='position: absolute; color: #D1DADD ; width: 400px; background-color: #000000;' class='ajaxarea' id='ajaxResp'></div></td></tr>";
            }
            
            $str .= "<table><tr><td valign='top'>\n";
            
            $dbt = new DBCollection("SELECT " . $tablename . ".id,name,type FROM " . $tablename . " LEFT JOIN " . $mode . " ON " . $mode . ".id=" . $tablename . "." . $fieldname . " WHERE type='Allié' AND id_Player\$src=" . $curplayer->get("id"), $db);
            
            $arrayMember = array();
            while (! $dbt->eof()) {
                if ($modify)
                    $arrayMember[] = array(
                        "0" => array(
                            "<input type='checkbox' name='checkA[]' value='" . $dbt->get("id") . "'>",
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $dbt->get("name"),
                            "class='mainbgbody' align='left'"
                        ),
                        "2" => array(
                            localize($dbt->get("type")),
                            "class='mainbgbody' align='left'"
                        )
                    );
                else
                    $arrayMember[] = array(
                        "0" => array(
                            " ",
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $dbt->get("name"),
                            "class='mainbgbody' align='left'"
                        )
                    );
                $dbt->next();
            }
            $str .= $formtarget;
            
            $str .= createTable(2, $arrayMember, array(
                array(
                    "<h2>" . localize("Allié") . "</h2>",
                    "class='mainbgtitle' colspan=3 align='left'"
                )
            ), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("Nom"),
                    "class='mainbglabel' width='70%' align='left'"
                )
            ), "class='maintable halfcenterareawidth'");
            if ($modify) {
                $str .= "<input type='submit' name='allied' value='" . localize("Supprimer") . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
            
            $str .= "</td><td valign='top'>\n";
            
            $dbt = new DBCollection("SELECT " . $tablename . ".id,name,type FROM " . $tablename . " LEFT JOIN " . $mode . " ON " . $mode . ".id=" . $tablename . "." . $fieldname . " WHERE type='Ennemi' AND id_Player\$src=" . $curplayer->get("id"), $db);
            
            $arrayMember = array();
            while (! $dbt->eof()) {
                if ($modify)
                    $arrayMember[] = array(
                        "0" => array(
                            "<input type='checkbox' name='checkE[]' value='" . $dbt->get("id") . "'>",
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $dbt->get("name"),
                            "class='mainbgbody' align='left'"
                        )
                    );
                else
                    $arrayMember[] = array(
                        "0" => array(
                            " ",
                            "class='mainbgbody' align='center'"
                        ),
                        "1" => array(
                            $dbt->get("name"),
                            "class='mainbgbody' align='left'"
                        )
                    );
                $dbt->next();
            }
            $str .= $formtarget;
            $str .= createTable(2, $arrayMember, array(
                array(
                    "<h2>" . localize("Ennemi") . "</h2>",
                    "class='mainbgtitle' colspan=3 align='left'"
                )
            ), array(
                array(
                    "",
                    "class='mainbglabel' width='5%' align='center'"
                ),
                array(
                    localize("Nom"),
                    "class='mainbglabel' width='90%' align='left'"
                )
            ), "class='maintable halfcenterareawidth'");
            if ($modify) {
                $str .= "<input type='submit' name='ennemy' value='" . localize("Supprimer") . "' />";
            }
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
            $str .= "</form>";
            
            $str .= "</td></tr></table>\n";
        }
        
        if ($err != "") {
            $str = $formtarget;
            $str .= "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
            $str .= "<input type='submit' name='back' value='" . localize("Terminer") . "' />";
            $str .= "</form>";
        }
        
        return $str;
    }
}

?>  
