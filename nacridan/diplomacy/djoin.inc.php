<?php

class DJoin extends HTMLObject
{

    public $db;

    public $curplayer;

    public $nacridan;

    public function DJoin($nacridan, $head, $db)
    {
        $this->db = $db;
        $this->nacridan = $nacridan;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);

        $head->add("<script language='javascript' type='text/javascript' src='". Cache::get_cached_file('../javascript/ajax.js') . "'></script>");
    }

    public function toString()
    {
        $curplayer = $this->curplayer;
        $db = $this->db;
        
        $str = "";
        $err = "";
        
        if (isset($_POST["allegiance"])) {
            if (! $this->nacridan->isRepostForm()) {
                require_once (HOMEPATH . "/factory/TeamFactory.inc.php");
                TeamFactory::askJoinTeam($curplayer, $_POST["allegiance"], $err, $db);
            } else {
                $str = eval(file_get_contents(HOMEPATH . "/include/reposterror.inc.php"));
            }
        } else {
            if ($curplayer->get("id_Team") <= 0) {
                $str .= "<table class='maintable centerareawidth'>\n";
                $str .= "<tr><td class='mainbgtitle'>\n";
                $str .= "<b><h1>" . localize("Préter Allégeance à un Ordre") . "</h1></b></td>\n";
                $str .= "</tr>\n";
                $str .= "</table>\n";
                $str .= "<form method='POST' action='" . CONFIG_HOST . '/diplomacy/diplomacy.php?center=join' . "' target='_self'>\n";
                $str .= "<table class='maintable' width='780px'>\n";
                $str .= "<tr><td class='mainbgtitle' width='170px'>" . localize("Entrez le nom de l'ordre :") . "</td>";
                $str .= "<td class='mainbgtitle'><input name='allegiance' id='allegiance' autocomplete='off' maxlength=40 size=40 value='' onKeyUp=\"checkAllegiance(this.value,'allegiance')\"></td><td>";
                $str .= "<input id='submitbt' type='submit' name='submitbt' value='Action' /></td></tr>";
                $str .= "</table>";
                $str .= "<table width='620px'>\n";
                $str .= "<tr><td width='150px'>&nbsp;</td><td class='maintextbody'><div class='ajaxarea' id='ajaxResp'></div></td></tr>";
                $str .= "</table>";
                
                $str .= "<table class='maintable centerareawidth'>";
                $str .= "<tr><td class='mainbgtitle'>";
                $str .= "<b><h3>" . localize("Les ordres existants") . "</h3></b></td>\n";
                $dbs = new DBCollection("SELECT name,Team.id,count(TeamRank.id) as nb FROM TeamRank LEFT JOIN Team ON Team.id=id_Team GROUP BY id_Team ORDER BY nb DESC", $db);
                $array = array();
                $i = 0;
                while (! $dbs->eof()) {
                    $i ++;
                    $array[] = array(
                        "0" => array(
                            "&nbsp;<a href='alliddegance.php?id=" . $dbs->get("id") . "' class='stylemainpc popupify'>" . $dbs->get("name") . "</a>",
                            "class='stats_body' align='center'"
                        ),
                        "1" => array(
                            $dbs->get("nb"),
                            "class='stats_body' align='center'"
                        )
                    );
                    
                    $dbs->next();
                }
                
                $str .= createTable(2, $array, array(), array(
                    array(
                        localize("Nom de l'ordre"),
                        "class='mainbglabel' width='60%' align='center'",
                        "EQ.name",
                        "mainbglabelhover",
                        "mainbglabel"
                    ),
                    array(
                        localize("Nb Membres"),
                        "class='mainbglabel' width='15%' align='center'",
                        "price",
                        "mainbglabelhover",
                        "mainbglabel"
                    )
                ), "class='maintable insidebuildingleftwidth'", "formid", "order");
                
                $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
                $str .= "</form>";
            } else {
                $err = localize("Tant que vous ferez partie de l'Ordre : <br/> <b>{name}</b> <br/> vous ne pourrez ni créer ni rejoindre un autre Ordre.", array(
                    "name" => $curplayer->getSub("Team", "name")
                ));
            }
        }
        
        if ($err != "") {
            $str = "<table class='maintable centerareawidth'>\n";
            $str .= "<tr><td class='mainbgtitle'>" . $err . "</td></tr></table>";
        }
        
        return $str;
    }
}
?>

