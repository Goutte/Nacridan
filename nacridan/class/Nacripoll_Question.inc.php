<?php

/**
 *  Define the Nacripoll_Question class. The class that characterizes the polls
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV2
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Nacripoll_Question extends DBObject
{

    function Nacripoll_Question()
    {
        parent::DBObject();
        $this->m_tableName = "Nacripoll_Question";
        $this->m_className = "Nacripoll_Question";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";        
        $this->m_attr["id_Nacripoll_Main"] = "";
        $this->m_attr["text"] = "";        
        $this->init();
    }
}

?>
