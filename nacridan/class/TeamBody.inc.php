<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamBody extends DBObject
{

    function TeamBody()
    {
        parent::DBObject();
        $this->m_tableName = "TeamBody";
        $this->m_className = "TeamBody";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["body"] = "";
        $this->init();
    }
}

?>