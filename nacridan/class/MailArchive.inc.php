<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MailArchive extends DBObject
{

    function MailArchive()
    {
        parent::DBObject();
        $this->m_tableName = "MailArchive";
        $this->m_className = "MailArchive";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Player\$receiver"] = "";
        $this->m_attr["id_Player\$sender"] = "";
        $this->m_attr["id_MailBody"] = "";
        $this->m_attr["new"] = "";
        $this->m_attr["title"] = "";
        $this->m_attr["important"] = "";
        $this->m_attr["id_repertory"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>