<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicMaterial extends DBObject
{

    function BasicMaterial()
    {
        parent::DBObject();
        $this->m_tableName = "BasicMaterial";
        $this->m_className = "BasicMaterial";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->init();
    }
}

?>