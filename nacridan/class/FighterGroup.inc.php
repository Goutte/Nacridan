<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class FighterGroup extends DBObject
{

    function FighterGroup()
    {
        parent::DBObject();
        $this->m_tableName = "FighterGroup";
        $this->m_className = "FighterGroup";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>