<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Mail extends DBObject
{

    function Mail()
    {
        parent::DBObject();
        $this->m_tableName = "Mail";
        $this->m_className = "Mail";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player\$receiver"] = "";
        $this->m_attr["id_Player\$sender"] = "";
        $this->m_attr["id_MailBody"] = "";
        $this->m_attr["new"] = "";
        $this->m_attr["title"] = "";
        $this->m_attr["important"] = "";
        $this->m_attr["id_repertory"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>