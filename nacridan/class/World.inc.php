<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class World extends DBObject
{

    function World()
    {
        parent::DBObject();
        $this->m_tableName = "World";
        $this->m_className = "World";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->init();
    }
}

?>