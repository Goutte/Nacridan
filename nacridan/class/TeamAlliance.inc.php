<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamAlliance extends DBObject
{

    function TeamAlliance()
    {
        parent::DBObject();
        $this->m_tableName = "TeamAlliance";
        $this->m_className = "TeamAlliance";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Team\$src"] = "";
        $this->m_attr["id_Team\$dest"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["public"] = "";
        $this->m_attr["datestart"] = "";
        $this->m_attr["datestop"] = "";
        $this->init();
    }
}

?>