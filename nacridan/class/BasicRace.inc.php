<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicRace extends DBObject
{

    function BasicRace()
    {
        parent::DBObject();
        $this->m_tableName = "BasicRace";
        $this->m_className = "BasicRace";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_BasicProfil"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["gender"] = "";
        $this->m_attr["pic"] = "";
        $this->m_attr["PC"] = "";
        $this->m_attr["class"] = "";
        $this->m_attr["alignment"] = "";
        $this->m_attr["frequency"] = "";
        $this->m_attr["land"] = "";
        $this->m_attr["behaviour"] = "";
        $this->m_attr["dropItem"] = "";
        $this->m_attr["percent"] = "";
        $this->m_attr["levelMin"] = "";
        $this->m_attr["levelMax"] = "";
        $this->m_attr["description"] = "";
        foreach ($this->m_realDBName as $key => $val) {
            $this->m_realAPIName[$val] = $key;
        }
        $this->init();
    }
}

?>
