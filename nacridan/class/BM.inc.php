<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BM extends DBObject
{

    function BM()
    {
        parent::DBObject();
        $this->m_tableName = "BM";
        $this->m_className = "BM";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Player\$src"] = "";
        $this->m_realDBName["id_StaticModifier"] = "id_StaticModifier_BM";
        $this->m_attr["id_StaticModifier"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["value"] = "";
        $this->m_attr["effect"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["date"] = "";
        $this->m_attr["life"] = "";
        foreach ($this->m_realDBName as $key => $val) {
            $this->m_realAPIName[$val] = $key;
        }
        $this->init();
    }
}

?>