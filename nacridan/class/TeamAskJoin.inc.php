<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamAskJoin extends DBObject
{

    function TeamAskJoin()
    {
        parent::DBObject();
        $this->m_tableName = "TeamAskJoin";
        $this->m_className = "TeamAskJoin";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>