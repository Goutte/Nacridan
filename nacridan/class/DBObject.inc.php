<?php
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/class/DBCollection.inc.php");

DEFINE("FOREIGN_KEY_SYNTAX", "/id_([^_\$]*)[_]?([^\$]*)(.*)/");
DEFINE("TABLE_SYNTAX", "/([^_]*)(.*)/");
// VIEW
DEFINE("SIZEAREA", 5);

/* MAP */

DEFINE("MAPHEIGHT", 400);
DEFINE("MAPWIDTH", 640);

/* DICE TYPE */

DEFINE("DICE_D3", 0);
DEFINE("DICE_D6", 1);
DEFINE("DICE_D8", 2);
DEFINE("DICE_D10", 3);
DEFINE("DICE_D12", 4);
DEFINE("DICE_D20", 5);
DEFINE("DICE_D100", 6);
DEFINE("DICE_ADD", 7);
DEFINE("DICE_MULT", 8);

DEFINE("DICE_CHARAC", 9);
DEFINE("DICE_NB", 7);

DEFINE("WIN_MONEY", "1");
DEFINE("WIN_XP", "2");
DEFINE("WIN_EQUIP", "3");
DEFINE("ADD_PLAYER", "4");
DEFINE("DEL_PLAYER", "5");
DEFINE("WIN_PLAYER", "6");
DEFINE("FREE_PLAYER", "7");

class DBObject
{

    public $m_attr;

    public $isnull;

    public $m_object;

    public $m_linkdbobject;

    public $m_realDBName;

    public $m_realAPIName;

    public $m_tableName;

    public $m_className;

    public $m_transaction;

    public $m_errorno;

    public $m_errormsg;

    function DBObject()
    {
        $this->m_attr = array();
        $this->isnull = array();
        $this->m_realDBName = array();
        $this->m_object = array();
        $this->m_transaction = array();
        $this->m_errorno = 0;
        $this->m_linkdbobject = array();
    }

    function init()
    {
        foreach ($this->m_attr as $key => $val) {
            if (preg_match("/id_(.*)/", $key, $class)) {
                if ($class[1] != "") {
                    $this->m_object[$class[1]] = "";
                    $this->m_linkdbobject[$class[1]] = false;
                }
            }
        }
    }

    function getASRenamer($from, $to)
    {
        $str = "";
        foreach ($this->m_attr as $key => $val) {
            if (isset($this->m_realDBName[$key])) {
                $realname = $this->m_realDBName[$key];
            } else {
                $realname = $key;
            }
            $str .= "," . $from . "." . $realname . " AS " . $to . $realname;
        }
        return substr($str, 1);
    }

    function initSheet($id, $tablename, $db)
    {
        $attachName = $this->getAttach();
        $this->setAttach($tablename);
        $this->load($id, $db);
        $this->set("id", "");
        $this->setAttach($attachName);
    }

    function errorNoDB()
    {
        return $this->m_errorno;
    }

    function errorMsgDB()
    {
        return $this->m_errormsg;
    }

    protected function attachObj($nameparam)
    {
        $name = "id_" . $nameparam;
        if (isset($this->m_realDBName[$name])) {
            if (preg_match(FOREIGN_KEY_SYNTAX, $this->m_realDBName[$name], $tab)) {
                $this->m_object[$tab[1]]->setAttach($tab[1] . "_" . $tab[2]);
            } else {
                trigger_error("Bad FOREIGN_KEY_SYNTAX attachObj() (" . $this->m_className . ")", E_USER_ERROR);
            }
        } else {
            $this->m_object[$nameparam]->setAttach($nameparam);
        }
    }

    function setAttach($attachName)
    {
        $this->m_tableName = $attachName;
    }

    function getAttach()
    {
        if (isset($this->m_tableName))
            $tableName = $this->m_tableName;
        else {
            if (isset($this->extraName)) {
                return - 1;
            }
            $tableName = $this->m_className;
        }
        return $tableName;
    }

    private function errorAttachment()
    {
        trigger_error("The " . $this->m_className . " object must have a valid table attachment i.e. \"Modifier_?\" use setAttach() to define a valid \"?\" target", E_USER_ERROR);
    }

    private function getAttachStopIfError()
    {
        $tableName = $this->getAttach();
        if ($tableName == - 1) {
            $this->errorAttachment();
        }
        return $tableName;
    }

    function get($varname)
    {
        if ($this->isExist($varname))
            return $this->m_attr[$varname];
        else
            return null;
    }

    function set($varname, $value)
    {
        if ($this->isExist($varname)) {
            $this->m_attr[$varname] = $value;
            unset($this->isnull[$varname]);
        } else
            return - 1;
    }

    function setNull($varname)
    {
        if ($this->isExist($varname)) {
            $this->isnull[$varname] = 1;
        } else
            return - 1;
    }

    function getSub($name, $varname)
    {
        if (isset($this->m_object[$name])) {
            return $this->m_object[$name]->get($varname);
        }
    }

    function setSub($name, $varname, $value)
    {
        if (isset($this->m_object[$name])) {
            return $this->m_object[$name]->set($varname, $value);
        } else {
            return - 1;
        }
    }

    function isExist($varname)
    {
        return isset($this->m_attr[$varname]);
    }

    function isExistObj($varname)
    {
        return isset($this->m_attr["id_" . $varname]);
    }

    function isInstantiateObj($varname)
    {
        if (! isset($this->m_attr[$varname]))
            return 0;
        return is_object($this->m_object[$varname]);
    }

    private function newSingleObj($name, $linkDB = false)
    {
        // echo $this->m_className." : ".$name." \n";
        if ($this->isExistObj($name)) {
            if (! $this->isInstantiateObj($name)) {
                $result = explode("\$", $name);
                $class = $result[0];
                $this->m_object[$name] = new $class();
                $this->attachObj($name);
            }
            
            if ($linkDB)
                $this->m_linkdbobject[$name] = true;
            
            return $this->m_object[$name];
        } else {
            return null;
        }
    }

    function externDBObj($names)
    {
        $this->externObj($names, true);
    }

    function resetExternDBObj()
    {
        foreach ($this->m_object as $key => $fkobj) {
            if ($this->isInstantiateObj($key)) {
                $this->m_object[$key]->resetExternDBObj();
            }
            $this->m_linkdbobject[$key] = false;
        }
    }

    function unlinkExternDBObj($name)
    {
        if (is_object($this->m_object[$name])) {
            $this->m_linkdbobject[$name] = false;
        }
    }

    function externObj($names, $linkDB = false)
    {
        foreach (explode(",", $names) as $fullname) {
            $obj = $this;
            foreach (explode("::", $fullname) as $name) {
                if (! $obj->isInstantiateObj($name)) {
                    $obj = $obj->newSingleObj($name, $linkDB);
                } else {
                    $obj->m_linkdbobject[$name] = $linkDB;
                    $obj = $obj->getObj($name);
                }
            }
        }
        return 0;
    }

    function loadExternDBObj($db)
    {
        foreach ($this->m_object as $key => $fkobj) {
            if ($this->m_linkdbobject[$key] == true) {
                $fkkey = "id_" . $key;
                $fkid = $this->get($fkkey);
                $this->m_object[$key]->load($fkid, $db);
            }
        }
    }

    function setObj($name, $obj, $linkDB = false)
    {
        if ($this->isExistObj($name)) {
            $result = explode("\$", $name);
            $class = $result[0];
            $this->m_object[$name] = $obj;
            $this->m_linkdbobject[$name] = $linkDB;
            $this->attachObj($name);
            return 0;
        } else {
            return - 1;
        }
    }

    function &getObj($name)
    {
        return $this->m_object[$name];
    }

    function &getAllObj()
    {
        return $this->m_object;
    }

    function &getAllAttr()
    {
        return $this->m_attr;
    }

    function &getRealAttr()
    {
        $arr = array();
        foreach ($this->m_attr as $key => $val) {
            if (isset($this->m_realDBName[$key])) {
                $key = $this->m_realDBName[$key];
            }
            $arr[$key] = $val;
        }
        return $arr;
    }

    private function rollback($db)
    {
        foreach ($this->m_transaction as $key => $id) {
            $query = sprintf("DELETE FROM %s WHERE id=%s;", $key, $id);
            $db->Execute($query);
        }
    }

    function addDBr($db)
    {
        $this->m_transaction = array();
        unset($this->m_attr["id"]);
        
        foreach ($this->m_object as $key => $fkobj) {
            if ($this->m_linkdbobject[$key]) {
                $lastid = $this->m_object[$key]->addDBr($db);
                // printf("Key : %s id:%d<br/>",$key,$lastid);
                if ($lastid != - 1) {
                    $this->m_transaction[$this->m_object[$key]->getAttach()] = $lastid;
                } else {
                    $this->m_errorno = $this->m_object[$key]->errorNoDB();
                    $this->m_errormsg = $this->m_object[$key]->errorMsgDB();
                    
                    $this->rollback($db);
                    
                    if ($lastid == - 2) {
                        $this->errorAttachment();
                    }
                    
                    return - 1;
                }
                $this->m_attr["id_" . $key] = $lastid;
            }
        }
        
        $lastid = $this->addDB($db);
        if ($lastid == - 1) {
            $this->rollback($db);
        }
        $this->m_transaction = array();
        return $lastid;
    }

    function addDB($db)
    {
        if (isset($db)) {
            $tableName = $this->getAttach();
            if ($tableName == - 1)
                return - 2;
            
            $fields = "(";
            $values = "(";
            foreach ($this->m_attr as $key => $value) {
                
                if (isset($this->m_realDBName[$key])) {
                    $key = $this->m_realDBName[$key];
                }
                
                if ($value != "") {
                    if ($fields != "(") {
                        $fields .= ",";
                        $values .= ",";
                    }
                    
                    $fields .= $key;
                    $values .= "\"" . quote_smart($value) . "\"";
                }
            }
            
            $fields .= ")";
            $values .= ")";
            
            $query = sprintf("INSERT INTO %s %s VALUES %s;", $tableName, $fields, $values);
            
            $dbI = new DBCollection($query, $db, 0, 0, false);
            
            if ($dbI->count() != 1)
                return - 1;
            
            $dbIi = new DBCollection("SELECT @@IDENTITY AS NewID", $db);
            $this->m_attr["id"] = $dbIi->get("NewID");
            return $this->m_attr["id"];
        } else {
            $name = $this->m_className;
            trigger_error("l'object DB de la classe $name = NULL", E_USER_ERROR);
            return - 1;
        }
    }

    function updateDBr($db)
    {
        if ($this->m_attr["id"] == "") {
            return $this->addDBr($db);
        } else {
            
            foreach ($this->m_object as $key => $fkobj) {
                if ($this->m_linkdbobject[$key]) {
                    $lastid = $this->m_object[$key]->updateDBr($db);
                    $this->m_attr["id_" . $key] = $lastid;
                }
            }
            
            return $this->updateDB($db);
        }
    }

    function updateDB($db)
    {
        if ($this->m_attr["id"] == "") {
            return $this->addDB($db);
        } else {
            if (isset($db)) {
                $tableName = $this->getAttachStopIfError();
                
                $query = "";
                $where = "WHERE ";
                foreach ($this->m_attr as $key => $value) {
                    
                    if (isset($this->m_realDBName[$key])) {
                        $key = $this->m_realDBName[$key];
                    }
                    
                    if ($key !== "id") {
                        if ($query != "") {
                            $query .= ",";
                        }
                        $query .= $key;
                        if (isset($this->isnull[$key])) {
                            $query .= "=NULL";
                        } else {
                            $query .= "=\"" . quote_smart($value) . "\"";
                        }
                    } else {
                        $where .= $key;
                        $where .= "=\"" . quote_smart($value) . "\"";
                    }
                }
                
                $query = sprintf("UPDATE %s SET %s %s;", $tableName, $query, $where);
                $dbU = new DBCollection($query, $db, 0, 0, false);
                
                // if ($dbU->count() != 1)
                // return - 1;
                
                return $this->m_attr["id"];
            } else {
                trigger_error("l'object DB de la classe $this->m_className = NULL", E_USER_ERROR);
                return - 1;
            }
        }
    }

    function updateDBPartial($attr, $db)
    {
        if ($this->m_attr["id"] == "") {
            return $this->addDB($db);
        } else {
            if (isset($db)) {
                if (isset($this->m_attr[$attr]) && $attr != "id") {
                    $tableName = $this->getAttachStopIfError();
                    
                    $query = "";
                    $where = "WHERE ";
                    
                    $key = $attr;
                    $value = $this->m_attr[$key];
                    
                    if (isset($this->m_realDBName[$key])) {
                        $key = $this->m_realDBName[$key];
                    }
                    
                    $query .= $key;
                    if (isset($this->isnull[$key])) {
                        $query .= "=NULL";
                    } else {
                        $query .= "=\"" . quote_smart($value) . "\"";
                    }
                    $where .= "id";
                    $where .= "=\"" . quote_smart($this->m_attr["id"]) . "\"";
                    
                    $query = sprintf("UPDATE %s SET %s %s;", $tableName, $query, $where);
                    $dbU = new DBCollection($query, $db, 0, 0, false);
                    
                    // if ($dbU->count() != 1)
                    // return - 1;
                }
                return $this->m_attr["id"];
            } else {
                trigger_error("l'object DB de la classe $this->m_className = NULL", E_USER_ERROR);
                return - 1;
            }
        }
    }

    function deleteDBr($db)
    {
        foreach ($this->m_object as $key => $fkobj) {
            if ($this->m_linkdbobject[$key]) {
                $fkkey = "id_" . $key;
                $fkid = $this->get($fkkey);
                
                $this->m_object[$key]->set("id", $fkid);
                $this->m_object[$key]->deleteDBr($db);
            }
        }
        $this->deleteDB($db);
    }

    function deleteDB($db)
    {
        if (isset($db)) {
            $this->m_errorno = 0;
            
            $tableName = $this->getAttachStopIfError();
            
            if ($tableName == - 1)
                trigger_error("The " . $this->m_className . " object must have a valid table attachment i.e. \"Modifier_?\" use setAttach() to define a valid \"?\" target", 
                    E_USER_ERROR);
            
            $where = "WHERE ";
            $value = $this->m_attr["id"];
            $where .= "id";
            $where .= "=\"" . $value . "\"";
            
            $query = sprintf("DELETE FROM %s %s;", $tableName, $where);
            $dbd = new DBCollection($query, $db, 0, 0, false);
            
            if ($dbd->count() != 1)
                return - 1;
            
            return 0;
        } else {
            trigger_error("l'object DB de la classe $this->m_className = NULL", E_USER_ERROR);
            return - 1;
        }
    }

    /**
     * fonction pour recharger depuis la base la valeur d'un attribut dans l'objet
     */
    function reloadPartialAttribute($id, $attr, $db)
    {
        $tableName = $this->getAttach();
        
        if ($tableName == - 1)
            trigger_error("The " . $this->m_className . " object must have a valid table attachment i.e. \"Modifier_?\" use setAttach() to define a valid \"?\" target", 
                E_USER_ERROR);
        
        if ($id != 0) {
            if (isset($db)) {
                if (isset($this->m_attr[$attr])) {
                    $query = sprintf("SELECT " . $attr . " FROM %s WHERE id=%s", $tableName, $id);
                    $dbS = new DBCollection($query, $db);
                    
                    if ($dbS->count() == 0)
                        return - 1;
                    
                    $this->m_attr[$attr] = $dbS->get($attr);
                } else {
                    $name = $this->m_className;
                    trigger_error("l'object DB de la classe $name = NULL", E_USER_ERROR);
                }
            } else {
                $name = $this->m_className;
                trigger_error("l'attribut de la classe $name n'existe pas", E_USER_ERROR);
            }
        } else {
            return - 1;
        }
    }

    function load($id, $db)
    {
        $tableName = $this->getAttach();
        
        if ($tableName == - 1)
            trigger_error("The " . $this->m_className . " object must have a valid table attachment i.e. \"Modifier_?\" use setAttach() to define a valid \"?\" target", 
                E_USER_ERROR);
        
        if ($id != 0) {
            if (isset($db)) {
                $query = sprintf("SELECT * FROM %s WHERE id=%s", $tableName, $id);
                
                $dbS = new DBCollection($query, $db);
                
                if ($dbS->count() == 0)
                    return - 1;
                
                $this->DBLoad($dbS);
            } else {
                $name = $this->m_className;
                trigger_error("l'object DB de la classe $name = NULL", E_USER_ERROR);
            }
            
            foreach ($this->m_object as $key => $fkobj) {
                if ($this->m_linkdbobject[$key] == true) {
                    $fkkey = "id_" . $key;
                    /*
                     * if(isset($this->m_realDBName[$fkkey]))
                     * {
                     * $fkkey=$this->m_realDBName[$fkkey];
                     * }
                     */
                    
                    $fkid = $this->get($fkkey);
                    if (isset($fkid)) {
                        $this->m_object[$key]->load($fkid, $db);
                    }
                }
            }
        } else {
            return - 1;
            // trigger_error ("id:\"".$id."\" ou nom de la table:\"".$tableName."\" non valide", E_USER_ERROR);
        }
    }

    function reload($db)
    {
        if (isset($this->m_attr["id"]) && $this->m_attr["id"] != 0) {
            $this->load($this->m_attr["id"], $db);
        }
    }

    function getClassName()
    {
        return $this->m_className;
    }

    function getPrefix()
    {
        return $this->m_objprefix;
    }

    function DBLoad(&$dbCollection, $extra = "")
    {
        foreach ($this->m_attr as $key => $val) {
            if (isset($this->m_realDBName[$key])) {
                $realname = $this->m_realDBName[$key];
            } else {
                $realname = $key;
            }
            if (null !== $dbCollection->get($extra . $realname)) {
                $this->m_attr[$key] = $dbCollection->get($extra . $realname);
            } else {
                $this->isnull[$key] = 1;
            }
        }
    }

    function show()
    {
        foreach ($this->m_attr as $key => $val) {
            echo $key . ":" . $this->m_attr[$key] . "\n";
        }
        
        foreach ($this->m_object as $key => $fkobj) {
            echo "****** " . $key . "\n";
            
            if ($this->isInstantiateObj($key))
                $fkobj->show();
        }
    }
    
    /*
     * function initVars($filename) {
     *
     * if (file_exists($filename)) {
     * include($filename);
     * }
     *
     * foreach($this->m_attr as $key => $data)
     * {
     * if(!preg_match(FOREIGN_KEY_SYNTAX,$key,$tab))
     * {
     * if(isset($cond[$key]))
     * {
     * $this->m_varConds[$key]=$cond[$key];
     * }
     * else
     * {
     * $this->m_varConds[$key]["type"]="text";
     * }
     *
     * $this->m_varConds[$key]["name"]=$key;
     * if($this->m_attr[$key]!="")
     * {
     * $this->m_varConds[$key]["value"]=$this->m_attr[$key];
     * }
     * }
     * }
     * }
     */
}

?>
