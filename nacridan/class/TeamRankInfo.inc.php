<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamRankInfo extends DBObject
{

    function TeamRankInfo()
    {
        parent::DBObject();
        $this->m_tableName = "TeamRankInfo";
        $this->m_className = "TeamRankInfo";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["num"] = "";
        $this->m_attr["auth1"] = "";
        $this->m_attr["auth2"] = "";
        $this->m_attr["auth3"] = "";
        $this->m_attr["auth4"] = "";
        $this->m_attr["auth5"] = "";
        $this->m_attr["auth6"] = "";
        $this->m_attr["auth7"] = "";
        $this->init();
    }
}

?>