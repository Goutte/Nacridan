<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class RolePlayAction extends DBObject
{

    function RolePlayAction()
    {
        parent::DBObject();
        $this->m_tableName = "RolePlayAction";
        $this->m_className = "RolePlayAction";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["description"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>