<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Move extends DBObject
{

    function Move()
    {
        parent::DBObject();
        $this->m_tableName = "Move";
        $this->m_className = "Move";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_BasicEvent"] = "";
        $this->init();
    }
}

?>
