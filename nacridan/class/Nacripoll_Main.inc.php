<?php

/**
 *  Define the Nacripoll_Main class. The class that characterizes the polls
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV2
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Nacripoll_Main extends DBObject
{

    function Nacripoll_Main()
    {
        parent::DBObject();
        $this->m_tableName = "Nacripoll_Main";
        $this->m_className = "Nacripoll_Main";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";        
        $this->m_attr["name"] = "";
        $this->m_attr["description"] = "";
        $this->m_attr["status"] = "";        
        $this->m_attr["start_date"] = "";
        $this->m_attr["end_date"] = "";
		$this->m_attr["participant_list"] = "";
        $this->init();
    }
}

?>
