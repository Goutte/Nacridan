<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MagicSpell extends DBObject
{

    function MagicSpell()
    {
        parent::DBObject();
        $this->m_tableName = "MagicSpell";
        $this->m_className = "MagicSpell";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_BasicMagicSpell"] = "";
        $this->m_attr["skill"] = "";
        $this->init();
    }
}

?>