<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
// TO REMOVE after we clean up all localize
class HActionMsgHisto
{

    function msgBuilder ($error, &$param, $src = 1)
    {
        $msg = "";
        switch ($param["TYPE_ACTION"]) {
            
            // ACTION DE BASE
            case GIVE_MONEY:
                $msg = self::msgGiveMoney($error, $param, $src);
                break;
            case GIVE_OBJECT:
                $msg = self::msgGiveObject($error, $param, $src);
                break;
            case ATTACK:
                $msg = self::msgAttack($error, $param, $src);
                break;
            case ARCHERY:
                $msg = self::msgArchery($error, $param, $src);
                break;
            case HUSTLE:
                $msg = self::msgHustle($error, $param, $src);
                break;
            case USE_POTION:
                $msg = self::msgPotion($error, $param, $src);
                break;
            case USE_PARCHMENT:
                $msg = self::msgParchment($error, $param, $src);
                break;
            case DRINK_SPRING:
                $msg = self::msgDrinkSpring($error, $param, $src);
                break;
            case LEVEL_UP:
                $msg = self::msgLevelUp($error, $param, $src);
                break;
            case GAIN_XP:
                $msg = self::msgPassiveGainXP($error, $param, $src);
                break;
            case BUY_PJ:
                $msg = self::msgBuyPJ($error, $param, $src);
                break;
            case MAGICAL_ATTACK_BUILDING:
                $msg = self::msgAttackBuilding($error, $param, $src);
                break;
            case NORMAL_ATTACK_BUILDING:
                $msg = self::msgAttackBuilding($error, $param, $src);
                break;
            
            // COMPETENCE
            case ABILITY_FIRSTAID:
                $msg = self::msgFirstAid($error, $param, $src);
                break;
            case ABILITY_TWIRL:
                $msg = self::msgTwirl($error, $param, $src);
                break;
            case ABILITY_SHARPEN:
                $msg = self::msgSharpen($error, $param, $src);
                break;
            case ABILITY_BOLAS:
                $msg = self::msgBolas($error, $param, $src);
                break;
            case ABILITY_DISARM:
                $msg = self::msgDisarm($error, $param, $src);
                break;
            case ABILITY_BRAVERY:
                $msg = self::msgBravery($error, $param, $src);
                break;
            case ABILITY_RESISTANCE:
                $msg = self::msgResistance($error, $param, $src);
                break;
            case ABILITY_EXORCISM:
                $msg = self::msgExorcism($error, $param, $src);
                break;
            case ABILITY_AUTOREGEN:
                $msg = self::msgAutoregen($error, $param, $src);
                break;
            case ABILITY_STEAL:
                $msg = self::msgSteal($error, $param, $src);
                break;
            case ABILITY_RUN:
                $msg = self::msgRun($error, $param, $src);
                break;
            case ABILITY_PROTECTION:
                $msg = self::msgProtection($error, $param, $src);
                break;
            case ABILITY_GUARD:
                $msg = self::msgGuard($error, $param, $src);
                break;
            case ABILITY_FLYARROW:
                $msg = self::msgFlyArrow($error, $param, $src);
                break;
            case ABILITY_FLAMING:
                $msg = self::msgFlaming($error, $param, $src);
                break;
            case ABILITY_AMBUSH:
                $msg = self::msgAmbush($error, $param, $src);
                break;
            case ABILITY_LARCENY:
                $msg = self::msgLarceny($error, $param, $src);
                break;
            
            // SORTILEGE
            case SPELL_BURNING:
                $msg = self::msgBurning($error, $param, $src);
                break;
            case SPELL_TEARS:
                $msg = self::msgTears($error, $param, $src);
                break;
            case SPELL_ARMOR:
                $msg = self::msgArmor($error, $param, $src);
                break;
            case SPELL_CURE:
                $msg = self::msgCure($error, $param, $src);
                break;
            case SPELL_REGEN:
                $msg = self::msgRegen($error, $param, $src);
                break;
            case SPELL_SPRING:
                $msg = self::msgSpring($error, $param, $src);
                break;
            case SPELL_WALL:
                $msg = self::msgWall($error, $param, $src);
                break;
            case SPELL_FIREBALL:
                $msg = self::msgFireBall($error, $param, $src);
                break;
            case SPELL_DEMONPUNCH:
                $msg = self::msgDemonPunch($error, $param, $src);
                break;
            case SPELL_FIRE:
                $msg = self::msgFire($error, $param, $src);
                break;
            case SPELL_BLOOD:
                $msg = self::msgBlood($error, $param, $src);
                break;
            case SPELL_SHIELD:
                $msg = self::msgShield($error, $param, $src);
                break;
            case SPELL_NEGATION:
                $msg = self::msgNegation($error, $param, $src);
                break;
            case SPELL_RAIN:
                $msg = self::msgRain($error, $param, $src);
                break;
            case SPELL_RELOAD:
                $msg = self::msgReload($error, $param, $src);
                break;
            case SPELL_SUN:
                $msg = self::msgSun($error, $param, $src);
                break;
            case SPELL_BLESS:
                $msg = self::msgBless($error, $param, $src);
                break;
            case SPELL_BARRIER:
                $msg = self::msgBarrier($error, $param, $src);
                break;
            case SPELL_BUBBLE:
                $msg = self::msgBubble($error, $param, $src);
                break;
            case SPELL_PILLARS:
                $msg = self::msgPillars($error, $param, $src);
                break;
            case SPELL_ANGER:
                $msg = self::msgAnger($error, $param, $src);
                break;
            case SPELL_CURSE:
                $msg = self::msgCurse($error, $param, $src);
                break;
            case SPELL_TELEPORT:
                $msg = self::msgTeleport($error, $param, $src);
                break;
            case S_MASSIVEINSTANTTP:
                $msg = self::msgMassiveTeleport($error, $param, $src);
                break;
            case SPELL_LIGHTTOUCH:
                $msg = self::msgLightTouch($error, $param, $src);
                break;
            case SPELL_RECALL:
                $msg = self::msgSpellRecall($error, $param, $src);
                break;
            case SPELL_FIRECALL:
            case SPELL_FIRECALL_CAPTURE:
                $msg = self::msgSpellFirecall($error, $param, $src);
                break;
            case SPELL_KINE:
                $msg = self::msgSpellKine($error, $param, $src);
                break;
            case SPELL_SOUL:
                $msg = self::msgSpellSoul($error, $param, $src);
                break;
            
            // PASSIVE
            
            case PASSIVE_SUN:
                $msg = self::msgPassiveSun($error, $param, $src);
                break;
            case PASSIVE_RAIN:
                $msg = self::msgPassiveRain($error, $param, $src);
                break;
            case PASSIVE_FIRE:
                $msg = self::msgPassiveFire($error, $param, $src);
                break;
            case PASSIVE_FLYARROW:
                $msg = self::msgPassiveFlyArrow($error, $param, $src);
                break;
            case PASSIVE_EXORCISM:
                $msg = self::msgPassiveExorcism($error, $param, $src);
                break;
            case PASSIVE_PROJECTION:
            case PASSIVE_PROJECTION_DEATH_OBST:
            case PASSIVE_PROJECTION_DEATH_OPP:
                $msg = self::msgPassiveProjection($error, $param, $src);
                break;
            case PASSIVE_PROJECTION_BUILDING:
                $msg = self::msgPassiveProjectionBuilding($error, $param, $src);
                break;
            case PASSIVE_TWIRL:
                $msg = self::msgPassiveTwirl($error, $param, $src);
                break;
            case PASSIVE_BIER:
                $msg = self::msgPassiveBier($error, $param, $src);
                break;
            case PASSIVE_VOID:
                $msg = self::msgPassiveVoid($error, $param, $src);
                break;
            
            // LES ACTIONS DES MONSTRES
            case M_INFEST:
                $msg = self::msgInfest($error, $param, $src);
                break;
            case M_INSTANTBLOOD:
                $msg = self::msgInstantBlood($error, $param, $src);
                break;
            case M_GRAPNEL:
                $msg = self::msgGrapnel($error, $param, $src);
                break;
            case M_BRANCH:
                $msg = self::msgBranch($error, $param, $src);
                break;
            case M_SPIT:
                $msg = self::msgSpit($error, $param, $src);
                break;
            case M_EVIL_BLOW:
                $msg = self::msgEvilBlow($error, $param, $src);
                break;
            case M_MORPH:
                $msg = self::msgMorph($error, $param, $src);
                break;
            
            // LES MORTS PASSIVES
            case PASSIVE_INFEST_DEATH:
                $msg = self::msgPassiveInfestDeath($error, $param);
                break;
            case PASSIVE_BLOOD_DEATH:
                $msg = self::msgPassiveBloodDeath($error, $param, $src);
                break;
            case PASSIVE_BARRIER_DEATH:
                $msg = self::msgPassiveBarrierDeath($error, $param, $src);
                break;
            case PASSIVE_POISON_DEATH:
                $msg = self::msgPassivePoisonDeath($error, $param, $src);
                break;
            case PASSIVE_WOUND_DEATH:
                $msg = self::msgPassiveWoundDeath($error, $param, $src);
                break;
            
            // EXTRACTION
            case TALENT_MINE:
                $msg = self::msgMine($error, $param, $src);
                break;
            case TALENT_DISMEMBER:
                $msg = self::msgDismember($error, $param, $src);
                break;
            case TALENT_SCUTCH:
                $msg = self::msgScutch($error, $param, $src);
                break;
            case TALENT_HARVEST:
                $msg = self::msgHarvest($error, $param, $src);
                break;
            case TALENT_SPEAK:
                $msg = self::msgSpeak($error, $param, $src);
                break;
            case TALENT_CUT:
                $msg = self::msgCut($error, $param, $src);
                break;
            
            // RAFFINAGE
            case TALENT_IRONREFINE:
            case TALENT_LEATHERREFINE:
            case TALENT_SCALEREFINE:
            case TALENT_LINENREFINE:
            case TALENT_WOODREFINE:
            case TALENT_PLANTREFINE:
            case TALENT_GEMREFINE:
                $msg = self::msgRefine($error, $param, $src);
                break;
            
            case TALENT_KRADJECKCALL:
                $msg = self::msgKradjeckCall($error, $param, $src);
                break;
            case TALENT_MONSTER:
                $msg = self::msgMonster($error, $param, $src);
                break;
            case TALENT_DETECTION:
                $msg = self::msgDetection($error, $param, $src);
                break;
            
            // ARTISANAT
            case TALENT_IRONCRAFT:
            case TALENT_LEATHERCRAFT:
            case TALENT_SCALECRAFT:
            case TALENT_LINENCRAFT:
            case TALENT_WOODCRAFT:
            case TALENT_PLANTCRAFT:
                $msg = self::msgArtisanat($error, $param, $src);
                break;
            case TALENT_GEMCRAFT:
                $msg = self::msgGemCraft($error, $param, $src);
                break;
            
            // BATIMENTS
            case TELEPORT:
                $msg = self::msgTempleTeleport($error, $param, $src);
                break;
            case BEDROOM_SLEEPING:
                $msg = self::msgBedroomSleeping($error, $param, $src);
                break;
            case BEDROOM_DEPOSIT:
                $msg = self::msgBedroomDeposit($error, $param, $src);
                break;
            case BEDROOM_WITHDRAW:
                $msg = self::msgBedroomWithdraw($error, $param, $src);
                break;
            case TEMPLE_HEAL:
                $msg = self::msgTempleHeal($error, $param, $src);
                break;
            case TEMPLE_BLESSING:
                $msg = self::msgTempleBlessing($error, $param, $src);
                break;
            case TEMPLE_RESURRECT:
                $msg = self::msgTempleResurrect($error, $param, $src);
                break;
            case HOSTEL_DRINK:
                $msg = self::msgHostelDrink($error, $param, $src);
                break;
            case HOSTEL_CHAT:
                $msg = self::msgHostelChat($error, $param, $src);
                break;
            case HOSTEL_ROUND:
                $msg = self::msgHostelRound($error, $param, $src);
                break;
            case HOSTEL_NEWS:
                $msg = self::msgHostelNews($error, $param, $src);
                break;
            case HOSTEL_TRADE:
                $msg = self::msgHostelTrade($error, $param, $src);
                break;
            case SCHOOL_LEARN_SPELL:
                $msg = self::msgLearnSpell($error, $param, $src);
                break;
            case SCHOOL_LEARN_ABILITY:
                $msg = self::msgLearnAbility($error, $param, $src);
                break;
            case SCHOOL_LEARN_TALENT:
                $msg = self::msgLearnTalent($error, $param, $src);
                break;
            case BANK_DEPOSIT:
                $msg = self::msgDeposit($error, $param, $src);
                break;
            case BANK_WITHDRAW:
                $msg = self::msgWithdraw($error, $param, $src);
                break;
            case BANK_TRANSFERT:
                $msg = self::msgTransfert($error, $param, $src);
                break;
            case BANK_TRANSFERT_PLAYER:
                $msg = self::msgTransfert($error, $param, $src);
                break;
            case BASIC_SHOP_BUY:
                $msg = self::msgBasicBuyShop($error, $param, $src);
                break;
            case SHOP_SELL:
                $msg = self::msgSell($error, $param, $src);
                break;
            case SHOP_REPAIR:
                $msg = self::msgRepair($error, $param, $src);
                break;
            case MAIN_SHOP_BUY:
                $msg = self::msgMainBuyShop($error, $param, $src);
                break;
            case GUILD_ORDER_EQUIP:
                $msg = self::msgOrderEquip($error, $param, $src);
                break;
            case GUILD_TAKE_EQUIP:
                $msg = self::msgTakeEquip($error, $param, $src);
                break;
            case GUILD_ORDER_ENCHANT:
                $msg = self::msgOrderEnchant($error, $param, $src);
                break;
            case GUILD_TAKE_ENCHANT:
                $msg = self::msgTakeEnchant($error, $param, $src);
                break;
            case CARAVAN_CREATE:
                $msg = self::msgCaravanCreate($error, $param, $src);
                break;
            case CARAVAN_TERMINATE:
                $msg = self::msgCaravanTerminate($error, $param, $src);
                break;
            case COLLECT_PACKAGE:
                $msg = self::msgCollectPackage($error, $param, $src);
                break;
            case SEND_PACKAGE:
                $msg = self::msgSendPackage($error, $param, $src);
                break;
            case RETRIEVE_PACKAGE:
                $msg = self::msgRetrievePackage($error, $param, $src);
                break;
            case PALACE_DEPOSIT:
                $msg = self::msgPDeposit($error, $param, $src);
                break;
            case PALACE_WITHDRAW:
                $msg = self::msgPWithdraw($error, $param, $src);
                break;
            case PALACE_TRANSFER_MONEY:
                $msg = self::msgPTransferMoney($error, $param, $src);
                break;
            case NAME_VILLAGE:
                $msg = self::msgNameVillage($error, $param, $src);
                break;
            case BUILDING_DESTROY:
                $msg = self::msgBuildingDestroy($error, $param, $src);
                break;
            case BUILDING_REPAIR:
                $msg = self::msgBuildingRepair($error, $param, $src);
                break;
            case BUILDING_CONSTRUCT:
                $msg = self::msgBuildingConstruct($error, $param, $src);
                break;
            case FORTIFY_VILLAGE:
                $msg = self::msgFortifyVillage($error, $param, $src);
                break;
            case OPERATE_VILLAGE:
                $msg = self::msgOperateVillage($error, $param, $src);
                break;
            case OPERATE_VILLAGE_TEMPLE:
                $msg = self::msgOperateVillageTemple($error, $param, $src);
                break;
            case OPERATE_VILLAGE_ENTREPOT:
                $msg = self::msgOperateVillageEntrepot($error, $param, $src);
                break;
            case PALACE_DECISION:
                $msg = self::msgPalaceDecision($error, $param, $src);
                break;
            case CONTROL_TEMPLE:
                $msg = self::msgControlTemple($error, $param, $src);
                break;
            case UPGRADE_FORTIFICATION:
                $msg = self::msgUpgradeFortification($error, $param, $src);
                break;
            case CREATE_PUTSCH:
                $msg = self::msgCreatePutsch($error, $param, $src);
                break;
            case ACT_PUTSCH:
                $msg = self::msgActPutsch($error, $param, $src);
                break;
            case TERRASSEMENT:
                $msg = self::msgTerrassement($error, $param, $src);
                break;
            case TERRAFORMATION:
                $msg = self::msgTerraformation($error, $param, $src);
                break;
            case EXCHANGE_OBJECT:
                $msg = self::msgSell($error, $param, $src);
                break;
            case SHOP_REPAIR_EX:
                $msg = self::msgRepair($error, $param, $src);
                break;
            case EXCHANGE_GET_OBJECT:
                $msg = self::msgMainBuyShop($error, $param, $src);
                break;
            case BUILDING_COLLAPSE_DEATH:
            case BUILDING_COLLAPSE:
                $msg = self::msgBuildingCollapse($error, $param, $src);
                break;
            
            default:
                break;
        }
        return $msg;
    }

    /* ************************************************************* FONCTIONS AUXILLIAIRES ******************************************************* */
    function msgInfoDeath ($param)
    {
        $msg = "";
        if (isset($param["TARGET_X"])) {
            $msg = localize("Vous avez laissé tomber {nb} pièces d'or en ({x},{y}).", array(
                    "nb" => $param["MONEY_FALL"],
                    "x" => $param["TARGET_X"],
                    "y" => $param["TARGET_Y"]
            )) . "<br/>";
            if (isset($param["LEVEL_UP"]) && $param["LEVEL_UP"] == 1)
                $msg .= localize("Vous avez perdu {nb} points d'expérience.", array(
                        "nb" => $param["TARGET_XP"]
                )) . "<br/>";
            else
                $msg .= localize("Vous avez perdu {nb} points d'expérience.", array(
                        "nb" => floor($param["TARGET_XP"] / 2)
                )) . "<br/>";
        }
        return $msg;
    }

    function msgGainXP ($param)
    {
        $msg = "<br/>";
        $msg = localize("Cette action vous a rapporté {xp} px ", array(
                "xp" => $param["XP"]
        )) . "<br/>";
        
        if (isset($param["NOMBRE"])) {
            for ($i = 1; $i < $param["NOMBRE"]; $i ++) {
                $msg .= localize("{name} gagne {xp} px ", array(
                        "name" => $param["PLAYER" . $i],
                        "xp" => $param["GAIN" . $i]
                )) . "<br/>";
            }
        }
        return $msg;
    }

    function msgPassiveGainXP ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("{name} ({id1}) a tué {target} ({id2}) de niveau {niveau}.", array(
                    "niveau" => $param["TARGET_LEVEL"],
                    "id2" => $param["TARGET_ID"],
                    "target" => $param["TARGET_NAME"],
                    "name" => isset($param["KILLER_NAME"]) ? $param["KILLER_NAME"] : $param["PLAYER_NAME"],
                    "id1" => isset($param["KILLER_ID"]) ? $param["KILLER_ID"] : $param["PLAYER_ID"]
            )) . "<br/>";
            $msg .= localize("Vous avez gagné {nb} points d'expérience correspondant à votre part de membre du même groupe de chasse.", array(
                    "nb" => $param["GAIN_XP"],
                    "name" => $param["TARGET_NAME"]
            )) . "<br/>";
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgLevelUp ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($param["CRABE"]) {
                $str = localize("Vos actions n'ont pas été suffisantes pour franchir un level.", array(
                        "level" => $param["LEVEL"]
                )) . "<br/><br/>";
            } else {
                $traduction = array();
                $traduction["strength"] = "FORCE";
                $traduction["dexterity"] = "DEXTERITE";
                $traduction["speed"] = "VITESSE";
                $traduction["magicSkill"] = "MAITRISE DE LA MAGIE";
                
                $str = localize("Vous êtes passé niveau {level}", array(
                        "level" => $param["LEVEL"]
                )) . "<br/><br/>";
                $str .= localize("Vous avez gagné 3 points de vie.") . "<br/>";
                
                if ($param["FIELD1"] == $param["FIELD2"] && $param["FIELD1"] != "hp") {
                    $str .= localize("Vous avez progressé de 2D6 dans la caractéristique : {carac}", array(
                            "carac" => $traduction[$param["FIELD1"]]
                    ));
                } else {
                    if ($param["FIELD1"] == "hp")
                        $str .= localize("Vous avez gagné 15 points de vie.") . "<br/>";
                    else
                        $str .= localize("Vous avez progressé de 1D6 dans la caractéristique : {carac}", array(
                                "carac" => $traduction[$param["FIELD1"]]
                        )) . "<br/>";
                    
                    if ($param["FIELD2"] == "hp")
                        $str .= localize("Vous avez gagné 15 points de vie.") . "<br/>";
                    else
                        $str .= localize("Vous avez progressé de 1D6 dans la caractéristique : {carac}", array(
                                "carac" => $traduction[$param["FIELD2"]]
                        )) . "<br/>";
                }
            }
        }
        return $str;
    }

    function msgBarrierEffect (&$param, $src)
    {
        $msg = "";
        if ($src) {
            
            if (isset($param["BARRIER_LEVEL"])) {
                $msg .= " <br/>";
                $msg .= localize("Votre adversaire etait protégé par une Barrière enflammée de niveau {nb}", array(
                        "nb" => $param["BARRIER_LEVEL"]
                )) . "<br/>";
                $msg .= "Elle vous a infligé ........................: " . $param["PLAYER_DAMAGE"] . " points de vie <br/>";
                if ($param["PLAYER_KILLED"]) {
                    $msg .= "Vous avez été tué(e)" . "<br/>";
                }
                $msg .= " <br/>";
            }
        } else {
            if (isset($param["BARRIER_LEVEL"])) {
                $msg .= " <br/>";
                $msg .= localize("Vous étiez protégé par une Barrière enflammée de niveau {nb}", array(
                        "nb" => $param["BARRIER_LEVEL"]
                )) . "<br/>";
                $msg .= "Elle a infligé à votre adversaire ........................: " . $param["PLAYER_DAMAGE"] . " points de vie <br/>";
                if ($param["PLAYER_KILLED"]) {
                    $msg .= "Il a été tué(e)" . "<br/>";
                }
                $msg .= " <br/>";
            }
        }
        
        return $msg;
    }

    function msgPassiveBubble ($param, $src = 1, $space = 1)
    {
        $msg = "";
        if ($src) {
            if (isset($param["BUBBLE_LIFE"]) && $param["BUBBLE_LIFE"] != 0) {
                if ($space)
                    $msg .= "<br/>";
                if (! $param["BUBBLE_CANCEL"]) {
                    $msg .= localize("Votre adversaire etait protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                            "damage" => $param["BUBBLE_LIFE"],
                            "level" => $param["BUBBLE_LEVEL"]
                    )) . "<br/>";
                } else {
                    $msg .= localize("Votre adversaire était protégé par une bulle de vie qui a absorbé la totalité du coup.") . "<br/>";
                }
                if ($space)
                    $msg .= "<br/>";
            }
        } else {
            if (isset($param["BUBBLE_LIFE"]) && $param["BUBBLE_LIFE"] != 0) {
                if ($space)
                    $msg .= "<br/>";
                if (! $param["BUBBLE_CANCEL"]) {
                    $msg .= localize("Vous étiez protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                            "damage" => $param["BUBBLE_LIFE"],
                            "level" => $param["BUBBLE_LEVEL"]
                    )) . "<br/>";
                } else {
                    $msg .= localize("Vous étiez protégé par une bulle de vie qui a absorbé la totalité du coup.") . "<br/>";
                }
                if ($space)
                    $msg .= "<br/>";
            }
        }
        return $msg;
    }

    /* ************************************************************* ACTION CONTEXTUELLE ******************************************************* */
    function msgDrinkSpring ($error, $param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Vous avez bu l'eau d'un Bassin Divin de niveau {niveau}.", array(
                    "niveau" => $param["SPRING_LEVEL"]
            )) . "<br/>";
            $msg .= localize("Vous avez récupéré {hp} points de vie", array(
                    "hp" => $param["HP_HEAL"]
            )) . "<br/><br/>";
        }
        return $msg;
    }

    function msgBuildingCollapse ($error, $param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Le bâtiment dans lequel vous vous trouviez a été détruit") . "<br/>";
            $msg .= localize("Vous avez été pris dans l'effrondrement et vous avez perdu {hp} points de vie.", array(
                    "hp" => $param["HP"]
            )) . "<br/>";
            if ($param["DEATH"])
                $msg .= localize("Vous avez été tué.") . "<br/><br/>";
            
            return $msg;
        }
    }

    /* ************************************************************* LE MENU OBJET ******************************************************* */
    function msgPotion ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Vous avez utilisé {name} de niveau {level}  potion a été utilisé.", array(
                    "level" => $param["POTION_LEVEL"],
                    "name" => $param["POTION_NAME"]
            )) . "<br/>";
            if (isset($param["HP_POTENTIEL"]))
                $msg = localize("La potion vous avez soigné de {hp} points de vie et vous aviez récupéré {pv} points de vie.", array(
                        "hp" => $param["HP_POTENTIEL"],
                        "pv" => $param["HP_HEAL"]
                )) . "<br/>";
            else {
                $msg = localize("La potion avez eu les effets suivants : ") . "<br/>";
                $static = new StaticModifier();
                foreach ($static->m_characLabel as $key) {
                    if (isset($param[$key])) {
                        $msg .= "- " . translateAtt($key) . localize(":") . $param[$key] . "<br/>";
                    }
                }
                $msg .= localize("pour 3 dla") . "<br/>";
            }
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgParchment ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = "Vous venez d'utiliser un parchemin avec succès pour " . USE_PARCHMENT_AP . " PA.";
            switch ($param["POINT"]) {
                case "Starting":
                    
                    $msg .= localize("Escortez le PNJ jusqu'au deuxième point pour valider une seconde fois le parchemin, 
				terminer la mission et recevoir une récompense.") . "<br/>";
                    break;
                
                case "Stop":
                    $msg .= localize("Félicitation vous avez réussi à escorter le PNJ jusqu'à destination. Vous recevez votre salaire : 
			 {money} PO", array(
                            "money" => $param["MONEY"]
                    )) . "<br/>";
                    break;
                
                default:
                    $msg = localize("Le personnage que vous devez escorter est trop éloigné de vous. 
						Rapprochez celui-ci de votre personnage principal pour valider le parchemin.") . "<br/>";
                    break;
            }
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgGiveObject ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src)
                $msg = localize("Vous avez donné {name} ({id}) à {target} ", array(
                        "name" => $param["OBJECT_NAME"],
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["OBJECT_ID"]
                ));
            else
                
                $msg = localize("{target} vous a donné {name} ({id})", array(
                        "name" => $param["OBJECT_NAME"],
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["OBJECT_ID"]
                ));
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    /* ************************************************************* LE MENU ACTION ******************************************************* */
    function msgBuyPJ ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = "";
            if ($src) {
                $msg .= localize("Vous avez acheté les objets suivants à " . $param["TARGET_NAME"]) . " : <br/>";
                for ($i = 1; $i < $param["NB"] + 1; $i ++)
                    $msg .= localize("- ") . $param["OBJECT_NAME"][$i] . "<br/>";
                
                $msg .= localize("Vous avez payé un total de {price} pièces d'or ", array(
                        "price" => $param["PRICE"]
                )) . "<br/>";
            } else {
                $msg .= localize($param["PLAYER_NAME"] . " vous a acheté les objets suivants : <br/>");
                for ($i = 1; $i < $param["NB"] + 1; $i ++)
                    $msg .= localize("- ") . $param["OBJECT_NAME"][$i] . "<br/>";
                
                $msg .= localize("Vous avez gagné un total de {price} pièces d'or", array(
                        "price" => $param["PRICE"]
                )) . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgHustle ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = "";
            if ($src) {
                $msg .= localize("Vous vouliez bousculer " . $param["TARGET_NAME"]) . " (" . $param["TARGET_ID"] . "<br/><br/>";
                
                if ($param["JET"]) {
                    
                    $msg .= localize("Votre Jet de maitrise  a été de ");
                    $msg .= localize("......................: " . $param["PLAYER_ATTACK"]) . "<br/>";
                    $msg .= localize("Le Jet de maitrise votre adversaire a été de ");
                    $msg .= "...........................: " . $param["TARGET_DEFENSE"] . " <br/><br/>";
                }
                if ($param["SUCCESS"]) {
                    $msg .= localize("Action réussie : vous avez interverti vos places avec {target} ", array(
                            "target" => localize($param["TARGET_NAME"])
                    )) . "<br/>";
                    $msg .= localize("Votre nouvelle position etait : ") . "X=" . $param["XNEW"] . " / Y=" . $param["YNEW"] . "<br/><br/>";
                } else
                    $msg .= localize("Action raté : vous n'avez pas réussi à intervertir vos places avec {target} ", array(
                            "target" => localize($param["TARGET_NAME"])
                    )) . "<br/><br/>";
                
                $msg .= localize("Cette action vous a couté {ap} PA", array(
                        "ap" => $param["AP"]
                )) . "<br/>";
            } else {
                $msg .= localize($param["PLAYER_NAME"] . " (" . $param["PLAYER_ID"] . "a essayé de vous bousculer.") . "<br/><br/>";
                
                if ($param["JET"]) {
                    
                    $msg .= localize("Son Jet de maitrise  a été de ");
                    $msg .= localize("......................: " . $param["PLAYER_ATTACK"]) . "<br/>";
                    $msg .= localize("Votre Jet de maitrise votre adversaire a été de ");
                    $msg .= "...........................: " . $param["TARGET_DEFENSE"] . " <br/><br/>";
                }
                if ($param["SUCCESS"]) {
                    $msg .= localize("Action réussie : {target} est parvevu à échanger sa place avec la votre.", array(
                            "target" => localize($param["PLAYER_NAME"])
                    )) . "<br/>";
                    $msg .= localize("Votre nouvelle position etait alors : ") . "X=" . $param["XOLD"] . " / Y=" . $param["YOLD"] . "<br/><br/>";
                } else
                    $msg .= localize("Action raté : {target} n'a pas réussi à prendre votre place.", array(
                            "target" => localize($param["PLAYER_NAME"])
                    )) . "<br/><br/>";
            }
            
            return $msg;
        }
    }

    function msgGiveMoney ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src)
                $msg = localize("Vous avez donné {value} PO à {target} ", array(
                        "value" => $param["VALUE"],
                        "target" => $param["TARGET_NAME"]
                ));
            else
                
                $msg = localize("{player} vous a donné {value} PO.", array(
                        "value" => $param["VALUE"],
                        "player" => $param["PLAYER_NAME"]
                ));
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgAttackBuilding (&$error, &$param, $src)
    {
        $msg = "";
        $msg = localize("Vous avez attaqué {name} niveau {level}", array(
                "name" => $param["BUILDING_NAME"],
                "level" => $param["BUILDING_LEVEL"]
        )) . "<br/>";
        
        if ($param["ATTACK"] == 1) {
            $msg .= localize("Votre Jet de dégât a été de ");
            $msg .= localize("........................: " . $param["BUILDING_DAMAGE"]) . "<br/>";
        } else {
            $msg .= localize("Votre Jet de Maitrise de la magie a été de : ");
            $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/>";
            $msg .= localize("Votre Jet de Dextérité a été de : ");
            $msg .= localize("........................: " . $param["PLAYER_DEX"]) . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau {level}", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/>";
        }
        
        $msg .= localize("Vous avez infligé au bâtiment une perte de {nb} points de structure", array(
                "nb" => $param["BUILDING_DAMAGE"]
        )) . "<br/>";
        if ($param["BUILDING_DEATH"])
            $msg .= localize("Vous avez <b> DETRUIT</b> le bâtiment") . "<br/>";
        
        return $msg;
    }

    function msgAttack ($error, &$param, $src = 1)
    {
        $msg = "";
        $pluriel = "s";
        if (! $error) {
            if (isset($param["PROTECTION"]) && $param["PROTECTION"])
                $msg .= self::msgInterposition($param, $src);
            else {
                if ($src) {
                    if ($param["TARGET_IS_NPC"]) {
                        if (isset($param["TARGET_GENDER"])) {
                            if ($param["TARGET_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else {
                            $art = "";
                        }
                        $msg = localize("Vous avez attaqué {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $art . " " . localize($param["TARGET_NAME"]),
                                "level" => $param["TARGET_LEVEL"],
                                "id" => $param["TARGET_ID"]
                        )) . " <br/><br/>";
                    } else
                        $msg = localize("Vous avez attaqué {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $param["TARGET_NAME"],
                                "level" => $param["TARGET_LEVEL"],
                                "id" => $param["TARGET_ID"]
                        )) . " <br/><br/>";
                    
                    $msg .= self::msgBarrierEffect($param, 1);
                    if (! $param["PLAYER_KILLED"]) {
                        
                        if ($param["IDENT"] == ABILITY_LIGHT)
                            $msg .= localize("Votre Jet de Maitrise de la magie a été de : ");
                        else
                            $msg .= localize("Votre Jet d'Attaque a été de ");
                        $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                        $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                        $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                        
                        if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                            $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire (Bonus de dégâts : {bonus})", array(
                                    "bonus" => $param["BONUS_DAMAGE2"]
                            )) . "<br/>";
                            if ($param["IDENT"] == ABILITY_TREACHEROUS) {
                                $msg .= "<br/>" . localize("Votre Jet de vitesse a été de ");
                                $msg .= "........................." . $param["PLAYER_SPEED"] . " <br/>";
                                $msg .= localize("Bonus de la griffe : {percent}% ", array(
                                        "percent" => (min(120, 50 + $param["PLAYER_SPEED"]))
                                )) . "<br/>";
                                $msg .= localize("Votre bonus dégât était donc de {percent}", array(
                                        "percent" => $param["BONUS_DAMAGE"]
                                )) . "<br/><br/>";
                            }
                            if (isset($param["UNDEAD"]) && $param["UNDEAD"] == 1)
                                $msg .= localize("La compétence Appel de la Lumière vous a confèré 30% de bonus de dégât sur les morts-vivant.") . "<br>";
                            
                            $msg .= localize("Votre Jet de dégâts a été de ");
                            $msg .= "............: " . $param["TARGET_DAMAGE"] . " <br/>";
                            
                            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGETOT"]
                            )) . "<br/>";
                            if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                                $pluriel = "";
                            
                            $msg .= self::msgPassiveBubble($param, 1);
                            
                            $knockout = "";
                            if ($param["IDENT"] == ABILITY_KNOCKOUT && $param["TARGET_KILLED"] == 0) {
                                if (isset($param["LEVEL_BENE"])) {
                                    $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                            "level" => $param["LEVEL_BENE"]
                                    )) . "<br/>";
                                    if ($param["CANCEL"])
                                        $msg .= localize("Sa protection était plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                                    else
                                        $msg .= localize("Les effets de votre compétence ont été réduit de {level} à {niveau} points de blessure mortelle.", array(
                                                "level" => floor($param["TARGET_HP"] / 10) * 10,
                                                "niveau" => ($param["CURSE"] * 10)
                                        )) . "<br/>";
                                } else
                                    $knockout = "dont " . ($param["CURSE"] * 10) . " PV de blessure mortelle.";
                            }
                            
                            if ($param["IDENT"] == ABILITY_THRUST) {
                                $msg .= localize("Vous êtes parvenu à trouver une faille dans l'armure de votre adversaire qui a été reduite de 80%") . "<br/>";
                                $msg .= localize("Il a perdu <b>{hp} point" . $pluriel . " de vie </b>", array(
                                        "hp" => $param["TARGET_HP"]
                                )) . "<br/>";
                            } else
                                $msg .= localize("Son armure l'a protégé et il n'a perdu que <b>{hp} point" . $pluriel . " de vie </b>" . $knockout, array(
                                        "hp" => $param["TARGET_HP"]
                                )) . "<br/>";
                            
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $msg .= "<br/>";
                            } else {
                                if ($param["IDENT"] == ABILITY_STUNNED) {
                                    if (floor($param["TARGET_HP"] / 10) > 0) {
                                        if (isset($param["LEVEL_BENE"])) {
                                            $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                                    "level" => $param["LEVEL_BENE"]
                                            )) . "<br/>";
                                            if ($param["CANCEL"])
                                                $msg .= localize("Sa protection était plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                                            else
                                                $msg .= localize("Les effets de votre compétence ont été réduit de {level} à {niveau} dés de malus de vitesse.", array(
                                                        "level" => floor($param["TARGET_HP"] / 10),
                                                        "niveau" => ($param["CURSE"])
                                                )) . "<br/>";
                                        } else
                                            $msg .= localize("Vous aviez étourdi votre adversaire. Il a subi un malus de {vit}D6 de vitesse pour les 2 prochains tours.", array(
                                                    "vit" => floor($param["TARGET_HP"] / 10)
                                            )) . "<br/>";
                                    } else
                                        $msg .= localize("Vos dégâts ont trop faibles et vous ne parvenez pas à étourdir votre adversaire") . "<br/>";
                                }
                                
                                if ($param["IDENT"] == ABILITY_THRUST) {
                                    if (floor($param["TARGET_HP"] / 5) > 0) {
                                        if (isset($param["LEVEL_BENE"])) {
                                            $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                                    "level" => $param["LEVEL_BENE"]
                                            )) . "<br/>";
                                            if ($param["CANCEL"])
                                                $msg .= localize("Sa protection était plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                                            else
                                                $msg .= localize("Les effets de votre compétence ont été réduit de {level} à {niveau} point(s) de vie pour les 3 prochains tours.", array(
                                                        "level" => floor($param["TARGET_HP"] / 5),
                                                        "niveau" => (floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"])
                                                )) . "<br/>";
                                        } else
                                            $msg .= localize("Vous avez infligez à votre adversaire ({id}) une blessure profonde de {hp} point(s) de vie pour les 3 prochains tours.", array(
                                                    "hp" => floor($param["TARGET_HP"] / 5),
                                                    "id" => $param["TARGET_ID"]
                                            )) . "<br/>";
                                    } else
                                        $msg .= localize("Vos dégâts ont été trop faibles et vous n'êtes pas parvenu pas à infliger une blessure profonde à votre adversaire") . "<br/>";
                                }
                                
                                if ($param["IDENT"] == ABILITY_PROJECTION) {
                                    if (! isset($param["MOVE1"])) {
                                        $msg .= localize("Vous n'avez pas eu assez de force pour projeter votre adversaire.") . "<br/>";
                                    } else {
                                        if ($param["MOVE2"])
                                            $msg .= localize("Votre adversaire a été projeté en arrière et a reculé de 2 cases") . "<br/>";
                                        if ($param["MOVE1"] && ! $param["MOVE2"])
                                            $msg .= localize("Votre adversaire a été projeté en arrière mais n'a reculé que d'une case car il a heurté quelque chose.") . "<br/>";
                                        if (! $param["MOVE1"])
                                            $msg .= localize("Votre adversaire a été projeté en arrière mais a heurté quelque chose et n'a pas reculé.") . "<br/>";
                                        if (isset($param["NAME_PJO"])) {
                                            $msg .= "<br/>L'obstacle est le personnage " . $param["NAME_PJO"] . "(" . $param["PJO_ID"] . ") qui perd <b>" . $param["PJO_EXTRA_DAMAGE"] . "</b> point(s) de vie. <br/>";
                                            $msg .= "Votre adversaire a subit des dégâts supplémentaires et perd <b>" . $param["OPP_EXTRA_DAMAGE"] . "</b> point(s) de vie.<br/><br/>";
                                        }
                                        if (isset($param["NAME_BUILDING"])) {
                                            $msg .= "<br/>L'obstacle est le bâtiment " . $param["NAME_BUILDING"] . "(" . $param["BUILDING_ID"] . ") qui perd <b>" . $param["BUILDING_EXTRA_DAMAGE"] . "</b> point(s) de structure. <br/>";
                                            $msg .= "Votre adversaire a subit des dégâts supplémentaires et perd <b>" . $param["OPP_EXTRA_DAMAGE"] . "</b> point(s) de vie.<br/><br/>";
                                        }
                                    }
                                }
                                $msg .= "<br/>";
                            }
                        } else {
                            $msg .= localize("Vous n'êtes pas parvenu à toucher votre adversaire.") . "<br/ >";
                        }
                        $msg .= self::msgGainXP($param);
                    }
                } else {
                    $msg = "";
                    if ($param["PLAYER_IS_NPC"]) {
                        if (isset($param["PLAYER_GENDER"])) {
                            if ($param["PLAYER_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else {
                            $art = "";
                        }
                        
                        if (isset($param["PIEGE"]) && $param["PIEGE"]) {
                            $msg = "Vous avez été piègé par Araignée piégeuse géante (" . $param["PLAYER_ID"] . "<br/>";
                            $msg .= "Votre attaque a été inefficace et Araignée piègeuse géante rispote" . "<br/><br/>";
                        }
                        
                        $msg .= localize("Vous avez été attaqué par {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $art . " " . localize($param["PLAYER_NAME"]),
                                "level" => $param["PLAYER_LEVEL"],
                                "id" => $param["PLAYER_ID"]
                        )) . " <br/><br/>";
                    } else
                        $msg .= localize("Vous avez été attaqué par {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $param["PLAYER_NAME"],
                                "level" => $param["PLAYER_LEVEL"],
                                "id" => $param["PLAYER_ID"]
                        )) . " <br/><br/>";
                    
                    $msg .= self::msgBarrierEffect($param, 0);
                    
                    if (! $param["PLAYER_KILLED"]) {
                        if ($param["IDENT"] == ABILITY_LIGHT)
                            $msg .= localize("Le Jet de Maitrise de votre adversaire a été de : ");
                        else
                            $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
                        $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                        $msg .= localize("Votre Jet de défense a été de ");
                        $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                        
                        if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                            $msg .= localize("Votre adversaire vous a donc <b>TOUCHÉ</b> (Bonus de dégâts : {bonus})", array(
                                    "bonus" => $param["BONUS_DAMAGE2"]
                            )) . "<br/>";
                            if ($param["IDENT"] == ABILITY_TREACHEROUS) {
                                $msg .= "<br/>" . localize("Le Jet de vitesse de votre adversaire a été de ");
                                $msg .= "........................." . $param["PLAYER_SPEED"] . " <br/>";
                                $msg .= localize("Bonus de la griffe : {percent}% ", array(
                                        "percent" => (min(120, 50 + $param["PLAYER_SPEED"]))
                                )) . "<br/>";
                                $msg .= localize("Son bonus dégât était donc de {percent}", array(
                                        "percent" => $param["BONUS_DAMAGE"]
                                )) . "<br/><br/>";
                            }
                            
                            $msg .= localize("Son Jet de dégâts a été de ");
                            $msg .= "............: " . $param["TARGET_DAMAGE"] . " <br/>";
                            $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGETOT"]
                            )) . "<br/>";
                            
                            if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                                $pluriel = "";
                            
                            $msg .= self::msgPassiveBubble($param, 0);
                            
                            $knockout = "";
                            if ($param["IDENT"] == ABILITY_KNOCKOUT && $param["TARGET_KILLED"] == 0) {
                                if (isset($param["LEVEL_BENE"])) {
                                    $msg .= localize("Vous étiez  protégé par une bénédiction de niveau {level}.", array(
                                            "level" => $param["LEVEL_BENE"]
                                    )) . "<br/>";
                                    if ($param["CANCEL"])
                                        $msg .= localize("Votre protection était plus puissante que le niveau de compétence de votre adversaire et en a annulé les effets") . "<br/>";
                                    else
                                        $msg .= localize("Les effets de la compétence de votre adversaire ont été réduit de {level} à {niveau} points de blessure mortelle.", array(
                                                "level" => floor($param["TARGET_HP"] / 10) * 10,
                                                "niveau" => ($param["CURSE"] * 10)
                                        )) . "<br/>";
                                } else
                                    $knockout = "dont " . ($param["CURSE"] * 10) . " PV de blessure mortelle.";
                            }
                            
                            if ($param["IDENT"] == ABILITY_THRUST) {
                                $msg .= localize("Votre adversaire est parvenu à trouver une faille dans votre armure qui a été reduite de 80%") . "<br/>";
                                $msg .= localize("Vous avez perdu <b>{hp} point" . $pluriel . " de vie </b>", array(
                                        "hp" => $param["TARGET_HP"]
                                )) . "<br/>";
                            } else
                                $msg .= localize("Votre armure vous a protégé et vous n'avez perdu que <b>{hp} point" . $pluriel . " de vie </b>" . $knockout, array(
                                        "hp" => $param["TARGET_HP"]
                                )) . "<br/>";
                            
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez été <b>TUÉ</b>.");
                                $msg .= "<br/>";
                                $msg .= self::msgInfoDeath($param);
                            } else {
                                if ($param["IDENT"] == ABILITY_STUNNED) {
                                    if (floor($param["TARGET_HP"] / 10) > 0) {
                                        if (isset($param["LEVEL_BENE"])) {
                                            $msg .= localize("Vous étiez protégé par une bénédiction de niveau {level}.", array(
                                                    "level" => $param["LEVEL_BENE"]
                                            )) . "<br/>";
                                            if ($param["CANCEL"])
                                                $msg .= localize("Votre protection était plus puissante que son niveau de compétence et en a annulé les effets") . "<br/>";
                                            else
                                                $msg .= localize("Les effets de sa compétence ont été réduit de {level} à {niveau} dés de malus de vitesse.", array(
                                                        "level" => floor($param["TARGET_HP"] / 10),
                                                        "niveau" => ($param["CURSE"])
                                                )) . "<br/>";
                                        } else
                                            $msg .= localize("Vous avez été étourdi par votre adversaire. Vous avez subi un malus de {vit}D6 de vitesse pour les 2 prochains tours.", array(
                                                    "vit" => floor($param["TARGET_HP"] / 10)
                                            )) . "<br/>";
                                    } else
                                        $msg .= localize("Ses dégâts ont été trop faibles et vous n'avez pas été étourdi par votre adversaire") . "<br/>";
                                }
                                if ($param["IDENT"] == ABILITY_THRUST) {
                                    if (floor($param["TARGET_HP"] / 5) > 0) {
                                        if (isset($param["LEVEL_BENE"])) {
                                            $msg .= localize("Vous étiez protégé par une bénédiction de niveau {level}.", array(
                                                    "level" => $param["LEVEL_BENE"]
                                            )) . "<br/>";
                                            if ($param["CANCEL"])
                                                $msg .= localize("Votre protection était plus puissante que son niveau de compétence et en a annulé les effets") . "<br/>";
                                            else
                                                $msg .= localize("Les effets de sa compétence ont été réduit de {level} à {niveau} point(s) de vie pour les 3 prochains tours.", array(
                                                        "level" => floor($param["TARGET_HP"] / 5),
                                                        "niveau" => (floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"])
                                                )) . "<br/>";
                                        } else
                                            $msg .= localize("Votre adversaire vous a infligé une blessure profonde de {hp} point(s) de vie pour les 3 prochains tours.", array(
                                                    "hp" => floor($param["TARGET_HP"] / 5)
                                            )) . "<br/>";
                                    } else
                                        $msg .= localize("Ses dégâts ont été trop faibles et votre adversaire n'est pas parvenu pas à vous inliger une blessure profonde") . "<br/>";
                                }
                                
                                if ($param["IDENT"] == ABILITY_PROJECTION) {
                                    if (! isset($param["MOVE1"])) {
                                        $msg .= localize("Vous avez eu assez de force pour resister à la projection.") . "<br/>";
                                    } else {
                                        if ($param["MOVE2"])
                                            $msg .= localize("Vous avez été projeté en arrière et vous avez reculé de 2 cases") . "<br/>";
                                        if ($param["MOVE1"] && ! $param["MOVE2"])
                                            $msg .= localize("Vous avez été projeté en arrière mais vous n'avez reculé que d'une case car vous avez heurté quelque chose.") . "<br/>";
                                        if (! $param["MOVE1"])
                                            $msg .= localize("Vous avez été projeté en arrière mais vous avez heurté quelque chose et vous n'avez pas reculé.") . "<br/>";
                                        if (isset($param["NAME_PJO"])) {
                                            $msg .= "<br/>L'obstacle est le personnage " . $param["NAME_PJO"] . "(" . $param["PJO_ID"] . ") qui perd <b>" . $param["PJO_EXTRA_DAMAGE"] . "</b> point(s) de vie <br/>";
                                            $msg .= "Vous avez subit des dégâts supplémentaires et perdez <b>" . $param["OPP_EXTRA_DAMAGE"] . "</b> point(s) de vie.<br/><br/>";
                                        }
                                    }
                                }
                                if ($param["IDENT"] == M_POISON) {
                                    $msg .= "<br>";
                                    if (isset($param["LEVEL_BENE"])) {
                                        $msg .= "Vous avez été empoisonné par " . $param["PLAYER_NAME"];
                                        $msg .= localize("Vous étiez  protégé par une bénédiction de niveau {level}.", array(
                                                "level" => $param["LEVEL_BENE"]
                                        )) . "<br/>";
                                        if ($param["CANCEL"])
                                            $msg .= localize("Votre protection était plus puissante que le niveau de compétence de votre adversaire et en a annulé les effets") . "<br/>";
                                        else
                                            $msg .= localize("Les effets de la compétence de votre adversaire ont été réduit de {level} à {niveau} points de poison pour les trois prochaines DLA.", array(
                                                    "level" => ($param["LEVEL_CURSE"] + $param["LEVEL_BENE"]) * 3,
                                                    "niveau" => ($param["LEVEL_CURSE"]) * 3
                                            )) . "<br/>";
                                    } else
                                        $msg .= "Vous avez été empoisonné de <b>" . ($param["LEVEL_CURSE"] * 3) . " points de vie </b> pour les 3 DLA suivantes.";
                                }
                            }
                        } else {
                            $msg .= localize("Votre adversaire n'est pas parvenu à vous toucher.") . "<br/ >";
                        }
                    }
                }
            }
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgInterposition (&$param, $src = 1)
    {
        $msg = "";
        
        $pluriel = "";
        if ($src) {
            if ($param["TARGET_IS_NPC"]) {
                if (isset($param["TARGET_GENDER"])) {
                    if ($param["TARGET_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $art . " " . localize($param["TARGET_NAME"]),
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $param["TARGET_NAME"],
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            
            $msg .= self::msgBarrierEffect($param, 1);
            
            if (! $param["PLAYER_KILLED"]) {
                if ($param["IDENT"] == ABILITY_LIGHT)
                    $msg .= localize("Votre Jet de Maitrise de la magie a été de : ");
                else
                    $msg .= localize("Votre Jet d'Attaque a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize(" (Bonus de dégâts : {bonus})", array(
                        "bonus" => $param["BONUS_DAMAGE"]
                )) . "<br/>";
                if ($param["IDENT"] == ABILITY_TREACHEROUS) {
                    $msg .= "<br/>" . localize("Votre Jet de vitesse a été de ");
                    $msg .= "............: " . $param["PLAYER_SPEED"] . " $<br/>";
                    $msg .= localize("Bonus de la griffe : {percent}% ", array(
                            "percent" => min(120, 50 + $param["PLAYER_SPEED"])
                    )) . "<br/>";
                    $msg .= localize("Vous bonus dégât était donc de {percent}", array(
                            "percent" => $param["BONUS_DAMAGE"]
                    )) . "<br/><br/>";
                }
                $msg .= localize("Votre Jet de Dégâts a été de ");
                $msg .= "........................: " . $param["TARGET_DAMAGE"] . "<br/>";
                $msg .= localize("Vous lui avez donc infligé {damage} points de dégâts.", array(
                        "damage" => $param["TARGET_DAMAGETOT"]
                )) . "<br/>";
                
                $msg .= self::msgPassiveBubble($param, 1);
                
                $msg .= localize("Le Jet de maitrise de magie de votre adversaire a été de ");
                $msg .= localize("........................: " . $param["TARGET_MM"]) . "$<br/>";
                $msg .= localize("Son bonus d'armure etait donc de {bonus}%", array(
                        "bonus" => $param["ARMOR_BONUS"]
                )) . "<br/>";
                
                if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                    $pluriel = "";
                $knockout = "";
                if ($param["IDENT"] == ABILITY_KNOCKOUT && $param["TARGET_KILLED"] == 0)
                    $knockout = "dont " . (floor($param["TARGET_HP"] / 10) * 10) . " PV de blessure mortelle.";
                
                if ($param["IDENT"] == ABILITY_THRUST) {
                    $msg .= localize("Vous êtes parvenu à trouver une faille dans l'armure de votre adversaire qui est reduite de 80%") . "<br/>";
                    $msg .= localize("Il a perdu <b>{hp} point" . $pluriel . " de vie </b>", array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                } else
                    $msg .= localize("Son armure l'a protègé et il n'a perdu que <b>{hp} point" . $pluriel . " de vie </b>" . $knockout, array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= "<br/>";
                    $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                    $msg .= "<br/>";
                } else {
                    if ($param["IDENT"] == ABILITY_STUNNED) {
                        if (floor($param["TARGET_HP"] / 10) > 0)
                            $msg .= localize("Vous avez étourdi votre adversaire qui a sublit un malus de {vit}D6 de vitesse pour 2 DLA.", array(
                                    "vit" => floor($param["TARGET_HP"] / 10)
                            )) . "<br/>";
                        else
                            $msg .= localize("Vos dégâts étaient trop faibles et vous n'etes pas parvenu pas à étourdir votre adversaire") . "<br/>";
                    }
                    
                    if ($param["IDENT"] == ABILITY_THRUST) {
                        if (floor($param["TARGET_HP"] / 5) > 0) {
                            if (isset($param["LEVEL_BENE"])) {
                                $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                        "level" => $param["LEVEL_BENE"]
                                )) . "<br/>";
                                if ($param["CANCEL"])
                                    $msg .= localize("Sa protection était plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                                else
                                    $msg .= localize("Les effets de votre compétence ont été réduit de {level} à {niveau} point(s) de vie pour les 3 prochains tours.", array(
                                            "level" => floor($param["TARGET_HP"] / 5),
                                            "niveau" => (floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"])
                                    )) . "<br/>";
                            } else
                                $msg .= localize("Vous avez infligez à votre adversaire ({id}) une blessure profonde de {hp} point(s) de vie pour les 3 prochains tours.", array(
                                        "hp" => floor($param["TARGET_HP"] / 5),
                                        "id" => $param["TARGET_ID"]
                                )) . "<br/>";
                        } else
                            $msg .= localize("Vos dégâts ont été trop faibles et vous n'êtes pas parvenu pas à infliger une blessure profonde à votre adversaire") . "<br/>";
                    }
                    
                    if ($param["IDENT"] == ABILITY_PROJECTION) {
                        if (! isset($param["MOVE1"])) {
                            $msg .= localize("Vous n'avez pas eu assez de force pour projeter votre adversaire.") . "<br/>";
                        } else {
                            if ($param["MOVE2"])
                                $msg .= localize("Votre adversaire a été projeté en arrière et a reculé de 2 cases") . "<br/>";
                            if ($param["MOVE1"] && ! $param["MOVE2"])
                                $msg .= localize("Votre adversaire a été projeté en arrière mais n'a reculé que d'une case car il a heurté quelque chose.") . "<br/>";
                            if (! $param["MOVE1"])
                                $msg .= localize("Votre adversaire a été projeté en arrière mais a heurté quelque chose et n'a pas reculé.") . "<br/>";
                        }
                    }
                    
                    $msg .= "<br/>";
                }
                $msg .= self::msgGainXP($param);
            }
        } else {
            if ($param["PLAYER_IS_NPC"]) {
                if (isset($param["PLAYER_GENDER"])) {
                    if ($param["PLAYER_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}", array(
                        "target" => $art . " " . localize($param["PLAYER_NAME"]),
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}  ", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            
            $msg .= self::msgBarrierEffect($param, 0);
            
            if (! $param["PLAYER_KILLED"]) {
                $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                if ($param["IDENT"] == ABILITY_TREACHEROUS) {
                    $msg .= "<br/>" . localize("Le Jet de vitesse de votre adversaire a été de ");
                    $msg .= "........................:" . $param["PLAYER_SPEED"] . " <br/>";
                    $msg .= localize("Bonus de la griffe : {percent}% ", array(
                            "percent" => min(120, 50 + $param["PLAYER_SPEED"])
                    )) . "<br/>";
                    $msg .= localize("Son bonus dégât était donc de {percent}", array(
                            "percent" => $param["BONUS_DAMAGE"]
                    )) . "<br/><br/>";
                } else
                    $msg .= localize(" (Bonus de dégâts : {bonus})", array(
                            "bonus" => $param["BONUS_DAMAGE"]
                    )) . "<br/>";
                $msg .= localize("Son Jet de Dégâts a été de ");
                $msg .= "........................: " . $param["TARGET_DAMAGE"] . "<br/>";
                $msg .= localize("Il vous a infligé donc {damage} points de dégâts.", array(
                        "damage" => $param["TARGET_DAMAGETOT"]
                )) . "<br/><br/>";
                
                $msg .= self::msgPassiveBubble($param, 0);
                
                $msg .= localize("Votre Jet de maitrise de magie a été de ");
                $msg .= localize("........................: " . $param["TARGET_MM"]) . "<br/>";
                $msg .= localize("Votre bonus d'armure était donc de {bonus}%", array(
                        "bonus" => $param["ARMOR_BONUS"]
                )) . "<br/>";
                if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                    $pluriel = "";
                
                $knockout = "";
                if ($param["IDENT"] == ABILITY_KNOCKOUT && $param["TARGET_KILLED"] == 0)
                    $knockout = "dont " . (floor($param["TARGET_HP"] / 10) * 10) . " PV de blessure mortelle.";
                
                if ($param["TARGET_HP"] > 1)
                    $pluriel = "s";
                
                if ($param["IDENT"] == ABILITY_THRUST) {
                    $msg .= localize("Votre adversaire est parvenu à trouver une faille dans votre armure qui a été reduite de 80%") . "<br/>";
                    $msg .= localize("Vous avez perdu <b>{hp} point" . $pluriel . " de vie </b>", array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                } else
                    $msg .= localize("Votre armure vous a protégé et vous n'avez perdu que <b>{hp} point" . $pluriel . " de vie </b>" . $knockout, array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= "<br/>";
                    $msg .= localize("Vous avez été <b>TUÉ</b>.");
                    $msg .= "<br/>";
                    $msg .= self::msgInfoDeath($param);
                } else {
                    if ($param["IDENT"] == ABILITY_STUNNED) {
                        if (floor($param["TARGET_HP"] / 10) > 0)
                            $msg .= localize("Vous avez été étourdi par votre adversaire et avez subit un malus de {vit}D6 de vitesse pour 2 DLA.", array(
                                    "vit" => floor($param["TARGET_HP"] / 10)
                            )) . "<br/>";
                        else
                            $msg .= localize("Ses dégâts étaient trop faibles et votre adversaire n'est pas parvenu à vous étourdir") . "<br/>";
                    }
                    
                    if ($param["IDENT"] == ABILITY_THRUST) {
                        if (floor($param["TARGET_HP"] / 5) > 0) {
                            if (isset($param["LEVEL_BENE"])) {
                                $msg .= localize("Vous étiez protégé par une bénédiction de niveau {level}.", array(
                                        "level" => $param["LEVEL_BENE"]
                                )) . "<br/>";
                                if ($param["CANCEL"])
                                    $msg .= localize("Votre protection était plus puissante que son niveau de compétence et en a annulé les effets") . "<br/>";
                                else
                                    $msg .= localize("Les effets de sa compétence ont été réduit de {level} à {niveau} point(s) de vie pour les 3 prochains tours.", array(
                                            "level" => floor($param["TARGET_HP"] / 5),
                                            "niveau" => (floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"])
                                    )) . "<br/>";
                            } else
                                $msg .= localize("Votre adversaire vous a infligé une blessure profonde de {hp} point(s) de vie pour les 3 prochains tours.", array(
                                        "hp" => floor($param["TARGET_HP"] / 5)
                                )) . "<br/>";
                        } else
                            $msg .= localize("Ses dégâts ont été trop faibles et votre adversaire n'est pas parvenu pas à vous inliger une blessure profonde") . "<br/>";
                    }
                    
                    if ($param["IDENT"] == ABILITY_PROJECTION) {
                        if (! isset($param["MOVE1"])) {
                            $msg .= localize("Vous avez eu assez de force pour resister à la projection.") . "<br/>";
                        } else {
                            if ($param["MOVE2"])
                                $msg .= localize("Vous avez été projeté en arrière et vous avez reculé de 2 cases") . "<br/>";
                            if ($param["MOVE1"] && ! $param["MOVE2"])
                                $msg .= localize("Vous avez été projeté en arrière mais vous n'avez reculé que d'une case car vous avez heurté quelque chose.") . "<br/>";
                            if (! $param["MOVE1"])
                                $msg .= localize("VOus avez été projeté en arrière mais vous avez heurté quelque chose et vous n'avez pas reculé.") . "<br/>";
                        }
                    }
                }
            }
        }
        
        return $msg . "<br/>&nbsp;";
    }

    function msgInterBurning (&$param, $src = 1)
    {
        $msg = "";
        
        $pluriel = "";
        if ($src) {
            if ($param["TARGET_IS_NPC"]) {
                if (isset($param["TARGET_GENDER"])) {
                    if ($param["TARGET_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2})  s'est interposé ", array(
                        "target" => $art . " " . localize($param["TARGET_NAME"]),
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $param["TARGET_NAME"],
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            
            $msg .= self::msgBarrierEffect($param, 1);
            
            if (! $param["PLAYER_KILLED"]) {
                $msg .= localize("Votre Jet d'Attaque magique a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Votre jet de maitrise de la magie a été de {mm}", array(
                        "mm" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                        "nb" => $param["SPELL_LEVEL"] * 1.7,
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                        "damage" => $param["TARGET_DAMAGE"]
                )) . "<br/><br/>";
                $msg .= self::msgPassiveBubble($param, 1);
                $msg .= localize("Le Jet de maitrise de magie de votre adversaire a été de ");
                $msg .= localize("........................: " . $param["TARGET_MM"]) . "<br/>";
                $msg .= localize("Son bonus d'armure etait donc de {bonus}%", array(
                        "bonus" => $param["ARMOR_BONUS"]
                )) . "<br/>";
                
                if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                    $pluriel = "";
                $msg .= localize("Son armure l'a protègé et il n'a perdu que <b>{hp} point" . $pluriel . " de vie </b>", array(
                        "hp" => $param["TARGET_HP"]
                )) . "<br/>";
                
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= "<br/>";
                    $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                    $msg .= "<br/>";
                }
                
                $msg .= "<br/>";
            }
            $msg .= self::msgGainXP($param);
        } else {
            if ($param["PLAYER_IS_NPC"]) {
                if (isset($param["PLAYER_GENDER"])) {
                    if ($param["PLAYER_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}", array(
                        "target" => $art . " " . localize($param["PLAYER_NAME"]),
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}  ", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            
            $msg .= self::msgBarrierEffect($param, 0);
            
            if (! $param["PLAYER_KILLED"]) {
                $msg .= localize("Le jet d'Attaque magique de votre adversaire a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de :");
                $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/>";
                
                $msg .= localize("Il a donc réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                        "nb" => ($param["SPELL_LEVEL"] * 1.5),
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                        "damage" => $param["TARGET_DAMAGE"]
                )) . "<br/><br/>";
                $msg .= self::msgPassiveBubble($param, 0);
                
                $msg .= localize("Votre Jet de maitrise de magie a été de ");
                $msg .= localize("........................: " . $param["TARGET_MM"]) . "<br/>";
                $msg .= localize("Votre bonus d'armure etait donc de {bonus}%", array(
                        "bonus" => $param["ARMOR_BONUS"]
                )) . "<br/>";
                if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                    $pluriel = "";
                
                $msg .= localize("Vous armure vous protège et vous n'avez perdu que <b>{hp} point" . $pluriel . " de vie </b>", array(
                        "hp" => $param["TARGET_HP"]
                )) . "<br/>";
                
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= "<br/>";
                    $msg .= localize("Vous avez été <b>TUÉ</b>.");
                    $msg .= "<br/>";
                    $msg .= self::msgInfoDeath($param);
                }
            }
        }
        
        return $msg . "<br/>&nbsp;";
    }

    function msgInterFireball (&$param, $src = 1)
    {
        $msg = "";
        
        $pluriel = "";
        if ($src) {
            if ($param["TARGET_IS_NPC"]) {
                if (isset($param["TARGET_GENDER"])) {
                    if ($param["TARGET_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $art . " " . localize($param["TARGET_NAME"]),
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $param["TARGET_NAME"],
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            
            $msg .= localize("Votre Jet d'Attaque magique a été de ");
            $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
            $msg .= localize("Votre jet de maitrise de la magie a été de {mm}", array(
                    "mm" => $param["PLAYER_MM"]
            )) . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                    "nb" => 1.2 * $param["SPELL_LEVEL"],
                    "level" => $param["SPELL_LEVEL"]
            ));
            $msg .= "<br/>";
            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                    "damage" => $param["TARGET_DAMAGE"]
            )) . "<br/><br/>";
            $msg .= self::msgPassiveBubble($param, 1);
            $msg .= localize("Le Jet de maitrise de magie de votre adversaire a été de ");
            $msg .= localize("........................: " . $param["TARGET_MM"]) . "<br/>";
            $msg .= localize("Son bonus d'armure etait donc de {bonus}%", array(
                    "bonus" => $param["ARMOR_BONUS"]
            )) . "<br/>";
            
            if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                $pluriel = "";
            $msg .= localize("Son armure l'a protègé et il n'a perdu que <b>{hp} point" . $pluriel . " de vie </b>", array(
                    "hp" => $param["TARGET_HP"]
            )) . "<br/>";
            
            if ($param["TARGET_KILLED"] == 1) {
                $msg .= "<br/>";
                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                $msg .= "<br/>";
            }
            
            $msg .= "<br/>";
            
            $msg .= self::msgGainXP($param);
        } else {
            if ($param["PLAYER_IS_NPC"]) {
                if (isset($param["PLAYER_GENDER"])) {
                    if ($param["PLAYER_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}", array(
                        "target" => $art . " " . localize($param["PLAYER_NAME"]),
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}  ", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            
            $msg .= localize("Le jet d'Attaque magique de votre adversaire a été de ");
            $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
            $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de :");
            $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/>";
            
            $msg .= localize("Il a donc réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                    "nb" => $param["SPELL_LEVEL"] * 1.2,
                    "level" => $param["SPELL_LEVEL"]
            ));
            $msg .= "<br/>";
            $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                    "damage" => $param["TARGET_DAMAGE"]
            )) . "<br/><br/>";
            $msg .= self::msgPassiveBubble($param, 0);
            
            $msg .= localize("Votre Jet de maitrise de magie a été de ");
            $msg .= localize("........................: " . $param["TARGET_MM"]) . "<br/>";
            $msg .= localize("Votre bonus d'armure etait donc de {bonus}%", array(
                    "bonus" => $param["ARMOR_BONUS"]
            )) . "<br/>";
            if ($param["TARGET_HP"] == 1 || $param["TARGET_HP"] == 0)
                $pluriel = "";
            
            $msg .= localize("Vous armure vous protège et vous n'avez perdu que <b>{hp} point" . $pluriel . " de vie </b>", array(
                    "hp" => $param["TARGET_HP"]
            )) . "<br/>";
            
            if ($param["TARGET_KILLED"] == 1) {
                $msg .= "<br/>";
                $msg .= localize("Vous avez été <b>TUÉ</b>.");
                $msg .= "<br/>";
                $msg .= self::msgInfoDeath($param);
            }
        }
        
        return $msg . "<br/>&nbsp;";
    }

    function msgArcheryInterposition (&$param, $src = 1)
    {
        $msg = "";
        
        $pluriel = "";
        if ($src) {
            if ($param["TARGET_IS_NPC"]) {
                if (isset($param["TARGET_GENDER"])) {
                    if ($param["TARGET_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $art . " " . localize($param["TARGET_NAME"]),
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez attaqué {target} ({id1}) mais {name} ({id2}) s'est interposé ", array(
                        "target" => $param["TARGET_NAME"],
                        "name" => $param["NAME"],
                        "id1" => $param["INTER_ID"],
                        "id2" => $param["TARGET_ID"]
                )) . " <br/><br/>";
            
            $msg .= localize("Votre Jet d'Attaque a été de ");
            $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
            
            $msg .= localize("Votre bonus aux dégâts a été de {bonus} point(s)", array(
                    "bonus" => $param["BONUS_DAMAGE"]
            )) . "<br/>";
            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                    "damage" => $param["TARGET_DAMAGE"]
            )) . "<br/>";
            $msg .= self::msgPassiveBubble($param, 1);
            
            if ($param["TARGET_DAMAGETOT2"] > 0) {
                $msg .= localize("Le Jet de maitrise de magie de votre adversaire a été de ");
                $msg .= localize("........................: " . $param["TARGET_MM"]) . "$<br/>";
                $msg .= localize("Son bonus d'armure etait donc de {bonus}%", array(
                        "bonus" => $param["ARMOR_BONUS"]
                )) . "<br/>";
            }
            $msg .= localize("Il a donc perdu {damage} points de de vie.", array(
                    "damage" => $param["TARGET_HP"]
            )) . "<br/>";
            
            if ($param["TARGET_KILLED"] == 1) {
                $msg .= "<br/>";
                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                $msg .= "<br/>";
            } else {
                if ($param["IDENT"] == ABILITY_DISABLING) {
                    if ($param["TARGET_HP"] > 0)
                        $msg .= localize("Vous avez blessé votre adversaire à la jambe. Ses déplacements lui coûteront 1PA supplémentaire pour la DLA en cours et la suivante.") . "<br/>";
                    else
                        $msg .= localize("Vos dégâts n'ont pas été suffisants pour infliger un malus de déplacement à votre adversaire") . "<br/>";
                }
            }
            if ($param["IDENT"] == ABILITY_NEGATION) {
                $msg .= "<br/>";
                if ($param["TARGET_HP"] > 0) {
                    $msg .= localize("Votre Jet de maitrise de la magie a été de ");
                    $msg .= "........................: " . $param["MAGICSKILL"] . "<br/>";
                    if (isset($param["LEVEL_BENE"])) {
                        $msg .= localize("Votre adversaire etait protégé par une bénédiction de niveau {level}.", array(
                                "level" => $param["LEVEL_BENE"]
                        )) . "<br/>";
                        if ($param["CANCEL"])
                            $msg .= localize("Sa protection etait plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                        else
                            $msg .= localize("Les effets de votre compétence ont été réduits de {level} à {niveau} dés de malus de magie.", array(
                                    "level" => $param["LEVEL_SPELL"],
                                    "niveau" => ($param["CURSE"])
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vous avez infligé un malus de {nb} D6 de MM à votre adversaire pour la DLA en cours et la suivante.", array(
                                "nb" => (1 + floor($param["MAGICSKILL"] / 8))
                        )) . "<br/>";
                } else
                    $msg .= localize("Vos dégâts n'ont pas été suffisants pour infliger un malus de magie à votre adversaire.", array(
                            "nb" => $param["CURSE"]
                    )) . "<br/>";
            }
            $msg .= self::msgGainXP($param);
        } 

        else {
            if ($param["PLAYER_IS_NPC"]) {
                if (isset($param["PLAYER_GENDER"])) {
                    if ($param["PLAYER_GENDER"] == "M")
                        $art = localize("un");
                    else
                        $art = localize("une");
                } else {
                    $art = "";
                }
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}", array(
                        "target" => $art . " " . localize($param["PLAYER_NAME"]),
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            } else
                $msg = localize("Vous avez subit l'attaque de {target} ({id}) pour défendre {name}  ", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "name" => $param["NAME"]
                )) . " <br/><br/>";
            
            $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
            $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
            $msg .= localize(" (Bonus de dégâts : {bonus})", array(
                    "bonus" => $param["BONUS_DAMAGE"]
            )) . "<br/>";
            $msg .= localize("Il vous a infligé donc {damage} points de dégâts.", array(
                    "damage" => $param["TARGET_DAMAGE"]
            )) . "<br/><br/>";
            $msg .= self::msgPassiveBubble($param, 0);
            
            if ($param["TARGET_DAMAGETOT2"] > 0) {
                $msg .= localize("Votre Jet de maitrise de magie a été de ");
                $msg .= localize("........................: " . $param["TARGET_MM"]) . "$<br/>";
                $msg .= localize("Votre bonus d'armure etait donc de {bonus}%", array(
                        "bonus" => $param["ARMOR_BONUS"]
                )) . "<br/>";
            }
            $msg .= localize("vous avez donc perdu {damage} points de de vie.", array(
                    "damage" => $param["TARGET_HP"]
            )) . "<br/>";
            
            if ($param["TARGET_KILLED"] == 1) {
                $msg .= "<br/>";
                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                $msg .= "<br/>";
            } else {
                if ($param["IDENT"] == ABILITY_DISABLING) {
                    if ($param["TARGET_HP"] > 0)
                        $msg .= localize("Vous avez été blessé à la jambe. Vos déplacements vous ont couté 1PA supplémentaire pour la DLA en cours et la suivante.") . "<br/>";
                    else
                        $msg .= localize("Les dégâts de votre adversaire n'ont pas été suffisants pour vous infliger un malus de déplacement") . "<br/>";
                }
                
                if ($param["IDENT"] == ABILITY_NEGATION) {
                    $msg .= "<br/>";
                    if ($param["TARGET_HP"] > 0) {
                        $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de ");
                        $msg .= "........................: " . $param["MAGICSKILL"] . "<br/>";
                        if (isset($param["LEVEL_BENE"])) {
                            $msg .= localize("Vous etiez protégé par une bénédiction de niveau {level}.", array(
                                    "level" => $param["LEVEL_BENE"]
                            )) . "<br/>";
                            if ($param["CANCEL"])
                                $msg .= localize("Votre protection etait plus puissante que votre niveau de compétence de votre adversaire et en a annulé les effets") . "<br/>";
                            else
                                $msg .= localize("Les effets de la compétence de votre adversaire ont été réduits de {level} à {niveau} dés de malus de magie.", array(
                                        "level" => $param["LEVEL_SPELL"],
                                        "niveau" => ($param["CURSE"])
                                )) . "<br/>";
                        } else
                            $msg .= localize("Votre adversaire vous a infligé un malus de {nb} D6 de MM pour la DLA en cours et la suivante.", array(
                                    "nb" => (1 + floor($param["MAGICSKILL"] / 8))
                            )) . "<br/>";
                    } else
                        $msg .= localize("Les dégât de votre adversaire n'ont pas été suffisants pour vous infliger un malus de magie.", array(
                                "nb" => $param["CURSE"]
                        )) . "<br/>";
                }
            }
        }
        
        return $msg . "<br/>&nbsp;";
    }

    function msgArchery ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($param["PROTECTION"])
                $msg .= self::msgArcheryInterposition($param, $src);
            else {
                if ($src) {
                    if ($param["TARGET_IS_NPC"]) {
                        if (isset($param["TARGET_GENDER"])) {
                            if ($param["TARGET_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else {
                            $art = "";
                        }
                        $msg = localize("Vous avez attaqué {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $art . " " . localize($param["TARGET_NAME"]),
                                "level" => $param["TARGET_LEVEL"],
                                "id" => $param["TARGET_ID"]
                        )) . " <br/><br/>";
                    } else
                        $msg = localize("Vous avez attaqué {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $param["TARGET_NAME"],
                                "level" => $param["TARGET_LEVEL"],
                                "id" => $param["TARGET_ID"]
                        )) . " <br/><br/>";
                    
                    $msg .= localize("Votre Jet d'Attaque a été de ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                        $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                        $msg .= localize("Votre bonus aux dégâts a été de {bonus} point(s)", array(
                                "bonus" => $param["BONUS_DAMAGE"]
                        )) . "<br/>";
                        $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_DAMAGE"]
                        )) . "<br/>";
                        
                        $msg .= self::msgPassiveBubble($param, 1);
                        $msg .= localize("Son armure l'a protégé et il n'a perdu que <b>{hp} point de vie </b>", array(
                                "hp" => $param["TARGET_HP"]
                        )) . "<br/>";
                        
                        if ($param["TARGET_KILLED"] == 1) {
                            $msg .= "<br/>";
                            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                            $msg .= "<br/>";
                        } elseif ($param["TYPE_ATTACK"] == M_STUN_EVENT) {
                            $msg .= "<br/>";
                            $msg .= localize("Vous avez <b>assommé</b> votre adversaire.");
                        } else {
                            if ($param["IDENT"] == ABILITY_DISABLING) {
                                if ($param["TARGET_HP"] > 0)
                                    $msg .= localize("Vous avez blessé votre adversaire à la jambe.<br/> Ses déplacements lui coûteront 1PA supplémentaire pour la DLA en cours et la suivante.") . "<br/>";
                                else
                                    $msg .= localize("Vos dégâts n'ont pas été suffisants pour infliger un malus de déplacement à votre adversaire") . "<br/>";
                            }
                        }
                        if ($param["IDENT"] == ABILITY_NEGATION) {
                            $msg .= "<br/>";
                            if ($param["TARGET_HP"] > 0) {
                                
                                $msg .= localize("Votre Jet de maitrise de la magie a été de ");
                                $msg .= "........................: " . $param["MAGICSKILL"] . "<br/>";
                                if (isset($param["LEVEL_BENE"])) {
                                    $msg .= localize("Votre adversaire etait protégé par une bénédiction de niveau {level}.", array(
                                            "level" => $param["LEVEL_BENE"]
                                    )) . "<br/>";
                                    if ($param["CANCEL"])
                                        $msg .= localize("Sa protection etait plus puissante que votre niveau de compétence et en a annulé les effets") . "<br/>";
                                    else
                                        $msg .= localize("Les effets de votre compétence ont été réduits de {level} à {niveau} dés de malus de magie.", array(
                                                "level" => $param["LEVEL_SPELL"],
                                                "niveau" => ($param["CURSE"])
                                        )) . "<br/>";
                                } else
                                    $msg .= localize("Vous avez infligé un malus de {nb} D6 de MM à votre adversaire pour la DLA en cours et la suivante.", array(
                                            "nb" => (1 + floor($param["MAGICSKILL"] / 8))
                                    )) . "<br/>";
                            } else
                                $msg .= localize("Vos dégâts n'ont pas été suffisants pour infliger un malus de magie à votre adversaire.", array(
                                        "nb" => $param["CURSE"]
                                )) . "<br/>";
                        }
                    } else {
                        $msg .= localize("Vous n'êtes pas parvenu pas à toucher votre adversaire.") . "<br/ >";
                    }
                    $msg .= self::msgGainXP($param);
                } else {
                    if ($param["PLAYER_IS_NPC"]) {
                        if (isset($param["PLAYER_GENDER"])) {
                            if ($param["PLAYER_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else {
                            $art = "";
                        }
                        $msg = localize("Vous avez été attaqué par {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $art . " " . localize($param["PLAYER_NAME"]),
                                "level" => $param["PLAYER_LEVEL"],
                                "id" => $param["PLAYER_ID"]
                        )) . " <br/><br/>";
                    } else
                        $msg = localize("Vous avez été attaqué par {target} ({id}) (Niveau : {level}) ", array(
                                "target" => $param["PLAYER_NAME"],
                                "level" => $param["PLAYER_LEVEL"],
                                "id" => $param["PLAYER_ID"]
                        )) . " <br/><br/>";
                    
                    $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Votre Jet de défense a été de ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                        $msg .= localize("Votre adversaire vous a donc <b>TOUCHÉ</b>") . "<br/>";
                        $msg .= localize("Son bonus de dégâts a été de {bonus} point(s)", array(
                                "bonus" => $param["BONUS_DAMAGE"]
                        )) . "<br/>";
                        $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_DAMAGE"]
                        )) . "<br/>";
                        
                        $msg .= self::msgPassiveBubble($param, 0);
                        $msg .= localize("Votre armure vous a protégé et vous n'avez perdu que <b>{hp} point de vie </b>", array(
                                "hp" => $param["TARGET_HP"]
                        )) . "<br/>";
                        
                        if ($param["TARGET_KILLED"] == 1) {
                            $msg .= "<br/>";
                            $msg .= localize("Vous avez été <b>TUÉ</b>.");
                            $msg .= "<br/>";
                            $msg .= self::msgInfoDeath($param);
                        } elseif ($param["TYPE_ATTACK"] == M_STUN_EVENT) {
                            $msg .= "<br/>";
                            $msg .= localize("Vous avez été <b>assommé</b> par votre adversaire.");
                        } else {
                            if ($param["IDENT"] == ABILITY_DISABLING) {
                                if ($param["TARGET_HP"] > 0)
                                    $msg .= localize("Vous avez été blessé à la jambe.<br/> Vos déplacements vous ont couté 1PA supplémentaire pour la DLA en cours et la suivante.") . "<br/>";
                                else
                                    $msg .= localize("Les dégâts de votre adversaire n'ont pas été suffisants pour vous infliger un malus de déplacement") . "<br/>";
                            }
                        }
                        if ($param["IDENT"] == ABILITY_NEGATION) {
                            $msg .= "<br/>";
                            if ($param["TARGET_HP"] > 0) {
                                $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de ");
                                $msg .= "........................: " . $param["MAGICSKILL"] . "<br/>";
                                if (isset($param["LEVEL_BENE"])) {
                                    $msg .= localize("Vous etiez protégé par une bénédiction de niveau {level}.", array(
                                            "level" => $param["LEVEL_BENE"]
                                    )) . "<br/>";
                                    if ($param["CANCEL"])
                                        $msg .= localize("Votre protection etait plus puissante que votre niveau de compétence de votre adversaire et en a annulé les effets") . "<br/>";
                                    else
                                        $msg .= localize("Les effets de la compétence de votre adversaire ont été réduits de {level} à {niveau} dés de malus de magie.", array(
                                                "level" => $param["LEVEL_SPELL"],
                                                "niveau" => ($param["CURSE"])
                                        )) . "<br/>";
                                } else
                                    $msg .= localize("Votre adversaire vous a infligé un malus de {nb} D6 de MM pour la DLA en cours et la suivante.", array(
                                            "nb" => (1 + floor($param["MAGICSKILL"] / 8))
                                    )) . "<br/>";
                            } else
                                $msg .= localize("Les dégât de votre adversaire n'ont pas été suffisants pour vous infliger un malus de magie.", array(
                                        "nb" => $param["CURSE"]
                                )) . "<br/>";
                        }
                    } else {
                        $msg .= localize("Vous adversaire n'est pas parvenu à vous toucher.") . "$<br/ >";
                    }
                }
            }
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    /* ************************************************************* LES COMPETENCES DE BASE ******************************************************* */
    function msgFirstAid ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé premiers soins sur {target} ({id})", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre jet de dextérité a été de : {dex}", array(
                        "dex" => $param["PLAYER_DEXTERITY"]
                )) . "<br/>";
                $msg .= localize("{target} a été soigné et a récupéré {hp} points de vie", array(
                        "target" => $param["TARGET_NAME"],
                        "hp" => $param["HP_HEAL"]
                )) . "<br/><br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé premiers soins sur vous", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son jet de dextérité a été de : {dex}", array(
                        "dex" => $param["PLAYER_DEXTERITY"]
                )) . "<br/>";
                $msg .= localize("Vous avez été soigné et vous avez récupéré {hp} points de vie", array(
                        "hp" => $param["HP_HEAL"]
                )) . "<br/><br/>";
            }
            
            return $msg;
        }
    }

    function msgGuard ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Vous avez utilisé Garde") . "<br/><br/>";
            $msg .= localize("Votre Jet de Défense a été de ");
            $msg .= "........................: " . $param["PLAYER_DEFENSE"] . " <br/>";
            $msg .= localize("Votre défense a donc été augmenté de {def}D6 jusqu'à votre prochaine DLA", array(
                    "def" => $param["GAIN_DEFENSE"]
            )) . "<br/><br/>";
            
            $msg .= localize("Cette action vous a couté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
            
            return $msg;
        }
    }

    /* ************************************************************* LES SORTILEGES DE BASE ******************************************************* */
    function msgBurning ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            if ($param["PROTECTION"] == 1)
                $msg .= self::msgInterBurning($param, $src);
            else {
                if ($src) {
                    if ($param["TARGET_IS_NPC"]) {
                        if (isset($param["TARGET_GENDER"])) {
                            if ($param["TARGET_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else
                            $art = "";
                        
                        $msg = localize("Vous avez attaqué {target} ({id}) ", array(
                                "target" => $art . " " . localize($param["TARGET_NAME"]),
                                "id" => $param["TARGET_ID"]
                        )) . "<br/><br/>";
                    } else
                        $msg = localize("Vous avez attaqué {target} ({id})", array(
                                "target" => $param["TARGET_NAME"],
                                "id" => $param["TARGET_ID"]
                        )) . "<br/><br/>";
                    
                    $msg .= self::msgBarrierEffect($param, 1);
                    if (! $param["PLAYER_KILLED"]) {
                        $msg .= localize("Votre Jet d'attaque a été ");
                        $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                        $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                        $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                        
                        if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                            $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                            $msg .= localize("Votre jet de maitrise de la magie a été de {mm}", array(
                                    "mm" => $param["PLAYER_MM"]
                            )) . "<br/>";
                            $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                                    "nb" => $param["SPELL_LEVEL"] * 1.7,
                                    "level" => $param["SPELL_LEVEL"]
                            ));
                            $msg .= "<br/>";
                            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGE"]
                            )) . "<br/>";
                            $msg .= self::msgPassiveBubble($param, 1);
                            $msg .= localize("Son armure le protège et il n'a perdu que <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/><br/>";
                            
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $msg .= "<br/>";
                            }
                            
                            $msg .= self::msgGainXP($param);
                            $msg .= "<br/>";
                        } else {
                            $msg .= localize("Vous n'êtes pas parvenu pas à toucher votre adversaire.") . "<br/ >";
                        }
                    }
                } 

                else {
                    if ($param["PLAYER_IS_NPC"]) {
                        if (isset($param["PLAYER_GENDER"])) {
                            if ($param["PLAYER_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else {
                            $art = "";
                        }
                        $msg = localize("Vous avez été attaqué par {target} ({id})", array(
                                "target" => $art . " " . localize($param["PLAYER_NAME"]),
                                "id" => $param["PLAYER_ID"]
                        )) . "<br/><br/>";
                    } else
                        $msg = localize("Vous avez été attaqué par {target} ({id})", array(
                                "target" => $param["PLAYER_NAME"],
                                "id" => $param["PLAYER_ID"]
                        )) . "<br/><br/>";
                    
                    $msg .= self::msgBarrierEffect($param, 0);
                    if (! $param["PLAYER_KILLED"]) {
                        
                        $msg .= localize("Le Jet d'attaque de votre adversaire a été de ");
                        $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                        $msg .= localize("Votre Jet de défense a été ");
                        $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                        
                        if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                            $msg .= localize("Vous avez donc été <b>TOUCHÉ</b>") . "<br/>";
                            $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de {mm}", array(
                                    "mm" => $param["PLAYER_MM"]
                            )) . "<br/>";
                            $msg .= localize("Il a donc réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                                    "nb" => (1 + ceil($param["SPELL_LEVEL"] * 1.5)),
                                    "level" => $param["SPELL_LEVEL"]
                            ));
                            $msg .= "<br/>";
                            $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGE"]
                            )) . "<br/>";
                            $msg .= self::msgPassiveBubble($param, 0);
                            $msg .= localize("Votre armure vous a protègé(e) et vous n'avez perdu que <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/><br/>";
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez été <b>TUÉ(E)</b>.");
                                $msg .= "<br/>";
                                $msg .= self::msgInfoDeath($param);
                            }
                        } else {
                            $msg .= localize("Votre adversaire n'est pas parvenu pas à vous toucher.") . "<br/ >";
                        }
                    }
                }
            }
        }
        return $msg;
    }

    function msgTears ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Larmes de Vie sur {target} ({id})", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre jet de maitrise de la magie a été de : {dex}", array(
                        "dex" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("{target} a été soigné et a récupéré {hp} points de vie", array(
                        "target" => $param["TARGET_NAME"],
                        "hp" => $param["HP_HEAL"]
                )) . "<br/><br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Larmes de Vie sur vous", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son jet de maitrise de la magie a été de : {dex}", array(
                        "dex" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Vous avez été soigné et vous avez récupéré {hp} points de vie", array(
                        "hp" => $param["HP_HEAL"]
                )) . "<br/><br/>";
            }
            
            return $msg;
        }
    }

    function msgArmor ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Armure d'Athlan sur {target} ({id})", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Force a été de ");
                $msg .= ".........................: " . $param["PLAYER_STRENGTH"] . " <br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= "............................: " . $param["PLAYER_MM"] . " <br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "nb" => $param["SPELL_LEVEL"],
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("L'armure de {target} a été augmentée de {hp} points.", array(
                        "target" => $param["TARGET_NAME"],
                        "hp" => $param["TARGET_ARMOR"]
                )) . "<br/><br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Armure d'Athlan sur vous.", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Le Jet de Force de votre adversaire a été de ");
                $msg .= "............................: " . $param["PLAYER_STRENGTH"] . " <br/>";
                $msg .= localize("Le Jet de Maîtrise de la Magie de votre adversaire a été de ");
                $msg .= ".............................: " . $param["PLAYER_MM"] . " <br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "nb" => $param["SPELL_LEVEL"],
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("Votre armure a été augmentée de {hp} points", array(
                        "hp" => $param["TARGET_ARMOR"]
                )) . "<br/><br/>";
            }
            
            return $msg;
        }
    }

    function msgSpellKine ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Force d'Aether sur {target} ({id}).", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Vitesse a été de ");
                $msg .= "....................... : " . $param["PLAYER_SPEED"] . " <br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de  ");
                $msg .= "....................... : " . $param["PLAYER_MM"] . " <br/>";
                $msg .= localize("Le niveau de votre sortilège a donc été de " . $param["SPELL_LEVEL"]) . "<br/><br/>";
                $msg .= localize("Le Jet de résitance (JMM/4) de votre adversaire a été de niveau : ");
                $msg .= "..................... : " . $param["TARGET_MM"] . " <br/><br/>";
                if ($param["SPELL_LEVEL"] > $param["TARGET_MM"]) {
                    if ($param["MOVE"])
                        $msg .= localize("Vous avez surpassé la résistance magique de votre adversaire et vous êtes parvenu à le déplacer en X={x} Y={y}", array(
                                "x" => $param["X"],
                                "y" => $param["Y"]
                        ));
                    else
                        $msg .= localize("Vous n'êtes pas  parvenu à déplacer votre adversaire car la case est occupée.", array(
                                "x" => $param["X"],
                                "y" => $param["Y"]
                        ));
                } else
                    $msg .= localize("Votre adversaire a contré votre sortilège et vous n'êtes pas parvenu à le déplacer");
                
                $msg .= "<br/><br/>";
                
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{target} ({id}) a utilisé Force d'Aether sur vous.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son Jet de Vitesse a été de ");
                $msg .= "......................... : " . $param["PLAYER_SPEED"] . " <br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de  ");
                $msg .= "........................... : " . $param["PLAYER_MM"] . " <br/>";
                $msg .= localize("Il a réalisé un sortilège de niveau " . $param["SPELL_LEVEL"]) . "<br/><br/>";
                
                $msg .= localize("Votre Jet de résistance (JMM/4) a été de de niveau : ");
                $msg .= "............................ : " . $param["TARGET_MM"] . " <br/><br/>";
                if ($param["SPELL_LEVEL"] > $param["TARGET_MM"]) {
                    if ($param["MOVE"])
                        $msg .= localize("Votre adversaire a surpassé votre résistance magique et il est parvenu à vous déplacer en X={x} Y={y}", array(
                                "x" => $param["X"],
                                "y" => $param["Y"]
                        ));
                    else
                        $msg .= localize("Il n'est pas  parvenu à vous déplacer car la case ciblée était occupée.", array(
                                "x" => $param["X"],
                                "y" => $param["Y"]
                        ));
                } else
                    $msg .= localize("Vous avez contré le sortilège de votre adversaire et il n'est pas parvenu à vous déplacer");
                
                $msg .= "<br/><br/>";
            }
            
            return $msg;
        }
    }

    /* ************************************************************* LES SORTILEGES DE l'Ecole de guérison ******************************************************* */
    function msgCure ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Souffle d'Athlan sur {target} ({id})", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre jet de maitrise de la magie a été de : {MM}", array(
                        "MM" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Tous les maux de {target} ont été réduit de {nb} niveau(x).", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Souffle d'Athlan sur vous.", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son jet de maitrise de la magie a été de : {dex}", array(
                        "dex" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Tous vos maux ont été réduit de {nb} niveau(x)", array(
                        "nb" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgRegen ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Régénération sur {target} ({id})", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre jet de maîtrise de la magie a été de : {MM}", array(
                        "MM" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("{target} a gagné {nb} points de régénération pour 3 tours.", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["TARGET_REGEN"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Régénération sur vous.", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son jet de maîtrise de la magie a été de : {dex}", array(
                        "dex" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Vous avez gagné {nb} points de régénération pour 3 tours.", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["TARGET_REGEN"]
                )) . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgSpring ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Vous avez utilisé Bassin Divin en (x={xb},y={yb}).", array(
                    "xb" => $param["XB"],
                    "yb" => $param["YB"]
            )) . "<br/>";
            $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
            $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/>";
            $msg .= localize("Vous avez créé un Bassin Divin de niveau {niv}", array(
                    "niv" => $param["SPELL_LEVEL"]
            )) . "<br/><br/>";
        }
        return $msg;
    }

    function msgRain ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Vous avez utilisé pluie sacrée en {x}-{y}", array(
                    "x" => $param["X"],
                    "y" => $param["Y"]
            )) . "<br/><br/>";
            $msg .= localize("Votre jet de MM a été de : {mm} ", array(
                    "mm" => $param["PLAYER_MM"]
            )) . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau {level} ", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/><br/>";
            $msg .= localize("Nombre de point potentiel de guérison : {PV}", array(
                    "PV" => $param["HP_POTENTIEL"]
            )) . "<br/><br/>";
            
            for ($i = 1; $i < $param["NB"] + 1; $i ++) {
                if ($param["SELF"][$i]) {
                    $msg .= localize("Vous êtes atteint par la pluie", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                    $msg .= localize("vous regagnez {hp} points de vie", array(
                            "target" => $param[$i]["TARGET_NAME"],
                            "hp" => $param["HP_HEAL"][$i]
                    )) . "<br/><br/>";
                } else {
                    $msg .= localize("{target} ({id}) est atteint par la pluie", array(
                            "target" => $param[$i]["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                    $msg .= localize("{target} est soigné de {hp} points de vie", array(
                            "target" => $param[$i]["TARGET_NAME"],
                            "hp" => $param["HP_HEAL"][$i]
                    )) . "<br/><br/>";
                }
            }
        }
        return $msg;
    }

    function msgReload ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé Recharge du bassin.") . "<br/><br/>";
            if (isset($param["SPELL_LEVEL"]))
                $msg .= "Vous réalisez un sort de niveau " . $param["SPELL_LEVEL"] . ".<br/>";
            if ($param["TYPE_ATTACK"] == SPELL_RELOAD_EVENT_FAIL) {
                $msg .= "Le Bassin Divin est de niveau " . $param["BUILDING_LEVEL"] . ".<br/>";
                $msg .= "Il n'a pas pu être rechargé.<br/><br/>";
            } else {
                // $msg.=localize("Vous utilisez recharge du bassin en {x}-{y}",array("x"=>$param["X"], "y" => $param["Y"]))."<br/><br/>";
                $msg .= localize("Le bassin ciblé a été rechargé avec succès") . "<br/>";
            }
            return $msg;
        }
    }

    function msgSun ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé Soleil de guérison") . "<br/><br/>";
            $msg .= localize("Votre Jet de maitrise de la magie a été de");
            $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
            $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/>";
            $msg .= localize("Chacun de vos alliés sous l'effet du soleil sera automatiquement guérit de {level}% de la perte subite en cas d'attaque et ce pendant 3 dla.", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/>";
            
            return $msg;
        }
    }

    /* ************************************************************* LES SORTILEGES DE l'Ecole de protection ******************************************************* */
    function msgShield ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                if ($param["SELF"])
                    $msg = localize("Vous avez utilisé Bouclier magique sur vous-même.") . "<br/>";
                else
                    $msg = localize("Vous avez utilisé Bouclier magique sur {target} ({id}).", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                $msg .= localize("Votre jet de maitrise de la magie a été de : {MM}", array(
                        "MM" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Votre jet de force a été de : {MM}", array(
                        "MM" => $param["PLAYER_STRENGTH"]
                )) . "<br/>";
                if ($param["SELF"])
                    $msg .= localize("Votre défense a été augmenté de {armor} %. ", array(
                            "target" => $param["TARGET_NAME"],
                            "armor" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                else
                    $msg .= localize("La défense de {target} a été augmenté de {armor} %.", array(
                            "target" => $param["TARGET_NAME"],
                            "armor" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Bouclier magique sur vous.", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son jet de maitrise de la magie a été de : {dex}", array(
                        "dex" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Son jet de force a été de : {dex}", array(
                        "dex" => $param["PLAYER_STRENGTH"]
                )) . "<br/>";
                $msg .= localize("Votre défense a été augmenté de {hp} %", array(
                        "hp" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
            }
            
            return $msg;
        }
    }

    function msgNegation ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Souffle de négation sur {target}.", array(
                        "target" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("{target} était alors invisible et ne pouvait plus être ciblé. ", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["SPELL_LEVEL"] / 2
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Souffle de négation sur vous.", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Vous étiez alor invisible et vous ne pouviez plus être ciblé.", array(
                        "dex" => $param["PLAYER_MM"]
                )) . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgBless ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                if ($param["SELF"])
                    $msg = localize("Vous avez utilisé Bénédiction sur vous-même.") . "<br/>";
                else
                    $msg = localize("Vous avez utilisé Bénédiction sur {target} ({id}).", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Votre Jet de Force a été de ");
                $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("Vous avez créé une protection de niveau {level} contre les malédictions pour les 3 prochaines dla. ", array(
                        "target" => $param["TARGET_NAME"],
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{target} ({id}) a utilisé Bénédiction sur vous.", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Son Jet de Force a été de ");
                $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("Il a créé une protection de niveau {level} contre les malédictions pour les 3 prochaines dla. ", array(
                        "target" => $param["TARGET_NAME"],
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgBarrier ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                if ($param["SELF"])
                    $msg = localize("Vous avez utilisé Barrière enflammée sur vous-même.") . "<br/>";
                else
                    $msg = localize("Vous avez utilisé Barrière enflammée sur {target} ({id}).", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Votre Jet de Force a été de ");
                $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                if ($param["SELF"])
                    $msg .= localize("Vous avez créé une barrière enflammée qui infligera {nb}D6 de dégât à tout adversaire vous attaquant au corps à corps pour les deux prochaines dla). ", array(
                            "target" => $param["TARGET_NAME"],
                            "nb" => $param["SPELL_LEVEL"] / 2
                    )) . "<br/>";
                else
                    $msg .= localize("Vous avez créé une barrière enflammée qui infligera {nb}D6 de dégât à tout adversaire attaquant {target} au corps à corps pour les deux prochaines dla. ", array(
                            "target" => $param["TARGET_NAME"],
                            "nb" => $param["SPELL_LEVEL"] / 2
                    )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{target} ({id}) a utilisé Barrière enflammée sur vous.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Son Jet de Force a été de ");
                $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("{target} ({id}) a créé une barrière enflammée qui infligeait {nb}D6 de dégât à tout adversaire vous attaquant au corps à corps pour les deux prochaines dla. ", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "nb" => $param["SPELL_LEVEL"] / 2
                )) . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgBubble ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                if ($param["SELF"])
                    $msg = localize("Vous avez utilisé Bulle de vie sur vous-même.") . "<br/>";
                else
                    $msg = localize("Vous avez utilisé Bulle de vie sur {target} ({id}).", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Votre Jet de Force a été de ");
                $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/>";
                $msg .= localize("Vous avez créé une bulle de vie capabe d'encaisser {nb} points de dégât. ", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["SPELL_LEVEL"] * 3
                )) . "<br/>";
                $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                        "xp" => $param["XP"]
                )) . "<br/>";
            } else {
                $msg = localize("{target} a utilisé Bulle de vie sur vous.", array(
                        "target" => $param["PLAYER_NAME"]
                )) . "<br/><br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Son Jet de Force a été de ");
                $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"]
                ));
                $msg .= "<br/><br/>";
                $msg .= localize("{target} ({id}) a créé une bulle de vie capable d'encaisser {nb} points de dégâts.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "nb" => $param["SPELL_LEVEL"] * 3
                )) . "<br/>";
            }
            
            return $msg;
        }
    }

    function msgWall ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Vous avez utilisé Réveil de la terre.") . "<br/>";
            $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
            $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
            $msg .= localize("Votre Jet de Force a été de ");
            $msg .= "........................: " . $param["PLAYER_STRENGTH"] . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/>";
            $msg .= localize("Vous avez créé {nb} mur(s) de Terre de niveau {niv}", array(
                    "nb" => $param["NB_WALL"],
                    "niv" => $param["SPELL_LEVEL"]
            )) . "<br/><br/>";
        }
        return $msg;
    }

    /* ************************************************************* LES SORTILEGES DE l'Ecole de destruction ******************************************************* */
    function msgFire ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé Brasier en {x}-{y}", array(
                    "x" => $param["X"],
                    "y" => $param["Y"]
            )) . "<br/><br/>";
            $msg .= localize("Votre Jet d'Attaque a été de ");
            $msg .= "........................: " . $param["PLAYER_ATTACK"] . "<br/>";
            $msg .= localize("Votre jet de MM a été de : {mm} ", array(
                    "mm" => $param["PLAYER_MM"]
            )) . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau {level} ", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/><br/>";
            
            for ($i = 1; $i < $param["NB"] + 1; $i ++) {
                if ($param["SELF"][$i]) {
                    $msg .= localize("Vous êtes atteint par le brasier", array(
                            "target" => $param[$i]["TARGET_NAME"]
                    )) . "<br/>";
                    $msg .= localize("Votre jet de défense est de ");
                    $msg .= "........................: " . $param["TARGET_DEFENSE"][$i] . "<br/>";
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$i]) {
                        if (isset($param["BUBBLE_LIFE2"][$i])) {
                            if (! $param["BUBBLE_CANCEL2"][$i])
                                $msg .= localize("Vous êtiez protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE2"][$i],
                                        "level" => $param["BUBBLE_LEVEL2"][$i]
                                )) . "<br/>";
                            else
                                $msg .= localize("Vous êtiez protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["TARGET_DAMAGE"][$i]
                                )) . "<br/>";
                        }
                        $msg .= localize("Vous perdez {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_DAMAGE2"][$i]
                        )) . "<br/>";
                        if ($param["KILLED"][$i] == 1) {
                            $msg .= localize("Vous avez été <b>TUÉ</b>.") . "<br/>";
                            $msg .= self::msgInfoDeath($param);
                        }
                        
                        $msg .= "<br/><br/>";
                    } else
                        $msg .= localize("Le braiser ne parvient pas à toucher votre adversaire.") . "<br/><br/>";
                } else {
                    $msg .= localize("{target} ({id}) a été atteint par le brasier", array(
                            "target" => $param[$i]["TARGET_NAME"],
                            "id" => $param[$i]["TARGET_ID"]
                    )) . "<br/>";
                    $msg .= localize("Son Jet de défense a été  de ");
                    $msg .= "........................: " . $param["TARGET_DEFENSE"][$i] . "<br/>";
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$i]) {
                        if (isset($param["BUBBLE_LIFE2"][$i])) {
                            if (! $param["BUBBLE_CANCEL2"][$i])
                                $msg .= localize("Votre adversaire etait protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE2"][$i],
                                        "level" => $param["BUBBLE_LEVEL2"][$i]
                                )) . "<br/>";
                            else
                                $msg .= localize("Votre adversaire etait protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["TARGET_DAMAGE"][$i]
                                )) . "<br/>";
                        }
                        $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_DAMAGE2"][$i]
                        )) . "<br/>";
                        if ($param["KILLED"][$i] == 1) {
                            $msg .= localize("Vous avez  <b>TUÉ</b> votre adversaire.");
                            
                            // $obj = new FallMsg();
                            // $msg.=$obj->msgBuilder($param);
                        }
                        $msg .= "<br/><br/>";
                    } else {
                        $msg .= localize("Le braiser ne parvient pas à toucher votre adversaire.") . "<br/><br/>";
                    }
                }
            }
            
            $msg .= self::msgGainXP($param);
            $msg .= "<br/>";
        }
        return $msg;
    }

    function msgDemonPunch ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            
            $msg .= self::msgBarrierEffect($param, $src);
            
            if (! $param["PLAYER_KILLED"]) {
                if ($src) {
                    $msg = localize("Vous avez attaqué {target} ({id})", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                    $msg .= localize("Votre Jet d'attaque a été ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                        $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                        $msg .= localize("Votre jet de maitrise de la magie a été de {mm}", array(
                                "mm" => $param["PLAYER_MM"]
                        )) . "<br/>";
                        $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b> ({nb} point de durabilité)", array(
                                "nb" => ($param["SPELL_LEVEL"] * 10),
                                "level" => $param["SPELL_LEVEL"]
                        ));
                        $msg .= "<br/>";
                        if ($param["TARGET_IS_NPC"]) {
                            $msg .= localize("Vous réalisez un sort de niveau {level}.", array(
                                    "level" => $param["SPELL_LEVEL"]
                            )) . "<br/>";
                            $msg .= localize("La résistance du monstre a été diminuée. Son armure baisse de {pt} points. ", array(
                                    "pt" => $param["SPELL_LEVEL"] * 1.2
                            )) . "<br/>";
                        } else {
                            if ($param["NB_OBJECT"] == 1 && $param["NAME_OBJECT"][0] == 0)
                                $msg .= localize("{target} ne portait aucune pièce d'équipement qui ne soit pas déjà cassée.", array(
                                        "target" => $param["TARGET_NAME"]
                                )) . "<br/>";
                            else {
                                $dep = 0;
                                if ($param["NAME_OBJECT"][0] == "none")
                                    $dep = 1;
                                for ($i = $dep; $i < $param["NB_OBJECT"]; $i ++)
                                    $msg .= localize("{objet} a été touché et sa durabilité est tombé à {nb}", array(
                                            "objet" => $param["NAME_OBJECT"][$i],
                                            "nb" => $param["DUR_OBJECT"][$i]
                                    )) . "<br/>";
                            }
                        }
                        $msg .= "<br/>";
                    } 

                    else {
                        $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/><br/>";
                    }
                    
                    $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                            "xp" => $param["XP"]
                    ));
                    $msg .= "<br/>";
                } else {
                    $msg = localize("Vous avez été attaqué par {target} ({id})", array(
                            "target" => $param["PLAYER_NAME"],
                            "id" => $param["PLAYER_ID"]
                    )) . "<br/><br/>";
                    $msg .= localize("Le Jet d'attaque de votre adversaire a été de ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Votre Jet de défense a été ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                        $msg .= localize("Vous avez donc été <b>TOUCHÉ</b>") . "<br/>";
                        $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de {mm}", array(
                                "mm" => $param["PLAYER_MM"]
                        )) . "<br/>";
                        if ($param["TARGET_IS_NPC"]) {
                            $msg .= localize("Il a réalisé un sort de niveau {level}.", array(
                                    "level" => $param["SPELL_LEVEL"]
                            )) . "<br/>";
                            $msg .= localize("Votre résistance a été diminuée. Votre armure a baissé de {pt} points. ", array(
                                    "pt" => $param["SPELL_LEVEL"] * 1.2
                            )) . "<br/>";
                        } else {
                            $msg .= localize("Il a donc réalisé un sort de niveau : <b>{level}</b> ({nb} point de durabilité)", array(
                                    "nb" => ($param["SPELL_LEVEL"] * 10),
                                    "level" => $param["SPELL_LEVEL"]
                            ));
                            $msg .= "<br/>";
                            if ($param["NB_OBJECT"] == 1 && $param["NAME_OBJECT"][0] == 0)
                                $msg .= localize("Vous ne portiez aucune pièce d'équipement qui ne soit pas déjà cassée.", array(
                                        "target" => $param["TARGET_NAME"]
                                )) . "<br/>";
                            else {
                                $dep = 0;
                                if ($param["NAME_OBJECT"][0] == "none")
                                    $dep = 1;
                                for ($i = $dep; $i < $param["NB_OBJECT"]; $i ++)
                                    $msg .= localize("{objet} a été touché et sa durabilité est tombé à {nb}", array(
                                            "objet" => $param["NAME_OBJECT"][$i],
                                            "nb" => $param["DUR_OBJECT"][$i]
                                    )) . "<br/>";
                            }
                        }
                    } else {
                        $msg .= localize("Votre adversaire n'est pas parvenu à vous toucher.") . "<br/><br/>";
                    }
                }
            }
            return $msg;
        }
    }

    function msgBlood ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            if ($src) {
                if ($param["TARGET_IS_NPC"]) {
                    if (isset($param["TARGET_GENDER"])) {
                        if ($param["TARGET_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else
                        $art = "";
                    
                    $msg = localize("Vous avez utilisé Sang de Lave sur {target} ({id})", array(
                            "target" => $art . " " . localize($param["TARGET_NAME"]),
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                } else
                    $msg = localize("Vous avez utilisé Sang de Lave sur {target} ({id})", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                $msg .= localize("Votre Jet de maitrise de la magie a été de");
                $msg .= "........................: " . $param["PLAYER_ATTACK"] . "<br/>";
                $msg .= localize("Le Jet de maitrise de la magie de votre adversaire a été de ");
                $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Vous avez réalisé un sort de niveau {level}", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                    )) . "<br/>";
                    if (isset($param["LEVEL_BENE"])) {
                        $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                "level" => $param["LEVEL_BENE"]
                        )) . "<br/>";
                        if ($param["CANCEL"])
                            $msg .= localize("Sa protection etait plus puissante que votre sortilège et en a annulé les effets") . "<br/>";
                        else
                            $msg .= localize("Les effets de votre sortilège ont été réduit de {level} à {niveau} points de dégât pour les 5 prochaines dla.", array(
                                    "level" => $param["TARGET_DAMAGE"],
                                    "niveau" => ($param["TARGET_DAMAGE"] - $param["LEVEL_BENE"])
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vous avez infligé à votre adversaire une malédiction de {damage} points de dégâts pour les 5 prochaines dla.", array(
                                "damage" => $param["TARGET_DAMAGE"]
                        )) . "<br/>";
                } 

                else {
                    $msg .= localize("Vous adversaire a contré votre sort.") . "<br/><br/>";
                }
                $msg .= "<br/>";
                $msg .= "<br/>";
                $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                        "xp" => $param["XP"]
                ));
            } else {
                if ($param["PLAYER_IS_NPC"]) {
                    if (isset($param["PLAYER_GENDER"])) {
                        if ($param["PLAYER_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else {
                        $art = "";
                    }
                    $msg = localize("{target} ({id}) a utilisé Sang de Lave sur vous ", array(
                            "target" => $art . " " . localize($param["PLAYER_NAME"]),
                            "id" => $param["PLAYER_ID"]
                    )) . "<br/><br/>";
                } else
                    $msg = localize("{target} ({id}) a utilisé Sang de Lave sur vous ", array(
                            "target" => $param["PLAYER_NAME"],
                            "id" => $param["PLAYER_ID"]
                    )) . "<br/><br/>";
                
                $msg .= localize("Le Jet de maitrise de la magie de votre adversaire a été de");
                $msg .= "........................: " . $param["PLAYER_ATTACK"] . "<br/>";
                $msg .= localize("Votre Jet de maitrise de la magie a été de ");
                $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Votre adversaire a réalisé un sort de niveau {level}", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                    )) . "<br/>";
                    if (isset($param["LEVEL_BENE"])) {
                        $msg .= localize("Vous étiez protégé par une bénédiction de niveau {level}.", array(
                                "level" => $param["LEVEL_BENE"]
                        )) . "<br/>";
                        if ($param["CANCEL"])
                            $msg .= localize("Votre protection etait plus puissante que le sortilège de votre adversaire et en a annulé les effets") . "<br/>";
                        else
                            $msg .= localize("Les effets du sortilège ont réduit de {level} à {niveau} points de dégât pour les 5 prochaines dla.", array(
                                    "level" => $param["TARGET_DAMAGE"],
                                    "niveau" => ($param["TARGET_DAMAGE"] - $param["LEVEL_BENE"])
                            )) . "<br/>";
                    } else
                        $msg .= localize("Votre adversaire vous a infligé une malédiction de {damage} points de dégâts pour les 5 prochaines dla.", array(
                                "damage" => $param["TARGET_DAMAGE"]
                        )) . "<br/>";
                    $msg .= "<br/>";
                } 

                else {
                    $msg .= localize("Vous avez contré le sort de votre adversaire.") . "<br/><br/>";
                }
            }
            
            return $msg;
        }
    }

    function msgFireball ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            if ($param["PROTECTION"] == 1)
                $msg .= self::msgInterFireball($param, $src);
            else {
                if ($src) {
                    if ($param["TARGET_IS_NPC"]) {
                        if (isset($param["TARGET_GENDER"])) {
                            if ($param["TARGET_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else
                            $art = "";
                        
                        $msg = localize("Vous avez lancé une boule de feu sur {target} ({id}) ", array(
                                "target" => $art . " " . localize($param["TARGET_NAME"]),
                                "id" => $param["TARGET_ID"]
                        )) . "<br/><br/>";
                    } else
                        $msg = localize("Vous avez lancé une boule de feu sur {target} ({id})", array(
                                "target" => $param["TARGET_NAME"],
                                "id" => $param["TARGET_ID"]
                        )) . "<br/><br/>";
                    
                    $msg .= localize("Votre Jet d'attaque a été de ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                        if ($param["GOLEM"])
                            $msg .= localize("Le Golem de Feu a absorbé l'énergie de votre boule de feu. Il est devenu plus fort") . "<br/>";
                        
                        else {
                            $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                            $msg .= localize("Votre jet de maîtrise de la magie a été de {mm}", array(
                                    "mm" => $param["PLAYER_MM"]
                            )) . "<br/>";
                            $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                                    "nb" => (1.2 * $param["SPELL_LEVEL"]),
                                    "level" => $param["SPELL_LEVEL"]
                            )) . "<br/>";
                            $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                    "damage" => $param["TARGET_DAMAGE"]
                            )) . "<br/>";
                            $msg .= self::msgPassiveBubble($param, 1);
                            $msg .= localize("Son armure l'a protégé et il n'a perdu que <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                            
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                                $msg .= "<br/>";
                            }
                        }
                    } else {
                        $msg .= localize("Vous n'êtes pas parvenu à toucher votre adversaire.") . "$<br/ >";
                    }
                    
                    $msg .= self::msgGainXP($param);
                    $msg .= "<br/>";
                } 

                else {
                    if ($param["PLAYER_IS_NPC"]) {
                        if (isset($param["PLAYER_GENDER"])) {
                            if ($param["PLAYER_GENDER"] == "M")
                                $art = localize("un");
                            else
                                $art = localize("une");
                        } else {
                            $art = "";
                        }
                        $msg = localize("{target} ({id}) vous a lancé une boule de feu ", array(
                                "target" => $art . " " . localize($param["PLAYER_NAME"]),
                                "id" => $param["PLAYER_ID"]
                        )) . "<br/><br/>";
                    } else
                        $msg = localize("{target} ({id})vous a lancé une boule de feu ", array(
                                "target" => $param["PLAYER_NAME"],
                                "id" => $param["PLAYER_ID"]
                        )) . "<br/><br/>";
                    
                    $msg .= localize("Le Jet d'attaque de votre adversaire a été de ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Votre Jet de défense a été ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                        if ($param["GOLEM"])
                            $msg .= localize("Le Golem de Feu a absorbé l'énergie de votre boule de feu. Il est devenu plus fort") . "<br/>";
                        else {
                            $msg .= localize("Vous avez donc été <b>TOUCHÉ</b>") . "<br/>";
                            $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de {mm}", array(
                                    "mm" => $param["PLAYER_MM"]
                            )) . "<br/>";
                            $msg .= localize("Il a donc réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                                    "nb" => ($param["SPELL_LEVEL"] * 1.2),
                                    "level" => $param["SPELL_LEVEL"]
                            )) . "<br/>";
                            $msg .= localize("Il vous a infligé {damage} points de dégât.", array(
                                    "damage" => $param["TARGET_DAMAGE"]
                            )) . "<br/>";
                            
                            $msg .= self::msgPassiveBubble($param, 0);
                            $msg .= localize("Votre armure vous a protégé et vous n'avez perdu que <b>{hp} point(s) de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                            
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= "<br/>";
                                $msg .= localize("Vous avez été <b>TUÉ(E)</b>.");
                                $msg .= "<br/>";
                                $msg .= self::msgInfoDeath($param);
                            }
                        }
                    } else {
                        $msg .= localize("Votre adversaire n'est pas parvenu à vous toucher.") . "<br/ >";
                    }
                }
            }
        }
        return $msg;
    }

    function msgPillars ($error, &$param, $src)
    {
        if (! $error) {
            $msg = localize("Vous avez invoqué des Piliers Infernaux en {x1}-{y1} et {x2}-{y2} et {x3}-{y3}", array(
                    "x1" => $param["X"][1],
                    "y1" => $param["Y"][1],
                    "x2" => $param["X"][2],
                    "y2" => $param["Y"][2],
                    "x3" => $param["X"][3],
                    "y3" => $param["Y"][3]
            )) . "<br/><br/>";
            $msg .= localize("Votre Jet de Maitrise de la magie a été de : ");
            $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/>";
            $msg .= localize("Votre Jet d'attaque magique a été de : ");
            $msg .= localize("........................: " . $param["PLAYER_ATTACK"]) . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau {level}", array(
                    "level" => $param["SPELL_LEVEL"]
            )) . "<br/><br/>";
            for ($i = 1; $i < 4; $i ++) {
                if (isset($param["BUILDING_LEVEL"][$i])) {
                    $msg .= localize("Vous avez attaqué {name} niveau {level}", array(
                            "name" => $param["BUILDING_NAME"][$i],
                            "level" => $param["BUILDING_LEVEL"][$i]
                    )) . "<br/>";
                    $msg .= localize("Vous avez infligé au bâtiment une perte de {nb} points de structure", array(
                            "nb" => $param["BUILDING_DAMAGE"]
                    )) . "<br/>";
                    if ($param["BUILDING_DEATH"][$i])
                        $msg .= localize("Vous avez <b> DETRUIT</b> le bâtiment") . "<br/>";
                }
                $msg .= "<br/>";
            }
        }
        return $msg;
    }

    function msgSpellFireCall ($error, &$param, $src)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé Appel du Feu") . "<br/><br/>";
            $msg .= localize("Votre Jet de Maitrise de la magie a été de : ");
            $msg .= localize("...........................: " . $param["PLAYER_MM"]) . "<br/>";
            $msg .= localize("Votre Jet de Dextérité a été de : ");
            $msg .= localize("..........................." . $param["PLAYER_ATTACK"]) . "<br/><br/>";
            if (isset($param["ID_FEU_FOL_TARGET"])) {
                $msg .= localize("Le Jet de Maitrise de la magie du Feu Fol (" . $param["ID_FEU_FOL_TARGET"] . ") est de : ");
                $msg .= localize("...........................: " . $param["FEU_FOL_MM"]) . "<br/>";
                $msg .= localize("Le Jet d'attaque magique du Feu Fol (" . $param["ID_FEU_FOL_TARGET"] . ") est de : ");
                $msg .= localize("...........................: " . $param["FEU_FOL_ATTACK"]) . "<br/><br/>";
                
                if ($param["FEU_FOL_CAPTURE"] == 1) {
                    $msg .= localize("Vous avez réussi à apprivoiser le Feu Fol.") . "<br/><br/>";
                } else {
                    $msg .= localize("Vous avez échoué à apprivoiser le Feu Fol.") . "<br/><br/>";
                }
            } else {
                $msg .= localize("Vous réalisez un sort de niveau {level}", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                $msg .= localize("Vous avez réussi l'invocation d'un golem de feu de niveau {niv}", array(
                        "niv" => ceil($param["SPELL_LEVEL"] / 1.5)
                )) . "<br/>";
                $msg .= "<br/>";
            }
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
        }
        
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }

    /* ************************************************************* LES SORTILEGES DE l'Ecole d'altération ******************************************************* */
    function msgAnger ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                if ($param["SELF"])
                    $msg .= localize("Vous avez utilisé Ailes de colère sur vous-même.") . "<br/>";
                else
                    $msg .= localize("Vous avez utilisé Ailes de colère sur {target} ({id}).", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Votre Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "<br/>";
                $msg .= localize("Vous avez créé un enchantement qui a augmenté la caractéristique : <b> {carac} </b> de {nb}D6 pour les deux prochaines dla. ", array(
                        "carac" => $param["CHARAC_NAME"],
                        "nb" => $param["SPELL_LEVEL"] / 2
                )) . "<br/>";
                $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                        "xp" => $param["XP"]
                ));
                $msg .= "<br/>";
            } else {
                $msg .= localize("{target} ({id}) a utilisé Ailes de colère sur vous.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Son Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "<br/>";
                $msg .= localize("Il a créé un enchantement qui a augmenté la caractéristique : <b> {carac} </b> de {nb}D6 pour les deux prochaines dla. ", array(
                        "carac" => $param["CHARAC_NAME"],
                        "nb" => $param["SPELL_LEVEL"] / 2
                )) . "<br/>";
                $msg .= "<br/>";
            }
        }
        return $msg;
    }

    function msgCurse ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg .= localize("Vous avez utilisé Malédiction d'Arcxos sur {target} ({id}).", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                
                $msg .= localize("Votre Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Le Jet de Maîtrise de la Magie de votre adversaire a été de ");
                $msg .= "........................: " . $param["TARGET_MM"] . "<br/>";
                if ($param["SUCCESS"]) {
                    $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] / 3)
                    ));
                    $msg .= "<br/>";
                    if ($param["CHARAC_NAME"] == "ENCHANTEMENTS") {
                        $msg .= localize("Tous les enchantements de {target} ont été réduit de {nb} niveau(x).", array(
                                "target" => $param["TARGET_NAME"],
                                "nb" => $param["SPELL_LEVEL_FINAL"]
                        )) . "<br/>";
                        $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
                    } else {
                        $msg .= localize("Vous avez créé une malédiction qui a réduit la caractéristique : <b> {carac} </b> de {nb}D6 pour les deux prochaines dla. ", array(
                                "carac" => $param["CHARAC_NAME"],
                                "nb" => $param["SPELL_LEVEL"] / 2.5
                        )) . "<br/>";
                    }
                } else
                    $msg .= localize("Votre adversaire vous a contré et vous n'êtes pas parvenu à lui infliger une malédiction") . "<br/>";
                
                $msg .= "<br/>";
                $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                        "xp" => $param["XP"]
                ));
                $msg .= "<br/>";
            } else {
                $msg .= localize("{target} ({id}) a utilisé Malédiction d'Arcxos sur vous.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                
                $msg .= localize("Son Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["TARGET_MM"] . "<br/>";
                if ($param["SUCCESS"]) {
                    $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] / 3)
                    ));
                    $msg .= "<br/>";
                    if ($param["CHARAC_NAME"] == "ENCHANTEMENTS") {
                        $msg .= localize("Il a créé une malédiction qui a réduit vos enchantements de {nb} niveau(x).", array(
                                "target" => $param["TARGET_NAME"],
                                "nb" => $param["SPELL_LEVEL_FINAL"]
                        )) . "<br/>";
                        $msg .= localize("Ceux dont le niveau a été réduit à 0 ont été dissipés") . "<br/>";
                    } else {
                        $msg .= localize("Il a créé une malédiction qui a réduit la caractéristique : <b> {carac} </b> de {nb}D6 pour les deux prochaines dla. ", array(
                                "carac" => $param["CHARAC_NAME"],
                                "nb" => $param["SPELL_LEVEL"] / 2.5
                        )) . "<br/>";
                    }
                } else
                    $msg .= localize("Vous avez contré votre adversaire qui n'est pas parvenu à vous infliger une malédiction");
            }
        }
        return $msg;
    }

    function msgMassiveTeleport ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg .= localize("Vous tentez une téléportation en  x={x} y={y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/><br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie est de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . " <br/>";
                $msg .= localize("Votre Jet de Vitesse est de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . " <br/>";
                $msg .= localize("Le temple ou vous vous trouvez est de niveau ");
                $msg .= "........................: " . $param["TEMPLE_LEVEL"] . " <br/>";
                $msg .= localize("Vous réalisez un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "<br/><br/>";
                if ($param["SUCCESS"]) {
                    $msg .= localize("Vous avez téléporté avec succès aux environs de la destination souhaitée les personnages de votre groupe de chasse suivants. Les autres sont restés où ils étaient.") . "<br/>";
                    if ($param["NB"] > 0) {
                        for ($i = 1; $i < $param["NB"] + 1; $i ++) {
                            $msg .= "  - " . $param["RESULT_PJ"][$i]["name"] . " (" . $param["RESULT_PJ"][$i]["id"] . ") a été téléporté en (" . $param["RESULT_PJ"][$i]["x"] . "/" . $param["RESULT_PJ"][$i]["y"] . ").<br/>";
                        }
                    } else {
                        $msg .= "  - Personne n'a été téléporté.<br/>";
                    }
                } else
                    $msg .= localize("Votre jet de MM n'est pas suffisant pour atteindre la destination souhaitée. Vous n'avez téléporté personne.") . "<br/>";
            } else {
                $msg .= localize("{target} ({id}) a utilisé Téléportation Massive sur vous.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Vous êtes téléporté en ({x}/{y}).", array(
                        "x" => $param["X_DEST"],
                        "y" => $param["Y_DEST"]
                )) . "<br/>";
            }
        }
        return $msg;
    }

    function msgTeleport ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez tenté une téléportation en  x={x} y={y}", array(
                    "x" => $param["X"],
                    "y" => $param["Y"]
            )) . "<br/>";
            $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
            $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
            $msg .= localize("Votre Jet de Vitesse a été de ");
            $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
            $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                    "level" => $param["SPELL_LEVEL"],
                    "nb" => ceil($param["SPELL_LEVEL"] / 3)
            ));
            $msg .= "<br/>";
            if ($param["CASE_BUSY"]) {
                $msg .= localize("Vous vous êtes téléporté jusqu'à la place séléctionnée mais celle-ci était occupée") . "<br/>";
                $msg .= localize("Vous êtes donc resté coincé dans une dimension d'espace-temps parallèle jusqu'à votre MORT.") . "<br/>";
            } else {
                if ($param["SUCCESS"])
                    $msg .= localize("Vous vous êtes téléporté avec succès jusqu'à la destination voulu.") . "<br/>";
                else
                    $msg .= localize("Votre jet de MM n'a pas été suffisant pour atteindre la destination souhaitée et vous n'avez pas bougé.") . "<br/>";
            }
        }
        return $msg;
    }

    function msgLightTouch ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg .= localize("Vous avez utilisé Touché de lumière sur {target} ({id}).", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                
                $msg .= localize("Votre Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/><br/>";
                
                $msg .= localize("{target} regagne {nb} points d'action.", array(
                        "target" => $param["TARGET_NAME"],
                        "nb" => $param["AP_GAIN"]
                )) . "<br/>";
                
                $msg .= "<br/>";
                $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                        "xp" => $param["XP"]
                ));
                $msg .= "<br/>";
            } else {
                $msg .= localize("{target} ({id}) a utilisé Touché de lumière sur vous.", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                
                $msg .= localize("Son Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                
                $msg .= localize("Vous avez regagné {nb} points d'action.", array(
                        "nb" => $param["AP_GAIN"]
                )) . "<br/>";
            }
        }
        return $msg;
    }

    function msgSpellSoul ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg .= localize("Vous avez utilisé Projection de l'âme en  x={x} y={y}", array(
                        "x" => $param["X"],
                        "y" => $param["Y"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= ".......................: " . $param["PLAYER_MM"] . " <br/>";
                $msg .= localize("Votre Jet de Vitesse a été de ");
                $msg .= ".....................: " . $param["PLAYER_SPEED"] . " <br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "<br/>";
                if ($param["SUCCESS"])
                    $msg .= localize("Vous avez réussi. Votre âme a été projeté à la destination souhaitée à plus ou mois 3 cases.") . "<br/>";
                else
                    $msg .= localize("Votre jet de MM n'a pas été suffisant ou les cases autour n'étaient pas libres pour projeter votre âme à la destination souhaitée.") . "<br/>";
            }
        }
        return $msg;
    }

    function msgSpellRecall ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg .= localize("Vous avez utilisé Rappel sur {target} ({id})", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Votre Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Votre Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "<br/>";
                if ($param["SUCCESS"])
                    $msg .= localize("Vous avez réussi votre sort de rappel. Votre cible a eu la possibilité de se téléporter à côté de vous à sa prochaine dla.", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                else
                    $msg .= localize("Votre niveau de sort n'a pas été suffisant pour rappeler une cible aussi éloignée.", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
            } else {
                $msg .= localize("{name} ({id}) a utilisé Rappel sur vous", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Son Jet de Maîtrise de la Magie a été de ");
                $msg .= "........................: " . $param["PLAYER_MM"] . "<br/>";
                $msg .= localize("Son Jet de Vitesse a été de ");
                $msg .= "........................: " . $param["PLAYER_SPEED"] . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau : <b>{level}</b>", array(
                        "level" => $param["SPELL_LEVEL"],
                        "nb" => ceil($param["SPELL_LEVEL"] / 3)
                ));
                $msg .= "<br/>";
                if ($param["SUCCESS"])
                    $msg .= localize("Il a réussi votre sort de rappel et avez eu la possibilité de vous téléporter à côté de lui à votre prochaine dla.", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
                else
                    $msg .= localize("Son niveau de sort n'a pas été suffisant pour rappeler une cible aussi éloignée.", array(
                            "target" => $param["TARGET_NAME"]
                    )) . "<br/>";
            }
        }
        return $msg;
    }

    /* ************************************************************* LES COMPETENCES DE L'ECOLE DE LA GARDE D'OCTOBIAN ******************************************************* */
    function msgSharpen ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Vous avez aiguisé {name}", array(
                    "name" => $param["OBJECT_NAME"]
            )) . ".<br/>";
            $msg .= localize("Le bonus de dégât de l'arme a été augmenté de " . ceil($param["PLAYER_SPEED"] / 5) . " points.") . "<br/>";
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgTwirl ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            
            $msg = localize("Vous avez réalisé un tournoiement ") . "<br/>";
            $msg .= localize("Votre jet d'attaque a été de: " . $param["PLAYER_ATTACK"]) . " <br/>";
            $msg .= localize("Votre jet de damage a été de: " . $param["TARGET_DAMAGE_T"]) . " <br/><br/>";
            for ($i = 1; $i < $param["NUMBER_OPPONENT"] + 1; $i ++) {
                $art = "il";
                if (isset($param["TARGET_GENDER"][$i])) {
                    if ($param["TARGET_GENDER"][$i] == "M")
                        $art = localize("il");
                    else
                        $art = localize("elle");
                }
                
                if (isset($param["BARRIER_LEVEL"][$i])) {
                    $param2 = array();
                    $param2["BARRIER_LEVEL"] = $param["BARRIER_LEVEL"][$i];
                    if (isset($param2["PIEGE"]))
                        $param2["PIEGE"] = $param["PIEGE"][$i];
                    $param2["PLAYER_DAMAGE"] = $param["PLAYER_DAMAGE"][$i];
                    $param2["PLAYER_KILLED"] = $param["PLAYER_KILLED"][$i];
                    $msg .= self::msgBarrierEffect($param2, $src);
                    unset($param2);
                }
                
                if (! $param["PLAYER_KILLED"][$i]) {
                    
                    if ($param["PROTECTION"][$i]) {
                        $msg .= localize("{name} s'était interposé pour défendre {target}", array(
                                "name" => $param["TARGET_NAME"][$i],
                                "target" => $param["INTER_NAME"][$i]
                        )) . "<br/>";
                        if (isset($param["BUBBLE_LIFE2"][$i])) {
                            if (! $param["BUBBLE_CANCEL2"][$i]) {
                                $msg .= localize("Votre adversaire était protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE2"][$i],
                                        "level" => $param["BUBBLE_LEVEL2"][$i]
                                )) . "<br/>";
                                $msg .= localize("Son jet de maitrise de magie a été de ");
                                $msg .= localize("........................: " . $param["TARGET_MM"][$i]) . "$<br/>";
                                $msg .= localize("Son bonus d'armure a été donc de {bonus}%", array(
                                        "bonus" => $param["ARMOR_BONUS"][$i]
                                )) . "<br/>";
                            } else
                                $msg .= localize("Votre adversaire était protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["TARGET_DAMAGE_T"]
                                )) . "<br/>";
                        }
                        
                        $msg .= localize("Vous l'avez touché. Il a perdu <b>{hp} points de vie</b>.", array(
                                "hp" => $param["TARGET_HP"][$i]
                        )) . "<br/>";
                        if ($param["TARGET_KILLED"][$i] == 1)
                            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                        
                        $msg .= "<br/><br/>";
                    } else {
                        $msg .= localize("Le Jet de défense de {name} a été de {def}", array(
                                "def" => $param["TARGET_DEFENSE"][$i],
                                "name" => $param["TARGET_NAME"][$i]
                        )) . "<br/>";
                        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$i] + 1) {
                            if (isset($param["BUBBLE_LIFE2"][$i])) {
                                if (! $param["BUBBLE_CANCEL2"][$i])
                                    $msg .= localize("Votre adversaire était protégé par une bulle de vie de niveau {level} qui absorbe  {damage} points de dégâts.", array(
                                            "damage" => $param["BUBBLE_LIFE2"][$i],
                                            "level" => $param["BUBBLE_LEVEL2"][$i]
                                    )) . "<br/>";
                                else
                                    $msg .= localize("Votre adversaire était protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                            "nb" => $param["TARGET_DAMAGE_T"]
                                    )) . "<br/>";
                            }
                            
                            $msg .= localize("Vous l'avez donc touché et " . $art . " a perdu <b>{hp} points de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"][$i]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"][$i] == 1)
                                $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.") . "<br/>";
                            ;
                        } else {
                            $msg .= localize("{name} a esquivé votre attaque", array(
                                    "name" => $param["TARGET_NAME"][$i]
                            )) . "<br/>";
                            $msg .= "<br/>";
                        }
                        
                        $msg .= "<br/>";
                    }
                }
            }
            $msg .= self::msgGainXP($param);
        }
        return $msg;
    }

    /* ************************************************************* LES COMPETENCES DE L'ECOLE DE L'OMBRE ******************************************************* */
    function msgDisarm ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utiliser Désarmer sur {name} ({id})", array(
                        "name" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= self::msgBarrierEffect($param, 1);
                if (! $param["PLAYER_KILLED"]) {
                    $msg .= localize("Votre Jet d'Attaque a été de ");
                    $msg .= localize("........................: " . $param["PLAYER_ATTACK"]) . "<br/>";
                    $msg .= localize("Le Jet de Défense de votre adversaire a été de ");
                    $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
                    
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                        if (isset($param["STUPID"]))
                            $msg .= localize("Vous auriez réussi à faire tomber l'arme de votre adversaire s'il en avait eu une ! :D.") . "<br/>";
                        else
                            $msg .= localize("Vous avez réussi à désarmer votre adversaire.") . "<br/>";
                    } else {
                        $msg .= localize("Vous n'avez pas réussi à désarmer votre adversaire.") . "<br/>";
                    }
                }
            } else {
                $msg = localize("{name} ({id}) a utilisé Désarmer sur vous", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= self::msgBarrierEffect($param, 0);
                if (! $param["PLAYER_KILLED"]) {
                    $msg .= localize("Son Jet d'Attaque a été de ");
                    $msg .= localize("........................: " . $param["PLAYER_ATTACK"]) . "<br/>";
                    $msg .= localize("Votre Jet de Défense a été de ");
                    $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
                    
                    if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                        if (isset($param["STUPID"]))
                            $msg .= localize("Votre adversaire aurait réussi à faire tomber votre arme... Si vous en étiez équipé ! :D") . "<br/>";
                        else
                            $msg .= localize("Votre adversaire a réussi à vous désarmer.") . "<br/>";
                    } else {
                        $msg .= localize("Votre adversaire n'a pas réussi à vous désarmer.") . "<br/>";
                    }
                }
            }
            
            return $msg;
        }
    }

    function msgBolas ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez lancé des bolas niveau {niv} sur {target} ({id}) (Niveau : {level}) ", array(
                        "niv" => $param["BOLAS_LEVEL"],
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"],
                        "level" => $param["TARGET_LEVEL"]
                )) . " <br/><br/>";
                
                if (isset($param["NB_BOLAS"]) && $param["NB_BOLAS"] >= 2) {
                    $msg .= localize("Vous n'êtes pas parvenu à toucher votre adversaire car il est ficelé par deux bolas.") . "<br/>";
                } else {
                    $msg .= localize("Votre Jet d'Attaque a été de ");
                    $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                    $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                    $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                    if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"])
                        $msg .= localize("Vous avez touché votre adversaire, il est entravé.") . "<br/>";
                    else
                        $msg .= localize("Vous n'êtes pas parvenu à toucher votre adversaire.") . "<br/>";
                }
            } else {
                $msg = localize("{target} ({id}) vous a lancé un bolas de niveau {niv} ", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"],
                        "niv" => $param["BOLAS_LEVEL"]
                )) . " <br/><br/>";
                
                $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Votre Jet de défense a été de ");
                $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"])
                    $msg .= localize("Votre adversaire a réussi à vous entraver avec son bolas.") . "<br/>";
                else
                    $msg .= localize("Vous avez réussi à éviter le bolas.") . "<br/>";
            }
            
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgRun ($error, &$param)
    {
        $msg = localize("Vous utilisez Course Celeste") . "<br/>";
        $msg .= localize("Tous vos déplacement ne vous couteront qu'un seul PA quel que soit le type de terrain pour cette DLA et la suivante.") . "<br/>";
        $msg .= localize("Cette action vous a coûté {ap} PA", array(
                "ap" => $param["AP"]
        )) . "<br/>";
        return $msg;
    }

    function msgSteal ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez tenté de voler {target} ({id}) (Niveau : {level}) ", array(
                        "target" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"],
                        "level" => $param["TARGET_LEVEL"]
                )) . " <br/><br/>";
                $msg .= localize("Votre Jet de maîtrise a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Le Jet de maîtrise de votre adversaire a été de ");
                $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"])
                    $msg .= localize("Vous avez réussi à voler votre adversaire et vous lui avez dérobé {nb} pièces d'or.", array(
                            "nb" => $param["MONEY"]
                    )) . "<br/>";
                else
                    $msg .= localize("Vous n'avez pas réussi à voler votre adversaire.") . "<br/>";
            } else {
                $msg = localize("{target} ({id}) a tenté de vous voler", array(
                        "target" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . " <br/><br/>";
                
                $msg .= localize("Le Jet de dextérité de votre adversaire a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Votre Jet de dextérité a été de ");
                $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"])
                    $msg .= localize("Votre adversaire a réussi à vous voler et vous a dérobé  {nb} pièces d'or.", array(
                            "nb" => $param["MONEY"]
                    )) . "<br/>";
                else
                    $msg .= localize("Votre adversaire n'a pas réussi à vous voler.") . "<br/>";
            }
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgAmbush ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = localize("Vous avez utilisé Embuscade.") . "<br/>";
            if (isset($param["AMBUSH_INBUILDING"]) && $param["AMBUSH_INBUILDING"] == 1) {
                $msg .= localize("Vous êtes devenus invisible pour tous ceux qui n'était pas dans la même salle que vous ou moins au moment de l'utilisation de la compétence.") . "<br/>";
            } else {
                $msg .= localize("Vous êtes devenus invisible pour tous ceux qui n'était pas à 5 cases de vous ou moins au moment de l'utilisation de la compétence.") . "<br/>";
                $msg .= localize("N'oubliez pas ceux qui se trouvaient en salle de garde, dans le clocher ou dans l'observatoire des bâtiments alentours.") . "<br/>";
                $msg .= localize("Ceux qui étaient dans la salle d'accueil des bâtiments à 2 cases de vous ou moins, vous ont vu aussi.") . "<br/>";
            }
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a couté {ap} PA", array(
                    "ap" => $param["AP"]
            )) . "<br/><br/>";
            return $msg;
        }
    }

    /* ************************************************************* LES COMPETECENCES DE L'ECOLDE DE PALADIN ******************************************************* */
    function msgAutoRegen ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Votre Jet de Force a été de ");
            $msg .= "........................: " . $param["PLAYER_STRENGTH"] . " <br/>";
            $msg .= localize("Vous avez récupéré {nb} points de vie", array(
                    "nb" => $param["HP_HEAL"]
            )) . "<br/>";
            $msg .= localize("Vous subissez un malus de {nb} dés de force pour les 2 prochaines DLA", array(
                    "nb" => $param["MALUS_STRENGTH"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgExorcism ($error, $param)
    {
        if (! $error) {
            $msg = localize("Vous avez utilisé Exorcisme de l'ombre.") . "<br/>";
            $msg .= localize("Vous avez libéré l'énergie noire accumulée au cours des trois dernières DLA") . "<br/>";
            if ($param["NUMBER_OPPONENT"] == 0)
                $msg .= localize("Aucune de vos cibles n'a été touchée.");
            else {
                $msg .= localize("Votre Jet de Maitise de la magie a été de ");
                $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/><br/>";
                $msg .= localize($param["PLAYER_MM"] . "% de l'énergie noire que vous aviez accumulée a été transformée en dégât.") . "<br/><br/>";
                for ($i = 1; $i < $param["NUMBER_OPPONENT"] + 1; $i ++) {
                    
                    if ($param["FAR"][$i])
                        $msg .= localize("La cible {name}({id}) n'était pas à portée ou était déjà morte, il n'a pas été touché.", array(
                                "name" => $param["TARGET_NAME"][$i],
                                "id" => $param["TARGET_VALUE_ID"][$i]
                        )) . "<br/>";
                    else {
                        $msg .= localize("{name}({id}) a été ciblé par l'exorcisme de l'ombre.", array(
                                "name" => $param["TARGET_NAME"][$i],
                                "id" => $param["TARGET_VALUE_ID"][$i]
                        )) . "<br/>";
                        $msg .= localize("Il recoit donc {dmg} point de dégât", array(
                                "dmg" => $param["TARGET_DAMAGE"][$i]
                        )) . "<br/>";
                        
                        if ($param["BUBBLE"][$i]) {
                            if (! $param["BUBBLE_CANCEL2"][$i])
                                $msg .= localize("Il était protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE2"][$i],
                                        "level" => $param["BUBBLE_LEVEL2"][$i]
                                )) . "<br/>";
                            else
                                $msg .= localize("Il etait protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["PLAYER_DAMAGE"][$i]
                                )) . "<br/>";
                        }
                        
                        $msg .= localize("Il a perdu {nb} points de vie", array(
                                "name" => $param["TARGET_NAME"][$i],
                                "nb" => $param["TARGET_HP"][$i]
                        )) . "<br/>";
                        if ($param["TARGET_KILLED"][$i] == 1) {
                            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.") . "<br/>";
                            // $obj = new FallMsg();
                            // $msg.=$obj->msgBuilder($param);
                        }
                        $msg .= "<br/>";
                    }
                }
                $msg .= self::msgGainXP($param);
            }
            return $msg;
        }
    }

    function msgProtection ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Protection sur {name} ({id})", array(
                        "name" => $param["TARGET_NAME"],
                        "id" => $param["TARGET_ID"]
                )) . "<br/>";
                $msg .= localize("Tant que la compétence sera activé, vous protégerez {name}", array(
                        "name" => $param["TARGET_NAME"]
                )) . "<br/>";
            } else {
                $msg = localize("{name} ({id}) a utilisé Protection sur vous", array(
                        "name" => $param["PLAYER_NAME"],
                        "id" => $param["PLAYER_ID"]
                )) . "<br/>";
                $msg .= localize("Tant que la compétence sera activé, {name} vous protegera", array(
                        "name" => $param["PLAYER_NAME"]
                )) . "<br/>";
            }
            return $msg;
        }
    }

    function msgBravery ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            
            $msg = localize("Vous avez activé votre aura de courage") . "<br/>";
            $msg .= localize("L'ensemble de votre groupe de chasse reçoit un bonus de {nb}% en attaque", array(
                    "nb" => $param["BONUS_ATTACK"]
            )) . "<br/>";
            $msg .= localize("Tant que l'Aura sera activée vous subirez une perte de {nb} points de vie à chaque nouveau tour ", array(
                    "nb" => $param["MALUS_LIFE"]
            )) . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
        }
        
        return $msg;
    }

    function msgResistance ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            
            $msg = localize("Vous avez activé votre aura de résistance") . "<br/>";
            $msg .= localize("Votre Jet de Maitise de la magie a été de ");
            $msg .= localize("........................: " . $param["PLAYER_MM"]) . "$<br/>";
            $msg .= localize("L'ensemble de votre groupe de chasse reçoit un bonus de {nb}% en armure", array(
                    "nb" => $param["BONUS_ARMOR"]
            )) . "<br/>";
            $msg .= localize("Tant que l'Aura sera activée vous subirez un malus de 20% de force") . "<br/>";
            $msg .= localize("Cette action vous a rapporté {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/>";
        }
        
        return $msg;
    }

    /* ************************************************************* LES ARCHERS DE KRIMA ******************************************************* */
    function msgFlaming (&$error, &$param, $src)
    {
        $msg = localize("Vous avez tiré une flèche enflammée sur {name} niveau {level}", array(
                "name" => $param["BUILDING_NAME"],
                "level" => $param["BUILDING_LEVEL"]
        )) . "<br/>";
        $msg .= localize("Vous avez infligé au bâtiment une perte de {nb} points de structure", array(
                "nb" => $param["BUILDING_DAMAGE"]
        )) . "<br/>";
        if ($param["BUILDING_DEATH"])
            $msg .= localize("Vous avez <b> DETRUIT</b> le bâtiment") . "<br/>";
        return $msg;
    }

    function msgFlyArrow ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = "Vous avez utilisé Volée de Flèches" . "<br/>";
            for ($i = 1; $i < 4; $i ++) {
                $msg .= " ------- FLECHE " . $i . "-----------" . "<br/>";
                if (($i == 2 && $param["ARROW2"]) || ($i == 3 && $param["ARROW3"]))
                    $msg .= localize("Vous n'avez pas décoché pas cette flèche, car votre cible etait déjà morte") . "<br/><br/>";
                else {
                    $msg .= localize("Vous avez attaquez {target}  (malus distance = {nb}%)", array(
                            "target" => localize($param["TARGET_NAME"][$i]),
                            "nb" => $param["MALUS_GAP"][$i]
                    )) . "<br/><br/>";
                    $msg .= localize("Votre Jet d'Attaque a été de ");
                    $msg .= "........................: " . $param["PLAYER_ATTACK"][$i] . "<br/>";
                    $msg .= localize("Le Jet de Défense de votre adversaire a été de ");
                    $msg .= "........................: " . $param["TARGET_DEFENSE"][$i] . "<br/>";
                    
                    if ($param["PLAYER_ATTACK"][$i] >= $param["TARGET_DEFENSE"][$i] + 1) {
                        $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                        $msg .= localize("Votre bonus aux dégâts était de {bonus} point(s)", array(
                                "bonus" => $param["BONUS_DAMAGE"][$i]
                        )) . "<br/>";
                        if (isset($param["BUBBLE_LIFE2"][$i])) {
                            if (! $param["BUBBLE_CANCEL2"][$i])
                                $msg .= localize("Votre adversaire etait protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE2"][$i],
                                        "level" => $param["BUBBLE_LEVEL2"][$i]
                                )) . "<br/>";
                            else
                                $msg .= localize("Votre adversaire était protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["TARGET_DAMAGE"][$i]
                                )) . "<br/>";
                        }
                        $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                                "damage" => $param["TARGET_HP"][$i]
                        )) . "<br/>";
                        if ($param["TARGET_KILLED"][$i] == 1) {
                            $msg .= "<br/>";
                            $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                            $msg .= "<br/>";
                            // $obj = new FallMsg();
                            // $msg.=$obj->msgBuilder($param);
                        }
                        $msg .= "<br/>";
                    } else {
                        $msg .= localize("Vous n'êtes pas parvenu pas à toucher votre adversaire.") . "<br/><br/>";
                    }
                }
            }
        }
        return $msg;
    }

    /* ************************************************************* LES TALENT D'EXTRACTION ******************************************************* */
    function msgMine ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $fem = "e";
            if ($param["TYPE"] == "Rubis")
                $fem = "";
            $msg = localize("Vous avez extrait un" . $fem . " {gemme} de niveau {niveau} d'un gisement", array(
                    "gemme" => $param["GNAME"],
                    "niveau" => $param["LEVEL"]
            ));
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgHarvest ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error)
            $msg = localize("Vous avez récolté une {herbe} de niveau {niveau} sur un buisson", array(
                    "herbe" => $param["ELEMENT"],
                    "niveau" => $param["ELEMENT_LEVEL"]
            ));
        
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgDismember ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $fem = "e";
            if ($param["ELEMENT"] == "cuir")
                $fem = "";
            $msg = localize("Vous avez récupéré un" . $fem . " {peau} de niveau {niveau} d'une dépouille", array(
                    "peau" => $param["ELEMENT"],
                    "niveau" => $param["ELEMENT_LEVEL"]
            ));
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgScutch ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Vous avez récupéré un lin de niveau {niveau} dans un champ", array(
                    "niveau" => $param["ELEMENT_LEVEL"]
            ));
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgSpeak ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Vous avez récupéré un fer de niveau {niveau} auprès d'un Kradjeck ferreux", array(
                    "niveau" => $param["ELEMENT_LEVEL"]
            ));
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgCut ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error)
            $msg = localize("Vous avez coupé un bois de niveau {niveau}", array(
                    "niveau" => $param["ELEMENT_LEVEL"]
            ));
        
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    /* ************************************************************* LES TALENT DE RAFFINAGE******************************************************* */
    function msgRefine ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            
            $msg .= localize("Vous avez utilisé {name}.", array(
                    "name" => $param["TALENT_NAME"]
            )) . "<br/><br/>";
            $msg .= localize("Votre première pièce etait de niveau {niv1}, votre seconde pièce etait de niveau {niv2}.", array(
                    "niv1" => $param["LEVEL_OBJECT1"],
                    "niv2" => $param["LEVEL_OBJECT2"]
            )) . "<br/>";
            if ($param["TOOL"])
                $msg .= localize("Bonus outils niveau {niv}: {bonus}%", array(
                        "niv" => $param["TOOL_LEVEL"],
                        "bonus" => $param["BONUS"]
                )) . "<br/>";
            $msg .= localize("Vos chances de réussite étaient donc de {percent}%.", array(
                    "percent" => $param["PERCENT"]
            )) . " <br/><br/>";
            $msg .= localize("Votre score a été de ");
            $msg .= ".........................: " . $param["SCORE"] . " &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . "<br/>";
            if ($param["SCORE"] <= $param["PERCENT"])
                $msg .= localize("Vous êtes parvenu à confectionner une pièce de niveau {niv} ", array(
                        "niv" => $param["FINAL_LEVEL"]
                )) . "<br/><br/>";
            else
                $msg .= localize("Vous n'êtes pas parvenu à confectionner une pièce de niveau supérieur ") . "<br/><br/>";
            
            if ($param["BROKEN"])
                $msg .= localize("Vous outils niveau {niv} était trop usé, il s'est cassé", array(
                        "niv" => $param["TOOL_LEVEL"]
                )) . "<br/><br/>";
        }
        
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgKradjeckCall ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé {name}", array(
                    "name" => $param["TALENT_NAME"]
            )) . "<br/><br/>";
            $msg .= localize("Vos chances de réussite étaient de {percent}%.", array(
                    "percent" => $param["PERCENT"]
            )) . " <br/>";
            $msg .= localize("Votre score a été de ");
            $msg .= "..........................: " . $param["SCORE"] . "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" . "<br/>";
            if ($param["SCORE"] <= $param["PERCENT"])
                $msg .= localize("Vous êtes parvenu à attirer un kradjeck Ferreux.") . " <br/><br/>";
            else
                $msg .= localize("Vous n'êtes pas parvenu à attirer un Kradjeck Ferreux. ") . " <br/><br/>";
        }
        
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgMonster ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé {name} ({id})", array(
                    "name" => $param["TALENT_NAME"],
                    "id" => $param["TARGET_ID"]
            )) . "<br/><br/>";
            $levelMonster = "";
            if (isset($param["MONSTER_LEVEL"])) {
                $levelMonster = " de niveau " . $param["MONSTER_LEVEL"];
            }
            $msg .= localize("Vous avez estimé les caractéristiques de {target} ({id})" . $levelMonster . ": ", array(
                    "target" => $param["TARGET_NAME"],
                    "id" => $param["TARGET_ID"]
            )) . " <br/>";
            $msg .= localize("Vie :");
            $msg .= ".....................: " . $param["TARGET_HP"] . "<br/>";
            $msg .= localize("{name1} : ", array(
                    "name1" => $param["CHARAC1_NAME"]
            ));
            $msg .= "........................: " . $param["CHARAC1_VALUE"] . "<br/>";
            $msg .= localize("{name2} : ", array(
                    "name2" => $param["CHARAC2_NAME"]
            ));
            $msg .= "........................: " . $param["CHARAC2_VALUE"] . "<br/><br/>";
        }
        
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgDetection ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé {name}", array(
                    "name" => $param["TALENT_NAME"]
            )) . "<br/>";
            
            if (isset($param["DIRECTION"]) && $param["DIRECTION"] == 99)
                $msg .= localize("Il ne semble pas y avoir de {name} de niveau {niveau} dans les parages", array(
                        "name" => $param["NAME"],
                        "niveau" => $param["NIVEAU"]
                ));
            elseif ($param["DIST"] > 30)
                $msg .= localize("Le {name} de niveau {niveau} le plus proche se trouve à environ {dist} lieues d'ici ", array(
                        "name" => $param["NAME"],
                        "niveau" => $param["NIVEAU"],
                        "dist" => $param["DIST"]
                ));
            elseif ($param["DIRECTION"] == "")
                $msg .= localize("Le {name} de niveau {niveau} le plus proche est en vue.", array(
                        "name" => $param["NAME"],
                        "niveau" => $param["NIVEAU"]
                ));
            else
                $msg .= localize("Le {name} de niveau {niveau} le plus proche se situe à environ <b> {dist} </b> lieues en suivant la direction : <b> {dir} </b>.", array(
                        "name" => $param["NAME"],
                        "niveau" => $param["NIVEAU"],
                        "dist" => $param["DIST"],
                        "dir" => $param["DIRECTION"]
                ));
            
            $msg .= "<br/><br/>";
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    /* ************************************************************* LES TALENT D'ARTISANAT******************************************************* */
    function msgArtisanat ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé {name}", array(
                    "name" => $param["TALENT_NAME"]
            )) . "<br/><br/>";
            $msg .= localize("Votre Jet de maitrise a été de ");
            $msg .= "........................: " . $param["PLAYER_SKILL"] . "<br/>";
            
            if ($param["NEW"]) {
                $msg .= localize("La matière première utilisée etait de niveau {niv} ", array(
                        "niv" => $param["LEVEL_RAWMATERIAL"]
                )) . "<br/>";
                if ($param["EQUIP_LEVEL"] < $param["LEVEL_RAWMATERIAL"])
                    $msg .= localize("Mais vous avez préféré la travailler comme une matière première de niveau {niv} ", array(
                            "niv" => $param["EQUIP_LEVEL"]
                    )) . " <br/>";
            }
            if ($param["SUCCESS"] == 0)
                $msg .= localize("Votre maitrise d'artisanat n'a pas été suffisante pour travailler sur une matière première de ce niveau") . "<br/>";
            else {
                
                if ($param["TOOL"])
                    $msg .= localize("Votre outils de niveau {niv} vous a conféré un bonus de {bonus}% d'avancement dans la confection", array(
                            "niv" => $param["TOOL_LEVEL"],
                            "bonus" => $param["BONUS"]
                    )) . "<br/>";
                
                if ($param["END"]) {
                    if ($param["FULL"])
                        $msg .= localize("Vous n'avez confectionné que {nb} {name} de niveau {niv} car votre inventaire est plein", array(
                                "nb" => $param["NB_EQUIP"],
                                "niv" => $param["EQUIP_LEVEL"],
                                "name" => $param["EQUIP_NAME"]
                        )) . "<br/><br/>";
                    else
                        $msg .= localize("Vous avez confectionné {nb} {name} de niveau {niv} ", array(
                                "nb" => $param["NB_EQUIP"],
                                "niv" => $param["EQUIP_LEVEL"],
                                "name" => $param["EQUIP_NAME"]
                        )) . "<br/><br/>";
                } else {
                    if ($param["PROGRESS"] == 100)
                        $msg .= localize("Vous avez terminé la confection d'un(e) {name} de niveau {niv} ", array(
                                "niv" => $param["EQUIP_LEVEL"],
                                "name" => $param["EQUIP_NAME"]
                        )) . "<br/><br/>";
                    else
                        $msg .= localize("Vous avez avancé de {vit}% dans la confection d'un(e) {name} de niveau {niv} ", array(
                                "vit" => $param["SPEED"],
                                "niv" => $param["EQUIP_LEVEL"],
                                "name" => $param["EQUIP_NAME"]
                        )) . "<br/><br/>";
                }
            }
            if ($param["BROKEN"])
                $msg .= localize("Votre outils d'artisan niveau {niv} était trop usée et s'est cassée", array(
                        "niv" => $param["TOOL_LEVEL"]
                )) . "<br/><br/>";
        }
        
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgGemCraft ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez utilisé Artisanat des gemmes") . "<br/><br/>";
            $msg .= localize("Votre Jet de Maitrise de la magie a été de ");
            $msg .= "........................: " . $param["PLAYER_SKILL"] . "<br/>";
            $msg .= localize("Vous avez réalisé une maitrise d'artisanat de niveau {niv}", array(
                    "niv" => $param["LEVEL_CRAFT"]
            )) . "<br/>";
            
            if ($param["SUCCESS"] == 0)
                $msg .= localize("Votre maitrise d'artisanat n'a pas été suffisante pour travailler sur une matière première de ce niveau") . "<br/>";
            else {
                if ($param["TOOL"])
                    $msg .= localize("Votre outils de niveau {niv} vous a confèré un bonus de {bonus}% d'avancement dans la confection", array(
                            "niv" => $param["TOOL_LEVEL"],
                            "bonus" => $param["BONUS"]
                    )) . "<br/>";
                
                switch ($param["CRAFT_TYPE"]) {
                    case 1:
                        if ($param["EQUIP_ID"] != 220) {
                            $msg .= localize("L'émeraude utilisée était de niveau {niv} ", array(
                                    "niv" => $param["GEM_LEVEL"]
                            )) . "<br/>";
                            $msg .= localize("Le bois utilisé était de niveau {niv} ", array(
                                    "niv" => $param["WOOD_LEVEL"]
                            )) . "<br/>";
                        }
                        
                        if ($param["PROGRESS"] == 100)
                            $msg .= localize("Vous avez terminé la confection d'un(e) {name} de niveau {niv} ", array(
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . "<br/><br/>";
                        else
                            $msg .= localize("Vous avez avancé de {vit}% dans la confection d'un(e) {name} de niveau {niv} ", array(
                                    "vit" => $param["SPEED"],
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . "<br/><br/>";
                        break;
                    case 2:
                        if ($param["PROGRESS"] == 100)
                            $msg .= localize("Vous avez terminé la confection d'un(e) {name} de niveau {niv} ", array(
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . "<br/><br/>";
                        else
                            $msg .= localize("Vous avez avancé de {vit}% dans la confection d'un(e) {name} de niveau {niv} ", array(
                                    "vit" => $param["SPEED"],
                                    "niv" => $param["EQUIP_LEVEL"],
                                    "name" => $param["EQUIP_NAME"]
                            )) . "<br/><br/>";
                        break;
                    case 3:
                    case 4:
                        if ($param["PROGRESS"] == 100)
                            $msg .= localize("Félicitations ! Vous avez terminé d'enchanter {equip} {lev} {name} ", array(
                                    "lev" => $param["EQUIP_LEVEL"],
                                    "equip" => $param["EQUIP_NAME"],
                                    "name" => $param["TEMPLATE_NAME"]
                            )) . "<br/><br/>";
                        else
                            $msg .= localize("Vous avez avancé de {vit}% dans l'enchantement de {equip} {lev} {name} ", array(
                                    "vit" => $param["SPEED"],
                                    "lev" => $param["EQUIP_LEVEL"],
                                    "equip" => $param["EQUIP_NAME"],
                                    "name" => $param["TEMPLATE_NAME"]
                            )) . "<br/>";
                        break;
                    case 5:
                        if ($param["PROGRESS"] == 100)
                            $msg .= localize("Félicitations ! Vous avez terminé d'enchanter {equip} {lev}", array(
                                    "lev" => $param["EQUIP_LEVEL"],
                                    "equip" => $param["EQUIP_NAME"]
                            )) . "<br/><br/>";
                        else {
                            $msg .= localize("Vous avez avancé de {vit}% dans l'enchantement {equip} {lev}", array(
                                    "vit" => $param["SPEED"],
                                    "lev" => $param["EQUIP_LEVEL"],
                                    "equip" => $param["EQUIP_NAME"]
                            )) . "<br/>";
                            $msg .= localize("L'enchantement est maintenant à {vit}% d'avancement ", array(
                                    "vit" => $param["PROGRESS"]
                            )) . "<br/><br/>";
                        }
                        break;
                }
                
                if ($param["BROKEN"])
                    $msg .= localize("Votre cristal d'enchantement niveau {niv} était trop usée, il s'est cassé", array(
                            "niv" => $param["TOOL_LEVEL"]
                    )) . "<br/><br/>";
            }
        }
        return $msg;
    }

    /* ************************************************************* LES ACTIONS DANS LES BATIMENTS ******************************************************* */
    function msgTempleHeal ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez payé {po} pièces d'or pour vous faire soigner", array(
                "po" => $param["PRICE"]
        )) . "<br/>";
        $msg .= localize("Le moine a réalisé une Larme de Vie de niveau {level} ", array(
                "level" => $param["SPELL_LEVEL"]
        )) . "<br/>";
        $msg .= localize("Vous avez été soigné de {pv} points de vie", array(
                "pv" => $param["HP_HEAL"]
        )) . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => TEMPLE_HEAL_AP
        ));
        return $msg;
    }

    function msgTempleBlessing ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez payé {po} pièces d'or pour vous faire soigner", array(
                "po" => TEMPLE_BLESSING_PRICE
        )) . "<br/>";
        $msg .= localize("Le moine réalise un Souffle d'Athlan de niveau " . $param["SPELL_LEVEL"]) . "<br/>";
        $msg .= localize("Le niveau des malédictions dont vous êtiez atteint a diminué de " . $param["SPELL_LEVEL"]) . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => TEMPLE_BLESSING_AP
        ));
        return $msg;
    }

    function msgTempleResurrect ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez payé {po} pièce d'or pour que le Temple en X={x} Y={y} soit votre nouveau lieu de résurrection.", array(
                "po" => $param["PRICE"],
                "x" => $param["X"],
                "y" => $param["Y"]
        )) . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgTempleTeleport ($error, &$param, $src = 1)
    {
        $msg = localize("Vous étiez en X={x} Y={y}", array(
                "x" => $param["XOLD"],
                "y" => $param["YOLD"]
        )) . "<br/>";
        $msg .= localize("Vous avez payé {po} pièce d'or pour vous téléporter dans Temple en X={x} Y={y}", array(
                "po" => $param["PRICE"],
                "x" => $param["XNEW"],
                "y" => $param["YNEW"]
        )) . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => TELEPORT_AP
        ));
        return $msg;
    }

    function msgBedroomSleeping ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = "";
            if ($param["TYPE_ATTACK"] == HOSTEL_SLEEPING_EVENT) {
                $msg .= localize("Vous avez payé {po} pièces d'or pour vous reposer dans la chambre d'une auberge.", array(
                        "po" => HOSTEL_SLEEP_PRICE
                )) . "<br/>";
                $msg .= localize("Vous avez regagné {pv} PV.", array(
                        "pv" => $param["HPGAIN"]
                )) . "<br/><br/>";
            } else {
                $msg = localize("Vous avez dormi dans  la chambre d'une maison, vous avez réussi à vous reposer efficacement.") . "<br/>";
                $msg .= localize("Vous avez regagné {pv} PV.", array(
                        "pv" => $param["HPGAIN"]
                )) . "<br/><br/>";
            }
            
            $msg .= localize("Cette action vous a couté {ap} PA", array(
                    "ap" => SLEEPING_AP
            ));
            return $msg;
        }
    }

    function msgHostelDrink ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez bu un verre dans une auberge pour {po} pièces d'or.", array(
                "po" => HOSTEL_DRINK_PRICE
        )) . "<br/>";
        $msg .= localize("Le verre a eu l'effet suivant : {effet} ", array(
                "effet" => $param["TEXT"]
        )) . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => HOSTEL_DRINK_AP
        ));
        return $msg;
    }

    function msgHostelChat ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez discuté avec les clients d'une auberge") . "<br/>";
        $msg .= localize("Vous aviez " . $param["LIMIT"] . " pourcent de chance d'obtenir une mission.") . "<br/>";
        $msg .= localize("Votre Jet a été de ");
        $msg .= localize("........................: " . $param["JET"]) . "<br/>";
        if ($param["MISSION"])
            $msg .= localize("La conversation a été fructueuse, vous avez reçu un parchemin") . "<br/><br/>";
        else
            $msg .= localize("La conversation s'est épuisé sans avoir apporté d'information intéressante.") . "<br/><br/>";
        
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => HOSTEL_CHAT_AP
        ));
        
        return $msg;
    }

    function msgHostelRound ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez payé une tournée dans une auberge pour {po} pièces d'or", array(
                "po" => HOSTEL_ROUND_PRICE
        )) . "<br/>";
        $msg .= localize("Vous aviez " . $param["LIMIT"] . " pourcent de chance d'obtenir une mission.") . "<br/>";
        $msg .= localize("Votre Jet a été de ");
        $msg .= localize("........................: " . $param["JET"]) . "<br/>";
        if ($param["MISSION"])
            $msg .= localize("La conversation a été fructueuse, en théorie vous auriez reçu un parcho (missions/quêtes pas encore codées)") . "<br/><br/>";
        else
            $msg .= localize("La conversation s'est épuisé sans avoir apporté d'information intéressante.") . "<br/><br/>";
        
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => HOSTEL_ROUND_AP
        ));
        return $msg;
    }

    function msgHostelNews ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez posté une annonce avec succès pour " . $param["PRICE"] . " PO") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => HOSTEL_NEWS_AP
        ));
        return $msg;
    }

    function msgHostelTrade ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez posté une offre commercial avec succès pour " . $param["PRICE"] . " PO.") . "<br/>";
        $msg .= localize("Cette action vous a couté {ap} PA.", array(
                "ap" => HOSTEL_TRADE_AP
        )) . "<br/><br/>";
        $msg .= localize("Votre message a été reçu par les personnages actif ce trouvant dans un rayon de " . $param["RAYON"] . " cases autour de l'auberge dans laquelle vous vous trouvez.") . "<br/><br/>";
        $msg .= localize("Voici l'historique de votre message:") . "<br/><br/>";
        $msg .= localize("Titre : " . $param["TITLE"] . "") . "<br/><br/>";
        $msg .= localize("Votre message commercial : " . $param["BODY"] . "");
        return $msg;
    }

    function msgLearnSpell ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez appris le sortilège " . $param["SPELL_NAME"] . " pour " . $param["PRICE"] . " PO.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => $param["AP"]
        ));
        return $msg;
    }

    function msgLearnAbility ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez appris la compétence " . $param["ABILITY_NAME"] . " pour " . $param["PRICE"] . " PO.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => $param["AP"]
        ));
        return $msg;
    }

    function msgLearnTalent ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez appris le savoir-faire " . $param["TALENT_NAME"] . " pour " . $param["PRICE"] . " PO.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => $param["AP"]
        ));
        return $msg;
    }

    function msgDeposit ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez déposé " . $param["FINALBANK"] . " PO sur votre compte. Cela vous a coûté " . ($param["MONEYDEP"] - $param["FINALBANK"]) . " PO. ") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgWithDraw ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez retiré " . $param["MONEY"] . " PO de votre compte.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgBedroomDeposit ($error, &$param, $src = 1)
    {
        $msg = "Vous avez déposé " . $param["MONEYDEP"] . " PO dans la caisse de votre maison.<br/><br/>";
        $msg .= "Cette action vous a couté 0 PA";
        return $msg;
    }

    function msgBedroomWithDraw ($error, &$param, $src = 1)
    {
        $msg = "Vous avez retiré " . $param["MONEY"] . " PO de la caisse de votre maison.<br/><br/>";
        $msg .= "Cette action vous a couté 0 PA";
        return $msg;
    }

    function msgPDeposit ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez déposé " . $param["MONEYDEP"] . " PO dans les caisses d'un village") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgPWithDraw ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez retiré " . $param["MONEY"] . " PO des caisses d'un village.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgPTransferMoney ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez transféré " . $param["MONEY"] . " PO de la caisse du palais vers le bâtiment " . $param["BAT_NAME"] . " (" . $param["BAT_ID"] . ")") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgTransfert ($error, &$param, $src = 1)
    {
        $name = "";
        if ($src == 1) {
            
            if ($param["TYPE"] == 1)
                $name .= " à " . $param["TARGET_NAME"];
            elseif ($param["TYPE"] == 2)
                $name .= " au village " . $param["TARGET_NAME"];
            
            $msg = localize("Vous avez transféré " . $param["MONEY_TRANSFERED"] . " PO " . $name . ". Cela vous a coûté " . $param["FINAL_COST"] . " PO. ") . "<br/><br/>";
            $msg .= localize("Cette action vous a couté {ap} PA", array(
                    "ap" => 0
            ));
        } else
            $msg = localize($param["PLAYER_NAME"] . " vous a transféré " . $param["MONEY_TRANSFERED"] . " PO") . "<br/><br/>";
        
        return $msg;
    }

    function msgBasicBuyShop ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez acheté un(e) " . $param["EQ_NAME"] . " de niveau 1 pour " . $param["EQ_PRICE"] . " PO.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgMainBuyShop ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez acheté un(e) " . $param["EQ_NAME"] . " de niveau " . $param["EQ_LEVEL"] . " pour " . $param["EQ_PRICE"] . " PO.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }
    
    // function msgRentTool($error, &$param, $src = 1)
    // {}
    function msgSell ($error, &$param, $src = 1)
    {
        $msg = "";
        for ($i = 0; $i < $param["NOMBRE"]; $i ++) {
            $msg .= "Vous avez vendu un(e) " . $param["EQ_NAME"][$i] . ". Vous avez gagné " . $param["EQ_PRICE"][$i] . " PO." . "<br/>";
        }
        $msg .= "Vous avez gagné un total de " . $param["TOTAL_PRICE"] . " PO." . "<br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgRepair ($error, &$param, $src = 1)
    {
        $msg = "";
        for ($i = 0; $i < $param["NOMBRE"]; $i ++) {
            $msg .= "Vous avez réparé un(e) " . $param["EQ_NAME"][$i] . " pour " . $param["EQ_PRICE"][$i] . " PO." . "<br/>";
        }
        $msg .= "Vous avez payé un total de " . $param["TOTAL_PRICE"] . " PO." . "<br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgOrderEquip ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez commandé un(e) " . $param["EQ_NAME"] . " de niveau " . $param["EQ_LEVEL"] . ", cela vous a coûté " . $param["EQ_PRICE"]) . " pièces d'or" . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgTakeEquip ($error, &$param, $src = 1)
    {
        $msg = "Vous avez pris l'objet commandé(e) : un(e) " . $param["EQ_NAME"] . " de niveau " . $param["EQ_LEVEL"] . ".<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgOrderEnchant ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez commandé un enchantement " . $param["EN_NAME"] . " de niveau " . $param["EN_LEVEL"] . " sur votre " . $param["EQ_NAME"] . ", cela vous a coûté " . $param["EN_PRICE"] . " PO.") . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgCollectPackage ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = "";
            if ($src) {
                $msg .= localize("Vous avez réceptionné un colis contenant :") . "<br/>";
                for ($i = 1; $i < $param["NB"] + 1; $i ++)
                    $msg .= localize("- ") . $param["OBJECT_NAME"][$i] . "<br/>";
                
                $msg .= localize("Vous avez payé {price} pièces d'or ", array(
                        "price" => $param["PRICE"]
                )) . "<br/>";
            }
            return $msg;
        }
    }

    function msgRetrievePackage ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = "";
            if ($src) {
                $msg .= localize("Vous avez récupéré un colis oublié par son destinataire contenant :") . "<br/>";
                for ($i = 1; $i < $param["NB"] + 1; $i ++)
                    $msg .= localize("- ") . $param["OBJECT_NAME"][$i] . "<br/>";
                
                $msg .= localize("Vous avez payé {price} pièces d'or ", array(
                        "price" => $param["PRICE"]
                )) . "<br/>";
            }
            return $msg;
        }
    }

    function msgSendPackage ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = "";
            if ($src) {
                $msg .= localize("Vous avez envoyé un colis contenant :") . "<br/>";
                for ($i = 1; $i < $param["NB"] + 1; $i ++)
                    $msg .= localize("- ") . $param["OBJECT_NAME"][$i] . "<br/>";
                
                $msg .= localize("Vous avez payé {price} pièces d'or ", array(
                        "price" => EXPRESS_TRANSPORT_PRICE
                )) . "<br/>";
            }
            return $msg;
        }
    }

    function msgTakeEnchant ($error, &$param, $src = 1)
    {
        $msg = "Vous avez pris votre objet " . $param["EQ_NAME"] . "  qui a reçu un enchantement <span class='template'>" . $param["EN_NAME"] . "</span>.<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }

    function msgCaravanCreate ($error, &$param, $src = 1)
    {
        $msg = "Vous avez organisé une mission commerciale de niveau " . $param["CARAVAN_LEVEL"] . ". Cela vous a coûté " . $param["CARAVAN_PRICE"] . " PO. Vous avez reçu un parchemin avec les détails de la mission. La tortue géante qui transporte les marchandises se trouve dans les écuries." . "<br/><br/>";
        // $msg.=localize("Cette action vous a couté {ap} PA",array("ap"=> 0));
        return $msg;
    }

    function msgCaravanTerminate ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez accompli avec succès une mission commerciale de niveau " . $param["CARAVAN_LEVEL"] . ". La vente des marchandises vous a rapporté " . $param["PRICE"] . " PO .") . "<br/><br/>";
        // $msg.=localize("Cette action vous a couté {ap} PA",array("ap"=> 0));
        return $msg;
    }

    function msgBuildingDestroy ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez ordonné la démolition de " . $param["NAME"] . "(" . $param["ID"] . "). Cette action vous a couté " . $param["PRICE"] . " PO .") . "<br/><br/>";
        // $msg.=localize("Cette action vous a couté {ap} PA",array("ap"=> 0));
        return $msg;
    }

    function msgBuildingRepair ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez ordonné la réparation de " . $param["NAME"] . "(" . $param["ID"] . ") Cette action vous a couté " . $param["PRICE"] . " PO .") . "<br/><br/>";
        // $msg.=localize("Cette action vous a couté {ap} PA",array("ap"=> 0));
        return $msg;
    }

    function msgBuildingConstruct ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez lancé la construction d un(e) {name} pour {prix} pièce d'or en ({x},{y}).", array(
                "x" => $param["X"],
                "y" => $param["Y"],
                "name" => $param["NAME"],
                "prix" => $param["PRICE"]
        )) . "<br/><br/>";
        $msg .= localize("Cette action vous a couté {ap} PA", array(
                "ap" => 0
        ));
        return $msg;
    }
    
    // --------------- Décision sale du gouverneur -----------------------
    function msgPalaceDecision ($error, &$param, $src = 1)
    {
        switch ($param["DECISION"]) {
            case 2:
                $msg = localize("Vous avez oeuvré pour augmenter de 10% la loyauté du village envers son gouverneur.");
                break;
            
            case 1:
                $msg = localize("Vous avez oeuvré pour diminuer de 10% la loyauté du village envers son gouverneur.");
                break;
            
            case 3:
                $msg = localize("Vous avez engager un garde pour défendre le palais du gouverneur.");
                break;
            default:
                break;
        }
        return $msg;
    }

    function msgCreatePutsch ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez lancé un coup d'état au profit de " . $param["LEADER_NAME"] . " sur un village.");
        return $msg;
    }

    function msgActPutsch ($error, &$param, $src = 1)
    {
        if ($param["BONUS"] == 10) {
            $msg = localize("Vous avez soutenu le putsch de " . $param["LEADER_NAME"]) . "<br/>";
            if ($param["EFFECT"] == 3)
                $msg .= localize("Votre groupe a pris le contrôle du village. " . $param["LEADER_NAME"] . " est devenu le nouveau gouverneur") . "<br/>";
            else
                $msg .= localize("Votre groupe a progressé de 10% dans la prise de contrôle du village") . "<br/>";
        }
        if ($param["BONUS"] == - 10) {
            $msg = localize("Vous avez lutté contre le putsch de " . $param["LEADER_NAME"]) . "<br/>";
            if ($param["EFFECT"] == 4)
                $msg .= localize("Son putsch a été annihilé.") . "<br/>";
            else
                $msg .= localize("Vous avez réduit la prise de contrôle du village de leur groupe de 10%") . "<br/>";
        }
        return $msg;
    }

    function msgControlTemple ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez bannis des joueurs qui ressuscitaient dans le Temple de votre village. </br></br>");
        $msg .= localize("Voici la liste des joueurs que vous avez bannis : </br></br>");
        foreach ($param["PLAYER"] as $player) {
            $msg .= localize(" - " . $player . " </br>");
        }
        
        return $msg;
    }

    function msgNameVillage ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez renommé votre village: " . $param["NAME_VILLAGE"] . ".<br/><br/>");
        $msg .= localize("Cette action vous a couté {ap} PA et {money} PO.", array(
                "ap" => $param["AP"],
                "money" => $param["PRICE"]
        ));
        return $msg;
    }

    function msgFortifyVillage ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez fortifié votre village.<br/><br/>");
        $msg .= localize("Cette action vous a couté {ap} PA et vous a couté {money} PO.", array(
                "ap" => $param["AP"],
                "money" => $param["PRICE"]
        ));
        return $msg;
    }

    function msgTerrassement ($error, &$param, $src = 1)
    {
        $msg = "Vous avez terrassé votre village en " . $param["TYPE_TERRASSEMENT"] . ".<br/><br/>";
        $msg .= "Cette action vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO.";
        return $msg;
    }

    function msgTerraformation ($error, &$param, $src = 1)
    {
        if ($param["TERRAFORMATION_CASE"] == "") {
            $msg = "Vous avez terraformé le tour de votre village en " . $param["TYPE_TERRAFORMATION"] . ".<br/><br/>";
        } else {
            $msg = "Vous avez terraformé la case " . $param["TERRAFORMATION_CASE"] . " autour de votre village en " . $param["TYPE_TERRAFORMATION"] . ".<br/><br/>";
        }
        $msg .= "Cette action vous a coûté " . $param["AP"] . "PA et " . $param["PRICE"] . "PO.";
        return $msg;
    }

    function msgUpgradeFortification ($error, &$param, $src = 1)
    {
        $msg = localize("Vous avez fait améliorer les fortifications de votre village.<br/><br/>");
        $msg .= localize("Cette action vous a couté {ap} PA et vous a couté {money} PO.", array(
                "ap" => $param["AP"],
                "money" => $param["PRICE"]
        ));
        return $msg;
    }

    function msgOperateVillage ($error, &$param, $src = 1)
    {
        switch ($param["DOOR"]) {
            case 1:
                $msg = localize("Vous avez ouvert les portes du village à tous le monde");
                break;
            
            case 2:
                $msg = localize("Vous avez fermé les portes du village à tous le monde");
                break;
            
            case 3:
                $msg = localize("Vous avez choisit les options suivante pour l'ouverture des portes à vos alliés.");
                $msg = localize("Vous avez ouvert les portes du village à vos alliés selon les critères suivants :") . "<br/>";
                if ($param["OPTION_ALLY"] % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux membres de votre ordre.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 10) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert à vos alliés personnels.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 100) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux ordres de vos alliances personnelles.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 1000) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux membres alliés de votre ordre.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 10000) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux ordres alliés à votre ordre.") . "<br>";
                break;
            
            case 4:
                $msg = localize("Vous avez fermé les portes du village à tous vos ennemis");
                break;
            
            default:
                break;
        }
        return $msg;
    }

    function msgOperateVillageTemple ($error, &$param, $src = 1)
    {
        switch ($param["DOOR_TEMPLE"]) {
            case 1:
                $msg = localize("Vous avez ouvert les portes du Temple à tous le monde");
                break;
            
            case 2:
                $msg = localize("Vous avez fermé les portes du Temple à tous le monde");
                break;
            
            case 3:
                $msg = localize("Vous avez choisit les options suivante pour l'ouverture du Temple à vos alliés.");
                $msg = localize("Vous avez ouvert les portes du Temple à vos alliés selon les critères suivants :") . "<br/>";
                if ($param["OPTION_ALLY"] % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux membres de votre ordre.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 10) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert à vos alliés personnels.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 100) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux ordres de vos alliances personnelles.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 1000) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux membres alliés de votre ordre.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 10000) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux ordres alliés à votre ordre.") . "<br>";
                break;
            
            case 4:
                $msg = localize("Vous avez fermé les portes du Temple à tous vos ennemis");
                break;
            
            default:
                break;
        }
        return $msg;
    }

    function msgOperateVillageEntrepot ($error, &$param, $src = 1)
    {
        switch ($param["DOOR_TEMPLE"]) {
            case 1:
                $msg = localize("Vous avez ouvert les portes de(s) Entrepôt(s) à tous le monde");
                break;
            
            case 2:
                $msg = localize("Vous avez fermé les portes de(s) Entrepôt(s) à tous le monde");
                break;
            
            case 3:
                $msg = localize("Vous avez choisit les options suivante pour l'ouverture des Entrepôts à vos alliés.");
                $msg = localize("Vous avez ouvert les portes de(s) Entrepôt(s) à vos alliés selon les critères suivants :") . "<br/>";
                if ($param["OPTION_ALLY"] % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux membres de votre ordre.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 10) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert à vos alliés personnels.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 100) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux ordres de vos alliances personnelles.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 1000) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux membres alliés de votre ordre.") . "<br>";
                if (floor($param["OPTION_ALLY"] / 10000) % 10 == 3)
                    $msg .= localize("- Vous avez ouvert aux ordres alliés à votre ordre.") . "<br>";
                break;
            
            case 4:
                $msg = localize("Vous avez fermé les portes de(s) Entrepôt(s) à tous vos ennemis");
                break;
            
            default:
                break;
        }
        return $msg;
    }

    /* ************************************************ ACTION PASSIVE ****************************************** */
    function msgPassiveTwirl ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé tournoiement sur " . $param["TARGET_NAME"]);
                if ($param["PROTECTION"])
                    $msg .= localize(" qui s'est interposé pour défendre {target}", array(
                            "name" => $param["TARGET_NAME"],
                            "target" => $param["INTER_NAME"]
                    ));
                $msg .= ".<br/>";
                $msg .= self::msgBarrierEffect($param, $src);
                
                if (! $param["PLAYER_KILLED"]) {
                    $msg .= localize("Votre jet d'attaque a été de " . $param["PLAYER_ATTACK"]) . ".<br/>";
                    $msg .= localize("Votre jet de dégâts a été de " . $param["TARGET_DAMAGE_T"]) . ".<br/><br/>";
                    if ($param["PROTECTION"]) {
                        if (isset($param["BUBBLE_LIFE"])) {
                            if (! $param["BUBBLE_CANCEL"]) {
                                $msg .= localize("Votre adversaire était protégé par une bulle de vie de niveau {level} qui a absorbé {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE"],
                                        "level" => $param["BUBBLE_LEVEL"]
                                )) . "<br/>";
                                $msg .= localize("Son jet de maitrise de magie a été de ");
                                $msg .= localize("........................: " . $param["TARGET_MM"]) . "$<br/>";
                                $msg .= localize("Son bonus d'armure était donc de {bonus}%", array(
                                        "bonus" => $param["ARMOR_BONUS"]
                                )) . "<br/>";
                            } else
                                $msg .= localize("Votre adversaire était protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["PLAYER_DAMAGE"]
                                )) . "<br/>";
                        }
                        
                        $msg .= localize("Vous adversaire a été touché et il a perdu <b>{hp} points de vie</b>.", array(
                                "hp" => $param["TARGET_HP"]
                        )) . "<br/>";
                        if ($param["TARGET_KILLED"] == 1) {
                            $msg .= localize("Vous avez été <b>TUÉ</b>") . "<br/>";
                            $msg .= self::msgInfoDeath($param);
                        }
                        
                        $msg .= "<br/><br/>";
                    } else {
                        $msg .= localize("Le jet de défense de votre adversaire a été de {def}.", array(
                                "def" => $param["TARGET_DEFENSE"],
                                "name" => $param["TARGET_NAME"]
                        )) . "<br/>";
                        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                            $plur = "";
                            if ($param["TARGET_HP"] > 1)
                                $plur = "s";
                            
                            $msg .= self::msgPassiveBubble($param, 1, 0);
                            $msg .= localize("Votre adversaire a été touché et a perdu <b>{hp} point" . $plur . " de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"] == 1)
                                $msg .= localize("Votre adversaire a été <b>TUÉ</b>.") . "<br/>";
                            ;
                        } else {
                            $msg .= localize("Votre adversaire a esquivé votre attaque", array(
                                    "name" => $param["TARGET_NAME"]
                            )) . "<br/>";
                            $msg .= "<br/>";
                        }
                        $msg .= "<br/>";
                    }
                }
            } else {
                $msg = localize("Vous avez subit le tournoiement de " . $param["PLAYER_NAME"]);
                if ($param["PROTECTION"])
                    $msg .= localize(" car vous vous êtes interposé pour défendre {target}", array(
                            "name" => $param["TARGET_NAME"],
                            "target" => $param["INTER_NAME"]
                    ));
                $msg .= ".<br/>";
                $msg .= self::msgBarrierEffect($param, $src);
                
                if (! $param["PLAYER_KILLED"]) {
                    $msg .= localize("Son jet d'attaque a été de " . $param["PLAYER_ATTACK"]) . ".<br/>";
                    $msg .= localize("Son jet de dégâts a été de " . $param["TARGET_DAMAGE_T"]) . ".<br/><br/>";
                    if ($param["PROTECTION"]) {
                        if (isset($param["BUBBLE_LIFE"])) {
                            if (! $param["BUBBLE_CANCEL"]) {
                                $msg .= localize("Vous étiez protégé par une bulle de vie de niveau {level} qui a absorbé {damage} points de dégâts.", array(
                                        "damage" => $param["BUBBLE_LIFE"],
                                        "level" => $param["BUBBLE_LEVEL"]
                                )) . "<br/>";
                                $msg .= localize("Votre jet de maitrise de magie a été de ");
                                $msg .= localize("........................: " . $param["TARGET_MM"]) . "$<br/>";
                                $msg .= localize("Votre bonus d'armure était donc de {bonus}%", array(
                                        "bonus" => $param["ARMOR_BONUS"]
                                )) . "<br/>";
                            } else
                                $msg .= localize("Vous étiez protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                        "nb" => $param["PLAYER_DAMAGE"]
                                )) . "<br/>";
                        }
                        
                        $msg .= localize("Vous avez été touché et vous avez perdu <b>{hp} points de vie</b>.", array(
                                "hp" => $param["TARGET_HP"]
                        )) . "<br/>";
                        if ($param["TARGET_KILLED"] == 1) {
                            $msg .= localize("Vous avez été <b>TUÉ</b>") . "<br/>";
                            $msg .= self::msgInfoDeath($param);
                        }
                        $msg .= "<br/><br/>";
                    } else {
                        $msg .= localize("Votre jet de défense a été de {def}.", array(
                                "def" => $param["TARGET_DEFENSE"],
                                "name" => $param["TARGET_NAME"]
                        )) . "<br/>";
                        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                            
                            $msg .= self::msgPassiveBubble($param, 0, 0);
                            $plur = "";
                            if ($param["TARGET_HP"] > 1)
                                $plur = "s";
                            
                            $msg .= localize("Vous avez été touché et vous avez perdu <b>{hp} point" . $plur . " de vie</b>.", array(
                                    "hp" => $param["TARGET_HP"]
                            )) . "<br/>";
                            if ($param["TARGET_KILLED"] == 1) {
                                $msg .= localize("Vous avez été <b>TUÉ</b>.") . "<br/>";
                                $msg .= self::msgInfoDeath($param);
                            }
                        } else {
                            $msg .= localize("Vous avez esquivé l'attaque de votre adversaire", array(
                                    "name" => $param["TARGET_NAME"]
                            )) . "<br/>";
                            $msg .= "<br/>";
                        }
                        $msg .= "<br/>";
                    }
                }
            }
            return $msg;
        }
    }

    function msgPassiveBier ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Vous avez subit les vapeurs de bière dégagé par " . $param["PLAYER_NAME"]) . "<br/>";
            $msg .= localize("Vous avez reçu un malus de {nb} D6 en force et en maitrise de la magie pour 2 dla.", array(
                    "nb" => $param["LEVEL"] * 2
            )) . "<br/>";
            $msg .= "<br/>";
        }
        
        return $msg;
    }

    function msgPassiveFire ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            if ($src) {
                if ($param["TARGET_IS_NPC"]) {
                    if (isset($param["TARGET_GENDER"])) {
                        if ($param["TARGET_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else
                        $art = "";
                    
                    $msg = localize("Vous avez lancé brasier sur {target} ({id})", array(
                            "target" => $art . " " . localize($param["TARGET_NAME"]),
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                } else
                    $msg = localize("Vous avez lancé brasier sur {target} ({id})", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                
                $msg .= localize("Votre Jet d'attaque a été ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                $msg .= "............: " . $param["TARGET_DEF"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] > $param["TARGET_DEF"]) {
                    $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                    $msg .= localize("Votre jet de maitrise de la magie a été de {mm}", array(
                            "mm" => $param["PLAYER_MM"]
                    )) . "<br/>";
                    $msg .= localize("Vous avez réalisé un sort de niveau : <b>{level}</b> ({nb}D6 de dégâts)", array(
                            "nb" => ($param["SPELL_LEVEL"]),
                            "level" => $param["SPELL_LEVEL"]
                    ));
                    $msg .= "<br/>";
                    $msg .= self::msgPassiveBubble($param, 1);
                    $msg .= localize("Vous lui avez infligé {damage} points de vie.", array(
                            "damage" => $param["TARGET_DAM2"]
                    )) . "<br/>";
                    
                    if ($param["TARGET_KILLED"] == 1) {
                        $msg .= "<br/>";
                        $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                        $msg .= "<br/>";
                    }
                    
                    $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                            "xp" => $param["XP"]
                    ));
                    $msg .= "<br/>";
                }
            } 

            else {
                if ($param["PLAYER_IS_NPC"]) {
                    if (isset($param["PLAYER_GENDER"])) {
                        if ($param["PLAYER_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else {
                        $art = "";
                    }
                    $msg = localize("Vous avez subi le braiser de {target} ", array(
                            "target" => $art . " " . localize($param["PLAYER_NAME"])
                    )) . "<br/><br/>";
                } else
                    $msg = localize("Vous avez subi le brasier de {target} ", array(
                            "target" => $param["PLAYER_NAME"]
                    )) . "<br/><br/>";
                
                $msg .= localize("Le Jet d'attaque de votre adversaire a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Votre Jet de défense a été ");
                $msg .= "............: " . $param["TARGET_DEF"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] > $param["TARGET_DEF"]) {
                    $msg .= localize("Vous avez donc été <b>TOUCHÉ</b>") . "<br/>";
                    $msg .= localize("Le jet de maitrise de la magie de votre adversaire a été de {mm}", array(
                            "mm" => $param["PLAYER_MM"]
                    )) . "<br/>";
                    $msg .= localize("Il a donc réalisé un sort de niveau : <b>{level}</b>", array(
                            "nb" => ($param["SPELL_LEVEL"]),
                            "level" => $param["SPELL_LEVEL"]
                    )) . "<br/>";
                    $msg .= self::msgPassiveBubble($param, 0);
                    $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                            "damage" => $param["TARGET_DAM2"]
                    )) . "<br/>";
                    if ($param["TARGET_KILLED"] == 1) {
                        $msg .= "<br/>";
                        $msg .= localize("Vous avez été <b>TUÉ(E)</b>.");
                        $msg .= "<br/>";
                        $msg .= self::msgInfoDeath($param);
                    }
                }
            }
        }
        return $msg;
    }

    function msgPassiveProjection ($error, $param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez été projeté sur " . $param["TARGET_NAME"] . "(" . $param["TARGET_ID"] . ")") . "<br/>";
                
                $msg .= self::msgBarrierEffect($param, $src);
                
                if ($param["BUBBLE_OBST"]) {
                    if (! $param["BUBBLE_CANCEL_OBST"])
                        $msg .= localize("Il était protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE_OBST"],
                                "level" => $param["BUBBLE_LEVEL_OBST"]
                        )) . "<br/>";
                    else
                        $msg .= "Il etait protégé par une bulle de vie qui a absorbé la totalité du coup." . "<br/>";
                }
                
                $msg .= localize("Il a perdu {nb} points de vie", array(
                        "nb" => $param["DAMAGE_OBST"]
                )) . "<br/>";
                if ($param["OBST_KILLED"] == 1) {
                    $msg .= localize("Il a été <b>TUÉ</b> par le choc.") . "<br/>";
                }
                
                $msg .= "<br/>";
                
                if ($param["BUBBLE_OPP"]) {
                    if (! $param["BUBBLE_CANCEL_OPP"])
                        $msg .= localize("Vous étiez protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE_OPP"],
                                "level" => $param["BUBBLE_LEVEL_OPP"]
                        )) . "<br/>";
                    else
                        $msg .= "Vous étiez protégé par une bulle de vie qui a absorbé la totalité du coup." . "<br/>";
                }
                
                $msg .= localize("Vous avez perdu {nb} points de vie", array(
                        "nb" => $param["DAMAGE_OPP"]
                )) . "<br/>";
                if ($param["OPP_KILLED"] == 1) {
                    $msg .= localize("Vous avez été <b>TUÉ</b> par le choc.") . "<br/>";
                }
                
                $msg .= "<br/>";
            } else {
                
                $msg = localize($param["PLAYER_NAME"] . "(" . $param["PLAYER_ID"] . ") a été projeté sur vous.") . "<br/>";
                
                $msg .= self::msgBarrierEffect($param, $src);
                
                if ($param["BUBBLE_OPP"]) {
                    if (! $param["BUBBLE_CANCEL_OPP"])
                        $msg .= localize("Il était protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE_OPP"],
                                "level" => $param["BUBBLE_LEVEL_OPP"]
                        )) . "<br/>";
                    else
                        $msg .= "Il était protégé par une bulle de vie qui a absorbé la totalité du coup." . "<br/>";
                }
                
                $msg .= localize("Il a perdu {nb} points de vie", array(
                        "nb" => $param["DAMAGE_OPP"]
                )) . "<br/>";
                if ($param["OPP_KILLED"] == 1) {
                    $msg .= localize("Il a été <b>TUÉ</b> par le choc.") . "<br/>";
                }
                
                $msg .= "<br/>";
                
                if ($param["BUBBLE_OBST"]) {
                    if (! $param["BUBBLE_CANCEL_OBST"])
                        $msg .= localize("Vous étiez protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE_OBST"],
                                "level" => $param["BUBBLE_LEVEL_OBST"]
                        )) . "<br/>";
                    else
                        $msg .= "Vous étiez protégé par une bulle de vie qui a absorbé la totalité du coup." . "<br/>";
                }
                
                $msg .= localize("Vous avez perdu {nb} points de vie", array(
                        "nb" => $param["DAMAGE_OBST"]
                )) . "<br/>";
                if ($param["OBST_KILLED"] == 1) {
                    $msg .= localize("Vous avez été <b>TUÉ</b> par le choc.") . "<br/>";
                }
                
                $msg .= "<br/>";
            }
        }
        
        return $msg;
    }

    function msgPassiveProjectionBuilding ($error, $param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez été projeté sur le bâtiment " . $param["NAME_BUILDING"] . "(" . $param["BUILDING_ID"] . ")") . "<br/>";
                
                $msg .= localize("Il a perdu {nb} points de structure", array(
                        "nb" => $param["DAMAGE_OBST"]
                )) . "<br/>";
                if ($param["OBST_KILLED"] == 1) {
                    $msg .= localize("Il a été <b>DÉTRUIT</b> par le choc.") . "<br/>";
                }
                
                $msg .= "<br/>";
                
                if ($param["BUBBLE_OPP"]) {
                    if (! $param["BUBBLE_CANCEL_OPP"])
                        $msg .= localize("Vous étiez protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE_OPP"],
                                "level" => $param["BUBBLE_LEVEL_OPP"]
                        )) . "<br/>";
                    else
                        $msg .= "Vous étiez protégé par une bulle de vie qui a absorbé la totalité du coup." . "<br/>";
                }
                
                $msg .= localize("Vous avez perdu {nb} points de vie", array(
                        "nb" => $param["DAMAGE_OPP"]
                )) . "<br/>";
                if ($param["OPP_KILLED"] == 1) {
                    $msg .= localize("Vous avez été <b>TUÉ</b> par le choc.") . "<br/>";
                }
                
                $msg .= "<br/>";
            }
        }
        
        return $msg;
    }

    function msgPassiveExorcism ($error, $param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("Vous avez utilisé Exorcisme de l'ombre.") . "<br/>";
                $msg .= localize("Vous avez libéré l'énergie noire accumulée au cours des trois dernières DLA") . "<br/>";
                $msg .= localize("Votre Jet de Maitise de la magie a été de ");
                $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/>";
                $msg .= localize($param["PLAYER_MM"] . "% de l'énergie noire que vous aviez accumulée a été transformée en dégât.") . "<br/><br/>";
                
                $msg .= localize("{name} a été ciblé par l'exorcisme de l'ombre.", array(
                        "name" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("Il a reçu {dmg} point de dégât", array(
                        "dmg" => $param["TARGET_DAMAGE"]
                )) . "<br/>";
                
                if ($param["BUBBLE"]) {
                    if (! $param["BUBBLE_CANCEL"])
                        $msg .= localize("Il était protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE"],
                                "level" => $param["BUBBLE_LEVEL"]
                        )) . "<br/>";
                    else
                        $msg .= localize("Il etait protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                "nb" => $param["PLAYER_DAMAGE"]
                        )) . "<br/>";
                }
                
                $msg .= localize("Il a perdu {nb} points de vie", array(
                        "name" => $param["TARGET_NAME"],
                        "nb" => $param["TARGET_HP"]
                )) . "<br/>";
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.") . "<br/>";
                    // $obj = new FallMsg();
                    // $msg.=$obj->msgBuilder($param);
                }
                $msg .= "<br/>";
            } else {
                $msg = localize("Vous avez été ciblé par l'éxorcisme de l'ombre de {player}.", array(
                        "player" => $param["PLAYER_NAME"]
                )) . "<br/>";
                $msg .= localize("Son Jet de Maitise de la magie a été de ");
                $msg .= localize("........................: " . $param["PLAYER_MM"]) . "<br/><br/>";
                $msg .= localize("Vous subissez " . $param["PLAYER_MM"] . "% des dégâts magiques que vous lui avez infligé les 3 dernières DLA .") . "<br/>";
                $msg .= localize("Vous avez donc reçu {dmg} points de dégât", array(
                        "dmg" => $param["TARGET_DAMAGE"]
                )) . "<br/>";
                if ($param["BUBBLE"]) {
                    if (! $param["BUBBLE_CANCEL"])
                        $msg .= localize("Vous étiez protégé par une bulle de vie de niveau {level} qui a absorbé  {damage} points de dégâts.", array(
                                "damage" => $param["BUBBLE_LIFE"],
                                "level" => $param["BUBBLE_LEVEL"]
                        )) . "<br/>";
                    else
                        $msg .= localize("Vous etiez protégé par une bulle de vie qui a absorbé la totalité du coup ({nb} points de damage).", array(
                                "nb" => $param["PLAYER_DAMAGE"]
                        )) . "<br/>";
                }
                
                $msg .= localize("Vous avez perdu {nb} points de vie", array(
                        "name" => $param["TARGET_NAME"],
                        "nb" => $param["TARGET_HP"]
                )) . "<br/>";
                if ($param["TARGET_KILLED"] == 1) {
                    $msg .= localize("Vous avez été <b>TUÉ</b>.") . "<br/>";
                    $msg .= self::msgInfoDeath($param);
                    // $msg.=$obj->msgBuilder($param);
                }
                $msg .= "<br/>";
            }
        }
        
        return $msg;
    }

    function msgPassiveRain ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            if ($src) {
                if ($param["TARGET_IS_NPC"]) {
                    if (isset($param["TARGET_GENDER"])) {
                        if ($param["TARGET_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else
                        $art = "";
                    
                    $msg = localize("Vous avez lancé pluie sacrée sur {target} ({id})", array(
                            "target" => $art . " " . localize($param["TARGET_NAME"]),
                            "id" => $param["TARGET_ID"]
                    )) . "<br/>";
                } else
                    $msg = localize("Vous avez lancé pluie sacrée sur {target} ({id})", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                
                $msg .= localize("Votre jet de MM a été de : {mm} ", array(
                        "mm" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Vous avez réalisé un sort de niveau {level} ", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                $msg .= localize("Nombre de point potentiel de guérison : {PV}", array(
                        "PV" => $param["HP_POTENTIEL"]
                )) . "<br/>";
                $msg .= localize("{target} est soigné de {hp} points de vie", array(
                        "target" => $param["TARGET_NAME"],
                        "hp" => $param["HP_HL"]
                )) . "<br/><br/>";
                $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                        "xp" => $param["XP"]
                ));
                $msg .= "<br/>";
            } else {
                if ($param["PLAYER_IS_NPC"]) {
                    if (isset($param["PLAYER_GENDER"])) {
                        if ($param["PLAYER_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else {
                        $art = "";
                    }
                    $msg = localize("Vous avez été régénéré par pluie sacrée de {target} ", array(
                            "target" => $art . " " . localize($param["PLAYER_NAME"])
                    )) . "<br/>";
                } else
                    $msg = localize("Vous avez été régénéré par la pluie sacrée de {target} ", array(
                            "target" => $param["PLAYER_NAME"]
                    )) . "<br/><br/>";
                
                $msg .= localize("Son jet de MM a été de : {mm} ", array(
                        "mm" => $param["PLAYER_MM"]
                )) . "<br/>";
                $msg .= localize("Il a réalisé un sort de niveau {level} ", array(
                        "level" => $param["SPELL_LEVEL"]
                )) . "<br/><br/>";
                $msg .= localize("Nombre de point potentiel de guérison : {PV}", array(
                        "PV" => $param["HP_POTENTIEL"]
                )) . "<br/>";
                $msg .= localize("Vous avez été soigné de {hp} points de vie", array(
                        "target" => $param["TARGET_NAME"],
                        "hp" => $param["HP_HL"]
                )) . "<br/>";
            }
            return $msg;
        }
    }

    function msgPassiveSun ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg .= localize("Vous avez été guérit automatiquement de {hp} point de vie par le Soleil de guérison de {target}", array(
                    "hp" => $param["PLAYER_HP"],
                    "target" => $param["PLAYER_NAME"]
            )) . "<br/>";
            
            return $msg;
        }
    }

    function msgPassiveFlyArrow ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            if ($src) {
                if ($param["TARGET_IS_NPC"]) {
                    if (isset($param["TARGET_GENDER"])) {
                        if ($param["TARGET_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else {
                        $art = "";
                    }
                    $msg = localize("Vous avez tiré une flèche de votre volée sur {target} ({id}) (Niveau : {level}) ", array(
                            "target" => $art . " " . localize($param["TARGET_NAME"]),
                            "id" => $param["TARGET_ID"],
                            "level" => $param["TARGET_LEVEL"]
                    )) . " <br/><br/>";
                } else
                    $msg = localize("Vous avez tiré une flèche de votre volée sur {target} ({id}) (Niveau : {level}) ", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"],
                            "level" => $param["PLAYER_LEVEL"]
                    )) . " <br/><br/>";
                
                $msg .= localize("Votre Jet d'Attaque a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Le Jet de défense de votre adversaire a été de ");
                $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Vous avez donc <b>TOUCHÉ</b> votre adversaire") . "<br/>";
                    $msg .= localize("Votre bonus aux dégâts a été de {bonus} point(s)", array(
                            "bonus" => $param["BONUS_DAMAGE"]
                    )) . "<br/>";
                    $msg .= localize("Vous lui avez infligé {damage} points de dégâts.", array(
                            "damage" => $param["TARGET_DAMAGE"]
                    )) . "<br/>";
                    $msg .= self::msgPassiveBubble($param, 1);
                    $msg .= localize("Son armure l'a protégé et il n'a perdu que <b>{hp} point de vie </b>", array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                    
                    if ($param["TARGET_KILLED"] == 1) {
                        $msg .= "<br/>";
                        $msg .= localize("Vous avez <b>TUÉ</b> votre adversaire.");
                        $msg .= "<br/>";
                    }
                } else
                    $msg .= localize("Vous ne parvenez pas à toucher votre adversaire.") . "<br/ >";
            } else {
                if ($param["PLAYER_IS_NPC"]) {
                    if (isset($param["PLAYER_GENDER"])) {
                        if ($param["PLAYER_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else {
                        $art = "";
                    }
                    $msg = localize("Vous avez été attaqué par une des flèches de la volée de {target} ({id}) (Niveau : {level}) ", array(
                            "target" => $art . " " . localize($param["PLAYER_NAME"]),
                            "level" => $param["PLAYER_LEVEL"],
                            "id" => $param["TARGET_ID"]
                    )) . " <br/><br/>";
                } else
                    $msg = localize("Vous avez été attaqué par une des flèches de la volée de {target} ({id}) (Niveau : {level}) ", array(
                            "target" => $param["PLAYER_NAME"],
                            "level" => $param["PLAYER_LEVEL"],
                            "id" => $param["TARGET_ID"]
                    )) . " <br/><br/>";
                
                $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
                $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
                $msg .= localize("Votre Jet de défense a été de ");
                $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Votre adversaire vous a donc <b>TOUCHÉ</b>") . "<br/>";
                    $msg .= localize("Son bonus de dégâts a été de {bonus} point(s)", array(
                            "bonus" => $param["BONUS_DAMAGE"]
                    )) . "<br/>";
                    $msg .= localize("Il vous a infligé {damage} points de dégâts.", array(
                            "damage" => $param["TARGET_DAMAGE"]
                    )) . "<br/>";
                    
                    $msg .= self::msgPassiveBubble($param, 0);
                    $msg .= localize("Votre armure vous a protégé et vous n'avez perdu que <b>{hp} point de vie </b>", array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                    
                    if ($param["TARGET_KILLED"] == 1) {
                        $msg .= "<br/>";
                        $msg .= localize("Vous avez été <b>TUÉ</b>.");
                        $msg .= "<br/>";
                        $msg .= self::msgInfoDeath($param);
                    }
                } else {
                    $msg .= localize("Vous adversaire n'est pas parvenu à vous toucher.") . "$<br/ >";
                }
            }
            
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgPassiveBarrierDeath ($error, &$param, $src = 1)
    {
        if (! $error) {
            if ($src) {
                $msg = localize("{name} a attaqué {target} .", array(
                        "target" => $param["INTER_NAME"],
                        "name" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("{target} était protégé(e) par votre barrière enflammée.", array(
                        "target" => $param["INTER_NAME"]
                )) . "<br/>";
                $msg .= localize("La barrière enflammée lui a infligé {nb} points de dégât.", array(
                        "nb" => $param["PLAYER_DAMAGE"]
                )) . "<br/>";
                $msg .= localize("{name} a été tué", array(
                        "name" => $param["TARGET_NAME"]
                )) . "<br/><br/>";
                $msg .= self::msgGainXP($param);
            } else {
                $msg = localize("Vous avez attaqué {target} .", array(
                        "target" => $param["INTER_NAME"],
                        "name" => $param["TARGET_NAME"]
                )) . "<br/>";
                $msg .= localize("{name} était protégé(e) votre barrière enflammée créé par {target}.", array(
                        "name" => $param["INTER_NAME"],
                        "target" => $param["PLAYER_NAME"]
                )) . "<br/>";
                $msg .= localize("La barrière enflammée vous a infligé {nb} points de dégât.", array(
                        "nb" => $param["PLAYER_DAMAGE"]
                )) . "<br/>";
                $msg .= localize("Vous avez été tué(e)");
            }
            
            return $msg;
        }
    }

    function msgPassiveBloodDeath ($error, &$param, $src = 1)
    {
        $msg = "";
        if ($src) {
            $msg = localize("{target} ({id}) a succombé de la perte de vie engendrée par le sortilège Sang de Lave que vous lui avez lancé", array(
                    "target" => localize($param["TARGET_NAME"]),
                    "id" => $param["TARGET_ID"]
            )) . "<br/><br/>";
            $msg .= localize("Vous avez gagné {xp} PX", array(
                    "xp" => localize($param["XP"])
            )) . "<br/><br/>";
        } else
            $msg = localize("Vous avez succombé de la perte de vie engendrée par la malédiction Sang de Lave de {target} ({id})", array(
                    "target" => localize($param["PLAYER_NAME"]),
                    "id" => $param["TARGET_ID"]
            )) . "<br/><br/>";
        
        return $msg;
    }

    function msgPassiveWoundDeath ($error, &$param, $src = 1)
    {
        $msg = "";
        if ($src) {
            $msg = localize("{target} ({id}) a succombé de la perte de vie engendrée par la blessure profonde que vous lui avez infligé", array(
                    "target" => localize($param["TARGET_NAME"]),
                    "id" => $param["TARGET_ID"]
            )) . "<br/><br/>";
            $msg .= localize("Vous avez gagné {xp} PX", array(
                    "xp" => $param["XP"]
            )) . "<br/><br/>";
        } else
            $msg = localize("Vous avez succombé de la perte de vie engendrée par la malédiction Sang de Lave de {target} ", array(
                    "target" => localize($param["PLAYER_NAME"])
            )) . "<br/><br/>";
        
        return $msg;
    }

    function msgPassivePoisonDeath ($error, &$param, $src = 1)
    {
        $msg = "";
        if ($src)
            $msg = localize("{target} ({id})a succombé de la perte de vie engendrée par le poison de ", array(
                    "target" => localize($param["TARGET_NAME"]),
                    "id" => $param["TARGET_ID"]
            )) . "<br/><br/>";
        else
            $msg = localize("Vous avez succombé de la perte de vie engendrée par le poison produit par {target} ", array(
                    "target" => localize($param["PLAYER_NAME"])
            )) . "<br/><br/>";
        
        return $msg;
    }

    function msgPassiveInfestDeath ($error, &$param)
    {
        $msg = localize("Vous étiez infesté(e) par une {target}. Vous avez succombé de la perte de vie engendrée par la maladie", array(
                "target" => $param["PLAYER_NAME"]
        )) . "<br/><br/>";
        return $msg;
    }

    /* ************************************************ ACTION SPECIALE DES MONSTRES ****************************************** */
    function msgInstantBlood ($error, &$param, $src = 1)
    {
        $msg = "";
        
        if (! $error) {
            if ($src) {
                if ($param["TARGET_IS_NPC"]) {
                    if (isset($param["TARGET_GENDER"])) {
                        if ($param["TARGET_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else
                        $art = "";
                    
                    $msg = localize("Vous avez attaqué avec un Rayon de lumière Noire {target} ({id})", array(
                            "target" => $art . " " . localize($param["TARGET_NAME"]),
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                } else
                    $msg = localize("Vous avez attaqué avec un Rayon de lumière Noire {target} ({id})", array(
                            "target" => $param["TARGET_NAME"],
                            "id" => $param["TARGET_ID"]
                    )) . "<br/><br/>";
                $msg .= localize("Votre Jet de maitrise de la magie a été de");
                $msg .= "........................: " . $param["PLAYER_ATTACK"] . "<br/>";
                $msg .= localize("Le Jet de maitrise de la magie de votre adversaire a été de ");
                $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Vous avez réalisé un sort de niveau {level}", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                    )) . "<br/>";
                    if (isset($param["LEVEL_BENE"])) {
                        $msg .= localize("Votre adversaire était protégé par une bénédiction de niveau {level}.", array(
                                "level" => $param["LEVEL_BENE"]
                        )) . "<br/>";
                        if ($param["CANCEL"])
                            $msg .= localize("Sa protection etait plus puissante que votre sortilège et en a annulé les effets") . "<br/>";
                        else
                            $msg .= localize("Les effets de votre sortilège ont été réduit de {level} à {niveau} points de dégât pour les 5 prochaines dla.", array(
                                    "level" => $param["TARGET_DAMAGE"],
                                    "niveau" => ($param["TARGET_DAMAGE"] - $param["LEVEL_BENE"])
                            )) . "<br/>";
                    } else
                        $msg .= localize("Vous avez infligé à votre adversaire une malédiction de {damage} points de dégâts pour les 5 prochaines dla.", array(
                                "damage" => $param["TARGET_DAMAGE"]
                        )) . "<br/>";
                } 

                else {
                    $msg .= localize("Vous adversaire a contré votre sort.") . "<br/><br/>";
                }
                $msg .= "<br/>";
                $msg .= "<br/>";
                $msg .= localize("Pour cette Action, vous avez gagné un total de {xp} PX.", array(
                        "xp" => $param["XP"]
                ));
            } else {
                if ($param["PLAYER_IS_NPC"]) {
                    if (isset($param["PLAYER_GENDER"])) {
                        if ($param["PLAYER_GENDER"] == "M")
                            $art = localize("un");
                        else
                            $art = localize("une");
                    } else {
                        $art = "";
                    }
                    $msg = localize("{target} a utilisé Rayon de lumière noire ", array(
                            "target" => $art . " " . localize($param["PLAYER_NAME"])
                    )) . "<br/><br/>";
                } else
                    $msg = localize("{target} a utilisé Rayon de lumière noire ", array(
                            "target" => $param["PLAYER_NAME"]
                    )) . "<br/><br/>";
                
                $msg .= localize("Le Jet de maitrise de la magie de votre adversaire a été de");
                $msg .= "........................: " . $param["PLAYER_ATTACK"] . "<br/>";
                $msg .= localize("Votre Jet de maitrise de la magie a été de ");
                $msg .= "........................: " . $param["TARGET_DEFENSE"] . "<br/>";
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
                    $msg .= localize("Votre adversaire a réalisé un sort de niveau {level}", array(
                            "level" => $param["SPELL_LEVEL"],
                            "nb" => ceil($param["SPELL_LEVEL"] * 1.5)
                    )) . "<br/>";
                    $msg .= localize("Il vous a infligé {damage} points de dégât.", array(
                            "damage" => $param["TARGET_DAMAGE"]
                    )) . "<br/>";
                    $msg .= self::msgPassiveBubble($param, 0);
                    $msg .= localize("Votre armure vous a protègé et vous n'avez perdu que <b>{hp} point(s) de vie</b>.", array(
                            "hp" => $param["TARGET_HP"]
                    )) . "<br/>";
                    if ($param["TARGET_KILLED"] == 1) {
                        $msg .= "<br/>";
                        $msg .= localize("Vous avez été <b>TUÉ(E)</b>.");
                        $msg .= "<br/>";
                        $msg .= self::msgInfoDeath($param);
                    }
                } else {
                    $msg .= localize("Vous avez contré le sort de votre adversaire.") . "<br/><br/>";
                }
            }
            
            return $msg;
        }
    }

    function msgInfest ($error, &$param)
    {
        if (isset($param["PLAYER_GENDER"])) {
            if ($param["PLAYER_GENDER"] == "M")
                $art = localize("un");
            else
                $art = localize("une");
        }
        
        $msg = localize("Vous avez été attaqué par {target} ({id}) (Niveau : {level}) ", array(
                "target" => $art . " " . localize($param["PLAYER_NAME"]),
                "level" => $param["PLAYER_LEVEL"],
                "id" => $param["PLAYER_ID"]
        )) . " <br/><br/>";
        $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
        $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
        $msg .= localize("Votre Jet de défense a été de ");
        $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
        
        if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
            if (isset($param["LEVEL_BENEDICTION"])) {
                $msg .= localize("Vous étiez protégé par une bénédiction de niveau " . $param["LEVEL_BENEDICTION"]) . "<br/>";
                if ($param["CANCEL"] == 1)
                    $msg .= localize("Votre protection était plus puissante que son attaque et en a annulé les effets") . "<br/>";
                else
                    $msg .= localize("Les effets de son attaque sont réduit d'autant", array(
                            "nb" => $param["VALUE_BM"],
                            "nb2" => $param["VALUE_BM_FINAL"]
                    )) . "<br/>";
            }
            $msg .= localize("Vous avez été infesté par {target} ", array(
                    "target" => $art . " " . localize($param["PLAYER_NAME"])
            )) . " <br/><br/>";
            
            return $msg;
        }
    }

    function msgGrapnel ($error, &$param, $src = 1)
    {
        if (! $error) {
            $msg = localize("{target} a utilisé Grapin de Branches sur vous.", array(
                    "target" => $param["PLAYER_NAME"]
            )) . "<br/>";
            $msg .= localize("Son Jet d'attaque a été de ");
            $msg .= "......................... : " . $param["PLAYER_ATTACK"] . " <br/>";
            
            $msg .= localize("Votre Jet de défense a été de : ");
            $msg .= "............................ : " . $param["TARGET_DEFENSE"] . " <br/><br/>";
            if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                if ($param["MOVE"])
                    $msg .= localize("Votre adversaire est parvenu à vous déplacer en X={x} Y={y}", array(
                            "x" => $param["X"],
                            "y" => $param["Y"]
                    ));
                else
                    $msg .= localize("Il n'est pas  parvenu à vous déplacer à cause d'un obstacle.", array(
                            "x" => $param["X"],
                            "y" => $param["Y"]
                    ));
            } else
                $msg .= localize("Vous avez contré l'attaque de votre adversaire et il n'est pas parvenu à vous déplacer");
            
            $msg .= "<br/><br/>";
            
            return $msg;
        }
    }

    function msgLarceny ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error)
            $msg = localize("Vous utilisez Larçin sur {name}", array(
                    "name" => $param["TARGET_NAME"]
            )) . "<br/>";
        $msg .= localize("Votre Jet de maitrise est de ");
        $msg .= localize("............: " . $param["PLAYER_ATTACK"]) . "<br/>";
        $msg .= localize("Le Jet de défense de votre adversaire est de ");
        $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
        
        if (floor($param["PLAYER_ATTACK"]) >= $param["TARGET_DEFENSE"] + 1)
            $msg .= localize("Vous réussissez à toucher votre adversaire. S'il meurt dans le tour en cours, il perdra un objet de son inventaire.") . "<br/>";
        else
            $msg .= localize("Vous ne parvenez pas à cibler votre adversaire.") . "<br/>";
        return "<br/>" . $msg . "<br/>&nbsp;";
    }

    function msgBranch ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("{target} a utilisé Immobilise-Sarment sur vous", array(
                    "target" => $param["PLAYER_NAME"],
                    "niv" => $param["BOLAS_LEVEL"]
            )) . " <br/><br/>";
            
            $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
            $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
            $msg .= localize("Votre Jet de défense a été de ");
            $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
            
            if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"])
                $msg .= localize("Votre adversaire a réussi à vous immobilisé au sol.") . "<br/>";
            else
                $msg .= localize("Vous avez réussi à éviter l'attaque.") . "<br/>";
            
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgSpit ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("{target} a craché son venin sur vous", array(
                    "target" => $param["PLAYER_NAME"]
            )) . " <br/><br/>";
            
            $msg .= localize("Le Jet d'Attaque de votre adversaire a été de ");
            $msg .= "............: " . $param["PLAYER_ATTACK"] . " <br/>";
            $msg .= localize("Votre Jet de défense a été de ");
            $msg .= "............: " . $param["TARGET_DEFENSE"] . " <br/>";
            
            if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                $msg .= localize("Votre adversaire a réussi à vous toucher.") . "<br/><br/>";
                if (isset($param["LEVEL_BENEDICTION"])) {
                    $msg .= localize("Vous étiez protégé par une bénédiction de niveau " . $param["LEVEL_BENEDICTION"]) . "<br/>";
                    if ($param["CANCEL"] == 1)
                        $msg .= localize("Votre protection était plus puissante que son venin et en a annulé les effets") . "<br/>";
                    else
                        $msg .= localize("Les effets de son venin sont réduit de {nb} à {nb2} points de vie pour les 2 prochaines dla", array(
                                "nb" => $param["VALUE_BM"],
                                "nb2" => $param["VALUE_BM_FINAL"]
                        )) . "<br/>";
                } else
                    $msg .= localize("Son venin vous infligera " . $param["VALUE_BM"] . " points de vie pour les 2 prochaines dla.") . "<br/>";
                $msg .= "<br/>" . localize("Si vous étiez déjà empoisonné, ce venin s'ajoute au précédent et en augmente d'autant la puissance et la durée.") . "<br/>";
            } else
                $msg .= localize("Vous avez réussi à éviter l'attaque.") . "<br/>";
            
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgEvilBlow ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("{target} a utilisé cyclone démoniaque sur vous.", array(
                    "target" => $param["PLAYER_NAME"]
            )) . " <br/>";
            $msg .= localize("Vous avez été projeté en (x=" . $param["X"] . ";y=" . $param["Y"] . ").") . "<br/>";
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgMorph ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Le doppelganger a pris votre apparence. Il a exactement les mêmes caractéristiques de base que vous mais pas votre équipement.") . " <br/>";
            
            return "<br/>" . $msg . "<br/>&nbsp;";
        }
    }

    function msgPassiveVoid ($error, &$param, $src = 1)
    {
        $msg = "";
        if (! $error) {
            $msg = localize("Vous avez subi le souffle de néant crée par {target}.", array(
                    "target" => $param["PLAYER_NAME"]
            )) . " <br/>";
            for ($i = 0; $i < count($param["BM_NAME"]); $i ++) {
                // if($param["BM_NAME"][$i] == "Aura de résistance")
                // $msg.=localize("-Votre aura de résistance a été désactivée")."<br/>";
                // elseif($param["BM_NAME"][$i] == "Aura de courage")
                // $msg.=localize("-Votre aura de résistance a été désactivée")."<br/>";
                // else
                $msg .= localize("-" . $param["BM_NAME"][$i] . " a été supprimé") . "<br/>";
            }
        }
        return "<br/>" . $msg . "<br/>&nbsp;";
    }
}
?>