<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class HActionMsg extends DBObject
{

    function HActionMsg()
    {
        parent::DBObject();
        $this->m_tableName = "HActionMsg";
        $this->m_className = "HActionMsg";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player\$src"] = "";
        $this->m_attr["id_Player\$dest"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>