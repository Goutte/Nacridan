<?php

/**
 *  Define the Mission class
 *
 * 
 *
 *@author Nacridan
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Mission extends DBObject
{

    public $infos;

    public $conds;

    public $actions;

    public $objs;

    public $objsClass;

    function Mission()
    {
        parent::DBObject();
        $this->m_tableName = "Mission";
        $this->m_className = "Mission";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_BasicMission"] = "";
        $this->m_attr["id_EscortMissionRolePlay"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["PlayerList"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["content"] = "";
        $this->m_attr["Mission_level"] = "";
        $this->m_attr["RP_Mission"] = "";
        $this->m_attr["In_List"] = "";
        $this->m_attr["Player_levelMax"] = "";
        $this->m_attr["Player_levelMin"] = "";
        $this->m_attr["Is_Standard"] = "";
        $this->m_attr["id_Quest"] = "";
        $this->m_attr["id_NextMission"] = "";
        $this->m_attr["Mission_Active"] = "";
        $this->m_attr["NumMission"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }

    public function addCond($x, $y, $cond)
    {
        $this->infos[$x . ":" . $y] = 1;
        $this->conds[$x . ":" . $y] = array(
            "c" => $cond
        );
    }

    public function addAction($x, $y, $type, $value)
    {
        $this->actions[$x . ":" . $y][] = array(
            "t" => $type,
            "v" => $value
        );
    }

    public function addObject($name, $value, &$db)
    {
        if (is_object($value)) {
            $this->objsClass[$name] = get_class($value);
        } else {
            $this->objsClass[$name] = null;
        }
        $value->set("hidden", 1);
        $value->set("incity", 1);
        $id = $value->addDBr($db);
        $this->objs[$name] = $id;
    }

    private function winMoney(&$playerSrc, $value)
    {
        $playerSrc->set("money", $playerSrc->get("money") + $value);
    }

    private function winXP(&$playerSrc, $value)
    {
        $playerSrc->set("xp", $playerSrc->get("xp") + $value);
    }

    private function winEquipments(&$playerSrc, $equipNames, $mid, &$db)
    {
        if (! is_array($equipNames))
            $equipNames = array(
                $equipNames
            );
        
        foreach ($equipNames as $equipName) {
            // , id_Player=".$playerSrc->get("id")."
            $dbp = new DBCollection("UPDATE Equipment SET state='Ground',map=" . $playerSrc->get("map") . ",x=" . $playerSrc->get("x") . ",y=" . $playerSrc->get("y") . ",hidden=0,incity=0,id_Mission=" . $mid . " WHERE id=" . $this->objs[$equipName], $db, 0, 0, false);
        }
    }

    function winPlayers(&$playerSrc, $playerNames, $mid, &$db)
    {
        if (! is_array($playerNames))
            $playerNames = array(
                $playerNames
            );
        
        foreach ($playerNames as $playerName) {
            $dbp = new DBCollection("UPDATE Player SET hidden=0,incity=0,state2=1,id_Member=" . $playerSrc->get("id_Member") . ",id_Player\$target2=" . $playerSrc->get("id") . ",id_Mission=" . $mid . ",creation=\"" . gmdate("Y-m-d H:i:00") . "\" WHERE id=" . $this->objs[$playerName], $db, 0, 0, false);
        }
    }

    function free($idMember, $objNames, $extra, &$db)
    {
        if (! is_array($objNames))
            $objNames = array(
                $objNames
            );
        
        foreach ($objNames as $objName) {
            $dbp = new DBCollection("UPDATE Player SET id_Member=" . $idMember . ",id_Mission=0" . $extra . " WHERE id=" . $this->objs[$objName], $db, 0, 0, false);
        }
    }

    function delPlayers(&$playerSrc, $playerNames, &$db)
    {
        if (! is_array($playerNames))
            $playerNames = array(
                $playerNames
            );
        
        foreach ($playerNames as $playerName) {
            $player = new Player();
            $player->load($this->objs[$playerName], $db);
            $dbt = new DBCollection("DELETE FROM Equipment WHERE id_Player=" . $player->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Modifier WHERE id=" . $player->get("id_Modifier"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Upgrade WHERE id=" . $player->get("id_Upgrade"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Player WHERE id=" . $player->get("id"), $db, 0, 0, false);
        }
    }

    public function solve(&$p1, $mid, $eqid, &$param, &$db)
    {
        $x = $p1->get("x");
        $y = $p1->get("y");
        if (isset($this->conds[$x . ":" . $y])) {
            if (is_array($this->objs)) {
                foreach ($this->objs as $objname => $id) {
                    if (strstr($this->conds[$x . ":" . $y]["c"], "\$" . $objname)) {
                        $classname = $this->objsClass[$objname];
                        $$objname = new $classname();
                        $$objname->load($id, $db);
                    }
                }
            }
            
            eval($this->conds[$x . ":" . $y]["c"]);
            
            if ($success == "true") {
                foreach ($this->actions[$x . ":" . $y] as $action) {
                    switch ($action["t"]) {
                        case WIN_MONEY:
                            $this->winMoney($p1, $action["v"], $db);
                            break;
                        case WIN_XP:
                            $this->winXP($p1, $action["v"], $db);
                            break;
                        case WIN_EQUIP:
                            $this->winEquipments($p1, $action["v"], 0, $db);
                            break;
                        case ADD_PLAYER:
                            $this->winPlayers($p1, $action["v"], $mid, $db);
                            break;
                        case DEL_PLAYER:
                            $this->delPlayers($p1, $action["v"], $db);
                            break;
                        case WIN_PLAYER:
                            $this->free($p1->get("id_Member"), $action["v"], ",state2=0,id_Mission=0,id_Player\$target2=0", $db);
                            break;
                        case FREE_PLAYER:
                            $this->free(0, $action["v"], ",state2=0,id_Player\$target2=0", $db);
                            break;
                        default:
                            break;
                    }
                }
                $p1->updateDB($db);
                unset($this->conds[$x . ":" . $y]);
                $this->set("id", $mid);
                $this->set("content", serialize($this));
                if (! empty($this->conds)) {
                    $this->updateDB($db);
                } else {
                    $dbd = new DBCollection("DELETE FROM Equipment WHERE id=" . $eqid, $db, 0, 0, false);
                    $dbd = new DBCollection("DELETE FROM Mission WHERE id=" . $mid, $db, 0, 0, false);
                    return "end";
                }
                
                return "true";
            }
            if ($success == "null") {
                $dbd = new DBCollection("DELETE FROM Equipment WHERE id=" . $eqid, $db, 0, 0, false);
                $dbd = new DBCollection("DELETE FROM Mission WHERE id=" . $mid, $db, 0, 0, false);
                return "null";
            }
            return "false";
        } else {
            return "false";
        }
    }

    public function ReceiveRewardStop(&$p1, $mid, &$db)
    {
        $reward = new MissionReward();
        $dbr = new DBCollection("SELECT " . $reward->getASRenamer("MissionReward", "R") . " FROM MissionReward WHERE idMission=" . $mid, $db, 0, 0);
        $reward->DBLoad($dbr, 'R');
        
        if ($reward->get("moneyStop") != 0) {
            $p1->set("money", $p1->get("money") + $reward->get("moneyStop"));
            $p1->updateDB($db);
        }
        
        if ($reward->get("xpStop") != 0) {
            $p1->set("xp", $p1->get("xp") + $reward->get("xpStop"));
            $p1->updateDB($db);
        }
        
        if ($reward->get("idEquipStop") != 0) {
            $dbu = new DBCollection("UPDATE Equipment SET state='Ground',map=" . $p1->get("map") . ",x=" . $p1->get("x") . ",y=" . $p1->get("y") . " WHERE id=" . $reward->get("idEquipStop"), $db, 0, 0, false);
        }
        
        if ($reward->get("idEscort") != 0) {
            $escort = new Player();
            $dbp = new DBCollection("SELECT " . $escort->getASRenamer("Player", "P") . " FROM Player WHERE id=" . $reward->get("idEscort"), $db, 0, 0);
            $escort->DBLoad($dbp, 'P');
            
            $dbt = new DBCollection("DELETE FROM Equipment WHERE id_Player=" . $escort->get("id"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Modifier WHERE id=" . $escort->get("id_Modifier"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Upgrade WHERE id=" . $escort->get("id_Upgrade"), $db, 0, 0, false);
            $dbt = new DBCollection("DELETE FROM Player WHERE id=" . $escort->get("id"), $db, 0, 0, false);
        }
        
        if ($reward->get("idEquipStart") != 0) {
            $dbt = new DBCollection("DELETE FROM Equipment WHERE id=" . $reward->get("idEquipStart"), $db, 0, 0, false);
        }
        
        $mission = new Mission();
        $dbm = new DBCollection("SELECT " . $mission->getASRenamer("Mission", "M") . " FROM Mission WHERE id=" . $mid, $db, 0, 0);
        $mission->DBLoad($dbm, 'M');
        if ($mission->get("id_NextMission") != 0) // si ce n'est pas la fin de la quête faut choper la mission suivante
{
            $dbu = new DBCollection("UPDATE Equipment SET state='Ground',map=" . $p1->get("map") . ",x=" . $p1->get("x") . ",y=" . $p1->get("y") . " WHERE id_Mission=" . $mission->get("id_NextMission"), $db, 0, 0, false);
            
            $condition = new MissionCondition();
            $dbc = new DBCollection("SELECT " . $condition->getASRenamer("MissionCondition", "C") . " FROM MissionCondition WHERE idMission=" . $mission->get("id_NextMission"), $db, 0, 0);
            $condition->DBLoad($dbc, 'C');
            
            require_once (HOMEPATH . "/lib/MapInfo.inc.php"); // Pour vérifier qu'on arrive pas trop à l'ouest
            $Player_x = $p1->get("x");
            $Player_y = $p1->get("y");
            $map = $p1->get("map");
            $mapinfo = new MapInfo($db);
            
            $validzone = $mapinfo->getValidMap($Player_x + $condition->get("distanceX"), $Player_y + $condition->get("distanceY"), 0, $map);
            
            if (! $validzone[0][0]) {
                $r = sqrt(pow($condition->get("distanceX"), 2) + pow($condition->get("distanceY"), 2));
                $notvalid = true;
                while ($notvalid) {
                    $theta = rand(0, 628) / 100;
                    $xtest = $Player_x + floor($r * cos($theta));
                    $ytest = $Player_y + floor($r * sin($theta));
                    $vz = $mapinfo->getValidMap($xtest, $ytest, 0, $map);
                    if ($vz[0][0]) {
                        $notvalid = false;
                        $condition->set("distanceX", floor($r * cos($theta)));
                        $condition->set("distanceY", floor($r * sin($theta)));
                        $condition->updateDB($db);
                    }
                }
            }
            
            if ($condition->get("positionXstop") == 0) // Pour se laisser le choix de le définir en amont
{
                
                $condition->set("positionXstop", $p1->get("x") + $condition->get("distanceX")); // il faudra faire un test pour le type de terrain
                $condition->set("positionYstop", $p1->get("y") + $condition->get("distanceY"));
                $condition->updateDB($db);
            }
            if (($condition->get("idEscort") != 0) && ($condition->get("positionXstart") == 0)) {
                $condition->set("positionXstart", $p1->get("x"));
                $condition->set("positionYstart", $p1->get("y"));
                $condition->set("positionXstop", 0);
                $condition->set("positionYstop", 0);
                $condition->set("EscortPositionX", $p1->get("x") + $condition->get("distanceX"));
                $condition->set("EscortPositionY", $p1->get("y") + $condition->get("distanceY"));
                $condition->updateDB($db);
            }
            if (($condition->get("idEquip") != 0) && ($condition->get("positionXstart") == 0)) {
                $condition->set("positionXstart", $p1->get("x"));
                $condition->set("positionYstart", $p1->get("y"));
                $condition->set("positionXstop", 0);
                $condition->set("positionYstop", 0);
                $condition->set("EquipPositionX", $p1->get("x") + $condition->get("distanceX"));
                $condition->set("EquipPositionY", $p1->get("y") + $condition->get("distanceY"));
                $condition->updateDB($db);
            }
        } else
            $dbdq = new DBCollection("DELETE FROM Quest WHERE id=" . $mission->get("id_Quest"), $db, 0, 0, false);
        $dbd1 = new DBCollection("DELETE FROM Equipment WHERE name='Parchemin' AND id_Mission= $mid", $db, 0, 0, false);
        $dbd2 = new DBCollection("DELETE FROM Mission WHERE id= $mid", $db, 0, 0, false);
        $dbd3 = new DBCollection("DELETE FROM MissionCondition WHERE idMission= $mid", $db, 0, 0, false);
        $dbd4 = new DBCollection("DELETE FROM MissionReward WHERE idMission= $mid", $db, 0, 0, false);
        
        return true;
    }

    public function ReceiveRewardStart(&$p1, $mid, &$db)
    {
        $reward = new MissionReward();
        $dbr = new DBCollection("SELECT " . $reward->getASRenamer("MissionReward", "R") . " FROM MissionReward WHERE idMission=" . $mid, $db, 0, 0);
        $reward->DBLoad($dbr, 'R');
        
        if ($reward->get("moneyStart") != 0) {
            $p1->set("money", $p1->get("money") + $reward->get("moneyStart"));
            $p1->updateDB($db);
        }
        
        if ($reward->get("xpStart") != 0) {
            $p1->set("xp", $p1->get("xp") + $reward->get("xpStart"));
            $p1->updateDB($db);
        }
        
        if ($reward->get("idEquipStart") != 0) {
            $dbu = new DBCollection("UPDATE Equipment SET state='Ground',map=" . $p1->get("map") . ",x=" . $p1->get("x") . ",y=" . $p1->get("y") . " WHERE id=" . $reward->get("idEquipStart"), $db, 0, 0, false);
        }
        
        if ($reward->get("idEscort") != 0) {
            $dbu = new DBCollection("UPDATE Player SET hidden=0,incity=0,state2=1,map=" . $p1->get("map") . ",x=" . $p1->get("x") . ",y=" . $p1->get("y") . ", id_Member=" . $p1->get("id_Member") . ", id_Player\$target2=" . $p1->get("id") . " WHERE id=" . $reward->get("idEscort"), $db, 0, 0, false);
        }
        
        return true;
    }

    /**
     * *Cette fonction gère tous les types de missions qui viennent de la liste.
     * Elle test les conditions d'accomplissement et redirige sur les fonctions qui gèrent les récompensent si besoin.
     */
    public function solveFromListMission(&$p1, $mid, $param, &$db)
    {
        $x = $p1->get("x");
        $y = $p1->get("y");
        $map = $p1->get("map");
        
        $condition = new MissionCondition();
        $dbc = new DBCollection("SELECT " . $condition->getASRenamer("MissionCondition", "C") . " FROM MissionCondition WHERE idMission=" . $mid, $db, 0, 0);
        $condition->DBLoad($dbc, 'C');
        
        $reward = new MissionReward();
        $dbr = new DBCollection("SELECT " . $reward->getASRenamer("MissionReward", "R") . " FROM MissionReward WHERE idMission=" . $mid, $db, 0, 0);
        $reward->DBLoad($dbr, 'R');
        
        if ((($condition->get("EnigmAnswer")) != '') && ($x == $condition->get("positionXstop")) && (($y == $condition->get("positionYstop")))) {
            
            if ($param["ANSWER"] == $condition->get("EnigmAnswer")) {
                $this->ReceiveRewardStop($p1, $mid, $db);
                return "end";
            } elseif ($param["ANSWER"] == '')
                return "getanswer";
            else
                return "wronganswer";
        }
        
        if (($condition->get("idPNJtoKill")) != 0) {
            if (($x == $condition->get("positionXstop")) && (($y == $condition->get("positionYstop")))) {
                
                $dbi = new DBCollection("SELECT id FROM Player WHERE id_Mission=" . $mid . " OR id_Mission=-$mid", $db, 0, 0);
                
                if (! $dbi->eof()) {
                    while (! $dbi->eof()) {
                        $idPNJ = $dbi->get("id");
                        $cible = new Player();
                        $dbp = new DBCollection("SELECT " . $cible->getASRenamer("Player", "P") . " FROM Player WHERE id=" . $idPNJ, $db, 0, 0);
                        
                        $cible->DBLoad($dbp, 'P');
                        $cible->set("hidden", 0);
                        $cible->set("x", $x);
                        $cible->set("y", $y);
                        $cible->set("map", $map);
                        $cible->set("id_Mission", - $mid);
                        $cible->updateDB($db);
                        $dbi->next();
                    }
                    return "true";
                } else {
                    $this->ReceiveRewardStop($p1, $mid, $db);
                    return "end";
                }
            } else {
                return "false";
            }
        }
        
        if (($condition->get("positionXstart")) != 0) {
            if (($x == $condition->get("positionXstart")) && (($y == $condition->get("positionYstart")))) {
                $this->ReceiveRewardStart($p1, $mid, $db);
                return "true";
            }
        }
        
        if (($condition->get("positionXstop")) != 0) {
            if (($x == $condition->get("positionXstop")) && (($y == $condition->get("positionYstop")))) {
                $this->ReceiveRewardStop($p1, $mid, $db);
                return "end";
            }
        }
        
        if (($condition->get("EquipPositionX")) != 0) {
            $equiptocarry = new Equipment();
            $dbe = new DBCollection("SELECT " . $equiptocarry->getASRenamer("Equipment", "E") . " FROM Equipment WHERE id=" . $reward->get("idEquipStart"), $db, 0, 0);
            $equiptocarry->DBLoad($dbe, 'E');
            
            if (($x == $equiptocarry->get("x")) && (($y == $equiptocarry->get("y")))) {
                $this->ReceiveRewardStop($p1, $mid, $db);
                return "end";
            }
        }
        
        if (($condition->get("EscortPositionX")) != 0) {
            $escort = new Player();
            $dbp = new DBCollection("SELECT " . $escort->getASRenamer("Player", "P") . " FROM Player WHERE id=" . $reward->get("idEscort"), $db, 0, 0);
            $escort->DBLoad($dbp, 'P');
            
            if (($x == $escort->get("x")) && (($y == $escort->get("y")))) {
                $this->ReceiveRewardStop($p1, $mid, $db);
                return "end";
            }
        }
        
        return "false";
    }
}

?>
