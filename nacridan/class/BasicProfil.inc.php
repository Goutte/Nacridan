<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicProfil extends DBObject
{

    function BasicProfil()
    {
        parent::DBObject();
        $this->m_tableName = "BasicProfil";
        $this->m_className = "BasicProfil";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["hp"] = "";
        $this->m_attr["strength"] = "";
        $this->m_attr["dexterity"] = "";
        $this->m_attr["speed"] = "";
        $this->m_attr["magicSkill"] = "";
        $this->m_attr["armor"] = "";
        $this->m_attr["attack_bm"] = "";
        $this->m_attr["defense_bm"] = "";
        $this->m_attr["damage_bm"] = "";
        $this->m_attr["magicSkill_bm"] = "";
        $this->m_attr["fightStyle"] = "";
        $this->init();
    }
}

?>