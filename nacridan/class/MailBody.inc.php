<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MailBody extends DBObject
{

    function MailBody()
    {
        parent::DBObject();
        $this->m_tableName = "MailBody";
        $this->m_className = "MailBody";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["recipientid"] = "";
        $this->m_attr["recipient"] = "";
        $this->m_attr["cpt"] = "";
        $this->m_attr["title"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>