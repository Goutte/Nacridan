<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Detail extends DBObject
{

    function Detail()
    {
        parent::DBObject();
        $this->m_tableName = "Detail";
        $this->m_className = "Detail";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["pic"] = "";
        $this->m_attr["body"] = "";
        $this->init();
    }
}

?>