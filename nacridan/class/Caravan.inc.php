<?php

/**
 *  Define the Building class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Caravan extends DBObject
{

    function Caravan()
    {
        parent::DBObject();
        $this->m_tableName = "Caravan";
        $this->m_className = "Caravan";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["finalprice"] = "";
        $this->m_attr["content"] = "";
        $this->m_attr["id_startbuilding"] = "";
        $this->m_attr["id_endbuilding"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>
