<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class FighterGroupAskJoin extends DBObject
{

    function Team()
    {
        parent::DBObject();
        $this->m_tableName = "FighterGroupAskJoin";
        $this->m_className = "FighterGroupAskJoin";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Guest"];
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>