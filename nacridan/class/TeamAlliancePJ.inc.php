<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class TeamAlliancePJ extends DBObject
{

    function TeamAlliancePJ()
    {
        parent::DBObject();
        $this->m_tableName = "TeamAlliancePJ";
        $this->m_className = "TeamAlliancePJ";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Team\$src"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["public"] = "";
        $this->m_attr["datestart"] = "";
        $this->m_attr["datestop"] = "";
        $this->init();
    }
}

?>