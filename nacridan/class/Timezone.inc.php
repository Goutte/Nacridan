<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Timezone extends DBObject
{

    function Timezone()
    {
        parent::DBObject();
        $this->m_tableName = "Timezone";
        $this->m_className = "Timezone";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["utcOffset"] = "";
        $this->m_attr["utcOffsetDLS"] = "";
        $this->init();
    }
}

?>