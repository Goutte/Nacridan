<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Team extends DBObject
{

    function Team()
    {
        parent::DBObject();
        $this->m_tableName = "Team";
        $this->m_className = "Team";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_TeamBody"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>