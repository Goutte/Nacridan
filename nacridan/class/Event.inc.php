<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");
require_once (HOMEPATH . "/class/HActionMsgHisto.inc.php");

class Event extends DBObject
{

    function Event()
    {
        parent::DBObject();
        $this->m_tableName = "Event";
        $this->m_className = "Event";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Member"] = "";
        $this->m_attr["id_Player\$src"] = "";
        $this->m_attr["id_Player\$dest"] = "";
        $this->m_attr["id_BasicRace\$src"] = "";
        $this->m_attr["id_BasicRace\$dest"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["idtype"] = "";
        $this->m_attr["typeEvent"] = "";
        $this->m_attr["typeAction"] = "";
        $this->m_attr["ip"] = "";
        $this->m_attr["ip_forward"] = "";
        $this->m_attr["active"] = "";
        $this->m_attr["auth"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }

    function getEvents()
    {
        global $_EVENT;
    }

    static function sendEmail($type, $email, $killed, $name, $body, &$res)
    {
        if (SMTPDOMAIN != "nacridan.com")
            return;
        
        require_once (HOMEPATH . "/lib/swift/Swift.php");
        require_once (HOMEPATH . "/lib/swift/Swift/Connection/SMTP.php");
        
        switch ($type) {
            
            case ATTACK:
            case PASSIVE_FIRE:
            case PASSIVE_FLYARROW:
            case PASSIVE_TWIRL:
            case PASSIVE_EXORCISM:
            case PASSIVE_PROJECTION:
            case PASSIVE_PROJECTION_BUILDING:
            case ATTACK:
            case ARCHERY:
            case ABILITY_POWERFUL:
            case ABILITY_THRUST:
            case ABILITY_DAMAGE:
            case ABILITY_TREACHEROUS:
            case ABILITY_KNOCKOUT:
            case ABILITY_STUNNED:
            case ABILITY_BOLAS:
            case ABILITY_DISARM:
            // case ABILITY_STEAL:
            case ABILITY_LIGHT:
            case ABILITY_FAR:
            case ABILITY_NEGATION:
            case ABILITY_DISABLING:
            case ABILITY_PROJECTION:
            // case ABILITY_PROTECTION :
            case ABILITY_LARCENY:
            case SPELL_BURNING:
            case SPELL_FIREBALL:
            case SPELL_BLOOD:
            case SPELL_DEMONPUNCH:
            case SPELL_CURSE:
            case SPELL_KINE:
            
            // Les morts passive
            case PASSIVE_BLOOD_DEATH:
            case PASSIVE_POISON_DEATH:
            case PASSIVE_INFEST_DEATH:
            case PASSIVE_BARRIER_DEATH:
            case PASSIVE_PROJECTION_DEATH_OBST:
            case PASSIVE_PROJECTION_DEATH_OPP:
            
            // Les actions spéciales des monstres
            case M_STUN:
            case M_INFEST:
            case M_INSTANTBLOOD:
            case M_GRAPNEL:
            case M_BRANCH:
                
                if ($killed) {
                    $subject = "[Nacridan] Évènement de type mort";
                    $message = "Votre Personnage (" . $name . ") est mort\n";
                } else {
                    $subject = "[Nacridan] Évènement de type combat";
                    $message = "Votre Personnage (" . $name . ") a été impliqué dans un combat\n";
                }
                break;
            // les soins
            case PASSIVE_RAIN:
            case SPELL_TEARS:
            case ABILITY_FIRSTAID:
            case SPELL_REGEN:
                $subject = "[Nacridan] Évènement de type soin";
                $message = "Votre Personnage (" . $name . ") a été soigné\n";
                break;
            default:
                return - 1;
                break;
        }
        
        $message .= "-------------------------------------\n";
        $message .= $body . "\n";
        $message .= "-------------------------------------\n";
        $message .= "Vous pouvez dés à present consulter les détails du combat sur " . CONFIG_ROOT . " dans la section 'Évènements'\n";
        $message .= "-------------------------------------\n";
        $message .= "Rappel : Si vous ne désirez plus recevoir cet E-Mail, allez dans la section 'Options' puis 'membre' de votre compte pour désactiver l'envoi automatique.\n";
        $message .= "-------------------------------------\n";
        
        $to = $email;
        $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
        if ($mailer->isConnected()) { // Optional
            
            if (SMTPLOGIN != "" && SMTPPASS != "") {
                require_once (HOMEPATH . "/lib/swift/Swift/Authenticator/PLAIN.php");
                $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
                $mailer->authenticate(SMTPLOGIN, SMTPPASS);
            }
            
            // Add as many parts as you need here
            $mailer->addPart($message);
            $mailer->addPart(str_replace("\n", '<br />', $message), 'text/html');
            $mailer->setCharset("UTF-8");
            $mailer->send($to, "Nacridan <noreply@" . SMTPDOMAIN . ".>", $subject);
            
            $mailer->close();
        }
    }

    static function logAction(&$db, $action, &$param)
    {
        require_once (HOMEPATH . "/class/HActionMsg.inc.php");
        if ($action != null) {
            
            if (isset($param["PLAYER_ID"])) {
                
                // Initialisation commune de l'event
                $idPlayer = $param["PLAYER_ID"];
                $idMember = $param["MEMBER_ID"];
                $racePlayer = $param["PLAYER_RACE"];
                $event = new Event();
                $event->set("id_Member", $idMember);
                $event->set("id_Player\$src", $idPlayer);
                $event->set("id_BasicRace\$src", $racePlayer);
                $event->set("typeAction", $action);
                $datetime = gmdate("Y-m-d H:i:s");
                $event->set("date", $datetime);
                if (isset($_SERVER['REMOTE_ADDR']))
                    $event->set("ip", $_SERVER['REMOTE_ADDR']);
                
                $event->set("active", 1);
                $figthMsg = null;
                $deadMsg = null;
                $targetEmail = null;
                if (isset($param["TARGET_ID"]) && ! empty($param["TARGET_ID"])) {
                    $dbs = new DBCollection(
                        "SELECT fightMsg,deadMsg,email FROM Player LEFT JOIN Member ON  Member.id=Player.id_Member LEFT JOIN MemberOption ON Member.id=MemberOption.id_Member LEFT JOIN MemberDetail ON Member.id=MemberDetail.id_Member WHERE Player.id=" .
                             $param["TARGET_ID"], $db);
                    if (! $dbs->eof()) {
                        $deadMsg = $dbs->get("deadMsg");
                        $figthMsg = $dbs->get("fightMsg");
                        $targetEmail = $dbs->get("email");
                    }
                }
                
                switch ($action) {
                    
                    /* ************************************************************* LOG DES ACTIONS AVEC TARGET AVEC HISTORIQUE **************************** */
                    // Les actions passives
                    case PASSIVE_FIRE:
                    case PASSIVE_MASSIVE_TELEPORTATION:
                    case PASSIVE_RAIN:
                    case PASSIVE_FLYARROW:
                    case PASSIVE_TWIRL:
                    case PASSIVE_EXORCISM:
                    case PASSIVE_SUN:
                    case PASSIVE_BIER:
                    case PASSIVE_LIGHTNING:
                    case PASSIVE_VOID:
                        $event->set("auth", 1);
                    
                    case PASSIVE_PROJECTION:
                    
                    case GIVE_OBJECT:
                    case GIVE_MONEY:
                    case BUY_PJ:
                    
                    case HUSTLE:
                    case ATTACK:
                    case ARCHERY:
                    
                    case ABILITY_STEAL:
                        if (isset($param["AUTH"]) && $param["AUTH"] == 2) {
                            $event2 = new Event();
                            $idTarget2 = $param["TARGET_ID"];
                            $raceTarget2 = $param["TARGET_RACE"];
                            $event2->set("id_Member", $idMember);
                            $event2->set("id_Player\$src", $idTarget2);
                            $event2->set("id_BasicRace\$src", $raceTarget2);
                            $event2->set("typeAction", $action);
                            $datetime2 = gmdate("Y-m-d H:i:s", time() + date("I") + 2);
                            $event2->set("date", $datetime2);
                            if (isset($_SERVER['REMOTE_ADDR']))
                                $event2->set("ip", $_SERVER['REMOTE_ADDR']);
                            $event2->set("active", 1);
                            $event2->set("typeEvent", ABILITY_STEAL_EVENT_SUCCESS_TARGET);
                            $event2->addDB($db);
                        }
                    case ABILITY_POWERFUL:
                    case ABILITY_THRUST:
                    case ABILITY_FIRSTAID:
                    case ABILITY_DAMAGE:
                    case ABILITY_TREACHEROUS:
                    case ABILITY_KNOCKOUT:
                    case ABILITY_STUNNED:
                    case ABILITY_BOLAS:
                    case ABILITY_DISARM:
                    case ABILITY_LIGHT:
                    case ABILITY_FAR:
                    case ABILITY_NEGATION:
                    case ABILITY_DISABLING:
                    case ABILITY_PROJECTION:
                    case ABILITY_PROTECTION:
                    case ABILITY_LARCENY:
                    
                    case SPELL_BURNING:
                    case SPELL_ARMOR:
                    case SPELL_CURE:
                    case SPELL_BUBBLE:
                    case SPELL_BARRIER:
                    case SPELL_REGEN:
                    case SPELL_BLESS:
                    case SPELL_RECALL:
                    case SPELL_FIREBALL:
                    case SPELL_BLOOD:
                    case SPELL_LIGHTTOUCH:
                    case SPELL_DEMONPUNCH:
                    case SPELL_BLESS:
                    case SPELL_CURSE:
                    case SPELL_ANGER:
                    case SPELL_SHIELD:
                    case SPELL_NEGATION:
                    case SPELL_KINE:
                    case SPELL_FIRECALL:
                    case SPELL_FIRECALL_CAPTURE:
                    // les soins
                    case PASSIVE_RAIN:
                    case SPELL_TEARS:
                    // Les morts passive
                    case PASSIVE_BLOOD_DEATH:
                    case PASSIVE_POISON_DEATH:
                    case PASSIVE_INFEST_DEATH:
                    case PASSIVE_BARRIER_DEATH:
                    case PASSIVE_WOUND_DEATH:
                    case PASSIVE_PROJECTION_DEATH_OBST:
                    case PASSIVE_PROJECTION_DEATH_OPP:
                    
                    // Les actions spéciales des monstres
                    case M_STUN:
                    case M_INFEST:
                    case M_INSTANTBLOOD:
                    case M_GRAPNEL:
                    case M_BRANCH:
                    case M_MORPH:
                    case M_SPIT:
                    case M_EVIL_BLOW:
                    
                    case BANK_TRANSFERT_PLAYER:
                    
                    // Les talents
                    case TALENT_MONSTER:
                    
                    // les bâtiments
                    case ACT_PUTSCH:
                    case CREATE_PUTSCH:
                        
                        // Modification du datetime pour avoir le bon ordre dans les log enchainé
                        if (isset($param["DELAI"])) {
                            if ($param["DELAI"] == 1)
                                $datetime = gmdate("Y-m-d H:i:s", time() + date("I") - 1);
                            elseif ($param["DELAI"] == 2)
                                $datetime = gmdate("Y-m-d H:i:s", time() + date("I") + 2);
                            elseif ($param["DELAI"] == 3)
                                $datetime = gmdate("Y-m-d H:i:s", time() + date("I") + 3);
                            $event->set("date", $datetime);
                        }
                        
                        // Modification du auth pour les actions secrètes
                        if (isset($param["AUTH"]) && $param["AUTH"] == 2)
                            $event->set("auth", 2);
                            
                            // Envoie de mail selon la config du joueur.
                        $idTarget = $param["TARGET_ID"];
                        $raceTarget = $param["TARGET_RACE"];
                        $killed = $param["TARGET_KILLED"];
                        $serial = serialize($param);
                        
                        // dbLogMessage("message serial->" . $serial);
                        
                        $class = "HActionMsgHisto";
                        $obj = new HActionMsgHisto();
                        $error = $param["ERROR"];
                        $msgBody = $obj->msgBuilder($error, $param, 0);
                        
                        if ($killed == 1 && $deadMsg == 1)
                            self::sendEmail($action, $targetEmail, $killed, $param["TARGET_NAME"], $msgBody, $res);
                        
                        if ($killed != 1 && $figthMsg == 1)
                            self::sendEmail($action, $targetEmail, $killed, $param["TARGET_NAME"], $msgBody, $res);
                            
                            // Initialisation de l'event et du log
                        $event->set("id_Player\$dest", $idTarget);
                        $event->set("id_BasicRace\$dest", $raceTarget);
                        
                        $fightmsg = new HActionMsg();
                        $fightmsg->set("body", $serial);
                        $fightmsg->set("date", $datetime);
                        $fightmsg->set("id_Player\$src", $idPlayer);
                        $fightmsg->set("id_Player\$dest", $idTarget);
                        $idfight = $fightmsg->addDB($db);
                        $event->set("type", "HActionMsg");
                        $event->set("idtype", $idfight);
                        $event->set("typeEvent", $param["TYPE_ATTACK"]);
                        $event->addDB($db);
                        break;
                    
                    /* ************************************************************* LOG DES ACTIONS AVEC TARGET SANS HISTORIQUE **************************** */
                    case RECALL:
                    case M_HEAL:
                    case M_DRAIN:
                    case SPELL_FIRECALL_PASSIVE_RELEASE:
                    case SPELL_FEU_FOL_DEATH:
                    case SPELL_FEU_FOL_DEATH_MM:
                        $event2 = new Event();
                        $event2->set("id_Member", $idMember);
                        $event2->set("id_Player\$src", $idPlayer);
                        $event2->set("id_BasicRace\$src", $racePlayer);
                        $event2->set("typeAction", $action);
                        $datetime = gmdate("Y-m-d H:i:s");
                        $event2->set("date", $datetime);
                        if (isset($_SERVER['REMOTE_ADDR']))
                            $event2->set("ip", $_SERVER['REMOTE_ADDR']);
                        
                        $event2->set("active", 1);
                        $idTarget = $param["TARGET_ID"];
                        $raceTarget = $param["TARGET_RACE"];
                        $event2->set("id_Player\$dest", $idTarget);
                        $event2->set("id_BasicRace\$dest", $raceTarget);
                        $event2->set("typeEvent", $param["TYPE_ATTACK"]);
                        $event2->addDB($db);
                        break;
                    
                    /* ****************************************************** LOG DES ACTION SANS TARGET AVEC HISTORIQUE************************* */
                    case NORMAL_ATTACK_BUILDING:
                    case MAGICAL_ATTACK_BUILDING:
                    case PASSIVE_BRAVERY_DEATH:
                    case DRINK_SPRING:
                    case USE_POTION:
                    case USE_PARCHMENT:
                    case LEVEL_UP:
                    case GAIN_XP:
                    case BUILDING_COLLAPSE:
                    case BUILDING_COLLAPSE_DEATH:
                    case PASSIVE_PROJECTION_BUILDING:
                    
                    case M_BIER:
                    
                    case ABILITY_GUARD:
                    case ABILITY_BRAVERY:
                    case ABILITY_RESISTANCE:
                    case ABILITY_SHARPEN:
                    case ABILITY_AUTOREGEN:
                    case ABILITY_RUN:
                    case ABILITY_TWIRL:
                    case ABILITY_EXORCISM:
                    case ABILITY_FLAMING:
                    case ABILITY_FLYARROW:
                    case ABILITY_AMBUSH:
                    
                    case SPELL_WALL:
                    case SPELL_FIRE:
                    case SPELL_SPRING:
                    case SPELL_RAIN:
                    case SPELL_RELOAD:
                    case SPELL_SUN:
                    case SPELL_PILLARS:
                    case SPELL_TELEPORT:
                    case SPELL_SOUL:
                    
                    // les sorts spéciaux
                    case S_MASSIVEINSTANTTP:
                    
                    case TALENT_MINE:
                    case TALENT_DISMEMBER:
                    case TALENT_SCUTCH:
                    case TALENT_HARVEST:
                    case TALENT_SPEAK:
                    case TALENT_CUT:
                    case TALENT_IRONCRAFT:
                    case TALENT_LEATHERCRAFT:
                    case TALENT_SCALECRAFT:
                    case TALENT_LINENCRAFT:
                    case TALENT_PLANTCRAFT:
                    case TALENT_WOODCRAFT:
                    case TALENT_GEMCRAFT:
                    case TALENT_IRONREFINE:
                    case TALENT_LEATHERREFINE:
                    case TALENT_SCALEREFINE:
                    case TALENT_LINENREFINE:
                    case TALENT_PLANTREFINE:
                    case TALENT_GEMREFINE:
                    case TALENT_WOODREFINE:
                    case TALENT_KRADJECKCALL:
                    case TALENT_DETECTION:
                    
                    case BEDROOM_SLEEPING:
                    case TELEPORT:
                    case TEMPLE_HEAL:
                    case TEMPLE_BLESSING:
                    case TEMPLE_RESURRECT:
                    case TEMPLE_PALACE:
                    case BEDROOM_WITHDRAW:
                    case BEDROOM_DEPOSIT:
                    case HOSTEL_DRINK:
                    case HOSTEL_CHAT:
                    case HOSTEL_ROUND:
                    case HOSTEL_NEWS:
                    case HOSTEL_TRADE:
                    case SCHOOL_LEARN_SPELL:
                    case SCHOOL_LEARN_ABILITY:
                    case SCHOOL_LEARN_TALENT:
                    case BANK_DEPOSIT:
                    case BANK_WITHDRAW:
                    case BANK_TRANSFERT:
                    case PALACE_DEPOSIT:
                    case PALACE_WITHDRAW:
                    case BASIC_SHOP_BUY:
                    case SHOP_SELL:
                    case SHOP_REPAIR:
                    case MAIN_SHOP_BUY:
                    case GUILD_ORDER_EQUIP:
                    case GUILD_TAKE_EQUIP:
                    case GUILD_ORDER_ENCHANT:
                    case GUILD_TAKE_ENCHANT:
                    case CARAVAN_CREATE:
                    case CARAVAN_TERMINATE:
                    case COLLECT_PACKAGE:
                    case SEND_PACKAGE:
                    case RETRIEVE_PACKAGE:
                    // case TREASURY_SET_TAX:
                    case BUILDING_CONSTRUCT:
                    case BUILDING_REPAIR:
                    case BUILDING_DESTROY:
                    case NAME_VILLAGE:
                    case FORTIFY_VILLAGE:
                    case OPERATE_VILLAGE:
                    case OPERATE_VILLAGE_TEMPLE:
                    case OPERATE_VILLAGE_ENTREPOT:
                    case PALACE_DECISION:
                    case PALACE_DECISION:
                    case CONTROL_TEMPLE:
                    case UPGRADE_FORTIFICATION:
                    case TERRASSEMENT:
                    case TERRAFORMATION:
                    case EXCHANGE_OBJECT:
                    case SHOP_REPAIR_EX:
                    case EXCHANGE_GET_OBJECT:
                    case PALACE_TRANSFER_MONEY:
                        
                        $serial = serialize($param);
                        $fightmsg = new HActionMsg();
                        $fightmsg->set("body", $serial);
                        $fightmsg->set("date", $datetime);
                        $fightmsg->set("id_Player\$src", $idPlayer);
                        $idfight = $fightmsg->addDB($db);
                        $event->set("typeEvent", $param["TYPE_ATTACK"]);
                        $event->set("type", "HActionMsg");
                        $event->set("idtype", $idfight);
                        $event->addDB($db);
                        break;
                    
                    /* ****************************************************** LOG DES ACTION SANS TARGET SANS HISTORIQUE************************* */
                    case MOVE:
                    case DROP_OBJECT:
                    case PICK_UP_OBJECT:
                    case EXIT_JAIL:
                    case RP_ACTION:
                    case GRAFFITI:
                    case CONTINUE_GRAFFITI:
                    
                    case DESTROY:
                    case RIDE_TURTLE:
                    case TURTLE_UNMOUNT:
                    case ARREST:
                    case FREE:
                    
                    case REVOKE_GOLEM:
                    case DISABLED_BRAVERY:
                    case DISABLED_RESISTANCE:
                    case TALENT_CLOSEGATE:
                    
                    case M_KOBOLD_CRAP:
                    case M_JUMP:
                    case M_TELEPORT:
                    case M_BACKWORLD:
                    case M_VOID:
                    case GUILD_RESET_GAUGE:
                        
                        if (isset($param["DELAI"])) {
                            if ($param["DELAI"] == 1)
                                $datetime = gmdate("Y-m-d H:i:s", time() + date("I") - 1);
                            elseif ($param["DELAI"] == 2)
                                $datetime = gmdate("Y-m-d H:i:s", time() + date("I") + 2);
                            elseif ($param["DELAI"] == 3) {
                                $datetime = gmdate("Y-m-d H:i:s", time() + date("I") + 3);
                            }
                            $event->set("date", $datetime);
                        }
                        
                        $event->set("typeEvent", $param["TYPE_ATTACK"]);
                        $event->addDB($db);
                        break;
                    
                    default:
                        break;
                }
            }
        }
    }
}

?>
