<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class BasicEvent extends DBObject
{

    function BasicEvent()
    {
        parent::DBObject();
        $this->m_tableName = "BasicEvent";
        $this->m_className = "BasicEvent";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["ident"] = "";
        $this->m_attr["description"] = "";
        $this->init();
    }
}

?>