<?php
require_once (HOMEPATH . "/class/StaticModifier.inc.php");

class Modifier extends StaticModifier
{

    function Modifier()
    {
        parent::StaticModifier();
        $this->m_tableName = "Modifier";
        $this->m_className = "Modifier";
        $this->m_extraName = "";
        $this->m_attr["hp_bm"] = "";
        $this->m_attr["dexterity_bm"] = "";
        $this->m_attr["strength_bm"] = "";
        $this->m_attr["speed_bm"] = "";
        $this->m_attr["magicSkill_bm"] = "";
        $this->m_attr["attack_bm"] = "";
        $this->m_attr["defense_bm"] = "";
        $this->m_attr["damage_bm"] = "";
        $this->m_attr["armor_bm"] = "";
        $this->m_attr["timeAttack_bm"] = "";
        
        $this->init();
        
        $this->m_dice = array(
            "3",
            "6",
            "8",
            "10",
            "12",
            "20",
            "100",
            "",
            ""
        );
        $this->initCharac();
    }

    function getAtt($name, $dice)
    {
        return $this->m_charac[$name][$dice];
    }

    function getScore($name)
    {
        $modif = array();
        for ($i = 0; $i < 9; $i ++) {
            $modif[$i] = $this->m_charac[$name][$i] + $this->m_charac[$name . "_bm"][$i];
        }
        $score = $this->score($modif);
        return $score;
    }

    function getScoreWithNoBM($name)
    {
        $modif = array();
        for ($i = 0; $i < 9; $i ++) {
            $modif[$i] = $this->m_charac[$name][$i];
        }
        $score = $this->score($modif);
        return $score;
    }

    function addModifObjToBM($obj)
    {
        if ($this->m_needinit)
            $this->initCharac();
        if ($obj->m_needinit)
            $obj->initCharac();
        
        foreach ($this->m_characLabel as $name) {
            
            for ($i = 0; $i < 9; $i ++) {
                $this->m_charac[$name . "_bm"][$i] += $obj->getModif($name, $i);
                
                // echo $name."_bm : ".$this->m_charac[$name."_bm"][$i]."\n";
            }
        }
        $this->m_needupdate = 1;
        $this->updateCharac();
    }

    function addModifObj2($obj)
    {
        if ($this->m_needinit)
            $this->initCharac();
        if ($obj->m_needinit)
            $obj->initCharac();
        
        foreach ($this->m_characLabel as $name) {
            if ($obj->get($name) != "////////") {
                for ($i = 0; $i < 9; $i ++) {
                    $this->m_charac[$name][$i] += $obj->getModif($name, $i);
                    
                    // echo $name."_bm : ".$this->m_charac[$name."_bm"][$i]."\n";
                }
            }
        }
        $this->m_needupdate = 1;
        $this->updateCharac();
    }

    function updateFromEquipmentLevel($level)
    {
        foreach ($this->m_characLabel as $name) {
            if ($this->get($name) != "////////") {
                for ($i = 0; $i < 9; $i ++) {
                    $this->m_charac[$name][$i] += ($this->m_charac[$name][$i] / 4) * (($level) * ($level) - $level);
                }
            }
        }
        $this->m_needupdate = 1;
        // $this->InitCharac();
    }

    function updateSharpen()
    {
        foreach ($this->m_characLabel as $name) {
            for ($i = 0; $i < 9; $i ++) {
                $this->m_charac[$name][$i] += ceil($this->m_charac[$name][$i] / 2);
            }
        }
        
        $this->m_needupdate = 1;
        // $this->updateCharac();
    }

    function updateFromTemplateLevel($level)
    {
        foreach ($this->m_characLabel as $name) {
            if ($this->get($name) != "////////") {
                for ($i = 0; $i < 9; $i ++) {
                    $this->m_charac[$name][$i] *= $level;
                }
            }
        }
        $this->m_needupdate = 1;
        // $this->updateCharac();
    }

    function validateBM()
    {
        foreach ($this->m_characLabel as $name) {
            for ($i = 0; $i < 9; $i ++) {
                if (abs($this->m_charac[$name . "_bm"][$i]) < 0.01) {
                    $this->m_charac[$name . "_bm"][$i] = 0;
                }
            }
        }
        $this->initCharacStr();
        $this->m_needupdate = 1;
        // $this->updateCharac();
    }

    function update()
    {
        $this->resetBM();
        
        $this->updateCharac();
        $this->initCharacStr();
        $this->m_needupdate = 1;
    }

    function resetBM()
    {
        foreach ($this->m_characLabel as $name) {
            for ($i = 0; $i < 9; $i ++) {
                $this->m_charac[$name . "_bm"][$i] = 0;
            }
        }
        $this->initCharacStr();
        $this->m_needupdate = 1;
        // $this->updateCharac();
    }

    function resetFigthCharac()
    {
        for ($i = 0; $i < 9; $i ++) {
            $this->m_charac["attack"][$i] = 0;
            $this->m_charac["defense"][$i] = 0;
            $this->m_charac["damage"][$i] = 0;
            $this->m_charac["timeAttack"][$i] = 0;
            // $this->m_charac["armor"][$i]=0;
        }
        
        $this->initCharacStr();
        $this->m_needupdate = 1;
        // $this->updateCharac();
    }

    function initFigthCharac($typeSword, $typeShield, $npc = 0)
    {
        
        /* ********* POUR LES PNJ - style de combat ************** */
        // Vitesse
        if ($npc % 10 == 1) {
            $typeSword = 1;
            $typeShield = 8;
        }
        // dex
        if ($npc % 10 == 2) {
            $typeSword = 2;
            $typeShield = 9;
        }
        // Force
        if ($npc % 10 == 3) {
            $typeSword = 3;
            $typeShield = 10;
        }
        // Magicien
        if ($npc % 10 == 4) {
            $typeSword = 6;
            $typeShield = 9;
        }
        
        /* *********** Monstre zone débutant pas de bouclier ************* */
        if ($npc > 10)
            $typeShield = 0;
            
            /* *************************** Defense ********************** */
        for ($i = 0; $i < 8; $i ++) {
            $this->m_charac["defense"][$i] = $this->m_charac["speed"][$i];
            $this->m_charac["defense"][$i] += $this->m_charac["speed_bm"][$i];
            $this->m_charac["defense"][$i] *= (100 + $this->m_charac["speed_bm"][DICE_MULT]) / 100;
        }
        
        switch ($typeShield) {
            case 8:
                for ($i = 0; $i < 8; $i ++) // bouclier léger
                    $this->m_charac["defense"][$i] += 0.5 * ($this->m_charac["speed"][$i] + $this->m_charac["speed_bm"][$i]) * ((100 + $this->m_charac["speed_bm"][DICE_MULT]) / 100);
                break;
            case 9:
                for ($i = 0; $i < 8; $i ++) // bouclier moyen
                    $this->m_charac["defense"][$i] += 0.5 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                break;
            
            case 10:
                for ($i = 0; $i < 8; $i ++) // bouclier grand
                    $this->m_charac["defense"][$i] += 0.5 * ($this->m_charac["strength"][$i] + $this->m_charac["strength_bm"][$i]) *
                         ((100 + $this->m_charac["strength_bm"][DICE_MULT]) / 100);
                break;
        }
        
        /**
         * *********************** Attaque et dégâts ************************
         */
        
        for ($i = 0; $i < 8; $i ++) {
            $this->m_charac["attack"][$i] = $this->m_charac["dexterity"][$i];
            $this->m_charac["attack"][$i] += $this->m_charac["dexterity_bm"][$i];
            $this->m_charac["attack"][$i] *= (100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100;
            $this->m_charac["damage"][$i] = $this->m_charac["strength"][$i] * 0.8;
            $this->m_charac["damage"][$i] += $this->m_charac["strength_bm"][$i] * 0.8;
            $this->m_charac["damage"][$i] *= (100 + $this->m_charac["strength_bm"][DICE_MULT]) / 100;
        }
        $this->m_charac["timeAttack"][7] = 8;
        
        switch ($typeSword) {
            case 1:
                for ($i = 0; $i < 8; $i ++) // Arme légère
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["speed"][$i] + $this->m_charac["speed_bm"][$i]) * ((100 + $this->m_charac["speed_bm"][DICE_MULT]) / 100);
                if ($typeShield == 0 or $typeShield == 21)
                    for ($i = 0; $i < 8; $i ++)
                        $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["speed"][$i] + $this->m_charac["speed_bm"][$i]) *
                             ((100 + $this->m_charac["speed_bm"][DICE_MULT]) / 100);
                
                $this->m_charac["timeAttack"][7] = 7;
                break;
            
            case 2:
                for ($i = 0; $i < 8; $i ++) // Arme moyenne
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                if ($typeShield == 0 or $typeShield == 21)
                    for ($i = 0; $i < 8; $i ++)
                        $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                             ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                
                break;
            
            case 3:
                for ($i = 0; $i < 8; $i ++) // Arme lourde 1 main
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["strength"][$i] + $this->m_charac["strength_bm"][$i]) *
                         ((100 + $this->m_charac["strength_bm"][DICE_MULT]) / 100);
                if ($typeShield == 0 or $typeShield == 21)
                    for ($i = 0; $i < 8; $i ++)
                        $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["strength"][$i] + $this->m_charac["strength_bm"][$i]) *
                             ((100 + $this->m_charac["strength_bm"][DICE_MULT]) / 100);
                $this->m_charac["timeAttack"][7] = 9;
                break;
            case 4:
                for ($i = 0; $i < 8; $i ++) // arme lourde 2 mains
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["strength"][$i] + $this->m_charac["strength_bm"][$i]) *
                         ((100 + $this->m_charac["strength_bm"][DICE_MULT]) / 100);
                if ($typeShield == 0 or $typeShield == 21 or $typeShield == 4)
                    for ($i = 0; $i < 8; $i ++)
                        $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["strength"][$i] + $this->m_charac["strength_bm"][$i]) *
                             ((100 + $this->m_charac["strength_bm"][DICE_MULT]) / 100);
                $this->m_charac["timeAttack"][7] = 9;
                break;
            
            case 5:
                $this->m_charac["timeAttack"][7] = 7;
            case 22:
                for ($i = 0; $i < 8; $i ++) // arcs
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                for ($i = 0; $i < 8; $i ++)
                    $this->m_charac["damage"][$i] = 0;
                for ($i = 0; $i < 8; $i ++)
                    $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                
                break;
            case 6:
                for ($i = 0; $i < 8; $i ++) // Arme magique à 1 main
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                if ($typeShield == 0 or $typeShield == 21)
                    for ($i = 0; $i < 8; $i ++)
                        $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                             ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                break;
            case 7:
                for ($i = 0; $i < 8; $i ++) // Arme magique à deux mains
                    $this->m_charac["attack"][$i] += 0.5 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                for ($i = 0; $i < 8; $i ++)
                    $this->m_charac["defense"][$i] += 0.25 * ($this->m_charac["dexterity"][$i] + $this->m_charac["dexterity_bm"][$i]) *
                         ((100 + $this->m_charac["dexterity_bm"][DICE_MULT]) / 100);
                $this->m_charac["timeAttack"][7] = 8;
                break;
        }
        
        $this->initCharacStr();
        $this->m_needupdate = 1;
        // $this->updateCharac();
    }
}

?>
