<?php

/**
 *  Define the MissionReward class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MissionReward extends DBObject
{

    function MissionReward()
    {
        parent::DBObject();
        $this->m_tableName = "MissionReward";
        $this->m_className = "MissionReward";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["idMission"] = "";
        $this->m_attr["moneyStart"] = "";
        $this->m_attr["xpStart"] = "";
        $this->m_attr["idEscort"] = "";
        $this->m_attr["idEquipStart"] = "";
        $this->m_attr["moneyStop"] = "";
        $this->m_attr["xpStop"] = "";
        $this->m_attr["idEquipStop"] = "";
        $this->m_attr["NameEquipStop"] = "";
        $this->m_attr["ExtranameEquipStop"] = "";
        $this->init();
    }
}

?>
