<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class PaypalProduct extends DBObject
{

    function PaypalProduct()
    {
        parent::DBObject();
        $this->m_tableName = "PaypalProduct";
        $this->m_className = "PaypalProduct";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["label"] = "";
        $this->m_attr["description"] = "";
        $this->init();
    }
}

?>