<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class MailContact extends DBObject
{

    function MailContact()
    {
        parent::DBObject();
        $this->m_tableName = "MailContact";
        $this->m_className = "MailContact";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Player\$friend"] = "";
        $this->init();
    }
}

?>