 <?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Player extends DBObject
{

    function Player ()
    {
        parent::DBObject();
        $this->m_tableName = "Player";
        $this->m_className = "Player";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Member"] = "";
        $this->m_attr["id_World"] = "";
        $this->m_attr["id_Team"] = "";
        $this->m_attr["id_Owner"] = "";
        $this->m_attr["id_BasicRace"] = "";
        $this->m_attr["id_Modifier"] = "";
        $this->m_attr["id_Upgrade"] = "";
        $this->m_attr["id_Detail"] = "";
        $this->m_attr["id_Mission"] = "";
        $this->m_attr["id_Caravan"] = "";
        $this->m_attr["id_FighterGroup"] = "";
        $this->m_attr["authlevel"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["gender"] = "";
        $this->m_attr["racename"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["merchant_level"] = "";
        $this->m_attr["xp"] = "";
        $this->m_attr["totalip"] = "";
        $this->m_attr["levelUp"] = "";
        $this->m_attr["ap"] = "";
        $this->m_attr["currhp"] = "";
        $this->m_attr["hp"] = "";
        $this->m_attr["money"] = "";
        $this->m_attr["moneyBank"] = "";
        $this->m_attr["moneyHistory"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["resurrectx"] = "";
        $this->m_attr["resurrecty"] = "";
        $this->m_attr["arenax"] = "";
        $this->m_attr["arenay"] = "";
        $this->m_attr["resurrect"] = "";
        $this->m_attr["resurrectid"] = "";
        $this->m_attr["overload"] = "";
        $this->m_attr["fightStyle"] = "";
        $this->m_attr["recall"] = "";
        $this->m_attr["incity"] = "";
        $this->m_attr["inbuilding"] = "";
        $this->m_attr["room"] = "";
        $this->m_attr["hidden"] = "";
        $this->m_attr["prison"] = "";
        $this->m_attr["disabled"] = "";
        $this->m_attr["nbkill"] = "";
        $this->m_attr["nbdeath"] = "";
        $this->m_attr["nbkillpc"] = "";
        $this->m_attr["status"] = "";
        $this->m_attr["atb"] = "";
        $this->m_attr["curratb"] = "";
        $this->m_attr["playatb"] = "";
        $this->m_attr["nextatb"] = "";
        $this->m_attr["creation"] = "";
        $this->m_attr["lastdeath"] = "";
        $this->m_attr["lastarena"] = "";
        $this->m_attr["inarena"] = "";
        $this->m_attr["state"] = "";
        $this->m_attr["id_Player\$target"] = "";
        $this->m_attr["behaviour"] = "";
        $this->m_attr["id_City"] = "";
        $this->m_attr["ip"] = "";
        $this->m_attr["ip_data"] = "";
        $this->m_attr["advert"] = "";
        $this->m_attr["Quest_Received"] = "";
        $this->m_attr["map"] = "";
        $this->m_attr["picture"] = "";
        $this->m_attr["modeAnimation"] = "";
        $this->init();
    }

    function getScore ($val)
    {
        return $this->getObj("Modifier")->getScore($val);
    }

    function getScoreFromDice ($val1, $val2, $val3)
    {
        return $this->getObj("Modifier")->getScoreFromDice($val1, $val2, $val3);
    }

    function getScoreWithNoBM ($val)
    {
        return $this->getObj("Modifier")->getScoreWithNoBM($val);
    }

    function getModif ($field, $dice)
    {
        if ($this->getObj("Modifier") == null)
            trigger_error("Modifier is null in getModif() (" . $this->m_className . ")", E_USER_ERROR);
        
        return $this->getObj("Modifier")->getModif($field, $dice);
    }

    function getModifStr ($field)
    {
        return $this->getObj("Modifier")->getModifStr($field);
    }

    function getModifMean ($field)
    {
        return $this->getObj("Modifier")->getModifMean($field);
    }

    function getModifMeanWithNoBM ($field)
    {
        return $this->getObj("Modifier")->getModifMeanWithNoBM($field);
    }

    function getModifMin ($field)
    {
        return $this->getObj("Modifier")->getModifMin($field);
    }

    function getModifMinWithNoBM ($field)
    {
        return $this->getObj("Modifier")->getModifMinWithNoBM($field);
    }

    function getModifMax ($field)
    {
        return $this->getObj("Modifier")->getModifMax($field);
    }

    function addModif ($name, $type, $value)
    {
        return $this->getObj("Modifier")->addModif($name, $type, $value);
    }

    function subModif ($name, $type, $value)
    {
        return $this->getObj("Modifier")->subModif($name, $type, $value);
    }

    function multModif ($name, $type, $value)
    {
        return $this->getObj("Modifier")->multModif($name, $type, $value);
    }

    function divModif ($name, $type, $value)
    {
        return $this->getObj("Modifier")->divModif($name, $type, $value);
    }

    function setModif ($name, $type, $value)
    {
        return $this->getObj("Modifier")->setModif($name, $type, $value);
    }

    function getHPPercent ()
    {
        $hp = $this->getObj("Modifier")->getModif("hp", DICE_ADD);
        $hppercent = $this->get("currhp") * 100;
        if ($hp != 0) {
            $hppercent /= $hp;
        } else {
            $hppercent = 0;
        }
        $hppercent = round($hppercent);
        return $hppercent;
    }

    function updateHidden ($db)
    {
        // Gestion du hidden pour la vue
        if (($this->isInCity($db) == 1) && floor($this->get("hidden") / 10) != 1)
            $this->set("hidden", $this->get("hidden") % 10 + 10);
        if (($this->isInCity($db) == 2) && floor($this->get("hidden") / 10) != 2)
            $this->set("hidden", $this->get("hidden") % 10 + 20);
        if (($this->isInCity($db) == 0) && floor($this->get("hidden") / 10) != 0)
            $this->set("hidden", $this->get("hidden") % 10);
        
        self::updateBuildingInformation($db);
    }

    function updateBuildingInformation ($db)
    {
        if (! self::isInBuilding($db)) {
            $this->set("room", 0);
            $this->set("inbuilding", 0);
        } else {
            if ($this->get("status") == "NPC")
                $this->set("room", ENTRANCE_ROOM);
        }
    }

    function isInBuilding ($db)
    {
        // 0 à l'extérieur
        // 1 pour dans un bâtiment
        $xp = $this->get("x");
        $yp = $this->get("y");
        $map = $this->get("map");
        
        $dbp = new DBCollection("SELECT * FROM Building WHERE  x=" . $xp . " AND y=" . $yp . " AND map=" . $map, $db);
        if ($dbp->count() > 0)
            return 1;
        return 0;
    }

    /*
     * function isInThisCity($id, $db) code jamais appelé
     * {
     * $dbt = new DBCollection("SELECT * FROM Temple WHERE name='Temple' AND id_City=" . $id, $db);
     * $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $id, $db);
     *
     * $dist = distHexa($this->get("x"), $this->get("y"), $dbt->get("x"), $dbt->get("y"));
     *
     * if ($dist > 5)
     * return 0;
     *
     * if ($dist > 4 && $dbc->get("type") != "Ville")
     * return 0;
     *
     * if ($dist == 4 && ($dbc->get("captured") == 3 or $dbc->get("captured") == 1))
     * return 2;
     *
     * if ($dist == 4 && ($dbc->get("captured") == 4 or $dbc->get("captured") == 2))
     * return 0;
     *
     * if ($dist < 4)
     * return 1;
     * }
     */
    function isInCity ($db)
    {
        // 0 pour pas dans dans ville (avec mur)
        // 1 pour dans une ville (avec mur)
        // 2 pour sur une porte ouverte
        $xp = $this->get("x");
        $yp = $this->get("y");
        
        // Porte ou rempart
        $dbp = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding>15 and id_BasicBuilding<27 AND x=" . $xp . " AND y=" . $yp, $db);
        if (! $dbp->eof())
            return 2;
        
        $dbt = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=5", $db);
        if ($dbt->eof())
            return 0;
        else {
            $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $dbt->get("id_City"), $db);
            if ($dbc->get("type") == "Ville")
                return 1;
            elseif (distHexa($xp, $yp, $dbt->get("x"), $dbt->get("y")) <= 3 && ($dbc->get("captured") == 3 || $dbc->get("captured") == 4))
                return 1;
            else
                return 0;
        }
    }

    function canSeePlayerById ($id_Opp, $db)
    {
        $opponent = new Player();
        $opponent->load($id_Opp, $db);
        return self::canSeePlayer($opponent, $db);
    }

    function canSeePlayer ($opponent, $db)
    {
        // 0 pour je le vois pas
        // 1 pour je le vois
        // 2 pour je le vois transparent
        $hidden1 = $this->get("hidden");
        $hidden2 = $opponent->get("hidden");
        
        if ($opponent->get("id") == $this->get("id")) {
            if ($hidden1 % 10 == 1 || $hidden1 % 10 == 2)
                return 2;
            elseif ($hidden1 % 10 == 3)
                return 0;
            else
                return 1;
        }
        
        if ($hidden2 == 99)
            return 0;
        
        switch ($hidden1) {
            case 0:
            case 1:
            case 2:
            case 3:
                switch ($hidden2) {
                    case 0:
                    case 20:
                        return 1;
                        break;
                    
                    case 1:
                    case 21:
                        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->get("id") . " AND id_Player\$Src=" . $opponent->get("id") . " AND name='Embuscade'", $db);
                        if ($dbbm->eof())
                            return 0;
                        else
                            return 2;
                        break;
                    
                    default:
                        return 0;
                        break;
                }
                break;
            
            case 10:
            case 11:
            case 12:
            case 13:
                switch ($hidden2) {
                    case 10:
                    case 20:
                        return 1;
                        break;
                    
                    case 11:
                    case 21:
                        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->get("id") . " AND id_Player\$Src=" . $opponent->get("id") . " AND name='Embuscade'", $db);
                        if ($dbbm->eof())
                            return 0;
                        else
                            return 2;
                        break;
                    
                    default:
                        return 0;
                        break;
                }
                break;
            
            case 20:
            case 21:
            case 22:
            case 23:
                switch ($hidden2) {
                    case 0:
                    case 10:
                    case 20:
                        return 1;
                        break;
                    
                    case 1:
                    case 11:
                    case 21:
                        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->get("id") . " AND id_Player\$Src=" . $opponent->get("id") . " AND name='Embuscade'", $db);
                        if ($dbbm->eof())
                            return 0;
                        else
                            return 2;
                        break;
                    
                    default:
                        return 0;
                        break;
                }
                break;
            case 99:
                return 0;
                break;
            default:
                break;
        }
    }

    function canSeeCase ($x, $y, $db)
    {
        // 0 pour je le vois pas
        // 1 pour je le vois
        $playerPosition = floor($this->get("hidden") / 10);
        $casePosition = isCaseInCityWithWall($x, $y, $db);
        
        if ($playerPosition == 2 || $casePosition == 2)
            return 1;
        
        if ($playerPosition == $casePosition)
            return 1;
        else
            return 0;
    }

    function isWanted ($db)
    {
        $dbc = new DBCollection("SELECT * FROM Player WHERE id_BasicRace = 5 and prison=" . $this->get("id"), $db);
        if ($dbc->eof())
            return 0;
        
        return 1;
    }

    function getTimeToMove ($db)
    {
        $PA = getTimetoMove($this->get("x"), $this->get("y"), $this->get("map"), $db);
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='Course Celeste' AND id_Player=" . $this->get("id"), $db);
        if (! $dbbm->eof())
            $PA = min($PA, 1);
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->get("id") . " AND id_Player\$Src=" . $this->get("id") . " AND name='Embuscade'", $db);
        if (! $dbbm->eof())
            $PA += 3;
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='Blessure gênante' AND id_Player=" . $this->get("id"), $db);
        if (! $dbbm->eof())
            $PA += 1;
        
        if ($this->get("state") == "creeping")
            $PA = 5;
        
        if ($this->get("state") == "turtle") {
            $PA += 1;
            if (getLandType($this->get("x"), $this->get("y"), $this->get("map"), $db) == "marsh")
                $PA = 1;
            
            $dbt = new DBCollection("SELECT Player.state FROM Player LEFT JOIN Caravan ON Caravan.id=Player.id_Caravan WHERE Caravan.id_Player=" . $this->get("id"), $db);
            if (! $dbt->eof() && $dbt->get("state") == "creeping")
                $PA = 5;
        }
        return $PA;
    }

    function accessRoom ($id_Building, $room, $db)
    {
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $id_Building, $db);
        if ($dbb->get("id_Player") == 0) {
            $dbc = new DBCollection("SELECT Player.* FROM Player inner join City on City.id_Player = Player.id WHERE City.id=" . $dbb->get("id_City"), $db);
        } else {
            $dbc = new DBCollection("SELECT * FROM Player WHERE id=" . $dbb->get("id_Player"), $db);
        }
        
        if ($dbb->get("id_Player") == $this->get("id") || ($dbc->get("id") == $this->get("id") && $room != BEDROOM))
            return 1;
        
        if ($dbc->get("id_Team") == $this->get("id_Team") && $dbc->get("id_Team") > 0) {
            $dbt = new DBCollection("SELECT * FROM TeamRankInfo WHERE id IN (SELECT id_TeamRankInfo FROM TeamRank WHERE id_Player=" . $this->get("id") . ")", $db);
            // 22 architecte - 23 trésorerie - 24 - salle du gouverneur
            if ($dbt->get("auth" . ($room - 17)) == 'Yes')
                return 1;
            else
                return 0;
        } else
            return 0;
    }

    function addBM ($bm, $style, &$param, $db)
    {
        $param["LEVEL_BM"] = $bm->get("level");
        $param["VALUE_BM"] = $bm->get("value");
        $param["CANCEL"] = 0;
        
        if ($bm->get("effect") == "NEGATIVE") {
            $dbb = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->get("id") . " AND name='Bénédiction'", $db);
            if (! $dbb->eof()) {
                $param["LEVEL_BENEDICTION"] = $dbb->get("level");
                if ($param["LEVEL_BENEDICTION"] >= $param["LEVEL_BM"])
                    $param["CANCEL"] = 1;
                else {
                    $param["LEVEL_BM_FINAL"] = $param["LEVEL_BM"] - $param["LEVEL_BENEDICTION"];
                    $param["VALUE_BM_FINAL"] = $bm->get("value") - floor($param["LEVEL_BENEDICTION"] * ($bm->get("value") / $bm->get("level")));
                    $bm->set("value", $param["VALUE_BM_FINAL"]);
                    $bm->set("level", $param["LEVEL_BM_FINAL"]);
                }
            }
        }
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_StaticModifier_BM=" . $bm->get("id_StaticModifier") . " AND id_Player=" . $this->get("id") . " AND name='" . addslashes($bm->get("name")) . "'", $db);
        if (! $dbbm->eof()) {
            if ($style == 1) // Ajout du level, life, value
                $dbu = new DBCollection("UPDATE BM SET value=value+" . $bm->get("value") . ", level=level+" . $bm->get("level") . ", life=life+" . $bm->get("life") . " WHERE id_StaticModifier_BM=" . $bm->get("id_StaticModifier") . " AND id_Player=" . $this->get("id") . " AND name='" . addslashes($bm->get("name")) . "'", $db, 0, 0, false);
            if ($style == 2) // ajout du level value sans toucher à life
                $dbu = new DBCollection("UPDATE BM SET value=value+" . $bm->get("value") . ", level=level+" . $bm->get("level") . " WHERE id_StaticModifier_BM=" . $bm->get("id_StaticModifier") . " AND id_Player=" . $this->get("id") . " AND name='" . addslashes($bm->get("name")) . "'", $db, 0, 0, false);
            if ($style == 3) // On remplace
                $dbu = new DBCollection("UPDATE BM SET value=" . $bm->get("value") . ", life=" . $bm->get("life") . ", level=" . $bm->get("level") . " WHERE id_StaticModifier_BM=" . $bm->get("id_StaticModifier") . " AND id_Player=" . $this->get("id") . " AND name='" . addslashes($bm->get("name")) . "'", $db, 0, 0, false);
        } 

        else
            $bm->addDB($db);
        
        PlayerFactory::initBM($this, $this->getObj("Modifier"), $param, $db, 0);
        $this->updateDBr($db);
    }
}
?>
