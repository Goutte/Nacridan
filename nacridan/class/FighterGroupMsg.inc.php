<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class FighterGroupMsg extends DBObject
{

    function FighterGroupMsg()
    {
        parent::DBObject();
        $this->m_tableName = "FighterGroupMsg";
        $this->m_className = "FighterGroupMsg";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_FighterGroup"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["body"] = "";
        $this->m_attr["date"] = "";
        $this->init();
    }
}

?>