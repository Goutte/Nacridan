<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Knowledge extends DBObject
{

    function Knowledge()
    {
        parent::DBObject();
        $this->m_tableName = "Knowledge";
        $this->m_className = "Knowledge";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_BasicTalent"] = "";
        $this->m_attr["id_BasicEquipment"] = "";
        $this->init();
    }
}

?>
