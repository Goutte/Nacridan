<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Place extends DBObject
{

    function Place()
    {
        parent::DBObject();
        $this->m_tableName = "Place";
        $this->m_className = "Place";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["map"] = "";
        $this->init();
    }
}

?>
