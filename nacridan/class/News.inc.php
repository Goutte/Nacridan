<?php

/**
 *  Define the News class
 *
 * 
 *
 *@author Aé Li
 *@version 1.0
 *@package NacridanV1
 *@subpackage Class
 */
require_once (HOMEPATH . "/class/DBObject.inc.php");

class News extends DBObject
{

    function News()
    {
        parent::DBObject();
        $this->m_tableName = "News";
        $this->m_className = "News";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["id_Player"] = "";
        $this->m_attr["id_Building"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->m_attr["map"] = "";
        $this->m_attr["level"] = "";
        $this->m_attr["type"] = "";
        $this->m_attr["date"] = "";
        $this->m_attr["title"] = "";
        $this->m_attr["content"] = "";
        $this->init();
    }
}

?>
