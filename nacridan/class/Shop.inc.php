<?php
require_once (HOMEPATH . "/class/DBObject.inc.php");

class Shop extends DBObject
{

    function Shop()
    {
        parent::DBObject();
        $this->m_tableName = "Shop";
        $this->m_className = "Shop";
        $this->m_extraName = "";
        $this->m_attr["id"] = "";
        $this->m_attr["name"] = "";
        $this->m_attr["x"] = "";
        $this->m_attr["y"] = "";
        $this->init();
    }
}

?>