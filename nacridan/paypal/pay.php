<?php
// file_put_contents("/tmp/paypal",gmdate("Y-m-d H:i:s")."\n",FILE_APPEND);
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

$db = DB::getDB();
// $dbe= new DBCollection("INSERT INTO Paypal (post_value) VALUES ('".mysql_real_escape_string(serialize($_POST))."')",$db,0,0,false);

$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
    $value = urlencode(stripslashes($value));
    $req .= "&$key=$value";
}

// renvoyer au système PayPal pour validation
$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
// $fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
$fp = fsockopen(PAYPAL, 80, $errno, $errstr, 30);

$item_name = mysql_real_escape_string($_POST['item_name']);
$item_number = mysql_real_escape_string($_POST['item_number']);
$mc_gross = mysql_real_escape_string($_POST['mc_gross']);
$mc_currency = mysql_real_escape_string($_POST['mc_currency']);
$mc_fee = mysql_real_escape_string($_POST['mc_fee']);
$txn_id = mysql_real_escape_string($_POST['txn_id']);
$payment_status = mysql_real_escape_string($_POST['payment_status']);
// $receiver_email = mysql_real_escape_string($_POST['receiver_email']);
// $payer_email = mysql_real_escape_string($_POST['payer_email']);

if (! $fp) {
    // ERREUR HTTP
} else {
    fputs($fp, $header . $req);
    while (! feof($fp)) {
        $res = fgets($fp, 1024);
        
        if (strcmp($res, "VERIFIED") == 0) {
            
            if ($payment_status = "Completed") {
                $dbe = new DBCollection("INSERT INTO Paypal (item_name,item_number,txn_id,mc_gross,mc_currency,mc_fee,post_value,date)  VALUES ('" . $item_name . "','" . $item_number . "','" . $txn_id . "','" . $mc_gross . "','" . $mc_currency . "','" . $mc_fee . "','" . mysql_real_escape_string(serialize($_POST)) . "','" . gmdate("Y-m-d H:i:s") . "')", $db, 0, 0, false);
                
                if ($item_number == 1) {
                    $players = explode(";", $item_name);
                    
                    $realPlayers = array();
                    
                    foreach ($players as $id) {
                        if (! empty($id)) {
                            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $id, $db);
                            
                            while (! $dbp->eof()) {
                                $realPlayers[] = $dbp->get("id");
                                $dbp->next();
                            }
                        }
                    }
                    
                    $count = count($realPlayers);
                    
                    foreach ($realPlayers as $id) {
                        $advert = ceil(($mc_gross * 14) / $count);
                        
                        $dbu = new DBCollection("UPDATE Player SET advert=advert+" . $advert . " WHERE id=" . $id, $db, 0, 0, false);
                    }
                }
            }
            
            // vérifier que payment_status est Terminé
            // vérifier que txn_id n'a pas été précédemment traité
            // vérifier que receiver_email est votre adresse email PayPal principale
            // vérifier que payment_amount et payment_currency sont corrects
            // traiter le paiement
        } else 
            if (strcmp($res, "INVALID") == 0) {
                // consigner pour étude manuelle
            }
    }
    fclose($fp);
}
?>
