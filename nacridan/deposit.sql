
-- blablabla
-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Ven 06 Ao�t 2010 � 18:45
-- Version du serveur: 5.1.32
-- Version de PHP: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de donn�es: `nacridan`
--

-- --------------------------------------------------------

-- INSERT INTO `nacridan`.`BasicEquipment` (`id` ,`id_Modifier_BasicEquipment` ,`id_EquipmentType` ,`name` ,`minIncreaseByLevel` ,
-- `frequency` ,`immediate` ,`minipic` ,`bigpic`) VALUES

-- ( '56' , '0', '20', 'Emeraude', '0', '3', 'No', '', NULL ),
-- ( '57' , '0', '20', 'Rubis', '0', '2', 'No', '', NULL ),
-- ( '58' , '0', '20', 'Diamant', '0', '1', 'No', '', NULL );


UPDATE `nacridan`.`EquipmentType` SET `name` = 'Gemme' WHERE `EquipmentType`.`id` =20 LIMIT 1 ;


INSERT INTO `nacridan`.`BasicAbility` (`id`, `name`, `ident`, `cost`) VALUES 
(9, 'Miner', 'ABILITY_MINE', '50'),
(10, 'Tailler', 'ABILITY_CARVE', '50');


INSERT INTO `nacridan`.`BasicMagicSpell` (`id`, `name`, `ident`, `cost`) VALUES
('6', 'Enchanter', 'SPELL_ENCHANT', '200');

--
-- Structure de la table `deposit`
--

CREATE TABLE IF NOT EXISTS `Deposit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT 'Gisement',
  `type` varchar(20) NOT NULL DEFAULT 'Emeraude',
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `value` int(11) NOT NULL DEFAULT '10',
  `captured` int(11) NOT NULL DEFAULT '0',
  `x` smallint(6) NOT NULL DEFAULT '0',
  `y` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `xy` (`x`,`y`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Contenu de la table `deposit`
--

INSERT INTO `Deposit` (`id`, `name`, `type`, `level`, `value`, `captured`, `x`, `y`) VALUES
(37, 'Gisement', 'Emeraude', 1, 10, 0, 363, 137),
(13, 'Gisement', 'Emeraude', 1, 6, 0, 135, 108),
(74, 'Gisement', 'Emeraude', 1, 10, 0, 390, 343),
(14, 'Gisement', 'Emeraude', 1, 10, 0, 457, 231),
(15, 'Gisement', 'Emeraude', 1, 10, 0, 654, 21),
(16, 'Gisement', 'Emeraude', 1, 10, 0, 323, 389),
(17, 'Gisement', 'Emeraude', 1, 10, 0, 506, 391),
(18, 'Gisement', 'Emeraude', 1, 10, 0, 414, 8),
(19, 'Gisement', 'Emeraude', 1, 10, 0, 301, 372),
(20, 'Gisement', 'Emeraude', 1, 10, 0, 338, 153),
(21, 'Gisement', 'Emeraude', 1, 10, 0, 291, 239),
(107, 'Gisement', 'Diamant', 1, 10, 0, 140, 372),
(23, 'Gisement', 'Emeraude', 1, 10, 0, 141, 285),
(24, 'Gisement', 'Emeraude', 1, 10, 0, 247, 163),
(25, 'Gisement', 'Emeraude', 1, 10, 0, 362, 284),
(26, 'Gisement', 'Emeraude', 1, 10, 0, 167, 176),
(39, 'Gisement', 'Emeraude', 1, 10, 0, 284, 62),
(38, 'Gisement', 'Emeraude', 1, 10, 0, 411, 207),
(40, 'Gisement', 'Emeraude', 1, 10, 0, 468, 243),
(41, 'Gisement', 'Emeraude', 1, 10, 0, 56, 111),
(42, 'Gisement', 'Emeraude', 1, 10, 0, 146, 386),
(43, 'Gisement', 'Emeraude', 1, 10, 0, 551, 252),
(44, 'Gisement', 'Emeraude', 1, 10, 0, 451, 362),
(45, 'Gisement', 'Emeraude', 1, 10, 0, 476, 37),
(46, 'Gisement', 'Emeraude', 1, 10, 0, 351, 198),
(47, 'Gisement', 'Emeraude', 1, 10, 0, 451, 36),
(48, 'Gisement', 'Emeraude', 1, 10, 0, 132, 291),
(49, 'Gisement', 'Emeraude', 1, 10, 0, 184, 257),
(50, 'Gisement', 'Emeraude', 1, 10, 0, 323, 197),
(51, 'Gisement', 'Emeraude', 1, 10, 0, 315, 119),
(52, 'Gisement', 'Emeraude', 1, 10, 0, 392, 12),
(53, 'Gisement', 'Emeraude', 1, 10, 0, 388, 253),
(54, 'Gisement', 'Emeraude', 1, 10, 0, 350, 61),
(55, 'Gisement', 'Emeraude', 1, 10, 0, 394, 202),
(56, 'Gisement', 'Emeraude', 1, 10, 0, 99, 52),
(57, 'Gisement', 'Emeraude', 1, 10, 0, 248, 43),
(58, 'Gisement', 'Emeraude', 1, 10, 0, 156, 191),
(59, 'Gisement', 'Emeraude', 1, 10, 0, 234, 101),
(60, 'Gisement', 'Emeraude', 1, 10, 0, 541, 305),
(61, 'Gisement', 'Emeraude', 1, 10, 0, 227, 10),
(62, 'Gisement', 'Emeraude', 1, 10, 0, 236, 72),
(63, 'Gisement', 'Emeraude', 1, 10, 0, 254, 201),
(64, 'Gisement', 'Emeraude', 1, 10, 0, 354, 89),
(65, 'Gisement', 'Emeraude', 1, 10, 0, 68, 27),
(66, 'Gisement', 'Emeraude', 1, 10, 0, 28, 130),
(67, 'Gisement', 'Emeraude', 1, 10, 0, 135, 357),
(68, 'Gisement', 'Emeraude', 1, 10, 0, 516, 162),
(69, 'Gisement', 'Emeraude', 1, 10, 0, 46, 362),
(70, 'Gisement', 'Emeraude', 1, 10, 0, 256, 155),
(71, 'Gisement', 'Emeraude', 1, 10, 0, 295, 289),
(72, 'Gisement', 'Emeraude', 1, 10, 0, 350, 242),
(77, 'Gisement', 'Rubis', 1, 10, 0, 344, 321),
(78, 'Gisement', 'Rubis', 1, 10, 0, 534, 78),
(79, 'Gisement', 'Rubis', 1, 10, 0, 115, 163),
(80, 'Gisement', 'Rubis', 1, 10, 0, 420, 194),
(81, 'Gisement', 'Rubis', 1, 10, 0, 37, 322),
(82, 'Gisement', 'Rubis', 1, 10, 0, 96, 284),
(83, 'Gisement', 'Rubis', 1, 10, 0, 584, 26),
(84, 'Gisement', 'Rubis', 1, 10, 0, 293, 218),
(85, 'Gisement', 'Rubis', 1, 10, 0, 376, 13),
(86, 'Gisement', 'Rubis', 1, 10, 0, 177, 373),
(87, 'Gisement', 'Rubis', 1, 10, 0, 106, 55),
(88, 'Gisement', 'Rubis', 1, 10, 0, 326, 259),
(89, 'Gisement', 'Rubis', 1, 10, 0, 583, 60),
(90, 'Gisement', 'Rubis', 1, 10, 0, 309, 192),
(91, 'Gisement', 'Rubis', 1, 10, 0, 316, 7),
(92, 'Gisement', 'Rubis', 1, 10, 0, 166, 255),
(93, 'Gisement', 'Rubis', 1, 10, 0, 213, 320),
(94, 'Gisement', 'Rubis', 1, 10, 0, 176, 89),
(95, 'Gisement', 'Rubis', 1, 10, 0, 417, 373),
(96, 'Gisement', 'Rubis', 1, 10, 0, 461, 35),
(97, 'Gisement', 'Rubis', 1, 10, 0, 122, 125),
(98, 'Gisement', 'Rubis', 1, 10, 0, 318, 61),
(99, 'Gisement', 'Rubis', 1, 10, 0, 217, 51),
(100, 'Gisement', 'Rubis', 1, 10, 0, 100, 139),
(101, 'Gisement', 'Rubis', 1, 10, 0, 111, 25),
(102, 'Gisement', 'Rubis', 1, 10, 0, 445, 21),
(103, 'Gisement', 'Rubis', 1, 10, 0, 224, 78),
(104, 'Gisement', 'Rubis', 1, 10, 0, 235, 131),
(105, 'Gisement', 'Rubis', 1, 10, 0, 118, 24),
(106, 'Gisement', 'Rubis', 1, 10, 0, 135, 303),
(108, 'Gisement', 'Diamant', 1, 10, 0, 566, 28),
(109, 'Gisement', 'Diamant', 1, 10, 0, 397, 212),
(110, 'Gisement', 'Diamant', 1, 10, 0, 584, 43),
(111, 'Gisement', 'Diamant', 1, 10, 0, 498, 34),
(112, 'Gisement', 'Diamant', 1, 10, 0, 284, 339),
(113, 'Gisement', 'Diamant', 1, 10, 0, 164, 19),
(114, 'Gisement', 'Diamant', 1, 10, 0, 88, 258),
(115, 'Gisement', 'Diamant', 1, 10, 0, 214, 334),
(116, 'Gisement', 'Diamant', 1, 10, 0, 163, 231),
(117, 'Gisement', 'Diamant', 1, 10, 0, 39, 345);
