<?php
require_once ("../../conf/config.ini.php");

switch (getBrowserDefaultLang()) {
    case "fr-fr":
    case "fr":
        $lang = "fr";
        break;
    default:
        $lang = "en";
        break;
}

switch ($lang) {
    case "fr":
        redirect(CONFIG_HOST . "/i18n/rules/" . $lang . "/rules.php");
        break;
    default:
        redirect(CONFIG_HOST . "/i18n/rules/fr/rules.php");
        break;
}
?>