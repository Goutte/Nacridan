<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 201; $i < 300; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i .
         "\");return false;' > ";
}
// class='stylepc'

?>

<a name="hdp"></a>
<div class='subtitle'>Introduction</div>
<p class='rule_pg'>
	Les sortilèges s'apprennent dans les écoles de magie des villes et des
	villages en échange de Pièces d'Or et de Points d'Action.<br /> Une
	fois apprise, vous obtiendrez une nouvelle action disponible dans le
	menu "sortilèges". Comme toutes les actions, les sortilèges nécessitent
	un nombre de points d'action qui leur sont propres pour être utilisés
	et influent sur vos jauges de progression spécifiquement.<br /> <br />
	Après apprentissage, vous ne maitrisez le sortilège qu'à 50% ce qui
	signifie que vous n'avez qu'une chance sur deux de réussir le
	sortilège. Si vous ratez votre jet de réussite, aucune action n'est
	réalisée et vous perdez des PA. <br /> A chaque fois que vous
	réussissez votre sortilège, vous avez une chance d'améliorer votre
	pourcentage de maitrise de 1%. Pour améliorer il vous faudra réussir un
	d100 supérieur à votre pourcentage de maîtrise. <small><?php echo $name[299];?>Détails </a></small><br />
</li>
<br />
Il y a 3 niveaux de sortilège :
<br />
-Les sortilèges élémentaires.
<br />
-Les sortilèges de niveau aspirant.
<br />
-Les sortilèges de niveau adepte.
<br />
<br />
Au début de votre aventure, vous ne pourrez apprendre que les sortilèges
élémentaires. Lorsque votre pourcentage de maitrise sera suffisant vous
aurez alors accès aux sortilèges de niveau supérieur
<small><?php echo $name[298];?> Détails </a></small>
<br />
<br />
<i>Dans le lien suivant, il est décrit le cadre Role Play de
	l'utilisation de la magie dans Nacridan. Il aide notamment à comprendre
	les caractéristiques utilisées dans chacune des écoles de magie et
	l'importance des bâtons et des sceptres dans l'utilisation des sorts de
	destruction. <small><?php echo $name[297];?> Détails </a></small><br />
</i>
<br />

</p>
<br />
<div class='subtitle'>
	<a name="Sorts de Base"></a>Les Sortilèges élémentaires
</div>
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b>Toucher Brulant (8 PA, Dex, MM):</b><br />
		Toucher brulant est un sortilège d'attaque qui permet d'infliger des
		dégâts à la créature ciblée à l'aide de sa maitrise de la magie. Il
		constitue l'attaque de base de tous les mages se destinant à maitriser
		les sorts de l'école de destruction.<br> <small><?php echo $name[201];?> Détails </a></small>
	
	<li style="margin-bottom: 20px;"><b>Armure d'Athlan (5 PA, Force, MM):</b><br />
		Armure d'Athlan est un sortilège de soutien qui permet au lanceur
		d'augmenter l'armure de la créature ciblée.<br /> <small><?php echo $name[202];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b>Larmes de Vie (7 PA, MM):</b><br />
		Basé sur la Maitrise de la Magie uniquement, ce sortilège soigne la
		créature ciblée.<br /> <small><?php echo $name[203];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Ailes de Colère"></a>Ailes
			de Colère (5 PA, Vit, MM):</b><br /> Ce sort invoque la fureur des
		vents pour soutenir les bras de la cible pendant 2 tours en lui
		octroyant un bonus en Attaque ou en Dégâts, au choix du magicien.<br />
		<small><?php echo $name[222];?> Détails </a></small></li>
</ul>
</p>
<br />
<br />
<div class='subtitle'>
	<a name="Guérison"></a>Les Sortilèges de l'École de Guérison
</div>
<br />
<b>Sortilèges de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Souffle d'Athlan"></a>Souffle
			d'Athlan (5 PA, MM):</b><br /> Souffle d'Athlan permet de dissiper
		toutes les malédictions et les maux qui accablent une cible comme le
		poison, les blessures profondes ou les malédictions <br /> <small><?php echo $name[204];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Régénération"></a>Charme
			de Vitalité (8 PA, MM): </b><br /> Charme de vitalité est un sort de
		guérison qui procure à la cible une régénération qui s'activera . <br />
		<small><?php echo $name[205];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Bassin Divin"></a>Bassin
			Divin (9 PA, MM):</b><br /> Permet de créer un bassin d'eau où tout
		aventurier ou monstre peut venir y boire pour soigner ses blessures.<br />
		<small><?php echo $name[206];?> Détails </a></small></li>
</ul>
</p>
<br />
<b>Sortilèges de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Pluie Sacrée"></a>Pluie
			Sacrée (9 PA, MM):</b><br /> Pluie Sacrée est un soin de soin de
		zone. Il permet de soigner tous les personnages situés dans zone
		d'effet du sort.<br> <small><?php echo $name[216];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Recharge du Bassin"></a>Recharge
			du Bassin (5 PA, MM):</b><br /> Remplit un Bassin Divin existant pour
		de nouveau en permettre 5 utilisations si le niveau du sort est au
		moins du niveau du (Bassin divin -2).<br /> <small><?php echo $name[217];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Soleil de Guérison"></a>Soleil
			de Guérison (10PA, MM):</b><br /> Soleil de guérison est un sort de
		guérison passif au bénéfice de tous les membres du groupe de chasse.
		Tant que le soleil de guérison est actif, tout membre du Groupe de
		Chasse subissant des dégâts "directs" est automatiquement soigné d'une
		partie de la perte de vie subie.<br /> <small><?php echo $name[218];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
<div class='subtitle'>
	<a name="Protection"></a>Les Sortilèges de l'École de Protection
</div>
<br />
<b>Sortilèges de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>

	<li style="margin-bottom: 20px;"><b><a name="Bouclier Magique"></a>Bouclier
			Magique (5 PA, For, MM):</b><br /> Bouclier magique est un sortilège
		permettant d'augmenter la défense d'un personnage.<br /> <small><?php echo $name[210];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Réveil de la Terre"></a>Réveil
			de la Terre (8 PA, For, MM):</b><br /> Reveil de la Terre permet au
		mage qui l'utilise de faire jaillir de terre jusqu'à 4 murs. Chaque
		mur peut être indépendamment révoqué par le sorcier.<br /> <small><?php echo $name[208];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Barrière Enflammée"></a>Barrière
			Enflammée (5 PA, For, MM):</b><br /> Crée une barrière de feu autour
		du personnage ciblé occasionnant des dégâts à toute créature réalisant une attaque ou un sort au contact sur ce personnage.<br /> <small><?php echo $name[209];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<b>Sortilèges de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Bénédiction"></a>Bénédiction
			(5 PA, For, MM):</b><br /> Le sortilège Bénédiction permet au mage de
		proteger sa cible des malédictions pendant une courte durée.<br /> <small><?php echo $name[207];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Bulle de Vie"></a>Bulle de
			Vie (8 PA, For, MM):</b><br /> Une Bulle de Vie se place autour de la
		cible absorbant les prochains dégâts qui lui seront infligés. La Bulle
		n'est pas efficace contre les pertes de vie venant de maux intérieurs.
		<br /> <small><?php echo $name[219];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Souffle de Négation"></a>Souffle
			de Négation (8 PA, For):</b><br /> Un Souffle de Négation propulse le
		corps d'un membre de son Groupe de Chasse hors de la réalité.
		L'enveloppe corporelle de la cible, bien que violemment abîmée demeure
		alors invisible ce qui empêche toute action sur celle-ci.<br /> <small><?php echo $name[215];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
<div class='subtitle'>
	<a name="Destruction"></a>Les Sortilèges de l'École de Destruction
</div>
<br />
<b>Sortilèges de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Boule de Feu"></a>Boule de
			Feu (8 PA, Dex, MM):</b><br /> Boule de feu est une attaque magique à
		distance qui permet d'infliger des dégâts à la cible. Moins puissante
		que le toucher brûlant, la boule de feu permet toutefois de toucher
		plus facilement les cibles qui possèdent une bonne défense. De plus,
		elle ignore la moitié de l'armure adverse.<br /> <small><?php echo $name[211];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Poing du Démon"></a>Poing
			du Démon (7 PA, Dex, MM):</b><br /> Poing du démon est une attaque
		magique au contact qui n'inflige pas de dégât mais qui permet de
		détruire les pièces d'armures de la cible. <br /> <small><?php echo $name[212];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Appel du Feu"></a>Appel du
			Feu (10 PA, Dex, MM):</b><br /> Le magicien invoque un Golem de Feu
		qu'il peut déplacer et faire attaquer via les sorts Boules de Feu et
		Sang de Lave. Il peut le révoquer à tout moment et ne peut pas en
		invoquer plus d'un à la fois. Tant que le Golem reste actif, le
		magicien subit un malus de 10% sur sa Maitrise de la Magie.<br /> <small><?php echo $name[227];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<b>Sortilèges de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Brasier"></a>Brasier (10
			PA, Dex, MM):</b><br /> Brasier est un sort d'attaque de zone. Toutes
		les créatures (y compris le lanceur) à l'intérieur de la zone
		subissent une attaque de feu.<br /> <small><?php echo $name[213];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Piliers Infernaux"></a>Piliers
			Infernaux (9 PA, Dex, MM):</b><br /> Ce sort fait apparaitre 3
		piliers de feu qui iront automatiquement frapper le bâtiment au
		contact du magicien ainsi que deux autres bâtiments adjacents avec le
		premier. <br /> <small><?php echo $name[220];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Sang de Lave"></a>Sang de
			Lave (8 PA, MM):</b><br /> Sang de lave est une attaque à distance
		basée sur une opposition magique pure. En surpassant la Maitrise de la
		magie de votre adversaire, vous lui infligerez une malédiction qui le
		consumera dans la durée.<br /> <small><?php echo $name[214];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
<div class='subtitle'>
	<a name="Altération"></a>Les Sortilèges de l'École d'Altération
</div>
<br />
<b>Sortilèges de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Malédiction d'Arcxos "></a>Malédiction
			d'Arcxos (8 PA, Vit, MM):</b><br /> Malédiction d'Arcxos permet au
		sorcier qui le lance d'infliger à sa cible un malus sur une
		caractéristique principale au choix du magicien pour une courte durée.
		Il permet aussi de diminuer les effets des enchantements positifs.<br />
		<small><?php echo $name[221];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b>Force d'Aether (4 PA, Vit, MM):</b><br />
		Forcé d'Aether est un sortilège qui permet au sorcier de déplacer une
		créature à la force de sa magie et de sa vitesse.<br /> <small><?php echo $name[228];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Projection de l'Ame"></a>Projection
			de l'Ame (5 PA, Vit, MM):</b><br /> Projection de l'âme est un sort
		qui permet au magicien d'observer une zone éloignée.<br /> <small><?php echo $name[225];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<br />
<b>Sortilèges de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Téléportation"></a>Téléportation
			(8 PA, Vit, MM):</b><br /> Ce sort permet au magicien de se déplacer
		instantanément vers la destination de son choix. <br /> <small><?php echo $name[223];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Toucher de Lumière"></a>Toucher
			de Lumière (5 PA, Vit, MM):</b><br /> Ce sort permet au magicien de
		redonner des points d'action à la cible dans la limite maximale des
		PAs. <br /> <small><?php echo $name[224];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Rappel"></a>Rappel (12 PA,
			Vit, MM):</b><br /> Ce sort permet au magicien de rappeler auprès de
		lui un autre personnage. La cible aura le choix d'accepter ou refuser
		le Rappel à son prochain tour. Si elle l'accepte cela lui coutera
		l'intégralité des PAs de sa DLA. <br /> <small><?php echo $name[226];?> Détails </a></small></li>
</ul>
</p>
<br />

<a href="#hdp">haut de page</a>
<br>
<br />
<br />
