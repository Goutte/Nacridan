<?php
require_once ("../../../conf/config.ini.php");
echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 100; $i < 200; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i . "\");return false;' > ";
}
// class='stylepc'

?>

<a name="hdp"></a>
<div class='subtitle'>Introduction</div>
<p class='rule_pg'>
	Les compétences s'apprennent dans les écoles de combat des villes et
	des villages en échange de Pièces d'Or et de Points d'Action.<br /> Une
	fois apprise, vous obtiendrez une nouvelle action disponible dans le
	menu "compétences". Comme toutes les actions, les compétences
	nécessitent un nombre de points d'action qui leur sont propres pour être
	utilisées et influent sur vos jauges de progression spécifiquement.<br />
	<br /> Après apprentissage, vous ne maitrisez la compétence qu'à 50% ce
	qui signifie que vous n'avez qu'une chance sur deux de réussir la
	compétence. Si vous ratez votre jet de réussite, aucune action n'est
	réalisée et vous perdez des PA. <br /> A chaque fois que vous
	réussissez votre compétence, vous avez une chance d'améliorer votre
	pourcentage de maitrise de 1%. Pour améliorer il vous faudra réussir un
	d100 supérieur à votre pourcentage de maîtrise. <small><?php echo $name[199];?>Détails </a></small><br />
</li>
<br />


Il y a 3 niveaux de compétence :
<br />
-Les compétences élémentaires.
<br />
-Les compétences de niveau aspirant.
<br />
-Les compétences de niveau adepte.
<br />
<br />
Au début de votre aventure, vous ne pourrez apprendre que les
compétences élémentaires. Lorsque votre pourcentage de maitrise sera
suffisant vous aurez alors accès aux compétences de niveau supérieur.
<small><?php echo $name[198];?> Détails </a></small>
<br />
<br />

<br />
<br />
<div class='subtitle'>Les Compétences Elémentaires</div>
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Dégâts Accrus"></a>Dégâts
			Accrus : Temps d'Attaque PA </b><br /> Dégâts accrus est une attaque
		au contact qui permet d'augmenter les dégâts infligés à votre cible en
		contrepartie d'un malus sur votre jet d'Attaque. A réserver sur les
		cibles que vous touchez facilement, dégâts accrus est la compétence
		idéale pour viser les records de dégât et fera le bonheur de tous les
		barbares. Son utilisation coûte le même nombre de PA qu'une attaque
		classique soit 7,8 ou 9pa selon le type d'arme que vous portez. Cette
		compétence s'appuie sur la force et la fera progresser davantage
		qu'une attaque classique.<br /> <small><?php echo $name[104];?> Détails </a></small>
	
	<li style="margin-bottom: 20px;"><b><a name="Garde"></a>Garde : 5 PA </b><br />
		Garde est une compétence qui permet d'augmenter votre défense.
		Particulièrement utilisée par les assassins à arme légère qui peuvent
		cumuler cette compétence après avoir porté un coup rapide, elle leur
		permet d'attendre plus sereinement la riposte de leur adversaire. Son
		utilisation fera particulièrement progresser votre vitesse et la
		caractéristique associée à votre bouclier si vous en portez un.<br />
		<small><?php echo $name[102];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Premiers Soins"></a>Premiers
			Soins : 5 PA </b><br /> Cette compétence permet d'apporter les
		premiers soins à autrui ou à vous même. Elle est donc indispensable à
		tout aventurier qui se destine à une carrière solitaire et
		particulièrement appréciée des groupes de chasse dont le guérisseur
		est débordé. Notez toutefois que les soins apportés par cette
		compétence ne permettent pas de guérir complétement la cible. La bonne
		qualité des premiers soins repose uniquement sur la dextérité qui sera
		donc très sollicitée lors de l'utilisation de cette compétence.<br />
		<small><?php echo $name[103];?> Détails </a></small></li>
	<br />
	<li style="margin-bottom: 20px;"><b><a name="Botte d'Estoc"></a>Botte
			d'Estoc : Temps d'attaque PA </b><br /> Botte d'estoc est une attaque
		au contact qui permet d'augmenter les dégâts infligés à votre cible.
		Pour effectuer cette compétence, vous devrez avoir une griffe équipée
		dans votre main gauche (attention, elle sera prise en compte dans
		votre charge de plate). Cette compétence est souvent l'attaque de base
		des maîtres de l'école de l'Ombre. Misant sur la rapidité pour
		infliger un coup supplémentaire avec la griffe, cette compétence vous
		fera davantage progresser en vitesse qu'une attaque classique.<br /> <small><?php echo $name[128];?> Détails </a></small>

	</li>
</ul>
</p>
<br />
<br />
<div class='subtitle'>
	<a name='Garde d_Octobian'></a>Les Compétences de l'École de la Garde
	d'Octobian
</div>
<br />
<b>Compétences de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Attaque-Puissante"></a>Attaque
			Puissante : Temps d'Attaque PA </b><br /> L'Attaque Puissante est une
		attaque au contact qui se base sur la force pour augmenter sa valeur
		d'attaque. Cette compétence indispensable dans l'arsenal de tout
		guerrier polyvalent vous permettra de passer la défense des créatures
		les plus coriaces.<br /> Cette compétence fera progresser davantage
		progresser votre force qu'une attaque normale. <br /> <small><?php echo $name[101];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Tournoiement"></a>Tournoiement
			: Temps d'Attaque +1 PA</b><br /> Tournoiement est une attaque au
		contact qui permet d'attaquer toutes les cibles adjacentes au
		guerrier. Basée sur le positionnement sur le champ de bataille, cette
		compétence peut être ravageuse et infliger des dégâts cumulés énormes.
		Toutefois, les dégâts que vous infligez à chaque cible seront
		inférieurs à ce que vous auriez pu infliger avec une attaque
		classique.<br /> Cette compétence fera davantage progresser votre
		force qu'une attaque normale. <br /> <small><?php echo $name[105];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Attaque-Perçante"></a>Attaque
			perçante : Temps d'Attaque PA</b><br /> L'attaque perçante est une
		attaque au contact qui permet d'ignorer une partie de l'amure de la
		cible et d'infliger des dégâts profonds qui persisteront plusieurs
		tours. Cette compétence se révèlera particulièrement utile face aux
		créatures lourdement armurées et doit faire partie, au même titre que
		l'Attaque Puissante, de l'arsenal d'un guerrier polyvalent.<br />
		Comme les autres compétences de la même école, cette compétence fera
		davantage progresser votre force qu'une attaque normale.<br /> <small><?php echo $name[107];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<b>Compétences de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Coup Assommant"></a>Coup
			Assommant : Temps d'Attaque PA</b><br /> Coup Assommant est une
		attaque au contact qui permet d'étourdir la créature ciblée. Assommer
		une créature pour la rendre davantage vulnérable afin de lui infliger
		encore plus de dégâts au prochain coup, voilà qui va plaire à tous les
		barbares qui se respectent.<br /> Cette compétence fera davantage
		progresser votre force qu'une attaque normale. <br /> <small><?php echo $name[109];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Projection"></a>Projection
			: Temps d'Attaque PA</b><br /> Projection est une attaque au contact
		qui permet, en plus d'infliger des dégâts, de projeter sa cible au
		loin. Trouvez-vous un pote barbare, apprenez cette compétence et jouez
		au ping-pong avec un gobelin pour votre plus grand plaisir.<br />
		Cette compétence fera davantage progresser votre force qu'une attaque
		normale. <br /> <small><?php echo $name[125];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Coup de Grâce"></a>Coup de
			Grâce : Temps d'Attaque PA</b><br /> Coup de Grâce est une attaque au
		contact qui inflige une blessure mortelle. Les dégâts infligés par
		cette compétence ne peuvent être guéris par des moyens normaux. La
		cible devra, avant d'être soignée, recevoir un sortilège de Souffle
		d'Athlan pour éliminer le mal résiduel.<br /> Cette compétence est
		particulièrement appréciée contre les monstres disposant d'une
		autorégénération importante ou contre les groupes de gobelins où le
		shaman prend son rôle un peu trop au sérieux.<br /> Cette compétence
		fera davantage progresser votre force qu'une attaque normale. <br /> <small><?php echo $name[110];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
<div class='subtitle'>
	<a name='Ecole de l_Ombre'></a>Les Compétences de l'École de l'Ombre
</div>
<br />
<b>Compétences de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Aiguise-Lame"></a>
			Aiguise-Lame : 5 PA</b><br /> Aiguise-Lame est une compétence qui vous
		permet d'augmenter les dégâts que procure une arme pour la prochaine
		fois ou vous vous en servez.<br /> Cette compétence est
		particulièrement utile pour les personnages se battant avec une arme
		légère car elle peut être cumulée sur un même tour juste avant une
		attaque. <br> Basé sur la rapidité de son utilisateur, cette
		compétence vous fera progresser en vitesse <br /> <small><?php echo $name[106];?> Détails </a></small>

	</li>
	<li style="margin-bottom: 20px;"><b><a name="Vol"></a>Vol à la Tire : 5
			PA</b><br /> Vol à la tire est une compétence au contact qui permet
		de dérober l'or d'un autre personnage. Cette compétence sera
		particulièrement appréciée de tous ceux qui n'aiment pas tenir une
		pioche ou qui se sont toujours demandé pourquoi affronter un dragon
		pour son trésor alors qu'il est bien plus aisé de dérober le tueur du
		dragon.<br /> Notez toutefois que cette action, bien évidemment mal
		vue en tout lieu, est carrément interdite dans les royaumes de départ
		et les gardes interviendront si vous vous faites remarquer.<br /> <small><?php echo $name[117];?> Détails </a></small>

	</li>
	<li style="margin-bottom: 20px;"><b><a name="Lancer de Bolas"></a>Lancer
			de Bolas : 8 PA</b><br /> Lancer de Bolas est une compétence à
		distance qui permet de renverser son adversaire en lui jetant un bolas
		préalablement équipé en arme. La cible visée tombe alors au sol et ne
		plus ni combattre pleinement ni se déplacer efficacement. <br />
		Parfait pour rattraper les couards en fuite ou pour laisser vos petits
		frères passer à tabac un trop fort pour eux, cette compétence se
		révèlera d'une utilité surprenante<br /> <small><?php echo $name[108];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<b>Compétences de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Désarmement"></a>Désarmer
			: Temps d'Attaque PA</b><br /> Désarmer est une attaque au contact
		qui n'inflige pas de dégâts mais qui a pour but de désarmer son
		adversaire. En cas de succès, l'arme de la victime tombe au sol. Ce
		dernier subit alors le malus de combattre à main nue. Pour les plus
		fourbes, cette compétence pourrait bien avoir pour seul but de dérober
		l'arme de son adversaire.<br /> Cette compétence vous fera davantage
		progresser en vitesse qu'une attaque normale <br /> <small><?php echo $name[111];?> Détails </a></small>
	</li>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Larcin"></a>Larcin : 5PA</b><br />
		Larcin est une attaque à distance qui permet de placer une malédiction
		sur un adversaire. Si ce dernier vient à mourir sous l'emprise de
		cette malédiction alors il sera délesté d'un objet au hasard de son
		inventaire qui tombera au sol.<br /> Cette compétence vous fera
		davantage progresser en vitesse qu'une attaque normale <br /> <small><?php echo $name[123];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Embuscade"></a>Embuscade :
			5 PA</b><br /> Embuscade est une compétence qui permet à celui qui
		l'utilise de devenir invisible aux yeux de ceux qui ne se trouvaient
		pas dans sa vue à ce moment. Un personnage embusqué peut se déplacer
		très doucement tout en restant invisible. Cependant, toute autre
		action de sa part le fera réapparaitre.<br /> <small><?php echo $name[127];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
<div class='subtitle'>
	<a name='Archers de Krima'></a>Les Compétences de l'École des Archers
	de Krima
</div>
<br />
<b>Compétences de Niveau Aspirant</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Tir Lointain"></a>Tir
			Lointain : Temps d'Attaque PA</b><br /> Tir lointain est une
		compétence de tir à l'arc qui permet d'atteindre des cibles plus
		éloignés. Pratique pour rester encore plus à distance des créatures
		dangereuses ou bien pour abattre les fuyards, cette compétence
		deviendra rapidement indispensable car elle offre de plus un malus
		plus petit qu'un tir normal lorsque la cible est proche.<br /> <small><?php echo $name[118];?> Détails </a></small>
		<br /></li>
	<li style="margin-bottom: 20px;"><b><a name="Tir Gênant"></a>Tir Gênant
			: Temps d'Attaque PA</b><br /> Tir gênant est une compétence de tir à
		l'arc qui permet, en plus d'infliger les dégâts d'une attaque normale,
		de blesser son adversaire pour le gêner dans ses déplacements.<br />
		<small><?php echo $name[119];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Flèche de Négation"></a>Flèche
			de Négation : Temps d'Attaque PA</b><br /> Flèche de négation est une
		compétence de tir à l'arc qui permet, en plus d'infliger les dégâts
		d'une attaque normale, d'infliger un malus de maitrise de la magie à
		la cible touchée par la flèche enchantée.<br /> <small><?php echo $name[120];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<b>Compétences de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Volée de Flèches"></a>Volée
			de Flèches : Temps d'Attaque +2 PA</b><br /> Volée de flèches est une
		compétence de tir à l'arc qui permet d'enchainer jusqu'à trois tirs
		successifs, sur trois cibles potentiellement différentes. Attention,
		en plus de la Dextérité de l'archer, la précision d'une volée (et donc
		le total des dégâts infligés) dépend également de sa Vitesse.<br /> <small><?php echo $name[126];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Flèche Enflammée"></a>Flèche
			Enflammée : Temps d'Attaque PA</b><br /> Flèche enflammée est une
		compétence de tire à l'arc qui permet de tirer une flèche enflammée
		sur un bâtiment générant ainsi des dégâts supplémentaires. Cette
		compétence ne peut être utilisée que contre des bâtiments.<br /> <small><?php echo $name[122];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Course Céleste"></a>Course
			Céleste : 3PA</b><br /> Course Céleste est une compétence qui permet
		de réduire le coût des déplacements dans les terrains difficiles comme
		la montagne, la forêt, le désert ou les marécages.<br /> <small><?php echo $name[121];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
<div class='subtitle'>
	<a name='Paladins de Tonak'></a>Les Compétences de l'École des Paladins
	de Tonak
</div>
<i>Pour bien débuter une carrière de Paladin, vous pouvez consulter
	l'onglet <a href='rules.php?page=step15'>Tutoriels</a>.
</i>
<br />
<br />
<br />
<b>Compétences de Niveau Aspirant</b>
</br>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Autorégénération "></a>Auto-Régénération
			: 10 PA</b><br /> Auto-Régénération est une compétence qui permet au
		Paladin de récupérer une grande partie de ses points de vie perdus au
		détriment d'une perte de sa force.<br /> <small><?php echo $name[112];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Appel de la Lumière"></a>Appel
			de la Lumière : Temps d'Attaque PA</b><br /> Appel de la Lumière est
		l'attaque de base du Paladin. Elle permet à celui-ci de porter un coup
		dont la précision dépendra de sa maitrise de la magie et non de sa
		dextérité. et sa cible. De plus, cette compétence octroie un bonus de
		dégât supplémentaire contre les démons et les mort-vivants.<br /> <small><?php echo $name[113];?> Détails </a></small>

	</li>
	<li style="margin-bottom: 20px;"><b><a name="Protection "></a>Protection
			: 8 PA</b><br /> La compétence Protection permet de protéger une
		cible adjacente à sa position. Jusqu'à sa désactivation volontaire ou
		à la mort d'un des personnages, le Paladin s'interposera sur <b>la
			plupart</b> des attaques dirigées contre la cible protégée. En
		s'interposant le Paladin renonce toutefois à se défendre contre les
		coups de l'attaquant mais sa foi lui octroie un bonus magique d'armure
		supplémentaire.<br /> <small><?php echo $name[124];?> Détails </a></small>
	</li>
</ul>
</p>
<br />
<br />
<b>Compétences de Niveau Adepte</b>
<br />
<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b><a name="Aura de Courage"></a>Aura
			de Courage : 5 PA</b><br /> Tant qu'elle reste active, l'Aura de
		Courage confère un bonus d'Attaque à chaque membre d'un Groupe de
		Chasse, en contrepartie duquel le Paladin subit une perte de Points
		de Vie à chaque DLA. Le Paladin peut désactiver l'Aura de Courage
		gratuitement et à tout moment.<br /> <small><?php echo $name[114];?> Détails </a></small>
	</li>
	<li style="margin-bottom: 20px;"><b><a name="Aura de Résistance"></a>Aura
			de Résistance : 5 PA</b><br /> Tant qu'elle reste active, l'Aura de
		Résistance confère un bonus d'Armure à chaque membre d'un Groupe de
		Chasse, en contrepartie duquel le Paladin subit un malus de Force. Le
		Paladin peut désactiver l'Aura de Résistance gratuitement et à tout
		moment.<br /> <small><?php echo $name[115];?> Détails </a></small></li>
	<li style="margin-bottom: 20px;"><b><a name="Exorcisme de l'Ombre"></a>Exorcisme
			de l'Ombre : 8 PA</b><br /> En effectuant un Exorcisme de l'Ombre, le
		Paladin renvoie à ses agresseurs encore en vue une partie des dégâts <b>magiques</b>
		que ceux-ci lui ont infligé au cours des 3 dernières DLA.<br /> <small><?php echo $name[116];?> Détails </a></small>
	</li>
</ul>
</p>
<br />

<a href="#hdp">haut de page</a>
<br>
<br />
<br />
