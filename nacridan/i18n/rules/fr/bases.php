<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 1; $i < 100; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i . "\");return false;' > ";
}
?>

<div class='subtitle'>Nacridan, qu'est-ce-donc ?</div>

<p class='rule_pg'>
	Nacridan est un <b>MMORPG médiéval-fantastique au tour par tour sur
		navigateur</b>. C'est donc un jeu de rôle dans un univers persistant
	ressemblant de loin à celui de J.R.R. Tolkien. On peut y incarner un ou
	une représentant(e) de différentes races : <b>Humains</b>, <b>Elfes</b>,
	<b>Nains</b> ou <b>Doranes</b>, ces derniers étant des êtres doués pour
	la magie, propres à Nacridan. Dans cet univers, les habitants font face
	à une invasion de monstres qui arrivent via des portails magiques. La
	plupart des joueurs incarnent donc des chasseurs de monstres qui
	luttent sans trêve contre cette invasion. Si vous n'avez pas la guerre
	dans le sang, vous pourrez également trouver votre bonheur :
	guérisseur, enchanteur, artisan, caravanier, voleur... à vous de
	choisir votre voie !<br> Le but étant bien sûr d'apprécier les quelques
	minutes de dépaysement quotidien que vous offre Nacridan. ;-)
</p>

<p class='rule_pg'>Nacridan est également un projet de développement web
	amateur, géré par une association loi 1901. Tous les membres de
	l'équipe sont des bénévoles, certes passionnés, mais qui prennent sur
	leur temps libre pour travailler sur le jeu, en parallèle de leur vie
	de famille, de leur boulot irl, etc. Soyez indulgents, et si ça vous
	dit, n'hésitez pas à passer dans le forum de développement pour les
	aider en contribuant au projet.</p>
<br>
<br>

<div class='subtitle'>Création d'un compte.</div>
<p class='rule_pg'>
	<b>1. Cliquez sur « Inscription » :</b></br> Une seule inscription par
	personne est autorisée. Jouer deux personnages à partir de deux comptes
	différents s'appelle couramment du "multicomptes" et il sera sanctionné
	par une désactivation des deux comptes.</br> </br> <b>2. Choisissez
		votre pseudo :</b></br> Lâchez-vous si vous le voulez, ceci n'est pas
	le nom de votre avatar mais bien celui grâce auquel vous serez reconnu
	en tant que joueur, pour accéder à votre compte dans le jeu et sur le <a
		href='http://www.nacridan.com/forum/index.php'>forum</a>.</br> </br>
	<b>3. Renseignez les informations de base :</b></br> E-mail, date de
	naissance, zone horaire n'ont pour but que de configurer le système au
	mieux : aucune information n'est vendue ou échangée et nous en
	profitons pour rappeler que <u>Nacridan n'a pas de but lucratif</u> !</br>
	</br> <b>4. Lisez la charte avant de l'accepter :</b></br> Franchement,
	elle n'est pas très longue ! Si vous ne lisez que les débuts de phrase
	et comprenez à demi-mot, les 15 règles seront survolées en moins d'une
	minute (ou 2 minutes si vous lisez toutes les phrases finalement !).</br>
	</br> <b>5. Confirmez l'inscription :</b></br> Votre inscription devra
	simplement être validée en cliquant sur le lien qui vous sera envoyé
	par e-mail et vous pourrez alors passer à l'étape de création de votre
	personnage.</br>
</p>
<br />


<div class='subtitle'>Création d'un personnage.</div>
<p class='rule_pg'>

	Au début de l'aventure votre personnage possède 40 Points de Vie (PV)
	et 3D6 dans chacune des 4 caractéristiques de base : la force, la
	dextérité, la vitesse et la maitrise de la magie. Le bonus racial vous
	donnera 1D6 supplémentaire dans l'une de ces caractéristiques. <br /> <br />
	<b>1. Choisissez le nom :</b></br> Même si vous êtes un "Power-Gamer"
	ce qui est tout à fait honorable, rappelez-vous toutefois qu'il s'agit
	d'un jeu de rôle. Adaptez le nom de votre personnage avec l'univers
	médiéval fantastique de Nacridan : évitez les Merg'Hez , Bourinator et
	autre Kevin93 (même si nous n'avons bien évidemment aucune animosité
	envers les Kevin ou la Seine-St-Denis !). Préférez si possible un nom
	qui n'aurait pas de signification particulière ou déjà issu d'un
	univers médiéval-fantastique très connu.</br> </br> <b>2. Choisissez la
		race :</b></br> Les nains démarrent l'aventure avec 1D6 de force
	supplémentaire, les humains avec 1D6 de dextérité supplémentaire, les
	elfes avec 1D6 de vitesse supplémentaire et les doranes avec 1D6 de MM
	supplémentaire. Certes le dé supplémentaire est important dans les
	premiers niveaux mais pensez également qu'il sera rapidement noyé dans
	les multiples améliorations que votre personnage obtiendra au cours de
	l'aventure, c'est pourquoi choisissez également la race pour l'affinité
	que vous avez avec celle-ci et pour le RP qu'elle amène.</br> </br> <b>3.
		Choisissez le sexe :</b></br> Alors là, pour le coup, c'est purement
	RP (Role Play)... Par contre, si vous choisissez le sexe opposé au
	vôtre, faîtes attention à jouer le jeu jusqu'au bout durant vos
	interactions avec les autres joueurs ! Ça peut même en aider certains à
	mieux distinguer "leur personne" de "leur personnage".</br> </br> <b>4.
		Choisissez l'arme de départ :</b></br> Si vous vous destinez à devenir
	mage le plus rapidement possible, ce choix importe peu car vous allez
	surement revendre cette arme rapidement pour acheter un sort. Sinon,
	sans rentrer dans les détails, disons simplement que chaque arme
	privilégie une caractéristique lors de l'attaque. La dague n'offre pas
	de bonus de dégât mais permet de porter des coups rapides. Elle
	s'appuie sur la vitesse. A l'opposé, la masse offre un bon bonus de
	dégât mais frapper avec demande plus de temps. Elle s'appuie sur la
	force. Enfin l'épée longue est un compromis entre les deux et s'appuie
	sur la dextérité.</br> </br> <b>5. Choisissez le lieu de départ :</b></br>
	Les différentes possibilités sont présentées sur cette carte : en bleu
	les villages, en rouge les bourgs, avec en plus les capitales des trois
	royaumes correspondant : Tonak, Artasse et Earok. <br /> <br /> <img
		style="float: center; margin-left: 5px"
		src="<?echo CONFIG_HOST;?>/pics/rules/mapstart.jpg" alt="" /> <br /> <br />Les
	villes principales (Earok, Artasse et Tonak) sont au cœur des royaumes
	: elles sont parfaitement achalandées (tous les bâtiments sont
	disponibles et de haut niveau) et les différentes ressources se
	trouvent à proximité tout autour. Par contre, elles pullulent de monde
	et ne représentent pas la meilleure opportunité pour chasser
	rapidement. Les bourgs et les villages sont tous situés en bordure de
	zone "Débutant", permettant un accès rapide aux zones de chasse d'un
	niveau supérieur. Les villages ne contiennent que le strict nécessaire
	: un temple, une auberge, une échoppe et un comptoir. Dans les bourgs,
	vous aurez accès en plus à une guilde des artisans et une école des
	métiers.</br> Les royaumes protégent ceux qui y vivent. En effet un
	joueur attaquant et/ou tuant un personnage en royaume sera arrêté par
	les garde et mené en prison. L'argent perdu sera déposé par les gardes
	sur le compte bancaire de la victime. Il en est de même pour le vol si
	il est découvert. <br />
</p>

</p>
<br />
<br>

<div class='subtitle'>Jouer vos deux premiers Tours de Jeu</div>
<p class='rule_pg'>
	Quand vous arrivez dans le jeu, vous avez 12 Points d'Action (PA).
	Exceptionnellement, votre prochain tour de jeu a été placé 20 min plus
	tard, ce qui signifie que vous aurez à nouveau 12PA à dépenser dans
	20min. L'heure de votre Date Limite d'Action (DLA) qui marque le début
	du nouveau tour est inscrite en bas du menu de droite. <br /> <br />Pour
	rentabiliser au mieux vos premiers PA, nous vous proposons des conseils
	sur 2 départs différents selon l'orientation que vous voulez donner à
	votre personnage. <br /> <br /> <b>Première action : Sortir du temple </b>
	(0,5pa)<br /> <br /> Vous apparaissez dans un temple, vous pouvez en
	visiter les différentes pièces sans utiliser de PA (hôpital, temple de
	téléportation, chapelle...) mais ne rentrez pas dans la chapelle. En
	entrant dans l'accueil du bâtiment vous pouvez accéder à une petite vue
	limitée à 2 cases et qui vous permet de sortir du bâtiment.</br>
<div>
	<img style="float: center;"
		src="http://www.nacridan.com/pics/rules/tutogoout.jpg" alt="" />
</div>
<br>
<p class='rule_pg'>
	C'est seulement lorsque vous sortez que votre avatar vous apparait
	enfin au centre de la vue. <br> </br>
</p>

<p class='rule_pg'>
	Ensuite, vous avez principalement deux possibilités : <br /> - Vous
	vous destinez à devenir un guerrier et vous souhaitez entrer dans le
	vif du sujet et occire vos premiers monstres.<br /> - Vous voulez
	devenir un mage. Pour cela, il va falloir que vous gagnez un peu d'or
	en vue d'acheter votre premier sort<br> <br> <b>Choix 1 : Tuer son
		premier monstre </b><br /> <br />
<ul>
	<li style="margin-bottom: 20px;"><b>Sortir de la ville </b>(2 à 3pa) :
		Déplacez-vous hors de la ville en cliquant sur les cases adjacentes à
		votre personnage puis en validant. Si vous êtes dans un bourg ou un
		village, il n'y a pas de muraille et vous percevez déjà l'extérieur de
		la ville. Si vous êtes dans une grande ville, il vous faudra dépasser
		les murailles avant de voir l'extérieur. Notez que vous pouvez sortir
		par les murs.
	
	<li style="margin-bottom: 20px;"><b>Repérer un monstre </b>(2 à 3pa) :
		avec un peu de chance vous distinguez déjà un monstre : crapaud géant,
		rat géant ou chauve-souris géante. Approchez-vous du monstre jusqu'à
		être sur une case adjacente. Si aucun monstre n'est aux alentours, un
		portail démoniaque ne tardera pas à apparaitre et des monstres en
		sortiront mais cela peut prendre plus d'une demi-journée.
	
	<li style="margin-bottom: 20px;"><b>Équipez votre arme </b>(2pa) : menu
		"Objet" "s'équiper" puis sélectionnez l'arme que vous avez choisie
		lors de la création. <br /> <br /> <i>Vous n'avez surement plus assez
			de PA pour porter une attaque. Il va falloir attendre votre nouveau
			tour de jeu dont nous parlions au début. Profitez-en pour prendre
			connaissance de l'interface : l'équipement, le profil, les options,
			la carte, votre compte et même la messagerie. Si vous avez encore du
			temps, lisez le premier paragraphe "Attaque au contact" de la page
			"Combat" des règles. Ceci vous permettra de comprendre un peu mieux
			les nombres de la première attaque que vous allez effectuer. </i><br />
		<br />
	
	<li style="margin-bottom: 20px;"><b> Activer son tour </b> : lorsque
		l'heure de votre DLA est dépassée vous devez vous déconnecter et vous
		reconnecter pour obtenir à nouveau vos PA.
	
	<li style="margin-bottom: 20px;"><b>Frapper le monstre </b>(7 à 9pa) :
		A moins d'être vraiment malchanceux, vous allez toucher votre première
		cible et avec de la chance vous pourriez même la tuer. Dans ce cas,
		elle aura laissé tomber un objet, pensez à le ramasser car vous
		pourrez en tirer 3po en le revendant à l'échoppe. Si la créature n'est
		pas morte, vous pourrez voir son état en cliquant dessus, elle sera
		probablement blessée ou pire. <br /> <br /> <i>Et voilà, vous devez
			maintenant attendre un nouveau tour pour frapper à nouveau.<br />La
			chasse permet de gagner tout juste les pièces d'or (PO) nécessaire à
			vos soins au temple et à l'entretien du matériel. Si vous avez besoin
			de gagner plus d'argent, penchez-vous alors sur les solutions
			décrites plus bas.
	</i>

</ul>
<br />

<p class='rule_pg'>
	<b>Choix 2 : Économisez pour acheter un sort, une compétence ou un
		équipement</b><br> <br> <i> Lorsque vous commencez l'aventure, vous
		disposez de 40 pièces d'or (PO). En vendant votre arme et les deux
		potions que vous possédez à l'échoppe de la ville, vous pouvez gagner
		32PO. Les sortilèges élémentaires coûtent 80PO dans les écoles de
		magie des grandes cités. Vous devez donc gagner un peu d'argent. </i>
<ul>
	<li style="margin-bottom: 20px;"><b> Faire une mission d'escorte </b> :
		c'est un moyen rapide d'obtenir la somme d'argent qui vous permettra
		d'acheter votre premier sort ou votre première compétence. <small><?php echo $name[1];?> Détails </a></small>
	
	<li style="margin-bottom: 20px;"><b> Apprendre un savoir-faire
			d'extraction </b> : c'est un petit investissement qui vous permettra
		d'apprendre à récolter les matières premières aux alentours de la
		ville. Leur revente à l'échoppe vous permettra de gagner quelques
		pièces d'or. <small><?php echo $name[2];?> Détails </a></small>

		</p> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
		<br /> <br /> <br /> <br /> <br /> <br />