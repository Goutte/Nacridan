<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 0; $i < 100; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i .
         "\");return false;' > ";
}
// class='stylepc'

?>

<img style="float: right;" src="<?

echo CONFIG_HOST;
?>
/pics/rules/warrior.jpg" alt="" />
<div class='subtitle'>Gagner des niveaux</div>
<p class='rule_pg'>
	Au début de votre aventure, votre personnage est de niveau 1. A force
	de combattre, de réussir des sortilèges ou des compétences votre
	personnage va franchir des niveaux. <br /> <br /> Pour atteindre un
	niveau supérieur, il faut gagner un certain nombre de points
	d'expérience (PX ou XP) spécifique au niveau actuel de votre
	personnage. Au début de votre aventure, vous avez besoin de 30 points
	d'expérience pour atteindre le niveau 2. Ce nombre augmentera au fur à
	mesure des niveaux. <small><?php echo $name[15];?>Détails </a></small></br>
	<br /> A chaque fois que votre personnage atteint le nombre de points
	d'expérience requis pour franchir un niveau, un bouton "niveau"
	apparait dans le menu d'action à gauche. Vous ne pouvez plus faire
	d'action tant que vous n'avez pas accepter le franchissement de niveau
	en cliquant sur le bouton.
</p>

<br />
<div class='subtitle'>Gagner et perdre des points d'expérience</div>
<p class='rule_pg'>
	Certaines actions, comme une compétence réussie, un sortilège ou un
	savoir-faire d'artisanat, apportent 1 point d'expérience à votre
	personnage. Le fait d'infliger des dégâts à une créature apporte
	également 1 point d'expérience.<br /> Une compétence d'attaque
	infligeant des dégâts à un ennemi peut donc vous faire gagner 2 points
	d'expérience, le premier pour avoir réussi votre compétence et le
	deuxième pour avoir touché votre adversaire <br /> <br /> Le gain le
	plus important de PX reste toutefois lorsque vous parvenez à tuer une
	créature. Dans ce cas, en plus de percevoir les PX liée à votre action
	(1PX pour avoir infligé des dégâts plus, éventuellement 1PX pour le
	sortilège ou la compétence) vous obtenez un nombre de points
	d'expérience dépendant du niveau de votre personnage et du niveau de la
	créature tuée. Ce nombre est donné par la formule suivante : <b> Gain
		PX = Max(Na, 2Na - N + 5)</b> Ou Na est le niveau de l'adversaire et N
	le niveau de votre personnage. <br />
</p>
<br />
<div class='subtitle'>Passage de niveau</div>
<p class='rule_pg'>Dans nacridan, nous avons choisi un système
	d'évolution de personnage du type "Deviens ce que tu fais". Ceci
	signifie qu'au franchissement de niveau, vous n'allez pas décider
	vous-même, comme dans la plupart des jeux, les caractéristiques que
	vous souhaitez voir augmenter chez votre personnage. L'amélioration est
	automatique et dépend des actions que vous avez réalisées pour gagner
	les points d'expérience nécessaires au franchissement de niveau.
	Quelles que soit vos actions, quand il franchit un niveau votre
	personnage gagne trois points de vie (PV), ensuite suivant vos actions,
	Vous recevez 2 améliorations parmi les possibilités suivantes : 15
	points de vie, 1D6 de dextérité, 1D6 de force, 1D6 de vitesse, 1D6 de
	MM. Il est à noter que chaque tranche de 10D de vitesse atteinte vous
	permet de gagner 1pa par tour dans la limite de 18pa(soit 12pa pour
	moins de 10D de vitesse, 13pa pour moins de 20D, 14pa jusque 29D, etc).</p>
<br />
<div class='subtitle'>Les jauges de progression</div>
<p class='rule_pg'>
	Afin de définir quelles sont les caractéristiques qui vont augmenter
	lors d'un passage de niveau, votre personnage possède cinq jauges de
	progression, chacune associée à une caractéristique de base. A chaque
	fois que vous effectuez une action, les jauges s'incrémentent
	spécifiquement. Plus une jauge est haute et plus vous avez sollicité,
	par vos actions, la caractéristique associée à cette jauge. Lors d'un
	passage de niveau, les caractéristiques associées aux deux jauges les
	plus élevée progressent. Il est possible de réajuster ses jauges de
	progression jusqu'au niveau 5 dans la <a href="rules.php?page=step12">salle
		des enchantements des guildes d'artisans</a>. <br /> <br /> Exceptions
	:<br /> a) Si la deuxième jauge la plus élevée est inférieure à la
	moitié de la plus élevée alors le personnage gagne 2D6 dans la
	caractéristique de la jauge la plus remplie. Cette dernière est remise
	à 0.<br /> b) Si la jauge la plus élevée est inférieure à 3 fois le
	niveau du personnage alors le passage de niveau est annulé. Le but est
	d'éviter qu'un personnage puisse gagner un niveau sans rien faire en
	squattant un groupe de chasse.. <br /> <br /> Une fois les
	améliorations reçues, la jauge la plus élevée diminue de la valeur de
	la deuxième jauge la plus élevée, cette dernière est remise à 0. Les
	autres jauges restent inchangées.

</p>
<br />
<div class='subtitle'>La mort</div>
<p class='rule_pg'>
	Au cours de votre aventure, il se peut que votre personnage soit tué.
	Cela ne constitue pas la fin du jeu, votre personnage est immédiatement
	ressuscité avec tous ses points de vie dans la chapelle du temple que
	vous avez désigné au début du jeu ou au cours de votre aventure. <br />
	<br /> Cependant, la mort entraine une perte de la moitié des PX que
	vous possédez et de 90% de votre or. Si vous êtes tué par un PJ dans un
	royaume où le meurtre est interdit, le garde intervenu pour arrêter
	votre assassin récupérera votre or et vous le transférera sur votre
	compte en banque, autrement votre or reste au sol. <br /> <br /> La
	chapelle est un lieu saint où vous êtes protégé de tout danger.
	Malheureusement entrer dans ce lieu affaiblit considérablement votre
	personnage pour une petite période. Après avoir été tué, vous
	personnage subira donc, pour le tour en cours et les deux prochains à
	compter de votre sortie de la chapelle, un malus de 40% sur les 4
	caractéristiques de base.
<p />
.
<br />


<div class='subtitle'>Répartition des PX dans un groupe de chasse</div>
<p class='rule_pg'>
	Lorsque vous appartenez à un Groupe de Chasse (GDC), les PX gagnés
	lorsqu'une créature est tuée par un membre du groupe sont partagés
	équitablement entre tous les membres du groupe situés à au plus 8 cases
	de celui qui a tué la cible. Si le reste de la division euclidienne du
	nombre de px par le nombre de membre n'est pas nulle, certains membres
	du groupe reçoivent aléatoirement 1px supplémentaire.
	Notez que les px gagnés par l'action de l'attaquant (PX pour la
	compétence, pour les dégâts infligés...) ne sont pas partagés. <br /> <br>
</p>







<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
