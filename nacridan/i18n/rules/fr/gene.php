<div class='subtitle'>F.A.Q.</div>
<p class='rule_pg'>
	Cette FAQ n'a pas la vocation de remplacer les règles, mais plutôt
	d'attirer l'attention sur des points qui ne sont pas évidents pour les
	nouveaux, en renvoyant sur les sections correspondantes des règles pour
	plus de détails. Vous pouvez contribuer à la rédaction de cette FAQ en
	postant vos remarques dans <a
		href="http://www.nacridan.com/forum/viewtopic.php?id=328">cette
		partie</a> du forum.
</p>

<p class='rule_pg'>
	<b>Les Premiers Pas :</b><br> - Pensez à équiper votre arme avant votre
	premier combat.<br> <br> <b>Caravanes :</b><br> - Pour faciliter le
	calcul des marchandises dont vous comptez remplir votre tortue, le prix
	des objets est indiqué dans les règles, section "Equipement".<br> -
	Lors d'une erreur à l'achat des marchandises, vous revenez à la page
	initiale. La destination préalablement retenue n'étant pas sauvegardée,
	pensez à la sélectionner à nouveau avant de poursuivre !<br> <br> <b>Niveaux
		des Objets à équiper :</b><br> - Attention, un personnage de bas
	niveau ne peut pas équiper d'objet de haut niveau car les limitations
	d'équipement/surcharge prennent en compte les niveaux de ces objets !
	Consultez les règles pour éviter toute déconvenue !<br> <br> <b>Ramassage
		d'objet :</b><br> - L'action "ramasser un objet" fonctionne si l'objet
	est situé sur votre case ou sur l'une des cases adjacentes à votre
	personnage (sauf si cette case est occupée). <br><br><b>Calculs des
		Attaques/défenses :</b><br> Les jets d'attaque et de défense dépendant
	du type d'arme et d'armure que vous avez équipés. Les détails sont dans
	la page des règles "Combats".<br> <br> <b>Mission d'escorte :</b><br> -
	C'est à vous de faire agir votre PNJ attitré pour la mission d'escorte.
	Pour cela il suffit de le sélectionner grâce au menu déroulant en haut
	à droite de l'écran (ou apparait le nom de votre avatar) et de
	sélectionner le PNJ. <br> - Vous pouvez le faire bouger mais également
	frapper ! Par contre il ne peut pas intégrer de Groupe de Chasse. Et
	vous en pouvez pas gagner de PX lorsqu'il achève une proie.<br>



</p>

<div class='subtitle'>Les classes</div>
<p class='rule_pg'>
	On appelle classe d'un personnage, le style de jeu adopté par un joueur
	pour "combattre" les ennemis, c'est à dire un regroupement de
	possibilités d'actions qui le caractérise, qui lui permet de tuer des
	monstres ou qui lui donne une utilité dans un groupe de chasse.
	Certaines classes sont bien connues et on les retrouve dans beaucoup de
	jeux : le barbare, l'archer, le druide, le paladin ou encore le voleur
	ou le mage de guerre. <br /> <br />Dans Nacridan, les classes ne sont
	pas imposées dès le début lors de la création du personnage. En fait,
	il n'y pas vraiment de classe figée, c'est vous, par vos choix
	d'apprentissage des différentes compétences et sortilèges qui allez
	donner à votre personnage une orientation vis à vis du combat. <br /> <br />
	Cependant, les écoles de magie et de combat induisent différentes
	classes que votre personnage peut jouer. Vous retrouvez donc 4 classes
	de guerrier et 4 classes de mage. Il y a évidemment plusieurs variantes
	de style à l'intérieur d'une même école. <br /> <br /> Il est tout à
	fait possible de choisir des sortilèges et des compétences dans des
	écoles différentes. Toutefois, toutes les compétences ou les sorts
	d'une même école sont cohérents pour l'évolution de votre personnage,
	dans le sens où ils s'appuient et font progresser les mêmes
	caractéristiques. Il est difficile de faire progresser toutes les
	caractéristiques de base de votre personnage. Si vous le faites, votre
	personnage sera évidemment polyvalent mais il ne sera pas suffisant
	efficace par manque de spécialisation.<br /> <br /> La plupart des
	écoles s'appuient sur deux caractéristiques et il est recommandé de se
	concentrer principalement sur deux caractéristiques, c'est à dire sur
	des sortilèges ou des compétences qui s'appuient sur les deux mêmes
	caractéristiques car dans Nacridan, ce n'est pas vous qui décidez de
	vos améliorations lors d'un passage de niveau, elles sont
	automatiquement déterminées par les actions qui vous ont amené à ce
	franchissement de niveau. <br /> <br /> Prenez garde donc, si vous vous
	lancez dans l'apprentissage de sortilèges ou de compétences d'écoles
	différentes, à garder une certaine cohérence dans l'évolution des
	caractéristiques de votre personnage au risque, sinon, d'obtenir un
	personnage bon à rien.<br /> <br /> <br />


</p>


<div id="comtools" class='subtitle'>Les outils de communication</div>
<p class='rule_pg'>
	Vous avez à votre disposition :<br> - Un forum, vous n'avez pas à vous
	inscrire. Il vous suffit de cliquer sur le panneau forum en bas à
	gauche dans le jeu, une fois connecté.<br> - Une messagerie interne,
	accessible une fois connecté dans le jeu, en bas à droite. Notez qu'il
	n'existe pas de messagerie interne propre au forum.<br> - Une tribune,
	accessible via le panneau tribune dans le jeu seulement si vous faites
	partie d'un groupe de chasse.<br> - Un chat IRC accessible <a
		href="http://webchat.quakenet.org/">là-bas</a>. Vous n'avez pas besoin
	d'installer un client IRC. Il suffit de choisir votre pseudo (similaire
	à celui employé sur Nacridan) et le canal (nacridan).
</p>


<div class='subtitle'>Quelques points à garder en tête</div>
<p class='rule_pg'>
<table style='margin-left: 20px;'>
	<tr>
		<td width="250px">1 - <b>Pas de Regen !</b></td>
		<td rowspan=2>Hormis la résurrection, il n'est pas possible de
			regagner des points de vie de manière automatique, que ce soit à
			l'activation de son tour de jeu ou lors d'un passage de niveau. Vous
			pourrez vous faire soigner dans les temples ou dormir dans les
			chambres des auberges et maisons. Mais tout ceci coutera quelques
			pièces d'or. Les potions de vie sont également relativement chères.
			Donc les compétences et sortilèges de soin sont particulièrement
			importants. Par exemple, un guerrier solitaire aura besoin de la
			compétence Premiers Soins.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>2 - <b>Lâcher-Prise !</b></td>
		<td rowspan=2>Les objets utilisés s'usent, et peuvent également être
			volés ou détruits avec les compétences/sortilèges adéquats. En
			conséquence la notion de propriété est beaucoup moins définitive que
			dans d'autres jeux (incluant la V1). Sans compter que l'entretien
			coûte des pièces d'or en fonction du niveau des équipements. Un
			équipement de très bonne qualité coutera d'autant plus cher à
			entretenir.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>3 - <b>L'Or... nerf de la guerre.</b></td>
		<td rowspan=2>Les points précédents mettent en valeur l'importance de
			l'or. Sans or, même un guerrier surpuissant finira sans équipement.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>4 - <b>Soyons réalistes.</b></td>
		<td rowspan=2>Le jeu est conçu pour être le plus cohérent possible, à
			tous les niveaux. Cela inclut le fait qu'un guerrier qui se bat à
			mains nues aura de grandes difficultés contre un personnage de plus
			bas niveau mais armé. Plus de détails sur ce point dans la section <a
			href='rules.php?page=step6'>Expérience</a>.
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>5 - <b>Enjoy ! ;-)</b></td>
		<td rowspan=2>C'est un jeu ! Restez dans le cadre du Role-Play, et
			appréciez pleinement les quelques minutes quotidiennes de dépaysement
			(ou plus ^^).</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>

</table>
<br />
<br />


</p>


<div id="Savoir-vivre" class='subtitle'>Savoir-vivre</div>
<p class='rule_pg'>
	<b>Nacridan est un jeu où les personnages ont la possibilité de jouer des rôles de « méchants ».</b><br> 
	Les agressions, le vol, les menaces, le meurtre et l’attaque de villages existent car cela est voulu par ses créateurs.<br>
    Il s’agit évidemment de la vie imaginaire d’avatars, en aucun cas des vraies relations entre les joueurs !<br><br>
    Nous précisons donc qu’il n’est pas permis de faire tout et n’importe quoi, sous prétexte que chaque situation n’est pas 
	explicitement régulée au travers des règles (ce qui est totalement impossible).<br><br>
    Le <b>« savoir-vivre »</b> consiste à vivre pleinement son « RP » tout en considérant que le côté « bourrin, méchant, cruel, 
	fou furieux, malhonnête,… » de son avatar peut se jouer dans le <b>respect du joueur d’en face</b>. Celui-ci peut certes accepter certains aléas,
	mais ne doit en aucun cas se retrouver « bloqué » ou dans l’impossibilité de jouer son propre style de jeu.<br> <br>
	Bien entendu, un message (MP) justifiant ses actes dans le cadre d’une histoire, d’un RP un minimum construit 
	(et pourquoi pas avec un peu d’humour) sera toujours une bonne idée pour accompagner une action « agressive » !!<br><br>
    Un style de jeu : « <b>Tueur en série sourd et muet </b>» sans aucun discernement, sans RP et qui « chasse » les PJs comme des PNJs sans 
	jamais donner de justifications, dans le seul but de gagner des PXs » <b>n’est pas admis sur Nacridan</b> !<br><br>
    Le cadre de jeu (texte en page d'accueil) + les Règles + un RP un peu construit, les 3 étant indissociables, doivent permettre de vivre 
	une belle aventure où chacun peut trouver sa place, conscient de ce à quoi il joue.<br><br>
    Toutefois, il faut également considérer que chaque joueur percevra un RP avec sa sensibilité personnelle et s’investira dans le jeu à sa façon 
	et avec le temps dont il dispose IRL. Il faut donc rester vigilant !<br><br>
    <b>Quelques exemples liés au PvP :</b><br>
    - "Mon personnage est un dangereux meurtrier un peu psychopathe qui a tendance à buter tous ceux qu’il croise". <br>Ne pas pousser le RP jusqu’à tuer 
	systématiquement tous les petits nouveaux qui débarquent fait partie du bon sens. A moins de vouloir la fin rapide de Nacridan faute de joueurs !<br><br>
    - "Tel personnage à une dette (argent, matériels, trahison, etc.) envers mon personnage". <br>Tenter de se venger, de récupérer son bien,etc.  est tout à fait 
	normal et y il a sans doute du meurtre dans l’air. Accepter l’idée qu’UN meurtre, UNE vengeance bien organisée est suffisant, fait aussi partie du bon sens. 
	A moins de vouloir faire arrêter un joueur, tuer 17 fois son personnage est complètement inutile !<br><br>
    - "Mon personnage est voleur professionnel". <br>Le bon sens, c’est de noter dans un coin de sa tête qui j’ai déjà volé dernièrement pour faire varier les cibles
	le plus largement possible. Surtout si les sommes sont importantes. Voler régulièrement le même personnage ne saurait que dégouter bêtement le joueur qui est derrière !<br><br>
    Il est clair que pour que tous les styles de jeu cohabitent, le but du jeu n’est en aucun cas de « détruire pour détruire » ou de « tuer pour tuer » ! Il existe 
	mille façons de rendre les conflits intéressants et de permettre à tous (vainqueurs comme vaincus) de continuer à jouer dans de bonnes conditions ! <br><br>
	<b>Les « mauvais coups » basés sur l’interaction</b>, avec des propositions « d’arrangements » constructives (Rançons, revente de matos volé, négociations 
	concernant des territoires ou des zones d’influence, échanges de bons procédés, petits arrangements secrets, …) <b>seront toujours plébiscitées</b>. <br>
	Les actions purement destinées à « casser les jouets du voisin » sans aucunes justifications, sans un contexte cohérent, ne peuvent apporter que des conflits 
	de joueurs et des disputes inutiles !<br><br>
    <b>Abus, anti-jeu, acharnement</b><br>
    Commençons par un petit rappel : que faire si vous subissez une situation que vous trouvez abusive ?<br>
    1) commencez par vous calmer si besoin<br>
    2) relisez cette section des règles pour situer votre cas.<br>
    3) contactez votre agresseur avec une grosse balise HRP (hors role-play) dans le titre du message pour lui présenter les faits sous votre point de vue.<br> 
	Par exemple s'il vous a tué deux fois de suite sans justification, rappelez les faits, et expliquez lui que ce n'est pas agréable, et tant qu'à faire vous pouvez 
	glisser un lien vers cette section des règles.<br>
    4) Si cela ne résout rien, envoyez un MP à Nacridan ou à Modérateur1. Ne postez pas d'accusation publique dans le forum !<br><br>
    Ici nous définirons l'<b>acharnement</b> comme une même <b>action négative effectuée à deux reprises sur le même personnage dans un intervalle de temps déraisonnablement 
	court</b> sans que la victime ne l'ait implicitement ou explicitement acceptée.<br><br>
	Le délai mentionné ici peut varier selon les situations, si vous avez un doute, demandez à l'équipe en exposant votre cas. <br><br>
	En temps normal, quand la victime n'a vraiment 	rien fait pour mériter le traitement, à part "être là" il faut envisager un long délai, 
	typiquement au moins deux mois. Cela laisse le temps à la victime de changer de zone 
	complètement ou bien de monter une expédition contre son agresseur. En fait, plus ce délai est long, mieux c'est. C'est la responsabilité de l'agresseur de trouver d'autres cibles. 
	La communauté est petite, mais les cibles ne manquent pas.<br><br>
    <B>Quelques exemples d'acceptations implicites :</b><br>
    - Vous faites parti d'un Ordre qui a des ennemis. Ces ennemis peuvent bien-sûr défendre leur territoire ou attaquer le votre, et ce genre d'opération conduit souvent à mettre 
	les personnages en danger de façon répétée.<br><br>
    - Vous êtes caravanier et vous vous obstinez à prendre des risques en passant régulièrement dans une zone contrôlée par des brigands.<br><br>
    - Vous êtes brigand et vous vous obstinez à attaquer des caravanes dans une zone bien défendue.<br>
    <br>Notez l'importance de la notion de territoire contrôlé dans ces exemples. L'île est vaste, il y aura toujours largement de la place, quitte à ouvrir d'autre cartes. <br>
	La prochaine carte est déjà en réserve. Donc vous êtes responsable du choix de la zone où vous vous trouvez, dès que vous atteignez un niveau suffisant. <br>
	Vous devez donc accepter les dangers inhérents à votre choix de zone, y compris les dangers venant du PvP.<br>
    <br>De même, si vous êtes en territoire ennemi, et que celui-ci vous en sort manu militari trois fois de suite sans que votre invasion ne progresse quantitativement, 
	alors le fair-play demande que vous lui concédiez la zone plusieurs semaines.<br>
    Bien-sûr tout accord RP ou HRP négocié avec le joueur au commande de l'ennemi a préséance sur ces règles. S'il y a un accord, il n'y a pas d'acharnement.<br>
    <br><B>Cas particuliers :</b><br>
    - le game-play offre plusieurs possibilités pour bloquer de façon permanente un personnage ennemi. L'utilisation de ces moyens sans proposer la négociation 
	d'une voie de sortie est considéré comme de l'anti-jeu.<br>
    - une action négative compensée, même répétée, n'est pas considérée comme de l'acharnement si elle est accompagnée d'un MP avec un peu de RP. <br>
	Exemples : un voleur qui rend l'argent, ou un mage qui frappe puis soigne, etc.<br>
	
</p>
    

<br />
<div class='subtitle'>Quelques abréviations utiles</div>
<p class='rule_pg'>Si vous êtes nouveau dans le vaste univers des jeux
	de rôles tour-par-tour en ligne, une petite liste des abréviations et
	anglicismes courants peut vous être utile:
<ul>
	<li>RP : Role-Play, désigne un texte, une situation qui fait partie de
		l'univers imaginaire dans lequel le jeu se situe. Lorsque l'on joue,
		il est important de s'adresser au personnage, et pas au joueur qui le
		joue, en rendant claire cette différence, surtout pour les propos
		agressifs. Exemple "Connard, tu pourris le jeu" vous vaudra d'être
		banni. Alors que la phrase RP suivante pourrait aussi bien s'appliquer
		: "Par les Saintes Colères de Makero, si je te retrouve je jure que tu
		tâteras si bien de ma hache que ta mère Dorane ne pourra plus te
		reconnaître !"</li>
	<li>HRP : Hors Role-Play, désigne un texte qui n'est pas RP.</li>
	<li>IG : In Game, désigne une action ou situation dans le jeu.</li>
	<li>IRL : In Real Life, désigne une action ou situation dans la vie
		réelle.</li>
	<li>IA : Intelligence Artificielle, programme qui gère les monstres.</li>
	<li>PJ : Personnage Joueur, désigne un personnage joué par un joueur,
		au contraire de ceux gérés par l'IA ou le staff.</li>
	<li>PNJ : Personnage Non Joueur, désigne un personnage joué par l'IA ou
		le staff.</li>
	<li>PvP : Player vs Player, combat entre deux PJ.</li>
	<li>PvE : Player vs Environnement, combat opposant PJ et PNJ ou Monstre.</li>
	<li>PvM : Player vs Monster, synonyme de PvE</li>
	<li>GdC : Groupe de chasse</li>
	<li>PV : Points de Vie, score représentant l'état de santé du
		personnage. Arrivé à 0, celui-ci meurt.</li>
	<li>Dn : Type de dés utilisé pour simuler un score, ex: D6 pour un dé à
		6 faces, D100 pour un dé a 100 faces (un pourcentage)</li>
	<li>xDn : Nombre de dés d'un certain type pour simuler un score, ex:
		Pour simuler une Attaque de 4D6 on lance 4 fois un dé a 6 faces.</li>
</ul>
</p>
<p class='rule_pg'>Cette liste n'est pas exhaustive, des listes plus
	complètes se trouvent facilement sur le web.</p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
