<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 300; $i < 400; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i . "\");return false;' > ";
}
// class='stylepc'

?>

<a name="hdp"></a>
<div class='subtitle'>Introduction</div>
<p class='rule_pg'>
	Apprendre à tirer profit des innombrables ressources naturelles
	disséminées à la surface de l'Ile de Nacridan, est une chose que tout
	aventurier devrait chercher à maîtriser. Que ce soit pour gagner leur
	vie grâce à la récolte ou dompter leur environnement grâce aux talents
	de la Nature, ces Savoir-faire rendront la vie des aventuriers plus
	facile et plus sûre. <br /> <br />
</p>
<div class='subtitle'>
	<a name="Ressources"></a>Les Ressources Naturelles
</div>
<p class='rule_pg'>
<ul>
	Les ressources naturelles peuvent être trouvées au gré des déplacements
	sur l'Ile de Nacridan. Toutes les ressources naturelles ont un niveau
	reflétant leur qualité, qui leur donne une valeur marchande. Au niveau
	1, leur valeur à la revente est celle-ci :
	<br />
	<br />
	<li style="margin-bottom: 20px;"><b>Les Rubis - 20 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Les Emeraudes - 10 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Le Fer - 4 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Le Cuir - 4 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Les Ecailles - 4 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Le Lin - 4 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Le Bois - 2 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Les Racines de Rhazot - 2 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Les Graines de Garnach - 2 PO</b><br /></li>
	<li style="margin-bottom: 20px;"><b>Les Feuilles de Folliane - 2 PO</b><br /></li>
</ul>
</p>
<br />

<div class='subtitle'>
	<a name="Les savoir-faire liés à l'artisanat"></a>L'Artisanat
</div>
<p class='rule_pg'>
<ul>

	Les savoir-faire s'apprennent dans les écoles des métiers des villes et
	des villages en échange de Pièces d'Or et de Points d'Action. Une fois
	apprise, vous obtiendrez une nouvelle action disponible dans le menu
	"savoir-faire". Comme toutes les actions, les savoir-faire nécessitent
	un nombre de points d'action qui leur sont propres pour être utilisées.
	<br /> Tout comme les sortilèges et les compétences, les savoir-faire
	s'obtiennent avec un pourcentage de maitrise qu'il faudra réussir pour
	que le savoir-faire se réalise.
	<small><?php echo $name[399];?>Détails. </a></small>
	<br />
	<br />
	<br /> Il y a 4 types de savoir-faire :
	<br /> -Les savoir-faire d'Extraction vous permettent de récupérer une
	matière première à partir des ressources de l'île.
	<br /> -Les savoir-faire de raffinage vous permettent d'obtenir une
	matière première de niveau plus élevé à partir de deux matières
	premières.
	<br /> -Les savoir-faire d'artisanat vous permettent de confectionner
	des pièces d'équipement à partir d 'une matière première.
	<br /> -Les savoir-faire de type nature
	<br />
	<br /> Notez que, contrairement aux compétences et aux sortilèges, les
	savoir-faire d'extraction et de raffinage n'influencent pas vos jauges
	de progression et ne rapportent aucun Point d'Expérience. Seuls les
	savoir-faire d'artisanat rapportent 1 Point d'Expérience en cas de
	réussite et influencent les jauges de progression.

	<br />
	<br />

	<a name="Ecole de Metiers"></a> Comme pour les compétences et les
	sortilèges, le prix et le temps d'apprentissage des savoir-faire
	pourront varier dans les bâtiments des cités contrôlés par d'autres
	personnages. Cependant, au début du monde apprendre un savoir-faire
	vous coûtera :
	<br />
	<br />
	<ul>
		<li style="margin-bottom: 20px;"><b>25 PO et 10 PA pour les
				savoir-faire d'Extraction </b><br /></li>
		<li style="margin-bottom: 20px;"><b>50 PO et 10 PA pour les
				savoir-faire de Raffinage </b><br /></li>
		<li style="margin-bottom: 20px;"><b>100 PO et 10 PA pour les
				savoir-faire d'Artisanat </b><br /></li>
		<li style="margin-bottom: 20px;"><b>15 PO et 8 PA pour les
				savoir-faire de type nature </b><br /></li>
	</ul>

	<br />
	<div class='subtitle'>
		<a name="Les Talents d_Extraction"></a>Les talents d'Extraction
	</div>
	<p class='rule_pg'>
	
	
	<ul>
		A chaque talent d'Extraction se trouve associé un outil particulier
		qui peut augmenter la qualité de la matière première extraite. Ceux-ci
		ne sont pas indispensable mais c'est bien évidemment à leurs outils
		que l'on reconnait les bons artisans.
		<br />
		<small><?php echo $name[393];?>Détails </a></small>
		<br />
		<br />
		<li style="margin-bottom: 20px;"><b>Le Fer</b><br /> Le fer est
			récupéré en négociant avec des Kradjeck Ferreux qui de ce que l'on
			sait, empruntent les portails des monstres pour venir visiter notre
			île. Une fois sur deux, le Kradjeck Ferreux repart dans son monde
			après avoir délivré un fer.<br /> Talent : <b>Dialecte Kradjeckien (6
				PA)</b><br /> Outil : <b>Gemme de Conviction</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Le Cuir et les Ecailles</b><br />
			Le Cuir et les Ecailles sont récupérés directement sur les dépouilles
			de certains monstres seulement. Il n'y a notamment aucun monstre de
			niveau 1 ou 2 qui puissent être dépecés. Prenez-en compte si vous
			vous destinez à ce métier dès vos premiers pas sur Nacridan.<br />

			Talent : <b>Dépecer (3 PA)</b><br /> Outil : <b>Pince à dépecer</b><br />
		</li>
		<li style="margin-bottom: 20px;"><b>Le Lin</b><br /> Le Lin se récolte
			dans des champs de lin. Au moins trois zones sont réputées pour la
			culture du lin sur l'île : à l'Est d'Artasse, à l'Ouest d'Earok et le
			long de la cote, au Nord-Ouest de Tonak. <br /> Talent : <b>Teiller
				(8 PA)</b><br /> Outil : <b>Teilleuse</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Le Bois</b><br /> Contrairement
			aux autres ressources, il ne faut pas se trouver à coté d'une source
			particulière pour le récolter : le Bois peut être coupé presque
			partout mais sa qualité dépendra de l'environnement dans lequel il
			est récolté, sachant que les déserts, les routes, les marécages et
			les montagnes y sont peu propices.<br /> Talent : <b>Couper du Bois
				(5 PA)</b><br /> Outil : <b>Cauchoir de bucheron</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Les Emeraudes et les Rubis</b><br />
			Les gemmes sont extraites des différents gisements affleurant à la
			surface de Nacridan. Plusieurs sites d'émeraude ont été repérés sur
			l'île de Nacridan. Les plus connus sont ceux à l'Est d'Artasse, au
			Sud-Est d'Earok et au Nord-Ouest de Tonak.<br /> Talent : <b>Miner (8
				PA)</b><br /> Outil : <b>Pioche de mineur</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Les Herbes</b><br /> Les herbes
			sont cueillies sur des buissons, qui sont répartis à peu près partout
			sur l'île de Nacridan, mais leur présence est toutefois plus riche en
			forêt que dans le desert. Des zones particulièrement luxuriantes ont
			d'ailleurs été repérées au Sud-Est d'Earok, au Nord-Est d'Artasse et
			juste à la sortie de Tonak, plein Nord. <b><br/> Talent : <b>Récolter (5
				PA)</b><br /> Outil : <b>Pressoir</b><br /></li>
	</ul>

	</p>
	<br />
	<a href="#hdp">haut de page</a>
	<br>
	<br />
	<br />
	<br />
	<div class='subtitle'>
		<a name="Les Talents de Raffinage"></a>Les Talents de Raffinage
	</div>
	<p class='rule_pg'>
	
	
	<ul>
		Les talents de raffinage permettent d'améliorer le niveau d'une
		matière première. Leur fonctionnement est identique pour tous les
		types de matériaux.
		<br /> Pour effectuer un raffinage, il est nécessaire d'avoir à sa
		disposition deux pièces de matière première qui en cas de réussite
		seront combinées en une seule supérieure de 2 niveau au niveau le plus
		élevé des deux pièces.
		<br />Le pourcentage de réussite de l'opération de raffinage est
		fonction des niveaux de chacune des deux pièces.
		<br />
		<small><?php echo $name[398];?>Détails </a></small>
		<br />
		<br />

		<li style="margin-bottom: 20px;"><b>Le Fer</b><br /> Talent : <b>Ferronnerie
				(5 PA)</b><br /> Outil : <b>Marteau de ferronnier</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Le Cuir</b><br /> Talent : <b>Tanner
				(5 PA)</b><br /> Outil : <b>Griffe à lacer de tanneur </b><br /></li>
		<li style="margin-bottom: 20px;"><b>Les Ecailles</b><br /> Talent : <b>Ecailler
				(5 PA)</b><br /> Outil : <b>Abat-carre d'écailleur</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Le Lin</b><br /> Talent : <b>Effiler
				(5 PA)</b><br /> Outil : <b>Roue à molette d'effileur</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Le Bois</b><br /> Talent : <b>Charpenterie
				(5 PA)</b><br /> Outil : <b>Chignole de charpentier</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Les Emeraudes et les Rubis</b><br />
			Talent : <b>Tailler (5 PA)</b><br /> Outil : <b>Burin de tailleur</b><br />
		</li>
		<li style="margin-bottom: 20px;"><b>Les Herbes</b><br /> Talent : <b>Filtrer
				(5 PA)</b><br /> Outil : <b>Filtre d'alchimiste</b><br /></li>
	</ul>
	</p>
	<br />
	<a href="#hdp">haut de page</a>
	<br>
	<br />
	<br />
	<div class='subtitle'>
		<a name="Les Talents d_Artisanat"></a>Les Talents d'Artisanat
	</div>
	<p class='rule_pg'>
	
	
	<ul>
		Les talents d'artisanat permettent d'utiliser les matières premières
		récoltées ou raffinées pour fabriquer des pièces d'équipement ou pour
		les enchanter.
		<br />
		<small><?php echo $name[397];?>Détails </a></small>
		<br />
		<br />

		<li style="margin-bottom: 20px;"><b>Artisanat du Fer (8 PA, For)</b><br />
			Permet de créer les pièces d'armure de plates ainsi que les armes en
			fer.<br /> Outil : <b>Tenaille d'artisan</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Artisanat du Cuir (8 PA, Dex)</b><br />
			Permet de créer les pièces d'équipement en cuir, sac compris.<br />
			Outil : <b>Alène d'artisan</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Artisanat des Ecailles (8 PA, Vit)</b><br />
			Permet de créer les pièces d'armure en écailles.<br /> Outil : <b>Plioir
				d'artisan</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Artisanat du Lin (8 PA, MM)</b><br />
			Permet de créer tous les objets magiques en lin.<br /> Outil : <b>Ciseaux
				d'artisan</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Artisanat du Bois (8 PA, Dex)</b><br />
			Permet de créer toutes les pièces d'équipement en bois: arcs, flèches
			et certains boucliers. <small><?php echo $name[394];?>Détails </a></small><br />
			Outil : <b>Râpe d'artisan</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Artisanat des Plantes (8 PA,
				MM+Vit)</b><br /> Permet de créer des potions à partir des plantes.
			<small><?php echo $name[396];?>Détails </a></small><br /> Outil : <b>Alambic
				d'artisan</b><br /></li>
		<li style="margin-bottom: 20px;"><b>Artisanat des Gemmes (8 PA, MM)</b><br />
			Permet de créer tous les enchantements ainsi que des sceptres et des
			bâtons. <small><?php echo $name[395];?>Détails </a></small><br />
			Outil : <b>Cristal d'enchantement</b><br /></li>

	</ul>
	</p>
</ul>
</p>
<br />
<div class='subtitle'>
	<a name="Talents de la Nature"></a>Les Talents de la Nature
</div>
<p class='rule_pg'>
<ul>
	Maîtriser un ou plusieurs talents de la nature est nécéssaire pour
	quiconque chercherait à dompter l'environnement sauvage de l'Île de
	Nacridan.
	<br />
	<br /> Les salles de classe des écoles de métiers d'Earok, d'Artasse et
	de Tonak proposent d'enseigner quatre talents de la nature contre la
	modique somme de 15 PO.
	<br />
	<br /> Le temps d'apprentissage de base d'un talent de la nature est de
	8 PA. Comme pour les compétences ou les sortilèges, il peut être réduit
	de 2PA pour chaque niveau de l'école de métier, jusqu'à un minimum de
	2PA.
	<br />
	<br /> Chaque Talent de la Nature nouvellement acquis est maîtrisé à
	50%. Avec la pratique, ce pourcentage augmentera jusqu'à atteindre la
	limite de maîtrise maximale de 90%. Cependant attention, réussir un
	talent de la nature ne rapporte pas de points d'expérience.
	<br />
	<br /> Pour augmenter un talent de 1% il faudra que le personnage :
	<ul>
		<li>Réussisse son talent.</li>
		<li>Réussisse son jet d'amélioration (un jet d'amélioration est réussi
			lorsque le joueur obtient sur un d100 une valeur supérieure à son
			pourcentage de maîtrise dans le talent à améliorer).<br />
		</li>
	</ul>
	<br />
</ul>
</p>

<p class='rule_pg'>
<ul>
	<li style="margin-bottom: 20px;"><b>Détection des ressources (2 PA)</b><br />
		Permet de repérer la direction et la distance approximative d'une
		ressource d'un type et d'un niveau donné. Ce talent ne fonctionne
		qu'en decà d'une certaine distance et la direction n'est donnée que si
		la ressource est suffisamment proche.</li>

	<li style="margin-bottom: 20px;"><b>Appel du Kradjeck (2 PA)</b><br />
		Permet d'appeler un Kradjeck par delà un portail en vue. Si le talent
		est réussi, le Kradjeck apparaitra par le portail.</li>

	<li style="margin-bottom: 20px;"><b>Connaissance des monstres (2 PA)</b><br />
		Permet d'évaluer les caractéristiques d'un monstre en vue. En plus du
		nombre de points de vie courant, ce talent permet d'obtenir <b>une
			approximation</b> de deux caractéristiques de combat au choix parmi
		l'attaque, la défense, les dégâts, l'armure et la maitrise de la
		magie.</li>


	<li style="margin-bottom: 20px;"><b>Fermeture des portails démoniaques
			(5 PA)</b><br /> A l'aide des incantations apprises des prêtres, vous
		avez la possibilité, si vous vous situez à moins de 15 cases d'un
		temple, d'utiliser son pouvoir pour forcer la fermeture d'un portail
		démoniaque.</li>


</ul>
</p>
<a href="#hdp">haut de page</a>
<br>
<br />
<br />
