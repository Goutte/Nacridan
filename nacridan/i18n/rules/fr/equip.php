<?php
require_once ("../../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

$stepX = 155;
$stepY = 60;

$equip[1] = array(
    220 - $stepX,
    255 - $stepY
);
$equip[2] = array(
    323 - $stepX,
    255 - $stepY
);

$equip[4096] = array(
    190 - $stepX,
    235 - $stepY
);
$equip[8192] = array(
    355 - $stepX,
    235 - $stepY
);

$equip[4] = array(
    273 - $stepX,
    185 - $stepY
);
$equip[8] = array(
    235 - $stepX,
    155 - $stepY
);
$equip[16] = array(
    311 - $stepX,
    155 - $stepY
);
$equip[32] = array(
    273 - $stepX,
    98 - $stepY
);

$equip[64] = array(
    array(
        200 - $stepX,
        415 - $stepY
    ),
    array(
        345 - $stepX,
        415 - $stepY
    )
);

$equip[128] = array(
    273 - $stepX,
    145 - $stepY
);
$equip[256] = array(
    0,
    0
);

$equip[512] = array(
    array(
        240 - $stepX,
        300 - $stepY
    ),
    array(
        302 - $stepX,
        300 - $stepY
    )
);

$equip[1024] = array(
    array(
        215 - $stepX,
        365 - $stepY
    ),
    array(
        330 - $stepX,
        365 - $stepY
    )
);
$equip[2048] = array(
    0,
    0
);

function getEquipCss()
{
    global $equip;
    
    $str = ".img{position: absolute; z-index: 1; border: 0;}\n";
    
    $i = 1;
    while ($i <= 8192) {
        if (isset($equip[$i])) {
            if (is_array($equip[$i][0])) {
                $j = 0;
                foreach ($equip[$i] as $equipval) {
                    $str .= "#equip" . $i . $j . " { left: " . $equipval[0] . "px; top: " . $equipval[1] . "px; }\n";
                    $j ++;
                }
            } else {
                $str .= "#equip" . $i . " { left: " . $equip[$i][0] . "px; top: " . $equip[$i][1] . "px; }\n";
            }
        }
        $i *= 2;
    }
    return $str;
}

function writeline($eqmodifier, &$characLabel, $color = 1)
{
    foreach ($characLabel as $val) {
        $extra = "";
        
        if ($color) {
            $charac = $eqmodifier->getModif($val, DICE_ADD);
            if ($charac == 0) {
                $charac = $eqmodifier->getModif($val, DICE_MULT);
            }
            if ($charac > - 99) {
                $extra = "class='dred'";
            }
            if ($charac > - 10) {
                $extra = "class='red'";
            }
            if ($charac > - 7) {
                $extra = "class='lred'";
            }
            if ($charac > - 3) {
                $extra = "class='vlred'";
            }
            if ($charac > 0) {
                $extra = "class='vlgreen'";
            }
            if ($charac > 3) {
                $extra = "class='lgreen'";
            }
            if ($charac > 7) {
                $extra = "class='green'";
            }
            if ($charac > 10) {
                $extra = "class='dgreen'";
            }
        } else {
            $extra = "class='lgreen'";
        }
        
        $str = $eqmodifier->getModifStr($val);
        
        if ($str != 0) {
            echo "<td align='center'" . $extra . ">" . $str . "</td>";
        } else
            echo "<td align='center'>-</td>";
    }
}

echo "<style type=\"text/css\">\n" . getEquipCss() . "</style>";

echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/equip.js") . "'></script>\n";

echo "<div id='overlay' style='overflow: auto; visibility: hidden; position: absolute;'></div>";

$eq = new BasicEquipment();
$eqmodifier = new Modifier();

$db = DB::getDB();

$dbe = new DBCollection("SELECT zone," . $eq->getASRenamer("EQ", "EQ") . "," . $eqmodifier->getASRenamer("EQM", "EQM") . ",mask FROM BasicEquipment AS EQ LEFT JOIN EquipmentType AS ET ON EQ.id_EquipmentType=ET.id LEFT JOIN Modifier_BasicEquipment AS EQM ON  id_Modifier_BasicEquipment=EQM.id ORDER BY EQ.id_EquipmentType ASC, EQ.name ASC", $db);

if (isset($_GET["t1level"])) {
    $selectedlevel = min($_GET["t1level"], 10);
} else {
    $selectedlevel = 1;
}

// ------------------------------------- ARMES -------------------------

echo "<div class='subtitle'> - Les Armes </div>";
?>
<p class='rule_pg'>
	L'arme est peut-être l'équipement le plus important pour votre
	aventurier. D'abord parce qu'elle confère un bonus important sur les
	caractéristiques de base ou intermédiaires mais surtout parce qu'elle
	détermine le type d'attaque que vous allez utiliser : contact ou
	distance et les caractéristiques qui vont être sollicitées lors d'une
	attaque. Voir la section <a href='rules.php?page=step5'>Combat</a> pour
	le détail de l'influence de la catégorie d'arme sur le déroulement des
	combats.<br /> <br /> Notez que l'utilisation d'un arc demande de
	posséder dans son équipement des Flèches barbelées ou des Flèches
	torsadées.<br /> <br /> Dans le tableau ci-dessous, vous pourrez
	retrouver le bonus et la catégorie de toutes les armes existantes dans
	le monde de Nacridan. Utilisez le sélecteur ci-dessous pour choisir le
	niveau des armes que vous voulez regarder.<br /> <br />
</p>
<?php

echo "<div align='center'>";
$str = "<form action=\"\" id=\"weapons_form\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 1; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t1level=" . $i . "#weapons_form' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

$characLabel = array(
    "attack",
    "damage",
    "magicSkill",
    "strength",
    "speed"
);
echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 220px;'>Nom</th><th>Attaque</th><th>Dégâts</th><th>Magie</th><th>Force</th><th>Vitesse</th><th>Nb zone(s)</th></th><th> Catégorie </th><th> Prix </th></tr>";
echo "<tr><td colspan=\"11\">&nbsp;</td></tr>";
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") == 21 || $eq->get("id_EquipmentType") == 28 || $eq->get("id_EquipmentType") == 22 || $eq->get("id_EquipmentType") < 8) {
        $mask = "Array(";
        $i = 0;
        if (isset($equip[$dbe->get("mask")]) && is_array($equip[$dbe->get("mask")][0])) {
            foreach ($equip[$dbe->get("mask")] as $value) {
                if ($i != 0)
                    $mask .= ",";
                $mask .= "'" . $dbe->get("mask") . "'";
                $i ++;
            }
            $mask .= ")";
        } else {
            $mask = "Array('" . $dbe->get("mask") . "')";
        }
        
        echo "<tr><td class='alignleft' onmouseover=\"equipShow(this," . $mask . ",'" . CONFIG_HOST . "',event)\" onmouseout=\"equipHide(this,event)\">" . $eq->get("name") . "</td>";
        $eqmodifier->updateFromEquipmentLevel($selectedlevel);
        $eqmodifier->initCharacStr();
        writeline($eqmodifier, $characLabel);
        echo "<td class='aligncenter'>" . $dbe->get("zone") . "</td>";
        
        $cat = "";
        if ($eq->get("id_EquipmentType") == 1)
            $cat = "Léger";
        if ($eq->get("id_EquipmentType") == 2 || $eq->get("id_EquipmentType") == 5 || $eq->get("id_EquipmentType") == 6 || $eq->get("id_EquipmentType") == 7)
            $cat = "Intermédiaire";
        if ($eq->get("id_EquipmentType") == 3 || $eq->get("id_EquipmentType") == 4)
            $cat = "Lourd";
        echo "<td class='aligncenter'>" . $cat . "</td>";
        
        require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
        // echo $eq->get("id_BasicEquipment");
        $prix = EquipFactory::getPriceBasicEquipment($eq->get("id"), $selectedlevel, $db);
        echo "<td class='aligncenter'>" . $prix . "</td>";
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table><br/></div>";

$dbe->first();

if (isset($_GET["t2level"])) {
    $selectedlevel = min($_GET["t2level"], 10);
} else {
    $selectedlevel = 1;
}

// ------------------------------------- ARMURES -------------------------

echo "<div class='subtitle' > - Les Armures</div>";

?>
<p class='rule_pg'>
	Les armures regroupent toutes les pièces d'équipement que votre
	personnage peut porter pour se protéger. Les boucliers sont
	particulièrement importants car ils influencent les caractéristiques
	utilisées par votre personnage pour se défendre. Utilisez le sélecteur
	ci-dessous pour choisir le niveau des amures que vous voulez regarder.<br />
	<br />
</p>
<?php

echo "<div align='center'>";
$str = "<form action=\"\" id=\"armor_form\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 1; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t2level=" . $i . "#armor_form' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

$characLabel = array(
    "attack",
    "defense",
    "armor",
    "magicSkill"
);
echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 200px;'>Nom</th><th>Attaque</th><th>Défense</th><th>Armure</th><th>Magie</th><th>Nb zone(s)</th><th> Prix </th></tr>";
echo "<tr><td colspan=\"8\">&nbsp;</td></tr>";
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") > 7 && $eq->get("id_EquipmentType") < 21) {
        $eqmodifier->updateFromEquipmentLevel($selectedlevel);
        $mask = "Array(";
        $i = 0;
        if (isset($equip[$dbe->get("mask")]) && is_array($equip[$dbe->get("mask")][0])) {
            foreach ($equip[$dbe->get("mask")] as $value) {
                if ($i != 0)
                    $mask .= ",";
                $mask .= "'" . $dbe->get("mask") . "'";
                $i ++;
            }
            $mask .= ")";
        } else {
            $mask = "Array('" . $dbe->get("mask") . "')";
        }
        echo "<tr><td onmouseover=\"equipShow(this," . $mask . ",'" . CONFIG_HOST . "',event)\" onmouseout=\"equipHide(this,event)\">" . $eq->get("name") . "</td>";
        $eqmodifier->initCharacStr();
        writeline($eqmodifier, $characLabel);
        echo "<td class='aligncenter'>" . $dbe->get("zone") . "</td>";
        $prix = EquipFactory::getPriceBasicEquipment($eq->get("id"), $selectedlevel, $db);
        echo "<td class='aligncenter'>" . $prix . "</td>";
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table><br/></div>";

$dbe->first();

if (isset($_GET["t3level"])) {
    $selectedlevel = min($_GET["t3level"], 10);
} else {
    $selectedlevel = 1;
}

// ------------------------------------- POTIONS -------------------------

echo "<div class='subtitle' > - Les Potions </div>";
?>
<p class='rule_pg'>
	Les potions permettent d'augmenter les caractéristiques de base de
	votre personnage pendant 3 tours. Elles peuvent être d'un grand secours
	lors de situations périlleuses. Les échoppes des différents villages
	vendent des potions de différents niveaux (jamais supérieur à 5
	cependant). Vous pouvez également les fabriquer vous-même avec des
	herbes et le savoir-faire artisanat des plantes.<br /> Un indice de
	tolérance de niveau de potions est effectif : <b>attention</b>, cela
	signifie concrètement que vous recevrez un malus si vous buvez une ou
	plusieurs potions dont le total des niveaux est supérieur à 1+
	log(niveau perso, 2) [lire log de 2]. Voici ce que cela donne pour les
	7 premiers niveaux :<br>
<ul>
	<li>Vous pouvez consommer 1 niveau de potion à partir du niveau 1</li>
	<li>Vous pouvez consommer 2 niveaux de potion à partir du niveau 2</li>
	<li>Vous pouvez consommer 3 niveaux de potion à partir du niveau 4</li>
	<li>Vous pouvez consommer 4 niveaux de potion à partir du niveau 8</li>
	<li>Vous pouvez consommer 5 niveaux de potion à partir du niveau 16</li>
	<li>Vous pouvez consommer 6 niveaux de potion à partir du niveau 32</li>
	<li>Vous pouvez consommer 7 niveaux de potion à partir du niveau 64</li>
	<li>Etc...</li>
</ul>

Utilisez le sélecteur ci-dessous pour choisir le niveau des potions que
vous voulez regarder.
<br />
<br />


</p>
<?php
echo "<div align='center'>";
$str = "<form action=\"\" id=\"potion_form\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 1; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t3level=" . $i . "#potion_form' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

$characLabel = array(
    "strength",
    "speed",
    "dexterity",
    "hp",
    "magicSkill"
);
echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 160px;'>Nom</th><th>Force</th><th>Vitesse</th><th>Dextérité</th><th>PDV</th><th>Magie</th><th> Prix </th></tr>";
echo "<tr><td colspan=\"7\">&nbsp;</td></tr>";
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") == 29) {
        echo "<tr><td>" . $eq->get("name") . "</td>";
        $eqmodifier->updateFromEquipmentLevel($selectedlevel);
        $eqmodifier->initCharacStr();
        writeline($eqmodifier, $characLabel, 0);
        $prix = EquipFactory::getPriceBasicEquipment($eq->get("id"), $selectedlevel, $db);
        echo "<td class='aligncenter'>" . $prix . "</td>";
        
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table>";
echo "</div><br/><br/>";

// ----------------------------------- ENCHANTEMENTS ------------------------------
?>
<br />
<div class='subtitle'>
	<a name="enchantements"></a>Enchantements
</div>
<p class='rule_pg'>
	Les équipements peuvent être enchantés pour apporter un bonus fixe sur
	une caractéristique de base. Soit N le niveau de l'enchantement, le
	bonus apporté est de +N dans la caractéristique correspondante. Le
	niveau de l'enchantement doit être inférieur ou égal au niveau de
	l'objet.<br /> <br /> Pour réaliser un enchantement, le mage doit se
	procurer la gemme correspondant au type d'enchantement qu'il désire:
	une émeraude pour un enchantement mineur et un rubis pour un
	enchantement majeur.<br /> Il doit ensuite réaliser un jet de maitrise
	de la magie pour effectuer l'enchantement proprement dit, étant bien
	évident que le jet de maitrise sera plus difficile pour un enchantement
	majeur que pour un mineur.<br /> Notez qu'on ne peut mettre qu'un seul
	enchantement mineur sur un même objet, et qu'on ne peut mettre un
	enchantement majeur que si l'objet est déjà porteur d'un enchantement
	mineur.<br />

</p>
<br />
<div align='center'>
	<table class='enchanttable'>
		<tr>
			<th style='border-right: 1px solid;'></th>
			<th>Force</th>
			<th>Vitesse</th>
			<th>Dextérité</th>
			<th>Magie</th>
			<th>Armure</th>
		</tr>
		<tr>
			<td style='border-right: 1px solid;'>Mineur</td>
			<td>du Kraken</td>
			<td>de la Manticore</td>
			<td>du Griffon</td>
			<td>du Phénix</td>
			<td>du Dragon</td>
		</tr>
		<tr>
			<td style='border-right: 1px solid;'>Majeur</td>
			<td>Colossal(e)</td>
			<td>Foudroyant(e)</td>
			<td>Flamboyant(e)</td>
			<td>Ethéré(e)</td>
			<td>Eternel(le)</td>
		</tr>
	</table>
</div>
<br />
<p class='rule_pg'>
	Par exemple, une dague (niv. 3) du Griffon (niv. 2) Ethéré(e)(niv. 3)
	Donnera + 2.5 en attaque comme une dague normale, avec en plus +2 en
	dextérité et +3 en maîtrise de la magie. <br /> <br /> Attention, il
	est à noter que poser un enchantement sur une arme n'en fait pas une
	arme magique. Seuls les Sceptres et les Bâtons rentrent dans la
	catégorie des armes magiques qui permettent et influencent les
	caractéristiques du mage utilisant des sortilèges d'attaque magique.<br />

</p>
<br />

<?php

// ----------------------------------- SURCHARGE ------------------------------
?>


<div class='subtitle'>Charge et Surcharge</div>

<p class='rule_pg'>
	Les trois types de charge associées à votre équipement sont décrites
	ci-dessous. Si vous allez au delà des limites, vous serez en surcharge.
	Ce dépassement vous infligera un malus de la moitié de vos PAs au début
	de chaque nouveau tour. Si vos points de charge sont même supérieurs à
	deux fois la limite, alors le malus passera à 3/4 de vos PAs.<br /> <br />
	- L'Encombrement : <br /> Si vous portez une somme d'équipements trop
	lourds pour vous, votre mobilité sera réduite. Concrètement, les points
	de charge d'encombrement d'un équipement correspondent à son niveau au
	carré et vous êtes en surcharge si la somme de ces points dépasse de 5
	fois votre niveau. L'état de l'encombrement, intitulé "charge", est
	noté dans la section équipement, en bas à gauche du parchemin qui
	illustre la liste de vos objets équipés. Notez que les sacs et tout ce
	qu'ils contiennent ne rentrent pas en compte dans le calcul de la
	charge d'encombrement.<br /> <br /> - La Limite de Plate : <br /> Si
	vous portez une armure trop lourde pour votre force, votre mobilité
	sera réduite. Les points de charge de plate correspondent à la somme
	des niveaux des pièces d'armure de plate que vous portez (<b>attention</b>,
	la Griffe augmente aussi cette charge). L'état de la charge de plate
	est noté en haut à droite du parchemin illustrant votre équipement et
	votre capacité totale est égale à votre nombre de dés de Force.<br /> <br />
	- La Limite de Lin :<br /> Si vous portez trop d'objets magiques pour
	votre maitrise de la magie, votre mobilité sera réduite. Les points de
	charge de lin correspondent à la somme des niveaux des objets magiques
	que vous portez. L'état de la charge de lin est noté en haut à droite
	du parchemin illustrant votre équipement et votre capacité totale est
	égale à votre nombre de dés de Maitrise Magique.<br /> <br />
</p>
<br />

<?php

// ----------------------------------- USURE ------------------------------
?>


<div class='subtitle'>Usure de l'équipement</div>

<p class='rule_pg'>
	Tous les équipements et les sacs s'usent au fur et à mesure de leur
	utilisation.<br /> Il y a cinq états d'usure: <b>Neuf</b>, <b>Bon Etat</b>
	de 1 à 30%, <b>Etat Moyen</b> de 31 à 70%, <b>Mauvais Etat</b> de 71 à
	99% et <b>Cassé</b>.<br /> Lorsqu'une pièce d'équipement se casse, on
	ne peut plus s'en servir et elle ne procure donc plus son bonus. Ne la
	jetez pas pour autant car elle pourra être réparée et retrouver ses
	fonctions. Notez que les outils d'artisanat sont beaucoup plus fragiles
	que les pièces d'équipement classiques telles que armes, armures
	boucliers et sacs.<br />
<?php
// Les équipements classiques et les sacs ont une durabilité de 60, les outils de 15.
// L'usure de l'arme est augmentée de 1 à chaque frappe.
// L'usure du bouclier est augmentée de 1 à chaque défense réussie.
// A chaque coup encaissé, chaque pièce d'équipement a 1 chance sur 2 de voir son usure augmenter de 1.
?>
<br />

</p>
<br />

<div class='subtitle'>Le transport du matériel</div>
<p class='rule_pg'>

	Votre sac d'équipements peut contenir jusqu'à 10 objets en tout genre,
	autres contenants <b>non équipés</b> inclus. Soit N le niveau de vos
	autres contenants, une fois équipés ils vous permettent tous de
	transporter <b>2+2*N objets spécifiques</b> (sauf indication contraire
	ci-dessous).<br /> - Le sac de matières premières permet de ranger les
	fer, cuir, écaille, lin et bois.<br /> - Le sac d'herbes permet de
	ranger les racines, graines et feuilles.<br /> - Le sac de gemmes
	permet de ranger les émeraudes et rubis.<br /> - Le carquois permet de
	ranger les flèches : contrairement aux autres contenants, <b>il peut
		contenir 6+2*N objets</b>.<br /> - La ceinture permet de ranger les
	potions : elles pourront alors être bues pour 1 PA (au lieu de 2 PA
	dans le sac d'équipement).<br /> - La trousse à outils permet de ranger
	les outils utiles aux différents <a href='rules.php?page=step11'>Savoir-Faire</a>
	(extraction, raffinage et artisanat).<br /> <b>Attention</b>, la
	trousse à outils et le carquois ne peuvent pas être équipés en même
	temps, ils occupent le même emplacement. <br /> <br /> Votre sac
	d'équipements est toujours accessible, mais tout autre contenant doit
	être équipé pour être utilisé. A ce propos, vous ne pouvez pas équiper
	deux sacs du même type. Quand vous ramassez un objet, vous choisissez
	le sac dans lequel vous voulez le placer. Avec l'action <b>"Ranger"</b>
	vous pouvez déplacer vos objets entre le sac d'équipements et le
	contenant spécifique associé. Cette action coûte 0 PA dans les
	bâtiments et 1 PA à l'extérieur. <b>Attention</b>, si vous posez,
	donnez, expédiez ou vendez l'un de vos contenants, tout ce qu'il
	contient le suivra !<br> <br>
</p>
<br>
<div class='subtitle'>
	<a name="Les_outils"></a>Les outils
</div>
<p class='rule_pg'>
	Pour faciliter l'expression de leur art, les artisans de Nacridan
	peuvent employer des outils (pensez à les équiper pour bénéficier de
	leur effet) : chacun d’eux apporte un bonus dans le savoir-faire
	associé.<br /> - Les outils d'extraction donnent des chances de
	récolter une matière de qualité supérieure.<br /> - Les outils de
	raffinage augmentent les chances de réussir à combiner 2 ressources
	pour en obtenir 1 de meilleure qualité.<br /> - Les outils de
	fabrication augmentent la rapidité de confection (ou le nombre
	d'équipements produits, en ce qui concerne les flèches et les potions).<br />
	<br /> L'effet de l'outil de fabrication est <b>cumulables avec l'effet
		de la guilde des artisans</b>. Cette dernière octroie un bonus sur le
	jet de caractéristique associée et donc sur le niveau de l'artisanat.
	Attention, dans le cas du raffinage dans une guilde des artisans,
	l'effet de la guilde se subtitue à l'effet de l'outil de raffinage
	éventuellement équipé. Vous retrouverez le fonctionnement détaillé des
	outils sur la page des <a href='rules.php?page=step11'>Savoir-Faire</a>
	et celui de la guilde des artisans sur la page consacrée aux <a
		href='rules.php?page=step12'>Villages</a>.<br /> <br /> Utilisez le
	sélecteur ci-dessous pour choisir le niveau des équipements que vous
	voulez regarder ainsi que leur prix. <b>Attention</b> : leur production
	étant facilité par le fait qu'ils n'utilisent pas de matière première,
	contrairement aux autres équipements le prix de vente des outils en
	échoppe n'est que de 2 POs par niveau.<br /> <br />

</p>
<?php
if (isset($_GET["t4level"])) {
    $selectedlevel = min($_GET["t4level"], 10);
} else {
    $selectedlevel = 1;
}
echo "<div align='center'>";
$str = "<form action=\"\" id=\"tools_form\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 1; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t4level=" . $i . "#tools_form' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 160px;'>Nom</th><th> Prix </th></tr>";
echo "<tr><td colspan=\"2\">&nbsp;</td></tr>";
$dbe->first();
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") == 40) {
        echo "<tr><td>" . $eq->get("name") . "</td>";
        $prix = EquipFactory::getPriceBasicEquipment($eq->get("id"), $selectedlevel, $db);
        echo "<td class='aligncenter'>" . $prix . "</td>";
        
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table>";
echo "</div><br/><br/>";
?>

<div class='subtitle'>
	<a name="Ressources"></a>Les Ressources Naturelles
</div>
<p class='rule_pg'>
<ul>
	Les ressources naturelles peuvent être trouvées au gré des déplacements
	sur l'Ile de Nacridan. Toutes les ressources naturelles ont un niveau
	reflétant leur qualité, qui leur donne une valeur marchande. Vous
	trouverez dans le tableau ci-dessous leur valeur marchande.
	</p>
<?php

if (isset($_GET["t5level"])) {
    $selectedlevel = min($_GET["t5level"], 10);
} else {
    $selectedlevel = 1;
}
echo "<div align='center'>";
$str = "<form action=\"\" id=\"resources_form\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 1; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t5level=" . $i . "#resources_form' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 160px;'>Nom</th><th> Prix </th></tr>";
echo "<tr><td colspan=\"2\">&nbsp;</td></tr>";
$dbe->first();
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") >= 30 && $eq->get("id_EquipmentType") <= 32) {
        echo "<tr><td>" . $eq->get("name") . "</td>";
        $prix = EquipFactory::getPriceBasicEquipment($eq->get("id"), $selectedlevel, $db);
        echo "<td class='aligncenter'>" . $prix . "</td>";
        
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table>";
echo "</div><br/><br/>";

?>
<!--
<br>
a) <a name="pioche_mineur"></a> « Pioche de mineur ». Apporte (10*niveau de la pioche) % de chance d’extraire une gemme de niveau 2 au lieu de niveau 1.<br>
<br>
b) <a name="ciseaux_tanneur"></a> « Ciseaux de tanneur ». Apporte (5*niveau du ciseau) % de chance de tanner un cuir ou une écaille d’un niveau supérieur à la normale soit de produire une peau de niveau n+2 avec deux peaux de niveau n (au lieu de n+1).<br>
<br>
c) <a name="burin_tailleur"></a> « Burin de tailleur ». Apporte (5*niveau du burin) %  de chance de tailler une gemme d’un niveau supérieur à la normale soit de produire une gemme de niveau n+2 avec deux gemmes de niveau n (au lieu de n+1).<br>
<br>
d) <a name="marteau_forgeron"></a> « Marteau du forgeron » . Apporte (5*niveau de la tenaille)% de chance de créer un fer d’un niveau supérieur à la normale soit de produire un fer de niveau n+2 avec deux fers de niveau n (au lieu de n+1)<br>
<br>
e) <a name="chignole_charpentier"></a> « Chignole de charpentier ».  Apporte (5*niveau de la chignole)% de chance de créer un bois d’un niveau supérieur à la normale soit de produire un bois de niveau n+2 avec deux bois de niveau n (au lieu de n+1)<br>
<br>
f) <a name="alambic_alchimiste"></a> « Alambic d’alchimiste ». Apporte 10 fois le niveau de l’alambic en bonus sur votre jet de MM lors de la fabrication d’une potion (Alchimie).<br>
<br>
g) <a name="arcane_enchantement"></a> « Arcane d’enchantement ». Apporte 10 fois le niveau de l’arcane en bonus sur votre jet de MM pour la fabrication des amulettes et des echantements (Joaillerie et Enchanter).<br>
<br>
h) <a name="cristal_magique"></a> « Cristal magique». Apporte 10 fois le niveau du crystal en bonus sur votre jet de MM pour la fabrication des equipements magiques (Sort Artisanat magique).<br>
<br>
i) <a name="cauchoire_bucheron"></a> « Cauchoire de bûcheron ». Apporte 7 fois le niveau du cauchoire en bonus sur votre jet de dégât lorsque vous couper du bois.<br>
<br>
j) <a name="tenaille_artisan"></a> « Tenaille d'artisan » . Ajoute 5 fois niveau de la tenaille en bonus sur votre jet de dégât pour la fabrication des armures de plates (Artisanat plate).<br>
<br/>
k) <a name="cisaille_artisan"></a> « Cisaille d'artisan » . Ajoute 5 fois niveau de la cisaille en bonus sur votre jet de d'attaque pour la fabrication des armes (Artisanat arme).<br>
<br>
l) <a name="alene_artisan"></a> « Alène d’artisan ». Apporte 5 fois le niveau de l’alène en bonus à votre jet d’attaque pour la création des équipements de cuir et d'écaille (Artisanat du Cuir-Écaille).<br>
<br>
m) <a name="pince_depecer"></a> « Pince à dépecer ». Apporte un bonus lorsque vous dépecez et vous permet de récupérer une peau de 0 à 3 niveaux supérieur à la normale, suivant le niveau de la pince et de la créature.<br>
<br>
n) <a name="fouet_dressage"></a> « Fouet de dressage ». Apporte un bonus à votre jet de dressage lorsque vous utilisez la compétence « dresser » et vous permet de dompter une créature plus rapidement.<br>

-->
	<br>
	<br>
	<br>
	<br>




	<!--

<div class='subtitle' >Équipement Légendaire (ou "set" en anglais) :</div>
Si vous avez par exemple tous les équipements au complet de la Balance (casque, arme, armure, ...) vous avez un bonus supplémentaire (de x2) dans les caractéristiques de base de l'Équipement magique. Par exemple l'équipement au complet de la balance donnera un bonus supplémentaire en Attaque et en Esquive !
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
-->