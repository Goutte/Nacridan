<?php
require_once ("../../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

if (isset($_GET["formula_id"])) {
    $id = $_GET["formula_id"];
}
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">';
echo '<head>';

echo '<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>';
echo '<title>Nacridan</title>';
echo '<link rel="stylesheet" type="text/css" href="' . Cache::get_cached_file('/css/nacridanMain.css') .'" />';
echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
echo "<style type=\"text/css\">\n";

?>

</style>
</head>

<body class='rule_bg'>
	<div class="rule_content">

<?php
echo "<div class='rule_center' style='position:absolute; top: 100px'>";
echo "<div style='height: 5px;'><img src='" . CONFIG_HOST . "/pics/rules/GdeToile_TextureOmbre.jpg' alt=\"\"/></div>";
echo "<div class='rule_center_content'>";
$db = DB::getDB();

$str = "<H1 align='center'>";
switch ($id) {
    case 1:
        $str .= "Les missions d'escorte";
        $str .= "</H1>";
        $str .= "<p  class='rule_pg'>";
        $str .= "Les missions d'escorte s'obtiennent en discutant avec les clients de l'auberge.<br/> Déplacez-vous jusqu'à une auberge.<br/><br/>";
        $str .= "<img src='Auberge.png'></br></br>";
        $str . "Entrez dans le batiment en cliquant sur l'image puis en validant dans le menu contextuel apparu en bas de page.<br>";
        $str .= "Rendez-vous dans la Grande Salle. Au centre de la fenêtre, il y a quatre possibilités. L'une d'entre-elles vous permet de Discuter avec les clients pour 3PA.";
        $str .= " Effectuez cette action. Si vous êtes suffisamment chanceux, vous allez obtenir un parchemin de mission disponible dans votre équipement. Allez le lire. Cliquez dans sur votre équipement puis sur le parchemin. Une fenêtre s'ouvre vous expliquant le but de votre mission. <br/><br/>";
        $str .= "Pour démarrer la mission, dans le menu objet : Utilisez le parchemin pour 5PA. Un Personnage Non Joueur (PNJ) apparait alors dans la pièce. Dans le sélecteur en haut à droite où est marqué votre nom, vous pouvez accéder à ce personnage en le sélectionnant. En basculant sur ce personnage, vous pouvez le contrôler de la même manière que le vôtre.";
        $str .= " Déplacez-le alors jusqu'au point de RDV indiqué sur le parchemin de mission. Votre propre personnage doit également s'y rendre et utiliser à nouveau le parchemin pour 5pa pour recevoir la récompense.</br>";
        $str .= " Attention : la mission est à durée limitée. Une mission non réussie (temps dépassé ou escorte tuée) entraine une perte de 0,25 points en réputation de marchand.</br>";
		break;
    
    case 2:
        $str .= "Apprendre un premier savoir-faire d'extraction";
        $str .= "</H1>";
        $str .= "<p  class='rule_pg'>";
        $str .= "Aux alentours des grandes villes, vous pouvez trouver des émeraudes, du lin et des buissons. Le bois se récolte presque partout. Vous pouvez donc apprendre indifféremment l'un de ces quatre savoir-faire afin de récolter rapidement quelques matières premières.";
        $str .= " N'apprenez pas le savoir-faire 'dépecer' car vous ne pourrez vous en servir dès le début. <br/>";
        $str .= "Les émeraudes se revendent cher mais sont plus difficiles à trouver que le lin ou les buissons. Le bois peut se récolter n'importe ou (même en plaine) mais sa valeur à la revente est faible<br/><br/>";
        $str .= "Lisez rapidement l'onglet des savoir-faire d'extraction dans les règles afin d'apprendre où sont situées les différentes ressources par rapport aux grandes villes. Il peut être intéressant d'apprendre le savoir-faire d'extraction qui s'accorde avec l'artisanat correspondant au profil auquel vous vous destinez<br/><br/>";
        $str .= "Dirigez-vous donc vers une école de métiers et entrez dans la salle des classes.<br><br>";
        $str .= "<img src='EcoleMetier.png'></br></br>";
        $str .= "Sélectionnez au centre de la page le savoir-faire que vous souhaitez apprendre et validez.";
        $str .= " L'action est désormais disponible dans le menu Savoir-faire à droite.<br/><br/>";
        $str .= "Sortez alors de la ville et dirigez-vous dans la direction indiquée dans les règles. Vous apercevrez rapidement les ressources.</br>";
        $str .= "Notez que vous pouvez aussi décider d'investir dans un outil en vous rendant dans l'Armurerie standard de l'échoppe. <br/>";
        break;
    
    case 10:
        $str .= "Les missions commerciales";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Description </div>";
        $str .= "<p  class='rule_pg'>";
        $str .= "Lorsque vous organisez une mission commerciale, vous devez d'abord choisir un comptoir commercial de destination où vous devrez acheminer les marchandises
			 (<b>attention</b> que le village ne soit pas fortifié ou assurez-vous que son gouverneur est prêt à vous ouvrir les portes).";
        $str .= " Ensuite vous devez choisir les marchandises que vous souhaitez transporter. Vous ne pouvez pas chosir plus de 3 types d'objet différent. <br/><br/>";
        $str .= "Une fois la mission validée, vous recevez un parchemin qui en résume les détails, et notamment le temps qui vous est accordé pour atteindre votre destination.
			 La Tortue Géante, dont la vaste carapace creuse contient et protège vos marchandises, attend dans l'écurie. ";
        $str .= "Pour monter dans la tortue, utilisez le menu Action (à gauche) pour 'Chevaucher la tortue (2PA)'.<br/><br/>";
        $str .= "Vous conduisez ensuite votre tortue de l'intérieur (votre personnage n'est plus visible) en vous déplaçant normalement depuis l'interface de votre personnage
			 et non celui de la tortue. Notez que la tortue n'a pas de PA et ce sont toujours vos PA qui sont utilisés.<br><br/>";
        $str .= "Une fois le comptoir de destination atteint, rendez-vous à nouveau dans l'écurie et descendez de la tortue (menu Action à gauche 'descendre de la tortue (1PA)').
			 Entrez dans la pièce Caravanserail et déchargez votre tortue. Vous avez fini votre mission. <br/><br/>";
        $str .= "Prenez garde, hors des royaumes, les tortues sont attaquées par les monstres tout comme le serait votre personnage : surveillez la vie de votre tortue depuis son interface
			 et organisez éventuellement vos premières missions entre deux comptoirs d'un royaume protégé, pour ne pas prendre de risques.<br/><br/>";
        $str .= "Vous pouvez trouver plus d'informations concernant la carrière de commerçant sur la page consacrée aux <a href='rules.php?page=step15'>Tutoriels</a>.<br/>";
        
        $str .= "<div class='subtitle'> Les principaux éléments </div>";
        $str .= "<p  class='rule_pg'>";
        $str .= "- Le niveau maximum de la caravane que vous pouvez organiser est égale à votre réputation de marchand.<br/>";
        $str .= "- La valeur totale des marchandises que vous pouvez achalander est égale à 60+100*(Niveau_Caravane -1).<br/>";
        $str .= "- Le nombre d'objets transportés est compris entre 10 minimum et 30 maximum. <br/>";
        $str .= "- La distance minimale à parcourir est min(400 , 15 + 6*Niveau_Caravane). La distance maximum est 80+6*Niveau_Caravane<br/>";
        $str .= "- Le bénéfice engendré par une mission menée à bien est (Valeur chargement)*Distance/100 <br/>";
        $str .= "- Le gain de réputation de marchand pour une mission menée à bien est de 0,25point.<br/>";
        $str .= "- Vous pouvez annuler votre mission dans le comptoir de départ sans perte de réputation et avec récupération totale de votre mise.<br/>";
        $str .= "- Le temps accordé pour réussir votre mission est 3*Dist/24 où Dist est la distance à vol d'oiseau entre les deux comptoirs.<br/>";
        $str .= "- Lorsque vous dépasser le délai imparti pour la réussite de votre mission, votre réputation de marchand chute de 0,2 point par tour. Lors du déchargement, vous récupérez tout de même 0,25point de réputation et votre mise de départ sans bénéfice.<br/>";
        $str .= "- Si votre tortue est tuée, les marchandises achalandés tombent au sol. Votre mission est annulée et vous perdez 0,8point de réputation.<br/>";
        $str .= "- Les tortues sont généralement plus lentes que votre personnage. Leurs déplacements nécessitent 1PA de plus que vous n'en auriez eu besoin, a l'exception des déplacements dans les marais dont les tortues sont originaires et dans lesquels elles sont très à l'aise et où leur déplacement ne coutent qu'un 1PA contre 2 pour un personnage classique.<br/>";
        $str .= "- Au-delà du niveau 4 de la réputation de marchand, il ne sera plus possible que la ville de départ et la ville d'arrivée se trouvent dans le même royaume.<br/>";
        break;
    
    case 11:
        $str .= "Envoyer un colis";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Description </div>";
        $str .= "<p  class='rule_pg'>";
        $str .= "Lorsque vous envoyer un colis, vous devez sélectionner un comptoir de destination, les objets de votre inventaire que vous souhaitez envoyés, le peronnage destinataire qui sera le seul abilité à récupérer le colis et l'argent qu'il devra payer pour récupérer les colis, cette argent sera automatiquement transféré sur votre compte en banque. <br/>";
        $str .= "Dès la validation, le colis est téléporté et le destinataire peut immédiatement récupérer les objets.<br/>";
        $str .= "Dans la partie expédition en attente, l'expéditeur peut récupérer le colis envoyé en ne payant que le double des frais d'envoi mais seulement si le colis n'a pas été récupéré avant.<br/><br/>";
        $str .= "Note : Le rayon d'action d'un comptoir est donné par 40*Niveau_Comptoir. Si les rayons d'action de deux comptoirs s'entrecoupent alors les deux comptoirs sont reliés et vous pouvez tranférer des colis de l'un à l'autre.<br/><br/>";
        $str .= "Note2 : Le prix maximum possible de vente est limité à 2.5 fois le prix d'achat en échoppe.<br/>";
        break;
    
    case 12:
        $str .= "Message commercial";
        $str .= "</H1>";
        $str .= "<p  class='rule_pg'>";
        $str .= "A faire <br/>";
        break;
    
    case 15:
        $str .= "Formule de PX";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Description </div>";
        $str .= "<p  class='rule_pg'>";
        $str .= "La formule est la suivante: (5 + floor(niveau/4) + floor(niveau/10) +max(0, niveau-10))*(5 +niveau) + max(0, 2*(niveau-100))";
        $str .= "<table class='coloredtable'>";
        $str .= "<tr><th align='center' style='width: 80px;'>Niveau</th><th align='center'  style='width: 80px;'> Nombre de PXs </th><th align='center'  style='width: 80px;'> Coef Jauge </th></tr>";
        $str .= "<tr><td colspan=\"2\">&nbsp;</td></tr>";
        for ($i = 2; $i <= 100; $i ++) {
            $pxValue = getXPLevelUP($i);
            $str .= "<tr><td align='center'>" . $i . "</td><td align='center'>" . $pxValue . "</td><td align='center'>" . $pxValue / (($i + 5) * 5) . "</td>";
        }
        $str .= "</table><br/>";
        break;
    
    case 21:
        $str .= "Les Bâtiments";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Prix & Amélioration</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<table class='coloredtable'>";
        $str .= "<tr><th align='left' style='width: 220px;'>Nom</th><th> Niveau 1 </th><th> Niveau 2 </th><th> Niveau 3 </th><th> Niveau 4 </th><th> Niveau 5 </th><th> Niveau 6 </th><th> Niveau 7 </th><th> Niveau 8 </th></tr>";
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE (id=14 OR id<12) AND id!=2 AND id!=3", $db);
        
        while (! $dbb->eof()) {
            switch ($dbb->get("solidity")) {
                case "Fragile":
                    $arr = FRAGILE . " PS";
                    break;
                case "Medium":
                    $arr = MEDIUM . " PS";
                    break;
                case "Solid":
                    $arr = SOLID . " PS";
                    break;
            }
            $price = $dbb->get("price");
            if ($dbb->get("id") == 14) {
                $arr = "indestructible";
                $price = " / ";
            }
            if ($dbb->get("id") == 11)
                $price = " / ";
            
            $str .= "<tr><td class='alignleft'>" . $dbb->get("name") . " (" . $arr . ")</td>";
            $str .= "<td width='9%' class='aligncenter'>" . $price . "</td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 1)) . "</td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 2)) . " </td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 3)) . "</td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 4)) . "</td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 5)) . "</td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 6)) . "</td>";
            $str .= "<td width='9%' class='aligncenter'>" . floor($dbb->get("price") * pow(3 / 2, 7)) . "</td></tr>";
            
            $dbb->next();
        }
        $str .= "</table><br/>";
        $str .= "<div class='subtitle'> Amélioration d'une école</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous ordonnez la construction d'une école de combat ou d'une école de magie, celle-ci est est de niveau 1 et dispense les compétences ou les sorts élémentaires. Améliorer cette école d'un niveau, vous permet d'ouvrir un nouveau cours c'est à dire les cours de niveau Aspirant d'un des quatre grands domaines de combat ou de magie. Vous ne pouvez ouvrir les cours de niveau Adepte d'un domaine que lorsque votre école possède déjà les cours de niveau Aspirant de ce même domaine. <br/><br/>";
        $str .= "Lorsque vous ordonnez la construction d'une école des métiers, celle-ci est de niveau 0 et ne dispence aucun cours. Il faudra l'améliorer à nouveau pour la passer au niveau 1 et obtenir la possibilité d'ouvrir les enseignements de 5 nouveaux métiers. Vous devrez donc payer 150PO pour construire le bâtiment puis à nouveau 150po pour passer l'école au niveau 1 et ouvrir 5 cours.<br/><br/>";
        
        break;
    
    case 98:
        
        break;
    
    case 99:
        $str .= "Tir à l'arc";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Calcul des dégâts (TAG#99)</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Caractéristique principale :</b> Dextérité <br/>";
        $str .= "<b>Détails :</b><br/>";
        $str .= " Delta = JATT - JDEF <br>";
        $str .= " Si Delta < JATT/3 alors Dégâts = 0.45*Delta <br>";
        $str .= " Si JATT/3 < Delta < 2*JATT/3  alors Dégâts = 0.45*JATT/3 + 0.35*min(Delta - JATT/3, JATT/3) <br>";
        $str .= " Si 2*JATT/3 < Delta  alors Dégâts = 0.45*JATT/3 + 0.35*min(Delta - JATT/3, JATT/3) + 0.25*(Delta - 2*JATT/3)  <br>";
        
        $str .= "<b>Progression :</b> 8 points de dextérité (TAG#enhance)<br/>";
        
        $str .= "</p>";
        
        break;
    
    case 199:
        $str .= "Réussir une compétence";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous tentez une compétence, vous jetez un D100. Si celui-ci est supérieur à votre maîtrise de la compétence, vous ratez votre action et vous perdez min(3; Temps Compétence) PA</p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 50% de chance de réussir cette compétence <br/>";
        $str .= "Votre score est de ............: 71 <br/>";
        $str .= "Vous avez raté votre compétence.<br/>";
        $str .= "Cette action vous a coûté 3 PA.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Si votre D100 est inférieur à votre jet de maîtrise, la compétence est réussie et va s'effectuer. Avant vous jetez un deuxième D100 pour savoir si vous améliorez votre pourcentage de maîtrise<br/>";
        $str .= "Si votre D100 est supérieur à votre maîtrise de la compétence, vous gagnez 1% de maîtrise </p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 68% de chance de réussir cette compétence <br/>";
        $str .= "Votre score est de ............: 13         Compétence réussie.<br/>";
        $str .= "Votre jet d'amélioration est de 90, vous améliorez votre compétence de 1%.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que votre maîtrise d'une compétence hors bonus ne peut dépasser 90%. Une fois cette valeur atteinte, il n'y a plus de jet d'amélioration <p/>";
        
        $str .= "<div class='subtitle'> Concentration </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous ratez une compétence, vous obtenez pour le tour en cours, un bonus de maîtrise pour retenter cette compétence <br/>";
        $str .= "Ce bonus dépend de votre pourcentage de maîtrise et du niveau de la compétence comme le montre la formule suivante. <br/>";
        $str .= "Bonus = (100- pourcentage de maîtrise)/(2+ niveau de la comp).<br/>";
        $str .= "Une compétence élémentaire est considérée de niveau 0, une compétence de niveau aspirant est considérée comme niveau 1 et une compétence de niveau adepte de niveau 2.</p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 92% de chance de réussir cette compétence (4% bonus) <br/>";
        $str .= "Votre score est de ............: 13         Compétence réussie.<br/>";
        $str .= "Votre jet d'amélioration est de 90, vous améliorez votre compétence de 1%.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici vous avez tenté une première fois une compétence de niveau aspirant que vous maîtrisez à 88%. En la retentant, vous bénéficiez de 4% de bonus (100-88)/3. <br/>";
        $str .= "Notez que dans ce cas, le jet d'amélioration a toujours lieu car votre maîtrise propre indépendamment du bonus est inférieure à 90% et un jet supérieur à 88% soit votre maîtrise propre sans bonus suffit pour améliorer votre maîtrise.</p><br/>";
        
        break;
    
    case 198:
        $str .= "Niveau des compétences et maîtrise";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Les compétences élémentaires <small> (niveau 0) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les compétences élémentaires sont les premières que vous pourrez apprendre. Elles sont au nombre de 4.<br/>";
        $str .= "Elles sont accessibles dans n'importe quelle école de combat de niveau 1. Au début du monde vous ne trouverez
			 de telle écoles que dans les grandes villes, l'apprentissage vous coûtera alors <b> 80 pièces d'or </b> et <b> 12 points d'action</b>. Par la suite, certains personnages prendront
			 le contrôle de village et pourront alors construire de nouvelles écoles, y fixer un autre prix d'apprentissage et même baisser le temps d'apprentissage en améliorant
			 leurs écoles. <small>(vous trouvez plus de détails concernant l'amélioration des écoles dans la section traitant des bâtiments)</small><br/><br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Les compétences de niveau aspirant <small>(niveau 1) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les compétences de niveau aspirant correspondent aux compétences du premier niveau de chacune des 4 grandes écoles de combat :<br/>";
        $str .= "- L'école de la Garde d'Octobian.<br/>";
        $str .= "- L'école de l'ombre.<br/>";
        $str .= "- L'école des Archers de Krima.<br/>";
        $str .= "- L'école des Paladins de Tonak.<br/><br/>";
        $str .= "Pour apprendre une compétence de niveau aspirant, vous devrez maîtriser une compétence élémentaire ou un sort élémentaire <b> à un minimum de 80%</b>.<br/>";
        $str .= "A la différence des compétences élémentaires, toutes les écoles de combat ne dispensent les cours pour apprendre les compétences de niveau aspirant. Il vous faudra trouver une école de combat qui possèdent les
			 cours de niveau aspirant de l'école que vous souhaitez suivre.<br/>";
        $str .= "Au début du monde, à l'exception des compétences de l'école de l'ombre, les compétences de niveau aspirant des trois autres écoles sont disponibles dans toutes les grandes villes et
			 en partiulier dans les villes des royaumes de départ comme Artasse, Earok ou Tonak. Leur apprentissage vous coûtera alors <b> 400 pièces d'or et 12 points d'action</b><br/>";
        $str .= "<div class='subtitle'> Les compétences de niveau adepte <small>(niveau 2) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les compétences de niveau adepte correspondent aux compétences du deuxième niveau de chacune des 4 grandes écoles de combat :<br/>";
        $str .= "Pour apprendre une compétence de niveau adepte d'une école, vous devrez maîtriser <b> deux compétences de niveau aspirant de cette même école à 90%. </b><br/>";
        $str .= "Les écoles qui dispensent les cours de niveau adepte sont plutôt rares au début du monde. Vous ne pourrez trouver ces compétences que dans la grande ville spécialiste de l'école que vous recherchez :<br/>";
        $str .= "- Octobian pour l'école de la Garde d'Octobian.<br/>";
        $str .= "- Krima pour l'école des Archers de Krima.<br/>";
        $str .= "- Nephy pour l'école de l'Ombre.<br/>";
        $str .= "- Tonak pour l'école des Paladins de Tonak.<br/>";
        $str .= "Dans ces écoles, l'apprentissage des compétences de niveau adepte vous coûtera alors <b> 800 pièces d'or et 12 points d'action.</b><br/><br/>";
        
        break;
    
    case 298:
        $str .= "Niveau des sortilèges et maîtrise";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Les sortilèges élémentaires <small> (niveau 0) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les sortilèges élémentaires sont les premièrs que vous pourrez apprendre. Ils sont au nombre de 4.<br/>";
        $str .= "Ils sont accessibles dans n'importe quelle école de magie de niveau 1. Au début du monde vous ne trouverez
			 de telle écoles que dans les grandes villes, l'apprentissage vous coûtera alors <b> 80 pièces d'or </b> et <b> 12 points d'action</b>. Par la suite, certains personnages prendront
			 le contrôle de village et pourront alors construire de nouvelles écoles, y fixer un autre prix d'apprentissage et même baisser le temps d'apprentissage en améliorant
			 leurs écoles. <small>(vous trouvez plus de détails concernant l'amélioration des écoles dans la section traitant des bâtiments)</small><br/><br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Les sortilèges de niveau aspirant <small>(niveau 1) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les sortilèges de niveau aspirant correspondent aux sortilèges du premier niveau de chacune des 4 grandes écoles de magie :<br/>";
        $str .= "- L'école de guérison.<br/>";
        $str .= "- L'école de protection.<br/>";
        $str .= "- L'école de destruction.<br/>";
        $str .= "- L'école d'altération.<br/><br/>";
        $str .= "Pour apprendre un sortilège de niveau aspirant, vous devrez maîtriser une compétence élémentaire ou un sort élémentaire <b> à un minimum de 80%</b>.<br/>";
        $str .= "A la différence des sortilèges élémentaires, toutes les écoles de magie ne dispensent pas les cours pour apprendre les sortilèges de niveau aspirant. Il vous faudra trouver une école de combat qui possèdent les
			 cours de niveau aspirant de l'école que vous souhaitez suivre.<br/>";
        $str .= "Au début du monde, les sortilèges de niveau aspirant des quatres écoles sont disponibles dans toutes les grandes villes et
			 en partiulier dans les villes des royaumes de départ comme Artasse, Earok ou Tonak. Leur apprentissage vous coûtera alors <b> 400 pièces d'or et 12 points d'action</b><br/>";
        $str .= "<div class='subtitle'> Les sortilèges de niveau adepte <small>(niveau 2) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les sortilèges de niveau adepte correspondent aux sortilèges du deuxième niveau de chacune des 4 grandes écoles de magie:<br/>";
        $str .= "Pour apprendre un sortilège de niveau adepte d'une école, vous devrez maîtriser <b> deux sortilèges de niveau aspirant de cette même école à 90%. </b><br/>";
        $str .= "Les écoles qui dispensent les cours de niveau adepte sont plutôt rares au début du monde. Vous ne pourrez trouver ces sortilèges que dans la grande ville spécialiste de l'école que vous recherchez :<br/>";
        $str .= "- Izandar pour l'école de guérison.<br/>";
        $str .= "- Djin pour l'école de protection.<br/>";
        $str .= "- Landar pour l'école de destruction.<br/>";
        $str .= "- Dust pour l'école d'altération.<br/>";
        $str .= "Dans ces écoles, l'apprentissage des sortilèges de niveau adepte vous coûtera alors <b> 800 pièces d'or et 12 points d'action.</b><br/><br/>";
        
        break;
    case 100:
        $str .= "Les compétences élémentaires";
        $str .= "</H1>";
        $str .= "<p class='rule_pg'>";
        
        $str .= "Les compétences élémentaires sont les premières que vous pourrez apprendre. Elles sont au nombre de 4.<br/>";
        $str .= "Chacun d'entre-elles fait appel à des caractéristiques de bases propres à votre personnage et leurs utilisations le feront évoluer dans ce sens.<br/><br/>";
        $str .= "Les compétences élémentaires sont accessibles dans n'importe quelle école de combat de niveau 1. Au début du monde vous ne trouverez
			 de telle écoles que dans les grandes villes, l'apprentissage vous coûtera alors <b> 80 pièces d'or </b> et <b> 12 points d'action</b>. Par la suite, certains personnages prendront
			 le contrôle de village et pourront alors construire de nouvelles écoles, y fixer un autre prix d'apprentissage et même baisser le temps d'apprentissage en améliorant
			 leurs écoles.<br/><br/>";
        $str .= "<b> maîtrisez  à 80% </b> l'une des 4 compétences élémentaires donne accès à l'apprentissage des compétences de niveau aspirant de n'importe quelle école de combat.<br/>";
        $str .= "</p>";
        break;
    
    case 104:
        $str .= "Dégâts Accrus";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Description </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Dégâts accrus est une attaque au contact qui permet d'augmenter les dégâts infligés à votre cible en contre partie d'un malus sur votre jet d'Attaque.<br/>";
        $str .= "A réserver sur les cibles que vous touchez facilement, dégâts accrus est la compétence idéale pour viser les records de dégât et fera le bonheur de tous les barbares.<br/>";
        $str .= "<div class='subtitle'> Résumé </div>";
        
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Ecole Elémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de combat.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> JAtt = (80% des D6 Attaque) + BM Attaque. JDeg = 2D6 + (120% des D6 Dégâts) + BM Dégâts.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Dégats Accrus, la résolution est exactement la même que lors d'une attaque normale. Seuls vos jets d'attaque et de dégât sont modifiés.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Shaman Gobelin.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 104.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 51.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 13).<br/>";
        $str .= "Votre Jet de dégâts est de ............: 116.<br/>";
        $str .= "Vous lui infligez 129 points de dégâts.<br/>";
        $str .= "Son armure le protége et il ne perd que 125 points de vie.</p><br/><br/>";
        
        $str .= "Ici 104 n'est pas votre jet d'attaque classique, il a été modifié par la compétence de même que 116 est votre jet de dégâts modifié.";
        $str .= " Aussi, il vous est impossible de savoir exactement de combien votre jet d'attaque et de dégâts ont été modifiés par la formule. Vous pouvez seulement comparer par rapport à votre moyenne classique.<br/><br/>";
        $str .= "<div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "La compétence ne modifie que les D6 de vos valeurs d'attaque et de dégâts.<br/>";
        $str .= "Imaginons que votre personnage ait les caractéristiques suivantes :<br/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td> Caractéristique </td> <td> Base </td><td> BM </td><td> Total </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Attaque </td><td>  8D6 - 2   </td><td> 2D6 + 4 </td><td> 10D6 + 2 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Dégâts </td><td>  22D6 + 6   </td><td> -2D6 </td><td> 20D6 + 6 </td></tr></table>";
        $str .= "<br/>Avec Dégât Accrus vous ne jetez que 80% des D6 de votre attaque et vous jetez 120% des D6 de vos dégâts + 2D6.<br/>";
        $str .= "Vous obtenez donc les valeurs suivantes : <br/><br/>";
        $str .= "<table width='700px' class='maintable'><tr  align='center'class='mainbglabel'><td> Caractéristique </td> <td> Attaque normale </td><td> Dégâts accrus </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Attaque </td><td>  10D6 +2  </td><td> 8D6 + 2 </td/></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Dégâts </td><td>  20D6 + 6   </td><td> 26D6+6 </td></tr></table>";
        $str .= "<br/><br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire <br/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 6 </td><td> 3 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 4 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 3 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 2 </td><td> 0 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <br/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 1 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 2 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 102:
        $str .= "Garde";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Compétence passive personnelle.<br/>";
        $str .= "<b>Niveau : </b> Ecole Elémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de combat.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse + caractéristique du bouclier porté.<br/>";
        $str .= "<b>Effets : </b> Bonus Défense :  [JDef sans bonus)/8+1]/2 D6";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "La résolution de la compétence garde est assez simple.";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Garde <br/>";
        $str .= "Jet de Défense sans bonus est de ............: 84 <br/>";
        $str .= "Votre défense est donc augmentée de 5,5D6 pour ce tour et le suivant </p>";
        $str .= "Explication : 84/8 = 10,5 --> 10 arrondi à l'entier inférieur --> 11 en ajoutant 1 --> 5,5 en divisant par 2.</p><br/>";
        
        $str .= "<div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "Le jet de défense intervenant dans le calcul des bonus octroyés par compétence est un jet sans bonus.<br/>";
        $str .= "Imaginons que votre personnage ait les caractéristiques suivantes :<br/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td> Caractéristique </td> <td> Base </td><td> BM </td><td> Total </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Défense </td><td>  8D6 - 2   </td><td> 2D6 + 4 </td><td> 10D6 + 2 </td></tr></table>";
        $str .= "<br/>Lorsque vous utilisez cette compétence vous n'allez jetez que votre Base de défense soit 8D6-2.<br/>";
        $str .= "Le bonus que vous obtenez s'ajoute dans les BM de la case défense.<br/><br/>";
        
        $str .= "<br/><br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Sans bouclier </td><td > 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Petit Bouclier </td><td > 0 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Bouclier moyen </td><td > 0 </td><td> 2 </td><td> 3 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Bouclier large </td><td > 2 </td><td> 0 </td><td> 3 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "</p>";
        break;
    case 103:
        $str .= "Premiers soins";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Compétence de soutien au contact.<br/>";
        $str .= "<b>Niveau : </b> Ecole Elémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de combat.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité.<br/>";
        $str .= "<b>Progression :</b> 5 points de dextérité<br/>";
        $str .= "<b>Effets : </b> Soins potentiels sur une cible humanoïde = (JDex/8+1)/2 D6";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "La compétence Premiers Soins ne permet de soigner une cible que jusqu'à 75% de ses Points de Vie maximum. Il se peut donc que votre maîtrise de compétence eusse permis d'apporter plus de points de vie à votre cible comme dans l'exemple suivant. ";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Premiers Soins sur un Gnoll  <br/>";
        $str .= "Votre Jet de Dextérité est de ............: 84 <br/>";
        $str .= "Vous réalisez une maîtrise de compétence de niveau 11 (5,5D6 pv). <br/>";
        $str .= "Vous apportez 23 points de vie à votre cible.<br/>";
        $str .= "Gnoll est soigné de 12 points de vie.</p>";
        $str .= "Explication : 84/8 = 10,5 --> 10 arrondi à l'entier inférieur --> 11 en ajoutant 1 --> jet(5,5D6) = 23<br/>";
        $str .= "Ici la vie du gnoll était de 63pv sur 100. Premiers soins ne permet donc de guérir le gnoll que jusqu'à 75pv, c'est à dire en apportant les 12 points manquants.<br/>";
        $str .= "Comprenez donc bien que si vous apportez les premiers soins à une cible dont la vie est au dessus de 75% de son maximum, vous ne lui apporterez aucun point de vie supplémentaire.<br/>";
        $str .= "Remarque : la compétence Premiers soins ne permet de soigner que des cibles de type humanoïde.</p><br/>";    
        break;
    case 128:
        $str .= "Botte d'estoc";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Ecole Elémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de combat.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse.<br/>";
        $str .= "<b>Effets : </b> Dégâts supplémentaires fonction du coup critique, soit bonus de dégâts * (min (120, (50+JVit))/100)";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Pour effectuer cette compétence, vous devrez avoir une griffe équipée dans votre main gauche (attention, elle sera prise en compte dans votre charge de plate). Lorsque vous utilisez Botte d'Estoc, un jet de vitesse est ajouté à la résolution d'une attaque normale pour définir le pourcentage par lequel votre bonus de dégât est multiplié et ainsi définir un nouveau bonus de dégât.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez une araignée piègeuse.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 118.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 37.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 20).<br/><br/>";
        $str .= "Votre Jet de vitesse est de ............: 69.<br/>";
        $str .= "Bonus de la griffe : 119%.<br/>";
        $str .= "Votre bonus de dégât est donc de 44.<br/><br/>";
        $str .= "Votre Jet de dégâts est de ............: 25.<br/>";
        $str .= "Vous lui infligez 69 points de dégâts.<br/>";
        $str .= "Son armure le protége et il ne perd que 62 points de vie.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici vous devez comprendre que votre bonus de dégât initial de 20 points (rappel : (JAtt-JDef)/4) se transforme en 44 points car on ajoute à ce dernier le bonus de la griffe soit 119% de sa valeur (20 + 20*119/100 = 44 arrondi à l'entier supérieur). <br/>";
        $str .= "Un jet de dégat classique est lancé pour définir l'ensemble de vos dégâts comme lors d'une attaque normale.<br/>";
        $str .= "Pour utiliser cette compétence à bon escient, il est donc nécessaire d'une part d'avoir une bonne attaque pour obtenir un bonus de dégâts initial grand et d'autre part une bonne vitesse pour s'octroyer un bon pourcentage bonus.</p><br/>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire <br/>";
        
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 3 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 4 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 1 </td><td> 2 </td><td> 4 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 1 </td><td> 2 </td><td> 2 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <br/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 2 </td><td> 1 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 0 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 0 </td><td> 0 </td><td> 3 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 0 </td><td> 0 </td><td> 2 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 101:
        $str .= "Attaque puissante";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de la Garde d'Octobian.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau aspirant de l'école de la Garde D'Octobian.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> JAtt = JAtt classique + 2D6 + (20% des D6 Dégâts seuls, c'est à dire BM Dégâts exclus !) + BM Attaque";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Attaque puissante, la résolution est exactement la même que lors d'une attaque normale. Seul votre jet d'attaque est modifié.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Griffon.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 384.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 302.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 20).<br/>";
        $str .= "Votre Jet de dégâts est de ............: 216.<br/>";
        $str .= "Vous lui infligez 236 points de dégâts.<br/>";
        $str .= "Son armure le protége et il ne perd que 190 points de vie.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici 384 n'est pas votre jet d'attaque classique, il a été modifié par la compétence.";
        $str .= "Aussi, il vous est impossible de savoir exactement de combien votre jet d'attaque a été modifié par la formule. Vous pouvez seulement comparer par rapport à votre moyenne classique.</p><br/>";
        
        $str .= "<div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le pourcentage bonus  n'est calculé que sur les D6 de votre valeur de dégât et est ajouté à votre attaque.<br/>";
        $str .= "Imaginons que votre personnage ait les caractéristiques suivantes :</p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td> Caractéristique </td> <td> Base </td><td> BM </td><td> Total </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Attaque </td><td>  8D6 - 2   </td><td> 2D6 + 4 </td><td> 10D6 + 2 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Dégâts </td><td>  22D6 + 6   </td><td> -2D6 </td><td> 20D6 + 6 </td></tr></table>";
        $str .= "<p class='rule_pg'>";
        $str .= "<br/>Avec Attaque puissante vous allez jetez vos valeurs d'attaque plus 2D6 plus 20% des D6 de votre valeur de dégâts.<br/>";
        $str .= "Vous obtenez donc les valeurs suivantes : <p/>";
        $str .= "<table width='700px' class='maintable'><tr  align='center'class='mainbglabel'><td> Caractéristique </td> <td> Attaque normale </td><td> Attaque puissante </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Attaque </td><td>  10D6 +2  </td><td> 16D6 + 2 </td/></tr></table>";
        $str .= "<p class='rule_pg'>";
        $str .= "En effet, 20/100*20D6 + 2D6 = 6D6. La compténce vous octroie donc 6D6 bonus d'attaque.<br> Vos dégâts reste inchangés.<p/>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 6 </td><td> 3 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 4 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 3 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 2 </td><td> 0 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 1 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 2 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 105:
        $str .= "Tournoiement";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de la Garde d'Octobian.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau aspirant de l'école de la Garde D'Octobian.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque +1 (Arme légère --> 8PA - Arme intermédiaire --> 9PA - Arme lourde --> 10PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> Toutes les cibles des cases adjacentes sont attaqués. JDeg = 80% du JDeg total (c'est à dire, BM dégâts inclus !)";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Tournoiement, vous jetez une seule fois vos valeurs d'attaque et de dégâts et ces jets seront valables pour toutes les cibles attaqués.";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Tournoiement.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 105 <br/>";
        $str .= "Votre Jet de dégâts est de ............: 62 <br/><br/>";
        $str .= "Le Jet de défense de Manticore est de ............: 33 <br/>";
        $str .= "Vous l'avez touché. Il perd 59 points de vie.<br/><br/>";
        $str .= "Le Jet de défense de Serpent Aqualien est de ............: 110 <br/>";
        $str .= "Shaman Gobelin a esquivé votre attaque. <br/><br/>";
        $str .= "Le Jet de défense de Abomination des marais est de ............: 39<br/>";
        $str .= "Vous l'avez touché. Il perd 46 points de vie.<br/>";
        $str .= "Vous avez TUÉ votre adversaire.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que, même si elle n'est pas mentionnée, l'armure des cibles se déduit de vos dégâts expliquant les différences des dommages infligés aux différentes créatures.<br/>";
        $str .= "Remarquez également, vous ne bénéficiez pas du bonus de dégâts lié à la différence entre votre
			        attaque et la défense de l'adversaire comme dans le cas d'une attaque normale </p>";
        
        $str .= "<div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "A la différence des bonus et malus d'Attaque Puissante ou de Dégâts Accrus, dans le cas d'un tournoiement, vous lancez l'ensemble de vos valeurs de dégâts classiques puis vous calculez ensuite 80% de ce score pour obtenir les dégâts infligés par la compétence.<br/>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Dans le dans d'un Tournoiement, les jauges de progression évoluent de la même façon que vous touchiez ou non des cibles durant cette attaque </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 8 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 6 </td><td> 3 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 4 </td><td> 3 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 5 </td><td> 0 </td><td> 0 </td><td> 4 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        break;
    
    case 107:
        $str .= "Attaque perçante";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de la Garde d'Octobian.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau aspirant de l'école de la Garde D'Octobian.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> 80% de l'armure de la cible est ignoré. JDeg = (70% des D6 Dégâts) + BM dégâts. Vous infligez à votre cible une Blessure profonde = 20% de la perte de vie immédiate pendant 3 tours";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Attaque Perçante, la résolution est la même que lors d'une attaque normale. Seul un texte est ajouté pour vous notifier que l'armure est ignorée
			 et pour indiquer le nombre de points de dégât que subira votre cible sur les 3 prochains tours.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Combattant Squelette.<br/>";
        $str .= "Votre Jet d'Attaque est ............: 234.<br/>";
        $str .= "Le Jet de défense de votre adversaire est ............: 212.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 5).<br/>";
        $str .= "Votre Jet de dégâts est de ............: 156.<br/>";
        $str .= "Vous lui infligez 161 points de dégâts.<br/>";
        $str .= "Vous parvenez à trouver une faille dans l'armure de votre adversaire qui est reduit de 80%<br/>";
        $str .= "Il perd 148 points de vie <br/>";
        $str .= "Vous infligez à votre adversaire une blessure profonde de 29 points de vie pour les 3 prochains tours.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Il y a 13 points d'écart entre les dégats infligés et les points de vie perdus par la cible : l'armure de la cible est de 65 points (vous en avez donc ignorés 52 points).";
        $str .= "De plus, ici 161 correspond à votre jet de dégâts classique malusé par la compétence. Enfin, en prenant 20% (arrondi à l'entier inférieur) de la perte de vie infligée par l'attaque, soit 20% de 148, on retrouve la valeur de la blessure profonde de 29 points.</br></br>";
        $str .= "Si vous êtes victime de cette compétence, vous pouvez vérifier la valeur et le temps restant de la blessure profonde à la fin de votre profil <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='left' class='mainbglabel'><td colspan=2 > <b>BONUS/MALUS</b> </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='left'> Blessure Profonde </td><td align='left' > -29PV (encore 3 dla) </td></tr></table>";
        $str .= "<br/><div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le pourcentage malus n'est calculé que sur les D6 de votre valeur de dégât.<br/>";
        $str .= "Imaginons que votre personnage ait les caractéristiques suivantes :</p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td> Caractéristique </td> <td> Base </td><td> BM </td><td> Total </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Dégâts </td><td>  22D6 + 6   </td><td> -2D6 </td><td> 20D6 + 6 </td></tr></table>";
        $str .= "<p class='rule_pg'>";
        $str .= "<br/>Avec Attaque perçante vous n'allez jetez que 70% de vos D6 de dégâts.<br/>";
        $str .= "Vous obtenez donc les valeurs suivantes : <p/>";
        $str .= "<table width='700px' class='maintable'><tr  align='center'class='mainbglabel'><td> Caractéristique </td> <td> Attaque normale </td><td> Attaque Perçante </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td> Dégâts </td><td>  20D6 +6  </td><td> 14D6 + 6 </td/></tr></table>";
        $str .= "<p class='rule_pg'>";
        $str .= "En effet, 70/100*20D6 = 14D6. La compténce diminue donc vos dégâts de 6D6.<br>";
        $str .= "Notez que si les dégâts infligés ne dépassent pas 5 points de vie alors vous n'infligerez aucune blessure profonde.<p/>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 7 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 4 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 3 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 2 </td><td> 0 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 1 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 2 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 109:
        $str .= "Coup assommant";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de la Garde d'Octobian.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau adepte de l'école de la Garde D'Octobian.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> Malus de vitesse pour la cible à hauteur de (Perte de PV)/10 D6 arrondi à l'inférieur pour le tour en cours et les 2 suivants";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Coup assommant, la résolution est la même que lors d'une attaque normale. Seul une phrase vous indiquant le malus de vitesse infligé à la cible est ajouté.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Orc.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 171.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 102.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 17).<br/>";
        $str .= "Votre Jet de dégâts est de ............: 116.<br/>";
        $str .= "Vous lui infligez 133 points de dégâts.<br/>";
        $str .= "Son armure le protége et il ne perd que 97 points de vie.<br/>";
        $str .= "Vous avez étourdi votre adversaire. Il subit un malus de 9D6 de vitesse pour les deux prochains tours.</p>";
        $str .= "<div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que le calcul du malus de vitesse étant arrondi à l'inférieur, vous n'infligerez aucun malus si vos dégâts ne dépassent pas 10 points de vie.<br/>";
        $str .= "Rappel : la vitesse influe grandement sur la défense mais intervient également sur le nombre de PA perçus à chaque nouveau tour et peut également modifier la valeur d'attaque et les dégâts infligés suivant le type d'attaque utilisé.<br/><br/>";
        $str .= "Si vous êtes victime de cette compétence, vous pouvez vérifier la valeur et le temps restant du malus à la fin de votre profil <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='left' class='mainbglabel'><td colspan=2 > <b>BONUS/MALUS</b> </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='left'> Assommé </td><td align='left' > -9D6 vitesse (encore 2 tours) </td></tr></table>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 7 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 4 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 3 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 2 </td><td> 0 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 1 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 2 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 125:
        $str .= "Projection";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de la Garde d'Octobian.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau adepte de l'école de la Garde D'Octobian.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> projète la cible jusqu'à 2 cases en arrière";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Projection, la résolution est la même que lors d'une attaque normale.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Scorpion Géant.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 221.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 162.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 14).<br/>";
        $str .= "Votre Jet de dégâts est de ............: 176.<br/>";
        $str .= "Vous lui infligez 190 points de dégâts.<br/>";
        $str .= "Son armure le protége et il ne perd que 157 points de vie.<p/>";
        $str .= "Afin de déterminer si la cible est projetée, un jet [FOR + 1/2 DEX] entre l'attaquant et le défenseur est réalisé. Une phrase vous indiquant si votre adversaire a été projeté ou non est ajouté.<br/>";
        $str .= "Notez que si vous ne parvenez pas à toucher votre adversaire, il se sera pas projeté.<br/>";        
        $str .= "<div class='subtitle'> Sens de la projection </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<img src='Exemple.png'></br></br>";
		$str .= "<div class='subtitle'> Collisions</div>";
        $str .= "Si les cases sont occupés par un bâtiment, un personnage ou une exploitation, la créature projetée sera stoppée.<br/>";
        $str .= "Ici, rien n'empêche le scorpion de reculer et il se retrouvera sur la case en surlignance après l'attaque.<br/><br/>";

		$str .= "Suivant l'occupation des cases adjacentes, votre adversaire peut reculer de deux cases, d'une seule ou pas du tout. Plusieurs cas de figure sont possibles :<br/>";
        $str .= " - Cas n°1 : le champ est libre et votre adversaire est projeté en arrière de deux cases<br/>";
		$str .= " - Cas n°2 : votre adversaire est projeté en arrière mais rencontre un autre personnage. Il est alors déplacé jusqu'à entrer en contact avec cet obstacle.<br/>";
        $str .= "Des dégats supplémentaires sont infligés aux deux personnages : le projeté et l'obstacle. Ces dégats sont égaux à 30% des dégats reçus lors de l'attaque intiale, répartis sur les deux personnages.<br/>";
		$str .= "Exemple : le projeté a 10 points d'armure, l'obstacle n'en a que 5. Sur les 30% de dégats supplémentaires, le projeté prend (5/15)*30% soit 10% des dégats subis intialement. L'obstacle prendra (10/15)*30%, soit 20% des dégats subis intialement par le défenseur.<br/>";
		$str .= " - Cas n°3 : votre adversaire est projeté en arrière mais rencontre une structure ou un bâtiment. Il est alors déplacé jusqu'à entrer en contact avec cet obstacle.<br/>";
		$str .= "Des dégats supplémentaires sont infligés au personnage projeté. Ces dégats sont égaux à 30% des dégats reçus lors de l'attaque intiale.<br/>";
		$str .= "Si l'obstacle est un mur de terre ou un bâtiment d'une ville contrôlée par un personnage joueur, alors il perd (1/3 * PVmax du projeté) Points de Structure, arrondis à l'inférieur. <br/>";
		$str .= "<div class='subtitle'> Cas particuliers</div>";
        $str .= " - En cas de collision, un personnage embusqué redevient visible.<br/>";
        $str .= " - Si un personnage protégé par un autre (à l'aide de la compétence Protection)est la cible d'une Projection, c'est le protecteur qui sera projeté au moment de son interposition.<br/><br/>";
		$str .= " - La bulle de vie permet d'encaisser les dégats subis lors d'une collision.<br/>";
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 7 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 4 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 3 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 2 </td><td> 0 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 1 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 2 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 110:
        $str .= "Coup de Grâce";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de la Garde d'Octobian.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau adepte de l'école de la Garde D'Octobian.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Force.<br/>";
        $str .= "<b>Effets : </b> les dégâts infligés, arrondis à la dizaine inférieur, sont comptés en blessure mortelle. Ce malus est cumulable et ne peut être enlevé que par le sortilège Souffle d'Athlan (avec niveau de la malédiction = 1/10 de la blessure). Tant que le malus persite, la cible ne peut regagner les points de vie correpondant à la blessure mortelle.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Coup de Grâce, la résolution est la même que lors d'une attaque normale. Seul une phrase indiquant le nombre de points de dégât de la blessure mortelle est ajouté.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Golem d'Argile.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 114.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 98.<br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 4).<br/>";
        $str .= "Votre Jet de dégâts est de ............: 93.<br/>";
        $str .= "Vous lui infligez 97 points de dégâts.<br/>";
        $str .= "Son armure le protège et il ne perd que 65 points de vie dont 60 PV de blessure mortelle.<p/>";
        $str .= "Notez que si vous infligez moins de 10 points de vie à votre cible, elle ne reçoit aucun malus de blessure mortelle.";
        $str .= "Si vous êtes victime de cette compétence, vous pouvez vérifier la valeur de la blessure mortelle à la fin de votre profil <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='left' class='mainbglabel'><td colspan=2 > <b>BONUS/MALUS</b> </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='left'> Blessure mortelle </td><td align='left' > -60PV  </td></tr></table>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 7 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 4 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 3 </td><td> 2 </td><td> 2 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 2 </td><td> 0 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 4 </td><td> 1 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 2 </td><td> 2 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 1 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 2 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 106:
        $str .= "Aiguise-Lame";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> action sur objet<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de l'Ombre.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école de l'Ombre.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse <br/>";
        $str .= "<b>Effets : </b> Bonus dégât de l'arme :  JVit/5 points de dégâts arrondi à l'entier supérieur.<br/>";
        $str .= "<b>Progression : </b> 5 points de vitesse.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous aiguisez votre Masse d'armes <br>";
        $str .= "Votre Jet de Vitesse est de ............: 82 <br/>";
        $str .= "Les dégâts de l'arme sont augmentées de 17 point(s).<p/><br/>";
        
        $str .= "<div class='subtitle'> Précision sur le calcul des bonus/malus </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le bonus octroyé par Aiguise-Lame se dissipe dès que vous avez frappé.<p/>";
        break;
    
    case 117:
        $str .= "Vol à la Tire";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Action au contact sur personnage joueur uniquement<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de l'Ombre.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école de l'Ombre.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA<br/>";
        $str .= "<b>Caractéristique principale :</b>  Vitesse <br/>";
        $str .= "<b>Effets : </b> Dérobe la moitié de l'or de la cible <br/>";
        $str .= "<b>Progression : </b> 5 points de vitesse.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez la compétence Vol à la Tire, vous opposez votre jet de maîtrise à la défense de votre adversaire. Ce jet de maîtrise est défini par : 1,5 * JVit <br/>";
        $str .= "Si votre jet de maîtrise est le plus élevé que la défense de votre adversaire alors vous parvenez à le voler <br/>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Vol à la tire sur Alicia de Lamb <br/>";
        $str .= "Votre Jet de maitrise est de ............: 90 <br/>";
        $str .= "Le Jet de maitrise de votre adversaire est de ............: 48 <br/>";
        $str .= "Vous réussissez à voler votre adversaire. Vous lui dérobez 54 pièces d'or <br/";
        $str .= "Vous avez été repéré par votre victime</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici, vous avez été repéré par votre victime ce qui signifie que l'action s'est enregistré dans ses évènements et il peut donc s'apercevoir que vous l'avez volé </br>";
        $str .= "Pour ne pas être repéré par votre adversaire, il vous faut effectuer un jet de maîtrise supérieur à deux fois sa défense. Dans ce cas, l'action ne s'enregistre que dans vos évènements personnels
			 et la victime ne peut pas le consulter. Elle peut s'apercevoir qu'elle a été dérobé en vérifiant le contenu de sa bourse mais n'a donc aucun moyen de savoir qui est le voleur. <br/>";
        $str .= "Si vous ne parvenez pas à voler votre victime, vous êtes repéré automatiquement</br>";
        
        $str .= "<div class='subtitle'> Loi dans les Royaumes </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Dans les trois royaumes de départ Artasse, Earok et Tonak dont leurs frontières est délimité en jaune sur la carte, le vol est interdit.
			 Si vous êtes repéré par votre victime, c'est à dire si votre jet de maîtrise est inférieur au double du sien, les gardes vous interpelleront et
			 vous demanderont de rendre l'argent et de payer une amende de 20 pièces d'or.
			 Si vous ne pouvez pas payer l'amende, vous serez conduit en prison après avoir rendu l'argent volé.
			 Si vous refusez de payer, les gardes tenteront de vous arrêter par la force et votre peine sera alourdit.<p/>";
        break;
    
    case 108:
        $str .= "Lancer de Bolas";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque à distance, 2 cases maximum.<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de l'Ombre.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Coût en PA : </b> 8 PA <br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Vitesse.<br/>";
        $str .= "<b>Effets : </b> Bonus +30% JAtt - l'adversaire se retrouve attaché par le bolas subissant un malus de 30% sur toutes les caractéristiques de bases et ne pouvant alors se déplacer qu'en rampant pour 5PA. <br/>";
        $str .= "<b>Progression : </b> 4 points de dextérité et 4 points de vitesse <br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Lancer de Bolas l'opposition est la même qu'une attaque normale, soit un Jet d'attaque (bonifié de 30%) pour le lanceur et un jet de défense classique pour la victime.<br/>";
        $str .= "Le Bolas étant considéré comme une arme légère, votre Jet d'Attaque est défini par JDex+JVit/2.<br/>";
        $str .= "<p width='700px' class='maintable'> Vous lancez votre bolas niveau 3 sur Nanik <br/>";
        $str .= "Votre Jet d'Attaque est de ............: 141 <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 18.<br/>";
        $str .= "Vous réussissez à faire tomber votre adversaire au sol.<br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici 141 est votre JAtt multiplié par 1,3. <br/><br/>";
        $str .= "Si vous êtes victime de cette compétence, vous pouvez vérifier la valeur du malus à la fin de votre profil <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='left' class='mainbglabel'><td colspan=2 > <b>BONUS/MALUS</b> </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='left'> A terre </td><td align='left' > -30% dextérité,-30% force,-30% vitesse,-30% MM  </td></tr></table>";
        $str .= "<p class='rule_pg'>";
        $str .= "Dans conquête, il est également affiché un message vous avertissant que vous êtes au sol<p/>";
        $str .= "<table width='700px' class='maintable'><tr align='left' class='mainbglabel'><td colspan=2 > <b>Nanik</b> </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='left'> Nombre de PA disponible(s) </td><td align='left' > 12 PA  </td></tr>";
        $str .= "<tr align='left' class='mainbglabel'><td colspan=2 > <b> Attention ce personnage est à terre, attaché !!!</b> </td></tr></table>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous êtes à terre, l'action 'Se déplacer' devient 'Ramper' et vous coutera 5PA<br/>";
        $str .= "Une nouvelle action 'Se libérer' apparait et pour 8PA vous libère du bolas et de son malus. Le bolas reste alors au sol et peut être ramassé.</br>";
        $str .= "Une cible ne peut pas être entravée par plus de deux bolas à la fois.<p/>";
        $str .= "<br/></br>(TAG#108=malus)";
        break;
    
    case 111:
        $str .= "Désarmer";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact sur personnage joueur uniquement.<br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque (Arme légère --> 7PA - Arme intermédiaire --> 8PA - Arme lourde --> 9PA).<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse <br/>";
        $str .= "<b>Effets : </b> l'arme de votre adversaire tombe au sol sur la case qu'il occupe.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Désarmer, vous opposez votre jet de maitrise (ou jet d'attaque) au jet de défense de votre cible. Votre jet d'attaque est donné par 1.5*(jet de vitesse)";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Désarmer sur Jorel.<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 114.<br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 98.<br/>";
        $str .= "Vous réussissez à désarmer votre adversaire, son arme est tombée au sol.<p/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Si votre jet d'attaque est inférieur au jet de défense de votre adversaire alors vous ne parvenez pas à le désarmer.</p>";
        
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à désarmer votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 3 </td><td> 0 </td><td> 5 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 0 </td><td> 3 </td><td> 5 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 0 </td><td> 0 </td><td> 7 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 0 </td><td> 0 </td><td> 5 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à désarmer votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 1 </td><td> 0 </td><td> 8 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 0 </td><td> 1 </td><td> 7 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 0 </td><td> 0 </td><td> 7 </td><td> 0 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 0 </td><td> 0 </td><td> 6 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        break;
    
    case 123:
        $str .= "Larçin";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Action à une distance de 2 cases maximum sur personnage joueur uniquement<br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Vitesse <br/>";
        $str .= "<b>Effets : </b> :  Inflige une malédiction qui oblige la cible à perdre un objet au hasard de son inventaire s'il meurt pendant le tour en cours. <br/>";
        $str .= "<b>Progression : </b> 5 points de vitesse.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez la compétence Larçin, vous opposez votre jet de maîtrise au jet de défense votre adversaire. Ce jet de maîtrise est défini par : 1,5*JVit <br/>";
        $str .= "Si votre jet de maîtrise est le plus grand que le jet de défense de votre adversaire, vous parvenez à le toucher et il est atteint par la malédiction. <br/>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Larçin sur Nidaïme <br/>";
        $str .= "Votre Jet de maitrise est de ............: 90 <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 48 <br/>";
        $str .= "Vous réussissez à toucher votre adversaire. S'il meurt dans le tour en cours, il perdra un objet de son inventaire.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "L'action est réussie et tant que la victime n'aura pas activé un nouveau tour, la malédiction opérera. </br>";
        
        break;
    
    case 127:
        $str .= "Embuscade";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Action passive personnelle<br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école de l'Ombre.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Vitesse <br/>";
        $str .= "<b>Effets : </b> :  le personnage devient invisible au yeux de ceux qui ne se trouvaient pas dans sa vue au moment de l'utilisation. <br/>";
        $str .= "<b>Progression : </b> 5 points de vitesse.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez la compétence Embuscade, il n'y a pas de jet de caractéritique.<p/>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Embuscade <br/>";
        $str .= "Vous êtes maintenant invisible pour tout ce qui n'était pas dans votre vue au moment de l'utilisation de la compétence.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "<div class='subtitle'> Être embusqué </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous êtes embusqué, vous et les personnes qui vous ont vu vous embusquer, percevez votre personnage en transparent. Les autres ne vous voient pas du tout. Cependant votre case reste occupée et les personnages ne pouvant s'y déplacer peuvent deviner votre présence.<br/>";
        $str .= "Lorsque vous êtes embusqué vous ne pouvez pas être ciblé personnellement par une attaque ou un sortilège de quelqu'un qui ne vous voit pas. Toutefois, vous subirez les actions de zone, comme un Brasier, une Pluie Sacrée ou un Tournoiement comme si vous étiez visible. <br/>";
        $str .= "Embusqué, vous pouvez vous déplacer, entrer ou sortir d'un bâtiment en restant invisible pour 3 Points d'Action de plus que normalement. Toute autre action vous fera redevenir visible au yeux de tous.<br>";
        $str .= "Vous pouvez toutefois décider de redevenir visible à tout moment pour 0 point d'action à l'aide du bouton qui sera disponible dans le menu action</p>";
        
        break;
    
    case 118:
        $str .= "Tir lointain";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque à l'arc <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école des Archers de Krima.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école des Archers de Krima .<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Dextérité <br/>";
        $str .= "<b>Progression :</b> 8 points de Dextérité si vous touchez votre cible, 4 points sinon <br/>";
        $str .= "<b>Effets : </b> :  le Tir lointain est une attaque qui vous permet d'attaquer des créatures situés à une distance de 4 cases. De plus le malus de distance est diminué de 10% : à quatre cases, le malus sur votre jet d'attaque est de 20%, 10% à trois cases et vous vous ne subissez plus malus pour les tirs à 1 et 2 cases de distance. <br/>";
        
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Tir lointain, la résolution est exactement la même que lors d'un tir à l'arc classique. Seul le malus de distance est modifié.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez Blême (malus distance = 0%) <br/>";
        $str .= "Votre Jet d'Attaque est de ............: 118 <br/>";
        $str .= "Le Jet de Défense de votre adversaire est de ............: 87 <br/>  ";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre bonus aux dégâts est de 11 point(s) <br/>";
        $str .= "Vous lui infligez 35 points de dégâts. <br/>";
        $str .= "Son armure le protège et il ne perd que 30 points de vie </p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici le Blème se trouve à une distance de deux cases et avec une attaque normale, vous auriez subi un malus de 10% sur votre jet d'attaque.</p>";
        
        break;
    
    case 119:
        $str .= "Tir gênant";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque à l'arc <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école des Archers de Krima.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école des Archers de Krima .<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Dextérité <br/>";
        $str .= "<b>Progression :</b> 8 points de Dextérité si vous touchez votre cible, 4 points sinon <br/>";
        $str .= "<b>Effets : </b> :  le Tir gênant est une attaque qui, en plus des dégâts habituels, inflige un malus de 1PA pour tous les déplacements que la cible touchée voudrait effectuer et ce pour le tour en cours et le tour suivant. <br/>";
        
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Tir gênant, la résolution est exactement la même que lors d'un tir à l'arc classique. Seul une phrase est ajouté pour avertir du malus octroyé.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez Blême (malus distance = 0%) <br/>";
        $str .= "Votre Jet d'Attaque est de ............: 118 <br/>";
        $str .= "Le Jet de Défense de votre adversaire est de ............: 87 <br/>  ";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre bonus aux dégâts est de 11 point(s) <br/>";
        $str .= "Vous lui infligez 35 points de dégâts. <br/>";
        $str .= "Son armure le protège et il ne perd que 30 points de vie ";
        $str .= "Ses déplacements lui couteront 1PA supplémentaire pour le tour en cours et le suivant.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez qui si vos dégâts sont réduits à 0 par l'armure de la cible, le malus de déplacement n'est pas infligé.</p>";
        
        break;
    
    case 120:
        $str .= "Flèche de négation";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque à l'arc <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école des Archers de Krima.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école des Archers de Krima .<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Dextérité et MM <br/>";
        $str .= "<b>Progression :</b> 4 points de Dextérité  et 4 points de MM si vous touchez votre cible, 2 points seulement sinon <br/>";
        $str .= "<b>Effets : </b> :  Flèche de négation est une attaque qui, en plus des dégâts habituels, inflige un malus MM à la cible à hauteur  (1+JMM/8) D6 pour le tour en cours et le suivant <br/>";
        
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Tir gênant, la résolution est exactement la même que lors d'un tir à l'arc classique. Seul une phrase est ajouté pour avertir du malus octroyé.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez Blême (malus distance = 0%) <br/>";
        $str .= "Votre Jet d'Attaque est de ............: 118 <br/>";
        $str .= "Le Jet de Défense de votre adversaire est de ............: 87 <br/>  ";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre bonus aux dégâts est de 11 point(s) <br/>";
        $str .= "Vous lui infligez 35 points de dégâts. <br/>";
        $str .= "Son armure le protège et il ne perd que 30 points de vie <br/>";
        $str .= "Votre Jet de maitrise de la magie est de ............: 34 <br/>";
        $str .= "Vous infligez un malus de 5 D6 de MM à votre adversaire pour le tour en cours et le suivant.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez qui si vos dégâts sont réduits à 0 par l'armure de la cible, le malus de maitrise de la magie n'est pas infligé.</p>";
        
        break;
    
    case 126:
        $str .= "Volée de flèches";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type :</b> Attaque à l'arc <br/>";
        $str .= "<b>Niveau :</b> Adepte de l'école des Archers de Krima.<br/>";
        $str .= "<b>Apprentissage :</b> Ecoles de combat dispensant les cours de niveau Adepte de l'école des Archers de Krima .<br/>";
        $str .= "<b>Coût en PA :</b> Temps d'attaque.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Dextérité et Vitesse <br/>";
        $str .= "<b>Progression :</b> 5 points de Dextérité  et 5 points de vitesse <br/>";
        $str .= "<b>Malus :</b> Il y a un malus en attaque de 1.5 * (dextérité - vitesse). Avoir autant de dés de dextérité que de vitesse est le seul moyen d'éviter le malus. <br/>";
        $str .= "Par exemple, si vous avez une dextérité de 30D6 et une vitesse de 20 D6, alors vous aurez un malus de 15d6 en attaque. Comme ce malus se base sur la dextérité et la vitesse, Aile de la Colère et Garde ne peuvent changer ce malus.<br/>";
        $str .= "<b>Effets :</b> JAtt : 1.5*Jdex+BMM | JDeg : 80% des dégâts d'un tir standard et de ses BMM
			 (à ce propos, notez que les effets du sort Ailes de colère ne s'appliquent qu'à la première des flèches tirées en volée).<br/>";
        
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous utilisez Volée de flèches, vous obtenez la résolution de toutes les flèches que vous avez tiré.";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Volée de Flèches <br/>";
        $str .= "------- FLECHE 1----------- <br/>";
        $str .= "Vous attaquez Araignée colossale (malus distance = 0%)<br/><br/>";
        $str .= "Votre Jet d'Attaque est de ............: 179 <br/>";
        $str .= "Le Jet de Défense de votre adversaire est de ............: 33 <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre bonus aux dégâts est de 43 point(s) <br/>";
        $str .= "Son armure le protège et il ne perd que 56 points de vie <br/><br/>";
        $str .= "------- FLECHE 2----------- <br/>";
        $str .= "Vous attaquez Sauteur fauchard (malus distance = 0%)<br/><br/>";
        $str .= "Votre Jet d'Attaque est de ............: 175 <br/>";
        $str .= "Le Jet de Défense de votre adversaire est de ............: 49 <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre bonus aux dégâts est de 39 point(s) <br/>";
        $str .= "Son armure le protège et il ne perd que 52 points de vie <br/><br/>";
        $str .= "------- FLECHE 3----------- <br/>";
        $str .= "Vous attaquez Homme-lézard (malus distance = 10%)<br/><br/>";
        $str .= "Votre Jet d'Attaque est de ............: 175 <br/>";
        $str .= "Le Jet de Défense de votre adversaire est de ............: 29 <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre bonus aux dégâts est de 43 point(s) <br/>";
        $str .= "Son armure le protège et il ne perd que 57 points de vie <br/><p/>";
        $str .= "<div class='subtitle'> Choix des cibles </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "La volée de flèches vous permet de choisir une, deux ou trois cibles différentes. Vous pouvez décider de ne tirer que deux flèches.
			 Pour cela, il vous faudra séléctionner 'ignorer ce champs' dans le sélécteur de la troisième cible ou de la troisième flèche.
			 Notez que vous ne pouvez pas tirer qu'une seule flèche.<br/>";
        
        break;
    
    case 122:
        $str .= "Flèche enflammée";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque sur bâtiment <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école des Archers de Krima.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école des Archers de Krima .<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Dextérité<br/>";
        $str .= "<b>Progression :</b> 8 points de Dextérité <br/>";
        $str .= "<b>Effets : </b> :  Flèche emflammée ne permet de cibler que les bâtiments. Elle permet d'infliger 2 fois les dégâts habituels de votre tir à l'arc + 100 points de structure à n'importe quel bâtiment destructible. <br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous tirez une flèche enflammée sur École de Magie niveau 5<br/>";
        $str .= "Votre Jet d'Attaque est de ............: 168 <br/>";
        $str .= "Vous infligez au bâtiment une perte de 232 points de structure <br/>  ";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici, les dégâts sont calculés en supposant une défense à 0. La formule de dégât de tir à l'arc donne donc 0,35*JAtt = 56 points. Ces dégâts sont multipliés par 2 puis on ajoute 100.</br>";
        $str .= "Reste à ajouter le bonus de dégât de l'arc et de la flèche utilisée comme dans le cas d'un tir classique.</br>";
        $str .= "Notez qu'il n'y a pas de malus de distance lorsque vous utilisez Flèches Enflammée et vos dégâts sont les mêmes à 1, 2 ou 3 cases de distance.</p>";
        
        break;
    
    case 121:
        $str .= "Course Celeste";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Compétence passive personnelle <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école des Archers de Krima.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école des Archers de Krima .<br/>";
        $str .= "<b>Coût en PA : </b> 3PA<br/>";
        $str .= "<b>Caractéristiques principales :</b> Vitesse <br/>";
        $str .= "<b>Progression :</b> 3 points de vitesse <br/>";
        $str .= "<b>Effets : </b> : Lorsqu'un personnage lance Course Celeste, il peut pour le tour en cours et le suivant se déplacer en forêt, en désert (1,5PA normalement), en montagne et en marécage (2PA normalement) pour seulement 1PA.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que cette compétence, contrairement à toutes les autres, ne fait gagner aucun PX.";
        
        break;
    
    case 112:
        $str .= "Auto-régénération";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Compétence de soin personnel<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Coût en PA : </b> 10 PA<br/>";
        $str .= "<b>Caractéristiques principales :</b> Force et PV <br/>";
        $str .= "<b>Progression :</b> 6 points de Vie et 4 points de Force <br/>";
        $str .= "<b>Effets : </b> : Auto-Régénération permet à celui qui l'utiliser de récupérer [(30+JForce/2)%  PV maximum] points de vie perdus<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        // $str.="Lorsque vous utilisez Tir gênant, la résolution est exactement la même que lors d'un tir à l'arc classique. Seul une phrase est ajouté pour avertir du malus octroyé.";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Auto-Régénération <br/>";
        $str .= "Votre Jet de Force est de ............: 121 <br/>";
        $str .= "Vous vous régénérez donc de 238 points de vie <br/>  ";
        $str .= "Vous subissez un malus de 17 dés de force pour le tour en cours et les 2 prochains<br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici, le paladin possède 34D6 de force et 261 points de vie maximum. Il régénère donc (30+121/2) = 91% de 261 soit 238 points de vie et subit un malus de 17D6 de force.</br>";
        break;
    
    case 113:
        $str .= "Appel de la lumière";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Coût en PA : </b> Temps d'attaque<br/>";
        $str .= "<b>Caractéristiques principales :</b> MM <br/>";
        
        $str .= "<b>Effets : </b> Lorsque le Paladin utilise Appel de la Lumière, son jet d'attaque classique est remplacé par 1,5*JMM (sans bonus d'attaque). Le bonus sur la différence entre le jet d'attaque et le jet de défense se calcule toujours de la même façon. Le jet de dégâts est également inchangé. A noter toutefois qu'il est bonifié de 30% contre les monstres appartement à la classe des démons et des morts-vivant.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        // $str.="Lorsque vous utilisez Tir gênant, la résolution est exactement la même que lors d'un tir à l'arc classique. Seul une phrase est ajouté pour avertir du malus octroyé.";
        $str .= "<p width='700px' class='maintable'> Vous attaquez Zombi  <br/>";
        $str .= "Votre Jet de Maitrise de la magie  est de : ............: 112 <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 44 ) <br/>  ";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 17) <br/>  ";
        $str .= "La compétence Appel de la Lumière vous confère 30% de bonus de dégât sur les morts-vivant. <br/>";
        $str .= "Votre Jet de Dégâts est de ............: 87 <br/>";
        $str .= "Vous lui infligez donc 104 points de dégâts. <br/>";
        $str .= "Son armure le protège et il ne perd que 81 points de vie  <br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "<br/><div class='subtitle'> Influence sur les jauges de progression </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 1 : Vous avez réussi à toucher votre adversaire </p>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 5 </td><td> 0 </td><td> 0 </td><td> 4 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 4 </td><td> 0 </td><td> 0 </td><td> 4 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 2 </td><td> 0 </td><td> 2 </td><td> 3 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 3 </td><td> 0 </td><td> 0 </td><td> 5 </td><td> 0 </td></tr>";
        $str .= "</table><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Cas 2 : Vous n'avez pas réussi à toucher votre adversaire <p/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Caractéristique </td><td > Force</td><td> Dextérité </td><td> Vitesse </td><td> Magie </td><td> Vie </td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Arme lourde </td><td > 3 </td><td> 0 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme intermédiaire </td><td > 0 </td><td> 2 </td><td> 0 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme légère </td><td > 0 </td><td> 0 </td><td> 1 </td><td> 2 </td><td> 0 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Arme magique </td><td > 0 </td><td> 0 </td><td> 0 </td><td> 4 </td><td> 0 </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        $str .= "Remarque : L'utilisation d'une masse sacrée avec Appel de la Lumière entraîne des gains sur les jauges équivalent à ceux d'une arme magique. <br/><br/>";
        break;
    
    case 124:
        $str .= "Protection";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Portée : </b> Contact<br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Aspirant de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Coût en PA : </b> 8 PA<br/>";
        $str .= "<b>Caractéristiques principales :</b> MM <br/>";
        $str .= "<b>Progression :</b> 4 points de Vie et 4 points de MM <br/>";
        $str .= "<b>Effets : </b> Le Paladin s'interpose à toutes les attaques physiques et magiques à l'exclusion des malédictions portées à la cible protégée. Sa défense et considéré à 0. En revanche, il bénéficie d'un bonus d'armure de 10+2*(1+JMM/8)% sur sa valeur d'armure.<br/> Lorsque la cible protégée est attaquée, le Paladin et son protégé échangent leur place";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Utilisation de la compétence </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Protection sur Trozenn  <br/>";
        $str .= "Tant que la compétence sera activé, vous protégerez Trozenn </p><br/><br/>  ";
        $str .= "<p class='rule_pg'>";
        $str .= "<div class='subtitle'> Résolution de l'attaque d'une cible protégée </div>";
        $str .= "<p width='700px' class='maintable'> Vous attaquez Trozenn mais Linsorld s'interpose. <br/>";
        $str .= "Votre Jet d'Attaque est de ............: 187 (Bonus de dégâts : 46) <br/>";
        $str .= "Votre Jet de Dégâts est de ............: 68  <br/>  ";
        $str .= "Vous lui infligez donc 114 points de dégâts. <br/><br/>  ";
        $str .= "Le Jet de maitrise de magie de votre adversaire est de ............: 72 <br/>";
        $str .= "Son bonus d'armure est donc de 30% <br/>";
        $str .= "Son armure le protège et il ne perd que 36 points de vie  </p><br/>";
        $str .= "Ici, le bonus de dégât est de 46 car la défene est considérée à 0 (187/4). L'armure classique du Protecteur est de 60. Les 30% bonus donné par le jet de MM (10+2*(1+72/8)) lui permet d'atteindre les 78 d'armure.";
        $str .= "La protection peut être désactivée  n'importe quand pour 0pa. </p>";
        $str .= "</p>";
        break;
    
    case 114:
        $str .= "Aura de Courage";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Portée : </b> 8 cases <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Coût en PA : </b> 5 PA<br/>";
        $str .= "<b>Caractéristiques principales :</b> PV <br/>";
        $str .= "<b>Progression :</b> 5 points de vie <br/>";
        $str .= "<b>Effets : </b> Octroie un bonus d'attaque de [5+(Vie Courante)/20]% à l'ensemble des membres du groupe de chasse situé à une distance de 8 cases maximum. Tant que l'aura est active, le paldin subira une perte de 5% de sa vie maximum à chaque nouveau tour.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Utilisation de la compétence </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous activez Aura de courage <br/>";
        $str .= "L'ensemble de votre groupe de chasse reçoit un bonus de 12% en d'attaque <br/>  ";
        $str .= "Tant que l'Aura sera activée vous subirez une perte de 14 points de vie à chaque nouveau tour <br/><br/></p>  ";
        $str .= "Ici, la vie courante du paladin est de 158 (5+158/20 = 7 aronndi à l'entier inférieur). Sa vie maximum est de 288, il subit donc une perte de vie de 14 points de vie (5*288/100 = 14 arrondi à l'entier inférieur.) a chaque nouveau tour. Notez donc que le paladin peut mourir par la perte de vie engendrée par la génération de l'aura de courage.";
        $str .= "L'aura peut être désactivée  n'importe quand pour 0pa. </p>";
        $str .= "</p>";
        break;
    
    case 115:
        $str .= "Aura de Résistance";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Portée : </b> 8 cases <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Coût en PA : </b> 5 PA<br/>";
        $str .= "<b>Caractéristiques principales :</b> MM <br/>";
        $str .= "<b>Progression :</b> 4 points de MM et 1 point de force <br/>";
        $str .= "<b>Effets : </b> Octroie un bonus d'armure de [10+JMM/4]% à l'ensemble des membres du groupe de chasse situé à une distance de 8 cases maximum. Tant que l'aura est active, le paladin subira un malus de force de 20%.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Utilisation de la compétence </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous activez Aura de resistance <br/>";
        $str .= "Votre Jet de Maitrise de la magie est de ............: 85 <br/>  ";
        $str .= "L'ensemble de votre groupe de chasse reçoit un bonus de 31% en armure <br/>";
        $str .= "Tant que l'Aura sera activée vous subirez un malus de 20% de force<br/></p>  ";
        break;
    
    case 116:
        $str .= "Exorcisme de l'ombre";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Portée : </b> 5 cases <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de combat dispensant les cours de niveau Adepte de l'école des Paladins de Tonak.<br/>";
        $str .= "<b>Coût en PA : </b> 8 PA<br/>";
        $str .= "<b>Caractéristiques principales :</b> MM <br/>";
        $str .= "<b>Progression :</b> 4 points de vie et 4 points de MM<br/>";
        $str .= "<b>Effets : </b> Le paladin inflige automatiquement à chaque agresseur dans sa vue des dégâts égal à min(150%,jMM%) des PVs perdus qui lui ont été individuellement infligés par ces dernies au cours des 3 derniers tours.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Emmanganiser l'énergie noire </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous possédez la compétence d'exorcisme de l'ombre, la quantité d'énergie noire emmagasinée est noté dans votre profil au niveau des bonus/malus.<br/>  ";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td > Exorcime de l'ombre </td><td > Quantité d'énergie noire à libérer : 95 sur Assassin Runique (id:9935)</td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td > Exorcime de l'ombre </td><td > Quantité d'énergie noire à libérer : 181 sur Gardien Squelette (id:6975)</td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td > Exorcime de l'ombre </td><td > Quantité d'énergie noire à libérer : 120 sur Ewakhine </td></tr>";
        $str .= "</table>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Utilisation de la compétence </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Exorcisme de l'ombre. <br/>";
        $str .= "Vous libérez l'énergie noire accumulée au cours des trois derniers tours <br/><br/>  ";
        $str .= "Votre Jet de Maitise de la magie est de ............: 82 <br/>";
        $str .= "82% de l'énergie noire accumulée sera transformée en dégât.<br/><br/>  ";
        $str .= "Assassin Runique n'est pas à portée  <br/><br/>";
        $str .= "Gardien Squelette est ciblé par l'exorcisme de l'ombre. <br/>";
        $str .= "Il recoit donc 148 points de dégât.<br/>";
        $str .= "Il perd 148 points de vie.<br/><br/>";
        $str .= "Ewakhine est ciblé par l'exorcisme de l'ombre. <br/>";
        $str .= "Il recoit donc 98 points de dégât.<br/>";
        $str .= "Il perd 98 points de vie.<br/>";
        $str .= "Vous avez TUÉ votre adversaire.<br/></p>";
        
        break;
        
        break;
    
    // ***************************************************************//
    // MAGIC SPELLS
    // ***************************************************************//
    case 299:
        $str .= "Réussir un sortilège";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous tentez un sortilège, vous jetez un D100. Si celui-ci est supérieur à votre maîtrise du sortilège, vous ratez votre action et vous perdez min(3; Temps Sortilège) PA</p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 50% de chance de réussir ce sortilège <br/>";
        $str .= "Votre score est de ............: 71 <br/>";
        $str .= "Vous avez raté votre sortilège.<br/>";
        $str .= "Cette action vous a coûté 3 PA.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Si votre D100 est inférieur à votre jet de maîtrise, le sortilège est réussi et va s'effectuer. Avant vous jetez un deuxième D100 pour savoir si vous améliorez votre pourcentage de maîtrise<br/>";
        $str .= "Si votre D100 est supérieur à votre maîtrise du sortilège, vous gagnez 1% de maîtrise </p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 68% de chance de réussir ce sortilège <br/>";
        $str .= "Votre score est de ............: 13         Sortilège réussi.<br/>";
        $str .= "Votre jet d'amélioration est de 90, vous améliorez votre sortilège de 1%.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que votre maîtrise d'un sortilège hors bonus ne peut dépasser 90%. Une fois cette valeur atteinte, il n'y a plus de jet d'amélioration <p/>";
        
        $str .= "<div class='subtitle'> Concentration </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous ratez un sortilège, vous obtenez pour le tour en cours, un bonus de maîtrise pour retenter ce sortilège <br/>";
        $str .= "Ce bonus dépend de votre pourcentage de maîtrise et du niveau du sortilège comme le montre la formule suivante. <br/>";
        $str .= "Bonus = (100- pourcentage de maîtrise)/(2+ niveau de la comp).<br/>";
        $str .= "Un sortilège élémentaire est considérée de niveau 0, un sortilège de niveau aspirant est considérée comme niveau 1 et un sortilège de niveau adepte de niveau 2.</p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 92% de chance de réussir ce sortilège (4% bonus) <br/>";
        $str .= "Votre score est de ............: 13         Sortilège réussi.<br/>";
        $str .= "Votre jet d'amélioration est de 90, vous améliorez votre sortilège de 1%.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici vous avez tenté une première fois un sortilège de niveau aspirant que vous maîtrisez à 88%. En la retentant, vous bénéficiez de 4% de bonus (100-88)/3. <br/>";
        $str .= "Notez que dans ce cas, le jet d'amélioration a toujours lieu car votre maîtrise propre indépendamment du bonus est inférieure à 90% et un jet supérieur à 88% soit votre maîtrise propre sans bonus suffit pour améliorer votre maîtrise.</p><br/>";
        
        break;
    
    case 298:
        $str .= "Niveau des sortilèges et maîtrise";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Les sortilèges élémentaires <small> (niveau 0) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les sortilèges élémentaires sont les premièrs que vous pourrez apprendre. Ils sont au nombre de 4.<br/>";
        $str .= "Ils sont accessibles dans n'importe quelle école de magie de niveau 1. Au début du monde vous ne trouverez
			 de telle écoles que dans les grandes villes, l'apprentissage vous coûtera alors <b> 80 pièces d'or </b> et <b> 12 points d'action</b>. Par la suite, certains personnages prendront
			 le contrôle de village et pourront alors construire de nouvelles écoles, y fixer un autre prix d'apprentissage et même baisser le temps d'apprentissage en améliorant
			 leurs écoles. <small>(vous trouvez plus de détails concernant l'amélioration des écoles dans la section traitant des bâtiments)</small><br/><br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Les sortilèges de niveau aspirant <small>(niveau 1) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les sortilèges de niveau aspirant correspondent aux sortilèges du premier niveau de chacune des 4 grandes écoles de magie :<br/>";
        $str .= "- L'école de guérison.<br/>";
        $str .= "- L'école de protection.<br/>";
        $str .= "- L'école de destruction.<br/>";
        $str .= "- L'école d'altération.<br/><br/>";
        $str .= "Pour apprendre un sortilège de niveau aspirant, vous devrez maîtriser une compétence élémentaire ou un sort élémentaire <b> à un minimum de 80%</b>.<br/>";
        $str .= "A la différence des sortilèges élémentaires, toutes les écoles de magie ne dispensent pas les cours pour apprendre les sortilèges de niveau aspirant. Il vous faudra trouver une école de combat qui possèdent les
			 cours de niveau aspirant de l'école que vous souhaitez suivre.<br/>";
        $str .= "Au début du monde, les sortilèges de niveau aspirant des quatres écoles sont disponibles dans toutes les grandes villes et
			 en partiulier dans les villes des royaumes de départ comme Artasse, Earok ou Tonak. Leur apprentissage vous coûtera alors <b> 500 pièces d'or et 12 points d'action</b><br/>";
        $str .= "<div class='subtitle'> Les sortilèges de niveau adepte <small>(niveau 2) </small> </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les sortilèges de niveau adepte correspondent aux sortilèges du deuxième niveau de chacune des 4 grandes écoles de magie:<br/>";
        $str .= "Pour apprendre un sortilège de niveau adepte d'une école, vous devrez maîtriser <b> deux sortilèges de niveau aspirant de cette même école à 90%. </b><br/>";
        $str .= "Les écoles qui dispensent les cours de niveau adepte sont plutôt rares au début du monde. Vous ne pourrez trouver ces sortilèges que dans la grande ville spécialiste de l'école que vous recherchez :<br/>";
        $str .= "- Izandar pour l'école de guérison.<br/>";
        $str .= "- Djin pour l'école de protection.<br/>";
        $str .= "- Landar pour l'école de destruction.<br/>";
        $str .= "- Dust pour l'école d'altération.<br/>";
        $str .= "Dans ces écoles, l'apprentissage des sortilèges de niveau adepte vous coûtera alors <b> 1500 pièces d'or et 12 points d'action.</b><br/><br/>";
        break;
    
    case 297:
        $str .= "Le RP de la magie et des sortilèges";
        $str .= "</H1>";
        $str .= "<p class='rule_pg'>";
        $str .= "<i> Notez tout d'abord que cette vision de la magie dans Nacridan ne fait pas autorité et toutes les variantes de description utilisant les caractéritiques de base réquises pour parvenir à la création d'un sort sont acceptées.<br/><br/>";
        $str .= "</i>Pour lancer un sortilège, le mage doit littéralement inscrire sa formule dans l'Aether ambiant : à l'aide de mouvements chargés de magie, il décrit en quelque sorte l'effet escompté. Aux yeux du néophyte, ces symboles constitués de lumières semblent flotter dans les airs. Dans notre monde, la cohérence de la magie est précaire : ils s'effacent parfois très rapidement après leur création.<br/>";
        $str .= "<br/>La magie a beau être obscure pour bon nombre d'êtres vivants, sa nature n'en reste pas moins évidente. Par exemple, observer un mage lancer un sort de Destruction donne le sentiment d'un combat âpre, face à un adversaire invisible et coriace : cela nécessite une grande dextérité.<br/>";
        $str .= "Les sorts de Protection procèdent d'un art beaucoup plus contenu : ils imposent un effort physique important car cette magie a vocation à résister au changement et elle est particulièrement difficile à ancrer dans l'Aether.<br/>";
        $str .= "A l'inverse, les sorts d'Altération doivent être tracés rapidement car ces symboles volatiles disparaissent particulièrement vite.<br/>";
        $str .= "Finalement, les sorts de Guérison sont reconnus comme les plus simples à invoquer : des mouvements à peine esquissés permettent au mage de se concentrer surtout sur la quantité de magie à produire.<br/>";
        $str .= "<br/> Il est à noter que les sceptres et bâtons magiques aident grandement le mage à canaliser le flux magique dans la réalisation de symboles complexes. Ainsi, leur influence est particulièrement notable lors de l'invocation d'une magie de Destruction, car ils augmentent nettement la précision de l'attaque.<br/>";
        
        break;
    
    case 201:
        $str .= "Toucher Brûlant";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque au contact.<br/>";
        $str .= "<b>Niveau : </b> Sortilège élémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de magie permettent d'apprendre les sortilèges élémentaires.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA, quelque soit l'arme.<br/>";
        $str .= "<b>Caractéristiques principales :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 5 points de MM et 3 points de Dextérité<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1 + MM/8 arrondi à l'inférieur<br>";
        $str .= "<b>Effets : </b> JAtt :  JAtt magique* | Dégâts : 1.7 * Niveau du sort D6 | L'armure de la cible est prise en compte.";
        
        $str .= "<br/><br/><i><small> Rappel : Le jet d'attaque magique est donné par Jet(1,5*Dex) + BMattaque si le lanceur porte une arme magique (bâton ou sceptre) et seulement JDex sinon.</small></i>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        
        $str .= "<p width='700px' class='maintable'> Vous attaquez un Fantôme.<br/>";
        $str .= "Votre Jet d'attaque magique est de ............: 73 <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 42 <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire.<br/>";
        $str .= "Votre jet de MM est de : 70 <br/>";
        $str .= "Vous réalisez un sort de niveau 9 (15.3D6 de dégâts). <br/>";
        $str .= "Vous lui avez infligé 42 points de dégâts.<br/>";
        $str .= "Son armure le protège et il ne perdra que 39 point(s) de vie.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que les dégâts ne prennent pas en compte la différence entre le jet d'attaque et le jet du défenseur.</p><br/>";
        break;
    
    case 202:
        $str .= "Armure d'Athlan";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien<br/>";
        $str .= "<b>Contact : </b> Contact<br/>";
        $str .= "<b>Niveau : </b> Sortilège élémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de magie permettent d'apprendre les sortilèges élémentaires.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie et Force.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de MM et 2.5 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JForce, JMM)/3.5  arrondi à l'inférieur<br>";
        $str .= "<b>Effets : </b> Bonus d'armure de 2.4 + Niveau du sort * 1.2 pour le tour en cours et le suivant.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>Vous utilisez Armure d'Athlan sur vous-même.<br/>";
        $str .= "Votre Jet de Force est de ............: 33 <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 37 <br/>";
        $str .= "Vous réalisez un sort de niveau : 9<br/>";
        $str .= "Votre armure est augmentée de 14.2 points pour ce tour et le suivant.</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez qu'un personnage ne peut pas, de manière générale, cumuler les bénéfices de deux sortilèges identiques.<br/>";
        $str .= "Toute nouvelle utilisation du sortilège Armure d'Athlan sur sur une cible déjà bonifiée par cet enchantement remplacera l'effet du précédent.";
        break;
    
    case 203:
        $str .= "Larmes de Vie";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison personnel<br/>";
        $str .= "<b>Portée : </b> Contact <br/>";
        $str .= "<b>Niveau : </b> Sortilège élémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de magie permettent d'apprendre les sortilèges élémentaires.<br/>";
        $str .= "<b>Coût en PA : </b> 7PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 7 points de MM <br/>";
        $str .= "<b>Niveau du sortilège :</b> 1 + JMM/8 arrondi à l'inférieur<br>";
        $str .= "<b>Effets : </b> Soin potentiel :  Niveau du sort D6 points de vie.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= " Vous utilisez Larmes de Vie sur DemoneLame.<br/>";
        $str .= " Votre Jet de Maîtrise de la Magie est de ............: 83 <br/>";
        $str .= " Vous réalisez un sort de niveau : 11 (11D6 Points de Vie)<br/>";
        $str .= " DemoneLame est soigné de 37 des 50 points de vie que le sortilège aurait pu vous rendre<br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici, le nombre de points potentiel de guérison est de 50. Démonelame n'est soigné que de 37 points de vie car il ne peut pas dépasser sa vie maximum.</p><br/>";
        break;
    
    case 222:
        $str .= "Ailes de Colère";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien.<br/>";
        $str .= "<b>Portée : </b> 2 cases <br/>";
        $str .= "<b>Niveau : </b> Sortilège élémentaire.<br/>";
        $str .= "<b>Apprentissage : </b> Toutes les écoles de magie permettent d'apprendre les sortilèges élémentaires.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de Vitesse et 2.5 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JVit,JMM)/3.5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Bonus d'attaque ou de dégâts à hauteur de : 0.5*Niveau du sort D6  ";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        
        $str .= "<p width='700px' class='maintable'> Avant de lancer le sortilège, le magicien choisit s'il veut augmenter les dégâts ou l'attaque de sa cible.<br/>";
        $str .= " Vous utilisez Ailes de colère sur Django<br/>";
        $str .= " Votre Jet de Maîtrise de la Magie est de ............: 75 <br/>";
        $str .= " Votre Jet de Vitesse est de ............: 90 <br/>";
        $str .= " Vous réalisez un sort de niveau : 21<br/>";
        $str .= " Vous avez créé un enchantement qui augmente la caractéristique : Dégât de 10.5D6 pour le tour en cours et les deux suivants. <br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que vous pouvez cumuler sur une même cible un sortilège Ailes de Colère bonifiant les dégâts avec un sortilège Ailes de Colère bonifiant l'attaque<br/>";
        $str .= "Par contre, comme tous les autres enchantements, nous ne pouvez faire profiter une cible du cumule de deux utilisations simultanées d'un enchantement de même type, les effets du nouveau sortilège remplaçant toujours  ceux de l'ancien.";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Attention: Cet enchantement disparait après la première utilisation ou au bout de 2 tours suivants le tour en cours. Sur une volée de flêches par exemple, il disparait après la première flêche, par contre il est valable pour toutes les cibles d'un tournoiement.";
        $str .= "</p>";
        break;
    
    /* ***************************** SORTILEGE DE GUERISON ****************************** */
    case 204:
        $str .= "Souffle d'Athlan";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de Guérison.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Guérison.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 5 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1+JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Diminue le niveau des maux et des malédictions de la cible à hauteur du niveau du sort. Lorsque le niveau d'une malédiction est réduit à 0, elle est effacée.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        
        $str .= "<p width='700px' class='maintable'> Vous utilisez Souffle d'Athlan sur Moa Zhi <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 48 <br/>";
        $str .= "Vous réalisez un sort de niveau : 7 <br/>";
        $str .= "Tous les maux de Moa Zhi ont été réduit de 7 niveau(x).<br/>";
        $str .= "Ceux dont le niveau a été réduit à 0 ont été dissipés <br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Réduire le niveau d'une malédiciton ou d'un maux qui accable une cible en réduit les effets.</p><br/>";
        $str .= "</p>";
        break;
    
    case 205:
        $str .= "Charme de Vitalité";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de Guérison.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Guérison.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 8 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1+JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Lors des 3 prochaines activations du tour de jeu de la cible, celle-ci est soignée à hauteur de (Niveau du sort D6)/2 points de vie.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Charme de vitalité sur Revelk <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 113 <br/>";
        $str .= "Vous réalisez un sort de niveau : 14 <br/>";
        $str .= "Revelk gagne 22 points de régénération pour les trois prochains tours.<br/>";
        $str .= "</p>";
        break;
    
    case 206:
        $str .= "Bassin Divin";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de Guérison.<br/>";
        $str .= "<b>Portée : </b> 2 cases.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Guérison.<br/>";
        $str .= "<b>Coût en PA : </b> 9PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 9 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1+JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Crée un bâtiment de 15 Points de Structure 'Bassin divin' de niveau égal au niveau du sort. Tout aventurier peut venir boire l'eau du bassin pour 2 PA ce qui le soigne de (Niveau du bassin D6)/2 PV. Le Bassin Divin permet 5 utilisations. Il est détruit automatiquement lorsqu'il est vide.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Bassin Divin en (x=563,y=341) <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 79 <br/>";
        $str .= "Vous réalisez un sort de niveau : 10 <br/>";
        $str .= "Vous avez créé un Bassin Divin de niveau 10<br/>";
        $str .= "</p>";
        $str .= "Un bassin créé subit une érosion de 1 Point de Structure (PS) par jour. Un bassin nouvellement créé disposant de 15 PS, il est automatiquement détruit au bout de 15 jours à moins qu'une recharge du bassin ne soit effectuée.<br/><br/>";
        $str .= "La liste et l'emplacement de chacun des bassins divins créés par le mage est affichée dans l'onglet Economie->vos batiments. C'est également à partir de cette liste que le mage peut choisir de révoquer un de ses bassins divins. <br/><br/>";
        $str .= "Notez également qu'un mage ne peut pas controlé plus de 2 bassins divins simultanément. Il devra en révoquer certains s'il veut lancer à nouveau le sortilège Bassin Divin.<br/><br/>";
        break;
    
    case 216:
        $str .= "Pluie Sacrée";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de Guérison.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau adepte de l'école de Guérison.<br/>";
        $str .= "<b>Coût en PA : </b> 9PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 9 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1+JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Redonne Jet(Niveau du sort D6)/2 PV arrondi à l'entier supérieur à tous les personnages situés dans le rayon d'action du sort. Le sort peut être lancé jusqu'à deux cases du lanceur et possède un rayon d'action d'une case.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Vous utilisez pluie sacrée en 438-263 <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 85 <br/>";
        $str .= "Vous réalisez un sort de niveau : 11 <br/>";
        $str .= "Nombre de points potentiels de guérison : 22<br/><br/>";
        $str .= "Gobelin est atteint par la pluie <br/>";
        $str .= "Gobelin est soigné de 14 points de vie <br/><br/>";
        $str .= "Madère est atteint par la pluie <br/>";
        $str .= "Madère est soigné de 3 points de vie <br/><br/>";
        $str .= "Cugel est atteint par la pluie <br/>";
        $str .= "Cugel est soigné de 22 points de vie <br/><br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici Gobelin et Madère n'ont pas récupéré 22 points de vie simplement parce qu'ils n'étaient pas blessés d'autant.<br>";
        $str .= "Notez que la pluie sacrée soigne également les monstres. ";
        $str .= "Pluie sacrée ne peut pas être lancé dans un bâtiment.";
        $str .= "</p>";
        
        break;
    
    case 217:
        $str .= "Recharge du Bassin";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de Guérison.<br/>";
        $str .= "<b>Portée : </b> 2 cases<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau adepte de l'école de Guérison.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 5 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1+JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Remplit un Bassin Divin existant, permettant ainsi 5 nouvelles utilisations et ramenant ses Point de Structure (PS) au maximum";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Le bassin ciblé a été rechargé avec succès<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que le niveau du sort doit être au moins égal au niveau du (Bassin Divin - 2) pour réussir à le recharger. <br/>";
        $str .= "Remarque : un bassin créé subit une érosion de 1 Points de Structure (PS) par jour. La recharge d'un bassin ramène ses PS à 15, son maximum.<br/>";
        $str .= "</p>";
        break;
    
    case 218:
        $str .= "Soleil de guérison";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de guérison <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de Guérison.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau adepte de l'école de Guérison.<br/>";
        $str .= "<b>Coût en PA : </b> 10PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 10 points de Maîtrise de la Magie.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1+JMM/8 arrondi au supérieur <br>";
        $str .= "<b>Effets : </b> Tout membre du Groupe de Chasse dans la vue du lanceur (hors bâtiment) subissant des dégâts est automatiquement soigné de (Niveau du sort)% de la perte de vie subie, arrondi à l'entier supérieur.
			 Le soleil de guérison reste actif pour le tour en cours et les deux suivants.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Vous utilisez Soleil de guérison<br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 121 <br/>";
        $str .= "Vous réalisez un sort de niveau : 16 <br/>";
        $str .= "Chacun de vos alliés sous l'effet du soleil sera automatiquement guérit de 11% de la perte de vie subite en cas d'attaque et ce pour ce tour et les deux suivants. <br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Evènements </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> 2014-03-21 15:23:59 	ATTAQUE  Phénix a frappé Gunthar qui a survécu.<br/>";
        $str .= "2014-03-21 15:24:01 	SORTILÈGE 	Gunthar a été guérit automatiquement par le soleil de guérison de Xalith </p>";
        $str .= "Notez que la guérison intervient après la perte total des points de vie infligé par l'attaque.
			 Il faut donc que le personnage puisse l'encaisser sans mourir pour être soigné par le soleil.
			 De plus, ce sort est inefficace contre les dégâts causés par une barrière enflammée, l'aura de courage ou toute malédiction de type <b>poison</b> (Sang de Lave, Blessure Profonde, etc).<br/><br/>";
        $str .= "<div class='subtitle'> Détail de la guérison </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>   Vous avez été guérit automatiquement de 23 points de vie par le Soleil de guérison de Xalith<br/>";
        $str .= "</p>";
        $str .= "Soleil de guérison ne guérit pas les membres du groupe de chasse situé dans un bâtiment. Il ne peut pas être lancé dans un bâtiment.";
        
        $str .= "Remarque : si plusieurs Soleil de Guérison sont lancés dans un même groupe de chasse, un seul sort choisi au hasard sera effectif";
        
        break;
    
    /* **************************************** SORTILEG D'ALTERATION ************************* */
    case 221:
        $str .= "Malédiction d'Arcxos";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort d'opposition magique  <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école d'Altération.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école d'Altération.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Portée : </b> 2 cases.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4 points de Maîtrise de la Magie et 4 points de vitesse.<br/>";
        $str .= "<b>Niveau du sortilège :</b> (min(JVit,JMM lanceur)*1,5 - JMM cible)/5 arrondi au supérieur <br>";
        $str .= "<b>Effets : </b> La cible recoit un malus de vitesse, de dextérité ou de force ou une diminution des enchantements au choix sur magicien à hauteur de 0,4D6 par niveau pour le tour en cours de la cible et le suivant.";
        $str .= "<br/>Pour les enchantements, il fonctionne comme un souffle d'Athlan mais sur les enchantements positifs: Armure d'Athlan, Bouclier Magique, Ailes de la Colère, Barrière Enflammée, Bulle de Vie, Charme de Vitalité. Le sortilège Protection Magique permet de diminuer les effets, voire les annuler de ce sortilège.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Malédiction d'Arcxos sur Rat géant. <br/>";
        $str .= "Votre Jet de Vitesse est de ............: 87  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 82   <br/>";
        $str .= "Le Jet de Maîtrise de la Magie de votre adversaire est de ............: 11   <br/>";
        $str .= "Vous réalisez un sort de niveau : 23 <br/>";
        $str .= "Vous avez créé une malédiction qui reduit la caractéristique : FORCE de 9,2D6 pour le tour en cours de la cible et le suivant. <br/>";
        $str .= "</p>";
        break;
    
    case 228:
        $str .= "Force d'Aether";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort d'opposition magique  <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école d'Altération.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école d'Altération.<br/>";
        $str .= "<b>Coût en PA : </b> 4PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 3 points de Maîtrise de la Magie et 2 points de vitesse.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JVit)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> La cible oppose au niveau du sortilège un JMM/4 arrondi à l'inférieur. Si le niveau du sort est supérieur à l'opposition magique de la cible, la créature est déplacée sur une case adjacente libre déterminée auparavant par le lanceur.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le mage séléctionne au moment de lancer son sort, une des 6 directions de la rose des vents afin de tenter de déplacer la créature dans cette direction.";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Force d'Aether sur Rat géant. <br/>";
        $str .= "Votre Jet de Vitesse est de ............: 40  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 42   <br/>";
        $str .= "Vous réalisez un sort de niveau : 11 <br/>";
        $str .= "La résistance de votre adversaire est de niveau ............: 8   <br/>";
        $str .= "Vous surpassez la résistance magique de votre adversaire et vous parvenez à le déplacer en X=565 Y=199 <br/>";
        $str .= "</p>";
        break;
    
    case 225:
        $str .= "Projection de l'âme";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort personnel  <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école d'Altération.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école d'Altération.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de Maîtrise de la Magie et 2.5 points de vitesse.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JVit)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Permet au sorcier de projeter son âme et donc de voir à travers elle à une distance maximum de 3 fois niveau du sort atteint. Tant que l'âme est projetée, le mage subit un malus de 50% MM et 50% Vitesse.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le mage sélectionne au moment de lancer son sort, une case où il veut projeter son âme. Si le niveau de sort n'est pas suffisant pour atteindre la case ciblée ou si toutes les cases à 3 cases autour de la destination , le sortilège est raté. Sinon, l'âme projetée à plus ou moins 3 cases de la destination souhaitée, et elle apparait dans le sélecteur de personnage en haut du menu de droite.";
        $str .= "<p width='700px' class='maintable'> Vous tentez une projection de l'âme en x=560 y=202 <br/>";
        $str .= "Votre Jet de Vitesse est de ............: 32  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 30   <br/>";
        $str .= "Vous réalisez un sort de niveau : 8 <br/>";
        $str .= "Vous avez réussi. Votre âme se trouve actuellement à la destination souhaitée.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Vous ne pouvez pas effectuer deux Projection de l'âme en même temps, vous devez d'abord annuler le sortilège (Récupérer son âme pour 0PA).";
        
        break;
    
    case 223:
        $str .= "Téléportation";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort personnel  <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école d'Altération.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau adepte de l'école d'Altération.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4 points de Maîtrise de la Magie et 4 points de vitesse.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JVit)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Permet au sorcier de se déplacer instantanément sur une case de son choix située à une distance maximum de (Niveau du Sort). Si l'éloignement de la case choisie est supérieur au niveau du sort alors la téléportation est ratée. Si la case choisie est occupée (personnage, ressource, bâtiment) le sorcier est automiquement tué.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le mage sélectionne au moment de lancer son sort, les coordonnées d'une case où il veut se téléporter.";
        $str .= "<p width='700px' class='maintable'> Vous tentez une téléportation en x=247 y=312<br/>";
        $str .= "Votre Jet de Vitesse est de ............: 52  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 30   <br/>";
        $str .= "Vous réalisez un sort de niveau : 8 <br/>";
        $str .= "Vous vous téléportez avec succès jusqu'à la destination souhaitée<br/>";
        $str .= "</p>";
        
        break;
    
    case 224:
        $str .= "Toucher de lumière";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien  <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école d'Altération.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau adepte de l'école d'Altération.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de Maîtrise de la Magie et 2.5 points de vitesse.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JVit)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Redonne à la cible du sorcier un total de 0,2pa par Niveau de Sort attient. Le sorcier subit alors un malus de maitrise de la magie de 20% pour le tour en cours et le suivant.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Touché de lumière sur Xalith<br/>";
        $str .= "Votre Jet de Vitesse est de ............: 52  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 50   <br/>";
        $str .= "Vous réalisez un sort de niveau : 14 <br/>";
        $str .= "Xalith regagne 2.8 points d'action.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le nombre maximum de PA de la cible ne peut pas être dépassé. Le sortilège ne peut pas être lancé sur soi-même.";
        $str .= "</p>";
        
        break;
    
    case 226:
        $str .= "Rappel";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien  <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école d'Altération.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau adepte de l'école d'Altération.<br/>";
        $str .= "<b>Coût en PA : </b> 12PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Vitesse et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 3 points de Maîtrise de la Magie et 2 points de vitesse.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JVit)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Permet au sorcier de rappeler un personnage auprès de lui. La distance de la cible ne peut doit pas être supérieure à 3*(Niveau du Sort) pour que le sortilège réussisse. A son prochain tour, la cible aura alors le choix d'utiliser tous les PAs de sa DLA pour être téléporté auprès du sorcier. Il devra faire ce choix avant toute autre action.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le mage sélectionne au moment de lancer son sort, un personnage inscrit dans ses contacts sur lequel il veut effectuer le Rappel.</br></br>";
        $str .= "Remarque : il est impossible de lancer le sort Rappel lorsque l'on se trouve dans un bâtiment.";
        
        $str .= "<p width='700px' class='maintable'> Vous utilisez Rappel sur Zakfar<br/>";
        $str .= "Votre Jet de Vitesse est de ............: 52  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 50   <br/>";
        $str .= "Vous avez réussi votre sort de rappel. Votre cible aura la possibilité de se téléporter à côté de vous à son prochain tour.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque le sort de rappel est lancé, un message est envoyé à la cible pour le lui indiquer. Le lanceur est alors libre de ses mouvements mais sa nouvelle position sera celle où sera téléporté sa cible au moment de la validation du Rappel.<br/>";
        $str .= "Il est donc possible d'être rappelé à l'intérieur d'un bâtiment si le lanceur s'y est introduit avant que le Rappel ne soit validé par sa cible.";
        break;
    
    /* ****************************************** SORTILEGE DE DESTRUCTION ******************************* */
    case 211:
        $str .= "Boule de feu";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sortilège d'attaque magique à distance  <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Portée : </b> 3 cases .<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 5 points de Maîtrise de la Magie et 3 points de Dextérité.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1 + JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> JAtt : Jet d'attaque magique * 1,2 | Dégât : Niveau du sort * 1,2D6 | Seul 50% de l'armure de la cible est prise en compte.<br/><br/>";
        $str .= "<i><small> Rappel : Le jet d'attaque magique est donné par Jet(1,5*Dex) + BMattaque si le lanceur porte une arme magique (bâton ou sceptre) et seulement JDex sinon.</small></i>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous lancez une boule de feu sur Combattant Squelette <br/>";
        $str .= "Votre Jet d'Attaque magique est de ............: 124  <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 59    <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre jet de MM est de : 82    <br/>";
        $str .= "Vous réalisez un sort de niveau 12 (14.4D6 de dégâts) <br/>";
        $str .= "Vous lui infligez 54 points de dégât. <br/>";
        $str .= "Son armure le protège et il ne perdra que 45 point(s) de vie.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que l'armure de la cible est en fait de 18, mais les dégâts de la boule de feu en ignore la moitié.";
        
        break;
    
    case 212:
        $str .= "Poing du démon";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sortilège d'attaque magique au contact  <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Coût en PA : </b> 7PA.<br/>";
        $str .= "<b>Portée : </b> Contact<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 3.5 points de Maîtrise de la Magie et 3.5 points de Dextérité.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1 + JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> JAtt : Jet d'attaque magique * 1,5 | Retire 4 * niveau du sort points de durabilité à la pièce d'armure de la cible visée par le sorcier. Si la durabilité de la pièce tombe à 0, elle devient inefficace et le sort cible aléatoirement une autre pièce d'armure et ce juqu'à épuisement des points de durabilité à retirer. Pour information, une pièce d'armure neuve a une durabilité de 60.<br/><br/>";
        $str .= "<i><small> Rappel : Le jet d'attaque magique est donné par Jet(1,5*Dex) + BMattaque si le lanceur porte une arme magique (bâton ou sceptre) et seulement JDex sinon.</small></i>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution sur PJ</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez poing du démon sur Cancrelat  <br/><br/>";
        $str .= "Votre Jet d'Attaque magique est de ............: 169  <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 100    <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre jet de MM est de : 121    <br/>";
        $str .= "Vous réalisez un sort de niveau 17 (68 points de durabilité) <br/>";
        $str .= "Bouclier moyen est touché et sa durabilité tombe à 0 <br/>";
        $str .= "Cuirasse de plate est touché et sa durabilité tombe à 0 <br/>";
        $str .= "Bottes de plate est touché et sa durabilité tombe à 47<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "<div class='subtitle'> Exemple de résolution sur PNJ</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez poing du démon sur Sauteur fauchard  <br/><br/>";
        $str .= "Votre Jet d'Attaque magique est de ............: 175  <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 49    <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire <br/>";
        $str .= "Votre jet de MM est de : 113    <br/>";
        $str .= "Vous réalisez un sort de niveau 16. <br/>";
        $str .= "La résistance du monstre a été diminuée. Son armure baisse de 19.2 points.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        
        break;
    
    case 227:
        $str .= "Appel de feu";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sortilège d'invocation  <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Coût en PA : </b> 10PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 5 points de Maîtrise de la Magie et 5 points de Dextérité.<br/>";
        $str .= "<b>Niveau du sortilège :</b> Racine Carré(JAtt Magique*JMM)/3.5 arrondi l'entier supérieur <br>";
        $str .= "<b>Effets : </b>Contre un malus de 10% sur la MM du magicien, ce sort crée un Feu Fol de niveau égal à (Niveau du sort)/1,5 arrondi à l'entier supérieur. </br>Le Feu-Fol attaque avec les sortilèges Boule de Feu ou Sang de Lave. </br></br>Il peut être controlé jusqu'à une distance de 5 cases maximum du sorcier. Au delà de cette distance, le Feu-Fol a 50% de chance d'être détruit et 50% de devenir sauvage. </br></br>De plus, pour empêcher que sa création ne disparaisse, le magicien devra réussir un test de MM à chaque début de tour. Mais il peut aussi choisir de le révoquer gratuitement à tout moment.</br></br>";
        $str .= "<i><small> Rappel : Le jet d'attaque magique est donné par Jet(1,5*Dex) + BMattaque si le lanceur porte une arme magique (bâton ou sceptre) et seulement JDex sinon.</small></i>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Appel du Feu <br/>";
        $str .= "Votre Jet de Maitrise de la magie est de : ............: 79 <br/>";
        $str .= "Votre Jet de Maitrise d'attaque magique est : ............: 120 <br/>";
        $str .= "Vous réalisez un sort de niveau 28 <br/>";
        $str .= "Vous avez réussi l'invocation d'un feu fol de niveau 19 <br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Remarques : </b>Si le magicien trouve un Feu Fol sauvage, c'est à dire sans propriétaire, il peut à l'aide du sort Appel de Feu tenter de le capturer. Il devra pour cela lancer le sort en étant au contact du Feu-Fol et réussir deux jets en opposition : un jet de MM supérieur à 80% de la MM du Feu-Fol et un Jet d'attaque magique supérieur à 80% du jet d'attaque du Feu-Fol. <br/><br/> <br/><br/>";
        $str .= "</p>";
        
        break;
    
    case 213:
        $str .= "Brasier";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sortilège d'attaque de zone  <br/>";
        $str .= "<b>Portée : </b> 2 cases  <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de Destruction.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Coût en PA : </b> 10PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4 points de Maîtrise de la Magie et 6 points de Dextérité.<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1 + JMM/8 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> JAtt : Jet d'attaque magique classique | Dégât : (Niveau du sort)D6*0,8. Le sort peut être lancé jusqu'à deux cases du lanceur et possède un rayon d'action d'une case. L'armure de la cible n'est pas prise en compte.<br/><br/>";
        $str .= "<i><small> Rappel : Le jet d'attaque magique est donné par Jet(1,5*Dex) + BMattaque si le lanceur porte une arme magique (bâton ou sceptre) et seulement JDex sinon.</small></i>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez brasier en 436-263 <br/>";
        $str .= "Votre Jet d'Attaque magique est de ............: 125 <br/>";
        $str .= "Votre jet de MM est de : 189 <br/>";
        $str .= "Vous réalisez un sort de niveau 25 <br/><br/>";
        $str .= "Xalith est atteint par le brasier <br/>";
        $str .= "Son Jet de défense est de ............: 21 <br/>";
        $str .= "Il perd 73 points de dégâts. <br/><br/>";
        $str .= "Esprit terrifiant est atteint par le brasier <br/>";
        $str .= "Son Jet de défense est de ............: 81 <br/>";
        $str .= "Il perd 81 points de dégâts. <br/>";
        $str .= "Esprit terrifiant a été TUÉ. <br/><br/>";
        $str .= "Goule est atteint par le brasier <br/>";
        $str .= "Son Jet de défense est de ............: 36 <br/>";
        $str .= "Il perd 76 points de dégâts. <br/>";
        $str .= "</p>";
        break;
    
    case 214:
        $str .= "Sang de Lave";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sortilège d'attaque de zone  <br/>";
        $str .= "<b>Portée : </b> 2 cases  <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de Destruction.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 8 points de Maîtrise de la Magie<br/>";
        $str .= "<b>Niveau du sortilège :</b> 1 + delta(JMM)/6 arrondi à l'entier supérieur <br>";
        $str .= "<b>Effets : </b> Sang de lave inflige une malédiction qui  otera 1,5*(Niveau du Sort)PV à la cible lors de ses 5 prochaines activations. Ni l'armure, ni la bulle de vie ne sont pris en compte.<br/><br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous attaquer Xalith <br/>";
        $str .= "Votre Jet de Maitrise de la magie est de ............: 193 <br/>";
        $str .= "Le Jet de Maitrise de la magie de votre adversaire est de ............: 83 <br/>";
        $str .= "Vous réalisez un sort de niveau 20 <br/>";
        $str .= "Vous infligez à votre adversaire une malédiction de 30 points de dégâts pour les 5 prochains tours.<br/>";
        $str .= "</p>";
        break;
    
    case 220:
        $str .= "Piliers infernaux";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Attaque de zone contre bâtiment<br/>";
        $str .= "<b>Portée : </b> 2 cases  <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de Destruction.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de Destruction.<br/>";
        $str .= "<b>Coût en PA : </b> 9PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Dextérité et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4.5 points de Maîtrise de la Magie et 4.5 points de dextérité.<br/>";
        $str .= "<b>Niveau du sortilège :</b> Racine Carré(JMM*JDex)/3.5 arrondi à l'entier supérieur <br>";
        $str .= "<b>Effets : </b> Le sortilège Piliers infernaux inflique à chacun des bâtiments touchés des dégâts à hauteur de (Niveau du Sort)*2D6 points de structure. Les emplacements choisis pour les piliers doivent être adjacents.<br/><br/>";
        $str .= "<i><small> Rappel : Le jet d'attaque magique est donné par Jet(1,5*Dex) + BMattaque si le lanceur porte une arme magique (bâton ou sceptre) et seulement JDex sinon.</small></i>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Piliers Infernaux en 440-260 et 440-261 et 440-262 <br/>";
        $str .= "Votre Jet de Maitrise de la magie est de ............: 165 <br/>";
        $str .= "Votre Jet d'attaque magique est de ............: 111 <br/>";
        $str .= "Vous réalisez un sort de niveau 39 <br/><br/>";
        $str .= "Vous attaquez Rempart niveau 1 <br/>";
        $str .= "Vous infligez au bâtiment une perte de 272 points de structure<br/><br/>";
        $str .= "Vous attaquez Echoppe niveau 4 <br/>";
        $str .= "Vous infligez au bâtiment une perte de 272 points de structure<br/><br/>";
        $str .= "Vous attaquez Palais du gouverneur niveau 2 <br/>";
        $str .= "Vous infligez au bâtiment une perte de 272 points de structure<br/><br/>";
        $str .= "</p>";
        break;
    
    /* *********************************** SORTILEGE DE PROTECTION *************************** */
    case 207:
        $str .= "Bénédiction";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien <br/>";
        $str .= "<b>Portée : </b> Contact <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de protection.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de protection.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Force et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de Maîtrise de la Magie et 2.5 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JForce)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Lorsqu'un mal ou une malédiction est infligé à une cible bénie, le niveau du mal est diminué d'autant que le niveau de la bénédiction, réduisant ainsi les effets. Si le niveau de la malédiction est inférieure au niveau au niveau de la bénédiction, cette dernière est repoussée. La bénédiction est active pour le tour en cours de la créature ciblée et les deux suivants.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Bénédiction sur Felgar <br/>";
        $str .= "Votre Jet de Force est de ............: 48  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 56   <br/>";
        $str .= "Vous réalisez un sort de niveau : 13 <br/>";
        $str .= "Vous créer une protection de niveau 13 contre les maux et malédictions pour le tour de la cible en cours et les 2 suivants.<br/>";
        $str .= "</p><br/>";
        $str .= "<div class='subtitle'> Application concrète </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Exemple de résolution contre une malédiction d'Arcxos <br/>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Malédiction d'Arcxos Felgar. <br/>";
        $str .= "Votre Jet de Vitesse est de ............: 100  <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 90   <br/>";
        $str .= "Le Jet de Maîtrise de la Magie de votre adversaire est de ............: 9   <br/>";
        $str .= "Vous réalisez un sort de niveau : 21 <br/>";
        $str .= " Votre adversaire est protégé par une bénédiction de niveau 13   <br/>";
        $str .= "Les effets de votre malédiction sont réduit de 10.5 à 4D6 de malus sur la caractéristique : FORCE pour le tour en cours de la cible et les deux suivants.   <br/>";
        $str .= "</p>";
        $str .= "Exemple de résolution contre une blessure mortelle <br/>";
        $str .= "<p width='700px' class='maintable'> Vous attaquez Felgar (compétence Coup de grâce). <br/>";
        $str .= "Votre Jet d'Attaque est de ............: 142 <br/>";
        $str .= "Le Jet de défense de votre adversaire est de ............: 64   <br/>";
        $str .= "Vous avez donc TOUCHÉ votre adversaire (Bonus de dégâts : 19)<br/>";
        $str .= "Votre Jet de Dégâts est de ............: 87  <br/>";
        $str .= " Vous lui infligez donc 106 points de dégâts.  <br/>";
        $str .= "Votre adversaire est protégé par une bénédiction de niveau 14   <br/>";
        $str .= "Sa protection est plus puissante que votre niveau de compétence et en annule les effets <br/>";
        $str .= "Son armure le protège et il ne perd que 87 points de vie   <br/>";
        $str .= "</p>";
        
        break;
    
    case 208:
        $str .= "Réveil de la Terre";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de protection.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de protection.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Force et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4 points de Maîtrise de la Magie et 4 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JForce)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Le sorcier choisi 1 à 4 cases vides et adjacentes à une distance de maximum de 2 sur lesquelles seront dressés les murs de terre. Chaque mur possède 15 * Niveau du sort Points de Structure.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Réveil de la terre <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 92   <br/>";
        $str .= "Votre Jet de Force est de ............: 43  <br/>";
        $str .= "Vous réalisez un sort de niveau : 12 <br/>";
        $str .= "Vous avez créé 4 mur(s) de terre de niveau 12   <br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les murs créés subissent une érosion de 1,5 % par jour de leur Points de Structure (PS), avec un minimum de 1. Un mur nouvellement créé est donc automatiquement détruit en moins de 67 jours.<br/>";
        $str .= "La liste et l'emplacement de chacun des murs controlés par le mage est affichée dans l'onglet Economie->vos batiments. C'est également à partir de cette liste que le mage peut choisir de révoquer un de ses murs. <br/>";
        $str .= "Notez également qu'un mage ne peut pas controlé plus de 10 murs simultanément. Il devra en révoquer certains s'il veut lancer à nouveau le sortilège Réveil de la Terre.<br/><br/>";
        $str .= "De plus, l'ensemble des murs contrôlés par le mage sont détruits à la mort de ce dernier.<br/>";
        
        break;
    
    case 209:
        $str .= "Barrière Enflammée";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien <br/>";
        $str .= "<b>Portée : </b> Contact <br/>";
        $str .= "<b>Niveau : </b> Aspirant de l'école de protection.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de protection.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Force et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de Maîtrise de la Magie et 2.5 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JForce)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b>  Crée une barrière de feu autour du personnage ciblé occasionnant des dégâts de 0,5D6 par niveau de sort ignorant l'armure à toute créature réalisant une attaque au corps à corps sur ce personnage. La barrière enflammée est active pour le tour en cours de la cible et les deux suivantes.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'> Vous utilisez Barrière enflammée sur Rinoks. <br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 86   <br/>";
        $str .= "Votre Jet de Force est de ............: 32  <br/>";
        $str .= "Vous réalisez un sort de niveau : 9 <br/>";
        $str .= "Vous avez créé une barrière enflammée qui infligera 4.5D6 de dégâts à tout adversaire attaquant Rinoks au corps à corps pour le tour en cours et les deux suivants.  <br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que la barrière enflammée se déclenche avant que l'attaque ne soit portée. Si la perte de points de vie engendrée par la barrière tue l'assaillant, l'attaque ne se fait pas. Le gain d'expérience dû à la mort de l'assaillant est obtenu par le lanceur de la barrière et non son porteur.";
        
        break;
    
    case 210:
        $str .= "Bouclier Magique";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien <br/>";
        $str .= "<b>Portée : </b> Contact <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de protection.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de protection.<br/>";
        $str .= "<b>Coût en PA : </b> 5PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Force et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 2.5 points de Maîtrise de la Magie et 2.5 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JForce)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> La défense de la cible est augmenté de (Niveau du sort)% pour le tour en cours et le suivant.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Vous utilisez Bouclier magique sur Xalith.<br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 79 <br/>";
        $str .= "Votre Jet de Force est de ............: 74 <br/>";
        $str .= "Vous réalisez un sort de niveau : 21 <br/>";
        $str .= "La défense de Xalith est augmenté de 21 %.<br/>";
        $str .= "</p>";
        break;
    
    case 219:
        $str .= "Bulle de vie";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de soutien <br/>";
        $str .= "<b>Portée : </b> Contact <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de protection.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de protection.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Force et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4 points de Maîtrise de la Magie et 4 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JForce)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Crée une bulle de vie autour de la cible permettant d'absorber 3 points de vie par niveau de sort. La bulle de vie ne disparait qu'une fois sa capacité d'absorption réduite à 0.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Vous utilisez Bulle de Vie sur Xalith.<br/>";
        $str .= "Votre Jet de Maîtrise de la Magie est de ............: 74 <br/>";
        $str .= "Votre Jet de Force est de ............: 98 <br/>";
        $str .= "Vous réalisez un sort de niveau : 21 <br/>";
        $str .= "Vous avez créé une bulle de vie capable d'encaisser 63 points de dégât.<br/>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Résolution passive </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Vous attaquez Xalith.<br/>";
        $str .= "............<br/>";
        $str .= "Vous lui infligez donc 78 points de dégâts. <br/><br/>";
        $str .= "Votre adversaire est protégé par une bulle de vie de niveau 21 qui absorbe 63 points de dégâts. <br/><br/>";
        $str .= "Son armure le protège et il ne perd que 12 points de vie .<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que la bulle de vie intervient avant l'armure, il n'y a donc aucun intérêt à créer une bulle de vie capable d'absorber moins de points de dégâts que l'armure de la cible.";
        
        break;
    
    case 215:
        $str .= "Souffle de Négation";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résumé </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<b>Type : </b> Sort de protection <br/>";
        $str .= "<b>Portée : </b> 3 cases <br/>";
        $str .= "<b>Niveau : </b> Adepte de l'école de protection.<br/>";
        $str .= "<b>Apprentissage : </b> Ecoles de magie dispensant les cours de niveau aspirant de l'école de protection.<br/>";
        $str .= "<b>Coût en PA : </b> 8PA.<br/>";
        $str .= "<b>Caractéristique principale :</b> Force et Maîtrise de la Magie.<br/>";
        $str .= "<b>Progression :</b> 4 points de Maîtrise de la Magie et 4 points de force.<br/>";
        $str .= "<b>Niveau du sortilège :</b> min(JMM, JForce)/3,5 arrondi à l'inférieur <br>";
        $str .= "<b>Effets : </b> Souffle de négation permet de rendre invisible un membre de votre groupe de chasse qui ne peut alors plus être ciblé, pas même par les actions de zone. La vie de la cible est alors réduit à 1. Elle peut alors se déplacer normalement mais toute autre action de sa part la fera réapparaitre au yeux de tous (entrer dans un bâtiment n'est pas considéré comme un déplacement). Elle peut également décider de réapparaitre à tout moment pour 0PA.";
        $str .= "Ce sort entraine un malus à la réapparition sur toutes les caractéristiques de min(-30,(-100 + (Niveau du sortilège))). Le malus durera 2 DLAs.";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>  Vous utilisez Souffle de négation sur Xalith.<br/>";
        $str .= "Xalith est désormais invisible et ne peut plus être ciblé.<br/>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Souffle de négation ne peut pas être lancé sur une cible dans un bâtiment.";
        
        break;
    
    // ***************************************************************//
    // SAVOIR FAIRE
    // ***************************************************************//
    
    case 399:
        $str .= "Réussir un savoir-faire";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous tentez un savoir-faire, vous jetez un D100. Si celui-ci est supérieur à votre maîtrise du savoir-faire, vous ratez votre action et vous perdez min(3; Temps Savoir-Faire) PA</p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 65% de chance de réussir ce savoir-faire <br/>";
        $str .= "Votre score est de ............: 71 <br/>";
        $str .= "Vous avez raté votre savoir-faire.<br/>";
        $str .= "Cette action vous a coûté 3 PA.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Si votre D100 est inférieur à votre jet de maîtrise, le savoir-faire est réussi et va s'effectuer. Avant vous jetez un deuxième D100 pour savoir si vous améliorez votre pourcentage de maîtrise<br/>";
        $str .= "Si votre D100 est supérieur à votre maîtrise du sortilège, vous gagnez 1% de maîtrise </p>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 68% de chance de réussir ce savoir-faire <br/>";
        $str .= "Votre score est de ............: 13         Savoir-faire réussi.<br/>";
        $str .= "Votre jet d'amélioration est de 90, vous améliorez votre maitrise de 1%.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que votre maîtrise d'un savoir-faire hors bonus ne peut dépasser 90%. Une fois cette valeur atteinte, il n'y a plus de jet d'amélioration <p/>";
        
        $str .= "<div class='subtitle'> Concentration </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous ratez un savoir-faire, vous obtenez pour le tour en cours, un bonus de maîtrise pour retenter ce savoir-faire <br/>";
        $str .= "Ce bonus est donné par la formule suivante : (100- pourcentage de maîtrise)/2 <br/>";
        $str .= "<p width='700px' class='maintable'>";
        $str .= "Vous avez 94% de chance de réussir ce savoir-faire (6% bonus) <br/>";
        $str .= "Votre score est de ............: 13         Savoir-faire réussi.<br/>";
        $str .= "Votre jet d'amélioration est de 90, vous améliorez votre savoir-faire de 1%.</p><br/>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ici vous avez tenté une première fois un savoir-faire que vous maîtrisez à 88%. En la retentant, vous bénéficiez de 6% de bonus (100-88)/2. <br/>";
        $str .= "Notez que dans ce cas, le jet d'amélioration a toujours lieu car votre maîtrise propre indépendamment du bonus est inférieure à 90% et un jet supérieur à 88% soit votre maîtrise propre sans bonus suffit pour améliorer votre maîtrise.</p><br/>";
        
        break;
    
    case 398:
        $str .= "Savoir-faire de raffinage";
        $str .= "</H1>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "La résolution de tous les savoir-faire de raffinage est la même et se déroule ainsi.";
        $str .= "<p width='700px' class='maintable'>Vous utilisez Charpenterie <br/>";
        $str .= "Votre première pièce est de niveau 3, votre seconde pièce est de niveau 1.  <br/>";
        $str .= "Vos chances de réussite sont donc de 60%.  <br/><br/>  ";
        $str .= "Votre score est de ............: 3         <br/>";
        $str .= "Vous parvenez à confectionner une pièce de niveau 5 <br/></p>";
        $str .= "<div class='subtitle'> Chance de Réussite </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "En supposant que la première pièce est celle dont le niveau est le plus élévée, vos chances de réussite de raffiner une matière première est défini par la formule : <br/>";
        $str .= " 105 - 15*(N1 - N2)-N1*5  borné par 1 et 100 avec N1 le niveau de la première matière première et N2 le niveau de la seconde.<br/></p> ";
        
        $str .= "<p class='rule_pg'>";
        $str .= "Le bonus octroyé par l'outil est alors donné par la formule : <br/>";
        $str .= "30 + 8*N - 6*N1 ou N est le niveau de l'outil</p>";
        
        break;
    
    case 397:
        $str .= "Savoir-faire d'artisanat";
        $str .= "</H1>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Apprendre un Artisanat </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous apprenez un savoir-faire d'artisanat, vous pouvez choisir le patron d'une pièce de l'artisanat choisi.";
        $str .= " En plus de la pièce d'équipement choisie, vous apprenez également à construire l'outil associée à votre artisanat.";
        $str .= " Notez que la fabrication de l'outil ne nécessite pas de matière première. <br/>";
        $str .= "Lorsque vous atteignez 55% de maitrise dans votre artisanat, vous pouvez retournez à l'école des métiers et en payant à nouveau le prix et les PA d'apprentissage du savoir-faire, apprendre à confectionner une nouvelle pièce.<br/>";
        $str .= "Vous pouvez à nouveau apprendre un autre patron lorsque vous atteignez une maitrise de 60%, 65%, 70% etc... ";
        $str .= "Lorsque vous apprenez à confectionner une nouvelle pièce d'équipement, votre pourcentage de maitrise chute de 2%. ";
        
        $str .= "<div class='subtitle'> Commencer à confectionner une pièce d'équipement </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Pour fabriquer une pièce d'équipement, vous avez besoin d'une matière première correspondant à l'artisanat utilisé.<br/>";
        $str .= "Dans un premier sélecteur, vous choisissez parmi les pièces que vous savez confectionner <br/>  ";
        $str .= "Dans un deuxième sélecteur, vous devez choisir la matière première que vous allez utiliser <br/>";
        $str .= "Enfin, dans un troisième sélecteur, vous devez choisir le niveau d'artisanat que vous voulez réaliser<br/>";
        $str .= "<img src='Artisanat.png'></br>";
        $str .= "Notez que le niveau d'artisanat visé ne peut pas dépasser le niveau de la matière première sélectionnée.</p></br><br/>";
        
        $str .= "<div class='subtitle'> Résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>Vous utilisez Artisanat du bois <br/>";
        $str .= "Votre Jet de Dextérité est de ............: 86  <br/>";
        $str .= "Vous réalisez donc une maitrise d'artisanat de niveau 5<br/>";
        $str .= "La matière première utilisée est de niveau 5 <br/>";
        $str .= "Mais vous avez préféré la travailler comme une matière première de niveau 4  <br/><br/>  ";
        $str .= "Vous avancez de 34% dans la confection d'un(e) Bouclier moyen de niveau 4  <br/></p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Notez que si votre maîtrise d'artisanat est inférieur au niveau d'artisanat que vous avez tentez, vous échouez dans la confection de la pièce. Vous ne perdez cependant pas votre matière première <br/></p>";
        
        $str .= "<div class='subtitle'> Caractéristiques associées et niveau de maîtrise </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Suivant le type d'artisanat que vous utilisez, les caractéristiques requises ainsi que la formule du niveau de maitrise varient. Ces informations sont récapitulées dans le tableau suivant<br/>";
        $str .= "<table width='700px' class='maintable'><tr align='center' class='mainbglabel'><td ><b> Artisanat </b></td><td ><b> Progression des jauges </b></td><td><b> Niveau de maitrise </b></td></tr>";
        $str .= "<tr  align='center'class='mainbglabel'><td align='center'> Artisanat du bois </td><td > 4 points de dextérité </td><td> 1+JDex/20 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Artisanat du fer </td><td > 4 points de force </td><td> 1+JFor/20</td> </tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Artisanat du cuir </td><td > 4 points de dextérité </td><td> 1+JDex/20 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Artisanat des écailles </td><td > 4 points de vitesse </td><td> 1+JVit/20 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Artisanat des plantes </td><td > 2 points de MM et 2 points de vitesse </td><td> 1+Racine(JVit*JMM)/15 arrondi au supérieur </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Artisanat du lin </td><td> 4 points de MM</td><td > 1+JMM/25 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Confection des objets magiques (Artisanat des gemmes) </td><td > 4 points de MM </td><td>  1+JMM/25 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Echantement mineur (Artisanat des gemmes) </td><td > 4 points de MM </td><td> 1+JMM/40 </td></tr>";
        $str .= "<tr align='center' class='mainbglabel'><td align='center'> Echantement majeur (Artisanat des gemmes) </td><td > 4 points de MM </td><td> 1+JMM/60 </td></tr>";
        $str .= "</table><br/>";
        
        $str .= "<div class='subtitle'> Continuer la confection une pièce d'équipement </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous voulez continuer la fabrication, vous devez sélectionner 'continuer la fabrication' dans le premier sélecteur puis choisir la pièce d'équipement que vous souhaitez poursuivre dans le deuxième sélecteur.";
        $str .= " Le sélecteur de choix du niveau n'a pas d'importance et peut être ignoré.";
        $str .= "<img src='Artisanat2.png'></br>";
        
        $str .= "<div class='subtitle'> Résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "<p width='700px' class='maintable'>Vous utilisez Artisanat du bois <br/>";
        $str .= "Votre Jet de Dextérité est de ............: 113  <br/>";
        $str .= "Vous réalisez donc une maitrise d'artisanat de niveau 6<br/>";
        $str .= "La pièce que vous travaillez est de niveau 3 <br/>";
        $str .= "Félicitations ! Vous avez terminé la confection d'un(e) Bouclier moyen de niveau 3<br/></p>  ";
        $str .= "Notez que le niveau d'artisanat requis est alors le niveau de la pièce d'équipement que vous travaillez";
        
        $str .= "<div class='subtitle'> Vitesse de fabrication </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Comme vous l'avez compris, certaines pièces d'équipement demandent plusieurs tours pour être confectionnées. La vitesse de confection d'une pièce d'équipement ";
        $str .= " dépend de votre maîtrise d'artisanat et de la difficulté de confection de la pièce défini par son niveau et son prix de base. <br/>";
        $str .= "Le pourcentage d'avancement dans la confection d'un objet est donné par la formule suivante : <br/>";
        $str .= "<p width='400px' align='center' class='maintable'>";
        $str .= "  <b> min(100, max(0, 100 - floor(10* sqrt(max(0,2*Pr-48))) + 4*(Ma-50)))  </b><br/>  où Ma représente votre pourcentage de maîtrise de l'artisanat et Pr le prix de la pièce à l'achat. </p>";
        
        $str .= "<div class='subtitle'> Les bonus des outils et de la guilde </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les outils doivent être équipés pour être utilisés. Vous pouvez toujours fabriquer l'outil de votre artisanat sans matière première.<br/>";
        $str .= "Les outils ont une durée de vie plus restreinte que les pièces d'équipement et il est impossible de les réparer une fois qu'ils sont cassés. Il faut donc penser à réparer régulièrement ses outils avant qu'ils ne soient détruit par l'usure. <br/>";
        $str .= "Les outils offrent un bonus dans la vitesse de confection des pièces d'équipement et permettent donc leur achèvement plus rapidement. Le gain est donné par la formule suivante : <br/>";
        $str .= "<p width='400px' align='center' class='maintable'>";
        $str .= "  <b> max(2, 8 * (Niveau_Outils+3) - 6*(Niveau_Objet))  </b></p>";
        $str .= "<p class='rule_pg'>";
        $str .= "Avec la guilde des artisans, dans la pièce des commandes, en payant 5 PO vous pouvez bénéficier des outils de la guilde. Ces outils vous donnent un bonus sur votre jet de maîtrise de l'artisanat à hauteur de : 7*Niveau_Guilde points :<br/>";
        break;
    
    case 396:
        $str .= "Artisanat des plantes";
        $str .= "</H1>";
        $str .= "</p>";
        $str .= "<p class='rule_pg'>";
        $str .= "L'artisanat des plantes permet de confectionner les potions à partir de plantes. Il existe 5 types de potion et 3 types de plantes : <br/>
			  - Les Racines de Razhot permettent de créer les potions de vie </br>
			  - Les Graines de Garnach permettent de créer les potions de dextérité et les potions de force <br/>
			  - Les Feuilles de Folianne permettent de créer les potions de vitesse et les potions de magie.<br/><br/>";
        $str .= "Lorsque vous apprenez l'artisanat des plantes pour la première fois, vous pouvez donc apprendre à confectionner l'un des 5 types de potions. <br/><br/>";
        $str .= "Contrairement aux autres artisanats, vous pouvez créer plusieurs potions en même temps.<br/>";
        $str .= "<p width='400px' align='center' class='maintable'>";
        $str .= "<b>  3-0.8*(N-1) + 0.1*(Ma-50) </b><br/> arrondi à l'entier inférieur avec N le niveau d'artisanat effectué et Ma votre pourcentage de maîtrise de l'artisanat.</p><br/>";
        $str .= "Notez qu'il est impossible de créer une potion de niveau supérieur à 10, même avec le meilleur outil.<br/>";
        $str .= "L'alambic d'artisan vous octroie un bonus qui peut selon les cas vous permettre de créer une potion supplémentaire. <br/>";
        $str .= "Le bonus octroyé par l'outil est donné par la formule suivante :";
        $str .= "<p width='400px' align='center' class='maintable'>";
        $str .= "<b>  max(2, 8*(N1+3)-6*N)/100*3 </b><br/> ou N1 est le niveau de l'outil et N le niveau de l'artisanat effectué</p><br/>";
        $str .= "<div class='subtitle'> Exemple </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Supposons que vous tentez de confectionner des potions niveau 5 avec 70% de maitrise d'artisanat<br/>";
        $str .= "La formule de confection ci-dessus donne 1,8 ce qui signifie que vous ne confectionnez qu'une potion puisque le nombre de la formule est arrondi à l'entier inférieur. <br/>";
        $str .= "Un alambic d'artisan de niveau 3 vous procurerait un bonus de 0,6 sur le nombre d'objet et vous permettrait donc de confectionner une potion supplémentaire (1,8+0,6= 2,4 --> 2 arrondi à l'entier inférieur). <br/>";
        break;
    
    case 395:
        $str .= "Artisanat des gemmes";
        $str .= "</H1>";
        $str .= "<p class='rule_pg'>";
        $str .= "Le fonctionnement de l'artisanat des gemmes est un peu plus complexe que celui des autres artisanats, car il offre plus de possibilités : la confection des objets magiques, les enchantements mineurs et les enchantements majeurs.<br/>";
        $str .= "Lorsque vous apprenez l'artisanat des gemmes, vous avez le choix entre apprendre à créer un objet magique ou apprendre un enchantement parmi les 5 types existants.<br/>";
        $str .= "Ces différentes possiblités entraînent des sélecteurs supplémentaires que nous allons vous apprendre à gérer selon vos besoins.<br/>";
        $str .= "<div class='subtitle'> Confectionner un objet magique </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Lorsque vous commencez la fabrication d'un objet magique, vous devez choisir dans le premier sélecteur parmi les objets que vous savez confectionner <br/>";
        $str .= "La confection des sceptres et des bâtons demande deux matières premières : un morceau de bois et une émeraude que vous devez renseigner dans le deuxième et troisième sélecteur.<br/>";
        $str .= "Chacun d'eux doit être de niveau au moins égal au niveau d'artisanat que vous souhaitez réaliser, c'est à dire le niveau de l'objet que vous souhaitez confectionner<br/>";
        $str .= "Vous choississez ensuite, comme pour les autres artisanats, le niveau de l'artisanat que vous souhaitez réaliser <br/>";
        $str .= "Enfin, vous devez ignorer le selecteur permettant de choisir un enchantement<br/>";
        $str .= "<img src='Artisanat3.png'></br>";
        $str .= "<div class='subtitle'> Continuer la confection d'un objet magique ou d'un enchantement </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Contrairement aux autres artisanats, les objets en cours de confection ou d'enchantement, apparaissent dans le premier sélecteur. ";
        $str .= "Vous devez alors ignorer les deux sélecteurs de matière première et celui de l'enchantement. ";
        $str .= "Le sélecteur de niveau n'est pas pris en compte.<br/>";
        $str .= "<div class='subtitle'> Enchanter une pièce d'équipement</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Les enchantements mineurs se font à l'aide d'une émeraude tandis que les enchantements majeurs nécessitent un rubis. ";
        $str .= "Vous pouvez créer un enchantement mineur sur n'importe quelle pièce d'équipement disponible dans votre inventaire, ";
        $str .= " alors que vous ne pouvez placer un enchantement majeur que sur une pièce d'équipement disposant d'un enchantement mineur terminé. <br/>";
        $str .= "Le niveau de l'enchantement ne peux dépasser le niveau de la pièce. Un morceau de bois n'étant pas nécessaire pour créer un enchantement, vous devrez ignorer le sélecteur en question.";
        break;
    
    case 394:
        $str .= "Artisanat du bois";
        $str .= "</H1>";
        $str .= "<div class='subtitle'> Le cas particulier des flèches </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Il existe deux types de flèche, comme vous l'avez peut-être déjà vu dans la section traitant de l'équipement, les flèches barbelées qui confèrent un bonus de dégât et les flèches torsadées qui confèrent un bonus d'attaque.<br/>";
        $str .= "A l'exception des autres objets, les flèches sont produites en lot. La formule de vitesse de confection d'un objet donnée dans le cas général n'a pas de sens pour les flèches<br/>";
        $str .= "Le nombre de flèches créés est donné par la formule suivante : <br/>";
        $str .= "<p width='400px' align='center' class='maintable'>";
        $str .= "<b>  5-1.4*(N-1) + 0.1*(Ma-50) </b><br/> arrondi à l'entier inférieur avec N le niveau d'artisanat effectué et Ma votre pourcentage de maîtrise de l'artisanat.</p><br/>";
        $str .= "Notez qu'il est impossible de créer une flèche de niveau supérieur à 8, même avec le meilleur outil.<br/>";
        $str .= "La râpes d'artisan vous octroie un bonus qui peut selon les cas vous permettre de créer des flèches supplémentaires. <br/>";
        $str .= "Le bonus octroyé par l'outil est donné par la formule suivante :";
        $str .= "<p width='400px' align='center' class='maintable'>";
        $str .= "<b>  max(2, 8*(N1+3)-6*N)/100*3</b><br/> ou N1 est le niveau de l'outil et N le niveau de l'artisanat effectué</p><br/>";
        $str .= "<div class='subtitle'> Exemple </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Supposons que vous tentez de confectionner des flèches niveau 3 avec 60% de maîtrise d'artisanat<br/>";
        $str .= "La formule de confection ci-dessus donne 4,6 ce qui signifie que vous ne confectionnez qu'une flèche puisque le nombre de la formule est arrondi à l'entier inférieur. <br/>";
        $str .= "Une râpe d'artisan de niveau 6 vous procurerait un bonus de 1,86 sur le nombre d'objet et vous permettrait donc de confectionner deux flèches supplémentaires (4,6+1,86= 6,46 --> 6 arrondi à l'entier inférieur). <br/>";
        break;
    
    case 393:
        $str .= "Savoir-faire d'Extraction";
        $str .= "</H1>";
        $str .= "</p>";
        $str .= "<div class='subtitle'> Exemple de résolution </div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Voici un exemple d'extraction avec outil : utilisation du Dialecte Kradjeckien, équipé d'une Gemme de Conviction de niveau 6.";
        $str .= "<p width='700px' class='maintable'>Vous négociez avec un Kradjeck ferreux de niveau 18<br/>";
        $str .= "Vous utilisez votre gemme à conviction.<br/>";
        $str .= "Félicitations, vous parvenez à obtenir un fer de niveau 4<br/>  ";
        $str .= "L'objet a été rangé dans votre sac de matières premières<br/>";
        $str .= "Cette action vous a coûté 3 PA<br/></p>";
        $str .= "<div class='subtitle'> Niveau de la matière obtenu</div>";
        $str .= "<p class='rule_pg'>";
        $str .= "Ce niveau est basé sur les 2 facteurs suivants : le <b>niveau de l'exploitation</b> et l'<b>utilisation d'un outil</b>.<br/>";
        $str .= "</p> ";
        $str .= "<p class='rule_pg'>";
        $str .= "L'influence du <b>niveau de l'exploitation</b> varie suivant le type de matière première extraite. ";
        $str .= "Le niveau du matériau obtenu sera égal au niveau de l'exploitation pour le lin, les herbes et les pierres précieuses (mines d'émeraudes et de rubis). ";
        $str .= "Le niveau des fers, cuirs ou écailles obtenus sera de (1 + niv_exploitation/8) arrondi à l'entier inférieur. ";
        $str .= "Enfin, le niveau du bois obtenu sera fonction du terrain :<br/>";
        $str .= "       * impossible d'en récolter sur la terre battue ou dans le désert,<br/>";
        $str .= "       * de niveau 1 en plaine, en montagne ou dans les marécages,<br/>";
        $str .= "       * de niveau 2 en forêt.<br/>";
        $str .= "</p> ";
        $str .= "<p class='rule_pg'>";
        $str .= "L'<b>utilisation d'un outil</b> procure un pourcentage de chance d'obtenir une matière de niveau N+1. La formule de calcul de ce pourcentage est la suivante :<br/>";
        $str .= "Bonus en pourcent = max (100, 82 - (val_outil_au_n1 * 2) + 5 * min(0, niv_outil - 5) )<br/>";
        $str .= "Vous remarquerez donc que chaque outil apporte un bonus de base différent dans sa spécialité. ";
        $str .= "De plus, investir dans un niveau d'outil supérieur n'est efficace qu'à partir du niveau 6. ";
        $str .= "Par exemple, les Cauchoirs offrent jusqu'au niveau 5, un bonus de 66% pour récolter un bois de niveau supérieur (71% au n6, 76% au n7, 81% au n8, etc).<br/>";
        $str .= "</p> ";
        
        break;
    
    default:
        break;
}

echo $str;
?>






<?php
echo "</div>";
echo "<div style='height: 5px;'><img src='" . CONFIG_HOST . "/pics/rules/Regle_Bottom.jpg' alt=\"\"/></div>";
echo "</div>";
?>



</div>
</body>

</html>