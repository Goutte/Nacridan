<?php
require_once ("../../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/NacridanCalendar.inc.php");

$NC = new NacridanCalendar();

?>


<img style="float: right; margin-left: 30px"
	src="<?echo CONFIG_HOST;?>/pics/rules/arrival.jpg" alt="" />
<div class='subtitle'>Introduction</div>

<p class='rule_pg'>Ce qui suit n'est pas nécessaire pour savoir jouer,
	ce sont juste les éléments du RP officiel du jeu. Il existe trois
	niveaux de RP officiel. Le premier est simplement les descriptions des
	monstres et des villes que vous croiserez. Ces descriptions
	apparaissent dans le profil, quand vous cliquez sur le nom du monstre
	ou de la ville dans le jeu. Si vous désirez en savoir plus vous pouvez
	consulter les textes présents ici même. Ils constituent le deuxième
	niveau et comportent un bref résumé de l'histoire du monde, une
	description courte de chaque région de l'Île ainsi que quelques détails
	supplémentaires sur les éléments clefs de ce monde : l'invasion des
	monstres, le rôle des temples. Le troisième niveau n'est pas encore en
	ligne, il contiendra une description détaillée des différentes races,
	une encyclopédie (incluant le bestiaire complet) et les textes relatant
	les hauts faits de l'ancien monde.</p>




<div class='subtitle'>Le calendrier</div>
<p class='rule_pg'>Le calendrier le plus couramment utilisé dans l'Île
	est celui dit Datation d'Artasse que nous allons détailler ci-dessous
	et que nous utiliserons par la suite. Il existe également un calendrier
	elfique, dit Datation de Krima. Les Elfes de Krima utilisent un
	calendrier sur dix mois de vingt-cinq jours (d'où la réputation qu'ils
	ont de vivre plus longtemps), datant de la fondation de leur cité. Les
	Nains n'utilisent pas de calendrier mensuel et journalier mais un
	décompte religieux avant la Fin du Monde découpé en heures, demi-années
	et siècles, qui ne s'est jamais exporté dans les autres cultures, que
	cela rend un peu nerveux. Les Doranes utilisent le calendrier Artassien
	comme tout le monde.</p>
<b>Les mois</b>
<p class='rule_pg'>
	L'an Un de la Datation d'Artasse correspond à la découverte de l'Île.
	Ce calendrier utilise 12 mois répartis en 3 saisons : <br /> *Temps de
	la Brume* <br /> - Fauchebrin (Septembre, premier mois de l'année)<br />
	- Roussylve (Octobre)<br /> - Sombrebruine (Novembre)<br /> - Morterre
	(Décembre)<br /> *Temps de la Longue Nuit*<br /> - Froidebise (Janvier)<br />
	- Grieffroi (Fevrier)<br /> - Clairsol (Mars)<br /> *Temps du Soleil*<br />
	- Boisvert (Avril)<br /> - Vifazur (Mai)<br /> - Semailles (Juin)<br />
	- Jachère (Juillet)<br /> - Orchaume (Août, dernier mois de l'année)<br />
</p>

<b>Les jours</b>
<p class='rule_pg'>
	Les mois du calendrier Artassien, sont divisés en septaines. A la fin
	de chaque mois, il y a deux jours sacrés, traditionnellement chômés
	sauf pour le mois de Fauchebrin qui termine par 7 jours sacrés. <br />
	<br /> - Jour du Labeur (Lundi)<br /> - Jour des Seigneurs (Mardi)<br />
	- Jour des Etoiles (Mercredi)<br /> - Jour des Ténèbres (Jeudi)<br /> -
	Jour des Hérault (Vendredi)<br /> - Jour d'Arryn (Samedi)<br /> - Jour
	d'Helion (Dimanche)<br />
</p>

<?php
if (isset($_GET["realdate"]))
    echo "La date demandée est : " . $NC->RealToArtassian($_GET["realdate"]);
else
    echo "Aujourd'hui nous sommes le " . $NC->RealToArtassian('now');
?>
<br>
<br>
Entrez une date des années 2013 à 2015 incluses au format YYYY-MM-DD
pour la convertir au calendrier Artassien:
<form>
	<input type="text" name="realdate"> <input type="submit"
		value="Convertir"> <input type="hidden" name="page" value="step14">
</form>

<div class='subtitle'>Chronologie</div>
<p class='rule_pg'>
	0 : découverte de l'Île par les premiers colons.<br /> <br /> 120 : <b>Fondation
		d'Artasse</b><br /> <br /> 222 : <b>Fondation d'Octobian</b><br /> <br />
	253 : Fondation de Santhoro-des-Collines<br /> <br /> 278 : <b>Fondation
		d'Earok</b><br /> <br /> 335 : Arrivée des Elfes au port d'Artasse,
	début de la sylvepeste. Les Elfes sont expulsés vers l'intérieur des
	terres.<br /> <br /> 344 : <b>Fondation de Krima</b><br /> <br /> 382 –
	384 : Construction de l'Arche.<br /> <br /> 387 : Proclamation par Guy
	III ''le Vengeur'', Roi d'Artasse, de la Bulle Dominion ad mare,
	ordonnant que « oncques ne puisse afaire prendre mer à telle nef qui
	lui soit bien propre ou au nom de tel tiers qui en possède la carène
	pour s'y livrer à navigation dans le but d'en tirer bénéfice si ce
	n'est qu'au plaisir et par grâce de Sa Majesté, dont les domaines
	comprennent toute mer pour Sa possession personnelle. ». Provoque
	immédiatement de fortes dissensions avec le clergé de Makero, et la
	guilde marchande.<br /> <br /> 390 : Proclamation de la Bulle Prohibo
	iuri mare condamnant à mort les capitaines et possesseurs de navires
	navigant en contradiction de la bulle Dominion ad mare.<br /> <br />
	392 : Helion 3 Sombrebruine. Jacquerie des Marchands Libre d'Artasse
	durant l'absense du Roi. Proclamation du Régiment Libre d'Artasse.
	Arryn 4 Terremorte. Massacre du 4 Arryn 392. Trahis par une partie de
	la population qui ouvre l'une des portes de l'enceinte, la milice des
	Marchands Libres, en dépit du soutien des Temples, est défaite par
	l'ost royal. A cette occasion, les chefs des mutins internes sont
	promus par le Roi sous le terme de Basilois. Si la plupart des
	marchands sont tués au combat ou pendus sur ce qui deviendra la Place
	de l'Exemple, les survivants, leurs familles et les membres du clergé
	de Makero, accusés de collusion, sont conduits aux limites du désert
	Safran, et abandonnés là pour y mourir « loin de toute mer ». Ils
	survivront pourtant, et fonderont Dust 7 ans plus tard.<br /> <br />
	399 : <b>Fondation de Dust</b><br /> <br /> 412 : Schisme des Ténèbres
	à Krima.<br /> <br /> 415 : Exil des Elfes Sarekiens.<br /> <br /> 419
	: <b>Fondation de Landar</b> par les Elfes Sarekiens<br /> <br /> 475 :
	Fondation de l'Université de l'Invisible. Apparition du terme « <b>Vallée
		de l'Izandar</b> » dans les capitulaires de l'époque.<br /> <br /> 500
	: Guerre des Troncs (Artasse vs. Santhoro)<br /> <br /> 508 : 3 Arryn
	Vifazur. La Grande Rétribution : invasion de Santhoro au terme de deux
	mois de siège ; la population mâle est massacrée, femmes et enfants
	réduits en esclavage et envoyés mourir dans les camps de travail
	d'Artasse. La cité de Santhoro est brulée, du sel est versé sur le sol
	et son nom est retiré des traités de l'époque. Date unanimement
	reconnue comme le point de départ de l'<b>Hégémon Artassien</b> (508 –
	772)<br /> <br /> 509 : Début des Guerres de Conquête.<br /> <br /> 590
	: Création du Duché de <b>Djin</b>. <br /> <br /> 595. Création du
	Duché Noir.<br /> <br /> 599. Création du Duché des Hauteurs.<br /> <br />
	685 : 2 Helion Sombrebruine. Le Basileus de Cuivre conduisant la XI ème
	Légion met le feu à Izandar pour rétribution de la Longue Nuit.<br /> <br />
	731 : 4 Arryn Froidebise. Nuit Ecarlate. Début de la grande révolte des
	Esclaves d'Artasse. 1 Seigneur Terremorte. Fuite navale, tempête.<br />
	<br /> 732. 2 Hérault Semailles. Proclamation de la République et
	fondation de la cité de <b>Tonak</b> par les esclaves évadés.<br /> <br />
	772. 4 Helion Grieffroi. Défaite des légions d'Artasse à la bataille
	des Champs Sanglants face à la Coalition des Peuples Libres (Dust,
	Earok, Izandar, Octobian, Tonak).<br /> <br /> 774 – 787 : Percement
	des Portes du Nord sous le massif des Crocs du Monde. Relie directement
	Izandar aux Territoires du Nord.<br /> <br /> 790. Généralement
	considérée comme le début de l'<b>Impérium Izandrin</b>, lequel
	s'adjuge les Territoires du Nord. Les frontières de ces territoires
	sont gardées sans relâche et l'immigration est limitée pour éviter la
	fondation d'une autre cité rivale tout en conservant le monopole des
	richesses Nordiques.<br /> <br /> 791. Début des Grandes Guerres
	Izandrines contre Octobian à l'Est et Krima au Sud.<br /> <br /> 879. 2
	Labeur de Fauchebrin. Défaite de l'armée d'Izandar à la bataille de la
	Gorge Noire, mort du Dauphin des Cimes.<br /> <br /> 2 Arryn de
	Grieffroi. Les mages Izandrins qui expérimentaient dans les Territoires
	du Nord perdent le contrôle de la Faille des Limbes. Début de la Longue
	Nuit. Chute d'Izandar et mort du dernier Prince.<br /> <br /> 3 Helion
	de Grieffroi. Date généralement donnée pour le début officiel des
	Guerres Démoniaques.<br /> <br /> 880. Début de la Grande Alliance. 5
	Hérault de Fauchebrin. Conclave d'Izandar. Fondation de l'Ordre des
	Temples.<br /> <br /> 883. Fermeture des Territoires du Nord par une
	barrière magique, au prix d'immenses pertes. Les démons sont bloqués à
	l'extérieur, contenus dans les contrées nordiques.<br /> <br /> 884. 2
	Etoiles de Grieffroi. Apparition du premier Portail relatée dans les
	Terres du Centre, à proximité d'Earok.<br /> <br /> 887. Orchaume.
	Concile oecuménique d'Earok II. Les Temples proclament que tous doivent
	concourir à la destruction des créatures. (<b>Début de la V1</b>)<br />
	<br /> 894. Jour des Ténèbres de la première semaine du mois de
	Sombrebruine. Concile oecuménique d'Earok III. Les Temples décident
	d'autoriser les aventuriers à lier leur âmes à un temple de leur choix
	pour être plus efficace dans la lutte contre les monstres. (<b>Début de
		la V2</b>)<br /> <br />
</p>
<div class='subtitle'>Les Démons et les Temples</div>

<p class='rule_pg'>
	Depuis l'aube des temps, les mages ont toujours eu tendance à
	expérimenter au dépend du bon sens. Parfois simplement par curiosité,
	souvent par soif de pouvoir. De loin en loin, les habitants de Nacridan
	ont vu des mages s'essayer à invoquer des Démons, de puissantes
	créatures issues des Sept Enfers. Après la défaite de l'armée
	Izandrine, l'Impérium dépêcha la fine fleur de l'Université de
	l'Invisible dans les Terres du Nord, dans le but d'y invoquer en secret
	le démon le plus puissant qui puisse exister : le Seigneur des Sept
	Enfers. Mais nulle magie ne fut assez puissante pour le lier à la
	volonté des mortels. Il massacra les mages et stabilisa le portail
	démoniaque pour faire traverser sa propre armée. L'Impérium Izandrin
	fut le premier à tomber. Puis les cités restantes s'unirent et
	réussirent à bloquer le Nord, pour y contenir les Démons. Cette
	victoire était dûe en grande partie à l'Ordre des Temples, une
	associations de prêtres magiciens puissants, qui étaient issus des
	différents clergés de l'Île.<br /> <br /> Ce ne fut pas suffisant.
	L'année suivante Le Seigneur de Sept Enfers utilisa une partie de la
	puissance du Portail Racine pour ouvrir des portails secondaires dans
	les terres du Sud. Ces portails téléportaient des monstres de Nacridan
	à l'intérieur des villes et villages. L'Ordre des Temples construisit
	un réseau de Temples dont le but était d'empêcher l'ouverture des
	portails à l'intérieur des zones habitées. Mais cela ne protégeait pas
	les routes et les portails continuèrent de s'ouvrir devant les
	voyageurs. Les cités étaient affaiblies par les guerres récentes, et ne
	pouvaient assurer la sécurité dans leurs territoires. Les mages des
	Temples cherchèrent alors à motiver le peuple à assurer sa propre
	sécurité. Pour cela ils mirent au point une technique permettant de
	lier l'âme d'un être conscient à un Temple, de sorte qu'il ne puisse
	mourir. En cas de mort, il se retrouve en fait télétransporté ailleurs.
	Un sortilège aussi puissant n'est pas sans effet secondaire. Le plus
	inattendu fut que tous les aventuriers qui recevaient cette bénédiction
	se retrouvaient liés psychiquement, de sorte que chacun était capable
	de savoir ce qu'avaient fait les autres récemment, en se concentrant un
	peu. Comme tout cela n'était pas suffisant, les Temples financèrent
	également un système de récompenses pour que tout trophé prouvant la
	mort d'un monstre soit racheté par les échoppes des villes et
	villages..
</p>

<br>
<br>
<br>



<div class='subtitle'>Les villes</div>

ATTENTION - version temporaire - une version retravaillée, avec les
fautes de français corrigées existe et sera mise en ligne dès que
possible.

<b>Earok</b>
<p class='rule_pg'>
	Blason : <i> De sinople au destrier d'or galopant, chiffré d'un bandeau
		d'or </i> <br> Earok la marchande dresse ses hautes et millénaires
	palissades parmi les vertes collines des plaines centrales, tirant
	profit de sa position unique au confluent des grandes routes qui
	traversent l'Ile de Nacridan de part en part, reliant le nord au sud et
	l'ouest à l'est. Demeure ancestrale des humains de l'Ile, Earok fut
	longtemps célébrée pour ses elevages de chevaux, et crainte pour les
	razzias et brigandages dont sa cavalerie redoutable se rendait
	coupable. Toutefois, le déclin progressif des chevaliers brigands et la
	montée en puissance des grandes guildes mercenaires et marchandes
	poussa les seigneurs de la Ville à se tourner vers le commerce, forme
	civilisée du vol, et où les risques de recevoir une flèche perdue sont
	nettement moindre. Désormais Earok est ville ouverte, et le seul danger
	qu'elle représente sont les receveur des taxes, ainsi que les nombreux
	lieux de débauche, de stupre et de luxure qu'elle réserve amicalement
	au voyageur nouvellement arrivé, au risque pour ce dernier d'y laisser
	jusqu'à son dernier ducat.
</p>

<img style="float: right; margin-left: 30px"
	src="<?echo CONFIG_HOST;?>/pics/rules/mapstart.jpg" alt="" />

<b> Dust</b>
<p class='rule_pg'>
	Blason : <i>Mi-partie d'azur et d'or à la nef de sable voguant</i><br>
	La Perle des Sables dresse ses blanches tours élancées vers un ciel
	impitoyable, oasis d'ombre entre les vagues de l'Océan et celles du
	Désert Safran, au sud de l'Ile. Dust est toute entière tournée vers la
	mer, dont elle tire sa subsistance et sa fortune. Sur ses hauts-fonds
	prospèrent de très rares huîtres diamantaires, dont les productions en
	la matière surpassent allègrement les plus beaux joyaux tirés des
	boyaux de la terre, tandis que du désert calciné, les elfes et les
	hommes au teint buriné qui vivent parmi les maisons chaulées de blanc
	de Dust ramènent les trésors brûlants, onyx, ambre solaire, encens
	précieux, miel du désert... Dust la blanche dont les maisons et les
	palais s'étalent nonchalament dans la chaleur de l'après-midi est aussi
	le seul havre pour les navires qui croisent entre Landar et Tonak ou
	Djin, tant l'eau est rare sur la côte désertique. Havre pour les
	navires marchands mais aussi pour les fines nefs des pirates, corsaires
	et frères de la côte par qui elle fut fondée, et qui règnent encore en
	maîtres prodigues à l'ombre des hauts minarets du Grand Temple de
	Makero où siège chaque second mercredi du mois le Conseil de la Haute
	Lice. Dust est d'ailleurs connue pour son rhum, le meilleur de toute
	l'Ile.
</p>

<b>Landar </b>
<p class='rule_pg'>
	Blason : <i>De sable à la faux de gueule</i><br> Landar la sombre se
	tapit dans l'ombre des Montagnes Noires, bâtie de la pierre même des
	monts qui l'encerclent, là où les rayons du soleil impitoyable
	s'estompent, et où la mer bleue et chaude des abords de Dust
	l'orientale s'assombrit, refroidit et semble soudainement agitée de
	mouvements brusques, agressifs, comme parcourus de mouvements obscurs.
	Les hautes masses noires des Montagnes s'avançent profondément dans les
	eaux glacés du golfe, enserrant la masse obscure de Landar dans une
	forteresse de pierre. De puissants forts défendent les accès maritime
	de la cité, qui se love derrière des murailles impressionnantes autour
	de l'immense Temple de Sareko, dont le clergé silencieux et vêtu de
	noir œuvre à des rituels sanglants derrière les portes toujours closes
	du Dôme, tout en régnant d'une main de fer sur la cité. Longtemps,
	Landar fut redoutée pour sa puissante flotte de galères de guerre aux
	coques noires, errant sur les mers en quête d'esclaves et de butins
	fastueux pour leur dieu avide. Temps désormais révolus, mais il ne fait
	pas encore bon vivre parmi la population laborieuse et triste de la
	ville noire.
</p>

<b>Nephy</b>
<p class='rule_pg'>
	Blason : <i>Mi-partie de sable et d'argent, au basilic de sinople
		couronné d'or</i><br> Loin à l'est, par delà les montagnes noires,
	s'étendent les marais gris du Désespoir. En leur coeur malodorant, hors
	de la portée de tous, même des dieux, dit-on, se dresse la ville floue
	de Néphy, dont les lueurs sulfureuses brillent timidement à travers les
	écharpes d'un brouillard trouble. Là, dans les marais hantés, demeures
	des vouivres et des basilics, parcourus de véritables légions de
	zombies, le voyageur chanceux - ou non - peut trouver au détour d'un
	brouillard particulièrement épais la dernière cité humaine de Nacridan.
	Autour d'une haute roche noire, entièrement creusée d'habitats
	troglodytes dont les lueurs percent les brumes du marais à des lieues à
	la ronde s'enroulent des bâtiments par dizaines sur des pilotis
	vermoulus, parcourus sans cesse par de longues pirogues barrées par des
	passeurs mélancoliques. Nulle loi ne régit Néphy, car Néphy est la
	demeure de ceux qui n'en n'ont plus. Là, dans ses canaux fétides, au
	creux de ses demeures obscures aux fenêtres bien souvent condamnées,
	tout se commet, tout s'expérimente... Dans ses places mouvantes formées
	de pirogues liées entre elles, se croisent Nécromanciens, invocateurs,
	démonistes, Frère de la nuit, alchimistes, prêtres défroqués, tueurs,
	aventuriers - ils sont partout ceux-là - ou vampires semi-sauvage...
	vendant, achetant, loin du regard suspicieux d'un justicier héroïque,
	tandis qu'au coeur du rocher noir, dans ses plus profondes cavernes,
	demeurent silencieuses et désertées les vastes salles de l'Union
	Obscure, dans l'attente du grand réveil du mal.
</p>

<b>Krima</b>
<p class='rule_pg'>
	Blason : <i>De sinople au soleil couchant d'argent</i><br> Krima des
	sylves se dresse au sein de la Grande Forêt, au plus profond de ses
	sous-bois enchanteurs. Bâtie il y a bien des siècles par les elfes
	sylvain de la maison d'Aeryal, elle en est restée depuis la demeure,
	sagement agrandie au fil des années, verdoyante et heureuse, ses
	maisons de pierre et bois s'agençant entre jardins fleuris, cascades
	joyeuses Krima entretient peu de liens avec l'extérieur, et aucune
	route ne profane les bois jalousement gardés par les Champatres, les
	soldats de la maison Aeryal. Les abords de la cité sont toutefois eux
	cultivés sur de vastes distance, champs de blé et d'avoine, vignes, qui
	constituent les principales richesses de la cité, outre sa prédilection
	pour les arts, qu'ils soient théâtraux ou magiques. , la principale
	faculté de magie elfique a depuis très longtemps installé ses quartiers
	dans le magnifique bâtiment du palais royal, tout de bois et d'ivoire
	végétal. Krima est une ville de culture sur laquelle règne un éternel
	printemps : on trouve de nombreux théâtres de verdures et surtout la
	Bibliothèque du Roi-Mage, propriété des rois de la maison Aeryal, mais
	dont l'accès est libre, bien que souvent occupé par les myriades de
	professeurs, érudits et autres philosophes qui prospèrent dans ses
	environs, au point que la production en livres annuelle de Krima est
	égale à celle de l'ensemble des autres cités réunies. Les autres
	plaisirs sont rarement satisfait à Krima, les elfes qui forment une
	largue partie de sa population étant abstinents et sobres...
</p>

<b>Artasse</b>
<p class='rule_pg'>
	Blason : <i>D'azur à la main d'or déployée à senestre tenant une nef
		d'argent voguant affronté à la main d'argent à dextre reployée tenant
		une épée d'or</i><br> La plus grande cité humaine de l'Ile est aussi
	bien souvent la porte d'entrée sur celle-ci pour le nouvel arrivant.
	Combien de jeunes aventuriers ont-ils pu contempler, l'oeil écarquillé,
	la splendeur de la Porte d'Argent, l'immense arche reliée à la mer par
	le célèbre port d'Orstieligad, via la Chaussée des Empereurs, bordée
	sur six lieux par les statues géante des empereurs successifs de
	l'Hégemon Artassien et qui protége le port d'Orstie des incursions
	ennemies, sous l'oeil bienveillant des grandes guildes qui recrutent
	activement dans les tavernes du front de mer, où arrivent régulièrement
	de vastes fournées de jeunes hommes et femmes avides de gloire et
	d'argent. Artasse fut longtemps la demeure des Rois Marins, qui
	s'appuyaient sur la vaste population : mais le caractère belliqueux de
	cette dynastie les entraina dans de ruineuses guerres de conquête qui
	saigna à blanc armées et trésors. Elle explique cependant l'éloignement
	des citées : au terme des guerres sempiternelles de conquête et de
	reconquêtes, les terres à des lieues à la ronde étaient jonchés d'armes
	et d'os, et les cités brulées, détruites, à la faveur des attaques et
	des contre-attaques. A la fin, une coalition entre Dust et Earok, les
	deux plus redoutable rivales d'Artasse parvint à vaincre l'armée du
	dernier des Rois Marins, permettant à Djinn, alors protectorat
	d'Artasse de rompre ses liens. La cité ne conserve plus maintenant
	qu'une ancienne forteresse et les reliefs de ses murailles arasée
	durant la guerre, et se consacre surtout au commerce maritime avec le
	continent, ainsi qu'à l'exploitation d'une fructueuse et inépuisable
	ressource : le jeune aventurier...
</p>

<b>Djin</b>
<p class='rule_pg'>
	Blason : <i>Ecartelé de gueules et d'azur à la tour d'argent
		triomphante</i><br> Ancien protectorat de la cité-état d'Artasse, Djin
	fut fondée par l'un des seigneurs de la cité comme forteresse et port
	militaire pour la défense de son flanc nord, face à la menace d'Izandar
	et d'Octobian. Encaissée dans un coude du Rugissant, elle s'échelonne
	entre tours crenelées et étendards sanglants flottant au vent du nord,
	autour de la masse redoutable de Castel Djin, demeure des Ducs de Djin,
	les Vystrose. Djin draîne maintenant une part non négligeable des
	richesses des montagnes, et se dispute régulièrement avec la République
	de Tonak, sa grande rivale, en général pour des raisons de droits de
	douanes, sans pouvoir l'emporter du fait de sa population réduite.
	Djinn vivote donc sans parvenir à retrouver sa grandeur du temps de
	l'empire artassien, à son grand dépit, et malgré une immigration naine
	qui se fait de plus en plus remarquée, contribuant à une nette remonté
	de l'artisanat métallurgique de la ville-forteresse.
</p>

<b>Tonak</b>
<p class='rule_pg'>
	Blason : <i>D'azur au poing d'or brandi et aux fers d'argent rompus</i><br>
	Seule et unique république de l'Ile de Nacridan, Tonak revendique avoir
	été crée lors de la grande révolte des esclaves à Artasse, en 2E745, au
	temps de Martin V d'Artasse. Sous le commandement de Vladimir
	Na'Zheerden et Algabor Frey'luminar, deux jeunes esclaves capturés lors
	du sac de Dust, la masse des esclaves qui trimaient dans la cité se
	révolta en une nuit particulièrement sanglante, prenant d'assaut les
	navires de la flotte marchande, et cinglant vers la liberté. Une
	tempête mis fin à ce projet en drossant la flotte le long de
	l'embouchure du Rugissant, sur les hautes falaises blanches, au sommet
	desquelles les palais et les murailles de Tonak se dressent désormais,
	uniquement accessible du coté marin par un système de panier tirés par
	de longues et lourdes cordes. Tonak se distingue par la maitrise totale
	de l'embouchure du Rugissant, notamment via une chaîne de tours de guet
	hérissée de balistes incendiaires, redoutables pour les navires rétifs
	à payer les taxes marchandes, ainsi que le contrôle des vastes terres
	arables jusqu'aux montagnes, produisant notamment une viande d'agneau
	de qualité, ainsi qu'un vin blanc sec particulièrement recherché.
</p>

<b>Octobian</b>
<p class='rule_pg'>
	Blason : <i>Ecartelé de gueules, de sable et d'or, aux cinq marteaux
		d'argent formant étoile</i><br> Les cinq quartiers troglodytes
	d'Octobian s'enfoncent loin sous les roches et les pierres, en
	centaines de cavernes, salles, et mines, sans cesse illuminées par les
	flammes des torches naines, et par le choeur incessant de la
	population, formée par la plus grande communauté naine de l'Ile,
	répartie entre les cinqs grands clans nains que sont les Alehammer,
	Drunkenshield, Beerstein, Humulusword et Ironbarrels, dont le conseil
	des Cinq-Cent Cinquante-Cinq anciens régit chaque aspect de la vie de
	la cité par le moyen d'incessantes sous-commissions où ils tentent de
	faire prendre l'avantage à leur clan d'origine via de subtiles arguties
	de droit minier - lequel rend par comparaison presque palpitant le
	droit administratif commun, pour l'élément de comparaison. Un mode de
	gestion que l'on estime préférable au précédent, où chaque contestation
	se réglait à la hache, ce qui entraina la disparition d'au moins deux
	clans, sur les sept de départ. Bien que souterraine, Octobian est un
	endroit où il fait bon vivre, l'ambiance étant particulièrement
	chaleureuse parmi les nains bons vivants (une taverne pour six
	habitants environ). Pour les guerriers ainsi que les armuriers, c'est
	également une forme de paradis sous terre, tant la profusion de forges
	et hauts fourneaux au mètre carré permet de s'approvisionner en armes
	et armures à prix presque réduits...
</p>

<b>Izandar</b>
<p class='rule_pg'>
	Blason : <i>De sable aux aigles de gueules affrontées l'un couronné
		d'or et l'autre d'argent</i><br> La cité des cimes aux toits d'or se
	dresse au pied des immenses Crocs du Monde, point le plus élevé de
	l'Ile. Autrefois fabuleusement riche de par son contrôle exclusif sur
	la Porte du Nord, la gloire d'Izandar et sa fortune lui assura durant
	de nombreuses années l'hégémonie sur Nacridan. Toutefois, la perte de
	sa richesse fut fatal à l'empire, écrasé par la remuante Artasse.
	Dressée orgueilleusement au sommet de son pic, dominant de ses hauts
	toits couverts d'or les plaines centrales, Izandar est également connue
	comme une très ancienne citée de Magie, la puissance des éléments s'y
	exprimant dans toute son ampleur. Il ne faut pas s'étonner alors d'y
	rencontrer un nombre élevé de doranes, de même que le centre historique
	de la recherche thaumaturgique, l'Université de l'Invisible. Force
	temporelle et pouvoir magique ont un accord, cependant, et Izandar est
	dirigée par un Patricien Unique, élu démocratiquement à la majorité
	qualifié de lui-même, et qui entend bien reprendre une place
	prépondérante dans la vie politique de l'Ile...
</p>


<br>
<br>
<br>


<div class='subtitle'>Géographie</div>


<p class='rule_pg'>
	1) <b>Côte aux Roches</b> <br /> Région d'Artasse, Rivage Sud-Est de
	l'Ile<br /> Comprise entre : Nord, l'embouchure du Rugissant, Ouest, la
	Forêt d'Yeuse, Sud, Rivage de la Mer d'Onyx.<br /> <br /> La grande
	cité d’Artasse domine les terres alentour, essentiellement des plaines
	d’herbes incultes gardant la trace des batailles passée, en particulier
	dans la toponymie des lieues dits, particulièrement martiale tout
	autour de la région (La Tour Brisée, le Champ de la Charge, Dernier
	Carré, le Mortval, sont des noms courants) et des champs, notamment
	complantés de vergers, pommiers (la fameuse Rougette d’Artasse) et
	poiriers (dont les cultivateurs tirent une eau de vie pas piquée des
	hannetons). Sols argilo-calcaires. Habitat relativement présent :
	nombreux corps de fermes, chaumières plus ou moins délabrés. Quelques
	chemins, un ou deux bourgs. La ville aspire de tout son poids la
	population autour, et ne laisse pas grand monde vivre à l’extérieur de
	ses enceintes délabrées. Faubourgs étendus depuis qu’Artasse n’est plus
	une ville guerrière. A l’ouest d’Artasse, les champs se font plus
	rares, et hormis la Route des Collines, en direction d’Earok, les
	chemins disparaissent. Des collines s’élèvent, boisées de loin en loin,
	jusqu’aux frondaisons de la Forêt d’Yeuses, une zone couverte de
	chênes, ormes, hêtres et frênes, où vivent un grand nombre d’animaux
	sauvages. Cette région est très parcourue de voyageurs et de jeunes
	aventuriers, qui y font leurs premières armes en chassant les créatures
	sauvages dont ils peuvent ensuite revendre les peaux aux paysans et aux
	villageois. La Forêt d’Yeuses s’étend sur une futaie d’une vingtaine de
	lieues, en des collines et vallons boisés de faible profondeur, parsemé
	de villages forestiers tel que La Yeuse ou Vaara, pour se terminer prêt
	des vastes plaines d’herbe verte des Plaines centrales, qui s’étendent
	de loin en loin sur une bonne partie du Centre de l’Ile.<br /> <br />
	2) <b>Plaines Centrales</b></br> Région d'Earok</br> Limites : Nord,
	les contrefort du Gramont et la Grande Forêt ; Ouest, le Bois-l'Orage
	et les Montagnes Noires ; Sud, le Lac d'Aon et le Désert Safran ; Est :
	la Forêt d'Yeuse.<br /> <br /> Les vastes plaines centrales sont lad
	emeure des cavaliers d’Earok. Parcourues de troupeau de cavales libres,
	et de chevaliers et sang-coureurs de la Maison d’Earok, ainsi que de
	nombreuses caravanes marchandes, qui traversent la mer d’herbe verte.
	L’herbe y est riche, l’eau vive, descendue pure du Gramont au nord,
	irrigue en de nombreux rus et cours d’eau les plaines. La ville
	marchande d’Earok se situe au centre de ces plaines. Au Sud, les
	collines de la Plaine centrale s’affaissent autour de la Cuvette du Lac
	d’Aon, qui recueille l’ensemble des eaux qui irriguent la plaine. Sol
	argileux, voire très argileux. Habitat particulièrement rare : hors la
	capitale d’Earok, très peu de structures sur la plaine libre, les
	cavaliers et les éleveurs y errant selon des schémas ancestraux et
	soigneusement établis. Les Maîtres Dresseurs des Maisons du Cheval
	décident encore de quelle parcelle de la plaine appartient à quelle
	famille pour la saison. Maître Dresseur dont la réputation est de
	pouvoir dresser tout, et notamment en des temps plus anciens, les
	hommes, quand Earok était une plaque centrale des trafics sur l’Ile,
	formant avec Landar, Dust, Nephy la ligue de l’Ouest. De loin en loin,
	le long des routes se dresse une Motte couronnée de bois et de pierre :
	la demeure d’un ancien seigneur brigand, où il entreposait captifs et
	rapines, désormais auberge pour les marchands et les convois en route
	vers les entrepôts d’Earok, point de passage obligé pour tout voyageur
	désireux de se rendre quelque part en Nacridan.</br> </br> 3) <b>Désert
		Safran</b></br> Région de Dust</br> Limites : Au Nord, les Plaines
	Centrales, A l'Ouest, le Bois-l'Orage, au Sud, la Grande Mer, à l'Est,
	la Mer d'Onyx<br /> <br /> Au sud de la cuvette du Lac d’Ajonc, le sol
	cessant d’être approvisionné en eau par les montagnes se dessèche. La
	chaleur augmente, la terre devient friable. Terre de pierrailles
	sèches, de petites maisons de pierre, et de culture d’anis et
	d’olivier, ainsi que d’une vigne coriace qui donne une piquette
	particulièrement agressive. Puis commence au bas des pentes de la Côte
	d’Anis les dunes calcinées du Désert Safran, dont le sable ocre roule
	sans fin sous la poussée du vent chaud. Nul n’habite les abords désolés
	du Désert, que des créatures de feu, qui ne craignent pas la chaleur
	que rien n’apaise. Il est si difficile de vivre dans les profondeurs du
	Désert Safran, que l’on impute les conditions climatiques à un accident
	magique. Au bord de la Grande Mer s’étale impudemment la Citée
	portuaire de Dust, havre des corsaires et des marins audacieux, perle
	des mers d’eau et de sable, dont les remparts de brique chaulées de
	blanc et les hautes tours dispense une bienfaisante ombre fraiche. Le
	Désert Safran s’étend sur bien des lieues à l’Ouest comme à l’Est, mais
	pourtant même sa puissance doit refluer devant celle des hautes
	Montagnes Noires. Sombre crocs sortis de la terre noire, devant les
	masses de pierre viennent mourir à la fois les sables de Safran et les
	herbes vertes des plaines centrales dont elle marque la limite la plus
	au Sud-Ouest.<br /> <br /> 4)<b> Le Bois-l'Orage</b> </br> Forêt au sud
	de Landar et à l'ouest des plaines centrales<br /> <br /> Limite
	ouestienne du grand désert, il s'agissait autrefois du Duché Noir, le
	plus petit et le plus sauvage des Duchés de l'Hegemon. Les arbres
	centenaires du Bois-l'Orage se dressent sous une pluie perpétuelle. Née
	de la confrontation entre les masses d'air chaud venue du désert et le
	froid glacial des Montagnes Noires, d'incessantes nuées crèvent au
	dessus de cette région perpétuellement maussade. Quelques chemins
	traversent le Bois-l'Orage, reliant entre eux les quelques villages
	perdus et bastions désolés des Seigneurs Pluvieux (qui s'appellent
	plutôt eux-mêmes les "Seigneurs de l'Orage et du Tonnerre Grondant"),
	une vieille ligue de barons oubliés, qui ne rendent hommage qu'à leur
	propre existence, et refusent d'être banneret des puissances de
	Nacridan. Non, il est vrai, que ces-dernières y tiennent beaucoup.
	Comme dit le proverbe "Va donc mourir d'ennui au Bois-la-Pluie"... On y
	trouve également un temple-forteresse de l'Ordo to Thalatta, ces
	mages-chevaliers qui ont juré service à Makero. Pour les gens qui
	désirent la paix et la tranquillité, loin des fouineurs de tout bord,
	le Bois-l'Orage est toutefois un endroit de choix : il est bien rare
	qu'un quelconque paladin vienne s'amuser à fouiller pendant des
	semaines les futaies détrempée, au risque de périr d'une bronchite
	aiguë. Même si l'ancienne présence de Durga, la Forteresse principale
	de la Fédération, à quelques dizaines de lieues de là, tend
	progressivement à faire évoluer cet état de fait.<br /> <br /> 5) <b>Les
		Montagnes Noires</b><br /> Région de Landar<br /> <br /> Il y a
	beaucoup de choses à dire sur les Montagnes Noires. Sans être aussi
	hautes que les Crocs du Monde, elles peuvent toiser de haut les nuages
	les plus bas. Elles tirent leur nom de la couleur de leur roche, sombre
	et froide, que le soleil réchauffe rarement. Mais également de leur
	histoire, longue et sanglante. En son sein, disent les anciennes
	légendes, vivent encore dans leurs salles glacées, les elfes noirs au
	cœur de pierre, attendant la mort d'Arduin pour sortir ravager les
	terres libres. Ici, sur le terrible Pic Ardent, régna le Comte Dément,
	qui érigea une muraille des crânes de ses ennemis morts. Là, dans les
	combes oubliées des races pensantes, erraient des créatures sans nom,
	hormis dans les chroniques les plus anciennes. On y trouvait encore il
	y a peu des géants et des trolls, des orques et des gobelins, et
	eux-même ne montaient vers certains sommets, n'entraient pas certaines
	grottes, et faisaient de larges détours pour éviter des vallées
	ombreuses. Rares sont les chemins qui traversent les Montagnes Noires.
	Sa face sud comme sa face ouest sont quasiment dépourvue de végétation.
	Quelques pins à peine se serrent les uns contre les autres sur cette
	muraille naturelle. Il en est bien autrement a l'est : les sombres
	pinèdes de la Forêt du Crépuscule descendent en rang serrés vers les
	brumeuses étendues des Marais du Désespoir, et on dit que le soleil n'y
	flamboie qu'au moment du couchant, quelques brèves minutes. Les elfes
	eux-même l'évitent : nombreux parmi eux croient que c'est là que s'en
	vont pour l'éternité les âmes de leurs défunts. On ne trouve pas de
	nains dans les Montagnes Noires. Il n'y a rien qu'ils y veuillent
	trouver en creusant. Hormis quelques aventuriers de haut niveau en
	quête de fortune et de gloire, ou de quoi que veuille trouver cette
	engeance, il n'y d'autre demeure dans les monts que les ruines de Durga
	Mudhito, la puissante forteresse de la Fédération des Peuples Libres.
	Les Montagnes Noires ont longtemps été une barrière naturelle pour la
	ville au cœur obscur de Landar, aux rites étranges et a la foi
	sanglante. La pierre y est toujours froide, qu’aucun soleil ne
	réchauffe jamais, celui-ci étant perpétuellement voilé par l’eau que la
	chaleur du Désert fait monter, et que la pierre froide refroidit et
	condense en une brume perpétuelle.<br /> <br /> 6) <b>Les Marais du
		Désespoir </b><br /> Région de Néphy<br /> <br /> Les contreforts est
	des Montagnes Noires sont peuplés d’arbres sombres, aux troncs gris et
	lépreux, forêts d’arbres morts et de sinistres conifères, dont les
	racines tordues viennent s’abreuver dans les eaux mortes des Marais du
	Désespoir, plaques de végétation verte pâle et grisâtre qui s’accroche
	à la tourbe humide ; au milieu de grands bouquets d’ajoncs et des
	troncs d’arbres depuis longtemps atteints par la pourriture. La
	progression y est mortellement malaisée, les eaux traitres et la terre
	plus encore, mais c’est au centre des Marais que se dresse la ville
	instable de Nephy, toute noyée de brumes et de brouillards souffre,
	d’exhalaisons suspectes, et dont les rues sont des canaux. Il est
	toutefois impossible à une armée d’approcher les abords de la cité par
	voie terrestre, et les cieux sont quadrillés par une forte troupe de
	chevaucheurs de wyvernes et gargouilles à la vigilance sans faille.
	Regroupé autour de la Roche Obscure, la cité de Néphy est pourtant un
	important centre de vie, qui regroupe de très nombreux habitants ou
	voyageurs de passage. Ceux-ci parviennent souvent à la ville par le
	biais des chemins parcourus par le petit peuple des Gens du Palus
	connaissent et aiment le marais. Eux seuls en connaissent les voies et
	les abris, et ce sont eux qui veillent à l'approvisionnement de la cité
	instable, en vertu d'un ancestral accord de protection et d'aide. Gare
	à ceux qui souhaiteraient passer sans leur accord ! Ceux que le marais
	n'avalera pas auront affaire aux flèches au poison foudroyant, aux
	sorts fourbes et aux pointes aiguës des tridents.<br /> <br /> 7) <b>
		Les Bois Gris</b><br /> Région à l'est de Néphy<br /> <br /> Au nord
	de Nephy, seules quelques âmes perdues rôdent dans les bois déserts des
	Bois Gris, les plus anciens de toute l’Ile. On raconte que les arbres
	multiséculaires qui peuplent ce bois silencieux se meuvent et parlent
	et qu’ils haïssent ceux qui n’ont pas d’écorce, et que leur courroux
	peut s’exercer de façon très définitive. Un Bois ancien, dans lequel
	les hommes de Nephy ne s’engagent jamais à la légère et toujours en
	nombre. Des monstres antiques y demeurent tapis. On y trouve quelques
	rares villages de bucherons et, à la lisière, des fermes qui vendent
	leurs récoltes à Néphy et aux villages de Grève Lointaine.<br /> <br />
	8) <b> Grève Lointaine</b></br> Région côtière à l'ouest de Nephy. <br />
	<br /> Point le plus occidental des terres centrales, Grève Lointaine
	est un endroit froid et quelque peu désolé, landes de genêts et de
	bruyères perpétuellement battues par les vents. Aucune route ne mène
	là, et les pimpants chevaliers de la Fédération n'ont que faire des
	quelques villages de pêcheurs isolés du monde qui se pressent dans
	l'ombre des Bois Gris, dans les criques isolées, derrière de hauts
	murs. Battus par les flots, en vues des côtes, se dressent quelques
	petites iles et ilots habités, point les plus excentrés de Nacridan...
	le Port-sous-le-Vent, le Roc au Marin... l'ile de Lumière étoilée... On
	y trouve quelques châteaux et petits domaines, noblesse ou aventuriers
	enrichis, mais ce sont pour la plupart des excentriques et des
	solitaires, et ils ploient le genoux devant la bannière baucéant de
	Néphy. Ainsi, souvent, devant le pavillon au navire de Dust ; les
	voiles grises des corsaires de la Citée blanche font bien souvent fait
	relâche dans les ilots et les villages de Grève Lointaine, de retour de
	leurs lointaines croisières vers le ponant, et des liens étroits
	relient les deux communautés. Les d'Argenlieu, les Grislevent, les du
	Noirflots, les de Greif, toute la fortune des vieilles familles nobles
	du Port-sous-le-vent et de Lumière étoilée... nombreux sont les fils
	cadets et puinés que l'on "donne à la mer" comme on les donnerait au
	temple ou à la paladinerie dans d'autres régions. Et puis, les beaux
	marins aux mains pleines d'or, aux rodomontades plein la bouche et aux
	yeux mystérieux ne laissent pas les filles du coin indifférentes - et
	il est rare que le navire qui s'en repart pour Dust n'ai pas à son bord
	quelque nouveau mousse, né une douzaine d'année avant, neuf mois après
	le départ du même navire ou de son frère.<br /> <br /> 9)<b> La Grande
		Forêt</b> (Elbereth o Cuil o Taerendil Vaara)<br /> Région de Krima<br />
	<br /> la Grande Forêt, ou le Vaara elfique, est la patrie des elfes de
	Krima, qui en connaissent presque seuls les nombreux sentiers et les
	parcourent sans cesse. La forêt est assez peuplée et giboyeuse, les
	monstres moins nombreux que les arbres antiques, du fait des
	patrouilles régulières des guerriers de Krima. Nombreux arbres anciens,
	toujours verte, figée dans un éternel printemps par les belles gens de
	la cité elfe. Une clairière s'ouvre assez largement au coeur de la
	Grande Forêt, cultivée pour nourrir la ville et les villages sylvains.<br />
	<br /> 10) <b>Les Monts Gris</b></br> Région de Djinn<br /> <br /> Des
	Monts Gris à l’Est nait la source du Rugissant ; ce grand fleuve coule
	jusqu’à la Mer d’Ivoire, baignant sur son passage de nombreux villages
	et bourgs principalement humains, sous la puissance des Ducs de Djin,
	les Vystroses. Théoriquement vassaux de l’Empereur d’Artasse, la mort
	il y a plusieurs siècles du dernier d’entre eux a permis à la
	Forteresse de Djin d’acquérir une parfaite indépendance, qu’elle met à
	profit pour de longues et nombreuses guéguerres avec la République de
	Tonak. La Voie du Nord part de Djinn et monte le long de pistes
	sinueuses et de cols parfois enneigés jusqu'aux hautes demeures de
	l'Izandar. De nombreux villages parsèment les montagnes ; la vie y est
	rude, et souvent frugale, et les monstres y errent en nombre imposant.
	On y trouve donc de nombreux aventuriers.<br /> <br /> 11) <b>Les
		falaises du Rugissant</b><br /> Région de Tonak<br /> <br /> Depuis la
	grande Arche du Rugissant, seul pont qui permet de franchir le fleuve
	autrement qu'à bateau, jusqu'à l'embouchure de celui-ci s'étendent les
	terres sous la suggestion de la république souveraine de Tonak. Grenier
	à grain de l'ile, ces grands espaces sont bien desservis par de
	nombreuses routes pavées, lieu d'intenses échanges commerciaux. Des
	bourgs prospères et des fermes imposantes parsèment un paysage
	essentiellement composé de champs, plantations et vignes. Des bois
	giboyeux forment les seules tâches de nature non domestiquée à des
	lieues à la ronde. Tout au long du fleuve, s’égrènent les petits
	villages fluviaux, autour de ports fourmillant d'activité, chargeant et
	déchargeant sans cesse sur les gabarres les produits à destination de
	l'ensemble de l'Ile. Vers l'Ouest, la Grand'Route des Plaines longe le
	Rugissant et ses chemins de halage jusqu'à Djinn et plus loin encore.
	Au Nord, l'ancienne Route Impériale monte vers Octobian.<br /> <br />
	12)<b> Les Crocs du Monde</b><br /> Région d'Izandar<br /> <br /> Il
	n'y pas de point plus haut sur l'Ile que les Crocs du Monde. Longtemps,
	cette immense chaîne de montagne qui bloque l'horizon et disparait dans
	les nuages a formé le bout du monde, ses hauteurs servant de demeure
	aux dragons royaux et autres griffons. Toutefois, le percement des
	Portes du Nord, il y a bien longtemps, à rendu possible de traverser
	ces montagnes, pour rejoindre les contrées nordiques.<br /> <br /> 13)
	<b> Le Lac du Miroir aux Nuées </b>(Kha'zar'um ihm' Goiatkkûd)<br />
	Région d'Octobian<br /> <br /> C'est au bord du grand Lac aux Nuées que
	se dressent les imprenables remparts d'Octobian, la puissance cité
	troglodyte des nains. Irrigué par des centaines de ruisseaux qui
	descendent des Monts du Serpent, le lac aux eaux si pures est l'objet
	d'une véritable vénération parmi les clans nains, qui croient dur comme
	fer que certains de leurs scaldes peuvent déterminer l'avenir rien
	qu'en regardant les nuages reflété dedans. Pour cette raison, il est
	strictement défendu, et même blasphématoire par décret du Kar'Scaldoak,
	de souiller le lac en se baignant dedans. Pour cette raison encore,
	lors des sièges menés par l'Izandar ou par Artasse, les soldats avaient
	ordre de tout jeter dans le lac : urines, excréments, prisonniers...
	Cependant, de nombreux petits villages, peuplés d'humains comme de
	nains. C'est là également que poussent les grands champs de houblon et
	d'orge, chargé d'apporter la matière première pour la bière, dont les
	peuples de Nacridan font une grande consommation. La bière sacrée des
	nains est d'ailleurs confectionnée avec l'eau du Kha"goiatkkûd, et nul
	étranger n'est autorisé à en goûter. Il parait qu'elle est vraiment
	très bonne.<br /> <br /> 14) <b>Les Monts de la Queue du Serpent </b></br>
	Arrière-pays d'Octobian.<br /> <br /> Chaîne de collines de pierres
	sèches et de montagnes de faible importance qui sinuent des hauteurs
	des Crocs (la tête du "serpent") jusqu'aux abords du Lac aux Nuées, la
	région est assez isolée : aucune route n'y mène réellement, et elle peu
	habitée. Des monstres nombreux y ont leur demeures, et la vie y est
	plus rude en hiver qu'en de nombreuses régions de Nacridan. Seuls
	quelques très rares villages, souvent les descendants de colons humains
	venus de Djinn, ont tenté et tente encore de mettre en valeur une
	région où la terre est peu arable. La rudesse du territoire a toutefois
	produit de grands aventuriers au fil des ans. La région est cependant
	dominée par de quelques grandes implantations naines, du fait de la
	présence de nombreux filons de minerais précieux, en particulier d'or
	et d'argent. On y trouve également la grande Mine naine de Gemmeciel
	(le Kûhnzgoia' Zvillaî), d'où les plus belles pierres précieuses
	terrestres sont produites. La richesse du sous-sol et la relative
	facilité que pouvaient avoir une armée pour descendre des Crocs du
	Monde jusqu'à la Queue du Serpent a été la cause de nombreuses
	batailles et guerres entre Octobian qui entendait garder la jouissance
	de ce domaine pour elle, et les autres cités, tour à tour Artasse,
	Djinn, puis Izandar...



</p>
<br />
<br />
<br />
<br />
