<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for ($i = 20; $i < 40; $i ++) {
    $name[$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i .
         "\");return false;' > ";
}

?>

<div class='subtitle'>Devenir gouveneur d'un village ou d'un bourg
	indépendant</div>
<p class='rule_pg'>
	Tous les villages et bourgs de Nacridan qui ne sont pas dans les
	royaumes de départ sont considérés comme indépendants. Vous pouvez donc
	en prendre le contrôle et en devenir le gouverneur. <br /> <br /> Pour
	devenir gouverneur, vous devez vous rendre dans le temple du village.
	Dans la salle de l'hôpital, si le village est indépendant, vous avez la
	possibilité d'ordonner la construction d'un palais du gouverneur pour
	2000 PO et 10 PA.<br /> Il vous faut alors choisir un emplacement pour
	le palais et un nom pour votre village. Ce nom apparaitra alors sur la
	carte comme un village contrôlé ainsi que dans la description des
	bâtiments dans la vue (choisissez donc un nom concordant au RP de
	Nacridan). La construction du palais durera entre 10 et 15 jours. <br />
	<br /> <b>Le nom du gouverneur d'un village contrôlé peut être trouvé
		dans la page des statistiques de l'écran d'accueil section : <a
		href=http://www.nacridan.com/main/stats.php> 'Les Villages Contrôlés'</a>
	</b> <br /> <br />Dès la construction du palais, vous êtes d'ores et
	déjà gouverneur du village, même si vous ne pouvez pas encore le gérer.
	Un prêtre niveau 6*niveau_temple (PNJ) est apparu dans l'hôpital du
	temple : celui-ci est en charge des résurrections. S'il est tué, aucun
	retour dans ce temple après une mort n'est possible. De plus, tous les
	personnages ayant fixé le lieu de resurrection dans ce temple seront
	alors avertis par un message et leur lieu de resurrection est déplacé
	au temple accessible le plus proche. <br /> <br /> Lorsque vous devenez
	le gouverneur d'un village, la loyauté des habitants est au début de
	50% : elle augmentera d'1% chaque semaine. Cette loyauté intervient
	lors d'une éventuelle bataille pour la prise du pouvoir. Notez qu'une
	telle bataille pour être source de destruction. En effet, contrairement
	aux villages ou bourgs indépendants, les bâtiments de ceux sous le
	contrôle d'un joueur peuvent être attaqués et éventuellement détruits.
	Vous trouverez plus d'informations sur ce sujet à la fin de l'onglet <a
		href='rules.php?page=step12'>Villages</a>, dans le pragraphe
	'Destruction des bâtiments'.
</p>
<br />


<div class='subtitle'>Prendre un village contrôlé</div>
<p class='rule_pg'>
	Il est possible de renverser un gouverneur déjà en place pour s'emparer
	de son village. Il vous faudra pour cela franchir les remparts et
	atteindre la salle du gouverneur du palais. Dans cette salle, vous
	pourrez alors baisser la loyauté des villageois envers leur gouverneur
	actuel. Lorsque la loyauté des villageois envers le gouverneur actuel
	atteint 0%, il est possible de <b>lancer un putsch</b>. Soutenez enfin
	l'avancement de ce putsch pour permettre à son meneur de devenir le
	nouveau gouverneur du village. Vous trouverez plus de détails
	concernant ces actions dans les paragraphes suivants, à propos de la
	gestion d'un village.<br /> <br /> Il existe un autre moyen pour
	évincer un opposant, plus radical toutefois : <b>détruire son palais</b>
	! Le gouverneur qui perd ce bâtiment perd également le contrôle du
	village ou du bourg qu'il avait conquis. De plus, notez bien que <b>les
		remparts tomberont également</b>. Le village ou bourg redevient alors
	indépendant, jusqu'à la construction d'un nouveau palais.<br /> <br />
<div class='subtitle'>Téléportation non autorisée dans un village</div>
<p class='rule_pg'>
	Un personnage qui n'est pas autorisé à se téléporter dans un village
	via le temple (voir 'Contrôler l'accès au temple' ci-après) risque de <b>déclencher
		une alarme</b> s'il se téléporte dans l'enceinte du village grâce aux
	sortilèges Téléportation ou Rappel. <br /> <br /> A chaque
	téléportation "sauvage", il y a ainsi 50% de chances pour qu'un message
	d'avertissement soit envoyé aux gestionnaires du village.<br /> <br />
<div class='subtitle'>Mort du prêtre du village</div>
<p class='rule_pg'>
	Si le prêtre d'un village est tué, les actions du temple deviennent
	inaccessibles.<br /> <br /> De plus, tuer le prêtre permet d'avoir
	accès aux possessions contenues dans toutes les maisons du village.<br />
	<br />
<div class='subtitle'>Gestion du village</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Palais.png'> Le palais
	du gouverneur est un bâtiment solide. Il possède 6000 points de
	structure. Améliorer son niveau permet d'augmenter la vitesse de
	construction, de réparation et de démolition des bâtiments. Il contient
	trois salles spécifiques. <br /> <br /> <br /> <br /> <b>La Salle de
		Trésorerie </b><br /> Les options disponibles dans la salle de
	Trésorerie ne sont accessibles que par le gouverneur et éventuellement
	par les personnages autorisés par le chef de l'ordre du gouverneur. <br />
<ul>
	<li>Gérer les taxes<br /> Les prix des services proposés par les
		bâtiments de votre ville sont composés d'un coût réel et de taxes
		perçues par la ville. À chaque fois qu'un personnage utilise les
		services d'un bâtiment de votre ville, une partie de l'argent payé est
		versée dans la caisse du bâtiment. Depuis la salle de trésorerie, vous
		pouvez pour chacun des services des différents bâtiments, fixer les
		taxes perçues par la cité. Évidemment, augmenter les taxes sur un
		service en augmente autant le prix pour le consommateur.
	</li>
	<li>Gérer la caisse centrale <br /> La salle de trésorerie contient
		également une caisse centrale dans laquelle vous pouvez rapatrier les
		bénéfices conservés dans les caisses de chacun des bâtiments. Vous
		pouvez également depuis ce lieu retirer et déposer librement de
		l'argent dans la caisse centrale.
	</li>
	<li>Alimenter la caisse d'un bâtiment de la cité<br /> La salle de
		trésorerie permet de transférer de l'argent de la caisse centrale vers
		un des bâtiment du village. Cette option n'est utile que pour
		alimenter la caisse de l'entrepôt.
	</li>
</ul>


<p class='rule_pg'>
	<b>Salle du gouverneur</b><br> Cette salle est gardée par un PNJ "Garde
	du palais" de niveau 5 qui apparait à l'achèvement du palais. C'est
	dans cette salle que le gouverneur peut prendre les grandes décisions
	pour son village.<br> <br /> <br /> Les actions réservées au gouverneur
	et aux personnes autorisées.
<ul>
	<li>Contrôler l'accès au Temple : cette option vous permet de définir
		la liste des aventuriers qui peuvent entrer dans votre village par la
		porte une fois les portes fermées. <br />La liste des options
		possibles:<br />
		<ul>
			<li>Ouvrir toutes les portes.</li>
			<li>Fermer toutes les portes.</li>
			<li>Ouvrir à ma liste d'allié uniquement.</li>
			<li>Fermer pour mes ennemis.</li>
		</ul>
	</li>
	<li>Gérer la resurrection au Temple : cette option vous permet de voir
		la liste des aventuriers ayant choisi le temple de votre village comme
		lieu de résurrection. Vous pouvez, si vous le souhaitez, les bannir de
		votre temple. Leur lieu de résurrection est alors automatiquement
		déplacé au temple accessible le plus proche.</li>
	<li>Contrôler l'accès au Temple : cette option vous permet de définir
		les aventuriers qui peuvent utiliser le temple pour se téléporter dans
		votre village. <br />La liste des options possibles:<br />
		<ul>
			<li>Temple accessible pour tout le monde.</li>
			<li>Temple inaccessible.</li>
			<li>Temple ouvert à liste d'allié uniquement.</li>
			<li>Temple fermée pour mes ennemis.</li>
		</ul>
	</li>
	<li>Engager un garde royal (10PA) : cette option apparait si l'ancien
		garde a été tué. Elle n'est possible que si la loyauté du village est
		supérieure à 50%. Elle demande 10 PA et coûte 100PO. L'option n'est
		disponible qu'une semaine après que le garde royal ait été tué. Le
		niveau de garde sera de 5 fois le niveau du palais.</li>
	<li>Organiser une cérémonie d'offrande (100PO) pour résussiter le
		prêtre du temple : l'option n'est disponible qu'une semaine après que
		le prêtre ait été tué. Le niveau du prêtre sera de 6 fois le niveau du
		temple.</li>
</ul>

Les alertes en cas d'invasion : un message est envoyé au gouverneur et
personnes autorisées quand :
<ul>
	<li>le prêtre se fait attaquer</li>
	<li>le garde se fait attaquer</li>
	<li>au début d'un putsch</li> Remarque : les alertes sont désactivées
	si le livre de compte du village est vide sur les 15 derniers jours.
</ul>

Les actions accessibles à tous.
<ul>
	<li>Oeuvrer pour baisser la loyauté du village envers le gouverneur
		actuel (10 PA). Cette action représente toute une gamme de RP
		possibles dont le résultat est une baisse de 10% de la loyauté des
		villageois envers le gouverneur.</li>
	<li>Oeuvrer pour augmenter la loyauté du village envers le gouverneur
		actuel (10 PA). Cette action permet d'augmenter la loyauté jusqu'à un
		maximum de 50%. Seul la durée et la stabilité du pouvoir en place
		permet d'atteindre 100%.</li>
	<li>Lancer un putsch (10 PA). Cette action n'est accessible que si la
		loyauté du village est à 0. Lorsque vous effectuez cette action vous
		devez choisir le chef du putsch au profit duquel vous menez votre coup
		d'état, c'est à dire la personne qui deviendra gouverneur dans le cas
		où le putsch est mené à terme (100%). L'avancement du putsch est alors
		de 10%.</li>
	<li>Soutenir ou lutter contre un coup d'état (10 PA) permet d'augmenter
		de baisser de 10% l'avancement du coup. Lorsqu'un putsch atteint 100%,
		le meneur de celui-ci devient alors gouverneur du village.</li>


</ul>


<p class='rule_pg'>
	<b>La Salle de l'Architecte </b><br /> Toutes les actions payantes
	disponibles de la salle de l'architecte utilise l'argent de la caisse
	centrale de la cité. Comme pour la salle de Trésorerie, ces actions ne
	sont accessibles que par le gouverneur et les personnages de l'ordre du
	gouverneur autorisés par le chef de l'ordre. <br />
<ul>
	<li> Construire un nouveau bâtiment : vous permet de construire tous les bâtiments disponibles dans les grandes cités (écoles, guilde des artisans, maison, etc...).
 Vous ne pouvez cependant ni construire un nouveau temple ni un deuxième palais. De plus vous devez laisser au moins 3 cases libres dans votre village. Retrouver tous les détails des prix des bâtiments <?php echo $name[21];?>ici</a>.
	</li>
	<li>Réparer un bâtiment endommagé / Détruire un bâtiment endommagé.</li>
	Remarque : la destruction d'un bâtiment n'est possible que si la
	loyauté est supérieure ou égale à 70%. Le coût de la destruction est de
	50 PO pour 500 PS du bâtiment.
	</br>
	<li>Améliorer un bâtiment. Il y a deux types d'amélioration possible.
		Vous pouvez décider d'améliorer la solidité d'un bâtiment : cette
		action coûte en PO 1/10 des points de structure courants et fait
		gagner 1000 points de structure au bâtiment. Vous pouvez aussi décider
		d'améliorer le niveau du bâtiment, améliorant ainsi les services qu'il
		propose. Le prix dépend alors du type de bâtiment et de son niveau
		actuel (voir le lien ci-dessus).</li>
	<li>Renommer le village / Changer sa description. Renommer le village
		coûte 100po et utilise 10pa. Changer la description vous permet
		d'utiliser une image pour illustrer votre cité et de l'accompagner
		d'un texte descriptif, tous deux accessibles en cliquant sur le nom de
		la ville à la suite de la description des bâtiments dans la vue.</li>
	<li>Gestion des maisons : ici, vous pouvez mettre en vente une maison
		de votre cité, elle sera alors marquée en vente dans la vue et pourra
		être achetée par un personnage. L'argent de la vente sera
		automatiquement reversé dans la caisse centrale de la cité.</li>
	<li>Fortifier le village / Améliorer les fortifications : construire
		des remparts (de 2000 PS pour 2 PA et 1200 PO) vous permet de
		sécuriser votre village. <b><u>Attention:</u></b> pour fortifier le
		village, il faudra avoir au préalable terrassé le village (voir
		ci-dessous les Opérations de terrassement). Il vous est alors possible
		d'en filtrer l'accès (par la route et / ou par le temple) depuis la
		salle du gouverneur. Améliorer les remparts coûte 2 PA et les fortifie
		chaque fois de 2000 points de structure supplémentaires. Le coût de
		l'amélioration est de 1,5 fois la précédente : soit 1800po au n2,
		2700po au n3, etc. Notez que vous ne pouvez pas améliorer les
		fortifications tant que tous les remparts ne sont pas finis ou que
		l'un d'eux est endommagé.
	</li>
	<li>Opérations de terrassement : vous permet de terrasser les
		fortifications pour y mettre soit un terrain de terre battue soit un
		espace pavé. Les deux apportent le même avantage, à savoir un coût de
		déplacement de 0.5 PA de base. Cependant l'espace pavé est 2 fois plus
		cher et donc réservé aux villages les plus riches. Un terrassement en
		terre battue peut être amélioré en espace pavé plus tard.</li>
	<li>Opérations de terraformation : vous permet de choisir le terrain
		que vous voulez au delà des remparts jusqu'à 2 cases. Le coût dépend
		du terrain d'origine et du terrain souhaité. Cela permet de choisir
		l'environnement de son village, en bordure de fôret, entouré de
		montagne pour un village surplombant une plaine, etc...</li>
</ul>

<b>Quelques exemples de terrassement et de terraformation</b>
<br />
<br />
Voici le village au départ.
<br />
<br />
<img style="margin-right: 5px" src='exemple_terraformation_1.png'>
</br>
</br>
Le terrassement des remparts est fait en espace pavés.
<br />
<br />
<img style="margin-right: 5px" src='exemple_terraformation_2.png'>
<br /></br>
Plusieurs terraformations sont faites dans le but de rajouter de la
forêt et de la montagne.
<br />
<br />
<img style="margin-right: 5px" src='exemple_terraformation_3.png'>
</p>
<br />
<br />

<!--
Lorsque vous d&eacute;cidez de devenir le protecteur d'un village, il faut que vous soyez sur la case du village choisi (et non dans le village). Dans "<B>Action</b>, choisissez 
-->
