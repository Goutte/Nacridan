<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

for($i = 1; $i < 20; $i ++) {
	$name [$i] = "<a href=\"formulas.php?formula_id=" . $i . "\" onclick='javascript:detailedRules(\"formulas.php?formula_id=" . $i . "\");return false;' > ";
}
// class='stylepc'

?>
<div class='subtitle'>Introduction</div>

<p class='rule_pg'>
	Dans les villages, bourgs et cités de l'île, vous trouverez divers
	bâtiments. Vous pouvez rentrer dans ces bâtiments lorsque vous êtes à
	une case de distance, en cliquant sur le bâtiment dans la vue 2D, un
	bouton "Entrer" apparaît. Vous arrivez alors dans la salle d'accueil du
	bâtiment. Pour ressortir il suffit de cliquer sur une case libre autour
	du bâtiment lorsque vous êtes dans l'accueil.<br> <br> Selon le type de
	bâtiment, vous aurez accès à différentes salles qui vous offriront
	leurs services. Deux salles sont communes à tous les bâtiments:
	l'accueil et les écuries. L'accueil est la salle dans laquelle vous
	arrivez quand vous entrez dans le bâtiment. C'est également dans cette
	salle que vous devez aller pour pouvoir ressortir. Les écuries ne sont
	actuellement utiles que dans le comptoir commercial, pour les
	caravanes. Les services présents dans les bâtiments sont décrits
	ci-dessous. <br /> <br /> Les prix indiqués des services décrits
	ci-dessous sont valables pour les villages, bourgs et cités au début de
	l'histoire de l'île de Nacridan. Quand un village passe sous le
	contrôle d'un joueur (voir la section <a href='rules.php?page=step13'>Forteresses</a>),
	il en devient le gouverneur et peut alors fixer ses propres prix.<br> <br>
	
	Remarque : la liste des villages contrôlés et de leur gouverneur est disponible en consultant les <a href=http://www.nacridan.com/main/stats.php>statistiques de la page d'accueil</a>.
</p>
<br>
<div class='subtitle'>Temple</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Temple.png'> Le Temple
	est présent dans chaque village, bourg et cité. Il est de niveau 2 dans
	toutes les villes, bourgs et villages des royaumes de départ. Il
	contient trois salles spécifiques, l'hôpital, le temple de
	téléportation et la chapelle. Il est indestructible, mais le prêtre du temple peut être tué.<br /> <br /><br /> 	
	Si le prêtre d'un village et tué, les actions du temple deviennent inaccessibles.<br />  
	De plus, tuer le prêtre permet d'avoir accès aux possessions contenues dans toutes les maisons du village.
</p>
<p class='rule_pg'>
	<br /> <b>Hôpital</b>
<ul>
	<li>Soin : 2 PA, coût en PO: le minimum entre votre niveau et 5. Le
		prêtre réalise un sort de Larme de Vie de niveau égal à six fois le
		niveau du temple.</li>
	<li>Effacer les malédictions : 2 PA, 12 PO. Le prêtre réalise un sort
		de Souffle d'Athlan de niveau égal à six fois le niveau du temple.</li>
	<li>Choisir le temple comme lieu de prochaine résurrection : 20 PO, le
		prêtre fixe votre âme à ce temple, vous y reviendrez lors de votre
		prochaine mort.</li>
</ul>
</p>

<p class='rule_pg'>
	<b>Temple de Téléportation</b><br /> La salle de la téléportation vous
	permet de vous téléporter vers un autre temple du réseau pour
	5+distance/2 PO et 12 PA. La portée du temple de téléportation est
	donné par 50+30*Niveau_Temple. Vous ne pouvez donc vous téléporter que
	dans les villages dont les temples sont situés dans ce rayon.<br />


</p>

<p class='rule_pg'>
	<b>La chapelle</b><br /> La chapelle est un lieu de refuge. Aucune
	action n'y est autorisée. Par conséquent, vous y êtes protégé de tout
	danger. C'est aussi la salle dans laquelle vous réapparaissez si vous
	êtes tué. Entrer dans la chapelle inflige à votre personnage un malus
	de 40% sur ses caractéristiques de base jusqu'à deux tours après que
	vous l'ayez quitté. C'est donc le malus que vous subirez après une
	mort.
</p>



<div class='subtitle'>Échoppe</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Echoppe.png'>
	L'échoppe est présente dans tous les villages, bourgs et cités. Elle
	est de niveau 5 dans les grandes villes, de niveau 3 dans les bourgs et
	de niveau 2 dans les villages. Elle comporte quatre salles spécifiques
	:
</p>
<br />
<br />

<p class='rule_pg'>
	<b>Boutique</b><br /> La boutique principale offre un peu de tout en
	quantité limitée armes, armures, potions, outils, matières premières...
	Toutefois, les échoppes adoptent toujours les spécialités de leur
	ville, c'est pourquoi vous pourrez trouver selon les échoppes beaucoup
	d'objet en cuir ou encore pour d'objet magique etc... Le prix de objets
	est le même quel que soit les échoppes si le village n'est pas contrôlé
	par un joueur et leur niveau est égal au niveau du l'échoppe ou moins
	1. Le nombre d'objet par catégorie est donné par 2+2*Niveau_Echoppe. Le
	stock est réajusté toutes les semaines. Lorsque vous vendez un objet,
	il peut être mis en vente dans la boutique s'il correspond aux critères
	de vente de l'échoppe.<br /> Le niveau maximal des flèches, potions et
	matières premières est de 3 et ce pour toutes les échoppes. <br /> <br />
	<b>Armurerie standard</b><br /> L'armurerie standard propose des armes,
	armures, potions et outils de niveau 1, en quantité illimitée. Si vous
	cherchez du niveau 1, vous êtes sûr de trouver votre bonheur. <br /> <br />
	<b>Salle des ventes.</b><br /> La salle des ventes permet de vendre ses
	objets, à condition qu'ils ne soient pas équipés. Toutes les échoppes
	rachètent les objets à 50% de leur prix usuel (l'état d'usure rentre en
	ligne de compte). Cependant, elles ne sont pas intéressées par les
	objets d'un niveau trop élevé, c'est pourquoi elles appliqueront un
	pourcentage de réduction de 4% par niveau supérieur au niveau 3 sur le
	prix de rachat de l'objet. De plus, leur production étant facilité par
	le fait qu'ils n'utilisent pas de matière première, contrairement aux
	autres équipements le prix de vente des outils en échoppe n'est que de
	2 POs par niveau. <br /> <br /> <b>Faire réparer ses objets.</b><br />
	Le forgeron peut réparer tout type d'objet contre une légère
	rémunération. Réparer un objet totalement cassé vous coûtera 1/3 de son
	prix d'achat.
</p>

<div class='subtitle'>Entrepôt</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Entrepot.PNG'>
	L'entrepôt n'est présent dans aucun des villages, bourgs et cité. Il ne
	peut être construit que dans un village ou un bourg contrôlé. Ce
	bâtiment permet de stocker des objets. Il comporte deux salles
	spécifiques :
</p>
<br />
<br />

<p class='rule_pg'>
	<b>Dépôt/Vente</b><br /> Cette salle permet de déposer ou de prendre
	des objets. Le prix d'achat ou de vente par l'echoppe est définit par
	le gouverneur du village comme un pourcentage du prix de vente en
	boutique à l'exception des outils qui peuvent être fabriqués par les
	artisans. Le bâtiment est prévu de fonctionner avec les valeurs
	suivantes: 65% pour l'achat et 75% pour la revente. Ainsi l'entrepôt
	rachète plus cher qu'en boutique pour inciter les habitants du village
	à partager les objets qui ne sont plus utiles et les mettre à
	disposition de la communauté. Un prix de vente plus élevé permet de
	dégager un bénéfice pour financer l'achat d'autres objets. Les objets
	qui sont déposé doivent être neuf, c'est pour ça que ce bâtiment
	héberge aussi une salle de réparation. Il n'est pas possible de stocker
	plus de 20 * (niveau du bâtiment) objets. Ce bâtiment ne peut acheter
	de matériel si sa caisse est vide. A charge au gouverneur de mettre de
	l'argent dans la caisse depuis le palais.<br /> Attention ce bâtiment
	est accessible dès que quelqu'un est entré dans le village, donc mettre
	un prix de vente à 0 s'expose à un pillage très aisé. <br /> <br /> <b>Faire
		réparer ses objets.</b><br /> Le forgeron peut réparer tout type
	d'objet contre une légère rémunération. Réparer un objet totalement
	cassé vous coûtera 1/3 de son prix d'achat.
</p>


<div class='subtitle'>Comptoir Commercial</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Comptoir.png'> Le
	comptoir commercial est présent dans les villages, les bourgs et les
	cités. Il est de niveau 4 dans les cités, de niveau 2 dans les bourgs
	et villages des 3 royaumes de départ et de niveau 1 dans tous les
	autres villages ou bourgs. Il contient 3 salles spécifiques : </br/> </br/>
	<br /> <b>Caravansérail</b><br /> Dans le caravansérail vous pouvez
	organiser une mission commerciale dans le but de gagner de l'argent.
	Les missions commerciales consistent à achalander une grande quantité
	de marchandise d'un comptoir à un autre à l'aide du tortue géante. <small><?php echo $name[10];?> Détails </a></small>

	</br/> </br/> <b>Transport d'objets</b><br /> Cette salle vous permet
	d'envoyer, à l'attention d'un autre personnage, jusqu'à 4 objets vers
	un autre comptoir commercial à portée pour la somme de 5 pièces d'or. <small><?php echo $name[11];?> Détails </a></small>

	</br/> </br/> <b>Banque</b><br /> Dans la banque vous pouvez déposer,
	retirer ou transférer de l'argent vers une cité ou sur le compte d'un
	autre personnage. Les frais de transfert sont de (12 - Niveau du
	comptoir)% tandis que lors d'un dépôt seul (89 + Niveau du comptoir)%
	de la somme versée arrivera sur votre compte (<b>attention</b> : dans
	un village contrôlé, les prélèvements d'une banque peuvent augmenter
	selon les règles de marge définies par son gouverneur). L'argent sur
	votre compte bancaire vous rapporte 1% par mois. <br />
<div class='subtitle'>Auberge</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Auberge2.png'>

	L'auberge est présente dans les villages, bourgs et cités. Il y a deux
	auberges de niveau 6 dans chacune des grandes cités. Les auberges des
	villages et bourgs des 3 royaumes de départ sont de niveau 2. Partout
	ailleurs elles sont de niveau 1. L'auberge contient deux salles
	spécifiques : <br /> <br /> <br /> <b>Grand-Salle</b><br /> Dans la
	grande salle vous pouvez :
<ul>
	<li>Boire un coup : 1 PO, 1 PA. Bonus ou malus de 1D dans une
		caractéristique de base pour le tour en cours.</li>
	<li>Dormir en chambre : 5 PO 12 PA. Redonne la totalité de vos points
		de vie.</li>
	<li>Discuter avec les clients : 3 PA. Permet avec de la chance
		d'obtenir une mission d'escorte <small><?php echo $name[1];?> Détails </a></small>
	</li>
	<li>Envoyer un message commercial : 10 PO et 2 PA. Permet d'envoyer un
		message à tous les personnages dans le rayon d'action de l'auberge.<small><?php echo $name[12];?> Détails </a></small>
	</li>
</ul>

<p class='rule_pg'>
	<b>Salle des Annonces</b>
<ul>
	<li>Liste de ventes : Vous pouvez chercher un objet parmi toutes les
		listes de vente de tous les PJ du jeu. La recherche propose divers
		critères : prix, niveau, type, enchanté ou non...</li>
	<li>Autres annonces : Vous pouvez consulter les annonces postées par
		les PJ et en poster vous même pour 5 PO et 3 PA. Vous êtes limité à
		une seule annonce qui sera affichée dans toutes les auberges dans un
		rayon de 50*(niveau de l'auberge), ce qui fait du 300 cases pour les
		auberges des cités.</li>
</ul>
</p>


<div class='subtitle'>Guilde des Artisans</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Guilde.png'> On peut
	trouver une Guilde des Artisans dans les bourgs et dans les cités.
	Elles sont de niveau 4 dans les cités, de niveau 2 dans les bourgs des
	royaumes et de niveau 1 dans les autres bourgs. Elles contiennent 2
	salles spécifiques.<br /> <br /> <br /> <b>Atelier </b><br /> Dans
	l'atelier, vous pouvez commander auprès des artisans la fabrication de
	n'importe quel équipement d'un niveau donné. Le prix est alors majoré
	de 20% par rapport au prix d'échoppe. Le niveau de l'équipement
	commandé ne peut pas dépasser le niveau du batiment et la durée de
	fabrication de l'objet sera de (Niveau_Equipement+1) jours. Les
	artisans ne peuvent pas traiter plus de 2*Niveau_Guilde commandes à la
	fois.<br /> <br />Dans cette salle, vous pouvez également utiliser les
	facilités de la Guilde. En payant 5PO (automatique si vous vous servez
	d'un de vos savoir-faire dans cette pièce), vous pouvez utiliser les
	outils mis à disposition par la guilde. Pour les savoir-faire de
	raffinage, cela revient à utiliser un outil de niveau égale au niveau
	de la guilde. Pour les savoir-faire d'artisanat, les outils de la
	guilde vous octroie un bonus de 7*Niveau_Guilde sur vos jets de
	maitrise d'artisanat. Vous pouvez donc cumuler ce bonus avec vos
	propres outils qui vous procurent des bonus dans la vitesse de
	confection.<br /> <br /> <b>Salle des enchantements</b><br /> Dans la
	salle des enchantements vous pouvez commander le placement d'un
	enchantement mineur sur un équipement de votre inventaire. Tout comme
	pour l'atelier, le prix sera majoré de 20% par rapport au prix du
	marché. Le niveau maximal de l'enchantement commandé ne peut pas
	dépasser (Niveau_Guilde)/2 arrondi à l'entier inférieur. La durée de
	l'enchantement sera de 2*Niveau_Enchantement. La Guilde ne peut gérer
	que Niveau_Guilde commande d'enchantement à la fois.<br /> Il est
	possible de demander à l'enchanteur de réaliser une Rune d'Équilibre
	sur tout personnage de niveau 5 et moins. Pour le prix de 30 PO, le
	personnage voit l'une de ses jauges retomber à zéro, ceci afin d'aider
	les jeunes personnages à rattraper leurs éventuelles erreurs.
</p>

<div class='subtitle'>Maison</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Maison.png'> Chaque
	village, bourg et cité contient plusieurs maisons.<br /> Elles
	comportent une seule salle spécifique.<br /> <br /> <br /> <br /> <b>Chambre</b><br />La
	chambre n'est accessible que si vous êtes le propriétaire de la maison.
	Vous pouvez y dormir pour 12PA et regagner la totalité de vos points de
	vie. Vous pouvez aussi y entreposer 10 objets en sécurité. Enfin, cette
	pièce donne accès à la caisse où vous pouvez gratuitement déposer et
	retirer vos pièces d'or (pas de taxes, mais pas d'intérêt non plus).
</p>

<div class='subtitle'>Ecoles</div>
<p class='rule_pg'>
	<img style="float: left; margin-right: 5px" src='Ecole.png'> On trouve
	trois types d'école sur Nacridan : reportez-vous aux sections adéquates
	pour plus de détails.<br /> <br /> Les <a href='rules.php?page=step8'>Ecoles
		de Magie</a> et les <a href='rules.php?page=step7'>Ecoles de Combat</a>
	ne sont disponibles que dans les grandes cités.<br /> <br /> Les <a
		href='rules.php?page=step11'>Ecoles de Métiers</a> sont disponibles
	dans les bourgs et les grandes cités. Celles des grandes cités sont de
	niveau 6 et enseignent tous les métiers connues dans Nacridan. Celles
	des bourgs sont de niveau 1 et seul 5 apprentissages aléatoires y sont
	dispensés.
</p>

<div class='subtitle'>Destruction de bâtiments</div>
<p class='rule_pg'>
	Les bâtiments des villages ou bourgs indépendants ne peuvent pas subir
	de dégâts. Par contre, une fois que l'un de ceux-la est passé sous le
	contrôle d'un joueur (voir la section <a href='rules.php?page=step13'>Forteresses</a>),
	il existe 3 moyens pour détruire un bâtiment.<br />
<ul>
	<li>L'attaquer au moyen de l'action <a href='rules.php?page=step4'>Attaquer
			un bâtiment</a>.
	</li>
	<li>Utiliser le sort <a href='formulas.php?formula_id=220'
		onclick='javascript:detailedRules("formulas.php?formula_id=220");return false;'>
			Piliers Infernaux</a>.
	</li>
	<li>Utiliser la compétence <a href='formulas.php?formula_id=122'
		onclick='javascript:detailedRules("formulas.php?formula_id=122");return false;'>Flêche
			enflammée.</a></li>
</ul>
Si des personnages se trouvent dans le bâtiment au moment de sa
destruction, ils encaissent une perte de vie immédiate égale à 1/10 des
PS maximum du bâtiment. Ces dégâts peuvent éventuellement les tuer.
<br />
<br />

Tout bâtiment détruit laisse le contenu de sa caisse accessible (action
'Ramasser un objet'), ainsi que tous les objets qui jonchaient déjà le
sol dans chacune des pièces. De plus, il y a quelques cas particuliers.
Les équipements stockés dans un entrepôt ou dans une maison tombent
également au sol. L'échoppe laisse en plus 3 objets supplémentaires,
choisis au hasard parmis ceux qui étaient en vente. Enfin, le comptoir
commercial laisse tomber les colis qui étaient en attente de
récupération.
<br />
<br />

<b> Attention</b>
, parmis les divers effets d'une destruction de bâtiment, celle du
palais d'un village ou bourg contrôlé par un joueur est la plus
importante ! Le gouverneur qui perd son palais perd également les
remparts (qui s'écroulent simultanément) et le contrôle du lieu, qui
redevient alors indépendant jusqu'à la construction d'un nouveau palais.
</p>
<br />
<div class='subtitle'>Construction de routes par évolution des terrains</div>
<p class='rule_pg'>
	A force de passer et repasser au même endroit, le terrain s'use et des
	routes apparraissent. Ainsi un terrain forêt se transformera en terrain
	plaine puis en terrain terre battue. Le terrain plaine et le terrain
	montagne peuvent se transformer en terre battue. <br />Le compteur des
	terrains est à 0 à l'origine. Un personnage quand il arrive dessus en
	déplacement, téléportation, projection,... augmente le compteur de 1,
	le feu fol de 1, la tortue de 3 et les monstres ne changent pas le
	compteur. La valeur maximale du compteur est de 60.<br /> Chaque
	semaine les compteurs descendent de 2. Une route inutilisée trop
	longtemps retrouvera le terrain original.<br />
<ul>
	<li>Le terrain forêt se change en terrain plaine quand le compteur
		arrive à 30, en terrain terre battue quand le compteur arrive à 45.</li>
	<li>Le terrain plaine se change en terrain terre battue quand le
		compteur arrive à 17.</li>
	<li>Le terrain montagne se change en terrain terre battue quand le
		compteur arrive à 40.</li>
</ul>
</p>
<br />
<br />
