<p class='rule_pg'>
	Votre personnage va évoluer par les actions que vous allez lui faire
	faire dans le jeu. Disponible dans le menu de gauche, chacune d'entre
	elles requiert un nombre de Points d'Action qui lui est propre. Il
	existe quatre modes dans le jeu : Conquête, Economie, Diplomatie et
	Royaume. Le mode Conquête est le mode que vous utiliserez le plus.
	C'est ici que vous voyez ce que votre personnage voit. Dans ce mode là,
	il existe des actions disponibles dès le début, d'autres que vous
	pourrez apprendre au fil de votre aventure.<br />
</p>

<div class='subtitle'>Le mode Conquête</div>
<p class='rule_pg'>
	<b>Actions de base</b>
<ul>
	<li><b>Se déplacer</b> dans l'île de Nacridan n'utilise pas toujours le
		même nombre de PA. Ce nombre varie en fonction du terrain sur lequel
		vous vous trouvez et des éventuels bonus ou malus de déplacement de
		votre personnage.<br /> Les coûts de base des déplacements sont ainsi
		définis :<br /> - Sur la route, la terre battue et les pavés : 0,5 PA<br />
		- En plaine : 1 PA <br /> - En forêt ou dans le désert: 1,5PA<br /> -
		En montagne ou dans les marais : 2PA.<br /> - Dans les remparts d'un
		village : variable (voir ci-dessous).<br /> On dit qu'un village est <a
		href='rules.php?page=step13'>fortifié</a> lorsqu'il dispose de
		remparts tout autour. Le coût de déplacement correspond alors à celui
		du terrain sur lequel la fortification est bâtie. Toutefois, lorsque
		la ville a les portes fermées, un coût supplémentaire de 2,5PA est
		appliqué pour <b>entrer ou sortir</b> de ses remparts (attention, les
		portes de la ville n'étant pas considérées comme faisant partie des
		remparts, passer d'un rempart à une porte implique le coût
		supplémentaire de 2,5PA).<br /> Il n'y a aucun surcoût en PA dans les
		cas suivant :<br /> - lorsque la ville est ouverte,<br /> - lorsque
		vous passez par l'une des portes,<br /> - Lorsque vous passez d'un
		rempart à un autre.<br /> <br />
	
	<li><b>Bousculer</b> un PNJ permet d'intervertir vos positions.<br />
		La réussite de cette action est soumise à un jet d'opposition <b>(JFor
			+ JDex/2)</b>. Pour réussir, vous devez obtenir un résultat
		strictement supérieur à celui de votre opposant. Chaque tentative vous
		coûtera 0,5PA + le coût du déplacement en cas de réussite : par
		exemple, en plaine la dépense sera de 1,5PA si réussite et 0,5pa en
		cas d'échec.<br /> Afin de facilité la circulation dans les régions
		les plus peuplées, deux précisions importantes concernant la
		bousculade :<br /> - la réussite est automatique dans les royaumes de
		départ,<br /> - les monstres de niveau 1 et 2 peuvent également être
		bousculés.</li>
	<br />
	<br />

	<li><b>Attaquer</b> un monstre ou une créature de l'île permet en cas
		de réussite de lui infliger des dégâts en le frappant avec votre arme.
		Le nombre de PA nécessaire varie de 7 à 9 PA suivant le type d'arme
		que vous portez. On notera ce nombre Temps d'Attaque "<b>TA</b>" par
		la suite.<br /> <br />
	
	<li><b>Attaquer un bâtiment</b> inflige des dégâts de structure à une
		construction. Avec la <a href='rules.php?page=step7'>compétence</a>
		Flèche Enflammée et le <a href='rules.php?page=step8'>sortilège</a>
		Piliers Infernaux, ce sont les seules façons d'attaquer un bâtiment.
		Cette action vous permet d'attaquer une construction de 2 manières
		différentes.<br /> - En choisissant l'option <b>attaque normale</b>
		pour Temps d'Attaque PA : vous réalisez un jet de dégâts automatiques,
		basés sur votre caractéristique Force (0,8 * JFOR + bonus aux dégats). <br /> - En choisissant l'option
		<b>attaque magique</b> pour 8 PA : vous réalisez un sortilège aux
		dégâts automatiques, basés sur vos caractéristiques Dextérité et
		Maîtrise de la Magie (racine carrée de [JDEX*JMM] / 4 * 1,5 D6).<br /> Pour plus d'informations sur la
		destruction de bâtiments, consultez les derniers paragraphes de la
		section <a href='rules.php?page=step12'>Villages</a>.<br /> <br />
	
	<li><b>Donner des PO</b> à quelqu'un vous permet de passer une partie
		ou la totalité de vos Pièces d'Or à un personnage à côté de vous pour
		1 PA.</li>
	<br />
	<li><b>Concentration</b> permet, pour 2 PAs, de réfléchir à l'origine
		d'une erreur commise. Ainsi, lorsqu'une compétence a été manquée,
		cette action permet d'obtenir un jet d'amélioration supplémentaire
		pour améliorer la compétence. Cette action n'est disponible que durant
		la DLA dans laquelle une compétence a été manquée.</li>
	<br />
</ul>
</p>
<p class='rule_pg'>
	<b>Objets</b>
<ul>
	<li><b>Ramasser un objet</b> permet de récupérer un objet au sol situé
		sur sa case ou sur une case adjacente si celle-ci n'est pas occupée.</li>
	<br />
	<li><b>Poser un objet</b> sur la case où se situe votre personnage
		coûte 1PA.</li>
	<br />
	<li><b>Donner un objet</b> à quelqu'un au contact coute 1PA. Si vous
		donnez un sac, tous les objets qu'il contient sont donnés également.</li>
	<br />
	<li><b>Ranger</b> un objet permet de faire passer un objet d'un sac à
		un autre. Dans un bâtiment, cette action coute 0 PA sinon elle coute
		1PA</li>
	<br />
	<li><b>S'équiper</b> d'une arme, d'une pièce d'armure ou d'un sac coute
		2 PA. L'objet passe alors de votre sac d'équipement aux équipements
		portés. Si un équipement est en conflit, il est automatiquement
		déséquipé et rangé dans votre sac d'équipement sans dépense de PA
		supplémentaires.</li>
	<br />
	<li><b>Se déséquiper</b> d'une arme, d'une pièce d'armure ou d'un sac
		coute 2 PA. L'objet retourne alors dans votre sac d'équipement. Notez
		que vous ne pouvez pas déséquiper d'une pièce si votre inventaire est
		plein.</li>
	<br />
	<li><b>Utiliser une potion</b> permet d'en obtenir les bénéfices. Cette
		action coute 1 PA si la potion se trouve rangée dans votre ceinture, 2
		PA sinon.</li>
	<br />
</ul>
</p>

<br />
<div class='subtitle'>Le mode Diplomatie - Les groupes de chasse</div>
<p class='rule_pg'>

	<br /> Au cours de votre aventure, vous rencontrerez beaucoup d'autres
	personnages. Vous voudrez peut-être à un moment donné vous allier avec
	certains d'entre eux afin d'unir vos forces et combattre des monstres
	que nous n'auriez pas pu affrontez seul. <br />Les groupes de chasses
	permettront d'améliorer la gestion des interactions avec vos
	co-équipiers. <br /> <br /> <b> Créer un groupe de chasse </b><br />
	Pour créer un groupe de chasse, vous devez commencer par inviter un
	autre joueur à l'aide du bouton dédié. Le joueur invité reçoit
	automatiquement un message privé. Il peut alors accepter ou refuser
	cette invitation en se rendant dans la section "vos invitations".
	Lorsque son choix est fait, vous recevez à votre tour un message. <br />S'il
	accepte, le groupe est créée et vous en devenez le chef. Vous pouvez
	alors décider d'inviter d'autre personne, de dissoudre le groupe, de
	transférer son commandement ou d'exclure un membre. <br /> Il n'est pas
	possible de demander directement à être invité dans un groupe de
	chasse, vous devrez en faire la demande au chef du groupe par un
	message traditionnel. <br /> <br /> <b> Utilité </b><br /> Un groupe de
	chasse sert principalement à la répartition des Points d'Expérience
	gagnés lorsqu'un ennemi est tué. Vous trouverez davantage de détail
	sur la répartition des PX au sein des GDC dans <a
		href='rules.php?page=step6'>la section Expérience</a>. Il sert
	également au fonctionnement de certaines compétences ou certains
	sortilèges qui n'opèrent que pour les membres du groupe de chasse. <br />
	Lorsque vous appartenez à un Groupe de Chasse, vous pouvez voir la
	position et l'état approximatif de santé de tous les membres. Enfin la
	tribune est un outil de communication priviligié pour les membres
	d'un même groupe de chasse
</p>
<br />
<div class='subtitle'>Le mode Diplomatie - Les Ordres</div>
<p class='rule_pg'>

	Un ordre est un regroupement d'aventurier partageant les mêmes buts et
	les mêmes idéaux. Il permet de former des groupes d'aventuriers
	importants et d'établir une hiérarchie entre les différents membres. <br />
	Les ordres permettent également des filtres sur les autorisations
	d'accès et les délégations de pouvoirs au sein des villages contrôlés,
	la création d'alliance avec d'autres personnages ou d'autres ordres. <br />
	Lorsque vous appartenez à un ordre, le nom de l'ordre apparait à la vue
	de tous, à côté du nom de votre personnage et le lien renvoie sur la
	description de l'ordre et la liste de ses membres. <br /> Vous
	retrouvez donc dans ce menu les options suivantes :
<ul>
	<li><b>Créer un Ordre : </b> Vous pouvez créer un ordre seul, vous en
		devenez évidemment le fondateur. En tant que fondateur, vous pouvez
		définir 20 rangs différents et leur donner la désignation que vous
		souhaitez.<br /> Vous pouvez alors associer chaque membre de l'ordre à
		un rang. Pour chaque rang vous pouvez déléguer les pouvoirs suivants :
		accepter un nouveau membre, rejeter une nouvelle demande d'allégeance,
		bannir un membre et modifier les rangs. <br />Notez qu'un membre
		affecté au 20ème rang ne voit pas la position des autres membres de
		l'ordre contrairement à tous les membres de rang supérieur. <br />En
		tant que fondateur, vous avez aussi autorité sur la gestion des
		alliances de l'ordre, la description de l'ordre.</li>
	<br />
	<li><b>Rejoindre un Ordre : </b> Envoie une demande d'allégeance au
		fondateur de l'ordre choisi.</li>
</ul>
</p>

<br />
<div class='subtitle'>Le mode Economie</div>
<p class='rule_pg'>Le mode Economie vous permet de voir les listes de
	ventes des personnes proches, de modifier votre propre liste de vente,
	et d'acheter des objets à d'autres personnages.</p>
<br />
<br />
