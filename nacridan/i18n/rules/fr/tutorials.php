<?php
require_once ("../../../conf/config.ini.php");

echo '<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>';
echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n";

$name = "<a href=\"formulas.php?formula_id=2010\" class='popupify'>Détails techniques</a> ";
// class='stylepc'

?>

<div class='subtitle'>Introduction</div>
<p class='rule_pg'>
	Que vous soyez novice ou expert dans les <a
		href='http://fr.wikipedia.org/wiki/Mmorpg'>MMORPG</a> par navigateur,
	certaines possibilités dans le jeu ne sont pas aisément réalisables et
	peuvent même paraitre complexe au premier abord, c'est pourquoi vous
	trouverez ici des aides sur divers points du jeu qui, selon nous,
	nécessitent d'être expliqués. Si toutefois certaines explications
	manquent de clarté, n'hésitez pas à posez votre question sur le forum.</br>
</p>
</br>


<div class='subtitle'>Devenir Paladin</div>

<p class='rule_pg'>
	<i>Attention ce tutoriel est très précis. Pour le comprendre, il faut
		que vous soyez au clair avec le système de progression et les jauges
		des caractéristiques. <br />
	</i><br /> Devenir un bon Paladin de Tonak (voire l'onglet <a
		href='rules.php?page=step7'>Compétences</a>.</i>) n'est pas chose
	aisée car c'est la seule classe de guerrier qui n'utilisent pas de
	dextérité. Il faut donc dans la mesure du possible éviter dès le début
	les actions s'appuyant sur la dextérité et qui vont donc augmenter sa
	jauge de progression au risque de se retrouver avec trop de dés de
	dextérité inutile. <br /> <br />Le plus facile est donc bannir toutes
	les attaques au corps à corps car que ce soit avec une dague, une épée
	longue ou une masse, elle requiert de la dextérité. Le mieux est donc
	de commencer à économiser de l'argent en vue d'acheter Armure d'Athlan,
	un sortilège élémentaire qui conservera de son utilité même à un niveau
	élevé. Reportez-vous donc au moyen d'obtenir de l'or comme les missions
	d'escorte, l'apprentissage d'un savoir-faire d'extraction ou encore les
	caravanes marchandes. <br /> <br /> Une fois que vous possédez votre
	sortilège Armure d'Athlan, tentez d'intégrer un Groupe de Chasse et
	utiliser votre sort pour soutenir votre groupe jusqu'à atteindre 80% de
	maitrise tout en économisant de l'or et en gagnant des niveaux et donc
	des dés de MM et Force exclusivement. <br />Lorsque vous atteignez 80%
	de maitrise, vous pouvez apprendre un sort de niveau aspirant.
	Choississez "Appel de la lumière" en échange de 400 pièces d'or.
	Investissez également dans une Masse Sacrée. <br /></br> Et voilà, vous
	pouvez enfin taper les monstre sans progresser en dextérité. Apprenez
	d'autres compétence de Paladin ou des sortilège des l'école de
	Protection et faites-vous plaisir. <br /> <br /> <i><b> Une autre
			alternative </b></i><br /> <br /> Si vous ne pouvez pas vous passer
	d'occir quelques monstres vous-même avant d'obtenir 80% de maitrise
	dans Armure d'Athlan, limitez alors la prise de dextérité en suivant
	les conseils suivants. <br /> <br /> Gagnez de l'or puis investissez
	dans une Masse Sacrée (arme lourde qui vous permettra de progresser
	principalement dans la jauge Force) et apprenez Dégâts Accrus pour 80
	pièces d'or. Vous limitez ainsi l'augmentation de votre jauge de
	Dextérité à chaque attaque et vous ne devriez pas avoir pris beaucoup
	d'amélioration de cette caractéristique le temps d'atteindre 80% de
	maîtrise pour Dégâts Accrus. Ce seuil de maîtrise minimum dans un
	talent élémentaire vous permettra alors d'apprendre les compétences
	d'aspirant de l'école des Paladins de Tonak. <br /> <br /> <br />
<div class='subtitle'>Réaliser une mission commerciale</div>
<p class='rule_pg'>
	Complète les éléments disponibles dans l'onglet <a
		href='rules.php?page=step12'>Villages</a>, section <a
		href='formulas.php?formula_id=10'
		onclick='javascript:detailedRules("formulas.php?formula_id=10");return false;'>Caravansérail</a>
	du Comptoir Commercial.<br> <br> <i />Devenir un marchand réputé est le
	moyen le plus efficace de devenir riche. Cependant, les débuts ne sont
	pas des plus rapides. Si vous avez besoin d'une petite somme pour un
	objectif à court terme, il existe d'autres moyens plus rapide au début
	de gagner de l'or. <br>Les missons commerciales consistent à achalander
	une cargaison d'un comptoir commercial à un autre. Pour porter cette
	cargaison, on vous confira une tortue géante : un animal docile et
	robuste mais lent. Vous aurez la lourde tâche de conduire cette tortue
	géante le long des routes. </i><br> <br> Rendez-vous dans un comptoir
	commercial, il s'agit d'un bâtiment aux tuiles vertes. Le plus optimal,
	est de posséder au moins 60po car c'est la somme correspondant à la
	cargaison que vous serez en droit d'acheter avec une réputation de
	marchand à 1.<br /> Au Caravansérail, vous pouvez organiser en détail
	les marchandises que vous souhaitez acheminer. Plus votre réputation de
	marchand sera élevé et plus vous aurez le droit d'acheter des
	cargaisons onéreuses augmentant ainsi votre profit à l'arrivée. <br />
	<br /> Cliquez sur Continuer et choisissez alors le Comptoir de
	destination : plus celui-ci est éloigné, plus les profits à l'arrivée
	augmentent. Attention de choisir <u>judicieusement</u> votre
	destination ! Entre autres facteurs de rentabilité à considérer, il
	faut surtout s'assurer que le village d'arrivée n'est pas sous le
	contrôle d'un joueur ayant fermé ses accès, ou que le trajet ne va pas
	vous faire entrer dans des zones où les dangers seraient bien
	supérieurs à votre niveau d'expérience ! <br> <br />Enfin, pour
	finaliser votre caravane, vous devez alors définir votre chargement
	selon certaines règles (valeur, quantité et type de chargement) : 10
	pièces minimum dont la valeur totale n'excéde pas 60po.</br> </br> Une
	fois la paperasse réalisée, une tortue géante vous attend dans les
	écuries du bâtiments (vous pouvez consulter ses caractéristiques à
	l'aide du sélecteur de personnage). Elle transporte votre cargaison.
	Dans le menu Action, <i> Chevaucher la tortue (2pa) </i>. Votre
	personnage entre alors dans la carapace de la tortue et n'est plus
	visible et vous n'avez plus accès aux pièces spécifiques du bâtiment
	car elles sont interdites aux tortues.<br /> <br /> Sortez du bâtiment
	normalement, la tortue apparait au centre de votre vu, à la place de
	votre personnage caché dedans. Déplacez-vous normalement, vos
	déplacements vous coûte 1PA supplémentaire. Notez que vous pouvez
	descendre n'importe quand de la tortue à l'aide du bouton disponible
	dans le menu Action. Il vous faudra d'ailleurs descendre de la tortue,
	après l'avoir laisser dans les écuries, pour entrer dans le
	caravansérail du comptoir commerciale de destination.<br /> <br />
	Attention, vous avez un temps imparti pour terminer cette mission de
	transport : si vous ne le respectez pas, vous ne gagnerez pas d'argent
	à l'arrivée et vous pouvez perdre de la réputation si le retard est
	conséquent. Veillez également sur votre tortue, elle peut être attaquée
	par les monstres et les PJ. Si votre tortue est tuée, vous échouez dans
	votre mission, le chargement tombera au sol vous perdrez de la
	réputation. </br>
</p>
<br /></br>
<br /></br>
