<div class='subtitle'>Le but c'est quoi ?</div>
<p class='rule_pg'>Devenir un homme influant, un puissant guerrier ou
	développer un commerce fructueux. Les possibilités offertes sur l'île
	pour votre personnage sont grandes. A vous de choisir votre voie et
	d'incarner le personnage qui vous corresponde le mieux. Monstres,
	sortilèges, secrets, armes magiques, alliances, commerce : voilà ce que
	sera votre quotidien...</p>


<div class='subtitle'>C'est bien mais comment ça marche ?</div>
<p class='rule_pg'>
	Le principe du jeu est simple. Vous incarnez un personnage possédant <b>7
		caractéristiques</b> :
</p>
<table style='margin-left: 20px'>
	<tr>
		<td width="250px">1 - <b>L'attaque</b></td>
		<td>L'attaque détermine votre capacité à porter une attaque et votre
			dextérité dans le maniement des armes offensives.</td>
	</tr>
	<tr>
		<td>2 - <b>L'esquive</b></td>
		<td>L'esquive détermine votre habileté à esquiver les coups des
			ennemis.</td>
	</tr>
	<tr>
		<td>6 - <b>La maîtrise de la magie</b></td>
		<td>La maîtrise de la magie représente votre puissance magique.</td>
	</tr>
	<tr>
		<td>3 - <b>Les dégâts</b></td>
		<td>Les dégâts déterminent la brutalité des attaques portées.</td>
	</tr>
	<tr>
		<td>4 - <b>La régénération </b></td>
		<td>La régénération correspond à la capacité de régénération en points
			de vie.</td>
	</tr>
	<tr>
		<td>5 - <b>L'armure</b></td>
		<td>L'armure permet d'absorber les dégâts infligés par une attaque.</td>
	</tr>
	<tr>
		<td>7 - <b>Les points de vie</b></td>
		<td>Les points de vie correspondent au nombre de points de dégâts que
			votre personnage pourra encaisser avant de mourir.</td>
	</tr>
</table>
<br />
<br />

Votre potentiel dans les caractéristiques attaque, esquive, maîtrise de
la magie, dégâts et régénération est représenté par le nombre de dés que
vous avez en votre possession dans ces catégories. La puissance de votre
armure dépend de votre équipement et pour finir, les points de vie
représentent les points de dégâts que votre personnage pourra encaisser
avant de mourir.
<br />


<div class='subtitle'>Quel personnage je peux incarner ?</div>
<p class='rule_pg'>
	Au départ, le joueur choisit la race de son personnage: <b>Elfe, Nain,
		Humain, Dorane</b>.<br />Chacune de ces races possède des
	particularités dans les caractéristiques précitées :
</p>
<br />
<table style='margin-left: 20px' cellspacing="1px">
	<tr>


		<td width="20px" bgcolor="#33cccc"></td>
		<td width="100px" bgcolor="#33cccc"><b>&nbsp;L'Elfe</b></td>
		<td><?php echo "<img src='".CONFIG_HOST."/pics/rules/Vign_Elf_General.jpg' alt=\"\"/>"; ?></td>
		<td>Avantage : Esquive</td>
		<td width="20px" bgcolor="#33cccc"></td>
	</tr>


	<tr>
		<td width="20px" bgcolor="#66cc33"></td>
		<td bgcolor="#66cc33"><b>&nbsp;Le Nain</b></td>
		<td><?php echo "<img src='".CONFIG_HOST."/pics/rules/Vign_Dwf_General.jpg' alt=\"\"/>"; ?></td>
		<td>Avantage : Dégâts</td>
		<td width="20px" bgcolor="#66cc33"></td>
	</tr>


	<tr>
		<td width="20px" bgcolor="#cc0000"></td>
		<td bgcolor="#cc0000"><b>&nbsp;L'Humain</b></td>
		<td><?php echo "<img src='".CONFIG_HOST."/pics/rules/Vign_Hum_General.jpg' alt=\"\"/>"; ?></td>
		<td>Avantage : Attaque</td>
		<td width="20px" bgcolor="#cc0000"></td>
	</tr>


	<tr>
		<td width="20px" bgcolor="#cc6699"></td>
		<td bgcolor="#cc6699"><b>&nbsp;Le Dorane</b></td>
		<td><?php echo "<img src='".CONFIG_HOST."/pics/rules/Vign_Dor_General.jpg' alt=\"\"/>"; ?></td>
		<td>Avantage : Maîtrise de la Magie</td>
		<td width="20px" bgcolor="#cc6699"></td>
	</tr>
</table>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />