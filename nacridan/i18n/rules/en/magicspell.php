<div class='subtitle'>Sortilèges</div>
<br />
Tout personnage peut apprendre des sortilèges (comme dans Harry
Potter... ) dans les villes et les villages en dépensant des points
d'expérience. Chaque nouveau sortilège fraîchement acquis n'est maîtrisé
qu'à 50%. Ceci signifie que le joueur aura 1 chance sur 2 de réussir son
sortilège à ses débuts. Bien évidemment, avec la pratique, ce
pourcentage augmentera jusqu'à atteindre la limite de maîtrise maximale
qui est de 90%.
<br />
<br />
Pour augmenter un sortilège de 1% il faudra que le personnage :
<br />
<ul>
	<li>Réussisse son sortilège.</li>
	<li>Et réussisse son jet d'amélioration (un jet d'amélioration est
		réussi lorsque le joueur obtient sur un dé 100 une valeur supérieure à
		son pourcentage de maîtrise du sortilège).</li>
</ul>
Le niveau de tout sortilège dépendra du jet de maîtrise de la magie (MM)
effectué par le personnage. Plus le joueur aura un nombre de dés en MM,
plus la puissance des sorts sera élevée.
<br />
<br />
Le niveau d'un sort d'attaque se calcule de la manière suivante :
<br />
<ul>
	<li>Soit JA le jet de MM de l'attaquant</li>
	<li>Soit JD le jet de MM du défenseur</li>
</ul>
Le niveau du sort est calculé de la sorte :
<b>Niveau du sort = (JA/12) + (JA-JD)/6</b>
(que l'on arrondi à l'entier inférieur)
<br />
<br />
Le niveau d'un sort de défense se calcule de la manière suivante :
<br />
<ul>
	<li>Soit JA le jet de MM de l'attaquant</li>
</ul>
Le niveau du sort est calculé de la sorte :
<b>Niveau du sort = (JA/6)</b>
(que l'on arrondi à l'entier inférieur)
<br />
<br />
Les Sortilèges :
<br />
<br />
<ul>
	<li style="margin-bottom: 20px;"><b>Boule de Feu ( 5 PA / 50 PX ):</b><br />
		Ce sort permet d'envoyer une boule de feu sur la cible lui
		occasionnant 2D3 points de dégats par niveau de sort atteint par le
		lanceur du sort.</li>
	<li style="margin-bottom: 20px;"><b>Torpeur d'Oz ( 4 PA / 200 PX ):</b><br />
		Ce sort plonge la cible dans un genre de demi-sommeil auquel il est
		difficile de résister. La cible perdra, jusqu'à sa prochaine DLA, 1D6
		d'esquive par 4 niveaux de sort atteint par le lanceur du sort.</li>
	<li style="margin-bottom: 20px;"><b>Larmes de Vie ( 4 PA / 50 PX ):</b><br />
		Ce sortilège de soin permet de redonner 1D3 Points de vie (PV) par
		niveau de sort atteint par le lanceur du sort.</li>
	<li style="margin-bottom: 20px;"><b>Bouclier d'Athlan ( 3 PA / 50 PX ):</b><br />
		Ce sortilège de soin permet de redonner +1 point d'armure par niveau
		de sort atteint par le lanceur du sort.</li>
</ul>
<br />
