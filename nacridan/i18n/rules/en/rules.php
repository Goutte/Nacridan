<?php
require_once ("../../../conf/config.ini.php");
$tabImage = array(
    "",
    "Butt_Rules_Princ_",
    "Butt_Rules_Gene_",
    "Butt_Rules_Exp_",
    "Butt_Rules_Inter_",
    "Butt_Rules_Act_",
    "Butt_Rules_Combats_",
    "Butt_Rules_Compet_",
    "Butt_Rules_Sorti_",
    "Butt_Rules_Equip_"
);
$page = "";

if (isset($_GET["page"])) {
    $page = $_GET["page"];
}
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">';
echo '<head>';

echo '<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>';
echo '<title>Nacridan</title>';
echo '<link rel="stylesheet" type="text/css" href="' . Cache::get_cached_file('/css/nacridanMain.css') . '" />';

echo "<style type=\"text/css\">\n";
for ($i = 1; $i <= 9; $i ++) {
    echo ".rule_step" . $i . " {display:block; height: 25px;\n width: 100px;\n background:  url(../../../pics/rules/Regle_Menu/" . $tabImage[$i] . "Off.jpg) no-repeat; \n}\n";
    echo ".rule_step" . $i . ":hover {\n background: url(../../../pics/rules/Regle_Menu/" . $tabImage[$i] . "Roll.jpg) no-repeat;)\n}\n";
}

?>
</style>
</head>

<body class='rule_bg'>
	<div class="rule_content">
<?php
echo "<a href='" . CONFIG_HOST . "' class='rule_home' ></a>\n";
// echo "<img src='".CONFIG_HOST."/pics/rules/Rules_TopBanner_Nacrid.jpg'/>";
// echo "<img src='".CONFIG_HOST."/pics/rules/Rules_TopBanner_Regles.jpg'/>";
for ($i = 1; $i <= 9; $i ++) {
    echo "<img src='" . CONFIG_HOST . "/pics/rules/Regle_Menu/" . $tabImage[$i] . "Roll.jpg' style='display:none' alt='' />";
}

echo "<ul class='rule_list_item' style='position:absolute; top: 100px'>";
for ($i = 1; $i <= 9; $i ++) {
    if ($page == "step" . $i) {
        echo "<li class='rule_item'><img src='" . CONFIG_HOST . "/pics/rules/Regle_Menu/" . $tabImage[$i] . "On.jpg' alt=\"\" /></li>";
    } else {
        echo "<li class='rule_item'><a href='rules.php?page=step" . $i . "' class='rule_step" . $i . "'></a></li>";
    }
}
echo "</ul>";

echo "<div class='rule_center' style='position:absolute; top: 125px'>";
echo "<div style='height: 5px;'><img src='" . CONFIG_HOST . "/pics/rules/GdeToile_TextureOmbre.jpg' alt=\"\"/></div>";
echo "<div class='rule_center_content'>";
switch ($page) {
    case "step1":
        include "bases.php";
        break;
    case "step2":
        include "gene.php";
        break;
    case "step3":
        include "xp.php";
        break;
    case "step4":
        include "panel.php";
        break;
    case "step5":
        include "action.php";
        break;
    case "step6":
        include "fight.php";
        break;
    case "step7":
        include "ability.php";
        break;
    case "step8":
        include "magicspell.php";
        break;
    case "step9":
        include "equip.php";
        break;
    default:
        include "bases.php";
        
        break;
}
echo "</div>";
echo "<div style='height: 5px;'><img src='" . CONFIG_HOST . "/pics/rules/Regle_Bottom.jpg' alt=\"\"/></div>";
echo "</div>";

?>
</div>
</body>

</html>