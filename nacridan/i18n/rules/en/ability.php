<div class='subtitle'>Compétences</div>
<br />
Tout personnage peut apprendre des compétences dans les villes et les
villages en dépensant des points d'expérience. Chaque nouvelle
compétence fraîchement acquise n'est maîtrisée qu'à 50%. Ceci signifie
que le joueur aura 1 chance sur 2 de réussir sa compétence à ses débuts.
Bien évidemment, avec la pratique, ce pourcentage augmentera jusqu'à
atteindre la limite de maîtrise maximale qui est de 90%.
<br />
<br />

Pour augmenter une compétence de 1% il faudra que le personnage :
<br />
<ul>
	<li>Réussisse sa compétence.</li>
	<li>Et réussisse son jet d'amélioration (un jet d'amélioration est
		réussi lorsque le joueur obtient sur un dé 100 une valeur supérieur à
		son pourcentage de maîtrise dans la compétence à améliorer).</li>
</ul>
<br />
<br />
Les Compétences :
<br />
<ul>
	<li style="margin-bottom: 20px;"><b>Attaque Éclair ( 5 PA / 75 PX ):</b><br />
		Cette compétence permet d'augmenter le jet d'attaque du personnage du
		nombre de dés de sa caractéristique attaque +4D6.</li>
	<li style="margin-bottom: 20px;"><b>Dégâts Accrus ( 5 PA / 75 PX ):</b><br />
		Cette compétence permet d'augmenter le jet de dégâts du personnage du
		nombre de dés de sa caractéristique dégâts (sans compter les bonus)
		divisé par deux.</li>
	<li style="margin-bottom: 20px;"><b>Encercler ( 2 PA / 25 PX ):</b><br />
		Grâce à cette compétence, un joueur peut encercler son adverssaire à
		hauteur de son nombre de dés d'esquive. Si elle n'arrive pas à fuir,
		le futur déplacement de la cible lui en coûtera 6 PA. (toute attaque
		ou déplacement réalisé par le joueur annule les encerclements qu'il
		avait réalisés).</li>
	<li style="margin-bottom: 20px;"><b>Fuite ( 2 PA / 25 PX ):</b><br />
		Grâce à fuite, vous pourrez tenter de contrer un encerclement et ainsi
		éviter le malus de 6 PA au déplacement. Pour déterminer si la fuite a
		réussi, un jet opposant l'esquive du joueur à la valeur de
		l'encerclement est effectué.</li>
	<li style="margin-bottom: 20px;"><b>Provocation ( 2 PA / 25 PX ):</b><br />
		Cette compétence permet à un joueur d'attirer l'attention d'un monstre
		(c'est à dire tout personnage non joueur (PNJ)) de façon à ce que ce
		dernier le prenne pour cible.</li>
</ul>
<br />
