<?php
require_once ("../../../conf/config.ini.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

$stepX = 155;
$stepY = 60;

$equip[1] = array(
    220 - $stepX,
    255 - $stepY
);
$equip[2] = array(
    323 - $stepX,
    255 - $stepY
);

$equip[4096] = array(
    190 - $stepX,
    235 - $stepY
);
$equip[8192] = array(
    355 - $stepX,
    235 - $stepY
);

$equip[4] = array(
    273 - $stepX,
    185 - $stepY
);
$equip[8] = array(
    235 - $stepX,
    155 - $stepY
);
$equip[16] = array(
    311 - $stepX,
    155 - $stepY
);
$equip[32] = array(
    273 - $stepX,
    98 - $stepY
);

$equip[64] = array(
    array(
        200 - $stepX,
        415 - $stepY
    ),
    array(
        345 - $stepX,
        415 - $stepY
    )
);

$equip[128] = array(
    273 - $stepX,
    145 - $stepY
);
$equip[256] = array(
    0,
    0
);

$equip[512] = array(
    array(
        240 - $stepX,
        300 - $stepY
    ),
    array(
        302 - $stepX,
        300 - $stepY
    )
);

$equip[1024] = array(
    array(
        215 - $stepX,
        365 - $stepY
    ),
    array(
        330 - $stepX,
        365 - $stepY
    )
);
$equip[2048] = array(
    0,
    0
);

function getEquipCss()
{
    global $equip;
    
    $str = ".img{position: absolute; z-index: 1; border: 0;}\n";
    
    $i = 1;
    while ($i <= 8192) {
        if (isset($equip[$i])) {
            if (is_array($equip[$i][0])) {
                $j = 0;
                foreach ($equip[$i] as $equipval) {
                    $str .= "#equip" . $i . $j . " { left: " . $equipval[0] . "px; top: " . $equipval[1] . "px; }\n";
                    $j ++;
                }
            } else {
                $str .= "#equip" . $i . " { left: " . $equip[$i][0] . "px; top: " . $equip[$i][1] . "px; }\n";
            }
        }
        $i *= 2;
    }
    return $str;
}

function writeline($eqmodifier, &$characLabel, $color = 1)
{
    foreach ($characLabel as $val) {
        $extra = "";
        
        if ($color) {
            $charac = $eqmodifier->getModif($val, DICE_ADD);
            if ($charac == 0) {
                $charac = $eqmodifier->getModif($val, DICE_MULT);
            }
            if ($charac > - 99) {
                $extra = "class='dred'";
            }
            if ($charac > - 10) {
                $extra = "class='red'";
            }
            if ($charac > - 7) {
                $extra = "class='lred'";
            }
            if ($charac > - 3) {
                $extra = "class='vlred'";
            }
            if ($charac > 0) {
                $extra = "class='vlgreen'";
            }
            if ($charac > 3) {
                $extra = "class='lgreen'";
            }
            if ($charac > 7) {
                $extra = "class='green'";
            }
            if ($charac > 10) {
                $extra = "class='dgreen'";
            }
        } else {
            $extra = "class='lgreen'";
        }
        
        $str = $eqmodifier->getModifStr($val);
        
        if ($str != 0) {
            echo "<td align='center'" . $extra . ">" . $str . "</td>";
        } else
            echo "<td align='center'>-</td>";
    }
}

echo "<style type=\"text/css\">\n" . getEquipCss() . "</style>";

echo "<script type='text/javascript' src='" . Cache::get_cached_file("/javascript/equip.js" . "'></script>\n";

echo "<div id='overlay' style='overflow: auto; visibility: hidden; position: absolute;'></div>";

echo "<div class='subtitle' >Équipements :</div>";
echo "<div class='rule_pg'>";
$eq = new BasicEquipment();
$eqmodifier = new Modifier();

$db = DB::getDB();

$dbe = new DBCollection("SELECT zone," . $eq->getASRenamer("EQ", "EQ") . "," . $eqmodifier->getASRenamer("EQM", "EQM") . ",mask FROM BasicEquipment AS EQ LEFT JOIN EquipmentType AS ET ON EQ.id_EquipmentType=ET.id LEFT JOIN Modifier_BasicEquipment AS EQM ON  id_Modifier_BasicEquipment=EQM.id ORDER BY EQ.id_EquipmentType ASC, EQ.name ASC", $db);

if (isset($_GET["t1level"])) {
    $selectedlevel = min($_GET["t1level"], 10);
} else {
    $selectedlevel = 0;
}
echo "<div class='subtitle'> - Les Armes de niveau " . $selectedlevel . "</div>";

$str = "<form action=\"\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 0; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t1level=" . $i . "' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

$characLabel = array(
    "attack",
    "dodge",
    "damage",
    "magicSkill"
);
echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 160px;'>Nom</th><th>Attaque</th><th>Esquive</th><th>Dégâts</th><th>Magie</th><th>Nb zone(s)</th></tr>";
echo "<tr><td colspan=\"8\">&nbsp;</td></tr>";
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") == 1 || $eq->get("id_EquipmentType") == 2) {
        $mask = "Array(";
        $i = 0;
        if (isset($equip[$dbe->get("mask")]) && is_array($equip[$dbe->get("mask")][0])) {
            foreach ($equip[$dbe->get("mask")] as $value) {
                if ($i != 0)
                    $mask .= ",";
                $mask .= "'" . $dbe->get("mask") . "'";
                $i ++;
            }
            $mask .= ")";
        } else {
            $mask = "Array('" . $dbe->get("mask") . "')";
        }
        
        echo "<tr><td class='alignleft' onmouseover=\"equipShow(this," . $mask . ",'" . CONFIG_HOST . "',event)\" onmouseout=\"equipHide(this,event)\">" . $eq->get("name") . "</td>";
        $eqmodifier->updateFromEquipmentLevel($selectedlevel);
        $eqmodifier->initCharacStr();
        writeline($eqmodifier, $characLabel);
        echo "<td class='aligncenter'>" . $dbe->get("zone") . "</td>";
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table><br/>";

$dbe->first();

if (isset($_GET["t2level"])) {
    $selectedlevel = min($_GET["t2level"], 10);
} else {
    $selectedlevel = 0;
}

echo "<div class='subtitle' > - Les Armures de niveau " . $selectedlevel . "</div>";

$str = "<form action=\"\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 0; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t2level=" . $i . "' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

$characLabel = array(
    "attack",
    "dodge",
    "damage",
    "armor",
    "magicSkill"
);
echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 160px;'>Nom</th><th>Attaque</th><th>Esquive</th><th>Dégâts</th><th>Armure</th><th>Magie</th><th>Nb zone(s)</th></tr>";
echo "<tr><td colspan=\"8\">&nbsp;</td></tr>";
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") != 8 && $eq->get("id_EquipmentType") != 9 && $eq->get("id_EquipmentType") != 12 && $eq->get("id_EquipmentType") != 1 && $eq->get("id_EquipmentType") != 2) {
        $eqmodifier->updateFromEquipmentLevel($selectedlevel);
        $mask = "Array(";
        $i = 0;
        if (isset($equip[$dbe->get("mask")]) && is_array($equip[$dbe->get("mask")][0])) {
            foreach ($equip[$dbe->get("mask")] as $value) {
                if ($i != 0)
                    $mask .= ",";
                $mask .= "'" . $dbe->get("mask") . "'";
                $i ++;
            }
            $mask .= ")";
        } else {
            $mask = "Array('" . $dbe->get("mask") . "')";
        }
        echo "<tr><td onmouseover=\"equipShow(this," . $mask . ",'" . CONFIG_HOST . "',event)\" onmouseout=\"equipHide(this,event)\">" . $eq->get("name") . "</td>";
        $eqmodifier->initCharacStr();
        writeline($eqmodifier, $characLabel);
        echo "<td class='aligncenter'>" . $dbe->get("zone") . "</td>";
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table><br/>";

$dbe->first();

if (isset($_GET["t3level"])) {
    $selectedlevel = min($_GET["t3level"], 10);
} else {
    $selectedlevel = 0;
}
echo "<div class='subtitle' > - Les Potions de niveau " . $selectedlevel . "</div>";

$str = "<form action=\"\"><select name=\"url\" onchange=\"window.location=this.options[selectedIndex].value\">";

for ($i = 0; $i <= 10; $i ++) {
    if ($i == $selectedlevel)
        $extra = "selected=\"selected\"";
    else
        $extra = "";
    
    $str .= "<option value='?page=step9&amp;t3level=" . $i . "' " . $extra . ">Niveau " . $i . "</option>\n";
}
$str .= "</select></form>\n";
echo $str;

$characLabel = array(
    "attack",
    "dodge",
    "damage",
    "hp",
    "regen"
);
echo "<table class='coloredtable'>";
echo "<tr><th align='left' style='width: 160px;'>Nom</th><th>Attaque</th><th>Esquive</th><th>Dégâts</th><th>PDV</th><th>Régénération</th></tr>";
echo "<tr><td colspan=\"7\">&nbsp;</td></tr>";
while (! $dbe->eof()) {
    $eq->DBLoad($dbe, "EQ");
    $eqmodifier->DBLoad($dbe, "EQM");
    if ($eq->get("id_EquipmentType") == 8 || $eq->get("id_EquipmentType") == 12) {
        echo "<tr><td>" . $eq->get("name") . "</td>";
        $eqmodifier->updateFromEquipmentLevel($selectedlevel);
        $eqmodifier->initCharacStr();
        writeline($eqmodifier, $characLabel, 0);
        echo "</tr>";
    }
    $dbe->next();
}
echo "</table>";
echo "</div><br/><br/>";

?>
<br />
<div class='subtitle'>Les équipements peuvent avoir des pouvoirs
	magiques :</div>

<table>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>de l'Aigle</b>
		</td>
		<td>(Bonus d'Attaque)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>des Ombres</b>
		</td>
		<td>(Bonus d'Esquive)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>Flamboyant(e)</b>
		</td>
		<td>(Bonus de Dégâts )</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>de Jade</b>
		</td>
		<td>(Bonus en Maîtrise de la Magie)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>de Crystal</b>
		</td>
		<td>(Bonus d'Armure)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>des Vampires</b>
		</td>
		<td>(Bonus de Régénération)</td>
	</tr>

	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>de la Loi</b>
		</td>
		<td>(Bonus d'Esquive et de Dégâts)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>du Chaos</b>
		</td>
		<td>(Bonus d'Attaque et de Dégâts)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>de la Balance</b>
		</td>
		<td>(Bonus d'Attaque et d'Esquive)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;- Équipement <b>Mythique</b>
		</td>
		<td>(Bonus dans toutes les caractéristiques)</td>
	</tr>
</table>
<br />
<br />
Les pouvoirs magiques ont des niveaux allant de 1 à 10.
<br />
<br />
Contraintes sur les enchantements :
<br />
- le niveau d'un enchantement d'un objet ne peut être suppérieur à celui
du niveau de l'objet.
<br />
- un objet ne peut recevoir qu'un seul type d'enchantement.
<br />
<br />

<div class='subtitle'>Surchage :</div>

Si la somme des niveaux des objets équipés est supérieure au niveau du
personnage, alors ce dernier sera en surcharge.
<br />
<br />
Exemple, un personnage niveau 3 est équipé de :
<br />
- Un casque niveau 0
<br />
- Une Épée niveau 2
<br />
- Un Bouclier niveau 2
<br />
<br />
La somme des niveaux des équipements est égale à : 0 + 2 + 2 = 4
<br />
Par conséquent, personnage sera en surcharge de 1 point.
<br />
<br />
En cas de surcharge, le personnage sera affecté par la folie des
équipements. Dans ce mode, certains objets ne seront peu ou pas du tout
activés.
<br />
<br />
<br />
<br />
<br />
<br />

<!--

<div class='subtitle' >Équipement Légendaire (ou "set" en anglais) :</div>
Si vous avez par exemple tous les équipements au complet de la Balance (casque, arme, armure, ...) vous avez un bonus supplémentaire (de x2) dans les caractéristiques de base de l'Équipement magique. Par exemple l'équipement au complet de la balance donnera un bonus supplémentaire en Attaque et en Esquive !
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
-->