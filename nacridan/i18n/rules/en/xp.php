
<img style="float: right;"
	src="<?echo CONFIG_HOST;?>/pics/rules/warrior.jpg" alt="" />
<div class='subtitle'>Comment on progresse ?</div>
<p class='rule_pg'>En combattant, en réussissant des sortilèges ou des
	compétences votre personnage gagne des points d'expérience (PX). Les
	vaincus peuvent aussi laisser tomber des pièces d'or (PO) et pièces
	d'équipement diverses (armes, casques, armures, etc...). La première
	façon de progresser est tout simplement de s'équiper correctement afin
	d'améliorer ses caratéristiques de base. La seconde façon de
	s'améliorer va être de dépenser ses PX (points d'expérience) afin
	d'améliorer son personnage sur la caractéristique de son choix
	(Attaque, Esquive, Dégâts, etc...).</p>

<div class='subtitle'>C'est tout ?</div>
<p class='rule_pg'>
	Presque, il faut juste savoir que les 7 caratéristiques de base
	évoluent de manière différente suite à une amélioration :<br /> <br />
</p>
<table class='rule_pg'>
	<tr>
		<td width="200px">1 - <b>L'attaque</b></td>
		<td>augmente de 1 dé à 6 faces.</td>
	</tr>
	<tr>
		<td>2 - <b>L'esquive</b></td>
		<td>augmente de 1 dé à 6 faces.</td>
	</tr>
	<tr>
		<td>6 - <b>La maîtrise de la magie</b></td>
		<td>augmente de 1 dé à 6 faces.</td>
	</tr>
	<tr>
		<td>3 - <b>Les dégâts</b></td>
		<td>augmente de 1 dé à 3 faces.</td>
	</tr>
	<tr>
		<td>4 - <b>La régénération </b></td>
		<td>augmente de 1 dé à 3 faces.</td>
	</tr>
	<tr>
		<td>5 - <b>L'armure</b></td>
		<td>elle ne peut s'augmenter que par l'équipement.</td>
	</tr>
	<tr>
		<td>7 - <b>Les points de vie</b></td>
		<td>augmente de 10.</td>
	</tr>
</table>

<p class='rule_pg'>
	Enfin, à force d'améliorations vous progresserez dans les niveaux afin
	d'être à même d'affronter les créatures les plus redoutables de l'île.<br />
	Ou alors d'éclater votre pote niveau 1 en un coup. ;-)
</p>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />