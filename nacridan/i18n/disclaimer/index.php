<?php
require_once ("../../conf/config.ini.php");

switch (getBrowserDefaultLang()) {
    case "fr-fr":
    case "fr":
        $lang = "fr";
        break;
    default:
        $lang = "en";
        break;
}

switch ($lang) {
    case "fr":
    case "en":
        redirect(CONFIG_HOST . "/i18n/disclaimer/" . $lang . "/disclaimer.php");
        break;
    default:
        redirect(CONFIG_HOST . "/i18n/disclaimer/en/disclaimer.php");
        break;
}
?>