




<html>
<head>
<meta http-equiv='Content-type' content='text/html; charset=UTF-8'>
<title>Nacridan</title>

<meta name='description' content='Jeu de rôle (JDR) en ligne où l' on
	incarne un Héros (guerrier, magicien, etc.) en tourpartour.'>
<meta name='keywords'
	content='Jeu, Rôle, RPG, JdR, pbem, jpem, héro, monstre, trésor, sortilège, sort, armure, arme, BM, jeux, php, gratuit, aventure, simulation, asynchrone'>
<meta name='robots' content='all'>
<meta name='revisit-after' content='1 week'>
<meta http-equiv='Content-Language' content='fr-FR'>
<meta name='author' content='Jails'>


<body text='#000000'>
	<table style='font-size: 12px;' border='0' cellpadding='0'
		cellspacing='5' width='100%'>
		<tbody>
			<tr>
				<td>
					<p align='center'>Charte d'Admission au Jeu : Nacridan</p>
					<p>
						<b>Nacridan </b> est un jeu gratuit, ouvert à toute personne
						possédant une adresse e-mail et un accès internet régulier. Afin
						de jouer dans de bonnes conditions, chaque joueur se doit de
						suivre les règles de cette présente charte sous peine de sanction
						ou d'exclusion définitive du jeu. <br> <br> 

						<b>1- <u>Données personnelles :</u></b> 

						<br> Vous disposez d'un droit d'accès, de
						modification, de rectification et de suppression des données vous
						concernant (loi « Informatique et Libertés » du 6 janvier 1978).
						Pour toute demande, adressez-vous à : nacridan@gmail.com<br />
						Pour éviter d'éventuelles tricheries, toute demande de
						modifications sur ses propres données avec des conséquences sur
						l'équilibre du jeu entraînera la suppression du compte du joueur
						et de son personnage. L'adresse e-mail fournie lors de votre
						inscription ne sera communiquée à aucune organisation tierce.
						Cependant, par cette inscription, vous autorisez au moteur du jeu
						et à l'équipe de Nacridan de vous contacter librement via cette
						adresse. <br> <br> 
						
						<b>2- <u>Multi-Compte :</u></b> 
						
						<br> Dans
						Nacridan, il est interdit d'avoir plusieurs comptes. Toute
						personne possédant plus d'un compte sera considéré comme
						pratiquant du multi-compte. En cas de multi-compte avéré, les
						comptes du joueur seront désactivés et/ou supprimés sans préavis.
						Il est possible de contrôler plusieurs personnages avec un seul
						compte et c'est la voie à suivre si un seul personnage ne vous
						suffit pas. <br> <br> 
						
						<b>3- <u>Sitting de Compte :</u></b> 
						
						<br> Le
						sitting de compte, consistant à jouer occasionnellement le compte
						d'un autre joueur, est autorisé. En cas d'abus, le sitting sera
						considéré comme un cas de multi-compte. « Occasionnel » signifie
						que le propriétaire d'un compte ne doit pas faire jouer son
						personnage par une tierce personne plus de 10 DLA par mois. Dans
						le cas d'une longue abscence, le joueur devra amener son
						personnage dans la chapelle d'un temple afin que ce dernier soit
						en sureté. Le choix de communiquer son mot de passe à un autre
						joueur est laissé à la discrétion du joueur. <br> <br> 
						
						<b>4- <u>Vente de compte :</u></b> 
								
								<br> Il est interdit de vendre à une tierce
						personne un compte et/ou des éléments du jeu pour de l'argent ou
						pour toutes autre forme de paiement quelle qu'elle soit. <br> <br> 
						
						<b>5- <u>Expérience :</u></b> <br>
						
						Nacridan étant un jeu où il est possible de jouer en groupe, il
						est interdit de sacrifier un des membres du groupe au bénéfice de
						l'évolution d'un autre. Un personnage sacrifiant son évolution au
						profit d'un autre sera considéré comme pratiquant du multi-compte.
						<br> <br> 
						
						<b>6- <u>Dons d'adieu :</u></b> 
						
						<br> Lorsqu'un joueur
						décide de quitter Nacridan et de désactiver son personnage pour de
						bon, il lui est interdit de faire des dons de pièces d'or et
						d'éléments du jeu à d'autres joueurs actifs. Les joueurs qui
						auront accepté de tels dons d'adieu seront désactivés sans
						préavis. Si votre personnage a reçu un tel don à son insu, il vous
						est possible de redonner une valeur identique des PO à Nacridan,
						en utilisant l'interface appropriée de la page des options. <br> <br>
						
						<b>7- <u>Messagerie interne :</u></b>
						
						 <br> La messagerie
						interne du jeu est un endroit où les joueurs doivent endosser le
						rôle de leur personnage. Les messages doivent rester en accord
						avec l'univers du jeu. Le joueur peut parler en son nom, ou en
						celui de son personnage, mais le contenu du message doit rester
						dans le contexte du jeu. Les messages qui ne sont pas dans le cadre du role-play doivent 
						commencer par une balise "HRP" (hors role play) pour le signaler. Le spam est interdit.  <br> <br> 
						
						<b>8-<u>Saisies textuelles :</u>
						</b> <br> Chaque joueur est responsable de ses saisies. Toute
						saisie qui sera jugée, par au moins un membres de l'équipe de
						Nacridan, comme une atteinte à la morale entraînera la suppression
						du compte du joueur. Cette règle s'applique sur tous le site
						www.nacridan.com (y compris le forum, les messages internes, la
						description de son personnage et/ou de son ordre). <br> <br> 
						
						<b>9-<u>Modérateur :</u>
						</b> <br> Les modérateurs ont pour objectif de déceler les
						tricheurs et de veiller à ce que toutes les règles de la charte
						soient suivies correctement. En cas de litige, le joueur peut se
						justifier, puis les modérateurs prendront la décision qu'ils
						auront jugée bonne. Ils ont les pleins pouvoirs concernant la
						résolution du différend. <br> <br> 
						
						<b>10- <u>Délation :</u></b> <br>
						La délation publique est interdite. En cas d'infraction révélée,
						le joueur devra contacter un modérateur en privé. <br> <br> 
						
						<b>11-<u>Complicité :</u>
						</b> <br> Être complice de l'infraction d'un autre personnage est
						sanctionnable. De même, si le personnage d'un joueur profite
						consciemment de l'infraction d'un autre personnage et que preuve
						est faite sur cette situation, ce joueur sera aussi sanctionnable
						(par exemple pour l'infraction d'un membre de son ordre). <br> <br>
						
						<b>12- <u>Automatisation :</u></b> <br> Nacridan est un jeu en
						tour par tour. Chaque action réalisée dans le jeu doit être faite
						par le joueur. Il est par conséquent interdit d'automatiser
						certaines actions par des scripts et/ou programmes quels qu'ils
						soient. <br> <br> 
						
						<b>13- <u>Bugs :</u></b> <br> Malheureusement,
						toute création informatique est souvent assujettie aux erreurs et
						aux bugs. Lorsqu'un joueur a été victime ou a profité d'un bug, il
						doit en faire part sur le forum du jeu ou directement à un des
						modérateurs, selon la gravité du bug. Utiliser volontairement un
						bug à son avantage sera sanctionné par une désactivation du
						compte. En cas de bug avéré, des compensations peuvent être accordées selon 
						le type de bug et la disponibilité de l'équipe. <br> <br> 
						
						<b>14- <u>Délais d'inactivité :</u></b> <br>
						Tout compte inactif durant une période de 6 mois pourra être
						supprimé sans préavis. <br> <br> 
						

						<b>15 - <u>Savoir-vivre :</u></b><br>

    On part du principe que les « règles du jeu » et la présente « charte du jeu » donnent le cadre
     dans lequel on doit jouer et que le « bon sens » de chacun permettra de maintenir une ambiance 
     de jeu agréable et conviviale. Le bon sens étant subjectif, il faut se référer à cette section 
     des règles pour avoir une idée plus précise de ce que l'on entend par là.<br><br>						
						

<b>    16 - <u>Anti-jeu, acharnement</u></b><br>

    Même si le game-play le permet, un acharnement avéré sur le personnage d'un autre joueur sera sanctionné,
     comme toute autre forme d'anti-jeu. Là encore l'acharnement est une notion subjective,
      il faut se référer à cette section des règles pour en avoir une idée plus précise.<br><br>
						
<b>17 - <u>Résolution des conflits</u></b><br>

    Des conflits HRP entre joueurs peuvent survenir. Ici nous parlons bien de joueurs et pas de personnages. Dans ce cas il est du devoir des joueurs de chercher d'abord à se calmer (si besoin en consultant la section Pleine Conscience des règles), puis à résoudre leur conflit via des échanges de MP en HRP. Si cela ne suffit pas, alors il faut faire appel à un modérateur en envoyant un MP à « Modérateur1 » ou « Nacridan ».
    Il ne faut pas poster le conflit dans le forum ! Les accusations publiques n'arrangent jamais la situation.<br><br>	
						
						<b>18- <u>Charte d'admission :</u></b>

						<br> Dans le cas d'une modification de cette présente charte,
						cette dernière sera, comme lors de l'inscription du joueur,
						soumise à acceptation auprès de chaque joueur. <br><br>
						
						    

    
					</p>
					<p>
						<b>L'équipe de Nacridan</b>
					</p>
					<p>&nbsp;</p>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
