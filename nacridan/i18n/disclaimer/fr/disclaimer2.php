<html>
<head>
<meta http-equiv='Content-type' content='text/html; charset=UTF-8'>
<title>Nacridan</title>

<meta name='description' content='Jeu de rôle (JDR) en ligne où l' on
	incarne un Héros (guerrier, magicien, etc.) en tourpartour.'>
<meta name='keywords'
	content='Jeu, Rôle, RPG, JdR, pbem, jpem, héro, monstre, trésor, sortilège, sort, armure, arme, BM, jeux, php, gratuit, aventure, simulation, asynchrone'>
<meta name='robots' content='all'>
<meta name='revisit-after' content='1 week'>
<meta http-equiv='Content-Language' content='fr-FR'>
<meta name='author' content='Jails'>


<body text='#000000'>
	<table style='font-size: 12px;' border='0' cellpadding='0'
		cellspacing='5' width='100%'>
		<tbody>
			<tr>
				<td>
					<p align='center'>Charte d'Admission au Jeu : Nacridan</p>
					<p>
						<b>Nacridan </b> est un jeu gratuit mis à votre disposition par
						ses créateurs. La présente charte a pour but d'établir quelques
						règles de bonne conduite pour permettre à tous les participants de
						s'amuser pleinement. Il est indispensable que tout joueur respecte
						ces quelques règles pour l'intérêt de tous. Tout manquement ou
						tout abus fera l'objet de sanctions pouvant aller jusqu'à la
						suppression définitive du compte du joueur. <br> <br> <b>(1) <u>Inscription
								:</u></b> <br> La participation au jeu est entièrement gratuite
						et l'inscription est ouverte à tous les internautes disposant
						d'une adresse Email personnelle et active et d'un accès à Internet
						régulier. <br> Il n'y a pas a proprement parler d'age minimum
						imposé pour pouvoir jouer à Nacridan.<br> <br> <br> <b>(2) <u>Email
								:</u></b> <br> Par votre inscription au jeu, vous autorisez
						l'envoi d'Emails de la part du moteur et des gestionnaires du jeu
						ainsi que de la part des autres joueurs à l'adresse Email que vous
						aurez fournie lors de votre inscription. Il existe cependant des
						options permettant de désactiver l'envoi de la majeure partie de
						ces Emails. <br> Il est indispensable que le joueur dispose d'une
						adresse Email qui lui est propre et auquel il accède
						régulièrement, ce moyen pouvant être utilisé par les gestionnaires
						du jeu pour le contacter. <br> <br> <b>(3) <u>Données Personnelles
								:</u></b> <br> Vous pouvez à tout moment demander à consulter,
						modifier et/ou supprimer les informations qui vous concernent. Une
						copie complète des informations vous concernant en notre
						possession peut vous être communiquée sur simple demande. <br>
						Néanmoins, certaines informations enregistrées dans notre base de
						donnée devant impérativement rester confidentielles pour le bon
						fonctionnement du jeu, le joueur demandant la suppression ou une
						copie d'une partie ou de toutes les informations de son/ses
						personnage(s) ne pourra plus continuer de jouer avec ces derniers.
						<br> Toutes les données du jeu sont consultables par l'Equipe du
						jeu, sans restriction. <br> <br> <b>(4) <u>Saisies :</u></b> <br>
						Toute saisie qui sera jugée comme une atteinte à la morale
						entraînera l'élimination immédiate et sans préavis du compte du
						joueur; Ceci inclu, de manière non exhaustive, les Forums, le
						Chat, les Messages envoyés aux autres joueurs, les noms et
						descriptifs de personnages, d'ordre etc. Les Emails envoyés à
						d'autres joueurs et ayant trait au jeu seront également soumis à
						cette règle. <br> Il est également interdit d'y faire de la
						publicité pour un quelconque produit ou une quelconque marque. <br>
						<br> <b>(5) <u>Vente de comptes :</u></b> <br> La vente pour de
						l'argent ou des biens matériels d'un compte et/ou d'éléments du
						jeu est formellement interdite. <br> <br> <b>(6) <u>Délais
								d'inactivité :</u></b> <br> Les comptes non activés seront
						supprimés définitivement après 5 jours. Les comptes seront
						supprimés après 30 jours d'inactivité et en cas de non réponse
						après qu'un message d'avertissement ait été envoyé par email au
						joueur concerné (attente de maximum 10 jours). <br> <br> <b>(7) <u>Multi-Compte
								:</u></b> <br> Chaque joueur ne peut posséder qu'un seul et
						unique compte. Si un cas de multi-Compte (le fait de jouer
						plusieurs personnages grâce à l'utilisation de comptes différents)
						est décelé et confirmé, les comptes incriminés seront purement et
						simplement supprimés. <br> <br> <b>(8) <u>Sitting de Compte :</u></b>
						<br> Pour plus de convivialité, il est autorisé occasionnellement
						de jouer le(s) personnage(s) d'un ami s'il ne peut jouer lui même.
						L'occasionnel se transformant en habitude sera considéré comme du
						Multi-Compte : chaque joueur doit toujours rester seul maître de
						son/ses personnage(s). <br> <br> La consultation du compte d'un
						ami est tolérée sans limite si aucune action n'est réalisée
						(l'activation d'une DLA est considérée comme une action) <br> La
						communication du mot de passe d'un compte est sous la seule
						responsabilité du joueur. <br> <br> 1º/ Les Sitting de Compte sont
						autorisés de manière occasionnelle et ne peuvent en aucun cas
						excéder 8 jours par mois. <br> 2º/ Au dela de 7 jours consécutifs,
						le compte doit être mis en hiberner ou être abandonné jusqu'à ce
						que le joueur propriétaire du compte le reprenne en charge. <br>
						3°/ Les relais de "Multi-Compte" de 7 en 7 jours par des joueurs
						différents sont évidemment strictement interdits. <br> 4º/ Tout
						Sitting de Compte sortant du cadre des règles précédentes sera
						considéré comme abusif et passible de lourdes sanctions. <br> <br>
						<b>(9) <u>Expérience :</u></b> <br> Nacridan étant un jeu de
						groupe, le partage d'expérience et d'équipement doit se faire a
						peu près équitablement entre chaque personnage travaillant
						"ensemble". Le sacrifice de l'évolution d'un personnage pour
						accélérer la progression d'un autre, et plus généralement
						l'utilisation d'un compte au profit d'un autre, sera considéré
						comme du Multi-Compte. <br> <br> <br> <b>(10) <u>Messagerie
								interne </u></b><u>:</u> <br> Dans le cadre du jeu, une
						messagerie interne est mise à disposition de tous les joueurs.
						Cette messagerie est une composante du jeu et des interactions
						entre personnages, elle ne peut donc pas être détournée à des fins
						privées et personnelles en dehors du contexte du jeu. <br> <br> (<b>11)
							<u>Enquêteurs :</u>
						</b> <br> Les Enquêteurs sont les arbitres officiels du jeu. Toute
						situation litigieuse non mentionnée dans la charte sera laissée à
						leur jugement. Les Enquêteurs disposent du droit de sanctionner
						(voire de supprimer) n'importe quel personnage s'ils le jugent
						nécessaire. <br> Dans le cours d&#8217;une enquête, il est
						indispensable que les joueurs se montrent coopératifs et ne se
						laissent pas aller à la vulgarité et l&#8217;ironie grinçante,
						sous peine d&#8217;alourdissement de la sanction. <br> Lors d'une
						enquête, soyez toujours correct avec l'enqueteur : râleries,
						énervement et remontrances ne servent à rien si ce n'est à
						alourdir les sanctions. <br> En cas d'insulte à l'encontre de
						l'enquêteur, le(s) compte(s) incriminé(s) sera (seront) purement
						et simplement supprimé(s). <br> La complicité de Multi-Compte et
						le mensonge lors d&#8217;une enquête sont également passibles de
						lourdes sanctions. <br> <br> <b>(12) <u>Délation :</u></b> <br> La
						délation publique d'une infraction est proscrite et sera réprimée.
						<br> Les fondateurs d'ordres doivent être particulièrement
						attentifs sur ce point car leur collaboration dans une enquête
						pourra être requise. <br> Un fondateur d'ordre fermant
						volontairement les yeux sur une infraction à la charte risque les
						mêmes sanctions que les joueurs en infraction. <br> <br> <b>(13) <u>Automatisation
								:</u></b> <br> Il est formellement interdit d'automatiser les
						actions de son ou de ses joueurs par quelque moyen que ce soit
						(script, programme, site web, etc.). Ceci inclut également
						l'interdiction d'activer automatiquement le calcul d'un nouveau
						tour de jeu (Activation de DLA). <br> <br> <b>(14) <u>Bugs :</u></b>
						<br> Le jeu n'est pas à l'abri de bugs et incohérences. Chaque
						joueur est tenu de rapporter (par mail ou sur le forum ad hoc) au
						plus vite tout problème décelé, et toute utilisation abusive ou
						volontaire d'un bug ou d'une faille se verra sanctionnée et pourra
						se solder par la suppression du compte du joueur concerné. <br> De
						plus, aucune compensation ne sera jamais accordée en cas de joueur
						lésé par un bug ou par un autre joueur ayant triché. <br> <br> <b>(15)
							<u>Charte d'admission :</u>
						</b> <br> La présente charte doit être lue et acceptée dans son
						intégralité pour pouvoir jouer à Nacridan. <br> Elle est
						susceptible d'être modifiée par les gestionnaires du jeu. Le cas
						échéant, il sera demandé aux joueurs d'accepter explicitement les
						modifications apportées pour pouvoir continuer à jouer. <br>
					</p>
					<p>
						<b>L'Équipe de Nacridan</b>
					</p>
					<p>&nbsp;</p>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
