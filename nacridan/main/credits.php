<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/indextemp.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");



$db = DB::getDB();



$str = "<div align=center style='color: #EEEEEE;'> L'équipe de Nacridan 1: <br/><br/> <table class='creditswidth' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Nacridan";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Programmation";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "JC";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Webdesign";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "</table></div><br/><br/><br/><br/>\n";

$str .= "<div align=center style='color: #EEEEEE;'> L'équipe active de Nacridan 2: <br/><br/> <table class='creditswidth' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Aé Li";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Gestion de projet, gestion de serveur, programmation, webdesign";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858; ' href='http://fr.linkedin.com/in/0elie0assemat0physics0'> Profil LinkedIn</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Lord Thergal";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Coeur du code, construction et équilibre du game-play";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #585858; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

// $str.="<a style='color: #999999; ' href='http://fr.linkedin.com/in/0elie0assemat0physics0'>profil LinkedIn</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Belette";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Directrice artistique, illustrations, pixel-art";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='https://www.facebook.com/pages/Arbelette/147580675330975'>Page facebook</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Nanik";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Mise en forme des Règles du Jeu";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Lotharim";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Programmation";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Klaus Barbiche";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Administration serveur";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";


$str .= "</table></div><br/><br/>\n";

/*
 * $str.="<div align=center style='color: #EEEEEE;'> L'équipe du forum: <br/><br/> <table class='creditswidth' >\n";
 *
 * $str.="<tr ><td valign='top' width='120px' style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
 * $str.="Cancrelat, Rinoks, Ewakhine Do'varden";
 * $str.="</td>\n";
 * $str.="<td style='color: #999999; '>\n";
 * $str.="Animateurs IG";
 * $str.="</td></tr>\n";
 *
 * $str.="</table></div><br/><br/>\n";
 */

$str .= "<div align=center style='color: #EEEEEE;'> L'équipe du forum: <br/><br/> <table class='creditswidth' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Cancrelat";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Administrateur";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Ambrune, Anraud et gerfa";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Modérateurs - Forum principal (RolePlay)";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Felgar et Rinoks";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Modérateurs - Forum Développement";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";

$str .= "</table></div>\n";

$str .= "<div align=center style='color: #EEEEEE;'> Les anciens modérateurs du forum: <br/><br/> <table class='creditswidth' >\n";
$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Jorel, RomulF, Ange Blood";
$str .= "</td>\n";

$str .= "</table></div>\n";

// ---------------------- ANCIENS

$str .= "<br/><br/> <div align=center style='color: #EEEEEE;'> Remerciements spéciaux : <br/><br/> <table width='550px' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Adrian von Ziegler";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Un grand merci pour avoir accepté que Nacridan utilise ses compositions";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='https://www.youtube.com/user/AdrianvonZiegler'>Profil Youtube</a>";
$str .= "</td></tr>\n";

$str .= "</table></div>\n";

$str .= "<br/><br/> <div align=center style='color: #EEEEEE;'> Membres de l'équipe anciennement actifs : <br/><br/> <table width='550px' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "DeKey";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Webdesign";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://dekey-s.deviantart.com/'>DevianArt</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Séverin";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Programmation";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Anraud";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Rédaction des textes";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #585858; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #999999; ' href='http://rivesaintlaurent.wordpress.com/'>Blog</a> et <a style='color: #999999; 'href='http://apostrophe.paliens.org/'>Site de journalisme amateur</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Tyrion";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Pixel-Art";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Sjoelie";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustrations";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://sjoelie.zyzomis.com/'>Blog</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Gromy";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://gromy-blog.blogspot.fr'>Blog</a> et <a style='color: #585858;' href='http://legromy.com'>Book</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Drou";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustrations";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://drou.artblog.fr/'>Blog</a> et <a style='color: #585858;' href='http://www.facebook.com/pages/drou/145546015536688'>Facebook<a/>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Aino Wallen";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Webdesign";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://ainowallen.deviantart.com/'>DevianArt</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Berien";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustrations";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://my.opera.com/Berien/blog/'>Blog</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Moonqueen";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustrations";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://www.moonqueen.fr/'>Site</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Dilvich";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Correction des textes, mise en forme.";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #585858; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Llyn";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Rédaction des textes";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #585858; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Sparadra";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Ajout de modules, programmation";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Ewakhine Do'varden";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Développement de module, programmation";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://klaewyss.forumactif.org/'>Forum JdR/RP</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Myscia";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Mise en forme des Règles du Jeu";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "El Zvountz";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustrations";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Kurien";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustrations";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "</table></div>\n";

// ------------------------ CONTRIB

$str .= "<br/><br/> <div align=center style='color: #EEEEEE;'> Avec les contributions de : <br/><br/> <table width='550px' >\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "FabJ";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Conseils, programmation";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "ISa";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Correction des textes.";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #585858; border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Adrien";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Infographie";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Emdjii";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Infographie";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";

$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Bob";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Pixel-Art";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://bob-leblog.blogspot.fr/'>Blog</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Kuro2Kazu";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://kuro2kazu.deviantart.com/'>DevianArt</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Flo";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://l-archi.deviantart.com/'>DevianArt</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Terazed";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://terazed.deviantart.com/'>DevianArt</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Kraaor";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://forum-dessin-peintur.graphforum.com/'>Forum</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Angelfaer";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://forum-dessin-peintur.graphforum.com/'>Forum</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Gaby";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://forum-dessin-peintur.graphforum.com/'>Forum</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Oxalis";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://forum-dessin-peintur.graphforum.com/'>Forum</a>";
$str .= "</td></tr>\n";

$str .= "<tr ><td valign='top' width='120px'  style='color: #EEEEEE; font-weight: bold;' rowspan=2>\n";
$str .= "Moufmouf";
$str .= "</td>\n";
$str .= "<td style='color: #999999; '>\n";
$str .= "Illustration";
$str .= "</td></tr>\n";
$str .= "<tr ><td style='color: #999999;border-bottom: 1px solid; border-color:black black #2A120A black; '>\n";
$str .= "<a style='color: #585858;' href='http://nefeldta.deviantart.com/'>DevianArt</a>";
$str .= "</td></tr>\n";

$str .= "</table><br/><br/> Et peut-être un jour, vous-même ? ... ;-) <br/><br/>  </div>\n";
$str .= "<div style='color: #999999; '> Un oubli ? une mise-à-jour ? ... passez dans le forum pour le signaler. </div>\n";

$str .= "<br><br> <br><br> \n";
$str .= "<div style='color: #999999; '>Enfin, Nacridan 2 a fortement été inspiré par les projets  <b>Mountyhall</b>, <b>Kraland</b> et <b>Battle for Wesnoth</b>.<br> De plus, une partie des graphismes de Battle for Wesnoth sont utilisés par Nacridan. Cette part va diminuer au fur et à mesure des travaux de nos graphistes. Actuellement elle concerne la plupart des bâtiments et les gardes des royaumes. Tous les monstres du bestiaires viennent déjà du projet Nacridan. Toute aide est la bienvenue pour accélérer ce processus.</div>\n";

$BOTTOMTitle->add("Un grand merci à :");
$BOTTOMMainArea->add($str);


$MAIN_PAGE->render();

?>
