<?php

class CQMMenu extends HTMLObject
{

    public $nacridan;

    public $lang;

    public $db;

    public $curplayer;

    public function CQMMenu($nacridan, $db, $lang)
    {
        $this->nacridan = $nacridan;
        $this->lang = $lang;
        $this->db = $db;
        $this->curplayer = $this->nacridan->loadCurSessPlayer($db);
    }

    public function toString()
    {
        // echo $this->lang;
        $db = $this->db;
        $curplayer = $this->curplayer;
        
        $str = "<div class='mmenu'>";
        $str .= "<a class='cq' href='../conquest/conquest.php'></a>";
        $str .= "<a class='di' href='../diplomacy/diplomacy.php'></a>";
        $str .= "<a class='ec' href='../economy/economy.php?view=1'></a>";
        
        // Messages
        $dbm = new DBCollection("SELECT Mail.id FROM Mail WHERE Mail.new=1 AND id_Player\$receiver =" . $curplayer->get("id"), $db);
        $cpt = $dbm->count();
        $dbn = new DBCollection("SELECT MailAlert.id FROM MailAlert WHERE MailAlert.new=1 AND id_Player\$receiver =" . $curplayer->get("id"), $db);
        $cpt += $dbn->count();
        $dbn = new DBCollection("SELECT MailTrade.id FROM MailTrade WHERE MailTrade.new=1 AND id_Player\$receiver =" . $curplayer->get("id"), $db);
        $cpt += $dbn->count();
        
        if ($cpt > 0) {
            $str .= "<a class='mgnnlu' href='../conquest/conquest.php?center=mail'></a>";
        } else {
            $str .= "<a class='mg' href='../conquest/conquest.php?center=mail'></a>";
        }
        
        $str .= "<a class='ru' href='../i18n/rules/" . $this->lang . "/rules.php' onclick='window.open(this.href); return false;'></a>";
        $str .= "<a class='op' href='../conquest/conquest.php?center=option'></a>";
        $str .= "<a class='ma' href='../conquest/conquest.php?center=map'></a>";
        $str .= "<a class='vi' href='../conquest/conquest.php?center=view'></a>";
        $str .= "<a class='vi1d' href='../conquest/conquest.php?center=view1d'></a>";
        $str .= "<a class='vi2d' href='../conquest/conquest.php?center=view2d&bottom=viewpanel'></a>";
        $str .= "<a class='eq' href='../conquest/conquest.php?center=equip'></a>";
        $str .= "<a class='ev' href='../conquest/conquest.php?center=event'></a>";
        $str .= "<a class='pr' href='../conquest/conquest.php?center=profile'></a>";
        $str .= "<a class='ac' href='../conquest/conquest.php?center=account'></a>";
        $str .= "<a class='ki' href='../kingdom/kingdom.php'></a>";
        if (isset($_SESSION['forum_connected']))
            $str .= "<a class='fo' href='" . CONFIG_HOST . "/forum/index.php' onclick='window.open(this.href); return false;'></a>";
        else
            $str .= "<a class='fo' href='" . CONFIG_HOST . "/forum/login.php?action=autoco' onclick='window.open(this.href); return false;'></a>";

        // Tribune
        require_once (HOMEPATH . "/diplomacy/dtribune.inc.php");
        $tribune = new DTribune($this->nacridan, $this->db);

        // Mise en évidence des messages de tribune non-lus
        $infos = $tribune->getUnreadMessagesInfos();

        if ($infos['success'] && $infos['id_FighterGroup'] != 0) {
            if ($infos['unreads'] > 0)
                $str .= "<a class='trnewmsg' href='../diplomacy/diplomacy.php?center=tribune'><span class='messageCount'>".$infos['unreads']."</span></a>";
            else
                $str .= "<a class='tr' href='../diplomacy/diplomacy.php?center=tribune'></a>";
        } else
            $str .= "<a class='tr' href='../diplomacy/diplomacy.php?center=notribune'></a>";
        
        $str .= "<a class='irc' href='../conquest/conquest.php?center=chanirc'></a>";
        $str .= "<a class='faq' href='../conquest/conquest.php?center=faq'></a>";
        
        $str .= "</div>";
        return $str;
    }
}
?>
