<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/indextemp.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

$db = DB::getDB();




$idList = "102"; // Chauve-souris
$idList .= " OR id=103"; // Kobold
$idList .= " OR id=108"; // Homme lézard
$idList .= " OR id=120"; // Gobelin Archer
$idList .= " OR id=124"; // Golem de chair
$idList .= " OR id=129"; // Horreur des sous-bois
$idList .= " OR id=130"; // Fée des bois
$idList .= " OR id=136"; // Combattant squelette
$idList .= " OR id=155"; // Sauteur fauchard
$idList .= " OR id=167"; // Kraken Terrestre
$idList .= " OR id=263"; // Tortue Géante

$dbm = new DBCollection("SELECT * FROM BasicRace WHERE id=".$idList, $db);


$str = "";
while (! $dbm->eof()) {
    
    $str .= "<div class='beastsample'>";
    $str .= "<b>" . $dbm->get("name") . "</b><br><br>";
    if ($dbm->get("pic") != "") {
        $str .= '<img style="float: right; margin-left: 30px" src="' . CONFIG_HOST . '/pics/character/' . $dbm->get("pic") . '" alt="" />';
    }
    
    $str .= bbcode($dbm->get("description"));
    $str .= "</div><br><br>";
    $str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";
    $dbm->next();
}
$BOTTOMMainArea->add($str);
$BOTTOMTitle->add("Quelques extraits du bestiaire");





$MAIN_PAGE->render();

?>
