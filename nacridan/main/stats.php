<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
        "sess" => "Session"
));

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

function getFullRankData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.level,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "'  AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY level DESC,xp DESC " . $limit, $db);
    
    $i = 0;
    $array = array();
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        $dbs1->get("level"),
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullMerchantData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.merchant_level,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0  LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY merchant_level DESC" . $limit, $db);
    
    $i = 0;
    $array = array();
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        floor($dbs1->get("merchant_level")),
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getRichessData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $query = "";
    $query .= "SELECT Player.id,Player.name,Player.racename,(Player.money+Player.moneyBank) as moneyP FROM Player  INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 ";
    $query .= " WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY moneyP DESC";
    
    $dbs1 = new DBCollection($query . $limit, $db);
    
    $i = 0;
    $array = array();
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        floor($dbs1->get("moneyP")),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullRichessData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $query = "";
    $query .= "SELECT Player.id,Player.name,Player.racename,(Player.money+Player.moneyBank+ifnull(totalPriceEquipment,0)) as moneyP,Player.gender,Team.id AS teamid,Team.name AS teamname ";
    $query .= "FROM Player  INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT outer JOIN Team ON Team.id=Player.id_Team ";
    $query .= "left outer join (";
    $query .= "	select price.playerId,sum(priceEquipement) as totalPriceEquipment";
    $query .= "	from (";
    $query .= "		select eq.playerId, eq.eqId, eq.frequency, eq.level, case eq.level when 0 then 0 else floor(eq.frequency * pow((3/2),(eq.level-1)) + pow((3/2),eq.level)) end + ";
    $query .= "		case minor_level when 0 then 0 else floor(50 * pow((3/2),(minor_level-1)) + pow((3/2),minor_level)) end + ";
    $query .= "		case major_level when 0 then 0 else floor(100 * pow((3/2),(major_level-1)) + pow((3/2),major_level))  end as priceEquipement";
    $query .= "		from(";
    $query .= "            SELECT Player.id as playerId,Equipment.id as eqId,BasicEquipment.frequency, Equipment.level,";
    $query .= "			max(CASE Template.pos WHEN 1 THEN Template.level ELSE 0 END) as minor_level,";
    $query .= "			max(CASE Template.pos WHEN 2 THEN Template.level ELSE 0 END) as major_level";
    $query .= "			FROM Player left outer join Equipment on Equipment.id_player = Player.id";
    $query .= "			LEFT outer JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id";
    $query .= "			left outer join Template on Template.id_Equipment = Equipment.id";
    $query .= "			left outer join BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate";
    $query .= "			where Player.status='PC' AND Player.authlevel<2  AND Player.disabled!=99 and Equipment.id_player > 0 and Equipment.level >0";
    $query .= "			group by Player.id,Equipment.id,BasicEquipment.frequency,Equipment.level";
    $query .= "		) eq ";
    $query .= "	) price ";
    $query .= "	group by price.playerId";
    $query .= ") price on price.playerId = Player.id ";
    $query .= " WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY moneyP DESC";
    
    $dbs1 = new DBCollection($query . $limit, $db);
    
    $i = 0;
    $array = array();
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        floor($dbs1->get("moneyP")),
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullKillDataRatio ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.nbkill/((UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(Player.creation))/(3600*24)) as nbkill,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player  INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY nbkill DESC,(totalip+xp) DESC " . $limit, $db);
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        $dbs1->get("nbkill") * 100,
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullKillData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.nbkill as nbkill,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY nbkill DESC,(totalip+xp) DESC " . $limit, $db);
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        $dbs1->get("nbkill"),
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullDeathData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.nbdeath/((UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(Player.creation))/(3600*24)) as nbdeath ,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2 AND  Player.disabled!=99 AND status='PC' ORDER BY nbdeath ASC,level DESC,(totalip+xp) DESC " . $limit, $db);
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        $dbs1->get("nbdeath") * 100,
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullPKDataRatio ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.nbkillpc/((UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(Player.creation))/(3600*24)) as nbkillpc,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY nbkillpc DESC,level DESC,(totalip+xp) DESC " . $limit, $db);
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        $dbs1->get("nbkillpc") * 100,
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getFullPKData ($db, $min = null, $max = null)
{
    $limit = "";
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Player.id,Player.name,Player.racename,Player.nbkillpc as nbkillpc,Player.gender,Team.id AS teamid,Team.name AS teamname FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 LEFT JOIN Team ON Team.id=Player.id_Team WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND status='PC' ORDER BY nbkillpc DESC,level DESC,(totalip+xp) DESC " . $limit, $db);
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a><span class='stats_body_id'>(" . $dbs1->get("id") . ")</span>",
                        "class='stats_body' align='left'"
                ),
                "2" => array(
                        $dbs1->get("racename"),
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        $dbs1->get("nbkillpc"),
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("teamid") . "' class='stylemainpc popupify'>" . $dbs1->get("teamname") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "5" => array(
                        "&nbsp;" . $dbs1->get("gender"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getRaceData ($db)
{
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    $dbs1 = new DBCollection("SELECT count(Player.id) as nbhuman FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND id_BasicRace=1 AND status='PC'", $db);
    $dbs2 = new DBCollection("SELECT count(Player.id) as nbelf FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND id_BasicRace=2 AND status='PC'", $db);
    $dbs3 = new DBCollection("SELECT count(Player.id) as nbdwarf FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND id_BasicRace=3 AND status='PC'", $db);
    $dbs4 = new DBCollection("SELECT count(Player.id) as nbdorane FROM Player INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE playatb > '" . $date . "' AND Player.id_member!=0 AND Player.authlevel<2  AND Player.disabled!=99 AND id_BasicRace=4 AND status='PC'", $db);
    
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[0] = array(
                "0" => array(
                        "Humain",
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        $dbs1->get("nbhuman"),
                        "class='stats_body' align='center'"
                )
        );
        $array[1] = array(
                "0" => array(
                        "Elfe",
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        $dbs2->get("nbelf"),
                        "class='stats_body' align='center'"
                )
        );
        $array[2] = array(
                "0" => array(
                        "Nain",
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        $dbs3->get("nbdwarf"),
                        "class='stats_body' align='center'"
                )
        );
        $array[3] = array(
                "0" => array(
                        "Dorane",
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        $dbs4->get("nbdorane"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getTeamData ($db, $min = null, $max = null)
{
    $limit = "";
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    $format2 = "Y-m-d H:i:s";
    $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
    $date = date($format2, $time);
    // $dbs1=new DBCollection("SELECT name,Team.id,count(TeamRank.id) as nb FROM TeamRank LEFT JOIN Team ON Team.id=id_Team WHERE 1 GROUP BY id_Team ORDER BY nb DESC ".$limit,$db);
    $dbs1 = new DBCollection("SELECT Team.name,Team.id,count(Player.id) as nb FROM Team INNER JOIN Player ON Team.id=Player.id_Team INNER JOIN Member on Player.id_member = Member.id and Member.authlevel < 2 and Player.id_World > 0 WHERE playatb > '" . $date . "'  GROUP BY id_Team ORDER BY nb DESC " . $limit, $db);
    $array = array();
    $i = 0;
    while (! $dbs1->eof()) {
        $i ++;
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='alliegance.php?id=" . $dbs1->get("id") . "' class='stylemainpc popupify'>" . $dbs1->get("name") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "2" => array(
                        $dbs1->get("nb"),
                        "class='stats_body' align='center'"
                )
        );
        
        $dbs1->next();
    }
    return $array;
}

function getQuestData ($db, $min = null, $max = null)
{
    $limit = "";
    
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $dbs1 = new DBCollection("SELECT Quest.id,Quest.name,Quest.nbMission,Quest.Player_levelMin,Quest.Player_levelMax FROM Quest WHERE In_List=1" . $limit, $db);
    
    $i = 0;
    $array = array();
    $arraytemp = array(); // Débutant power :P
    
    while (! $dbs1->eof()) {
        $i ++;
        $test = array_search(array(
                $dbs1->get("name"),
                $dbs1->get("Player_levelMax")
        ), $arraytemp);
        if ($test === false) {
            $array[] = array(
                    "0" => array(
                            $dbs1->get("id"),
                            "class='stats_body' align='left'"
                    ),
                    "1" => array(
                            $dbs1->get("name"),
                            "class='stats_body' align='center'"
                    ),
                    "2" => array(
                            $dbs1->get("nbMission"),
                            "class='stats_body' align='center'"
                    ),
                    "3" => array(
                            $dbs1->get("Player_levelMin"),
                            "class='stats_body' align='center'"
                    ),
                    "4" => array(
                            $dbs1->get("Player_levelMax"),
                            "class='stats_body' align='center'"
                    ),
                    "5" => array(
                            1,
                            "class='stats_body' align='center'"
                    )
            );
            $arraytemp[] = array(
                    $dbs1->get("name"),
                    $dbs1->get("Player_levelMax")
            );
        } else {
            $array[$test]["5"][0] = $array[$test]["5"][0] + 1;
        }
        
        $dbs1->next();
    }
    return $array;
}

function getCityData ($db, $min = null, $max = null)
{
    $limit = "";
    
    if (! is_null($min) && ! is_null($max)) {
        $limit = " limit " . $min . "," . $max;
    }
    
    $query = " SELECT City.x, City.y, City.id AS idC, Player.name AS nameP, City.name as nameC, Player.id as idP, developmentPrice ";
    $query .= " from  ";
    $query .= " ( ";
    $query .= " 	select step_2.id_City, sum(cumulative_price) as developmentPrice  ";
    $query .= " 	from ";
    $query .= " 	( ";
    $query .= " 		select step_1.id_City, step_1.id, step_1.money, step_1.level,step_1.buildingPrice,step_1.building_name, ";
    $query .= " 		(case step_1.basicBuildingId ";
    $query .= " 		when 8 then 150  ";
    $query .= " 		when 11 then 2000  ";
    $query .= " 		else 0 end)+ ";
    $query .= " 		(case step_1.level -1 ";
    $query .= " 		when 0 then floor(step_1.buildingPrice) ";
    $query .= " 		when 1 then floor(step_1.buildingPrice * pow(3/2,1)) ";
    $query .= " 		when 2 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) ";
    $query .= " 		when 3 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) ";
    $query .= " 		when 4 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) ";
    $query .= " 		when 5 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) ";
    $query .= " 		when 6 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) ";
    $query .= " 		when 7 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) ";
    $query .= " 		when 8 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) ";
    $query .= " 		when 9 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) ";
    $query .= " 		when 10 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) + floor(step_1.buildingPrice * pow(3/2,10)) ";
    $query .= " 		when 11 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) + floor(step_1.buildingPrice * pow(3/2,10)) + floor(step_1.buildingPrice * pow(3/2,11)) ";
    $query .= " 		when 12 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) + floor(step_1.buildingPrice * pow(3/2,10)) + floor(step_1.buildingPrice * pow(3/2,11)) + floor(step_1.buildingPrice * pow(3/2,12)) ";
    $query .= " 		when 13 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) + floor(step_1.buildingPrice * pow(3/2,10)) + floor(step_1.buildingPrice * pow(3/2,11)) + floor(step_1.buildingPrice * pow(3/2,12)) + floor(step_1.buildingPrice * pow(3/2,13)) ";
    $query .= " 		when 14 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) + floor(step_1.buildingPrice * pow(3/2,10)) + floor(step_1.buildingPrice * pow(3/2,11)) + floor(step_1.buildingPrice * pow(3/2,12)) + floor(step_1.buildingPrice * pow(3/2,13)) + floor(step_1.buildingPrice * pow(3/2,14)) ";
    $query .= " 		when 15 then floor(step_1.buildingPrice * pow(3/2,1)) + floor(step_1.buildingPrice * pow(3/2,2)) + floor(step_1.buildingPrice * pow(3/2,3)) + floor(step_1.buildingPrice * pow(3/2,4)) + floor(step_1.buildingPrice * pow(3/2,5)) + floor(step_1.buildingPrice * pow(3/2,6)) + floor(step_1.buildingPrice * pow(3/2,7)) + floor(step_1.buildingPrice * pow(3/2,8)) + floor(step_1.buildingPrice * pow(3/2,9)) + floor(step_1.buildingPrice * pow(3/2,10)) + floor(step_1.buildingPrice * pow(3/2,11)) + floor(step_1.buildingPrice * pow(3/2,12)) + floor(step_1.buildingPrice * pow(3/2,13)) + floor(step_1.buildingPrice * pow(3/2,14)) + floor(step_1.buildingPrice * pow(3/2,15))end) as cumulative_price ";
    $query .= " 		from ( ";
    $query .= " 			select City.id as id_City,  Building.id, Building.money, Building.level,BasicBuilding.id as basicBuildingId, ";
    $query .= " 			case BasicBuilding.id when 16 then BasicBuilding.price else BasicBuilding.price end as buildingPrice,BasicBuilding.name as building_name ";
    $query .= " 			from City ";
    $query .= " 			left outer join Building on Building.id_City = City.id ";
    $query .= " 			left outer join BasicBuilding on Building.id_BasicBuilding = BasicBuilding.id ";
    $query .= " 			where City.id_Player > 0 ";
    $query .= " 		)step_1 ";
    $query .= " 	) step_2 ";
    $query .= " 	group by step_2.id_City  ";
    $query .= " )step_3  ";
    $query .= " left outer join City on step_3.id_City = City.id ";
    $query .= " LEFT outer JOIN Player ON City.id_Player=Player.id  ";
    $query .= " WHERE City.captured>3  ";
    $query .= " order by developmentPrice desc ";
    
    $dbs1 = new DBCollection($query . $limit, $db);
    
    $i = 0;
    $array = array();
    
    while (! $dbs1->eof()) {
        $i ++;
        $place = $dbs1->get("x") . "-" . $dbs1->get("y");
        $array[] = array(
                "0" => array(
                        $i,
                        "class='stats_body1' align='center'"
                ),
                "1" => array(
                        "&nbsp;<a href='profilecity.php?id=" . $dbs1->get("idC") . "' class='stylemainpc popupify'>" . $dbs1->get("nameC") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "2" => array(
                        $place,
                        "class='stats_body' align='center'"
                ),
                "3" => array(
                        "&nbsp;<a href='profile.php?id=" . $dbs1->get("idP") . "' class='stylemainpc popupify'>" . $dbs1->get("nameP") . "</a>",
                        "class='stats_body' align='center'"
                ),
                "4" => array(
                        $dbs1->get("developmentPrice"),
                        "class='stats_body' align='center'"
                )
        );
        $dbs1->next();
    }
    return $array;
}

/**
 * ***************************************************
 */
/**
 * ***************** END OF FUNCTIONS ***************
 */
/**
 * ***************************************************
 */

$db = DB::getDB();

if (isset($_GET["param"])) {
    $param = $_GET["param"];
} else {
    $param = "all";
}

$cache = new TCache_Lite(array(
        "cacheDir" => "/tmp/nacridan/stats/",
        "lifeTime" => "3600"
));

$BOTTOMTitle->add("<div class=\"stats_div_title\">Statistiques de l'île de Nacridan</div>");

switch ($param) {
    case "expe":
        $array = getFullRankData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Les plus Expérimentés",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Niveau"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "kill":
        $array = getFullKillData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Ceux qui ont commis le plus de Meurtres",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Nb de Meurtres"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "killratio":
        $array = getFullKillDataRatio($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Pourcentage de Meurtres par jour",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("% de Meurtres"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "death":
        $array = getFullDeathData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Ceux qui sont Morts le moins de fois",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Taux de Décès"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "pk":
        $array = getFullPKData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Ceux qui ont tué le plus de Joueurs",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Nb de Meurtres"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "pkratio":
        $array = getFullPKDataRatio($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Pourcentage de Joueurs tués par jour",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("% de Meurtres"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "team":
        $array = getTeamData($db);
        
        $table1 = createTable(3, $array, array(
                array(
                        "Les Ordres",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=300"
                ),
                array(
                        localize("Nb Membres"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    
    case "merchant":
        $array = getFullMerchantData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Les Caravaniers les plus réputés",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Réputation"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    
    case "money":
        $array = getFullRichessData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Les personnages les plus riches",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Race"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Richesse en équivalent PO"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Allégeance"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Sexe"),
                                "class='stats_label' align='center' width=40"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    case "cities":
        $array = getCityData($db);
        
        $table1 = createTable(5, $array, array(
                array(
                        "Les Villages contrôlés",
                        "class='stats_title' colspan=5 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1'  align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Position"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Gouverneur"),
                                "class='stats_label' align='center' width=80"
                        ),
                        array(
                                localize("Coûts en POs développement"),
                                "class='stats_label' align='center' width=120"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        
        break;
    case "quest":
        $array = getQuestData($db);
        $table1 = createTable(6, $array, array(
                array(
                        "Liste des quêtes disponibles",
                        "class='stats_title' colspan=6 align='center'"
                )
        ), 
                array(
                        array(
                                localize("Id"),
                                "class='stats_label' align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=220"
                        ),
                        array(
                                localize("Nombre de missions"),
                                "class='stats_label' align='center' width=40"
                        ),
                        array(
                                localize("Niveau minimum"),
                                "class='stats_label' align='center' width=55"
                        ),
                        array(
                                localize("Niveau maximum"),
                                "class='stats_label' align='center' width=50"
                        ),
                        array(
                                localize("Nombre d'exemplaires"),
                                "class='stats_label' align='center' width=50"
                        )
                ), "class='stats_table'", "", "");
        $BOTTOMTitle->add("<br/><a href='" . CONFIG_HOST . "/main/stats.php' class='statsevent'>&lt;&lt;&lt; Retour</a><br/>");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        break;
    default:
        $array = getFullRankData($db, 0, 20);
        
        $BOTTOMTitle->add("<div class=\"stats_div_conteneur\">");
        
        $table1 = createTable(4, $array, array(
                array(
                        "Les plus Expérimentés",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1' align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("Niveau"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=expe' class='statsevent'>&gt;&gt;&gt; En savoir plus</a>");
        $BOTTOMTitle->add("</div>");
        
        $array = getFullDeathData($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Ceux qui sont Morts le moins de fois",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("% de Décès"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=death' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        $BOTTOMTitle->add("<div class=\"stats_div_conteneur\">");
        
        $array = getFullKillData($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Ceux qui ont commis le plus de Meurtres",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("Nb de Meurtres"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=kill' class='statsevent'>&gt;&gt;&gt; En savoir plus</a>");
        $BOTTOMTitle->add("</div>");
        
        $array = getFullKillDataRatio($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Pourcentage de Meurtres par jour",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("% de Meurtres"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=killratio' class='statsevent'>&gt;&gt;&gt; En savoir plus</a>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        $BOTTOMTitle->add("<div class=\"stats_div_conteneur\">");
        
        $array = getFullPKData($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Ceux qui ont tué le plus de Joueurs",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("Nb de Meurtres"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div  class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=pk' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        
        $array = getFullPKDataRatio($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Pourcentage de Joueurs tués par jour",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("% de Meurtres"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div  class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=pkratio' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        $BOTTOMTitle->add("<div class=\"stats_div_conteneur\">");
        
        $array = getFullMerchantData($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Les caravaniers les plus réputés",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1' align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("Réputation"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=merchant' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        
        $array = getRichessData($db, 0, 20);
        
        $table1 = createTable(4, $array, array(
                array(
                        "Les personnages les plus riches",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1' align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=100"
                ),
                array(
                        localize("Race"),
                        "class='stats_label' align='center' width=55"
                ),
                array(
                        localize("Richesse en PO"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div  class=\"stat_floating_div\">");
        $BOTTOMTitle->add($table1);
        $remark = "<span class='statsevent'>Ce tableau affiche seulement les PO, <br> pour la richesse totale cliquez</span>";
        $remark .= "<a href='" . CONFIG_HOST . "/main/stats.php?param=money' class='statsevent'>&gt;&gt;&gt;  ici.</a><br>";
        $remark .= " <span class='statsevent'>Attention l'affichage de cette page <br>peut prendre du temps!</span><br/><br/>";
        $BOTTOMTitle->add($remark);
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("</div>");
        $BOTTOMTitle->add("<div style='clear:both' class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>");
        
        $array = getCityData($db, 0, 20);
        
        $table1 = createTable(5, $array, array(
                array(
                        "Les Villages contrôlés",
                        "class='stats_title' colspan=5 align='center'"
                )
        ), 
                array(
                        array(
                                "#",
                                "class='stats_label1'  align='center' width=20"
                        ),
                        array(
                                localize("Nom"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Position"),
                                "class='stats_label' align='center' width=150"
                        ),
                        array(
                                localize("Gouverneur"),
                                "class='stats_label' align='center' width=80"
                        ),
                        array(
                                localize("Coûts en POs développement"),
                                "class='stats_label' align='center' width=120"
                        )
                ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div >");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=cities' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        
        $BOTTOMTitle->add("<div style='clear:both'></div>");
        $array = getTeamData($db, 0, 20);
        
        $table1 = createTable(3, $array, array(
                array(
                        "Les Ordres les plus populaires",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "#",
                        "class='stats_label1'  align='center' width=20"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=150"
                ),
                array(
                        localize("Membres actifs"),
                        "class='stats_label' align='center' width=80"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div >");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=team' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        
        $BOTTOMTitle->add("<div style='clear:both'></div>");
        
        $array = getRaceData($db, 0, 20);
        
        $table1 = createTable(2, $array, array(
                array(
                        "Les Races",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        "Nom",
                        "class='stats_label1'  align='center' width=100"
                ),
                array(
                        "Nombre",
                        "class='stats_label' align='center' width=100"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div >" . $table1 . "<br/><br/></div>");
        
        $array = getQuestData($db, 0, 20);
        
        $table1 = createTable(3, $array, array(
                array(
                        "Liste des Quêtes disponibles",
                        "class='stats_title' colspan=4 align='center'"
                )
        ), array(
                array(
                        localize("Id"),
                        "class='stats_label1' align='center' width=30"
                ),
                array(
                        localize("Nom"),
                        "class='stats_label' align='center' width=80"
                ),
                array(
                        localize("Nombre de missions"),
                        "class='stats_label' align='center' width=30"
                )
        ), "class='stats_table'", "", "");
        
        $BOTTOMTitle->add("<div >");
        $BOTTOMTitle->add($table1);
        $BOTTOMTitle->add("<a href='" . CONFIG_HOST . "/main/stats.php?param=quest' class='statsevent'>&gt;&gt;&gt; En savoir plus</a><br/><br/>");
        $BOTTOMTitle->add("</div>");
        break;
}

$MAIN_PAGE->render();

?>
