<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/indextemp.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");


$db = DB::getDB();



$str = "<div class='about'><div  class='about-title'><b> Le contexte</b><br/><br/></div>";
$str .= "<div class='about-main'>A l'origine nacridan.com appartenait à la société CrysaLEAD, cofondée par Simon Jaillet, le développeur originel de Nacridan. Le projet étant sévèrement déficitaire, il a été abandonné par CrysaLEAD et a été placé sous licence <a href='../license/GNU-GPL-license.pdf'>GNU GPL</a>. De janvier 2009 à février 2011, Aé Li a pris en charge la quasi-totalité des frais du projet. Sur cette période les développeurs ont principalement enrichi la version 1 du jeu. <br>";

$str .= " L'équipe a ensuite décidé de se lancer dans le développement d'une version 2, et l'un des objectifs était d'avoir un projet autonome financièrement. Nous avons commencé par créer une association loi 1901 début 2011 pour gérer les faibles flux financiers liés au projet.<br><br>";
$str .= " </div><br>\n";

$str .= "<div  class='about-title'><b> Objectifs</b><br/><br/></div>";
$str .= "<div class='about-main'>";
$str .= "L'association a d'abord pour but le développement du jeu Nacridan, pour le plaisir des joueurs. Mais c'est un peu plus que ça, comme le précise l'article 2 des statuts de l'association Nacridan : <br><i>Cette association a pour but la gestion et le développement du site internet
www.nacridan.com
. 
A travers ce site, l'association favorisera la diffusion du Jeu de Rôle en
tant que moyen de développement personnel. En effet, tout comme le théâtre, le Jeu de Rôle
permet d'apprendre, entre autre, le lâcher prise émotionnel. 
De plus, les fondateurs de cette association sont conscients que ce type de site web peut-être
amené à gérer plus d'argent que nécessaire à une telle association. Si ce cas de figure advient,
alors l'association aura également pour but le partage des richesses et le soutient aux projets
servant le développement harmonieux et durable de l'humanité.  </i><br>";



$str .= "<div class='about-title'><b> Le fonctionnement</b><br/><br/></div>";

$str .= "<div class='about-main'> Les décisions importantes liées au projet sont prises en commun avec les joueurs intéressés dans le forum. De plus, l'association s'est beaucoup endettée au cours du développement de la version 2, donc toutes les rentrées d'argent dans les années à venir vont simplement servir à payer les frais de fonctionnement et à rembourser les dettes. Ainsi, pour les prochaines années il n'y aura pas a priori de décision importante à prendre concernant la gestion financière de l'association. C'est pourquoi l'association est réduite à son minimum : trois membres pour le bureau, dont le travail est réduit à la tenue des comptes. Au début il était prévu que les comptes soient publics, mais après discussion avec le Crédit Coopératif, il s'avère que ce n'est pas possible d'obtenir des relevés numériques du compte bancaire donc nous nous limitons à poster ici les comptes rendus des assemblées générales qui incluent un état des lieux financier.";

$str .= " </div><br><br>\n";

$str .= "<div class='about-title'><b> Le bureau</b><br/><br/></div>";

$str .= "<div class='about-main'> Secrétaire : Belette a.k.a. Gustavus a.k.a. Céline Pollet<br> 
Trésorier : Lord Thergal a.k.a. Aurélien Lebrun<br>
Président : Aé Li a.k.a. Elie Assémat";

$str .= " </div><br><br>\n";

$str .= "<div class='about-title'><b> Les documents officiels </b><br/><br/></div>";

$str .= "<div class='about-main'> <a class='stylecontact' href='../association/statuts.pdf'>Les statuts</a> <br> 
<a class='stylecontact' href='../association/creation_asso.pdf'>Récépissé de création</a><br>
<a class='stylecontact' href='../association/CR-AG-2012.pdf'>Compte rendu de l'AG 2012</a><br>
<a class='stylecontact' href='../association/CR-AG-2013.pdf'>Compte rendu de l'AG 2013</a><br>
<a class='stylecontact' href='../association/CR-AG-2014.pdf'>Compte rendu de l'AG 2014</a><br>";

$str .= " </div><br><br>\n";

$str .= "<div class='about-title'><b> Remarques sur les finances</b><br/><br/></div>";

$str .= "<div class='about-main'> - <b>Choix de la banque</b> : le Crédit Coopératif est une des rares banques françaises dont toutes les ressources sont utilisées pour financer des projets de l'économie sociale et solidaire et/ou des projets qui permettent un développement durable de l'humanité. Grâce à cette politique c'est également une des rares banques françaises à n'avoir pas du tout été affectée par la crise. <br><br>
- <b>Santé financière de l'association </b>:  La première année a été très positive, mais le rythme des rentrées s'est calmée avec le ralentissement de l'arrivée de nouveaux joueurs. Pour le moment il n'y a pas de soucis mais à long terme il faudra soit augmenter le nombre de joueurs soit envisager d'autres solutions<br><br>
- <b>Les dettes </b>: Les dettes ont évolué au cours de l'année 2014. Nous avons remboursé les 500€ que l'association devait à Alicia de Lamb. En attendant le CR de l'AG 2015, voici un petit résumé des dettes actuelles. Désormais les dettes ne feront que se résorber avec le temps. L'association a des dettes envers les personnes suivantes : Céline Pollet (140€) et Elie Assémat (2010€) <br><br> 
- <b>Le financement du projet</b> initial du projet a été possible grâce à de nombreux soutiens listés <a class='stylecontact' href='benefactors.php'>là-bas</a>. Un grand merci à eux.
";

$str .= "<br>";

$str .= " </div><br><br>\n";



$BOTTOMTitle->add("A propos du projet");
$BOTTOMMainArea->add($str);


$MAIN_PAGE->render();

?>
