<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

page_open(array(
    "sess" => "Session"
));

require_once (HOMEPATH . "/include/indextemp.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

$db = DB::getDB();

$BOTTOMTitle->add("Quelques aperçus du plateau de jeu");



$str = "<div class='screenshots-first-label'>Attaque du village fortifié Ar-Nak interrompu par une horde d'hommes-lézards durant la bêta.</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/screen3.jpg'><img  src='" . CONFIG_HOST . "/pics/screenshots/screen3.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Carte du sud du continent insulaire de Nacridan. La carte nord était ouverte à la fin de la version 1 et sera réouverte quand les aventuriers auront le niveau nécessaire. Les ronds représentent les villages contrôlés par des joueurs. (le 29/11/2015)</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/map-sud.png'><img src='" . CONFIG_HOST . "/pics/screenshots/map-sud.png' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>L'Union Obscure face aux hordes maléfiques qui tentent de forcer la porte sud d'Earok ! Au centre, l'archère tristement célèbre KrimOmbra, fondatrice de l'Union Obscure. (début de la v2, novembre 2013)</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/screen1.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/screen1.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Un Nain berger et son troupeau de Kradjecks ferreux.</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/screen4.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/screen4.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Premier meurtre sur l'île de Nacridan : le châtiment d'Adon par Dolgaröm. Adon venait de voler le Nain colérique. On peut voir les gardes officiels du royaume d'Artasse (en rose) qui vont trainer Dolgaröm en prison. Ce qui mènera à la création de la Garde du Crépuscule par Umbre et Dolgaröm</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/screen2.png'><img src='" . CONFIG_HOST . "/pics/screenshots/screen2.png' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Un Nain escortant trois caravaniers et leurs tortues géantes.</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/et-en-avant.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/et-en-avant.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Vol en formation de chauve-souris géantes et vue partielle d'Ald Mora peu après sa fondation par les Légendes. Au centre on peut voir le mage Lotharim, célèbre membre du Conseil des Légendes. </div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/aldmora.png'><img src='" . CONFIG_HOST . "/pics/screenshots/aldmora.png' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Campement forestier de la Garde du Crépuscule attaqué par des mort-vivants. On peut voir les murs de terre invoqué par les mages pour protéger le camp. Lequel sera peu de temps après pris de force par l'Union Obscure. </div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/foret-mv.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/foret-mv.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Alicia de Lamb (en robe blanche) surprenant le kidnapping du gouverneur d'Earok par un homme-lézard travaillant pour les Templiers du Chaos.</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/Alicia-earok.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/Alicia-earok.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Vue partielle du champ de bataille opposant les Templiers du Chaos et la grande coalition qui s'efforce de sauver les gouverneurs des royaumes.</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/bataille1.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/bataille1.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$str .= "<div class='screenshots'>Une autre partie du champ de bataille.</div>";
$str .= "<div class='screenshots'><a href='" . CONFIG_HOST . "/pics/screenshots/bataille2.jpg'><img src='" . CONFIG_HOST . "/pics/screenshots/bataille2.jpg' ></a></div>";
$str .= "<div class='horizontal-fancy-line'><img src='" . CONFIG_HOST . "/pics/new-homepage/horizontal-sword.png' ></div>";

$BOTTOMMainArea->add($str);




$MAIN_PAGE->render();

?>
