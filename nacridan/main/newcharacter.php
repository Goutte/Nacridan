<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/TplForm.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/class/DBCollection.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");
//require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/factory/MailFactory.inc.php");

DEFINE("RX_ALPHA", "([a-z]|[A-Z]|ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û)");
DEFINE("RX_UPPER_ALPHA", "([A-Z]|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û)");
DEFINE("RX_LOWER_ALPHA", "([a-z]|ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î)");
DEFINE("RX_ALPHA_NUM", "([a-z]|[0-9]|[A-Z]|ö|ô|ù|é|è|à|ç|ê|â|ä|ë|ï|î|î|Ï|Ü|Ä|Ë|Ö|Â|Ê|Î|Ô|Û)");

ini_set("display_errors", 1);

class NewCharacter extends HTMLObject
{

    public $master;

    public $lang;

    public $mainForm;

    public $auth;

    public $db;

    function getCharac()
    {
        
        
        $str = "<div id='ncdesc_human' class='ncdesc'>\n";
        
        $str .= "<b>" . ("Humain") . " :</b><br/><br/>\n";
        
        $str .="De loin les plus nombreux parmi les quatre Races Ainées, les hommes forment également la population la plus métissée. Présents dans toutes les régions de l'Île, des sables du désert Safran jusqu'aux plaines glacées du grand nord, ils semblent pouvoir s'adapter aisément à tous les environnements et à toutes les situations. Leur puissance s'exprime par le nombre mais aussi par une prodigieuse soif de connaissance et de savoir, qu'ils partagent semblent-ils avec un don pour l'apprentissage. De carrure plus robuste que les elfes et les doranes, ils peuvent aussi bien être d'excellents archers ou d'habiles bretteurs, que de puissants magiciens. Souvent très endurants, ils aiment à parcourir les vastes plaines centrales de Nacridan..." ;
        
        $str .= "<div class='nc-img-dices'>\n";

        $str .= "<div class='ncimg'>\n";
        
        $str .= "<img id='ncraceimg' src='../pics/character/human.jpg'>\n";
        $str .= "</div>\n";
        
        $str .= "<div class='nc-dices'>";
        
		  $str .= "<table>\n";
        $str .= "<tr><td width=120px > </td><td width=150px ><b>" . ("Dextérité") . "</b></td><td width=50px><b>:&nbsp</b><span class='ncdescav'> 4 D6 </span><br/> </td> <td></td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Force") . "</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Vitesse") . "</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Maîtrise de la Magie") . "&nbsp;</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td>&nbsp;</td><td> &nbsp;</td></tr>\n";
        $str .= "</table>\n";
        $str .= "</div>";
        $str .= "</div>\n";
        $str .= "</div>\n";
        
        
        
        
        $str .= "<div id='ncdesc_elf' class='ncdesc' style='display:none'> \n";
        
        $str .= "<b>" . ("Elfe") . " :</b><br/><br/>\n";
        $str .= "Fins et graciles, les elfes aux cheveux teints et aux longues oreilles dépourvues de lobes se considèrent comme le Beau Peuple, et il est vrai que peu parmi les races ainées peuvent prétendre les égaler en charisme. Peuple de nature et de magie, les elfes révèrent la Mère Nature et les esprits de l'air, auxquels ils rendent grâce pour leur œil infaillible ainsi que leur habilité proprement démoniaque à l'arc. Les elfes accordent une grande importance aux arts, dont ils tirent grand plaisir et satisfaction, et au nombre desquels ils comptent musique, poésie, théâtre ainsi que magie, dont ils sont souvent des pratiquants redoutablement doués, comme en témoigne le Centre elfique de magie, en leur ville de Krima.";
        $str .= "<div class='nc-img-dices'>\n";
        $str .= "<div class='ncimg'>\n";
        
        $str .= "<img id='ncraceimg' src='../pics/character/elf.jpg'>\n";
        $str .= "</div>\n";
        $str .= "<div class='nc-dices'>";
        $str .= "<table>\n";
        $str .= "<tr><td width=120px > </td><td width=150px ><b>" . ("Dextérité") . "</b></td><td width=50px><b>:&nbsp</b> 3 D6 <br/> </td> <td></td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Force") . "</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Vitesse") . "</b></td><td><b>:&nbsp</b><span class='ncdescav'> 4 D6 </span><br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Maîtrise de la Magie") . "&nbsp;</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td>&nbsp;</td><td> &nbsp;</td></tr>\n";
        $str .= "</table>\n";
        $str .= "</div>";
        $str .= "</div>";
        $str .= "</div>\n";
        
        $str .= "<div id='ncdesc_dwarf' class='ncdesc' style='display:none'>\n";
        
         
        $str .= "<b>" . ("Nain") . " :</b><br/><br/>\n";
        $str .= "Presque aussi larges qu'ils sont hauts, les fiers aventuriers nains à la barbe fournie et aux sourcils broussailleux sont en général des combattants féroces et tenaces, mus par un grand sens de l'honneur et du clan - ainsi, disent les mauvaises langues, que par une atavique soif de l'or. Ils portent le plus souvent de lourdes armures de plate forgées par leurs soins, et gardent leurs haches de guerres aiguisées avec un soin jaloux. Qu'ils viennent des profondes mines de leur cité d'Octobian ou qu'ils aient franchi les Mers jusqu'aux rivages de l'Île, ce sont des artisans plus que compétents, et bien qu'ils soient durs en affaires, ils se révèlent souvent des marchands honnêtes. Courageux, les nains affectionnent la bagarre, surtout après quelques bières, dont ils font une consommation importante.";
        
        $str .= "<div class='nc-img-dices'>\n";
        $str .= "<div class='ncimg'>\n";        
        $str .= "<img id='ncraceimg' src='../pics/character/dwarf.jpg'>\n";
        $str .= "</div>\n";
        $str .= "<div class='nc-dices'>";
        $str .= "<table>\n";
        $str .= "<tr><td width=120px > </td><td width=150px ><b>" . ("Dextérité") . "</b></td><td width=50px><b>:&nbsp</b> 3 D6 <br/> </td> <td></td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Force") . "</b></td><td><b>:&nbsp</b><span class='ncdescav'> 4 D6 </span><br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Vitesse") . "</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Maîtrise de la Magie") . "&nbsp;</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td>&nbsp;</td><td> &nbsp;</td></tr>\n";
        $str .= "</table>\n";
        $str .= "</div>";
        $str .= "</div>";
        $str .= "</div>\n";
        
        $str .= "<div id='ncdesc_dorane' class='ncdesc' style='display:none'>\n";
        
        $str .= "<b>" . ("Dorane") . " :</b><br/><br/>\n";
        $str .= "Venus dans leurs nefs aux flancs nacrés des lointains Archipels de Dol'Onoran et Sul'Mandar, les doranes forment un peuple mystérieux et farouche, apparenté aux elfes plutôt qu'aux hommes, lesquels ont longtemps craint ces sorciers à la peau violette et aux longs cheveux blancs, dont les arts mystiques surpassaient en puissance les autres magies. Car les doranes sont un peuple de magie par excellence, et celle-ci prend place dans chaque aspect de leur vie, étant une véritable religion. Généralement grands et fins, le port altier, les doranes ne vivent guère plus longtemps que les hommes, et même souvent moins, ce qui est considéré comme la juste rétribution des dieux pour tant de puissance, de même que l'on considère que leur présence en Izandar amena de grands malheurs sur la ville.";
        $str .= "<div class='nc-img-dices'>\n";
        $str .= "<div class='ncimg'>\n";        
        $str .= "<img id='ncraceimg' src='../pics/character/dorane.jpg'>\n";
        $str .= "</div>\n";
        $str .= "<div class='nc-dices'>";
        $str .= "<table>\n";
        $str .= "<tr><td width=120px > </td><td width=150px ><b>" . ("Dextérité") . "</b></td><td width=50px><b>:&nbsp</b> 3 D6 <br/> </td> <td></td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Force") . "</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Vitesse") . "</b></td><td><b>:&nbsp</b> 3 D6 <br/> </td></tr>\n";
        $str .= "<tr><td></td><td><b>" . ("Maîtrise de la Magie") . "&nbsp;</b></td><td><b>:&nbsp</b><span class='ncdescav'> 4 D6 </span><br/> </td></tr>\n";
        $str .= "<tr><td></td><td>&nbsp;</td><td> &nbsp;</td></tr>\n";
        $str .= "</table>\n";
        $str .= "</div>";
        $str .= "</div>";
        $str .= "</div>\n";
        
        // $str.="<div style='float:left'><br/><br/><span class='ncdescav'> Vert : </span>".("ces caractéristiques seront un peu plus faciles à améliorer")."<br/>";
        // $str.="<span class='ncdescdis'> Rouge : </span>".("ces caractéristiques seront un peu plus difficiles à améliorer")."<br/></div>";
        return $str;
    }

    function getJScript()
    {
        $str = "<script type='text/javascript'>\n";
        $str .= "<!--\n";
        $str .= "var myimages=new Array();\n";
        $str .= "var current='human';\n";
        $str .= "var arr=new Array('human','elf','dwarf','dorane');\n";
        
        $str .= "function preloadimages(){\n";
        $str .= "   for (i=0;i<preloadimages.arguments.length;i++){\n";
        $str .= "     myimages[i]=new Image()\n";
        $str .= "     myimages[i].src=preloadimages.arguments[i]\n";
        $str .= "   }\n";
        $str .= "}\n";
        
        $str .= "function racech(id){\n";
        $str .= "  var d = document.getElementById(id);\n";
        $str .= "  var img = document.getElementById('ncraceimg');\n";
        $str .= "  img.src=myimages[d.value-1].src;\n";
        
        $str .= "  document.getElementById('ncdesc_'+current).style.display='none';\n";
        $str .= "  current=arr[d.value-1];\n";
        $str .= "  document.getElementById('ncdesc_'+current).style.display='block';\n";
        
        $str .= "}\n";
        
        $str .= "preloadimages('../pics/character/human.jpg','../pics/character/elf.jpg','../pics/character/dwarf.jpg','../pics/character/dorane.jpg');\n";
        $str .= "-->\n";
        $str .= "</script>\n";
        
        return $str;
    }

    function NewCharacter($master, $auth, $session = null)
    {
        $this->master = $master;
        $this->lang = 'fr';
        $this->auth = $auth;
        $this->db = DB::getDB();
        
        $dbp = new DBCollection("SELECT id FROM Player WHERE status='PC' AND id_Member =" . $this->auth->auth["uid"], $this->db);
        $dbm = new DBCollection("SELECT maxNbPlayers AS N FROM Member WHERE id =" . $this->auth->auth["uid"], $this->db);
        
        if (($this->auth->auth["newplayer"] > 0) || ($dbp->count() < $dbm->get("N"))) {
            $this->db = DB::getDB();
            
            $dbr = new DBCollection("SELECT * FROM BasicRace WHERE PC='Yes'", $this->db);
            $o = array();
            while (! $dbr->eof()) {
                $o[] = array(
                    "label" => ($dbr->get("name")),
                    "value" => $dbr->get("id")
                );
                $dbr->next();
            }
            
            $o2 = array();
            $o2[] = array(
                "label" => ("Homme"),
                "value" => "M"
            );
            $o2[] = array(
                "label" => ("Femme"),
                "value" => "F"
            );
            
            $o3 = array();
            $o3[] = array(
                "label" => ("Epée"),
                "value" => "sword"
            );
            $o3[] = array(
                "label" => ("Dague"),
                "value" => "dagger"
            );
            $o3[] = array(
                "label" => ("Masse"),
                "value" => "mace"
            );
            
            $o4 = array();
            // ROYAUME ARTASSE
            $idCity = 159;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => "Artasse - " . max(0, (50 - $dbp->count())) . " places",
                "value" => $temple
            );
            
            $idCity = 150;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Bourg - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            // ROYAUME EAROK
            $idCity = 135;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Earok - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            /*
             * ON ENLEVE FOSSAYE
             * $idCity = 124;
             * $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=".$idCity, $this->db);
             * $temple=$dbs->get('id');
             * $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0 AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=".$temple, $this->db);
             * $o4[]=array("label"=>("Bourg Nord - ".max(0,(50 - $dbp->count()))." places"),"value"=>$temple);
             */
            $idCity = 143;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Bourg Est - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            $idCity = 134;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Village Ouest - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            $idCity = 142;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Village Sud - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            $idCity = 151;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Tonak - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            $idCity = 140;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Bourg - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            $idCity = 132;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Village Ouest - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            $idCity = 128;
            $dbs = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $idCity, $this->db);
            $temple = $dbs->get('id');
            $dbp = new DBCollection("SELECT * FROM Player WHERE id_member!=0  AND authlevel=0 AND disabled!=99 AND status='PC' AND name IS NOT NULL  AND ADDDATE(curratb, INTERVAL 14 DAY)> NOW() AND resurrectid=" . $temple, $this->db);
            $o4[] = array(
                "label" => ("Village Est - " . max(0, (50 - $dbp->count())) . " places"),
                "value" => $temple
            );
            
            // ------------- Main FORM
            $formname = "form";
            $this->mainForm = new TplForm($formname, $session);
            $this->mainForm->setAction(CONFIG_HOST . "/main/newcharacter.php");
            $this->mainForm->addElement(
                array(
                    "name" => "name",
                    "targetid" => "name_err",
                    "type" => "text",
                    "value" => "",
                    "valid_regex" => "^" . RX_ALPHA . "+(" . RX_ALPHA . "| ){2,}" . RX_ALPHA . "{1}$",
                    "targetmsg" => ("Le nom de votre personnage doit être cohérent avec un univers médiéval-fantastique. Si vous manquez d'inspiration, vous pouvez consulter les divers générateurs de noms de fiction disponibles sur internet, comme <a class='stylecontact' hrf='http://www.gunof.net/'>Gunof.net</a>."),
                    "targeterr" => "err",
                    "targetcss" => "info",
                    "valid_e" => ('Ce champ doit comporter au moins 4 caractères, sans chiffre, les espaces sont autorisés.')
                ));
            
            $this->mainForm->addElement(
                array(
                    "type" => "select",
                    "name" => "race",
                    "targetid" => "race_err",
                    "options" => $o,
                    "size" => 1,
                    "targetmsg" => ('Choisissez votre race'),
                    "targeterr" => "err",
                    "targetcss" => "info",
                    "valid_e" => ('Race non valide'),
                    "value" => "1",
                    "extrahtml" => "id=\"ncrace\" onchange=javascript:racech(\"ncrace\");"
                ));
            
            $this->mainForm->addElement(
                array(
                    "type" => "select",
                    "name" => "gender",
                    "targetid" => "gender_err",
                    "options" => $o2,
                    "size" => 1,
                    "targetmsg" => ('Choisissez le sexe de votre personnage'),
                    "targeterr" => "err",
                    "targetcss" => "info",
                    "valid_e" => ("Sexe non valide"),
                    "value" => "1"
                ));
            
            $this->mainForm->addElement(
                array(
                    "type" => "select",
                    "name" => "arme",
                    "targetid" => "arme_err",
                    "options" => $o3,
                    "size" => 1,
                    "targetmsg" => ("Choisissez votre arme de départ. <br/> Attention, l'arme va influencer l'évolution de votre profil à chaque utilisation, de façon définitive. L'épée est associée à la dextérité, la masse à la force et la dague à la vitesse."),
                    "targeterr" => "err",
                    "targetcss" => "info",
                    "valid_e" => ('Arme non valide'),
                    "value" => "1"
                ));
            
            $this->mainForm->addElement(
                array(
                    "type" => "select",
                    "name" => "ville",
                    "targetid" => "ville_err",
                    "options" => $o4,
                    "size" => 1,
                    "targetmsg" => ("Choisissez votre lieu de départ. <br/> Les bourgs et villages situés dans la liste en-dessous du nom d'une ville se trouvent à proximité de cette ville. Pour plus d'information sur les différents lieux de départ, rendez-vous à <a class='stylecontact' href='../i18n/rules/fr/rules.php'>cette page des règles</a>."),
                    "targeterr" => "err",
                    "targetcss" => "info",
                    "valid_e" => ('Ville non valide'),
                    "value" => "1"
                ));
            
            $this->mainForm->addElement(array(
                "name" => "submit",
                "type" => "submit",
                "value" => ("Création du personnage !"),
                "extrahtml" => "class=\"button\""
            ));
            $this->master->getObjectById("head")->add($this->mainForm->getJsValidation());
        }
    }

    function toString()
    {
        $dbp = new DBCollection("SELECT id FROM Player WHERE status='PC' AND id_Member =" . $this->auth->auth["uid"], $this->db);
        $dbm = new DBCollection("SELECT maxNbPlayers AS N FROM Member WHERE id =" . $this->auth->auth["uid"], $this->db);
        
        if (($this->auth->auth["newplayer"] > 0) || ($dbp->count() < $dbm->get("N"))) {
            $step = $this->mainForm->getStep($_POST);
            
            switch ($step) {
                case 1:
                    // return "<div class='registration'>OK</div>";
                    return $this->step1();
                    break;
                case 0:
                    return $this->step0();
                    break;
                default:
                    printf("empty page <br />\n");
                    break;
            }
        } else {
            $str = "<div class='registration'>";
            $str .= ("Pour l'instant, vous n'avez pas la capacité de créer un nouveau personnage ...ID : " . $this->auth->auth["uid"] . ".");
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            $this->auth->logout();
            return $str;
        }
    }

    public function step0()
    {
        
        $str = $this->getJScript();
        
        $form = "<table class='registration'>";
        $form .= "<tr><td>" . ("Nom") . "</td><td>" . $this->mainForm->getElement("name") . "</td><td id='name_err' " . $this->mainForm->getTargetCss("name") . ">" . $this->mainForm->getTargetMsg("name") . "</td></tr>";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "<tr><td>" . ("Race") . "</td><td>" . $this->mainForm->getElement("race") . "</td><td id='race_err' " . $this->mainForm->getTargetCss("race") . ">" . $this->mainForm->getTargetMsg("race") . "</td></tr>";
        $form .= "<tr><td>&nbsp;</td><td></td></tr></table>\n";
        
        $form .= "<div class='registration'>";
        $form .= $this->getCharac();
        $form .= "</div>\n";
        
        // $form.="<div class='registration_title'><b> - ".("Création de votre personnage, suite.")."</b></div><br/><br/>";
        
        //$form .= "<br><br><br><br><br><br><br><br>";
        $form .= "<div class='nc-bottom-form'>";
        $form .= "<table class='registration'>";
        $form .= "<tr><td width=10px>Sexe</td><td width=220px>" . $this->mainForm->getElement("gender") . "</td><td id='gender_err' " . $this->mainForm->getTargetCss("gender") . ">" . $this->mainForm->getTargetMsg("gender") . "</td></tr>";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "<tr><td >Arme</td><td>" . $this->mainForm->getElement("arme") . "</td><td id='arme_err' " . $this->mainForm->getTargetCss("arme") . ">" . $this->mainForm->getTargetMsg("arme") . "</td></tr>";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "<tr><td >Lieu de départ</td><td>" . $this->mainForm->getElement("ville") . "</td><td id='ville_err' " . $this->mainForm->getTargetCss("ville") . ">" . $this->mainForm->getTargetMsg("ville") . "</td></tr>";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "<tr ><td>" . $this->mainForm->getElement("submit") . "</td><td></td></tr></table>";
        $form .= "</div>\n";
        
        
        $str .= $this->mainForm->getForm($form);
        
        /*
         * $str.="<div class='registration'>";
         * $str.=$this->getCharac();
         * $str.="</div>\n";
         */
        
        return $str;
    }

    public function step1()
    {
        $this->mainForm->loadDefaults($_POST);
        
        if ($this->mainForm->validate() && ! $this->mainForm->isRepostForm()) {
            $basicrace = new BasicRace();
            $basicrace->load($this->mainForm->get("race"), $this->db);
            $racename = $basicrace->get("name");
            
            $player = new Player();
            $player->set("id_Member", $this->auth->auth["uid"]);
            $player->set("id_World", 1);
            $player->set("name", unHTMLTags($this->mainForm->get("name")));
            $player->set("racename", $racename);
            $player->set("id_BasicRace", $this->mainForm->get("race"));
            $player->set("gender", $this->mainForm->get("gender"));
            $player->set("level", 1);
            $player->set("merchant_level", 1);
            $player->set("state", "walking");
            $player->set("hidden", 10);
            $player->set("status", "PC");
            
            $player->set("ap", BASE_AP);
            $player->set("atb", 720);
            $player->set("ip", $_SERVER['REMOTE_ADDR']);
            
            $dbt = new DBCollection("SELECT * FROM Building WHERE id=" . quote_smart($this->mainForm->get("ville")), $this->db);
            
            $player->set("inbuilding", $this->mainForm->get("ville"));
            $player->set("resurrectid", $this->mainForm->get("ville"));
            $player->set("x", $dbt->get("x"));
            $player->set("y", $dbt->get("y"));
            
            $player->set("map", 1);
            
            $player->set("money", 40);
            
            $moneyHistory = array();
            for ($i = 0; $i < 10; $i ++) {
                $moneyHistory[$i] = 0;
            }
            $player->set("moneyHistory", serialize($moneyHistory));
            
            $player->set("atb", 720);
            $datetime = time() + 3600;
            
            $player->set("creation", gmdate("Y-m-d H:i:00", $datetime));
            $player->set("playatb", gmdate("Y-m-d H:i:00", $datetime));
            $player->set("curratb", gmdate("Y-m-d H:i:00", $datetime));
            $datetime += 20 * 60;
            $player->set("nextatb", gmdate("Y-m-d H:i:00", $datetime));
            
            $modifier = new Modifier();
            
            switch ($this->mainForm->get("race")) {
                case 1:
                    $modifier->setModif("hp", DICE_ADD, 40);
                    $modifier->setModif("dexterity", DICE_D6, 4);
                    $modifier->setModif("strength", DICE_D6, 3);
                    $modifier->setModif("speed", DICE_D6, 3);
                    $modifier->setModif("magicSkill", DICE_D6, 3);
                    break;
                
                case 2:
                    $modifier->setModif("hp", DICE_ADD, 40);
                    $modifier->setModif("dexterity", DICE_D6, 3);
                    $modifier->setModif("strength", DICE_D6, 3);
                    $modifier->setModif("speed", DICE_D6, 4);
                    $modifier->setModif("magicSkill", DICE_D6, 3);
                    break;
                
                case 3:
                    $modifier->setModif("hp", DICE_ADD, 40);
                    $modifier->setModif("dexterity", DICE_D6, 3);
                    $modifier->setModif("strength", DICE_D6, 4);
                    $modifier->setModif("speed", DICE_D6, 3);
                    $modifier->setModif("magicSkill", DICE_D6, 3);
                    break;
                
                case 4:
                    $modifier->setModif("hp", DICE_ADD, 40);
                    $modifier->setModif("dexterity", DICE_D6, 3);
                    $modifier->setModif("strength", DICE_D6, 3);
                    $modifier->setModif("speed", DICE_D6, 3);
                    $modifier->setModif("magicSkill", DICE_D6, 4);
                    break;
            }
            
            $upgrade = new Upgrade();
            
            $player->set("currhp", $modifier->getModif("hp", DICE_ADD));
            $player->set("hp", $modifier->getModif("hp", DICE_ADD));
            
            $player->setObj("Modifier", $modifier, true);
            $player->setObj("Upgrade", $upgrade, true);
            
            $player->addDBr($this->db);
            if ($player->errorNoDB()) {
                if ($player->errorNoDB() == 1062) {
                    $this->mainForm->setTargetCss("name", "err");
                    $this->mainForm->setTargetMsg("name", ("Ce pseudo est déjà utilisé !"));
                } else {
                    $err = "<div class='registration'>";
                    $err .= ("Erreur : " . $player->errorNoDB() . " Impossible d'ajouter un nouveau joueur dans la base de données. Veuillez contacter l'équipe de développement.");
                    $err .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
                    $err .= "</div>";
                    return $err;
                }
            } else {
                
                $detail = new Detail();
                $detail->set("id_Player", $player->get("id"));
                $detail->set("body", $basicrace->get("description"));
                $detail->addDBr($this->db);
                $player->set("id_Detail", $detail->get("id"));
                $player->updateDB($this->db);
                
                // Création du message d'accueil RP
                $title = "Bienvenue! [RP]";
                $author = new Player();
                $author->load(2, $this->db); // id de Prêtre = 2
                
                $array = explode(",", $player->get("name"));
                $type = 0; // message normal
                
                $body = "Salut à toi jeune aventurier. 
				 
				 Je viens de fixer ton âme à ce temple. Si tu venais à mourir, tu y serais automatiquement réincarné. Je vois que tu possèdes une belle arme. Voici deux potions de vie. Elles complèteront ton équipement car tu seras vite à cours d'argent vu le peu de pièces d'or que tu possèdes. Même si les temples payent un peu pour que les jeunes aventuriers tuent les monstres aux alentours de la ville, la chasse n'est guère rentable et il est souvent plus intéressant de réaliser quelques missions d'escorte ou d'apprendre un savoir-faire qui permettra d'obtenir quelques matières premières pour les revendre.
				 
Allez-va ! Et ne t'éloigne pas trop de la ville au début, les monstres deviennent rapidement plus coriaces loin des villes. Un indice important à ce sujet : les plaines fleuries marquent les limites du royaume et des monstres correspondant au niveau des jeunes aventuriers. Et rappelle-toi, dans ce royaume, le meurtre d'un autre citoyen est interdit. Les tortues géantes qui acheminent les marchandises d'une ville à l'autre sont également protégées par la loi. Transgresser ces règles pourrait te conduire en prison.

Puisse la Lumière d'Athlan te protéger ! ";
                MailFactory::sendSingleDestMsg($author, $title, $body, $array, $type, $err, $this->db, - 1, 0);
                
                // Création du message d'accueil HRP
                $title = "Bienvenue! [HRP]";
                $author = new Player();
                $author->load(1, $this->db);
                
                $array = explode(",", $player->get("name"));
                
                $body = "Bienvenue dans Nacridan 2,
				 
				      Si vous êtes là, c'est que vous avez déjà passé les 2 étapes indispensables pour pouvoir jouer : la création d'un compte et la création du personnage.

    Il est maintenant temps de jouer. Nacridan est un jeu tour par tour, ce qui signifie que vous incarnez un personnage qui reçoit 12 points d'action (PA) toutes les 12 heures. Ces PAs vous permettent de réaliser des actions qui feront interagir votre personnage avec le reste du monde

    Plus concrètement, à votre première connexion dans le jeu vous êtes à l'intérieur d'un temple, c'est pourquoi vous ne voyez pas votre avatar mais uniquement les options de la pièce du bâtiment dans laquelle vous vous trouvez. En cliquant sur Accueil, en haut de page vous obtiendrez une première vue du monde environnant (peut ne pas fonctionner avec IE, priviligier FF ou Chrome). Ce n'est qu'une fois sorti du temple (en cliquant sur une case adjacente et en validant) que votre personnage vous apparaitra enfin au centre de la vue.

    Si vous vous sentez un peu perdu et que vous ne savez pas quoi faire au début, nous vous encourageons à lire le paragraphe des règles traitant des deux premiers tours de jeu, qui est disponible à la fin de la première page des règles. Ne lisez pas toutes les règles, ce paragraphe à lui seul vous permettra de bien commencer l'aventure, que vous ayez juste du mal à sortir du temple, que vous vouliez rapidement occire un monstre ou que vous souhaitiez obtenir le plus rapidement un sortilège. 
	
	Vous pouvez également aller chercher quelques conseils sur le forum ou en vous adressant aux Personnages vous entourant, s'il y en a.

    Bon jeu.



---- Remarques sur l'équipe -----

Nous avons fait de notre mieux pour créer un jeu agréable. Cependant, veuillez garder à l'esprit que nous sommes des bénévoles amateurs, donc il reste bien des points à améliorer. Vous pouvez participer en partageant vos impressions et suggestions dans le forum.

Merci pour votre aide.";
                MailFactory::sendSingleDestMsg($author, $title, $body, $array, $type, $err, $this->db, - 1, 0);
                
                // ------------------ EQUIPEMENTS
                
                switch ($this->mainForm->get("arme")) {
                    case "sword":
                        $equip = new Equipment();
                        $equip->set("name", "Epée longue");
                        $equip->set("id_BasicEquipment", 4);
                        $equip->set("id_EquipmentType", 2);
                        $equip->set("durability", 60);
                        $equip->set("level", 1);
                        $equip->set("date", gmdate("Y-m-d H:i:s"));
                        $equip->set("state", "Carried");
                        $equip->set("id_Player", $player->get("id"));
                        $equip->addDBr($this->db);
                        break;
                    
                    case "dagger":
                        $equip = new Equipment();
                        $equip->set("name", "Dague");
                        $equip->set("id_BasicEquipment", 1);
                        $equip->set("id_EquipmentType", 1);
                        $equip->set("durability", 60);
                        $equip->set("level", 1);
                        $equip->set("date", gmdate("Y-m-d H:i:s"));
                        $equip->set("state", "Carried");
                        $equip->set("id_Player", $player->get("id"));
                        $equip->addDBr($this->db);
                        break;
                    
                    case "mace":
                        $equip = new Equipment();
                        $equip->set("name", "Masse d'armes");
                        $equip->set("id_BasicEquipment", 7);
                        $equip->set("id_EquipmentType", 3);
                        $equip->set("durability", 60);
                        $equip->set("level", 1);
                        $equip->set("date", gmdate("Y-m-d H:i:s"));
                        $equip->set("state", "Carried");
                        $equip->set("id_Player", $player->get("id"));
                        $equip->addDBr($this->db);
                        break;
                }
                
                $equip = new Equipment();
                $equip->set("name", "Potion de Vie");
                $equip->set("id_BasicEquipment", 400);
                $equip->set("id_EquipmentType", 29);
                $equip->set("level", 2);
                $equip->set("date", gmdate("Y-m-d H:i:s"));
                $equip->set("state", "Carried");
                $equip->set("id_Player", $player->get("id"));
                $equip->addDBr($this->db);
                
                $equip = new Equipment();
                $equip->set("name", "Potion de Vie");
                $equip->set("id_BasicEquipment", 400);
                $equip->set("id_EquipmentType", 29);
                $equip->set("level", 2);
                $equip->set("date", gmdate("Y-m-d H:i:s"));
                $equip->set("state", "Carried");
                $equip->set("id_Player", $player->get("id"));
                $equip->addDBr($this->db);
                
                // ---------------- NEW ATB
                
                require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
                $param = array();
                $errATB = PlayerFactory::newATB($player, $param, $this->db);
                
                if ($errATB) {
                    $err = "<div class='registration'>";
                    $err .= ("Erreur : Le personnage a été créé avec succès mais sa DLA n'a pas été initialisée. Veuillez contacter l'équipe de développement.");
                    $err .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
                    $err .= "</div>";
                    return $err;
                }
                
                // ---------------- FIN DE LA CREATION
                
                $member = new Member();
                $member->load($this->auth->auth["uid"], $this->db);
                $member->set("newplayer", $member->get("newplayer") - 1);
                $member->updateDB($this->db);
                
                // ------------------- BONUS DES ULULERS
                
                $dbemail = new DBCollection("SELECT email FROM MemberDetail WHERE id_Member=" . $this->auth->auth["uid"], $this->db);
                $errorbonus = "";
                
                include_once (HOMEPATH . "/liste-mail-ululers.php");
                $mailinglist = givelist_chara();
                if (in_array($dbemail->get("email"), $mailinglist)) {
                    $member->set('maxNbPlayers', 2);
                    $member->updateDB($this->db);
                }
                
                $mailinglist = givelist_armor();
                
                if (in_array($dbemail->get("email"), $mailinglist)) {
                    $BM = new BM();
                    $BM->set("id_Player", $player->get("id"));
                    $BM->set("id_StaticModifier", 23);
                    $BM->set("name", "Armure naturelle du Soutien");
                    $BM->set("level", 1);
                    $BM->set("life", - 3);
                    $BM->set("date", gmdate("Y-m-d H:i:00", $datetime));
                    $BM->updateDB($this->db);
                }
                
                $mailinglist = givelist_house();
                if (in_array($dbemail->get("email"), $mailinglist) && ($member->get("newplayer") == 0)) {
                    $dbcity = new DBCollection("SELECT id_City FROM Building WHERE id=" . quote_smart($this->mainForm->get("ville")), $this->db);
                    $dbhouse = new DBCollection("SELECT * FROM Building WHERE id_City=" . $dbcity->get("id_City") . " AND id_Player<2 AND id_BasicBuilding=10", $this->db);
                    if ($dbhouse->count() > 0)
                        $dbhome = new DBCollection("UPDATE Building SET id_Player=" . $player->get("id") . " WHERE id=" . $dbhouse->get("id"), $this->db);
                    else
                        $errorbonus = "Aucune maison libre ne se trouve près de votre lieu de départ, contactez les développeurs. <br>";
                }
                
                // ---------------- MAJ DU COMPTE GG
                /*
                 * $dbGG = DB::getGGDB();
                 * $dbpGG = new DBCollection("SELECT playername FROM user WHERE username='".$member->get("login")."'", $dbGG);
                 * if($dbpGG->get("playername") == '')
                 * $namearray = array();
                 * else
                 * $namearray = unserialize($dbpGG->get("playername"));
                 * $namearray[] = unHTMLTags($this->mainForm->get("name"));
                 *
                 * $dbuGG = new DBCollection("UPDATE user SET playername='".quote_smart(serialize($namearray))."' WHERE username='".$member->get("login")."'", $dbGG);
                 */
                // ---------------- FIN MAJ COMPTE GG
                
                $this->mainForm = new TplForm("form", null, CONFIG_HOST . "/conquest/conquest.php?center=first");
                $this->mainForm->addElement(array(
                    "type" => "hidden",
                    "name" => "dla",
                    "value" => "true"
                ));
                
                $str = "<div class='registration'>";
                $str .= $errorbonus . " Votre personnage <b>" . $player->get("name") .
                     "</b> a été créé avec succès!<br> - En cliquant sur <b>Terminer</b>, en bas de cette page, vous serez redirigé sur le jeu. <br>Une fois dans le jeu, lisez les messages d'accueil dans la messagerie.  <br> - Si vous êtes tout nouveau dans ce genre de jeu, il est conseillé de NE PAS lire l'ensemble des règles du jeu, dont les détails complexes ne sont pas nécessaires à une prise en main rapide et agréable. Vous pouvez vous contenter des onglets  \"Principes de base\" et \"Interface\". ";
                
                $str .= "<br><br> Au début vous êtes dans un temple, pour en sortir, suivez l'image ci-dessous, que vous retrouverez à la première page des règles. <br> Pour vos premiers pas, il est conseillé : <br>";
                $str .= "- soit de courir vers la campagne le plus vite possible, de trouver un rat géant ou un crapaud géant, et de le tuer.<br> ";
                $str .= "- soit de trouver l'auberge du village pour obtenir une mission bien payée en discutant avec les clients dans la grande salle. <br> ";
                $str .= "<br><br><div style='width:600px; height:615px;'><img style='float: left; ' src='" . CONFIG_HOST . "/pics/rules/newchargoout.jpg' alt='' /></div>";
                $str .= "<br/><br><br/><input id='newplayer' name='newplayer' type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
                $str .= "<br><br></div>";
                return $str;
                // return $this->mainForm->getForm()."Personnage créé !!!\n".$this->mainForm->getNextStep();
            }
        }
        
        $this->mainForm->setStep(0);
        
        if ($this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $str .= ("Erreur : Ces données ont déjà été envoyées ou le délai de la page précédente a expriée.");
            $str .= "<br/><br/><input type='button' value='" . ("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            return $str;
        } else {
            return $this->step0();
        }
    }
}

page_open(array(
    "sess" => "Session",
    "auth" => "Auth"
));

require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

//Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "registration", true, '../i18n/messages/cache', $filename = "");

if (isset($auth->auth["uid"])) {
    $query = sprintf("select * from Member where id=%s", $auth->auth["uid"]);
    
    $db = DB::getDB();
    $dbc = new DBCollection($query, $db);
    
    if (! $dbc->eof()) {
        $uid = $dbc->get("id");
        $auth->auth["uid"] = $uid;
        $auth->auth["lang"] = $dbc->get("lang");
        $auth->auth["utc"] = $dbc->get("utc");
        $auth->auth["newplayer"] = $dbc->get("newplayer");
    }
}

$newchar = new NewCharacter($MAIN_PAGE, $auth, $sess);
$CENTER->add("<div class='registration_title'><b> - " . ("Création de votre personnage") . "</b></div><br/><br/>");
$BOTTOMTitle->add($newchar);
$MAIN_PAGE->render();
//Translation::saveMessages();

?>
