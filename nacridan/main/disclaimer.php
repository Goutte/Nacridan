<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/TplForm.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/class/DBCollection.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");

class Disclaimer extends HTMLObject
{

    public $master;

    public $lang;

    public $mainForm;

    public $auth;

    public $db;

    function Disclaimer($master, $lang, $auth, $session = null)
    {
        $this->master = $master;
        $this->lang = $lang;
        $this->auth = $auth;
        $this->db = DB::getDB();
        
        // ------------- Main FORM
        $formname = "form";
        $this->mainForm = new TplForm($formname, $session);
        
        $this->mainForm->addElement(array(
            "name" => "discheck",
            "targetid" => "discheck_err",
            "targeterr" => "err",
            "targetcss" => "info",
            "type" => "checkbox",
            "value" => "accept",
            "exclude" => "false",
            "targetmsg" => "",
            "valid_e" => localize("Vous n'avez pas accepté la charte.")
        ));
        
        $this->mainForm->addElement(array(
            "name" => "submit",
            "type" => "submit",
            "value" => localize("Valider"),
            "extrahtml" => "class=\"button\""
        ));
        $this->master->getObjectById("head")->add($this->mainForm->getJsValidation());
    }

    public function step0()
    {
        $str = "<div class='attackevent'><b>Attention :</b><br/><div style='text-align: justify; padding-left: 20px; width: 80%'>Une nouvelle charte a été définie pour Nacridan. Pour pouvoir continuer à jouer, vous devez lire et accepter cette nouvelle charte dans son intégralité.</span></div></div><br/><br/>";
        
        $str .= "<div class='registration_title'><b> - Nouvelle Charte</b></div><br/><br/>";
        
        $eng = "";
        $form = "<table class='registration' width='590px'>";
        
        $form .= "<tr><td  >" . localize("J'accepte la nouvelle") . " <a class='link' target='blank' href='" . CONFIG_HOST . "/i18n/disclaimer/" . $this->lang . "/disclaimer.php'>" . localize("charte") . "</a>" . $eng . "</td><td>" . $this->mainForm->getElement("discheck") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='discheck_err' " . $this->mainForm->getTargetCss("discheck") . ">" . $this->mainForm->getTargetMsg("discheck") . "</td></tr>\n";
        
        $form .= "</table><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $this->mainForm->getElement("submit") . "\n";
        
        return $str . $this->mainForm->getForm($form);
    }

    public function step1()
    {
        $this->mainForm->loadDefaults($_POST);
        
        if ($this->mainForm->validate() && ! $this->mainForm->isRepostForm()) {
            $db = $this->db;
            $query = sprintf("UPDATE Member SET lastlog='" . gmdate("Y-m-d H:i:s") . "' WHERE id=" . $this->auth->auth["uid"]);
            $db = DB::getDB();
            $dbc = new DBCollection($query, $db);
            
            redirect(CONFIG_HOST . "/conquest/conquest.php");
        }
        
        $this->mainForm->setStep(0);
        
        if ($this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $str .= localize("Erreur : Ces données ont déjà été envoyées ou le délai de la page précédente a expriée.");
            $str .= "<br/><br/><input type='button' value='" . localize("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/index.php'\" />";
            $str .= "</div>";
            return $str;
        } else {
            return $this->step0();
        }
    }

    public function toString()
    {
        $step = $this->mainForm->getStep($_POST);
        $ret = array();
        switch ($step) {
            case 1:
                return $this->step1();
                break;
            case 0:
                return $this->step0();
                break;
            
            default:
                printf("empty page <br />\n");
                break;
        }
    }
}

page_load(array(
    "sess" => "Session",
    "auth" => "Auth"
));
require_once (HOMEPATH . "/include/index.inc.php");
require_once (HOMEPATH . "/include/meta.inc.php");

Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "registration", true, '../i18n/messages/cache', $filename = "");

$disclaimer = new Disclaimer($MAIN_PAGE, $lang, $auth, $sess);
$CENTER->add($disclaimer);
$MAIN_PAGE->render();
Translation::saveMessages();

?>