<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/TplForm.inc.php");

class MailContact extends HTMLObject
{

    public $master;

    public $lang;

    public $mainForm;

    public $db;

    public function MailContact($master, $lang, $session = null)
    {
        $this->master = $master;
        $this->lang = $lang;
        $this->db = DB::getDB();
        $formname = "form";
        $this->mainForm = new TplForm($formname, $session, CONFIG_HOST . "/main/contact.php");
        
        $this->mainForm->addElement(
            array(
                "name" => "email",
                "targetid" => "email_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^([a-z]|[A-Z]|[0-9]|\.|-|_)+@([a-z]|[A-Z]|[0-9]|\.|-|_)+\.([a-z]|[A-Z]|[0-9]){2,5}$",
                "errorclass" => "error",
                "targetmsg" => localize('Cette adresse sera celle utilisée pour vous répondre.'),
                "valid_e" => localize("Adresse E-mail non valide")
            ));
        
        $this->mainForm->addElement(
            array(
                "name" => "title",
                "targetid" => "title_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^.+$",
                "errorclass" => "error",
                "targetmsg" => localize('Titre du message.'),
                "valid_e" => localize("Le titre est vide")
            ));
        
        $this->mainForm->addElement(
            array(
                "name" => "body",
                "targetid" => "body_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "textarea",
                "rows" => 6,
                "cols" => 40,
                "value" => "",
                "valid_regex" => "^(.|\s)+$",
                "errorclass" => "error",
                "targetmsg" => localize('Corps du message.'),
                "valid_e" => localize("Le message est vide")
            ));
        
        $this->mainForm->addElement(
            array(
                "name" => "imgcode",
                "targetid" => "imgcode_err",
                "targeterr" => "err",
                "targetcss" => "info",
                "type" => "text",
                "value" => "",
                "valid_regex" => "^([a-z]|[A-Z]|[0-9]|\.|-|_)+$",
                "errorclass" => "error",
                "targetmsg" => localize('Entrez le code de validation ci-contre'),
                "valid_e" => localize("Ce champ ne doit contenir que des chiffres ou des lettres")
            ));
        
        $this->mainForm->addElement(array(
            "name" => "submit",
            "type" => "submit",
            "value" => localize("Envoyer le Message"),
            "extrahtml" => "class=\"button\""
        ));
        $this->master->getObjectById("head")->add($this->mainForm->getJsValidation());
    }

    protected function sendEmail($subject, $message, $from, &$res)
    {
        require_once (HOMEPATH . "/lib/swift/Swift.php");
        require_once (HOMEPATH . "/lib/swift/Swift/Connection/SMTP.php");
        
        $to = "nacridan.contact@gmail.com";
        $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
        $succes = false;
        if ($mailer->isConnected()) // Optional
{
            
            if (SMTPLOGIN != "" && SMTPPASS != "") {
                require_once (HOMEPATH . "/lib/swift/Swift/Authenticator/PLAIN.php");
                $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
                $mailer->authenticate(SMTPLOGIN, SMTPPASS);
            }
            
            // Add as many parts as you need here
            $mailer->addPart($message);
            $mailer->setCharset("UTF-8");
            $succes = $mailer->send($to, $from, "[Nacridan Site] " . $subject);
            
            $mailer->close();
        }
        if ($succes) {
            $res = "";
        } else {
            $res = localize("Erreur, le message n'a pas pu être délivrée.");
        }
        return $succes;
    }

    public function step0()
    {
        $str = "<div class='registration_title'><b> - Contacter l'Équipe</b></div><br/><br/>";
        
        $form = "<table class='registration' width='590px'>";
        
        $form .= "<tr><td  width='230px'>" . localize("Votre Adresse E-mail") . "</td><td>" . $this->mainForm->getElement("email") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='email_err' " . $this->mainForm->getTargetCss("email") . ">" . $this->mainForm->getTargetMsg("email") . "</td></tr>\n";
        $form .= "<tr><td  width='230px'>" . localize("Titre :") . "</td><td>" . $this->mainForm->getElement("title") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='title_err' " . $this->mainForm->getTargetCss("title") . ">" . $this->mainForm->getTargetMsg("title") . "</td></tr>\n";
        $form .= "<tr><td  width='230px'>" . localize("Message :") . "</td><td>" . $this->mainForm->getElement("body") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='body_err' " . $this->mainForm->getTargetCss("body") . ">" . $this->mainForm->getTargetMsg("body") . "</td></tr>\n";
        $form .= "<tr><td><img src='../lib/securimage/securimage_show.php' alt=\"country_feint\" /><br /></td>";
        $form .= "<td>" . $this->mainForm->getElement("imgcode") . "</td></tr>\n";
        $form .= "<tr><td></td><td id='imgcode_err' " . $this->mainForm->getTargetCss("imgcode") . ">" . $this->mainForm->getTargetMsg("imgcode") . "</td></tr>\n";
        $form .= "<tr><td>&nbsp;</td><td></td></tr>\n";
        
        $form .= "</table><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $this->mainForm->getElement("submit") . "\n";
        
        return $str . $this->mainForm->getForm($form);
        $str .= "<table class='registration' width='590px'>";
        return $str;
    }

    public function step1()
    {
        $this->mainForm->loadDefaults($_POST);
        include_once (HOMEPATH . "/lib/securimage/securimage.php");
        $securimage = new Securimage();
        
        $valid = $securimage->check($_POST['imgcode']);
        
        $err_array = array();
        
        if ($this->mainForm->validate() && $valid == true && ! $this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $db = $this->db;
            $res = "";
            $this->sendEmail($this->mainForm->get("title"), $this->mainForm->get("body"), $this->mainForm->get("email"), $res);
            
            if ($res == "") {
                $str .= localize("Votre message a correctement été envoyée.");
                $str .= "<br/><br/>";
            } else
                $str .= $res;
            $str .= "</div>";
            
            return $str;
        }
        
        $this->mainForm->setStep(0);
        $this->mainForm->resetValue("imgcode", "");
        
        if ($valid == false) {
            $this->mainForm->setTargetCss("imgcode", "err");
            $this->mainForm->setTargetMsg("imgcode", localize("Code incorrect !"));
        }
        
        if ($this->mainForm->isRepostForm()) {
            $str = "<div class='registration'>";
            $str .= localize("Erreur : Ces données ont déjà été envoyées ou le délai de la page précédente a expriée.");
            $str .= "<br/><br/><input type='button' value='" . localize("Terminer") . "' onclick=\"window.location='" . CONFIG_HOST . "/contact.php'\" />";
            $str .= "</div>";
            return $str;
        } else {
            return $this->step0();
        }
    }

    public function toString()
    {
        $step = $this->mainForm->getStep($_POST);
        $ret = array();
        
        switch ($step) {
            case 1:
                return $this->step1();
                break;
            case 0:
                return $this->step0();
                break;
            
            default:
                printf("empty page <br />\n");
                break;
        }
    }
}

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
$MAIN_PAGE = new HTMLObject("html", "html", 'xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr"');
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head", "head");
$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body", "body", "style='background-color: #000000;'");

$lang = "";
switch (getBrowserDefaultLang()) {
    case "fr-fr":
    case "fr":
        $lang = "fr";
        break;
    default:
        $lang = "en";
        break;
}

$MAIN_HEAD->add('<link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="icon"/><link href="' . CONFIG_HOST . '/favicon.ico" type="image/x-icon" rel="shortcut icon"/>');
$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'/>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "' />\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script type='text/javascript' src='" . Cache::get_cached_file( "/javascript/nacridan.js") . "'></script>\n");

Translation::init('gettext', HOMEPATH . '/i18n/messages', $lang, 'UTF-8', "login", true, HOMEPATH . '/i18n/messages/cache', "");

$CONTENT = $MAIN_BODY->addNewHTMLObject("div", null, "");

// $CENTER->add("<img src='".CONFIG_HOST."/pics/register/frise.jpg' style='margin-top: 10px; margin-bottom: 30px' />");
$contact = new MailContact($MAIN_PAGE, $lang);
$CONTENT->add($contact);
$db = DB::getDB();

$MAIN_PAGE->render();
Translation::saveMessages();

?>