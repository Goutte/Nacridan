<?php
require_once ("../conf/config.ini.php");
require_once (HOMEPATH . "/lib/phplib/Page.inc.php");
require_once (HOMEPATH . "/lib/phplib/Session.inc.php");
require_once (HOMEPATH . "/lib/phplib/Auth.inc.php");
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/lib/HTMLObject.inc.php");
// page_open(array("sess" => "Session", "auth" => "Auth"));

require_once (HOMEPATH . "/include/NacridanModule.inc.php");
require_once (HOMEPATH . "/class/Detail.inc.php");
require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");

require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

$db = DB::getDB();
$nacridan = null;
$lang = "fr";

Translation::init('gettext', '../i18n/messages', $lang, 'UTF-8', "main", true, '../i18n/messages/cache', $filename = "");

$curplayer = null;
$prefix = "";

$MAIN_PAGE = new HTMLObject("html");
$MAIN_HEAD = $MAIN_PAGE->addNewHTMLObject("head");

$MAIN_HEAD->add("<title>Nacridan</title>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridanMain.css") . "'>\n");
$MAIN_HEAD->add("<link rel='stylesheet' type='text/css' href='" . Cache::get_cached_file("/css/nacridan" . $lang . ".css") . "'>\n");
$MAIN_HEAD->add('<script language="javascript" type="text/javascript" src="' . CONFIG_HOST . '/javascript/jquery-3.1.0.min.js"></script>');
$MAIN_HEAD->add("<script language='javascript' type='text/javascript' src='" . Cache::get_cached_file("/javascript/nacridan.js") . "'></script>\n");

$MAIN_BODY = $MAIN_PAGE->addNewHTMLObject("body");

$isBodySet = isset($_GET["body"]);
if ($isBodySet) {
    $bodyValue = $_GET["body"];
} else {
    $bodyValue = 0;
}

if (isset($_GET["id"]))
    $_GET["id"] = quote_smart($_GET["id"]);

if (isset($_POST["search"])) {
    if (is_numeric($_POST["search"])) {
        $_GET["id"] = $_POST["search"];
    } else {
        $dbp = new DBCollection("SELECT id FROM Player WHERE name='" . quote_smart($_POST["search"]) . "'", $db);
        if (! $dbp->eof()) {
            $_GET["id"] = $dbp->get("id");
        }
    }
}

if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
    $id = $_GET["id"];
    $curplayer = new Player();
    $curplayer->externDBObj("BasicRace,Modifier,Team,Detail");
    $curplayer->load($id, $db);
    
    $state = array(
        localize("Agonisant"),
        localize("Gravement blessé(e)"),
        localize("Blessé(e)"),
        localize("Légèrement blessé(e)"),
        localize("Indemne"),
        localize("est mort"),
        localize("vit paisiblement dans son monde")
    );
    
    $hp = $curplayer->get("hp");
    if ($hp != 0) {
        $curstate = min(floor($curplayer->get("currhp") * 5 / $hp), 4);
    } else {
        $dbe = new DBCollection("SELECT * FROM Event WHERE typeAction=605 AND id_Player\$src=" . $id, $db);
        if (! $dbe->eof())
            $curstate = 6;
        else {
            $curstate = 5;
        }
        $bodyValue = 1;
        $isBodySet = true;
    }
    
    $extra = "";
    if (isset($isBodySet)) {
        $extra = "&body=1";
    }
    
    $str = "<body\n>";
    $str .= "<table class='maintable mainbgtitle' width='640px'>\n";
    $str .= "<tr><td>\n";
    if ($curplayer->get("status") == "NPC" || $curplayer->get("id") == 0) {
        $str .= "<b><h1>";
        if ($curstate >= 5)
            $str .= "Ce monstre " . $state[$curstate];
        else
            $str .= localize($curplayer->get('racename'));
        $str .= "</h1></b></td>\n";
        $str .= "<td align='right'>\n";
        $str .= "&nbsp;<br/><br/><form action='?id=" . $id . $extra . "' method='POST'>" . localize("Recherche (n°/nom)") .
             " <input name='search' type='text' size=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</form></td>\n";
    } else {
        $str .= "<b><h1>" . $curplayer->get('name') . "</h1></b></td>\n";
        $str .= "<td align='right'>\n";
        $str .= "&nbsp;<br/><br/><form action='?id=" . $id . $extra . "' method='POST'>" . localize("Recherche (n°/nom)") .
             " <input name='search' type='text' size=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</form></td>\n";
    }
    $str .= "</tr>\n";
    $str .= "</table>\n";
    if ($curplayer->get("status") == "PC" || ($curplayer->get("status") == "NPC" && $curstate < 5)) {
        $str .= "<a href='profile.php?id=" . $id . "' class='tabmenu'>&nbsp;&nbsp;" . localize('I N F O R M A T I O N') . "</a>&nbsp;&nbsp;|&nbsp;&nbsp;\n";
        $str .= "<a href='profile.php?id=" . $id . "&body=1' class='tabmenu'>" . localize('É V È N E M E N T S') . "</a>\n";
        
        if ($curplayer->get("status") == "PC")
            $str .= "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='profile.php?id=" . $id . "&body=2' class='tabmenu'>" . localize('É Q U I P E M E N T') . "</a>\n";
    }
    $str .= "<br/><br/>\n";
    
    if (! $isBodySet) {
        $str .= "<table><tr><td valign='top'>\n";
        $str .= "<table class='maintable' width='270px'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('I N F O R M A T I O N') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        if ($curplayer->get("status") == "NPC")
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('NOM :') . "</b></td><td class='mainbgbody'>" . localize($curplayer->get('name')) . " (" . $_GET["id"] .
                 ")</td></tr>\n";
        else
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('NOM :') . "</b></td><td class='mainbgbody'>" . $curplayer->get('name') . ' (' . $curplayer->get(
                'id') . ')' . "</td></tr>\n";
        
        if ($curplayer->get("id_Team") != 0) {
            $alliegance = "<a href='../main/alliegance.php?id=" . $curplayer->get("id_Team") . "'  class='styleall'>" . $curplayer->getSub('Team', 'name') . "</a>";
            
            $dbrank = new DBCollection(
                "SELECT TeamRankInfo.name FROM TeamRank LEFT JOIN TeamRankInfo ON TeamRankInfo.id=id_TeamRankInfo WHERE id_Player=" . $curplayer->get("id") .
                     " AND TeamRank.id_Team=" . $curplayer->get("id_Team"), $db);
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ALLÉGEANCE :') . "</b></td><td class='mainbgbody'>" . $alliegance . "<br/>" .
                 ($dbrank->count() > 0 ? $dbrank->get('name') : "") . "</td></tr>\n";
        } else {
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ALLÉGEANCE :') . "</b></td><td class='mainbgbody'>" . localize("Aucune") . "</td></tr>\n";
        }
        
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('LEVEL :') . "</b></td><td class='mainbgbody'>" . $curplayer->get('level') . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('CLASSE :') . "</b></td><td class='mainbgbody'>" . localize($curplayer->getSub('BasicRace', 'class')) .
             "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('RACE :') . "</b></td><td class='mainbgbody'>" . localize($curplayer->getSub('BasicRace', 'name')) .
             "</td></tr>\n";
        
        $gender = $curplayer->get('gender');
        if ($gender == "M")
            $gender = "Homme";
        else
            $gender = "Femme";
        
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('SEXE :') . "</b></td><td class='mainbgbody'>" . localize($gender) . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ÉTAT :') . "</b></td><td class='mainbgbody'>" . $state[$curstate] . "</td></tr>\n";
        $time = strtotime($curplayer->get('creation'));
        $curtime = time();
        $nbday = floor(($curtime - $time) / 3600 / 24);
        
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('CRÉATION :') . "</b></td><td class='mainbgbody'>" . gmdate("Y-m-d", $time) . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('ANCIENNETÉ :') . "</b></td><td class='mainbgbody'>" . $nbday . " " . localize("jours") . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('MEURTRE(S) :') . "</b></td><td class='mainbgbody'>" . $curplayer->get('nbkill') . "</td></tr>\n";
        $str .= "<tr><td class='mainbglabel' width='110px'><img src='../pics/misc/sub.gif' class='nostyle'/><b>" . localize('Dont PJ :') . "</b></td><td class='mainbgbody'>" .
             $curplayer->get('nbkillpc') . "</td></tr>\n";
        if ($curplayer->get("rider") != 0)
            $str .= "<tr><td class='mainbglabel' width='130px'><b>" . localize('AGE :') . "</b></td><td bgcolor='#3b302b'>" . $curplayer->get('nbdeath') . "</td></tr>\n";
        else
            $str .= "<tr><td class='mainbglabel' width='110px'><b>" . localize('DÉCÈS :') . "</b></td><td class='mainbgbody'>" . $curplayer->get('nbdeath') . "</td></tr>\n";
        
        $str .= "</table>\n";
        $str .= "</td><td> <img src='" . getImgSrc($curplayer, $db) . "'></td></tr>";
        $str .= "</table>";
        
        $str .= "<br/>\n";
        $str .= "<table class='maintable' width='590px'>\n";
        $str .= "<tr>\n";
        $str .= "<td colspan='2' class='mainbgtitle'><b>" . localize('D E S C R I P T I O N') . "</b>\n";
        $str .= "</td>\n";
        $str .= "</tr>\n";
        if ($curplayer->get("id_Detail") == 0)
            $str .= "<tr><td class='mainbgbody'>&nbsp;" . localize($curplayer->getSub('BasicRace', 'description')) . "</td></tr>\n";
        else
            $str .= "<tr><td class='mainbgbody'>" . bbcode($curplayer->getSub('Detail', 'body')) . "</td></tr>\n";
        
        $str .= "</table>";
        
        $MAIN_BODY->add($str);
    } else {
        $MAIN_BODY->add($str);
        if ($bodyValue == 1) {
            require_once (HOMEPATH . "/conquest/cqevent.inc.php");
            $MAIN_BODY->add(new CQEvent($nacridan, "centerareawidth", $db));
        } else {
            require_once (HOMEPATH . "/class/Equipment.inc.php");
            $str = "<table class='maintable' width='500px'>\n";
            $str .= "<tr>\n";
            $str .= "<td colspan='6' class='mainbgtitle'><b>" . localize('É Q U I P E M E N T &nbsp;&nbsp;&nbsp; U T I L I S É') . "&nbsp;&nbsp;&nbsp;</b>\n";
            $str .= "</td>\n";
            $str .= "</tr>\n";
            
            $eq = new Equipment();
            $eqmodifier = new Modifier();
            $modifier = new Modifier();
            
            $dbe = new DBCollection(
                "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") .
                     ", mask,wearable,BasicEquipment.durability AS dur  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id LEFT JOIN Mission ON Equipment.id_Mission=Mission.id WHERE EquipmentType.small='No' AND Equipment.weared!='No' AND Equipment.id_Player=" .
                     $id, $db);
            
            while (! $dbe->eof()) {
                $eq->DBLoad($dbe, "EQ");
                $eqmodifier->DBLoad($dbe, "EQM");
                $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                
                $extraname = "";
                
                if ($eq->get("extraname") != "") {
                    $i = 0;
                    $tmodifier = new Modifier();
                    $template = new Template();
                    $dbt = new DBCollection(
                        "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") . "," .
                             $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                             " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                             $eq->get("id") . " order by Template.pos asc", $db);
                    while (! $dbt->eof()) {
                        $i ++;
                        $template->DBLoad($dbt, "TP");
                        $tmodifier->DBLoad($dbt, "MD");
                        $tmodifier->updateFromTemplateLevel($template->get("level"));
                        $eqmodifier->addModifObj($tmodifier);
                        
                        if ($i == 1)
                            $extraname = $dbt->get("name1") . " ";
                        if ($i == 2)
                            $extraname .= $dbt->get("name2");
                        
                        $dbt->next();
                    }
                }
                $eqmodifier->initCharacStr();
                
                $str .= "<tr>";
                $str .= "<td class='mainbgbody'><b><span class='" . ($eq->get("maudit") == 1 ? "maudit" : "") . "'>" . localize($eq->get("name")) . "</span></b>";
                
                if ($eq->get("extraname") != "") {
                    $str .= " <span class='template'>" . localize($extraname) . "</span>";
                }
                $str .= "</td></tr>";
                
                $dbe->next();
            }
            
            $str .= "</table>\n";
            $MAIN_BODY->add($str);
        }
    }
    $MAIN_PAGE->render();
}
