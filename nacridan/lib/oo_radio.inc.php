<?php

class oo_radio extends oo_element
{

    public $m_values;

    public $m_valid_e;

    public $m_checked;
    
    // Constructor
    function oo_radio($a)
    {
        $this->setup_element($a);
        
        $this->m_values = array();
    }

    function addValue($val)
    {
        $this->m_values[] = $val;
    }

    function self_get($val)
    {
        $str = "";
        $str .= "<input type='radio' name='$this->m_name' value='$val'";
        
        if ($this->m_errorclass && $this->m_error == true) {
            $str .= " class='$this->m_errorclass'";
        }
        
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        if ($this->m_checked == $val) {
            $str .= " checked=\"checked\"";
        }
        $str .= "/>";
        
        return $str;
    }

    function self_validate($val)
    {
        if ($this->m_valid_e && ! isset($this->m_checked)) {
            return $this->m_valid_e;
        }
        
        foreach ($this->m_values as $key => $data) {
            if ($data == $this->m_checked)
                return false;
        }
        return $this->m_valid_e;
    }

    function self_load_default($val)
    {
        $this->m_value = $val;
        $this->m_checked = $val;
    }
} // end RADIO

?>
