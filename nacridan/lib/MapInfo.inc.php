<?php
require_once (HOMEPATH . "/lib/DB.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");
require_once (HOMEPATH . "/lib/MinimapCase.inc.php");

class MapInfo
{

    public $m_db;

    function MapInfo($db)
    {
        $this->m_db = $db;
    }

    function getValidMap($x, $y, $len, $map)
    {
        $dbnpc = new DBCollection(
            "
	SELECT x,y,map,color,ground,cost_PA,label FROM
	(SELECT x,y,map,id_Ground,wayCount FROM  MapGround WHERE map=" .
                 $map . " AND x<=(" . ($len) . "+" . $x . ") AND x>=(-" . ($len) . "+" . $x . ") AND y<=(" . ($len) . "+" . $y . ") AND y>=(-" . ($len) . "+" . $y . ")) as MapGround 
	left outer join GroundEvolution on id_Ground = id_GroundInit and wayCount between startCount and endCount
	inner join Ground on Ground.id = ifnull(id_GroundReplace,id_Ground)	", $this->m_db, 0, 0);
        
        $minimap = array();
        while (! $dbnpc->eof()) {
            $x1 = $dbnpc->get("x");
            $y1 = $dbnpc->get("y");
            $ground = $dbnpc->get("ground");
            if ($ground != "river" && $ground != "ocean") {
                $minimap[$y1 - $y + $len][$x1 - $x + $len] = 1;
            } else {
                $minimap[$y1 - $y + $len][$x1 - $x + $len] = 0;
            }
            $dbnpc->next();
        }
        return $minimap;
    }

    function getValidMapCost($x, $y, $len, $map)
    {
        $dbnpc = new DBCollection(
            "
	SELECT x,y,map,color,ground,cost_PA,label FROM
	(SELECT x,y,map,id_Ground,wayCount FROM  MapGround WHERE map=" .
                 $map . " AND x<=(" . ($len) . "+" . $x . ") AND x>=(-" . ($len) . "+" . $x . ") AND y<=(" . ($len) . "+" . $y . ") AND y>=(-" . ($len) . "+" . $y . ")) as MapGround 
	left outer join GroundEvolution on id_Ground = id_GroundInit and wayCount between startCount and endCount
	inner join Ground on Ground.id = ifnull(id_GroundReplace,id_Ground)	", $this->m_db, 0, 0);
        
        $minimap = array();
        while (! $dbnpc->eof()) {
            $x1 = $dbnpc->get("x");
            $y1 = $dbnpc->get("y");
            $ground = $dbnpc->get("ground");
            $result = array();
            $result["ground"] = $ground;
            $result["cost_EL"] = 0;
            $result["EL_type"] = "";
            if ($ground != "river" && $ground != "ocean") {
                $result["cost_PA"] = $dbnpc->get("cost_PA");
            } else {
                $result["cost_PA"] = 100000;
            }
            $minimap[$x1][$y1] = $result;
            $dbnpc->next();
        }
        return $minimap;
    }

    function getMap($x, $y, $len, $map)
    {
        $dbnpc = new DBCollection(
            "
	SELECT x,y,map,color,ground,cost_PA,label FROM
	(SELECT x,y,map,id_Ground,wayCount FROM  MapGround WHERE map=" .
                 $map . " AND x<=" . ($len) . "+" . $x . " AND x>=-" . ($len) . "+" . $x . " AND y<=" . ($len) . "+" . $y . " AND y>=-" . ($len) . "+" . $y . ") as MapGround 
	left outer join GroundEvolution on id_Ground = id_GroundInit and wayCount between startCount and endCount
	inner join Ground on Ground.id = ifnull(id_GroundReplace,id_Ground)	", $this->m_db, 0, 0);
        
        $minimap = array();
        while (! $dbnpc->eof()) {
            $x1 = $dbnpc->get("x");
            $y1 = $dbnpc->get("y");
            $minimap[$y1 - $y + $len][$x1 - $x + $len] = new MiniMapCase($dbnpc->get("ground"), $dbnpc->get("label"), $dbnpc->get("cost_PA"), $x1, $y1);
            $dbnpc->next();
        }
        return $minimap;
    }

    function updateWayCount($x, $y, $map, $playerSrc)
    {
        $valueWayCount = 0;
        if ($playerSrc->get("status") == 'NPC') {
            $valueWayCount = 0;
            if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
                $valueWayCount = 1;
            }
            if ($playerSrc->get("id_BasicRace") == 263) {
                $valueWayCount = 3;
            }
        } else {
            $valueWayCount = 1;
            if ($playerSrc->get("state") == "turtle") {
                $valueWayCount = 3;
            }
        }
        $dbnpc = new DBCollection(
            "UPDATE MapGround set wayCount = CASE WHEN wayCount >= 60 or (wayCount + " . $valueWayCount . ") >= 60 THEN 60 else (wayCount + " . $valueWayCount . ") END where x= " .
                 $x . " and y=" . $y . " and map = " . $map, $this->m_db, 0, 0);
    }
}
?> 