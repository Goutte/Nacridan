<?php
require_once ("ooform.inc.php");

class TplForm
{

    var $m_formname;

    var $m_classname = "TplForm";

    var $m_formData;

    var $m_fields;

    var $m_labels;

    var $m_radiolabels;

    var $m_error = "";

    var $m_hasDefaults = 0;

    var $m_tplstep;

    var $m_action;

    var $m_target;

    var $m_targetmsg;

    var $m_targetcss;

    var $m_targeterr;

    var $m_submits;

    var $m_radios;

    var $m_token;

    var $m_isrepostform;

    function TplForm($formname = "form", $session = null, $action = "", $target = "")
    {
        $this->m_action = $action;
        $this->m_target = $target;
        
        $this->m_formData = new ooform();
        $this->m_fields = array();
        $this->m_radios = array();
        $this->m_radiolabels = array();
        $this->m_submits = array();
        $this->m_formname = $formname;
        $this->m_tplstep = 0;
        $this->m_targetmsg = array();
        $this->m_targetcss = array();
        $this->m_targeterr = array();
        
        if ($session != null) {
            $previousIdForm = "";
            $sendIdForm = "NOVALUE";
            
            if (isset($_POST[$this->m_formname . "_idform"])) {
                $previousIdForm = $session->get($this->m_formname . "_idform");
                $sendIdForm = $_POST[$this->m_formname . "_idform"];
            }
            
            $pageid = $this->m_formname . "_idform";
            $this->m_token = md5(uniqid(mt_rand(), true));
            $$pageid = $this->m_token;
            
            $session->set($pageid, $$pageid);
            
            if ($previousIdForm == $sendIdForm) {
                $this->m_isrepostform = false;
            } else {
                $this->m_isrepostform = true;
            }
        }
    }

    function isRepostForm()
    {
        return $this->m_isrepostform;
    }

    function setAction($action)
    {
        $this->m_action = $action;
    }

    function setTarget($target)
    {
        $this->m_target = $target;
    }

    function addElement($tab)
    {
        if (! isset($tab["name"]))
            return false;
        
        if ($tab["type"] == "ignore")
            return true;
        
        $name = $tab["name"];
        $this->m_targetmsg[$name] = "";
        $this->m_targeterr[$name] = "";
        $this->m_targetcss[$name] = "";
        
        if (isset($tab["targetmsg"]))
            $this->m_targetmsg[$name] = $tab["targetmsg"];
        if (isset($tab["targetcss"]))
            $this->m_targetcss[$name] = $tab["targetcss"];
        if (isset($tab["targeterr"]))
            $this->m_targeterr[$name] = $tab["targeterr"];
        
        switch ($tab["type"]) {
            case "radio":
                $this->m_radios[$tab["name"]][] = $tab["value"];
                $this->m_radiolabels[$tab["name"]][$tab["value"]] = $tab["radiolabel"];
                break;
            case "submit":
                $this->m_submits[$tab["name"]][] = $tab["value"];
                break;
            default:
                if (isset($tab["type"])) {
                    $this->m_fields[$tab["name"]] = $tab["type"];
                }
                break;
        }
        
        if (isset($tab["label"])) {
            $this->m_labels[$name] = $tab["label"];
        }
        
        if ($tab["type"] == "separator")
            return true;
        
        $name = $tab["name"];
        if (isset($tab["check"])) {
            $tab["check"] = 0;
            $this->m_formData->add_element($tab);
            $tab["name"] = $tab["name"] . "check";
            $tab["label"] = $tab["label"] . " (2)";
            $tab["check"] = 1;
            if (isset($tab["check_e"])) {
                $tab["valid_e"] = $tab["check_e"];
                unset($tab["check_e"]);
            }
            unset($tab["valid_regex"]);
            $this->m_formData->add_element($tab);
            if (isset($tab["type"])) {
                $this->m_fields[$tab["name"]] = $tab["type"];
                
                $this->m_labels[$tab["name"]] = $tab["label"];
            }
        } else {
            $this->m_formData->add_element($tab);
        }
        
        return true;
    }

    function getElement($name, $value = "")
    {
        return $this->m_formData->get_element($name, $value);
    }

    function addElements($fields)
    {
        foreach ($fields as $key => $data) {
            $tab = array();
            
            foreach ($data as $val1 => $val2) {
                $tab[$val1] = $val2;
            }
            $this->addElement($tab);
        }
    }

    function getStep($array)
    {
        if (isset($array[$this->m_formname . "_tplstep"])) {
            $this->m_tplstep = $array[$this->m_formname . "_tplstep"];
        }
        return $this->m_tplstep;
    }

    function get($varname)
    {
        if (isset($this->m_formData->m_elements[$varname]["ob"])) {
            $el = $this->m_formData->m_elements[$varname]["ob"];
            return $el->m_value;
        }
        return NULL;
    }

    function getTargetMsg($varname)
    {
        if ($this->m_targetmsg[$varname] == "")
            return "&nbsp;";
        else
            return $this->m_targetmsg[$varname];
    }

    function getTargetCss($varname)
    {
        return "class='" . $this->m_targetcss[$varname] . "'";
    }

    function setTargetMsg($varname, $value)
    {
        $this->m_targetmsg[$varname] = $value;
    }

    function setTargetCss($varname, $value)
    {
        $this->m_targetcss[$varname] = $value;
    }

    function resetValue($varname, $val)
    {
        if (isset($this->m_formData->m_elements[$varname]["ob"])) {
            $el = $this->m_formData->m_elements[$varname]["ob"];
            $el->m_value = $val;
            $this->m_formData->m_elements[$varname]["ob"] = $el;
        }
        return NULL;
    }

    function setStep($val)
    {
        $this->m_tplstep = $val;
    }

    function getNextStep()
    {
        return "<script type==\"text/javascript\">document.{$this->m_formname}.submit();</script>\n";
    }

    function nextStep()
    {
        echo $this->getNextStep();
    }

    function loadDefaults($array)
    {
        if (isset($array[$this->m_formname . "_tplstep"])) {
            $this->m_tplstep = $array[$this->m_formname . "_tplstep"];
        }
        $this->m_formData->load_defaults($array);
    }

    function validate()
    {
        $ret = true;
        $array = $this->m_formData->validate();
        foreach ($array as $key => $val) {
            $this->m_targetmsg[$key] = $val;
            $this->m_targetcss[$key] = $this->m_targeterr[$key];
            $ret = false;
        }
        return $ret;
    }

    function addHidden($array)
    {
        foreach ($array as $key => $val) {
            if (! isset($this->m_fields[$key]) && ! isset($this->m_radios[$key]) && ! isset($this->m_submits[$key])) {
                if ($key != $this->m_formname . "_tplstep") {
                    $this->m_formData->add_element(array(
                        "name" => $key,
                        "type" => "hidden",
                        "value" => $val
                    ));
                }
            }
        }
    }

    function clear()
    {
        $this->m_fields = array();
        $this->m_radios = array();
        $this->m_submits = array();
        $this->m_formData->clear();
    }

    function getJsValidation()
    {
        return $this->m_formData->gen_js_validation("validateStandard(this, 'error');", $this->m_formname);
    }

    /*
     * function displayHiddenForm($array=array()) {
     * $this->m_formData->add_element(array("name"=>"{$this->m_formname}_tplstep","type"=>"hidden","value"=>$this->m_tplstep+1));
     * $this->m_formData->start("POST",$this->m_action,$this->m_target,$this->m_formname);
     *
     * foreach($array as $key => $val)
     * {
     * if($key!=$this->m_formname."_tplstep")
     * {
     * printf("<input type='hidden' name='$key' value='$val'>");
     * }
     * }
     *
     * $this->m_formData->finish();
     * }
     *
     * function finish() {
     * $this->m_formData->finish();
     * }
     */
    function getForm($template = "")
    {
        $this->m_formData->add_element(array(
            "name" => "{$this->m_formname}_tplstep",
            "type" => "hidden",
            "value" => $this->m_tplstep + 1
        ));
        $str = $this->m_formData->get_start("post", $this->m_action, $this->m_target, $this->m_formname);
        
        if ($template == "") {
            foreach ($this->m_fields as $key => $data) {
                $str .= $this->m_formData->get_element($key) . "<br/>";
            }
            foreach ($this->m_radios as $key => $data) {
                foreach ($data as $value) {
                    $str .= $this->m_formData->get_element($key, $value) . "<br/>";
                }
            }
            foreach ($this->m_submits as $key => $data) {
                foreach ($data as $value) {
                    $str .= $this->m_formData->get_element($key, $value) . "<br/>";
                }
            }
        } else {
            $str .= $template;
        }
        
        $str .= "<input name='" . $this->m_formname . "_idform' type='hidden' value='" . $this->m_token . "' />\n";
        
        $str .= $this->m_formData->get_finish();
        return $str;
    }
}
?>