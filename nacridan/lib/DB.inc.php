<?php

class DB
{

    static $m_errorno;

    static $m_errormsg;

    static function getDB()
    {
        static $db = null;
        
        try {
            $strConnection = 'mysql:host=' . MYSQLHOST . ';dbname=' . DB; // Ligne 1
            $arrExtraParam = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ); // Ligne 2
            $db = new PDO($strConnection, LOGIN, PASSWD, $arrExtraParam); // Ligne 3; Instancie la connexion
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Ligne 4
        } catch (PDOException $e) {
            $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
            die($msg);
        }
        return $db;
    }

    static function getGGDB()
    {
        static $db = null;
        
        try {
            $strConnection = 'mysql:host=' . MYSQLHOST . ';dbname=' . DBGG; // Ligne 1
            $arrExtraParam = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ); // Ligne 2
            $db = new PDO($connStr, LOGIN, PASSWD, $arrExtraParam); // Ligne 3; Instancie la connexion
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Ligne 4
        } catch (PDOException $e) {
            $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
            die($msg);
        }
        return $db;
    }
}

?>