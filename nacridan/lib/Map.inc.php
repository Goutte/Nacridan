<?php
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/lib/Land.inc.php");
require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
require_once (HOMEPATH . "/conquest/cqdef.inc.php");

DEFINE("GATE_MIN_NPC", 3);
DEFINE("GATE_MAX_NPC", 8);

DEFINE("GATE_ACTIVATION_TIME_MIN", 10);
DEFINE("GATE_ACTIVATION_DIFF", 4);
DEFINE("NPC_ACTIVATION_TIME", 6);
DEFINE("NPC_ACTIVATION_DIFF", 2);

class Map
{

    public $m_land;

    public $m_db;

    public $m_imgarea;

    public $m_mapinfo;

    public $m_mapstartkingdom;

    public $m_map;

    function Map($database, $map)
    {
        
        /*
         * $this->m_land["mountain"]=new Land("mountain",$database);
         * $this->m_land["ocean"]=new Land("ocean",$database);
         * $this->m_land["marsh"]=new Land("marsh",$database);
         * $this->m_land["forest"]=new Land("forest",$database);
         * $this->m_land["river"]=new Land("river",$database);
         * $this->m_land["desert"]=new Land("desert",$database);
         * $this->m_land["underground"]=new Land("underground",$database);
         * $this->m_land["polar"]=new Land("polar",$database);
         */
        $this->m_db = $database;
        $this->m_map = $map;
        $this->m_imgarea = imagecreatefrompng(HOMEPATH . MAPS_STATIC_FOLDER . "/maparea0" . $map . ".png");
        $this->m_mapinfo = new MapInfo($this->m_db);
        $this->m_mapstartkingdom = imagecreatefrompng(HOMEPATH . MAPS_STATIC_FOLDER . "/mapghostkingdom0" . $map . ".png");
    }

    /* ********************************************************** FONCTION AUXILLIAIRES *********************************************************** */
    function getNumberNPC($map, $x, $y, $gap)
    {
        $dbnpc = new DBCollection(
            "SELECT id FROM Player WHERE status='NPC' AND hidden=0 AND id_Member=0 AND id_BasicRace!=253 AND disabled=0 AND map=" . $map . " AND (abs(x-" . $x . ") + abs(y-" . $y .
                 ") + abs(x+y-" . $x . "-" . $y . "))/2 <=" . $gap, $this->m_db, 0, 0);
        return $dbnpc->count();
    }

    function getNumberPC($map, $x, $y, $gap)
    {
        $dbnpc = new DBCollection(
            "SELECT id FROM Player WHERE status='PC' AND hidden =0 AND disabled=0 AND map=" . $map . " AND (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y .
                 "))/2 <=" . $gap, $this->m_db, 0, 0);
        return $dbnpc->count();
    }

    function getNumberGate($map, $x, $y, $gap)
    {
        $dbnpc = new DBCollection("SELECT id FROM Gate WHERE map=" . $map . " AND (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <=" . $gap, $this->m_db, 
            0, 0);
        return $dbnpc->count();
    }

    function getTypeLandFromMap($i, $j, $map)
    {
        $minimap = $this->m_mapinfo->getMap($i, $j, 0, $map);
        return $minimap[0][0]->type;
    }
    
    // ---- à changer avec nouvelle zone de niveau.
    function getLevelMinFromMap($i, $j)
    {
        $zone = self::getZoneFromMap($i, $j);
        switch ($zone) {
            case 1:
                $level = 1;
                break;
            case 2:
                $level = 3;
                break;
            case 3:
                $level = 5;
                break;
            case 4:
                $level = 7;
                break;
            case 5:
                $level = 10;
                break;
            case 6:
                $level = 15;
                break;
            case 7:
                $level = 25;
                break;
            case 8:
                $level = 37;
                break;
            case 9:
                $level = 51;
                break;
            case 10:
                $level = 67;
                break;
            case 11:
                $level = 85;
                break;
            default:
                $level = 600;
                break;
        }
        return $level;
    }

    function getLevelMeanFromMap($i, $j)
    {
        $zone = self::getZoneFromMap($i, $j);
        switch ($zone) {
            case 1:
                $level = 1;
                break; // 1 ou 2
            case 2:
                $level = 3;
                break; // 3 ou 4
            case 3:
                $level = 5;
                break; // 5 ou 6
            case 4:
                $level = 7;
                break; // 7,8,9
            case 5:
                $level = 12;
                break; // 10->12 ou 11->13 ou 12->14
            case 6:
                $level = 21;
                break; // 15->18 ou 16->19 ou 17->20 ou 18->21 ou 19->22
            case 7:
                $level = 31;
                break; // 25->29 ou 26->30 ou 27->31 ou 28-> 32 ou 29 -> 34 ou 30 -> 35 ou 31 -> 36
            case 8:
                $level = 43;
                break; // 37-> 43 ou 38 -> 44 ou 39 -> 45 ou 40 ->46 ou 41-> 47 ou 42 -> 48 ou 43-> 50
            case 9:
                $level = 57;
                break; // 51 -> 59 57 -> 66
            case 10:
                $level = 73;
                break; // 67 -> 77 73 -> 84
            case 11:
                $level = 87;
                break; // 85 -> 98 87 -> 100
            default:
                $level = 600;
                break;
        }
        return $level;
    }

    function getLevelGate($xpos, $ypos)
    {
        $max = 0;
        $min = 200;
        $mean = 0;
        $nb = 0;
        for ($i = - 4; $i < 5; $i ++) {
            for ($j = - 4; $j < 5; $j ++) {
                $level = self::getLevelMinFromMap($xpos + $i, $ypos + $j);
                if ($level != 600) {
                    $nb = $nb + 1;
                    $mean += $level;
                    if ($level > $max)
                        $max = $level;
                    if ($level < $min)
                        $min = $level;
                }
            }
        }
        
        $mean = floor($mean / $nb);
        echo $min . "-" . $max . "-" . $mean;
        return $mean;
    }

    function getZoneFromMap($i, $j)
    {
        $validzone = $this->m_mapinfo->getValidMap($i, $j, 0, $this->m_map);
        if ($validzone[0][0] == 0)
            return;
        
        $rgb = imagecolorat($this->m_imgarea, $i, $j);
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;
        $zone = floor($r / 10);
        return $zone;
    }

    function getIdBand($level, $typeLand)
    {
        $dbc = new DBCollection(
            "SELECT band FROM BasicRace WHERE PC='No' AND levelMin <=" . $level . " AND levelMax>=" . $level . " AND (land ='" . $typeLand . "' OR land='plain') group by band", 
            $this->m_db);
        if ($dbc->eof())
            return 0;
        
        $alea = mt_rand(1, $dbc->count());
        for ($i = 1; $i < $alea; $i ++)
            $dbc->next();
        
        return $dbc->get("band");
    }

    function getAleaFreePlace($x, $y, $map, $gap, $object = 1)
    {
        $place = array();
        $k = 0;
        for ($i = - $gap; $i < $gap + 1; $i ++) {
            for ($j = - $gap; $j < $gap + 1; $j ++) {
                if (($x + $i) >= 0 && ($x + $i) < MAPWIDTH && ($y + $j) >= 0 && ($y + $j) < MAPHEIGHT && BasicActionFactory::freePlace($x + $i, $y + $j, $map, $this->m_db)) {
                    $place[$k]["x"] = $x + $i;
                    $place[$k]["y"] = $y + $j;
                    $k ++;
                }
            }
        }
        if ($k == 0)
            return 0;
        
        $choice = mt_rand(0, $k - 1);
        return $place[$choice];
    }

    /* ********************************************************* FONCTION PARCOURRU PAR L'IA ************************************************** */
    function createGate($map)
    {
        // la limite en x et y est pour l'arene
        $dbplayer = new DBCollection("SELECT id,x,y FROM Player WHERE map=" . $map . " AND status='PC' AND hidden<10 and disabled=0 AND ((x<590) OR (y<220))", $this->m_db, 0, 0);
        
        while (! $dbplayer->eof()) {
            $x = $dbplayer->get("x");
            $y = $dbplayer->get("y");
            $validzone = $this->m_mapinfo->getValidMap($x, $y, 0, $map);
            
            $nbpc = $this->getNumberPC($map, $x, $y, 5);
            $nbnpc = $this->getNumberNPC($map, $x, $y, 5);
            $nbgate = $this->getNumberGate($map, $x, $y, 8);
            
            if ($nbpc >= $nbnpc && $nbpc > 0 && $nbgate == 0 && $validzone[0][0] == 1) {
                echo "creation Gate en " . $x . "," . $y . ": map->" . $map . "\n";
                // Choix alétoire d'une case libre à distance max de 3
                $place = self::getAleaFreePlace($x, $y, $map, 3, 1);
                $xpos = $place["x"];
                $ypos = $place["y"];
                
                $level = self::getLevelGate($xpos, $ypos);
                $distancetemple = 8;
                if ($level > 15) {
                    $distancetemple = 11;
                }
                // Pas de création si on est à moins de 3 cases des murs d'un village
                $dbb = new DBCollection(
                    "SELECT * FROM Building WHERE id_BasicBuilding = 14 AND map =" . $map . " AND (abs(x-" . $xpos . ") + abs(y-" . $ypos . ") + abs(x+y-" . $xpos . "-" . $ypos .
                         "))/2 <=" . $distancetemple, $this->m_db);
                
                if ($place != 0 && $dbb->eof()) {
                    // Création de la gate
                    echo "\nID=" . $dbplayer->get("id");
                    echo " PC=" . $nbpc;
                    echo " NPC=" . $nbnpc;
                    echo " NBGate=" . $nbgate;
                    echo " NEW GATE : Map : " . $map . " / Position X :" . $xpos . " / Y :" . $ypos . " \n";
                    $gate = new Gate();
                    
                    $gate->set("level", $level);
                    // $gate->set("level",mt_rand($this->getLevelMinFromMap($xpos,$ypos), $this->getLevelMeanFromMap($xpos,$ypos)));
                    $gate->set("nbNPC", 3 + floor($gate->get("level") / 7));
                    $gate->set("x", $xpos);
                    $gate->set("y", $ypos);
                    $gate->set("map", $map);
                    $time = time();
                    $time += (GATE_ACTIVATION_TIME_MIN * 3600 + GATE_ACTIVATION_DIFF * rand(0, 3600));
                    $gate->set("activation", gmdate("Y-m-d H:i:s", $time));
                    
                    $idBand = self::getIdBand($level, $this->getTypeLandFromMap($xpos, $ypos, $map));
                    $gate->set("npcBand", $idBand);
                    $gate->addDB($this->m_db);
                }
                // else
                // echo "Pas de gate car trop près d'un village ou pas de case libre\n";
            }
            $dbplayer->next();
        }
    }

    function gateActivation($map)
    {
        // test
        // $dbgate= new DBCollection("SELECT * FROM Gate WHERE map=".$map,$this->m_db,0,0,0);
        $dbgate = new DBCollection("SELECT * FROM Gate WHERE map=" . $map . " AND activation<\"" . gmdate("Y-m-d H:i:s") . "\"", $this->m_db, 0, 0);
        while (! $dbgate->eof()) {
            $gate = $dbgate->getCurObject("Gate");
            $x = $gate->get("x");
            $y = $gate->get("y");
            $map = $gate->get("map");
            
            $nbpc = $this->getNumberPC($map, $x, $y, 5);
            $nbnpc = $this->getNumberNPC($map, $x, $y, 7);
            if ($dbgate->get("nbNPC") <= 0) {
                $gate->deleteDB($this->m_db);
            } else {
                if ($nbpc == 0) {
                    if ($nbnpc == 0)
                        self::CreateNPC($x, $y, $map, $gate->get("id"), $gate->get("level"), $gate->get("npcBand"), 1);
                    
                    $gate->deleteDB($this->m_db);
                } else {
                    $dbnpc = new DBCollection(
                        "SELECT * FROM Player WHERE id_TEAM=" . (- $dbgate->get("id")) . " AND (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <=8", 
                        $this->m_db);
                    $NPCleft = $dbnpc->count();
                    
                    $delta = $nbpc - $nbnpc;
                    if ($delta >= 0) {
                        $bm = mt_rand(0, 1);
                        self::CreateNPC($x, $y, $map, $gate->get("id"), $gate->get("level"), $gate->get("npcBand"), $delta + $bm);
                        if ($delta + $bm > 0)
                            $gate->set("nbNPC", $gate->get("nbNPC") - 1);
                    }
                    
                    $time = time();
                    $time += GATE_ACTIVATION_TIME_MIN * 3600 + GATE_ACTIVATION_TIME_MIN * rand(0, 3600);
                    
                    if ($gate->get("nbNPC") == 0)
                        $time += GATE_ACTIVATION_TIME_MIN * 3600 + GATE_ACTIVATION_TIME_MIN * rand(0, 3600);
                    
                    $gate->set("activation", gmdate("Y-m-d H:i:s", $time));
                    $gate->updateDB($this->m_db);
                }
            }
            $dbgate->next();
        }
    }

    function CreateNPC($x, $y, $map, $id_Gate, $level, $band, $nb)
    {
        if ($band == 18) {
            $datetime = time();
            $dbc = new DBCollection("SELECT * FROM BasicRace WHERE band =" . $band, $this->m_db);
            while (! $dbc->eof()) {
                $place = self::getAleaFreePlace($x, $y, $map, 2);
                $xpos = $place["x"];
                $ypos = $place["y"];
                
                echo "Creation monstre id=" . $dbc->get("id") . " level=100";
                self::insertNPC(100, $dbc->get("id"), $xpos, $ypos, $map, $id_Gate);
                
                // $npc->set("curratb",gmdate("Y-m-d H:i:s",$datetime-3600));
                // $npc->set("playatb",gmdate("Y-m-d H:i:s",$datetime-3600));
                // $npc->set("nextatb",gmdate("Y-m-d H:i:s",$datetime-3600));
                
                $dbc->next();
            }
        } else {
            while ($nb > 0) {
                // détermination de la race du monstre parmi band
                $dbc = new DBCollection("SELECT * FROM BasicRace WHERE band =" . $band, $this->m_db);
                $vector = new VectorProba();
                while (! $dbc->eof()) {
                    $vector->pushBack($dbc->get("id"), $dbc->get("frequency"));
                    $dbc->next();
                }
                
                $vector->setInterval(PREC);
                $index = $vector->searchUpperOrEqual(rand(1, PREC));
                if ($index >= 0)
                    $ret = $vector->getKey($index);
                else
                    $ret = 253; // Kradjeck ferreux
                
                $pnjLevel = $level + mt_rand(0, ceil($level / 7));
                
                $place = self::getAleaFreePlace($x, $y, $map, 3);
                $xpos = $place["x"];
                $ypos = $place["y"];
                
                echo "Creation monstre id=" . $ret . " level=" . $pnjLevel . ":";
                self::insertNPC($pnjLevel, $ret, $xpos, $ypos, $map, $id_Gate);
                
                $nb --;
            }
        }
    }

    function insertNPC($level, $idrace, $x, $y, $map, $id_Team)
    {
        $npc = new Player();
        $npc = PNJFactory::getPNJFromLevel($idrace, $level, $this->m_db);
        $npc->set("x", $x);
        $npc->set("y", $y);
        $npc->set("map", $map);
        $npc->set("resurrectx", $x);
        $npc->set("resurrecty", $y);
        
        $npc->set("id_Team", - $id_Team);
        // $npc->set("id_Member", 1);
        
        if ($level <= 2 && $idrace != 253) {
            $npc->set("behaviour", 1);
            $npc->set("fightStyle", $npc->get("fightStyle") + 10);
        }
        
        $datetime = time();
        $npc->set("creation", gmdate("Y-m-d H:i:s", $datetime));
        $datetime += NPC_ACTIVATION_TIME * 3600 + NPC_ACTIVATION_DIFF * mt_rand(0, 3600);
        $npc->set("curratb", gmdate("Y-m-d H:i:s", $datetime - 3600));
        $npc->set("playatb", gmdate("Y-m-d H:i:s", $datetime - 3600));
        $npc->set("nextatb", gmdate("Y-m-d H:i:s", $datetime - 3600));
        
        PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $this->m_db);
        
        $idnpc = $npc->addDBr($this->m_db);
        $npc->set("id", $idnpc);
        PlayerFactory::NewATB($npc, $param, $this->m_db);
        $this->errorDB($npc->errorNoDB());
        
        return $idnpc;
    }

    function getStartKingdomIdFromMap($i, $j)
    {
        $rgb = imagecolorat($this->m_mapstartkingdom, $i, $j);
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;
        
        $str = $r . " " . $g . " " . $b;
        
        $id = ID_UNKNOWN_KINGDOM;
        switch ($str) {
            case COLOR_EAROK:
                $id = ID_EAROK;
                break;
            case COLOR_ARTASSE:
                $id = ID_ARTASSE;
                break;
            case COLOR_TONAK:
                $id = ID_TONAK;
                break;
            default:
                $id = ID_UNKNOWN_KINGDOM;
                break;
        }
        return $id;
    }

    function getStartKingdomNameFromMap($i, $j)
    {
        $id = self::getStartKingdomIdFromMap($i, $j);
        switch ($id) {
            case ID_EAROK:
                $name = localize("Earok");
                break;
            case ID_ARTASSE:
                $name = localize("Artasse");
                break;
            case ID_TONAK:
                $name = localize("Tonak");
                break;
            default:
                $name = localize("");
                break;
        }
        return $name;
    }

    function errorDB($num)
    {
        if ($num) {
            if ($num == 1062) 

            {
                trigger_error("Erreur insertion PNJ/Equipement (doublon)", E_USER_ERROR);
            } else 

            {
                trigger_error("Impossible d'ajouter un nouveau PNJ/Equipement dans la base de donnée.", E_USER_ERROR);
            }
        }
    }
}
?>
