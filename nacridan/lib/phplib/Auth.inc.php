<?php

class Auth
{

    public $classname = "Auth";

    public $lifetime = 45;
    // # Max allowed idle time before
    public $auth = array();
    // # Data array
    public $in = false;

    public $db;

    public $database_table = "Member";

    function start()
    {
        global $sess;
        
        if ($this->isAuthenticated()) {
            $this->auth["exp"] = time() + (60 * $this->lifetime);
        } else {
            if ($this->validateLogin()) {
                $this->auth["exp"] = time() + (60 * $this->lifetime);
            } else {
                if (isset($this->auth["uid"]) && $this->auth["uid"] != "") { // session expirée
                    $this->unAuth();
                    $this->loginForm();
                } else {
                    $this->unAuth();
                    if (isset($_POST["username"]) && isset($_POST["password"])) { // mauvais mot de passe
                        $this->loginForm();
                    } else { // accès à la page d'accueil
                        $this->homepageForm();
                    }
                }
                exit();
            }
        }
    }

    function unAuth($nobody = false)
    {
        $this->auth["uid"] = "";
        $this->auth["perm"] = "";
        $this->auth["exp"] = 0;
    }

    function logout($nobody = "")
    {
        global $sess;
        
        $sess->clear("auth");
        $sess->clear("id_member");
        $sess->set("connect", false);
        unset($this->auth["uname"]);
        $this->unAuth($nobody);
    }
    
    // Use an own login form
    function loginForm()
    {
        global $sess;
        redirect(CONFIG_HOST . "/include/login.inc.php", true);
        // include (HOMEPATH . "/include/login.inc.php");
        // include (HOMEPATH . "/main/homepage.php");
    }

    function homepageForm()
    {
        global $sess;
        include (HOMEPATH . "/main/homepage.php");
    }

    function validateLogin()
    {
        global $username, $password, $sess;
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            $username = quote_smart($_POST["username"]);
            $password = quote_smart($_POST["password"]);
            
            $sess->start();
            $sess->set("register", true);
            $sess->set("connect", true);
            // $sess->destroy();
        } else {
            $sess->set("connect", false);
            return 0;
        }
        
        $this->auth["uname"] = $username;
        $uid = false;
        
        $query = sprintf(
            "select * from %s LEFT JOIN MemberOption ON MemberOption.id_Member=Member.id LEFT JOIN MemberDetail ON MemberDetail.id_Member=Member.id WHERE  Member.login = '%s' AND Member.password = '%s'", 
            $this->database_table, $username, $password);
        $db = DB::getDB();
        $dbc = new DBCollection($query, $db);
        
        while (! $dbc->eof()) {
            $uid = $dbc->get("id");
            $this->auth["uid"] = $uid;
            $this->auth["lang"] = $dbc->get("lang");
            $this->auth["utc"] = $dbc->get("utc");
            $this->auth["view2d"] = $dbc->get("view2d");
            $this->auth["newplayer"] = $dbc->get("newplayer");
            $this->auth["lastlog"] = $dbc->get("lastlog");
            $sess->set("id_member", $uid);
            // $this->auth["level"]=$dbc->get("authlevel");
            $dbc->next();
        }
        
        if ($uid == 0) {
            $this->err = 1;
            $sess->set("connect", false);
            $_POST["errLogin"] = true;
        }
        
        return $uid;
    }

    function isAuthenticated()
    {
        if (isset($this->auth["uid"]) && $this->auth["uid"] && (($this->lifetime <= 0) || (time() < $this->auth["exp"]))) {
            return $this->auth["uid"];
        } else {
            return false;
        }
    }
}
?>
