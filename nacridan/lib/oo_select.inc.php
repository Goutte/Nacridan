<?php

class oo_select extends oo_element
{

    public $m_options;

    public $m_size;

    public $m_valid_e;

    public $m_multiple;
    
    // Constructor
    function oo_select($a)
    {
        $this->setup_element($a);
        if ($a["type"] == "multiple") {
            $this->m_multiple = 1;
        }
        if (! isset($this->m_options)) {
            $this->m_options = array();
        }
        
        if (! isset($this->m_value)) {
            if (isset($this->m_options[0]["value"])) {
                $this->m_value = $this->m_options[0]["value"];
            }
        }
    }

    function self_get($val)
    {
        $str = "";
        
        if ($this->m_multiple) {
            $t = "select multiple";
        } else {
            $t = "select";
        }
        
        $n = $this->m_name;
        
        $str .= "<$t name='$n'";
        if ($this->m_size) {
            $str .= " size='$this->m_size'";
        }
        
        if ($this->m_errorclass && $this->m_error == true) {
            $str .= " class='$this->m_errorclass'";
        }
        
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= ">";
        
        foreach ($this->m_options as $k => $o) {
            $str .= "<option";
            if (is_array($o)) {
                $str .= " value='" . $o["value"] . "'";
            }
            
            if (! $this->m_multiple && (strcmp($this->m_value, $o["value"]) == 0 || $this->m_value == $o)) {
                $str .= " selected=\"selected\"";
            } elseif ($this->m_multiple && is_array($this->m_value)) {
                foreach ($this->m_value as $tk => $v) {
                    if ($v == $o["value"] || $v == $o) {
                        $str .= " selected=\"selected\"";
                        break;
                    }
                }
            }
            $str .= ">" . (is_array($o) ? $o["label"] : $o) . "</option>\n";
        }
        $str .= "</select>";
        return $str;
    }

    function self_validate($val)
    {
        // echo $this->m_exclude."<>".$this->m_value."<br/>";
        if (isset($this->m_exclude) && strcmp($this->m_exclude, $this->m_value) == 0) {
            return $this->m_valid_e;
        }
        return false;
    }
} // end SELECT

?>
