<?php

/* NPC State */
require_once (HOMEPATH . "/lib/MapInfo.inc.php");

class Graph
{

    public $db;

    public $map;

    public $x;

    public $y;

    public $grille;

    public $matrice;

    public function Graph($x, $y, $map, $db)
    {
        $this->db = $db;
        $this->map = $map;
        $this->x = $x;
        $this->y = $y;
        
        $format = '%Y-%m-%d %H:%M:%S';
        
        $this->initGrille();
        
        $this->initMatrice();
    }

    /* ***************************************************************** Initialisation ********************************************************************** */
    public function initGrille()
    {
        $x = $this->x;
        $y = $this->y;
        
        /*
         * Beaucoup trop long
         * for($i =-5; $i< 6; $i++)
         * {
         * for($j =-5; $j< 6; $j++)
         * {
         * if(distHexa($x, $y, $x+$j, $y+$i)<6)
         * {
         * if(BasicActionFactory::freePlace($x+$j, $y+$i, $this->map, $this->db))
         * $this->grille[$x+$j][$y+$i]= 0;
         * else
         * $this->grille[$x+$j][$y+$i]= 1;
         *
         * }
         * }
         * }
         */
        $mapinfo = new MapInfo($this->db);
        
        for ($i = - 5; $i < 6; $i ++) {
            for ($j = - 5; $j < 6; $j ++) {
                if ((distHexa($x, $y, $x + $j, $y + $i) < 6) && ($x + $j) >= 0 && ($x + $j) < MAPWIDTH && ($y + $i) >= 0 && ($y + $i) < MAPHEIGHT) {
                    // echo ($x+$j)."-".($y+$i)."<br> ";
                    $validzone = $mapinfo->getValidMap($x + $j, $y + $i, 0, $this->map);
                    // echo serialize($validzone)."<br>";
                    $temp[$x + $j][$y + $i] = $validzone[0][0] == 1 ? 0 : 1;
                }
            }
        }
        
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        
        $dbplayer = new DBCollection("SELECT * FROM Player WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbplayer->eof()) {
            $temp[$dbplayer->get("x")][$dbplayer->get("y")] = 1;
            $dbplayer->next();
        }
        
        $dbexploitation = new DBCollection("SELECT * FROM Exploitation WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbexploitation->eof()) {
            $temp[$dbexploitation->get("x")][$dbexploitation->get("y")] = 1;
            $dbexploitation->next();
        }
        
        $dbbuilding = new DBCollection("SELECT * FROM Building WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbbuilding->eof()) {
            $temp[$dbbuilding->get("x")][$dbbuilding->get("y")] = 1;
            $dbbuilding->next();
        }
        
        $dbgate = new DBCollection("SELECT * FROM Gate WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbgate->eof()) {
            $temp[$dbgate->get("x")][$dbgate->get("y")] = 1;
            $dbgate->next();
        }
        
        $k = 1;
        for ($i = - 5; $i < 6; $i ++) {
            for ($j = - 5; $j < 6; $j ++) {
                if ((distHexa($x, $y, $x + $j, $y + $i) < 6) && ($x + $j) >= 0 && ($x + $j) < MAPWIDTH && ($y + $i) >= 0 && ($y + $i) < MAPHEIGHT) {
                    $this->grille[$k] = array(
                        "x" => $x + $j,
                        "y" => $y + $i,
                        "value" => $temp[$x + $j][$y + $i]
                    );
                    $k ++;
                }
            }
        }
        
        // print_r($this->grille)."\n";
    }

    public function initMatrice()
    {
        $matrice = array();
        for ($i = 1; $i < 92; $i ++) {
            for ($j = 1; $j < 92; $j ++) {
                if (self::isAdjacent($i, $j) && self::getGrille($j, "value") == 0)
                    $this->matrice[$i][$j] = 1;
                else
                    $this->matrice[$i][$j] = 0;
            }
        }
    }

    public function getGrille($case, $value)
    {
        // echo "getGrille ".print_r($case).",".$value." |\n";
        // echo "| ".serialize($case)." |--|".serialize($value)."<br>";
        // echo "test1:->".array_key_exists ( $case , $this->grille)."\n";
        // echo "test2:->".array_key_exists ( $value , $this->grille[$case])."\n";
        if (! array_key_exists($case, $this->grille) || ! array_key_exists($value, $this->grille[$case])) {
            return 0;
        }
        return $this->grille[$case][$value];
    }

    public function IsFree($case1)
    {
        if (self::getGrille($case1, "value"))
            return 1;
        else
            return 0;
    }

    /**
     * ******************************************************************* Methodes ********************************************************************
     */
    public function getVoisin($case1)
    {
        $adj = array();
        for ($i = 1; $i < 92; $i ++) {
            if ($this->matrice[$case1][$i] == 1)
                $adj[] = $i;
        }
        return $adj;
    }

    public function ajouteVoisin(&$tab, $case)
    {
        for ($i = 1; $i < 92; $i ++) {
            if ($this->matrice[$case][$i] == 1 and ! self::appartient($tab, $i))
                $tab[] = $i;
        }
        return $tab;
    }

    public function distReel($case1, $case2)
    {
        if ($case1 == $case2)
            return 0;
        
        $atteint = array();
        $voisin = array();
        $inter = array();
        
        $adj = array();
        $adj = array();
        $adj = self::getAdjacent($case2);
        $dist = 1;
        
        if (self::appartient($adj, $case1))
            return $dist;
        
        $atteint[] = $case1;
        
        while (count(array_intersect($adj, $atteint)) == 0 && $dist < 20) {
            // echo "/-/";
            $dist ++;
            $inter = $atteint;
            foreach ($inter as $arr) {
                // echo $arr."-";
                self::ajouteVoisin($atteint, $arr);
            }
        }
        
        return $dist;
    }

    public function getChemin($case1, $case2)
    {
        $chemin = array();
        $chemin[] = $case1;
        $dist = self::distReel($case1, $case2);
        if ($dist != 20) {
            while ($dist > 1) {
                $voisin = self::getVoisin($chemin[sizeof($chemin) - 1]);
                foreach ($voisin as $arr) {
                    if (self::distReel($arr, $case2) < $dist) {
                        $chemin[] = $arr;
                        break;
                    }
                }
                $dist --;
            }
            return $chemin;
        } else
            return - 1;
    }

    public function getCircle($case1, $dist)
    {
        $atteint = array();
        $atteint[] = $case1;
        $result = array();
        for ($i = 1; $i < ($dist + 1); $i ++) {
            $inter = $atteint;
            foreach ($inter as $arr)
                self::ajouteVoisin($atteint, $arr);
        }
        
        foreach ($atteint as $arr) {
            if (self::distReel($case1, $arr) == $dist)
                $result[] = $arr;
        }
        return $result;
    }

    public function getPlaceForWall($case1)
    {
        $circle = self::getCircle($case1, 3);
        foreach ($circle as $arr) {
            if (self::isCircleFree($arr))
                return $arr;
        }
        return - 1;
    }

    public function isCircleFree($case1)
    {
        $adj = self::getVoisin($case1);
        if (count($adj) == 6)
            return 1;
        else
            return 0;
    }

    /**
     * ******************************************************************** FONCTIONS STATIC **************************************************************
     */
    public function getAdjacent($case1)
    {
        $adj = array();
        for ($i = 1; $i < 92; $i ++) {
            if (self::isAdjacent($case1, $i))
                $adj[] = $i;
        }
        return $adj;
    }

    static function appartient($liste, $case)
    {
        foreach ($liste as $arr) {
            if ($arr == $case)
                return 1;
        }
        return 0;
    }

    /*
     * public function getX($case)
     * {
     * $x = $this->x;
     * $y = $this->y;
     * for($i =-5; $i< 6; $i++)
     * {
     * for($j =-5; $j< 6; $j++)
     * {
     * if(distHexa($x, $y, $x+$j, $y+$i)<6)
     * {
     * $case--;
     * if($case == 0)
     * return $x+$j;
     * }
     * }
     * }
     * }
     *
     * public function getY($case)
     * {
     * $x = $this->x;
     * $y = $this->y;
     * for($i =-5; $i< 6; $i++)
     * {
     * for($j =-5; $j< 6; $j++)
     * {
     * if(distHexa($x, $y, $x+$j, $y+$i)<6)
     * {
     * $case--;
     * if($case == 0)
     * return $y+$i;
     * }
     * }
     * }
     * }
     */
    public function getCase($a, $b)
    {
        $x = $this->x;
        $y = $this->y;
        $case = 0;
        
        for ($i = - 5; $i < 6; $i ++) {
            for ($j = - 5; $j < 6; $j ++) {
                if (distHexa($x, $y, $x + $j, $y + $i) < 6) {
                    $case ++;
                    if ($x + $j == $a && $y + $i == $b)
                        return $case;
                }
            }
        }
    }

    public function isAdjacent($case1, $case2)
    {
        if (distHexa(self::getGrille($case1, "x"), self::getGrille($case1, "y"), self::getGrille($case2, "x"), self::getGrille($case2, "y")) == 1)
            return 1;
        else
            return 0;
    }
}
?>
