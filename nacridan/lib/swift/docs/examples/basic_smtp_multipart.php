<?php
require ('../../Swift.php');
require ('../../Swift/Connection/SMTP.php');

$mailer = new Swift(new Swift_Connection_SMTP('smtp.somedomain.com'));

// If anything goes wrong you can see what happened in the logs
if ($mailer->isConnected()) {
    // Add as many parts as you need here
    $mailer->addPart('Some plain text part');
    $mailer->addPart('Some HTML <strong>part with bold</strong> text', 'text/html');
    
    // Leaving the body out of send() makes the mailer send a multipart message
    $mailer->send('"Joe Bloggs" <joe@bloggs.com>', '"Your name" <you@yourdomain.com>', 'Some Subject');
    
    $mailer->close();
} else
    echo "The mailer failed to connect. Errors: <pre>" . print_r($mailer->errors, 1) . "</pre><br />
	Log: <pre>" . print_r($mailer->transactions, 1) . "</pre>";

?>
