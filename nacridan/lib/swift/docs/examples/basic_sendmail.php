<?php
require ('../../Swift.php');
require ('../../Swift/Connection/Sendmail.php');

$connection = new Swift_Connection_Sendmail(SWIFT_AUTO_DETECT);

$mailer = new Swift($connection);

// If anything goes wrong you can see what happened in the logs
if ($mailer->isConnected()) {
    // Sends a simple email
    $mailer->send('"Joe Bloggs" <joe@bloggs.com>', '"Your name" <you@yourdomain.com>', 'Some Subject', "Hello Joe it's only me!");
    // Closes cleanly... works without this but it's not as polite.
    $mailer->close();
} else
    echo "The mailer failed to connect. Errors: <pre>" . print_r($mailer->errors, 1) . "</pre><br />
	Log: <pre>" . print_r($mailer->transactions, 1) . "</pre>";

?>
