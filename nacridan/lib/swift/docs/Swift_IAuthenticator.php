<?php

/**
 * This is the Authenticator Interface for Swift Mailer, a PHP Mailer class.
 *
 * @package	Swift Mailer
 * @version	>= 0.0.4
 * @author	Chris Corbyn
 * @date	20th May 2006
 * @license http://www.gnu.org/licenses/lgpl.txt Lesser GNU Public License
 *
 * @copyright Copyright &copy; 2006 Chris Corbyn - All Rights Reserved.
 * @filesource
 * 
 *    "Chris Corbyn" <chris@w3style.co.uk>
 *
 */

/**
 * Swift Authenticator Interface.
 * Describes the methods which authenticators should implement
 */
interface Swift_IAuthenticator
{

    /**
     * Required Properties
     * private SwiftInstance;
     * public serverString;
     */
    
    /**
     * Loads an instance of Swift to the Plugin
     *
     * @param
     *            object SwiftInstance
     * @return void
     */
    public function loadBaseObject(&$object);

    /**
     * Executes the logic in the authentication mechanism
     *
     * @param
     *            string username
     * @param
     *            string password
     * @return bool
     */
    public function run($username, $password); // bool
}

?>
