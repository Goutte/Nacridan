<?php

/**
 * This is the Plugin Interface for Swift Mailer, a PHP Mailer class.
 *
 * @package	Swift Mailer
 * @version	>= 0.0.4
 * @author	Chris Corbyn
 * @date	20th May 2006
 * @license http://www.gnu.org/licenses/lgpl.txt Lesser GNU Public License
 *
 * @copyright Copyright &copy; 2006 Chris Corbyn - All Rights Reserved.
 * @filesource
 * 
 *    "Chris Corbyn" <chris@w3style.co.uk>
 *
 */

/**
 * Swift Plugin Interface.
 * Describes the methods which plugins should implement
 */
interface Swift_IPlugin
{

    /**
     * Required Properties
     *
     * private SwiftInstance;
     * public pluginName;
     */
    
    /**
     * Loads an instance of Swift to the Plugin
     *
     * @param
     *            object SwiftInstance
     * @return void
     */
    public function loadBaseObject(&$object); // Void

/**
 * Optional Methods to implement
 *
 * public function onLoad();
 * public function onClose();
 * public function onFail();
 * public function onError();
 * public function onBeforeSend();
 * public function onSend();
 * public function onBeforeCommand();
 * public function onCommand();
 * public function onLog();
 * public function onAuthenticate();
 * public function onFlush();
 * public function onResponse();
 */
}

?>
