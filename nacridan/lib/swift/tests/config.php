<?php

// DO NOT enclose the email addresses you give in < > tags.

// Where is the base directory of simpletest? (leave off the trailing slash!)
// This needs to be the ABSOLUTE path
define('SIMPLETEST_BASE', '/path/to/simpletest');

// Specify a maximum amount of time to run the tests for before dying
// note that several email will be sent, some with attachments
define('MAX_TIME_LIMIT', 90);

// Provide your sender address
define('SENDER_ADDRESS', 'you@address.com');

// Provide some details of an SMTP server which DOES NOT require authentication
define('SMTP_NO_AUTH_ADDRESS', 'host.tld');
define('SMTP_NO_AUTH_PORT', 25);
define('SMTP_NO_AUTH_SENDTO', 'your@address.com');

// Provide the sendmail info
define('SENDMAIL_COMMAND', '/usr/sbin/sendmail -bs');
define('SENDMAIL_SENDTO', 'your@address.com');

// Provide an address to mail to with mail()
define('MAIL_SENDTO', 'your@address.com');

?>
