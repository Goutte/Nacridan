<?php
require_once '../config.php';
require_once SIMPLETEST_BASE . '/unit_tester.php';
require_once SIMPLETEST_BASE . '/reporter.php';
require_once '../../Swift.php';
require_once '../../Swift/Connection/NativeMail.php';
set_time_limit(MAX_TIME_LIMIT);

class TestOfNativeMail extends UnitTestCase
{

    private $swift;

    public function testConnect()
    {
        $this->swift = new Swift(new Swift_Connection_NativeMail());
        $this->assertTrue($this->swift->isConnected());
    }

    public function testSendingPlainTextMail()
    {
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [1]: Plain Text Mail', "Make sure the following text matches the image at" . " http://www.swiftmailer.org/contrib/tests/test1.png\n\n" . "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus ultricies fringilla libero." . " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec tellus tortor, posuere a," . " fermentum ut, vulputate eu, nulla. In iaculis condimentum ipsum. Integer pulvinar nunc sit" . " amet turpis. Proin tempor congue arcu. Nam dignissim, sapien sed accumsan accumsan.");
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testSendingHtmlMail()
    {
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [2]: HTML Mail', "Make sure the following text matches the image at" . " <a href=\"http://www.swiftmailer.org/contrib/tests/test2.png\">" . "http://www.swiftmailer.org/contrib/tests/test2.png</a><br /><br />" . "<strong>Lorem ipsum dolor</strong> sit amet, consectetuer adipiscing elit. Vivamus ultricies fringilla libero." . " <span style=\"color:red;\">Lorem ipsum dolor</span> sit amet, consectetuer adipiscing elit. Donec tellus tortor, posuere a," . " fermentum ut, vulputate eu, nulla. In iaculis condimentum ipsum. Integer pulvinar nunc sit" . " amet turpis. Proin tempor congue arcu. Nam dignissim, sapien sed accumsan accumsan.", 'text/html');
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testSendingMultiPartMail()
    {
        $this->swift->addPart("Make sure the following text matches the image at" . " <a href=\"http://www.swiftmailer.org/contrib/tests/test3.png\">" . "http://www.swiftmailer.org/contrib/tests/test3.png</a>.  If your mail client supports" . " viewing this email in Plain Text format, do this next.<br /><br />" . "<strong>Lorem ipsum dolor</strong> sit amet, consectetuer adipiscing elit. Vivamus ultricies fringilla libero." . " <span style=\"color:red;\">Lorem ipsum dolor</span> sit amet, consectetuer adipiscing elit. Donec tellus tortor, posuere a," . " fermentum ut, vulputate eu, nulla. In iaculis condimentum ipsum. Integer pulvinar nunc sit" . " amet turpis. Proin tempor congue arcu. Nam dignissim, sapien sed accumsan accumsan.", 'text/html');
        
        $this->swift->addPart("Make sure the following text matches the image at" . "http://www.swiftmailer.org/contrib/tests/test4.png.  If your mail client supports" . " viewing this email in HTML format, do this next.\n\n" . "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus ultricies fringilla libero." . " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec tellus tortor, posuere a," . " fermentum ut, vulputate eu, nulla. In iaculis condimentum ipsum. Integer pulvinar nunc sit" . " amet turpis. Proin tempor congue arcu. Nam dignissim, sapien sed accumsan accumsan.");
        
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [3]: Multipart Mail');
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testSendingMailWithAttachment()
    {
        $this->swift->addAttachment(file_get_contents('../../files/durham.jpg'), 'durham.jpg', 'image/jpeg');
        $this->swift->addPart("Make sure there is a file attached, that it is recognized as a JPEG image, " . "that it is called durham.jpg and that it looks like the image at" . " http://www.swiftmailer.org/contrib/tests/durham.jpg");
        
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [4]: File Attachment');
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testSendingIso_8859_1()
    {
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [5]: ISO-8859-1 Mail', "Make sure the following text matches the image at" . " http://www.swiftmailer.org/contrib/tests/test5.png\n\n" . file_get_contents('../emails/iso-8859-1.txt'));
        
        // Auto-detected
        $this->assertEqual(strtolower($this->swift->charset), 'iso-8859-1');
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testSendingUtf8()
    {
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [6]: UTF-8 Mail', "Make sure the following text matches the image at" . " http://www.swiftmailer.org/contrib/tests/test6.png\n\n" . file_get_contents('../emails/utf8.txt'));
        
        // Auto-detected
        $this->assertEqual(strtolower($this->swift->charset), 'utf-8');
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testSendingIso_8859_6()
    {
        $this->swift->setCharset('iso-8859-6');
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [7]: ISO-8859-6 Mail', "Make sure the following text matches the image at" . " http://www.swiftmailer.org/contrib/tests/test7.png\n\n" . file_get_contents('../emails/iso-8859-6.txt'));
        
        // Auto-detected
        $this->assertEqual(strtolower($this->swift->charset), 'iso-8859-6');
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testEncodedHeaders()
    {
        $this->swift->setCharset('utf-8');
        $sent = $this->swift->send(MAIL_SENDTO, SENDER_ADDRESS, 'Swift SMTP [8]: содержимое программном содержимое программном содержимое программном содержимое программном содержимое программном содержимое программном содержимое программном содержимое программном', "Make sure the subject line matches the image at" . " http://www.swiftmailer.org/contrib/tests/test8.png\n\n");
        
        if ($this->swift->isConnected()) {
            $this->assertFalse($this->swift->hasFailed());
            $this->assertTrue($sent);
        } else {
            $this->assertTrue($this->swift->hasFailed());
            $this->assertFalse($sent);
        }
    }

    public function testDisconnect()
    {
        $this->swift->close();
    }
}

$test = new TestOfNativeMail();
$test->run(new HtmlReporter());

?>
