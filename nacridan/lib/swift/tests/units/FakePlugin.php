<?php

// Look to the subclass MockFakePluginWithEvents for
// the implementation of these methods
class FakePlugin implements Swift_IPlugin
{

    public function loadBaseObject(&$swift)
    {}

    public function onLoad()
    {}

    public function onSend()
    {}

    public function onBeforeSend()
    {}

    public function onBeforeCommand()
    {}

    public function onCommand()
    {}

    public function onResponse()
    {}

    public function onUnload()
    {}

    public function onClose()
    {}

    public function onError()
    {}

    public function onFail()
    {}

    public function onLog()
    {}

    public function onConnect()
    {}

    public function onAuthenticate()
    {}
}

?>