<?php
require_once '../config.php';
require_once SIMPLETEST_BASE . '/unit_tester.php';
require_once SIMPLETEST_BASE . '/reporter.php';
require_once SIMPLETEST_BASE . '/mock_objects.php';
require_once '../../Swift/Stream.php';
require_once '../../Swift/Stream/Processor.php';
require_once 'SmtpStub.php';
require_once '../../Swift.php';
require_once 'FakeConnection.php';
require_once 'FakePlugin.php';

class TestOfSwift extends UnitTestCase
{

    private $swift;

    public function testCreatingConnection()
    {
        $this->swift = new Swift(new FakeConnection(array(
            "8BITMIME",
            "PIPELINING",
            "AUTH PLAIN"
        )));
        $this->assertTrue($this->swift->isConnected());
        $this->assertFalse($this->swift->hasFailed());
    }

    public function testAddingCc()
    {
        $amount1 = count($this->swift->getCcAddresses());
        $this->swift->addCc('foo@bar');
        $amount2 = count($this->swift->getCcAddresses());
        $this->assertTrue($amount1 < $amount2);
        
        $this->swift->addCc('foo@bar');
        $amount3 = count($this->swift->getCcAddresses());
        $this->assertTrue($amount2 < $amount3);
    }

    public function testFlushingCc()
    {
        $this->swift->addCc('foo@bar');
        $amount1 = count($this->swift->getCcAddresses());
        $this->assertTrue($amount1 > 0);
        $this->swift->flushCc();
        $amount2 = count($this->swift->getCcAddresses());
        $this->assertTrue($amount2 == 0);
    }

    public function testAddingBcc()
    {
        $amount1 = count($this->swift->getBccAddresses());
        $this->swift->addBcc('foo@bar');
        $amount2 = count($this->swift->getBccAddresses());
        $this->assertTrue($amount1 < $amount2);
        
        $this->swift->addBcc('foo@bar');
        $amount3 = count($this->swift->getBccAddresses());
        $this->assertTrue($amount2 < $amount3);
    }

    public function testFlushingBcc()
    {
        $this->swift->addBcc('foo@bar');
        $amount1 = count($this->swift->getBccAddresses());
        $this->assertTrue($amount1 > 0);
        $this->swift->flushBcc();
        $amount2 = count($this->swift->getBccAddresses());
        $this->assertTrue($amount2 == 0);
    }

    public function testAddPart()
    {
        $num_parts1 = $this->swift->numParts();
        $this->swift->addPart('Part');
        $num_parts2 = $this->swift->numParts();
        $this->assertTrue($num_parts1 < $num_parts2);
    }

    public function testFlushParts()
    {
        $this->swift->addPart('Part');
        $num_parts1 = $this->swift->numParts();
        $this->swift->flushParts();
        $num_parts2 = $this->swift->numParts();
        $this->assertTrue($num_parts1 > $num_parts2);
    }

    public function testAddAttachment()
    {
        $num_attach1 = $this->swift->numAttachments();
        $this->swift->addAttachment(file_get_contents('../../files/durham.jpg'), 'test.jpg');
        $num_attach2 = $this->swift->numAttachments();
        $this->assertTrue($num_attach1 < $num_attach2);
    }

    public function testFlushAttachment()
    {
        $this->swift->addAttachment(file_get_contents('../../files/durham.jpg'), 'test2.jpg');
        $num_attach1 = $this->swift->numAttachments();
        $this->swift->flushAttachments();
        $num_attach2 = $this->swift->numAttachments();
        $this->assertTrue($num_attach1 > $num_attach2);
    }

    public function testGetAddress()
    {
        $address1 = 'foo@bar';
        $safe_address = $this->swift->getAddress($address1);
        $this->assertEqual($safe_address, '<foo@bar>');
        
        $address2 = '<foo@bar>';
        $safe_address = $this->swift->getAddress($address2);
        $this->assertEqual($safe_address, '<foo@bar>');
        
        $address3 = 'Test Address <user@host.tld>';
        $safe_address = $this->swift->getAddress($address3);
        $this->assertEqual($safe_address, '<user@host.tld>');
        
        $address4 = "Test Address <user-name_x%&@host.\ntld>";
        $safe_address = $this->swift->getAddress($address4);
        $this->assertEqual($safe_address, '<user-name_x%&@host.tld>');
    }

    public function testLoadingAndRemovingPlugin()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $plugin_count1 = $this->swift->numPlugins();
        $this->swift->loadPlugin($plugin);
        $plugin_count2 = $this->swift->numPlugins();
        $this->assertTrue($plugin_count1 < $plugin_count2);
        
        $this->swift->removePlugin('Fake');
        $plugin_count3 = $this->swift->numPlugins();
        $this->assertTrue($plugin_count3 < $plugin_count2);
    }

    public function testPluginEventCalls()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $plugin->expectOnce('loadBaseObject', array(
            $this->swift
        ));
        $plugin->expectOnce('onLoad');
        $plugin->expectOnce('onUnload');
        $plugin->expectOnce('onBeforeSend');
        $plugin->expectOnce('onSend');
        $plugin->expectAtLeastOnce('onBeforeCommand');
        $plugin->expectAtLeastOnce('onCommand');
        $plugin->expectAtLeastOnce('onResponse');
        $plugin->expectAtLeastOnce('onLog');
        
        $this->swift->loadPlugin($plugin);
        
        $this->swift->send('a@b', 'd@c', 'Subj', 'Body');
        
        $this->swift->removePlugin('Fake');
    }

    public function testPluginAccessToParent()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $this->swift->loadPlugin($plugin);
        
        $cc_count1 = count($this->swift->getCcAddresses());
        $this->swift->getPlugin('Fake')->callSwiftAddCc('foo@bar');
        $cc_count2 = count($this->swift->getCcAddresses());
        $this->assertTrue($cc_count1 < $cc_count2);
        
        $this->swift->removePlugin('Fake');
        
        $this->swift->flushCc();
    }

    public function testSingleSend1()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $plugin->expectOnce('onSend');
        
        $this->swift->loadPlugin($plugin);
        
        $sent = $this->swift->send('a@b', 'd@c', 'Subj', 'Body');
        $this->assertTrue($sent);
        
        $this->swift->removePlugin('Fake');
    }

    public function testSingleSend2()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $plugin->expectOnce('onSend');
        
        $this->swift->loadPlugin($plugin);
        
        $this->swift->addCc('foo@bar');
        $this->swift->addCc('user@host.tld');
        
        $sent = $this->swift->send('a@b', 'd@c', 'Subj', 'Body');
        $this->assertTrue($sent);
        
        $this->swift->removePlugin('Fake');
        $this->swift->flush();
    }

    public function testSingleSend3()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $plugin->expectOnce('onSend');
        
        $this->swift->loadPlugin($plugin);
        $this->swift->useExactCopy(true);
        
        $sent = $this->swift->send(array(
            'a@b',
            'e@f',
            'x@y'
        ), 'd@c', 'Subj', 'Body');
        $this->assertTrue($sent);
        
        $this->swift->removePlugin('Fake');
        $this->swift->useExactCopy(false);
    }

    public function testBatchSend()
    {
        Mock::generate('FakePlugin');
        require_once 'MockFakePluginWithEvents.php';
        $plugin = new MockFakePluginWithEvents($this);
        
        $plugin->expectCallCount('onSend', 3);
        
        $this->swift->loadPlugin($plugin);
        
        $sent = $this->swift->send(array(
            'a@b',
            'e@f',
            'x@y'
        ), 'd@c', 'Subj', 'Body');
        $this->assertTrue($sent);
        
        $this->swift->removePlugin('Fake');
    }

    public function testHeaderInjectionSecurity()
    {
        $this->swift->send("a@b", "\"Foo Bar\" \r\nX-Hacked: Test\r\n <user@host.tld>", "Subj", "Body");
        $this->assertNoPattern("/^X-Hacked: Test/m", $this->swift->currentMail[3]);
        
        $this->swift->send("a@b", "Test <\r\nX-Hacked-Twice: Foo\r\nuser@host.tld>", "Subj", "Body");
        $this->assertNoPattern("/^X-Hacked-Twice: Foo/m", $this->swift->currentMail[3]);
        
        $this->swift->send("a@b", "Test <\nX-Hacked-Twice: Foo\nuser@host.tld>", "Subj", "Body");
        $this->assertNoPattern("/^X-Hacked-Twice: Foo/m", $this->swift->currentMail[3]);
        
        $this->swift->send("a@b", "Test <\rX-Hacked-Twice: Foo\ruser@host.tld>", "Subj", "Body");
        $this->assertNoPattern("/^X-Hacked-Twice: Foo/m", $this->swift->currentMail[3]);
        
        // This one is valid
        $this->swift->send("a@b", "Test <user@host.tld>", "Subj", "Body");
        $this->assertPattern("/^From: Test .*$/m", $this->swift->currentMail[3]);
        
        // The SMTP server will also have bailed out (safely) at dodgy RCPT envelopes
        $this->assertTrue($this->swift->hasFailed());
        $this->swift->failed = false;
        $this->assertFalse($this->swift->hasFailed());
    }

    public function testCommandResponseChecking()
    {
        $this->assertFalse($this->swift->hasFailed());
        $this->swift->addExpectedCode('swifttest', 500);
        $this->swift->command("swifttest\r\n");
        $this->assertFalse($this->swift->hasFailed());
        $this->swift->addExpectedCode("mytest", 250);
        $this->swift->command("mytest\r\n");
        $this->assertTrue($this->swift->hasFailed());
        $this->swift->failed = false;
        $this->assertFalse($this->swift->hasFailed());
    }

    public function testEncoding()
    {
        $string = "Olá Moçada!";
        
        $utf8 = $this->swift->detectUTF8($string);
        $this->assertTrue($utf8);
        
        $newstring1 = $this->swift->encode($string, 'quoted-printable');
        $utf8 = $this->swift->detectUTF8($newstring1);
        $this->assertFalse($utf8);
        
        $newstring2 = $this->swift->encode($string, 'base64');
        $utf8 = $this->swift->detectUTF8($newstring2);
        $this->assertFalse($utf8);
    }
    
    // We need to close our open connection in any case
    public function testClosingConnection()
    {
        $this->swift->close();
    }

    public function testNo8BitMimeSupport()
    {
        // Notice, there's no 8BITMIME extension in this one!
        $swift = new Swift(new FakeConnection(array(
            "AUTH PLAIN"
        )));
        $this->assertTrue($swift->isConnected());
        
        // So this UTF-8 string CANNOT be sent over SMTP
        $string = "Olá Moçada!";
        $utf8 = $swift->detectUTF8($string);
        $this->assertTrue($utf8);
        
        // But we'll try!
        $sent = $swift->send("user@host.tld", "user@host.tld", "Subject", $string);
        $this->assertTrue($sent);
        
        // Adn we'll hope we dealt with it accordingly
        $this->assertFalse($swift->detectUTF8($swift->currentMail[3]));
        
        $swift->close();
    }

    public function test8BitMimeSupport()
    {
        $swift = new Swift(new FakeConnection(array(
            "AUTH PLAIN",
            "8BITMIME"
        )));
        $this->assertTrue($swift->isConnected());
        
        $string = "Olá Moçada!";
        $utf8 = $swift->detectUTF8($string);
        $this->assertTrue($utf8);
        
        $sent = $swift->send("user@host.tld", "user@host.tld", "Subject", $string);
        $this->assertTrue($sent);
        
        $this->assertTrue($swift->detectUTF8($swift->currentMail[3]));
        
        $swift->close();
    }

    public function testAuthentication()
    {
        // Should pass
        $swift = new Swift(new FakeConnection(array(
            "AUTH PLAIN",
            "8BITMIME"
        ), "test", "pass123"));
        $this->assertTrue($swift->isConnected());
        
        $auth = $swift->authenticate("test", "pass123");
        $this->assertTrue($auth);
        
        $swift->close();
        
        // Should fail
        $swift = new Swift(new FakeConnection(array(
            "AUTH PLAIN",
            "8BITMIME"
        ), "test", "pass123"));
        $this->assertTrue($swift->isConnected());
        
        $auth = $swift->authenticate("test", "pass1xxxx");
        $this->assertFalse($auth);
        $this->assertTrue($swift->hasFailed());
        
        $swift->close();
    }
}

if (! defined('IN_ALLTESTS')) {
    $test = new TestOfSwift();
    $test->run(new HtmlReporter());
}

?>