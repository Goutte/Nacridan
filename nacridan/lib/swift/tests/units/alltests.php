<?php
define('IN_ALLTESTS', 1);

require_once '../config.php';
require_once SIMPLETEST_BASE . '/unit_tester.php';
require_once SIMPLETEST_BASE . '/reporter.php';

require_once 'testofstream.php';
require_once 'testofsmtpstub.php';
require_once 'testofswift.php';

$test = new GroupTest('Swift Component Tests');
$test->addTestCase(new TestOfStream());
$test->addTestCase(new TestOfSmtpStub());
$test->addTestCase(new TestOfSwift());

$test->run(new HtmlReporter());

?>
