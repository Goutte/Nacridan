<?php

/**
 * Pop Before SMTP authenticator compat file stub
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Chris Corbyn
 * @date	30th July 2006
 */
require_once (dirname(__FILE__) . '/Authenticator/POP3SMTP_.php');

class Swift_POP3_SMTP_Authenticator extends Swift_Authenticator_POP3SMTP
{
}

?>