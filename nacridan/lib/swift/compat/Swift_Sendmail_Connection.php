<?php

/**
 * Sendmail connection compat file stub
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Chris Corbyn
 * @date	30th July 2006
 */
require_once (dirname(__FILE__) . '/Connection/Sendmail.php');

class Swift_Sendmail_Connection extends Swift_Connection_Sendmail
{
}

?>