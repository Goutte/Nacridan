<?php

/**
 * SMTP Connection compat file stub
 *
 * @package	Swift
 * @version	>= 2.0.0
 * @author	Chris Corbyn
 * @date	30th July 2006
 */
require_once (dirname(__FILE__) . '/Connection/SMTP.php');

class Swift_SMTP_Connection extends Swift_Connection_SMTP
{
}

?>