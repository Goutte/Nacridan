<?php

class oo_checkbox extends oo_element
{

    public $m_checked;

    public $m_valid_e;
    
    // Constructor
    function oo_checkbox($a)
    {
        $this->setup_element($a);
        if ($this->m_checked != "true")
            $this->m_checked = "false";
    }

    function self_validate($val)
    {
        if (isset($this->m_exclude) && $this->m_exclude == $this->m_checked) {
            return $this->m_valid_e;
        }
        return false;
    }

    function self_get($val)
    {
        $str = "";
        $str .= "<input type='checkbox' name='$this->m_name'";
        $str .= " value='$this->m_value'";
        
        if ($this->m_checked == "true") {
            $str .= " checked=\"checked\"";
        }
        
        if ($this->m_errorclass) {
            $str .= " class='$this->m_errorclass'";
        }
        
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= "/>\n";
        return $str;
    }

    function self_load_default($val)
    {
        if (isset($val) && (! $this->m_value || $val == $this->m_value)) {
            $this->m_checked = "true";
        } else {
            $this->m_checked = "false";
        }
    }
} // end CHECKBOX

?>
