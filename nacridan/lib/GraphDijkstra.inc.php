<?php

class PriorityQueue extends SplPriorityQueue
{

    public function compare ($a, $b)
    {
        if ($a[0] === $b[0]) {
            if ($a[1] === $b[1])
                return 0;
            else {
                return $a[1] > $b[1] ? - 1 : 1;
            }
        } else {
            return $a[0] > $b[0] ? - 1 : 1;
        }
    }
}

class GraphDijkstra
{

    protected $x;

    protected $y;

    protected $xTarget = - 1;

    protected $yTarget = - 1;

    protected $player;

    protected $buildingDamage;

    protected $map;

    protected $distanceRechercheMax;

    protected $graph;

    protected $grille;

    protected $db;

    protected $NO_WAY = 100000;

    protected $lastShortPath;

    public function __construct ($player, $distanceRechercheMax, $db)
    {
        $this->x = $player->get("x");
        $this->y = $player->get("y");
        $this->map = $player->get("map");
        $this->player = $player;
        $this->distanceRechercheMax = $distanceRechercheMax;
        $this->db = $db;
        
        $damage = $player->getScore("damage");
        $mm = $player->getScore("magicSkill");
        $dex = $player->getScore("dexterity");
        $spelllevel = ceil(sqrt($dex * $mm) / 4);
        $this->buildingDamage = max($spelllevel * 1.5, $damage, 1);
    }

    private function initGrille ()
    {
        $x = $this->x;
        $y = $this->y;
        
        $mapinfo = new MapInfo($this->db);
        $validzone = $mapinfo->getValidMapCost($x, $y, $this->distanceRechercheMax, $this->map);
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        
        $dbplayer = new DBCollection("SELECT id,x,y FROM Player WHERE map=" . $this->map . " and (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        
        // $this->yTarget . ")");
        
        while (! $dbplayer->eof()) {
            if ($dbplayer->get("id") != $this->player->get("id") && ($dbplayer->get("x") != $this->xTarget || $dbplayer->get("y") != $this->yTarget)) {
                $validzone[$dbplayer->get("x")][$dbplayer->get("y")]["cost_EL"] = $this->NO_WAY;
                $validzone[$dbplayer->get("x")][$dbplayer->get("y")]["EL_type"] = "Player";
            }
            $dbplayer->next();
        }
        
        $dbexploitation = new DBCollection("SELECT * FROM Exploitation WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbexploitation->eof()) {
            if ($dbexploitation->get("type") == "body") {
                $validzone[$dbexploitation->get("x")][$dbexploitation->get("y")]["cost_EL"] = 1;
                $validzone[$dbexploitation->get("x")][$dbexploitation->get("y")]["EL_id"] = $dbexploitation->get("id");
            } else {
                $validzone[$dbexploitation->get("x")][$dbexploitation->get("y")]["cost_EL"] = $this->NO_WAY;
            }
            $validzone[$dbexploitation->get("x")][$dbexploitation->get("y")]["EL_type"] = "Exploitation";
            $dbexploitation->next();
        }
        
        $dbbuilding = new DBCollection("SELECT * FROM Building WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbbuilding->eof()) {
            
            if ($dbbuilding->get("id_BasicBuilding") == 27) {
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["cost_EL"] = ceil(($dbbuilding->get("currsp") / $this->buildingDamage)) * 8;
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["EL_type"] = "Building_" . $dbbuilding->get("id_BasicBuilding");
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["EL_id"] = $dbbuilding->get("id");
            } elseif ($dbbuilding->get("id_BasicBuilding") == 15) {
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["cost_EL"] = $dbbuilding->get("value") * 2;
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["EL_type"] = "Building_" . $dbbuilding->get("id_BasicBuilding");
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["EL_id"] = $dbbuilding->get("id");
            } else {
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["cost_EL"] = $this->NO_WAY;
                $validzone[$dbbuilding->get("x")][$dbbuilding->get("y")]["EL_type"] = "Building";
            }
            
            $dbbuilding->next();
        }
        
        $dbgate = new DBCollection("SELECT * FROM Gate WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <= 5", $this->db);
        while (! $dbgate->eof()) {
            $validzone[$dbgate->get("x")][$dbgate->get("y")]["cost_EL"] = $this->NO_WAY;
            $validzone[$dbgate->get("x")][$dbgate->get("y")]["EL_type"] = "Gate";
            $dbgate->next();
        }
        
        $isFlying = $this->player->get("state") == "flying";
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='Course Celeste' AND id_Player=" . $this->player->get("id"), $this->db);
        $isCourseCeleste = ! $dbbm->eof();
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->player->get("id") . " AND id_Player\$Src=" . $this->player->get("id") . " AND name='Embuscade'", $this->db);
        $isEmbuscade = ! $dbbm->eof();
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='Blessure gênante' AND id_Player=" . $this->player->get("id"), $this->db);
        $isBlessureGenante = ! $dbbm->eof();
        
        $isCreeping = $this->player->get("state") == "creeping";
        
        $isTurtle = ($this->player->get("state") == "turtle");
        if ($isTurtle) {
            $dbt = new DBCollection("SELECT Player.state FROM Player LEFT JOIN Caravan ON Caravan.id=Player.id_Caravan WHERE Caravan.id_Player=" . $this->player->get("id"), $this->db);
            $isCreeping = ! $dbt->eof() && $dbt->get("state") == "creeping";
        }
        
        $k = 1;
        for ($i = - 5; $i < 6; $i ++) {
            for ($j = - 5; $j < 6; $j ++) {
                if ((distHexa($x, $y, $x + $j, $y + $i) < 6) && ($x + $j) >= 0 && ($x + $j) < MAPWIDTH && ($y + $i) >= 0 && ($y + $i) < MAPHEIGHT) {
                    
					$costPA = $validzone[$x + $j][$y + $i]["cost_PA"];
					if($costPA < $this->NO_WAY){
						$costPA = ($isFlying ? 0.5 : ($validzone[$x + $j][$y + $i]["cost_PA"]));
						$costPA = $isCourseCeleste ? min($PA, 1) : $costPA;
						$costPA = $isEmbuscade ? $costPA + 3 : $costPA;
						$costPA = $isBlessureGenante ? $costPA + 1 : $costPA;
						$costPA = $isTurtle ? ($validzone[$x + $j][$y + $i]["ground"] == "marsh" ? 1 : $costPA + 1) : $costPA;
						$costPA = $isCreeping ? 5 : $costPA;
					}else{
						$costPA = ($isFlying ? 0.5 : ($validzone[$x + $j][$y + $i]["cost_PA"]));
					}
                    
                    $this->grille[$k] = array(
                            "x" => $x + $j,
                            "y" => $y + $i,
                            "cost_PA" => $costPA,
                            "cost_EL" => ($validzone[$x + $j][$y + $i]["cost_EL"]),
                            "EL_type" => ($validzone[$x + $j][$y + $i]["EL_type"]),
                            "EL_id" => isset($validzone[$x + $j][$y + $i]["EL_id"]) ? $validzone[$x + $j][$y + $i]["EL_id"] : 0
                    );
                    $k ++;
                }
            }
        }
    }

    private function initMatrice ()
    {
        for ($i = 1; $i < 92; $i ++) {
            for ($j = 1; $j < 92; $j ++) {
                if (self::isAdjacent($i, $j)) {
					if($this->grille[$i]["cost_PA"] < $this->NO_WAY){
						if (! array_key_exists($i, $this->graph))
							$this->graph[$i] = array();
						$this->graph[$i][$j] = $this->grille[$i]["cost_PA"] + $this->grille[$j]["cost_EL"];
					}
                }
            }
            if (isset($this->graph[$i]))
                asort($this->graph[$i], SORT_NUMERIC);
        }
    }

    private function isAdjacent ($case1, $case2, $dist = 1)
    {
		if(self::getGrille($case1, "cost_PA") >= $this->NO_WAY || self::getGrille($case2, "cost_PA") >= $this->NO_WAY)
			return 0;			
        if (distHexa(self::getGrille($case1, "x"), self::getGrille($case1, "y"), self::getGrille($case2, "x"), self::getGrille($case2, "y")) == $dist)
            return 1;
        else
            return 0;
    }

    public function getGrille ($case, $value)
    {
        if (! array_key_exists($case, $this->grille) || ! array_key_exists($value, $this->grille[$case])) {
            return - 1;
        }
        return $this->grille[$case][$value];
    }

    public function getVoisin ($case1, $dist = 1)
    {
        $adj = array();
        for ($i = 1; $i < 92; $i ++) {
            if (self::getGrille($i, "EL_type") == "" && self::isAdjacent($case1, $i, $dist))
                $adj[] = $i;
        }
        return $adj;
    }

    public function printShortestPath ($xTarget, $yTarget)
    {
        $result = "Plus court chemin:";
        
        self::calculShortestPath($xTarget, $yTarget);
        $target = self::getCase($xTarget, $yTarget);
        // we can now find the shortest path using reverse
        // iteration
        $S = new SplStack(); // shortest path with a stack
        $u = $target;
        $dist = 0;
        // traverse from target to source
        while (isset($this->lastShortPath[$u]) && $this->lastShortPath[$u]) {
            $S->push($u);
            $dist += $this->graph[$u][$this->lastShortPath[$u]]; // add distance to
                                                                 // predecessor
            $u = $this->lastShortPath[$u];
        }
        
        // stack will be empty if there is no route back
        if ($S->isEmpty()) {
            $result .= "Pas de route entre (" . self::getGrille($source, "x") . ", " . self::getGrille($source, "y") . ") et (" . self::getGrille($target, "x") . ", " . self::getGrille($target, "y") . ")";
        } else {
            // add the source node and print the path in reverse
            // (LIFO) order
            $S->push($source);
            $result .= "$dist:";
            $sep = '';
            foreach ($S as $v) {
                $result .= $sep . "(" . self::getGrille($v, "x") . ", " . self::getGrille($v, "y") . ",)";
                $sep = '->';
            }
        }
        return $result;
    }

    public function initGraph ()
    {
        self::initGrille();
        $this->graph = array();
        self::initMatrice();
    }

    private function calculShortestPath ($xTarget, $yTarget)
    {
        $this->xTarget = $xTarget;
        $this->yTarget = $yTarget;
        
        self::initGraph();
        
        $source = self::getCase($this->x, $this->y);
        $target = self::getCase($xTarget, $yTarget);
        
        // array of best estimates of shortest path to each
        // vertex
        $d = array();
        // array of predecessors for each vertex
        $this->lastShortPath = array();
        // queue of all unoptimized vertices
        $Q = new PriorityQueue();
        
        foreach ($this->graph as $v => $adj) {
            $d[$v] = PHP_INT_MAX; // set initial distance to "infinity"
            $this->lastShortPath[$v] = null; // no known predecessors yet
            foreach ($adj as $w => $cost) {
                // use the edge cost as the priority
                $Q->insert($w, array(
                        distHexa(self::getGrille($source, "x"), self::getGrille($source, "y"), self::getGrille($w, "x"), self::getGrille($w, "y")),
                        $cost
                ));
            }
        }
        
        // initial distance at source is 0
        $d[$source] = 0;
        
        while (! $Q->isEmpty()) {
            // extract min cost
            $u = $Q->extract();
            if (array_key_exists($u, $this->graph)) {
                // "relax" each adjacent vertex
                foreach ($this->graph[$u] as $v => $cost) {
                    // alternate route length to adjacent neighbor
                    $alt = $d[$u] + $cost;
                    // if alternate route is shorter
                    if ($alt < $d[$v]) {
                        $d[$v] = $alt; // update minimum length to vertex
                        $this->lastShortPath[$v] = $u; // add neighbor to predecessors
                                                           // for vertex
                    }
                }
            }
        }
    }

    public function getShortestPath ($xTarget, $yTarget)
    {
        self::calculShortestPath($xTarget, $yTarget);
        
        // we can now find the shortest path using reverse
        // iteration
        $S = new SplStack(); // shortest path with a stack
        $u = self::getCase($xTarget, $yTarget);
        ;
        // traverse from target to source
        while (isset($this->lastShortPath[$u]) && $this->lastShortPath[$u]) {
            $S->push($u);
            $u = $this->lastShortPath[$u];
        }
        return $S;
    }

    public function getRealDistance ($xTarget, $yTarget)
    {
        self::calculShortestPath($xTarget, $yTarget);
        return self::getDistanceFromPreviousCalcul();
    }

    private function getDistanceFromPreviousCalcul ()
    {
        // we can now find the shortest path using reverse
        // iteration
        $u = self::getCase($this->xTarget, $this->yTarget);
        $dist = 0;
        // traverse from target to source
        $i = 0;
        while (isset($this->lastShortPath[$u]) && $this->lastShortPath[$u]) {
            $dist += $this->graph[$u][$this->lastShortPath[$u]]; // add distance to
            $u = $this->lastShortPath[$u];
        }
        return $dist;
    }

    public function getCase ($a, $b)
    {
        $x = $this->x;
        $y = $this->y;
        $case = 0;
        
        for ($i = - 5; $i < 6; $i ++) {
            for ($j = - 5; $j < 6; $j ++) {
                if (distHexa($x, $y, $x + $j, $y + $i) < 6) {
                    $case ++;
                    if ($x + $j == $a && $y + $i == $b)
                        return $case;
                }
            }
        }
    }
}
?>