<?php
DEFINE("KEY", 0);
DEFINE("VALUE", 1);
DEFINE("TMPVAL", 2);

class ActionEvt
{

    public $m_action;

    public $m_parameters;

    function ActionEvt()
    {
        $this->m_action = 0;
        $this->m_parameters = array();
    }

    function setValue($value)
    {
        $this->m_parameters[$name] = $value;
    }

    function setAction(&$arr)
    {
        if (isset($arr["action"]))
            $this->m_action = quote_smart($arr["action"]);
        foreach ($arr as $name => $value) {
            $this->m_parameters[$name] = quote_smart($value);
        }
    }

    function getType()
    {
        return $this->m_action;
    }

    function setType($type)
    {
        $this->m_action = $type;
    }

    function setParameter($name, $value)
    {
        $this->m_parameters[$name] = $value;
    }

    function isExist($name)
    {
        return isset($this->m_parameters[$name]);
    }

    function getParameter($name)
    {
        if (isset($this->m_parameters[$name]))
            return $this->m_parameters[$name];
        else
            return "";
    }
}

class VectorProba
{

    public $m_cur;

    public $m_map;

    public $m_interval;

    public $m_total;

    function VectorProba()
    {
        $this->m_cur = - 1;
        $this->m_map = array();
        $this->m_total = 0;
    }

    function setInterval($interval)
    {
        $this->m_interval = $interval;
        
        if ($this->m_total == 0)
            $step = 0;
        else
            $step = $interval / $this->m_total;
        
        for ($i = 0; $i <= $this->m_cur; $i ++) {
            $this->m_map[$i][TMPVAL] = $this->m_map[$i][VALUE] * $step;
        }
        
        $this->m_map[$this->m_cur][TMPVAL] = $interval;
    }

    function pushBack($key, $value)
    {
        $this->m_cur ++;
        
        $this->m_total += $value;
        
        $this->m_map[$this->m_cur][KEY] = $key;
        $this->m_map[$this->m_cur][VALUE] = $this->m_total;
    }

    function getKey($index)
    {
        return $this->m_map[$index][KEY];
    }

    function getValue($index)
    {
        return $this->m_map[$index][VALUE];
    }

    function searchUpperOrEqual($rnd)
    {
        if ($this->m_cur < 0) {
            return - 1;
        }
        
        $map = &$this->m_map;
        $start = 0;
        $stop = count($map) - 1;
        $center = floor($stop / 2);
        while ($map[$center][TMPVAL] < $rnd || (($center >= 1) && $map[$center - 1][TMPVAL] >= $rnd)) {
            if (! isset($map[$center])) {
                echo "Error searchUpperOrEqual";
                echo "RND: " . $rnd . "/" . $center . "\n";
                print_r($map);
                exit(- 1);
            }
            
            // echo $center." Value: ".$map[$center][TMPVAL]."\n";
            if ($map[$center][TMPVAL] < $rnd) {
                $start = $center;
                $center = ceil(($stop + $center) / 2);
                // echo "++$center\n";
            } else {
                $stop = $center;
                $center = floor(($start + $center) / 2);
                // echo "--$center\n";
            }
        }
        // echo $center.":".$rnd."\n";
        return $center;
    }

    function getRandomElement()
    {
        // if($print) echo "******START : ".$this->m_total."\n";
        $randomValue = mt_rand(1, $this->m_total);
        // if($print) echo "******Random value : ".$randomValue."\n";
        $currentIndex = 0;
        while ($this->m_map[$currentIndex][VALUE] < $randomValue && $currentIndex < $this->m_cur) {
            $currentIndex ++;
        }
        // if($print) echo "******END : ".$currentIndex." -- (".$this->m_map[$currentIndex][KEY].$this->m_map[$currentIndex][VALUE].") \n";
        return $currentIndex;
    }
}

function getImgSrc($curplayer, $db)
{
    $detail = new Detail();
    if ($curplayer->get("id_Detail") != 0)
        $detail->load($curplayer->get("id_Detail"), $db);
    
    if (($str = $detail->get("pic")) != "") {
        return "../pics/PJ/" . getPathFromPlayerName($curplayer->get("name")) . $str;
    } else {
        if (($str = $curplayer->getSub("BasicRace", "pic")) != "") {
            return "../pics/character/" . $str;
        } else {
            return "../pics/character/blank.jpg";
        }
    }
}

function getPathFromPlayerName($name)
{
    $path = "";
    $lower = strtolower($name);
    if (ord($lower{0}) >= 97 && ord($lower{0}) <= 122)
        $path .= $lower{0};
    else
        $path .= '_';
    
    $path .= '/';
    if (ord($lower{1}) >= 97 && ord($lower{1}) <= 122)
        $path .= $lower{1};
    else
        $path .= '_';
    $path .= '/';
    return $path;
}

function createTable($nbcols, $array, $title, $label, $csstable, $fromid = "", $hiddenid = "")
{
    $str = "<table " . $csstable . ">";
    
    if (count($title) != 0) {
        $str .= "<tr>";
        
        foreach ($title as $key => $val) {
            $str .= "<td " . $val[1] . ">" . $val[0] . "</td>";
        }
        
        $str .= "</tr>";
    }
    $str .= "<tr>";
    
    if ($fromid != "") {
        $str .= "<input id='" . $hiddenid . "' name='" . $hiddenid . "' type='hidden' value='' />";
        $prev = "";
        if (isset($_POST[$hiddenid])) {
            if (isset($_POST[$hiddenid . "prev"]) && $_POST[$hiddenid . "prev"] != $_POST[$hiddenid])
                $prev = $_POST[$hiddenid];
        }
        $str .= "<input id='" . $hiddenid . "prev' name='" . $hiddenid . "prev' type='hidden' value='" . $prev . "' />";
    }
    
    for ($i = 0; $i < $nbcols; $i ++) {
        if (isset($label[$i][2])) {
            $str .= "<td id='tablelabel" . $i . "' " . $label[$i][1] . " onMouseOver=\"setClassName('tablelabel" . $i . "','" . $label[$i][3] .
                 "')\" onMouseOut=\"setClassName('tablelabel" . $i . "','" . $label[$i][4] . "')\"><div onMouseDown=\"submitTheForm('" . $fromid . "','" . $hiddenid . "','" .
                 $label[$i][2] . "')\">" . $label[$i][0] . "</div></td>";
        } else {
            $str .= "<td " . $label[$i][1] . ">" . $label[$i][0] . "</td>";
        }
    }
    $str .= "</tr>";
    
    foreach ($array as $key => $val) {
        $str .= "<tr>";
        for ($i = 0; $i < $nbcols; $i ++) {
            $str .= "<td " . $val[$i][1] . ">" . $val[$i][0] . "</td>";
        }
        $str .= "</tr>";
    }
    
    $str .= "</table>";
    // $str.="</form>";
    
    return $str;
}

function getSQLCondFromArray($array, $condName, $operator)
{
    $msg = "";
    $first = 1;
    
    if (isset($array)) {
        foreach ($array as $key => $val) {
            if ($first)
                $msg = $condName . "=" . $val;
            else
                $msg .= " " . $operator . " " . $condName . "=" . $val;
            $first = 0;
        }
    }
    return $msg;
}

function unHTMLTags($string)
{
    $search = array(
        '@<script[^>]*?>.*?</script>@si', // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
                                 // '@([\r\n])[\s]+@', // Strip out white space
        '@&(quot|#34);@i', // Replace HTML entities
        '@&(amp|#38);@i',
        '@&(lt|#60);@i',
        '@&(gt|#62);@i',
        '@&(nbsp|#160);@i',
        '@&(iexcl|#161);@i',
        '@&(cent|#162);@i',
        '@&(pound|#163);@i',
        '@&(copy|#169);@i'
    ); // evaluate as php
    
    $replace = array(
        '',
        '',
        '\1',
        '"',
        '&',
        '<',
        '>',
        ' ',
        chr(161),
        chr(162),
        chr(163),
        chr(169),
        'chr(\1)'
    );
    
    $result = preg_replace($search, $replace, $string);
    
    return preg_replace_callback('@&#(\d+);@', function ($matches)
    {
        return chr($matches[1]);
    }, $result);
}

function sign($x)
{
    if ($x == 0)
        return 0;
    if ($x < 0)
        return - 1;
    if ($x > 0)
        return 1;
}

// ------------------ outils lié à la vue ou la map.
function localizedday($date)
{
    $eng_date_array = explode(" ", $date);
    
    $eng_date_array['num'] = $eng_date_array[0];
    unset($eng_date_array[0]);
    $eng_date_array['month'] = $eng_date_array[1];
    unset($eng_date_array[1]);
    $eng_date_array['year'] = $eng_date_array[2];
    unset($eng_date_array[2]);
    
    $local_date_array_db = array(
        'month' => array(
            'Jan' => localize("jan"),
            'Feb' => localize("feb"),
            'Mar' => localize("mar"),
            'Apr' => localize("apr"),
            'May' => localize("may"),
            'Jun' => localize("jun"),
            'Jul' => localize("jul"),
            'Aug' => localize("aug"),
            'Sep' => localize("sep"),
            'Oct' => localize("oct"),
            'Nov' => localize("nov"),
            'Dec' => localize("dec")
        )
    );
    
    $local_date_array['num'] = $eng_date_array['num'] . " ";
    
    foreach ($local_date_array_db['month'] as $eng_month => $local_month)
        if ($eng_month == $eng_date_array['month'])
            $local_date_array['month'] = $local_month . " ";
    
    $local_date_array['year'] = $eng_date_array['year'] . " ";
    
    $local_date = implode("", $local_date_array);
    return $local_date;
}

function translateAtt($name)
{
    switch ($name) {
        case "hp":
            return "PV";
            break;
        case "attack":
            return "attaque";
            break;
        case "defense":
            return "défense";
            break;
        case "damage":
            return "dégât";
            break;
        case "magicSkill":
            return "MM";
            break;
        case "armor":
            return "armure";
            break;
        case "dexterity":
            return "dextérité";
            break;
        case "strength":
            return "force";
            break;
        case "speed":
            return "vitesse";
            break;
        default:
            return $name;
    }
}

function translateAttshort($name)
{
    switch ($name) {
        case "hp":
            return "PV";
            break;
        case "attack":
            return "Att";
            break;
        case "defense":
            return "Déf";
            break;
        case "magicSkill":
            return "MM";
            break;
        case "armor":
            return "Arm";
            break;
        case "damage":
            return "Dég";
            break;
        case "dexterity":
            return "Dex";
            break;
        case "strength":
            return "For";
            break;
        case "speed":
            return "Vit";
            break;
        default:
            return $name;
    }
}

function distHexa($x1, $y1, $x2, $y2)
{
    $dx = $x2 - $x1;
    $dy = $y2 - $y1;
    
    $dist = (abs($dx) + abs($dy) + abs($dx + $dy)) / 2;
    
    return $dist;
}

function getXPLevelUp($level)
{
    return (5 + floor($level / 4) + floor($level / 10) + max(0, $level - 10)) * (5 + $level) + max(0, 2 * ($level - 100));
}

function convert($rgb)
{
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    return $r . " " . $g . " " . $b;
}

function RgbToCss($rgb)
{
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    $str = $r . " " . $g . " " . $b;
    
    switch ($str) {
        case "179 213 137":
            $classname = "plain";
            break;
        
        case "148 126 107":
            $classname = "mountain";
            break;
        
        case "0 0 255":
            $classname = "ocean";
            break;
        
        case "180 142 88":
            $classname = "marsh";
            break;
        
        case "144 169 88":
            $classname = "forest";
            break;
        
        case "64 128 255":
            $classname = "river";
            break;
        
        case "233 217 173":
            $classname = "desert";
            break;
        
        case "255 255 255":
            $classname = "arctic";
            break;
        
        case "151 151 150":
            $classname = "cobblestone";
            break;
        
        case "129 96 58":
            $classname = "beatenearth";
            break;
        
        case "244 192 69":
            $classname = "flowers";
            break;
        
        default:
            $classname = "";
            break;
    }
    
    return $classname;
    // return $str;
}

function getLandType($x, $y, $map, $db)
{
    require_once (HOMEPATH . "/lib/MapInfo.inc.php");
    $mapinfo = new MapInfo($db);
    $case = $mapinfo->getMap($x, $y, 0, $map);
    $landType = $case[0][0]->type;
    return $landType;
}

function getZoneFromMap($i, $j, $map, $db)
{
    require_once (HOMEPATH . "/lib/MapInfo.inc.php");
    $mapinfo = new MapInfo($db);
    $validzone = $mapinfo->getValidMap($i, $j, 0, $map);
    if ($validzone[0][0] == 0)
        return;
    
    $imgarea = imagecreatefrompng(MAPS_STATIC_FOLDER . "/maparea0" . $map . ".png");
    $rgb = imagecolorat($imgarea, $i, $j);
    $r = ($rgb >> 16) & 0xFF;
    $g = ($rgb >> 8) & 0xFF;
    $b = $rgb & 0xFF;
    $zone = floor($r / 10);
    return $zone;
}

function getTimeToMove($x, $y, $map, $db)
{
    require_once (HOMEPATH . "/lib/MapInfo.inc.php");
    $mapinfo = new MapInfo($db);
    $case = $mapinfo->getMap($x, $y, 0, $map);
    $PA = $case[0][0]->groundPaCost;
    return $PA;
}

function isCaseInCityWithWall($xp, $yp, $db)
{
    // 0 pour pas dans dans ville avec mur
    // 1 pour dans une ville avec mur
    // 2 pour sur une porte ouverte ou rempart
    $dbp = new DBCollection("SELECT * FROM Building WHERE (name='Porte ouverte' || name='Rempart') AND x=" . $xp . " AND y=" . $yp, $db);
    if (! $dbp->eof())
        return 2;
    
    $dbt = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <=5", $db);
    if ($dbt->eof())
        return 0;
    else {
        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $dbt->get("id_City"), $db);
        if ($dbc->get("type") == "Ville")
            return 1;
        elseif (distHexa($xp, $yp, $dbt->get("x"), $dbt->get("y")) <= 3 && ($dbc->get("captured") == 3 || $dbc->get("captured") == 4))
            return 1;
        else
            return 0;
    }
}

function getSchoolName($i)
{
    switch ($i) {
        case SCHOOL_WARRIOR:
            $name = "École de la Garde d'Octobian";
            break;
        case SCHOOL_ARCHERY:
            $name = "École des Archers de Krima";
            break;
        case SCHOOL_PALADIN:
            $name = "École des Paladins de Tonak";
            break;
        case SCHOOL_SHADOW:
            $name = "École de l'Ombre";
            break;
        case SCHOOL_HEALER:
            $name = "École de Guérison";
            break;
        case SCHOOL_PROTECTION:
            $name = "École de Protection";
            break;
        case SCHOOL_DESTRUCTION:
            $name = "École de Destruction";
            break;
        case SCHOOL_ALTERATION:
            $name = "École d'Altération";
            break;
        case SCHOOL_BASIC_WARRIOR:
            $name = "École Élémentaire du Guerrier";
            break;
        case SCHOOL_BASIC_MAGIC:
            $name = "École Élémentaire du Mage";
            break;
        default:
            $name = "inconnu";
            break;
    }
    
    return $name;
}

function getPlaylist()
{
    $arrayTheme = array(
        "Bone Temple - Adrian von Ziegler" => "https://www.youtube.com/watch?v=9Gj7Fq42AE0",
        "Legend - Adrian von Ziegler" => "https://www.youtube.com/watch?v=2_kKFjwpwqc",
        "Wolf blood - Adrian von Ziegler" => "https://www.youtube.com/watch?v=06H_6oI4EK4",
        "For the king - Adrian von Ziegler" => "https://www.youtube.com/watch?v=59Ri26PIOLs",
        "Druidic dreams - Adrian von Ziegler" => "https://www.youtube.com/watch?v=N2SURjzRU08",
        "Where I belong - Adrian von Ziegler" => "https://www.youtube.com/watch?v=7pzYyP6nOHU",
        "Celtic Tale - Adrian von Ziegler" => "https://www.youtube.com/watch?v=8dOpd30QapA",
        "The Sylvans Path - Adrian von Ziegler" => "https://www.youtube.com/watch?v=3Pk-bKm1q3E",
        "Feather and Skull - Adrian von Ziegler" => "https://www.youtube.com/watch?v=9RuNdb1Dt0o",
        "Stand as one - Adrian von Ziegler" => "https://www.youtube.com/watch?v=B5Y0zipUXc8",
        "Fear no Darkness - Adrian von Ziegler" => "https://www.youtube.com/watch?v=tWpe2ppRRGg",
        "Morrigan - Adrian von Ziegler" => "https://www.youtube.com/watch?v=E8LyfgvRhSM"
    );
    
    $playlist = "[";
    $nb = count($arrayTheme);
    $check = 0;
    foreach ($arrayTheme as $title => $themeurl) {
        $playlist .= "{\"title\":\"" . $title . "\",\"url\":\"" . $themeurl . "\"}";
        $check ++;
        if ($check < $nb)
            $playlist .= ",";
    }
    $playlist .= "]";
    
    return $playlist;
}

function stripAccents($string)
{
    return strtr($string, 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

function displayActivePoll($id_Member, $db)
{
    $str = "";
    
    $dbpoll = new DBCollection("SELECT * FROM Nacripoll_Main WHERE status='active'", $db); // au plus un sondage actif
    
    if ($dbpoll->count() == 1) {
        
        require_once (HOMEPATH . "/lib/BBCodeParser/BBCodeParser.inc.php");
        $participant_list = unserialize($dbpoll->get('participant_list'));
        $total = count($participant_list);
        if (in_array($id_Member, $participant_list))
            $showResults = true;
        else
            $showResults = false;
        
        $str .= "<form name='pollform'  method='POST' action='" . CONFIG_HOST . "/conquest/conquest.php?action=act' target='_self'>";
        $str .= "<table class='maintable centerareawidth'>";
        if ($showResults)
            $str .= "<tr class='mainbgtitle'><td colspan='2' align='center'> Résultats du <b>SONDAGE</b> en cours <br> " . bbcode($dbpoll->get("name")) . "</td></tr>";
        else
            $str .= "<tr class='mainbgtitle'><td colspan='2' align='center'> <b>SONDAGE</b> <br> " . bbcode($dbpoll->get("name")) . "</td></tr>";
        
        $str .= "<tr class='mainbgbody'><td colspan='2'>  " . bbcode($dbpoll->get("description")) . "</td></tr>";
        
        $dbq = new DBCollection("SELECT * FROM Nacripoll_Question WHERE id_Nacripoll_Main=" . $dbpoll->get('id'), $db);
        
        $iQ = 0;
        while (! $dbq->eof()) {
            $iQ ++;
            $str .= "<tr class='mainbgtitle'><td colspan='2'> " . bbcode($dbq->get('text')) . "</td></tr>";
            
            $iA = 0;
            $dba = new DBCollection("SELECT * FROM Nacripoll_Answer WHERE id_Nacripoll_Question=" . $dbq->get('id'), $db);
            while (! $dba->eof()) {
                $iA ++;
                if ($showResults)
                    $str .= "<tr class='mainbgbody'><td width='50px'>" . $dba->get('votes') . "/" . $total . "</td><td> " . bbcode($dba->get('text')) . "</td></tr>";
                else
                    $str .= "<tr class='mainbgbody'><td width='30px'><input type='radio' name='q_" . $iQ . "' id='q_" . $iQ . "_ans_" . $iA . "' value=" . $dba->get('id') .
                         " /></td><td> " . bbcode($dba->get('text')) . "</td></tr>";
                
                $dba->next();
            }
            
            $dbq->next();
        }
        if (! $showResults) {
            $str .= "<tr class='mainbgtitle'><td colspan='2' align='center'> <input type='submit' name='POLL' value='Envoyer le sondage !'/></td></tr>";
            $str .= "<tr class='mainbgtitle'><td colspan='2' align='center'> <input type='hidden' name='POLL_ID' value='" . $dbpoll->get('id') . "'/></td></tr>";
            $str .= "<input name='action' type='hidden' value='" . ANSWER_TO_POLL . "' />";
            $str .= "<input name='idform' type='hidden' value='" . getCurrentPageId() . "' />\n";
        }
        
        $str .= "</table>";
        
        $str .= "</form>";
    }
    
    return $str;
}

?>
