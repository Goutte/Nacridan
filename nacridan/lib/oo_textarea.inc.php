<?php

/*
 * OOHForms: textarea
 *
 * Copyright (c) 1998 by Jay Bloodworth
 *
 * $Id: of_textarea.inc,v 1.3 2002/04/28 03:59:32 richardarcher Exp $
 */
class oo_textarea extends oo_element
{

    public $m_rows;

    public $m_cols;

    public $m_wrap;
    
    // Constructor
    function oo_textarea($a)
    {
        $this->setup_element($a);
    }

    function self_get($val)
    {
        $str = "";
        $str .= "<textarea name='$this->m_name'";
        $str .= " rows='$this->m_rows' cols='$this->m_cols'";
        if ($this->m_wrap) {
            $str .= " wrap='$this->m_wrap'";
        }
        if ($this->m_errorclass) {
            $str .= " class='$this->m_errorclass'";
        }
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        $str .= ">" . htmlspecialchars($this->m_value) . "</textarea>";
        return $str;
    }
} // end TEXTAREA

?>
