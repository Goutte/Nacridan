<?php

function bbcode($text, $classe = "none")
{
    $text = nl2br($text);
    $text = preg_replace_callback("#\[img\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/img\]#si", function ($m)
    {
        return "<img src='$m[1]" . str_replace(' ', '%20', $m[3]) . "'>";
    }, $text);
    $text = preg_replace_callback("#\[url\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/url\]#si", function ($m)
    {
        return "<a class=\"stylepc\" href=\"$m[1]" . str_replace(' ', '%20', $m[3]) . "\" target=blank>$m[1]$m[3]</a>";
    }, $text);
    
    $text = preg_replace("/\[url=(.+?)\](.+?)\[\/url\]/", "<a class='$classe' href=$1>$2</a>", $text);
    
    $text = preg_replace("#\[b\](.+?)\[\/b\]#s", "<b>$1</b>", $text);
    $text = preg_replace("#\[i\](.+?)\[\/i\]#s", "<i>$1</i>", $text);
    $text = preg_replace("#\[u\](.+?)\[\/u\]#s", "<u>$1</u>", $text);
    $text = preg_replace("#\[s\](.+?)\[\/s\]#s", "<strike>$1</strike>", $text);
    $text = preg_replace("#\[center\](.+?)\[\/center\]#s", "<div align='center' width='100%'>$1</div>", $text);
    
    $text = preg_replace("#\[code\](.+?)\[\/code\]#s", "<br /><b>Code :</b><br /><div class='code'>$1</div><br />", $text);
    $text = preg_replace("#\[quote\](.+?)\[\/quote\]#s", "<br /><b>Quote :</b><br /><div class='quote'>$1</div>", $text);
    $text = preg_replace("#\[quote=(.+?)\](.+?)\[\/quote\]#s", "<br /><b>Quote $1 :</b><br /><div class='quote'>$2</div><br />", $text);
    $text = preg_replace("#\[color=(.+?)\](.+?)\[\/color\]#s", "<font color=$1>$2</font>", $text);
    return $text;
}

?>

