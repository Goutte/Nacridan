<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Exemple BBCode</title>
</head>

<body>
	
	<?php
if ($_POST["teststring"]) {
    // Inclus le perser BBCode
    require_once "bbcode.php";
    // Récupère les tags dans le fichier XML
    getBBTags('bbcode.xml');
    // Affiche le texte parsé
    echo render($_POST["teststring"]);
    echo "<hr align='center'>";
}
?>
	<form action="<?=$_SERVER["PHP_SELF"]?>" method="POST">
		Entrez votre texte a parser<br />
		<textarea cols="80" rows="10" name="teststring"><? if (!$_POST["teststring"]) { ?>[quote]Ceci est un exemple[/quote]
[b]Le gras[/b]
[i]L'italique[/i]
[u]Le souligné[/u]
[url=http://www.lalex.com/blog/]Un lien vers mon blog[/url]
Une image : [img]http://www.lalex.com/resources/smoke.gif[/img]
[list][*]Liste[*]Normale[/list]
[list=1][*]Liste[*]avec numéros[/list]
[nohtml]<strong>Empêcher l'utilisation du HTML</strong>[/nohtml]
[nobbcode][b][i]Empêcher l'utilisation du bbcode[/i][/b][/nobbcode]<? } else { echo stripslashes($_POST["teststring"]); } ?></textarea>
		<br /> <input type="submit" value="Afficher les BBCodes" />

	</form>

</body>
</html>
