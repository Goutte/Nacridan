<?php

/* NPC State */
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/lib/Graph.inc.php");
require_once (HOMEPATH . "/lib/GraphDijkstra.inc.php");

class Behavior
{

    public $db;

    public $m_graph;

    public $m_npc;

    public $m_infoPC;

    public $m_infoNPC;

    public $m_riposte;

    public $m_landspeed;

    private $MAX_DISTANCE = 50;

    public function Behavior($db)
    {
        $this->db = $db;
        $riposte = 0;
    }

    public function getRisposte()
    {
        return $this->m_riposte;
    }

    public function setRiposte($value)
    {
        $this->m_riposte = $value;
    }

    public function InitLandSpeed()
    {
        switch (getLandType($this->m_npc->get("x"), $this->m_npc->get("y"), $this->m_npc->get("map"), $this->db)) {
            case "cobblestone":
            case "beatenearth":
                $this->m_landspeed = 0.5;
                break;
            
            case "flowers":
            case "plain":
                $this->m_landspeed = 1;
                break;
            
            case "arctic":
            case "desert":
            case "forest":
                $this->m_landspeed = 1.5;
                break;
            
            case "marsh":
            case "mountain":
                $this->m_landspeed = 2;
                break;
            
            default:
                $this->m_landspeed = 2;
        }
    }

    public function playNPC($id)
    {
        $format = '%Y-%m-%d %H:%M:%S';
        
        // Chargment du monstre
        $this->m_npc = new Player();
        $this->m_npc->externDBObj("Modifier");
        $this->m_npc->load($id, $this->db);
        
        $previousAP = $this->m_npc->get("ap") + 1;
        
        $time = strtotime($this->m_npc->get('creation'));
        $curtime = time();
        $nbday = ($curtime - $time) / 3600 / 24;
        $nbloop = 0;
        while ($this->m_npc->get("ap") > 0 && ($nbloop == 0 || ($this->m_npc->get("level") > 19 && $this->m_npc->get("currhp") > 0 && $this->m_npc->get("ap") < $previousAP &&
             $nbday >= 0.5 && $this->m_npc->get("id_Player\$target") != 0))) {
                $nbloop ++;
            $previousAP = $this->m_npc->get("ap");
            if ($this->m_npc->get("ap") != 0 && ! ($this->m_npc->get("id_BasicRace") == 5 && $this->m_npc->get("id_Player\$target") == 0) && $this->m_npc->get("behaviour") != 0) {
                // Détermination de la vitesse de déplacement
                self::initLandSpeed();
                
                // Initialisation du graphe pour les recherche de chemin
                
                $this->m_graph = new GraphDijkstra($this->m_npc, 5, $this->db); // new Graph($this->m_npc->get("x"), $this->m_npc->get("y"), $this->m_npc->get("map"), $this->db);
                                                                                
                // Initialisation d'un tableau recueillant toutes les infos des
                                                                                // PJ dans la vue
                self::initInfoPC();
                
                switch ($this->m_npc->get("behaviour")) {
                    
                    case 1:
                        self::neutral();
                        break;
                    case 2:
                        self::ChauveSouris();
                        break;
                    case 3:
                        self::RatGeant();
                        break;
                    case 4:
                        self::CrapaudGeant();
                        break;
                    case 5:
                        self::Loup();
                        break;
                    case 6:
                        self::AraigneeEclipsante();
                        break;
                    case 7:
                        self::Kobold();
                        break;
                    case 8:
                        self::FourmiGuerriere();
                        break;
                    case 9:
                        self::FourmiReine();
                        break;
                    case 10:
                        self::HommeLezard();
                        break;
                    case 11:
                        self::MangeCoeur();
                        break;
                    case 12:
                        self::AraigneeSabre();
                        break;
                    case 13:
                        self::AraigneePiegeuse();
                        break;
                    case 14:
                        self::AmeEnPeine();
                        break;
                    case 15:
                        self::Fantome();
                        break;
                    case 16:
                        self::EspritTerrifiant();
                        break;
                    case 52:
                    case 17:
                        self::DoppleGanger();
                        break;
                    case 18:
                        self::AssassinRunique();
                        break;
                    case 19:
                        self::AngeNoir();
                        break;
                    case 20:
                        self::Gobelin();
                        break;
                    case 21:
                        self::ShamanGobelin();
                        break;
                    case 22:
                        self::GobelinArcher();
                        break;
                    case 23:
                        self::Goule();
                        break;
                    case 24:
                        self::Ombre();
                        break;
                    case 25:
                        self::Bleme();
                        break;
                    case 51:
                    case 26:
                        self::Golem();
                        break;
                    case 27:
                        self::Momie();
                        break;
                    case 28:
                        self::Squelette();
                        break;
                    case 29:
                        self::Gobelours();
                        break;
                    case 30:
                        self::Orc();
                        break;
                    case 31:
                        self::Horreur();
                        break;
                    case 32:
                        self::Fee();
                        break;
                    case 33:
                        self::Mante();
                        break;
                    case 34:
                    case 35:
                        self::DoppleGanger2();
                        break;
                    case 36:
                        self::Hobgobelin();
                        break;
                    case 37:
                        self::Troll();
                        break;
                    case 38:
                        self::Necromancien();
                        break;
                    case 39:
                        self::Zombie();
                        break;
                    case 40:
                        self::Combattant();
                        break;
                    case 41:
                        self::Gardien();
                        break;
                    case 42:
                        self::FeuFol();
                        break;
                    case 43:
                        self::Serpent();
                        break;
                    case 44:
                        self::Abobination();
                        break;
                    case 45:
                        self::Basilic();
                        break;
                    case 46:
                        self::Basilic2();
                        break;
                    case 47:
                        self::Manticore();
                        break;
                    case 48:
                        self::Etranglesaule();
                        break;
                    case 49:
                        self::Sauteur();
                        break;
                    case 50:
                        self::Gargouille();
                        break;
                    case 53:
                        self::AraigneeTitanesque();
                        break;
                    case 54:
                        self::GolemBiere();
                        break;
                    case 55:
                        self::DragonVent();
                        break;
                    case 56:
                        self::AraigneeColossale();
                        break;
                    case 57:
                        self::Gnoll();
                        break;
                    case 59:
                        self::Viwerne();
                        break;
                    case 58:
                        self::GMViwerne();
                        break;
                    case 60:
                        self::DragonDesDunes();
                        break;
                    case 61:
                        self::ScorpionGeant();
                        break;
                    case 62:
                        self::BasilicRoi();
                        break;
                    case 63:
                        self::Vouivre();
                        break;
                    case 64:
                        self::DragonRouge();
                        break;
                    case 65:
                        self::Phenix();
                        break;
                    case 66:
                        self::Griffon();
                        break;
                    case 67:
                        self::Kraken();
                        break;
                    case 68:
                        self::Ogre();
                        break;
                    case 69:
                        self::GardeNoir();
                        break;
                    case 70:
                        self::GuerrierPossede();
                        break;
                    case 71:
                        self::Balor();
                        break;
                    case 72:
                        self::GardeTenebres();
                        break;
                    case 80:
                        self::Demon();
                        break;
                    case 253:
                        self::KradjeckFerreux();
                        break;
                    case 1000:
                        self::Garde();
                        break;
                    default:
                }
                
                self::springDrink();
                
                unset($this->m_infoPC);
                unset($this->m_infoNPC);
                
                $this->m_npc->updateDB($this->db);
                // avec updateDBr morph plante le id_modifier ne change pas,
                // c'est bizarre.
            }
        }
    }

    /*
     * *********************************************************** DEBUT DES
     * COMPORTEMENTS ***************************************************
     */
    public function KradjeckFerreux()
    {
        $x = $this->m_npc->get("x");
        $y = $this->m_npc->get("y");
        // $curstate=min(floor($this->m_npc->get("currhp")*5/$this->m_npc->get("hp")),4);
        $dbg = new DBCollection("SELECT * FROM Gate WHERE (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2 <=5", $this->db);
        
        if ($dbg->count() && ($this->m_npc->get("id_Player\$target") != 0)) {
            $distRealPA = $this->m_graph->getRealDistance($dbg->get("x"), $dbg->get("y"));
            $dist = distHexa($this->m_npc->get("x"), $this->m_npc->get("y"), $dbg->get("x"), $dbg->get("y"));
            // $this->m_graph->getCase($this->m_npc->get("x"), $this->m_npc->get("y")), $this->m_graph->getCase($dbg->get("x"), $dbg->get("y")));
            if ($dist > 1 && $distRealPA != $this->MAX_DISTANCE && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveToCase($dbg->get("x"), $dbg->get("y"), 5);
            elseif ($dist == 1 && $this->m_npc->get("ap") > 0)
                self::backToMyWorld();
        } elseif ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", 0);
            } else {
                $dist = $this->m_infoPC[$index]["distanceBirdFly"];
				$distRealPA = $this->m_infoPC[$index]["distance"];
                if ($distRealPA >= $this->MAX_DISTANCE)
                    $this->m_npc->set("id_Player\$target", 0);
                else
                    self::moveAwayFromTarget($player->get("x"), $player->get("y"), 5);
            }
        } else
            self::moveRandom(30);
    }

    public function Garde()
    {
        $x = $this->m_npc->get("x");
        $y = $this->m_npc->get("y");
        
        $player = new Player();
        $player->externDBObj("Modifier");
        $player->load($this->m_npc->get("id_Player\$target"), $this->db);
        
        $index = self::getIndexPC($player->get("id"));
        if ($index == - 1) {
            $place = BasicActionFactory::getAleaFreePlace($player->get("x"), $player->get("y"), $player->get("map"), 2, $this->db);
            if ($place != 0) {
                
                if ($player->get("inbuilding") > 0) {
                    $this->m_npc->set("x", $player->get("x"));
                    $this->m_npc->set("y", $player->get("y"));
                    $this->m_npc->set("inbuilding", $player->get("inbuilding"));
                    $this->m_npc->set("room", $player->get("room"));
                } else {
                    $this->m_npc->set("x", $place["x"]);
                    $this->m_npc->set("y", $place["y"]);
                    $this->m_npc->set("inbuilding", 0);
                    $this->m_npc->set("room", 0);
                }
                $this->m_npc->updateHidden($this->db);
                
                BasicActionFactory::globalInfo($this->m_npc, $param);
                $param["TYPE_ACTION"] = M_TELEPORT;
                $param["TYPE_ATTACK"] = M_TELEPORT_EVENT;
                Event::logAction($this->db, M_TELEPORT, $param);
                $this->m_npc->updateDB($this->db);
            }
        } else {
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            
            if (($dist == 1 || ($dist == 0 && $this->m_npc->get("inbuilding") == $player->get("inbuilding"))) && $player->get("state") == "stunned") {
                self::arrest($player);
            } elseif (($dist == 1 || ($dist == 0 && $this->m_npc->get("inbuilding") == $player->get("inbuilding"))) && $this->m_npc->get("ap") > 6) {
                self::stun($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 6);
                $time = time();
                $time += mt_rand(5, 10) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist == 2 && $this->m_npc->get("ap") > 6 && $player->get("state") != "creeping" && $player->get("state") != "stunned") {
                self::bolas($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 6);
                $time = time();
                $time += mt_rand(5, 10) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist >= 2 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveToCase($player->get("x"), $player->get("y"), 5);
        }
    }

    public function neutral()
    {
        if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
            self::moveRandom(30);
        elseif ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", 0);
            } else {
                $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			    $distRealPA = $this->m_infoPC[$index]["distance"];
                if ($distRealPA >= $this->MAX_DISTANCE)
                    $this->m_npc->set("id_Player\$target", 0);
                elseif ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                elseif ($this->m_npc->get("ap") >= 8) {
                    self::attack($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $this->m_npc->set("playatb", $this->m_npc->get("nextatb"));
                }
            }
        }
    }

    public function chauveSouris()
    {
        $index = self::chooseTarget("closer", 0, 1, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && (6 + $distRealPA) < $this->m_npc->get("ap"))
                self::moveToCase($player->get("x"), $player->get("y"), 5);
            
            elseif ($dist == 1 && $this->m_npc->get("ap") >= 6) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 6);
                $time = time();
                $time += mt_rand(5, 10) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($distRealPA < 3 and $this->m_npc->get("ap") < 6 and $this->m_npc->get("ap") > 1)
                self::moveAwayFromTarget($player->get("x"), $player->get("y"), 5);
            
            else {
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function RatGeant()
    {
        $index = self::chooseTarget("closer", 0, 1, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            
            elseif ($this->m_npc->get("ap") >= 8) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $this->m_npc->set("playatb", $this->m_npc->get("nextatb"));
            }
        }
    }

    public function CrapaudGeant()
    {
        $index = self::chooseTarget("max", "hp", 1, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
            if ($dist <= 4 && $dist >= 3 && $this->m_npc->get("ap") > 2)
                self::JumpToCase($player->get("x"), $player->get("y"), 5);
            
            elseif ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            
            elseif ($dist == 1 && $this->m_npc->get("ap") >= 8) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $this->m_npc->set("playatb", $this->m_npc->get("nextatb"));
            }
        }
    }

    public function Loup()
    {
        $index = self::chooseTarget("min", "speed", 1, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            
            elseif ($this->m_npc->get("ap") >= 8) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AraigneeEclipsante()
    {
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMinPC("strength", 1, true));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", self::getMinPC("strength", 1, true));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distanceBirdFly"] >= 6) {
                $this->m_npc->set("id_Player\$target", self::getMinPC("strength", 1, true));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
            if ($dist > 2 && $this->m_npc->get("ap") >= 2) {
                self::moveTeleportClose($player->get("x"), $player->get("y"), 5);
            } elseif ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8) {
                $dead = self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                if (! $dead && $this->m_npc->get("ap") >= 2)
                    self::moveTeleportAway($player->get("x"), $player->get("y"), 30);
                
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Kobold()
    {
        $this->m_npc = $this->m_npc;
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMaxPC("armor", 1, true));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", self::getMinPC("armor", 1, true));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distanceBirdFly"] >= 6) {
                $this->m_npc->set("id_Player\$target", self::getMinPC("armor", 1, true));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
            if ($curstate == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveAwayFromTarget($player->get("x"), $player->get("y"), 15);
            } elseif ($dist >= 3 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($dist == 2 && $this->m_npc->get("ap") >= 8) {
                $nb = mt_rand(1, 4);
                
                if ($nb == 1)
                    self::bdfmissed();
                else
                    self::fireball($player);
                
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
            } elseif ($dist == 1 && $this->m_npc->get("ap") <= 7 && mt_rand(0, 1)) {
                self::moveAwayFromTarget($player->get("x"), $player->get("y"), 30);
            } elseif ($dist == 1 && $this->m_npc->get("ap") >= 8) {
                $nb = mt_rand(0, 1);
                if ($nb)
                    self::attack($player);
                else {
                    $nb = mt_rand(1, 4);
                    if ($nb == 1)
                        self::bdfmissed();
                    else
                        self::fireball($player);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function FourmiGuerriere()
    {
        $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
        $move = 0;
        if ($curstate == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
            self::initInfoNPC();
            $id = self::getCloserRace(107, 4);
            if ($id != 0) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($id, $this->db);
                $index = self::getIndexNPC($player->get("id"));
				$dist = $this->m_infoNPC[$index]["distanceBirdFly"];
				$distRealPA = $this->m_infoNPC[$index]["distance"];
                if ($dist >= 2) {
                    self::moveToCase($player->get("x"), $player->get("y"), 15);
                    $move = 1;
                }
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") == 0 && $move == 0) {
            $this->m_npc->set("id_Player\$target", self::getMinPC("dexterity", 1, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0 && $move == 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", self::getMaxPC("dexterity", 1, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMinPC("armor", 1, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0 && $move == 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function FourmiReine()
    {
        $this->m_npc = $this->m_npc;
        $curstate = 5;
        
        if ($this->m_npc->get("ap") >= 6) {
            $index = - 1;
            self::initInfoNPC();
            for ($i = 0; $i < count($this->m_infoNPC); $i ++) {
				$dist = $this->m_infoNPC[$i]["distanceBirdFly"];
				$distRealPA = $this->m_infoNPC[$i]["distance"];
                if ($dist == 1 && $this->m_infoNPC[$i]["id_BasicRace"] == 106 && ($this->m_infoNPC[$i]["currhp"] * 5 / $this->m_infoNPC[$i]["hp"]) <= 2) {
                    if ($curstate > ($this->m_infoNPC[$i]["currhp"] * 5 / $this->m_infoNPC[$i]["hp"])) {
                        $index = $i;
                        $curstate = ($this->m_infoNPC[$i]["currhp"] * 5 / $this->m_infoNPC[$i]["hp"]);
                    }
                }
            }
            if ($index != - 1) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($this->m_infoNPC[$index]["id"], $this->db);
                self::firstAid($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 6);
                $time = time();
                $time += mt_rand(30, 60) * 60;
            }
        }
        
        if ($this->m_npc->get("ap") >= 12) {
            $id = self::chooseCloserPC(1, false);
            if ($id != 0) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($id, $this->db);
                if (distHexa($this->m_npc->get("x"), $this->m_npc->get("y"), $player->get("x"), $player->get("y")) <= 2) {
                    self::attack($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 12);
                    $this->m_npc->set("playatb", $this->m_npc->get("nextatb"));
                }
            }
        }
    }

    public function HommeLezard()
    {
        $id = self::getCloserState("creeping", 3,false);
        if ($id != 0)
            $this->m_npc->set("id_Player\$target", $id);
        
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMaxPC("damage", 1.5, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", self::getMaxPC("damage", 1.5, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMaxPC("damage", 1.5, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($dist ==1 && $this->m_npc->get("ap") >= 8) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 10) {
                    $isProjection = self::ProjectionIfProtected($player);
                    if (! $isProjection) {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 2);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function MangeCoeur()
    {
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMaxPC("magicSkill", 1.5, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", self::getMaxPC("magicSkill"), 1.5, false);
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMaxPC("magicSkill", 1.5, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist == 2 && $player->get("state") == "walking" && $this->m_npc->get("ap") >= 9) {
                
                self::bolas($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 9);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 10) {
                    $isProjection = self::ProjectionIfProtected($player);
                    if (! $isProjection) {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 2);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AraigneeSabre()
    {
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMinPC("defense", 2, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                $this->m_npc->set("id_Player\$target", self::getMinPC("defense", 2, false));
                if ($this->m_npc->get("id_Player\$target") == 0)
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMinPC("defense", 2, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
           $dist = $this->m_infoPC[$index]["distanceBirdFly"];
				$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8) {
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AraigneePiegeuse()
    {
        $dbbx = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Piège'", $this->db);
        $piege = $dbbx->count();
        
        $this->m_npc->set("id_Player\$target", self::chooseCloserPC(5, false));
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
				$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist >= 4 && $this->m_npc->get("ap") >= 1 && min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4) == 4) {
                $bm = new BM();
                $bm->set("name", "Piège");
                $bm->set("effect", "POSITIVE");
                $bm->set("level", floor($this->m_npc->get("currhp") / 10));
                $bm->set("id_Player", $this->m_npc->get("id"));
                $bm->set("id_Player\$src", $this->m_npc->get("id"));
                $bm->set("date", gmdate("Y-m-d H:i:s"));
                $bm->set("life", - 2);
                $bm->set("id_StaticModifier", 0);
                $bm->addDBr($this->db);
                $this->m_npc->set("currhp", $this->m_npc->get("currhp") % 10);
                PlayerFactory::initBM($this->m_npc, $this->m_npc->getObj("Modifier"), $param, $this->db);
            } elseif (! $piege && $dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Piège'", $this->db);
                if (! $dbc->eof()) {
                    $this->m_npc->set("currhp", $this->m_npc->get("currhp") + 10 * $dbc->get("level"));
                    $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Piège'", $this->db);
                    PlayerFactory::initBM($this->m_npc, $this->m_npc->getObj("Modifier"), $param2, $this->db, 0);
                }
                $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Piège'", $this->db);
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AmeEnPeine()
    {
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMinPC("strength", 3, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                
                $this->m_npc->set("id_Player\$target", self::getMinPC("strength", 3, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMinPC("strength", 3, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    self::attack($player);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Fantome()
    {
        if (! mt_rand(0, 3))
            $this->m_npc->set("hidden", 0);
        
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("hidden", 0);
            $this->m_npc->set("id_Player\$target", self::getMinPC("magicSkill", 4, true));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= 1)
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                
                $this->m_npc->set("hidden", 0);
                $this->m_npc->set("id_Player\$target", self::getMinPC("magicSkill", 4, true));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distanceBirdFly"] >= 6) {
                $this->m_npc->set("hidden", 0);
                $this->m_npc->set("id_Player\$target", self::getMinPC("magicSkill", 4, true));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
            if ($dist > 2 && $this->m_npc->get("ap") > 1) {
                $this->m_npc->set("hidden", 0);
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($dist < 3 && $this->m_npc->get("ap") >= 8) {
                $modifier = new Modifier();
                $this->m_npc->set("hidden", 0);
                
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") * 1.1) {
                    self::fireball($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                } else {
                    self::instantBlood($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                }
                if (mt_rand(0, 1))
                    $this->m_npc->set("hidden", 1);
                
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function EspritTerrifiant()
    {
        self::initInfoNPC();
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMinPC("dexterity", 3, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            // self::initInfoNPC();
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                
                $this->m_npc->set("id_Player\$target", self::getMinPC("dexterity", 3, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMinPC("dexterity", 3, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            
            if ($dist > 1 && (8 + $dist) < $this->m_npc->get("ap")) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    $modifier = new Modifier();
                    $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                    self::attack($player, $modifier, null, ABILITY_DAMAGE);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($this->m_npc->get("ap") > 3 && ($id = self::getCloserRace(112, 2))) {
                $friend = new Player();
                $friend->externDBObj("Modifier");
                $friend->load($id, $this->db);
                self::barrier($friend);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
                if ($dist > 1)
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function DoppleGanger()
    {
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getMaxPC("level", 4, false));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            if ($index == - 1) { // Pas dans la vue
                
                $this->m_npc->set("id_Player\$target", self::getMaxPC("level", 4, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } elseif ($this->m_infoPC[$index]["distance"] >= $this->MAX_DISTANCE) {
                $this->m_npc->set("id_Player\$target", self::getMaxPC("level", 4, false));
                if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            }
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist < 4 && $this->m_npc->get("ap") > 8) {
                self::morph($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 3 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function DoppleGanger2()
    {
        
        // Dopple morphé en guerrier
        if ($this->m_npc->get("behaviour") == 34) {
            
            $index = self::chooseTarget("min", "defense", 4, false);
            
            if ($this->m_npc->get("id_Player\$target") != 0) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($this->m_npc->get("id_Player\$target"), $this->db);
                $index = self::getIndexPC($player->get("id"));
                $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
                if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                    self::attack($player, null, null, ABILITY_KNOCKOUT);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
        }
        
        // Dopple morphé en magicien
        if ($this->m_npc->get("behaviour") == 35) {
            $index = self::chooseTarget("min", "defense", 4, true);
            if ($this->m_npc->get("id_Player\$target") != 0) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($this->m_npc->get("id_Player\$target"), $this->db);
                $index = self::getIndexPC($player->get("id"));
                $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
                if ($dist > 2 && $this->m_npc->get("ap") > 1)
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                
                elseif ($dist < 3 && $this->m_npc->get("ap") >= 8) {
                    self::blood($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                } elseif ($this->m_npc->get("ap") > 3) {
                    self::armor($this->m_npc);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
        }
    }

    public function AssassinRunique()
    {
        $index = self::chooseTarget("max", "defense", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                $modifier = new Modifier();
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AngeNoir()
    {
        $index = self::chooseTarget("max", "damage", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 2 && $this->m_npc->get("ap") > 1) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif (3 > $dist && $this->m_npc->get("ap") >= 8) {
                self::blood($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($this->m_npc->get("ap") > 4) {
                self::initInfoNPC();
                $id = self::getCloserRace(116, 2);
                if ($id) {
                    $player = new Player();
                    $player->externDBObj("Modifier");
                    $player->load($id, $this->db);
                    self::anger($player, "dégât");
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
        }
    }

    public function Gobelin()
    {
        $index = self::chooseTarget("min", "speed", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 10) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.5)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 2);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function ShamanGobelin()
    {
        self::initInfoNPC();
        $index = self::getCloserWounded(2.8, 3, 0);
		
		$distRealPA = 0;
        if ($index != - 1) {
            $dist = $this->m_infoNPC[$index]["distanceBirdFly"];
            $distRealPA = $this->m_infoNPC[$index]["distance"];
        }
		
        if ($index != - 1 && $this->m_npc->get("ap") >= 5 + $distRealPA) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_infoNPC[$index]["id"], $this->db);
			
            if ($this->m_graph->getRealDistance($player->get("x"), $player->get("y")) > 2)
                // while ($this->m_graph->distReel($this->m_graph->getCase($this->m_npc->get("x"), $this->m_npc->get("y")), $this->m_graph->getCase($player->get("x"), $player->get("y"))) >
                // 2)
                self::moveToCase($player->get("x"), $player->get("y"), 5);
            
            if ($dist < 3) {
                self::tears($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        } else {
            $index = self::chooseTarget("min", "defense", 4, true);
            if ($this->m_npc->get("id_Player\$target") != 0) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($this->m_npc->get("id_Player\$target"), $this->db);
                $index = self::getIndexPC($player->get("id"));
                $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			    $distRealPA = $this->m_infoPC[$index]["distance"];
                if ($dist > 2 && $this->m_npc->get("ap") > 1) {
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                } elseif (3 > $dist && $this->m_npc->get("ap") >= 8) {
                    self::fireball($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                } elseif ($this->m_npc->get("ap") > 4) {
                    self::initInfoNPC();
                    $id = self::getCloserRace(120, 2);
                    if ($id) {
                        $player = new Player();
                        $player->externDBObj("Modifier");
                        $player->load($id, $this->db);
                        self::shield($player);
                        $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                        $time = time();
                        $time += mt_rand(30, 60) * 60;
                        $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                    }
                }
            }
        }
    }

    public function GobelinArcher()
    {
        $index = self::chooseTarget("max", "defense", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist < 4) {
                self::archery($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 3 && $this->m_npc->get("ap") >= $distRealPA + 8) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($dist < 3 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveAwayFromTarget($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Goule()
    {
        $index = self::chooseTarget("min", "defense", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    self::attack($player, null, null, 608);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") >= $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Ombre()
    {
        $index = self::chooseTarget("max", "defense", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist <= 2) {
                self::instantBlood($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 2 && $this->m_npc->get("ap") >= $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Bleme()
    {
        $index = self::chooseTarget("max", "damage", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist <= 2) {
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_StaticModifier_BM=7 AND id_Player=" . $player->get("id"), $this->db);
                if ($dbc->eof())
                    self::curse($player, 2);
                else
                    self::instantBlood($player);
                
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 2 && $this->m_npc->get("ap") >= $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Golem()
    {
        $index = self::chooseTarget("min", "hp", 4, false);
        
        $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
        if ($curstate <= 1 && $this->m_npc->get("ap") >= 4) {
            self::autoregen();
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
            $time = time();
            $time += mt_rand(30, 60) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            
            elseif ($dist == 1 && $this->m_npc->get("ap") >= 8) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    self::attack($player);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Squelette()
    {
        $index = self::chooseTarget("min", "speed", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 10) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $isProjection = self::ProjectionIfProtected($player);
                        if (! $isProjection) {
                            $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                            self::attack($player, $modifier, null, ABILITY_DAMAGE);
                        }
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 2);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Momie()
    {
        $index = self::chooseTarget("min", "speed", 3, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                self::fireball($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Gobelours()
    {
        $index = self::chooseTarget("min", "hp", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 9 && $dist == 1) {
                if (self::getNbOpponentClose() > 1)
                    self::twirl();
                else {
                    // $modifier = new Modifier();
                    // $modifier->setModif("damage",DICE_D6,floor($this->m_npc->getModif("damage",DICE_D6)*0.3));
                    $isProjection = self::ProjectionIfProtected($player);
                    if (! $isProjection) {
                        self::attack($player, null, null, ABILITY_STUNNED);
                    }
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 9);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Orc()
    {
        $index = self::chooseTarget("max", "damage", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    $modifier = new Modifier();
                    $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                    self::attack($player, $modifier, null, ABILITY_DAMAGE);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Horreur()
    {
        $index = self::chooseTarget("min", "defense", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $ret = self::infest($player);
                if ($ret) {
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
        }
    }

    public function Fee()
    {
        self::initInfoNPC();
        $id = self::getCloserRace(131, 2);
        
        if ($id > 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($id, $this->db);
            $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id") . " AND name='Ailes de Colère'", $this->db);
            if ($dbc->count() < 2) {
                if ($dbc->count() > 0 && $dbc->get("id_StaticModifier_BM") == 4)
                    self::Anger($player, 2);
                else
                    self::Anger($player, 1);
                
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
        
        $index = self::chooseTarget("max", "defense", 3, true);
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
            if ($dist > 2 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist <= 2) {
                self::instantBlood($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Mante()
    {
        $index = self::chooseTarget("min", "hp", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Hobgobelin()
    {
        $index = self::chooseTarget("min", "hp", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 9 && $dist == 1) {            	 
                if (self::getNbOpponentClose() > 1)
                    self::twirl();
                else{
                	$isProjection = self::ProjectionIfProtected($player);
                	if (! $isProjection) {
                    self::attack($player, null, null, ABILITY_STUNNED);
                	}		
                }
                    
                
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 9);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Troll()
    {
        $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
        if ($curstate < 4 && $this->m_npc->get("ap") >= 12) {
            self::autoregen();
            $dbc = new DBCollection("DELETE FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='autoRegen'", $this->db);
        }
        
        $index = self::chooseTarget("max", "armor", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("attack", DICE_D6) * 0.5));
                self::attack($player, $modifier, null, ABILITY_DAMAGE);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Necromancien()
    {
        self::initInfoNPC();
        $id = self::getCloserRace(124, 4);
        if ($id > 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($id, $this->db);
            $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
            if ($curstate < 3 && $this->m_npc->get("ap") >= 2) {
                self::drain($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 2);
            }
            $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id") . " AND name='Ailes de Colère'", $this->db);
            if ($this->m_npc->get("ap") >= 2 && $dbc->count() == 0) {
                self::anger($player, 2, 1);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 2);
            }
        } elseif ($this->m_npc->get("ap") >= 8) {
            self::invoke(124);
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
            $time = time();
            $time += mt_rand(30, 60) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
        }
        
        $index = self::chooseTarget("max", "armor", 3, false);
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
           $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
            if ($this->m_npc->get("ap") >= 10 && $dist == 1) {
                $modifier = new Modifier();
                $modifier->setModif("speed", DICE_D6, floor($this->m_npc->getModif("magicSkill", DICE_D6) * 0.4));
                self::curse($player, 3, $modifier);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
            }
            if ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                $modifier = new Modifier();
                $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("magicSkill", DICE_D6) * 0.6));
                self::attack($player, $modifier, null, ABILITY_THRUST);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Zombie()
    {
        $index = self::chooseTarget("max", "damage", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                self::attack($player, $modifier, null, ABILITY_DAMAGE);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Combattant()
    {
        $index = self::chooseTarget("min", "speed", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 15) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.4));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 4);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Gardien()
    {
        $index = self::chooseTarget("max", "level", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 15);
            }
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense")) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("attack", DICE_D6, 4);
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("attack", DICE_D6, 4);
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.5) + 4);
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("damage", DICE_D6, 4);
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.3)) + 4);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                
                if ($this->m_npc->get("ap") >= 1)
                    self::moveAwayFromTarget($player->get("x"), $player->get("y"), 5);
                
                if ($this->m_npc->get("ap") >= 3) {
                    $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Garde'", $this->db);
                    if ($dbc->count() == 0) {
                        self::guard();
                        $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
                    }
                }
                
                if ($this->m_npc->get("ap") >= 3) {
                    $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Armure d\'Athlan'", $this->db);
                    if ($dbc->count() == 0) {
                        self::armor($this->m_npc, 2);
                        $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
                    }
                }
            }
        }
    }

    public function FeuFol()
    {
        $indexMaxArmor = 0;
        $indexMaxDefense = self::chooseTarget("max", "defense", 3, true);
        if ($indexMaxDefense == - 1 || $this->m_infoPC[$indexMaxDefense]["magicSkill"] > $this->m_npc->getScore("magicSkill") * 0.5) {
            $this->m_npc->set("id_Player\$target", 0);
            $indexMaxArmor = self::chooseTarget("max", "armor", 3, true);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($indexMaxArmor == 0) {
                if ($dist > 2 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                } elseif ($this->m_npc->get("ap") >= 8 && $dist <= 2) {
                    self::blood($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            } else {
                if ($dist > 3 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                    self::moveToCase($player->get("x"), $player->get("y"), 30);
                } elseif ($this->m_npc->get("ap") >= 7 && $dist == 3) {
                    self::fireball($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                } elseif ($this->m_npc->get("ap") >= 8 && $dist <= 3) {
                    self::blood($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
        }
    }

    public function Serpent()
    {
        $index = self::chooseTarget("min", "defense", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    self::attack($player, null, null, 608);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Abobination()
    {
        $index = self::chooseTarget("max", "damage", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                self::attack($player, $modifier, null, ABILITY_DAMAGE);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Basilic()
    {
        $index = self::chooseTarget("max", "defense", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
            if ($this->m_npc->get("ap") >= 10 && $dist == 1) {
                $modifier = new Modifier();
                $modifier->setModif("speed", DICE_D6, floor($this->m_npc->getModif("magicSkill", DICE_D6) * 0.4));
                self::curse($player, 3, $modifier, 1);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
            }
            if ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                // $modifier = new Modifier();
                // $modifier->setModif("magicSkill",DICE_D6,floor($this->m_npc->getModif("magicSkill",DICE_D6)*0.3));
                self::fireball($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Basilic2()
    {
        $index = self::chooseTarget("max", "hp", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Manticore()
    {
        $index = self::chooseTarget("min", "speed", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 15) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $isProjection = self::ProjectionIfProtected($player);
                        if (! $isProjection) {
                            $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.4));
                            self::attack($player, $modifier, null, ABILITY_DAMAGE);
                        }
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 4);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Etranglesaule()
    {
        $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
        if ($curstate <= 1 && $this->m_npc->get("ap") >= 4) {
            self::autoregen();
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
            $time = time();
            $time += mt_rand(30, 60) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
        }
        
        $index = self::chooseTarget("closer", 0, 4, true);
        if ($this->m_npc->get("id_Player\$target") != 0) {
            
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            // $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            $ok = 0;
            if ($dist >= 3 && $this->m_npc->get("ap") >= 4) {
                $voisin = array();
                $this->m_graph->initGraph();
                $voisin = $this->m_graph->getVoisin($this->m_graph->getCase($player->get("x"), $player->get("y")));
                for ($i = 0; $i < count($voisin); $i ++) {
                    // if ($this->m_graph->distReel(46, $voisin[$i]) < $dist)
                    if ($dist > distHexa($this->m_graph->getGrille(46, "x"), $this->m_graph->getGrille(46, "y"), $this->m_graph->getGrille($voisin[$i], "x"), 
                        $this->m_graph->getGrille($voisin[$i], "y")))
                        $ok = $voisin[$i];
                }
                if ($ok != 0) {
                    self::aether($player, $this->m_graph->getGrille($ok, "x"), $this->m_graph->getGrille($ok, "y"));
                    $player->reload($this->db);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
            
            if ($dist <= 2) {
                if ($player->get("state") != "creeping" && $this->m_npc->get("ap") >= 4) {
                    
                    self::immobilise($player);
                    $player->reload($this->db);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
                
                if ($this->m_npc->get("ap") >= 8) {
                    $isProjection = self::ProjectionIfProtected($player);
                    if (! $isProjection) {
                        self::attack($player);
                    }
                    $player->reload($this->db);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
                    $time = time();
                    $time += mt_rand(30, 60) * 60;
                    $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                }
            }
        }
    }

    public function Gargouille()
    {
        $index = self::chooseTarget("max", "hp", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Sauteur()
    {
        $index = self::chooseTarget("min", "defense", 3, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 15) {
                        if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                            $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                            self::attack($player, null, $modifier, ABILITY_THRUST);
                        } else {
                            $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.4));
                            self::attack($player, $modifier, null, ABILITY_DAMAGE);
                        }
                    } else {
                        $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 4);
                        self::attack($player, $modifier, null, ABILITY_POWERFUL);
                    }
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AraigneeTitanesque()
    {
        $index = self::chooseTarget("min", "defense", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            $nb = mt_rand(0, 1);
            if ($this->m_npc->get("ap") >= 8 && $dist == 1 && $nb) {
                self::attack($player, null, null, 608);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $isProjection = self::ProjectionIfProtected($player);
                if (! $isProjection) {
                    self::attack($player, null, null, ABILITY_STUNNED);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function DragonVent()
    {
        $index = self::chooseTarget("min", "defense", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $this->m_npc->addModif("attack", DICE_D6, $this->m_npc->getModif("speed", DICE_D6) * 0.7);
                self::fireball($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function GolemBiere()
    {
        $index = self::chooseTarget("min", "defense", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 3) {
                self::bier();
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function AraigneeColossale()
    {
        $index = self::chooseTarget("min", "defense", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                self::attack($player, null, null, 608);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Gnoll()
    {
        $index = self::chooseTarget("min", "defense", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 15) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.5));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.3)) + 4);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function Viwerne()
    {
        $index = self::chooseTarget("min", "hp", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 20) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.3));
                        self::attack($player, null, null, ABILITY_STUNNED);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 2);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function GMViwerne()
    {
        self::initInfoNPC();
        $nb = self::getNBWounded();
        if ($nb > 2 && $this->m_npc->get("ap") >= 8) {
            self::pluie();
            $time = time();
            $time += mt_rand(30, 60) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
        } 

        elseif ($nb >= 1 && $this->m_npc->get("ap") >= 3) {
            $index = self::getCloserWounded(5, 3, 0);
            if ($index != - 1) {
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($this->m_infoNPC[$index]["id"], $this->db);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
                self::tears($player);
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id") . " AND name='Régénération'", $this->db);
                if ($dbc->count() == 0 && $this->m_npc->get("ap") >= 3) {
                    self::regen($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
                }
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id") . " AND name='Bouclier magique'", $this->db);
                if ($dbc->count() == 0 && $this->m_npc->get("ap") >= 3) {
                    self::shield($player);
                    $this->m_npc->set("ap", $this->m_npc->get("ap") - 3);
                }
            }
        }
        
        $index = self::chooseTarget("min", "magicSkill", 4, true);
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist < 4) {
                self::instantBlood($player);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
            } elseif ($dist > 3 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function DragonDesDunes()
    {
        $index = self::chooseTarget("min", "hp", 4, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($this->m_npc->get("ap") >= 8 && $dist <= 3) {
                $this->m_npc->addModif("attack", DICE_D6, $this->m_npc->getModif("speed", DICE_D6) * 0.5);
                self::fireball($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function ScorpionGeant()
    {
        $index = self::chooseTarget("min", "hp", 4, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 6);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($this->m_npc->get("ap") >= 5 && $dist == 1) {
                self::attack($player, null, null, 608);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($dist > 1 && $this->m_npc->get("ap") > $distRealPA) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            }
        }
    }

    public function BasilicRoi()
    {
        $index = self::chooseTarget("max", "armor", 5, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 4 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist <= 4) {
                self::poison($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($this->m_npc->get("ap") >= 5 && $dist <= 2) {
                
                self::curse($player, mt_rand(1, 3));
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Vouivre()
    {
        $index = self::chooseTarget("min", "defense", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist == 1) {
                
                self::attack($player, null, null, ABILITY_TREACHEROUS);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function DragonRouge()
    {
        $index = self::chooseTarget("max", "hp", 5, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 3 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 10 && $this->m_InfoPC->count() > 2) {
                self::brasier();
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 10);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            } elseif ($this->m_npc->get("ap") >= 7) {
                $this->m_npc->addModif("magicSkill", DICE_D6, $this->m_npc->getModif("magicSkill", DICE_D6) * 0.4);
                self::fireball($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Griffon()
    {
        $index = self::chooseTarget("max", "speed", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 14) {
                self::attack($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 10);
            }
            
            if ($this->m_npc->get("ap") >= 7) {
                self::attack($player, null, null, ABILITY_PROJECTION);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Phenix()
    {
        self::initInfoNPC();
        $index = self::getCloserWounded(2.8, 3, 1);
        
        if ($index != - 1 && $this->m_npc->get("ap") >= 5) {
            
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_infoNPC[$index]["id"], $this->db);
            
            if ($this->m_infoNPC[$index]["distanceBirdFly"] < 3) {
                if ($player->get("id") == $this->m_npc->get("id"))
                    self::tears($this->m_npc);
                else
                    self::tears($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
        
        $index = self::chooseTarget("max", "damage", 5, true);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 3 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 7 && $dist < 3) {
                self::blood($player);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 7);
            }
        }
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Barrière enflammée'", $this->db);
        if ($this->m_npc->get("ap") >= 5 && $dbbm->eof()) {
            $this->m_npc->addModif("strength", DICE_D6, $this->m_npc->getModif("magicSkill", DICE_D6));
            self::barrier($this->m_npc);
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 5);
            $time = time();
            $time += mt_rand(30, 60) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
        }
    }

    public function Kraken()
    {
        $index = self::chooseTarget("max", "hp", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8) {
                self::attack($player, null, null, ABILITY_STUNNED);
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Ogre()
    {
        $index = self::chooseTarget("max", "attack", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 9 && $dist == 1) {
                if (self::getNbOpponentClose() > 1)
                    self::twirl();
                else {
                    // $modifier = new Modifier();
                    // $modifier->setModif("damage",DICE_D6,floor($this->m_npc->getModif("damage",DICE_D6)*0.3));
                    self::attack($player, null, null, ABILITY_STUNNED);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
        
        if ($this->m_npc->get("ap") >= 4) {
            $this->m_npc->addModif("magicSkill", DICE_D6, $this->m_npc->getModif("strength", DICE_D6) * 0.5);
            self::armor($this->m_npc);
            $time = time();
            $time += mt_rand(30, 60) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
        }
    }

    public function GardeNoir()
    {
        $index = self::chooseTarget("max", "armor", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 20) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.4));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 4);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function GuerrierPossede()
    {
        $index = self::chooseTarget("max", "armor", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            if ($dist > 1 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db)) {
                self::moveToCase($player->get("x"), $player->get("y"), 30);
            } elseif ($this->m_npc->get("ap") >= 12 && $dist == 1) {
                // self::sharpen();
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 4);
            }
            
            if ($this->m_npc->get("ap") >= 8 && $dist == 1) {
                $modifier = new Modifier();
                if ($this->m_npc->getScore("attack") > $player->getScore("defense") + 25) {
                    if ($player->getScore("armor") > $this->m_npc->getScore("damage") / 3) {
                        $modifier->setModif("armor", DICE_ADD, - (floor($player->getModif("armor_bm", DICE_ADD) * 0.8)));
                        self::attack($player, null, $modifier, ABILITY_THRUST);
                    } else {
                        $modifier->setModif("damage", DICE_D6, floor($this->m_npc->getModif("damage", DICE_D6) * 0.4));
                        self::attack($player, $modifier, null, ABILITY_DAMAGE);
                    }
                } else {
                    $modifier->setModif("attack", DICE_D6, (floor($this->m_npc->getModif("attack", DICE_D6) * 0.2)) + 4);
                    self::attack($player, $modifier, null, ABILITY_POWERFUL);
                }
                
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Garde'", $this->db);
                if ($dbc->eof())
                    self::guard();
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 8);
                $time = time();
                $time += mt_rand(30, 60) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            }
        }
    }

    public function Demon()
    {
        $dbc = new DBCollection("SELECT * FROM BM WHERE name='Soleil de guérison' AND id_Player=" . $this->m_npc->get("id"), $this->db);
        if ($dbc->eof())
            self::sun();
        /*
         * $xp = $this->m_npc->get("x");
         * $yp = $this->m_npc->get("y");
         * $map = $this->m_npc->get("map");
         *
         * $cond = "(name='Soleil de guérison' or name='Bénédiction' or
         * name='Charme de vitalité' or name='Ailes de Colère' or
         * name='Bouclier magique' or name='Barrière enflammée'";
         * $cond.=" or name='Aura de résistance' or name='Aura de courage' or
         * id_StaticModifier_BM = 9)";
         * $dbbm = "SELECT id FROM Player WHERE status='PC' AND map=".$map." AND
         * (abs(x-".$xp.") + abs(y-".$yp.") + abs(x+y-".$xp."-".$yp."))/2 <= 4";
         * $dbc = new DBCollection("SELECT * FROM BM WHERE ".$cond." AND
         * id_Player IN (".$dbbm.")", $this->db);
         * if($dbc->count() > 3)
         * self::Neant();
         */
    }
    
    // 11 4 5 9 Régénération - Bénédiction - Soleil de guérison -
    // Barrière enflammée
    public function Combo1()
    {
        $index = self::chooseTarget("max", "damage", 5, false);
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $player = new Player();
            $player->load($this->m_npc->get("id_Player\$target"), $this->db);
            $player->externDBObj("Modifier");
            $index = self::getIndexPC($player->get("id"));
            $dist = $this->m_infoPC[$index]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$index]["distance"];
            $case = $this->m_graph->getPlaceForWall(46);
            if ($dist == 1 && $case != - 1) {
                $player->set("x", $this->m_graph->getGrille($case, "x"));
                $player->set("y", $this->m_graph->getGrille($case, "y"));
                $player->updateDB($this->db);
                $param["ERROR"] = 0;
                BasicActionFactory::globalInfo($this->m_npc, $param);
                BasicActionFactory::globalInfoOpponent($player, $param);
                $param["TYPE_ATTACK"] = M_EVIL_BLOW_EVENT;
                $param["TYPE_ACTION"] = M_EVIL_BLOW;
                $param["X"] = $player->get("x");
                $param["Y"] = $player->get("y");
                Event::logAction($this->db, M_EVIL_BLOW, $param);
                $this->m_npc->updateDB($this->db);
                $this->m_graph->initGraph();
                $adj = $this->m_graph->getVoisin($case);
                self::wall($adj);
                
                self::blood($player);
            }
        }
        
        /*
         * $dbnpc = new DBCollection("SELECT * FROM Player WHERE
         * id_Team=".$this->m_npc->get("id_Team"), $db);
         * while(!$dbnpc->eof())
         * {
         *
         * $index = self::chooseTarget("max", "armor", 5, false);
         *
         * if($this->m_npc->get("id_Player\$target") != 0)
         * {
         * $player = new Player();
         * $player->externDBObj("Modifier");
         * $player->load($this->m_npc->get("id_Player\$target"), $this->db);
         * $index = self::getIndexPC($player->get("id"));
         * $dist = $this->m_infoPC[$index]["distanceBirdFly"];
         * if($dist > 1 && $this->m_npc->get("ap") >=
         * $distRealPA)
         * {
         * self::moveToCase($player->get("x"), $player->get("y"),30);
         * }
         * elseif($this->m_npc->get("ap") >= 8 && $dist==1)
         * {
         * $modifier = new Modifier();
         * if($this->m_npc->getScore("attack") >
         * $player->getScore("defense")+20)
         * {
         * if($player->getScore("armor") > $this->m_npc->getScore("damage") / 3)
         * {
         * $modifier->setModif("armor",DICE_ADD,-(floor($opponent->getModif("armor_bm",DICE_ADD)*0.5)));
         * self::attack($player, null, $modifier, ABILITY_THRUST);
         * }
         * else
         * {
         * $modifier->setModif("damage",DICE_D6,floor($this->m_npc->getModif("damage",DICE_D6)*0.4));
         * self::attack($player, $modifier, null, ABILITY_DAMAGE);
         * }
         * }
         * else
         * {
         * $modifier->setModif("attack",DICE_D6,(floor($this->m_npc->getModif("attack",DICE_D6)*0.2))+4);
         * self::attack($player, $modifier, null, ABILITY_POWERFUL);
         * }
         * $this->m_npc->set("ap", $this->m_npc->get("ap")-8);
         * $time=time();
         * $time+=mt_rand(30,60)*60;
         * $this->m_npc->set("playatb",gmdate("Y-m-d H:i:s",$time));
         * }
         * }
         * }
         */
    }

    /*
     * ********************************************************* FONCTION DE
     * CHOIX DE CIBLE ******************************************************
     */
    protected function searchPlaceForWall($dist)
    {}

    protected function chooseTarget($cond, $charac = 0, $ratio = 1, $isBirdFly = false)
    {
        $index = - 1;
        
        if ($this->m_npc->get("id_Player\$target") == 0) {
            $this->m_npc->set("id_Player\$target", self::getPC($cond, $charac, $ratio, $isBirdFly));
            if ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                self::moveRandom(30);
        }
        
        if ($this->m_npc->get("id_Player\$target") != 0) {
            $index = self::getIndexPC($this->m_npc->get("id_Player\$target"));
            if ($index == - 1) { // Pas dans la vue, dans un bat ou caché
                $this->m_npc->set("id_Player\$target", self::getPC($cond, $charac, $ratio, $isBirdFly));
                if ($this->m_npc->get("id_Player\$target") != 0)
                    $index = self::getIndexPC($this->m_npc->get("id_Player\$target"));
                elseif ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                    self::moveRandom(30);
            } else {
                $dist = $this->m_infoPC[$index]["distanceBirdFly"];
				$distRealPA = $this->m_infoPC[$index]["distance"];
                if (($distRealPA >= $this->MAX_DISTANCE && !$isBirdFly) || ( $isBirdFly && $dist > 5)) {
                    $this->m_npc->set("id_Player\$target", self::getPC($cond, $charac, $ratio, $isBirdFly));
                    if ($this->m_npc->get("id_Player\$target") != 0)
                        $index = self::getIndexPC($this->m_npc->get("id_Player\$target"));
                    elseif ($this->m_npc->get("id_Player\$target") == 0 && $this->m_npc->get("ap") >= $this->m_npc->getTimeToMove($this->db))
                        self::moveRandom(30);
                }
            }
        }
        
        return $index;
    }

    public function getIndexPC($id)
    {
        for ($i = 0; $i < count($this->m_infoPC); $i ++) {
            if ($this->m_infoPC[$i]["id"] == $id)
                return $i;
        }
        return - 1;
    }

    public function getIndexNPC($id)
    {
        for ($i = 0; $i < count($this->m_infoNPC); $i ++) {
            if ($this->m_infoNPC[$i]["id"] == $id)
                return $i;
        }
        return - 1;
    }

    protected function initInfoPC()
    {
        $this->m_infoPC = array();
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $characLabel = array(
            "hp",
            "dexterity",
            "strength",
            "speed",
            "magicSkill",
            "attack",
            "defense",
            "damage",
            "armor",
            "timeAttack"
        );
        $xp = $this->m_npc->get("x");
        $yp = $this->m_npc->get("y");
        $map = $this->m_npc->get("map");
        $room = $this->m_npc->get("room");
        $hidden = floor($this->m_npc->get("hidden") / 10);
        
        $dbp = new DBCollection(
            "SELECT * FROM Player WHERE (status='PC' or id_BasicRace=263 or (id_BasicRace=" . ID_BASIC_RACE_FEU_FOL . " AND id_Member!=0)) AND (inbuilding=" .
                 $this->m_npc->get("inbuilding") . ") AND (hidden=" . $hidden . ") AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp .
                 "))/2 <= 5 and room = " . $room . " and map=" . $map, $this->db);
        
        while (! $dbp->eof()) {
            $opponent->load($dbp->get("id"), $this->db);
            $pc = array();
            $pc["id"] = $opponent->get("id");
            $pc["name"] = $opponent->get("name");
            $pc["level"] = $dbp->get("level");
            $pc["state"] = $dbp->get("state");
            $pc["hidden"] = $dbp->get("hidden");
            $pc["inbuilding"] = $dbp->get("inbuilding");
            $pc["distance"] = $this->m_graph->getRealDistance($opponent->get("x"), $opponent->get("y"));
            $pc["distanceBirdFly"] = distHexa($this->m_npc->get("x"), $this->m_npc->get("y"), $opponent->get("x"), $opponent->get("y"));
            foreach ($characLabel as $name)
                $pc[$name] = $opponent->getScore($name);
            
            $this->m_infoPC[] = $pc;
            $dbp->next();
        }
    }

    protected function initInfoNPC()
    {
        $this->m_infoNPC = array();
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $characLabel = array(
            "hp",
            "dexterity",
            "strength",
            "speed",
            "magicSkill",
            "attack",
            "defense",
            "damage",
            "armor",
            "timeAttack"
        );
        $xp = $this->m_npc->get("x");
        $yp = $this->m_npc->get("y");
        $dbp = new DBCollection("SELECT * FROM Player WHERE status='NPC' AND id_Member=0 AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 5", 
            $this->db);
        
        while (! $dbp->eof()) {
            $opponent->load($dbp->get("id"), $this->db);
            $pc = array();
            $pc["id"] = $opponent->get("id");
            $pc["id_BasicRace"] = $opponent->get("id_BasicRace");
            $pc["level"] = $dbp->get("level");
            $pc["currhp"] = $dbp->get("currhp");
            
            $pc["distance"] = $this->m_graph->getRealDistance($opponent->get("x"), $opponent->get("y"));
            $pc["distanceBirdFly"] = distHexa($this->m_npc->get("x"), $this->m_npc->get("y"), $opponent->get("x"), $opponent->get("y"));
            
            foreach ($characLabel as $name)
                $pc[$name] = $opponent->getScore($name);
            
            $this->m_infoNPC[] = $pc;
            $dbp->next();
        }
    }

    protected function chooseCloserPC($ratio = 1, $isBirdFly = false)
    {
        // en fonction de la distance de la cible et du ratio prend le perso
        // pour cible ou pas.
        $index = - 1;
        $dist = $this->MAX_DISTANCE;
        for ($i = 0; $i < count($this->m_infoPC); $i ++) {
            if (! $isBirdFly) {
                if ($this->m_infoPC[$i]["distance"] < $dist) {
                    $index = $i;
                    $dist = $this->m_infoPC[$i]["distance"];
                }
            } else {
                if ($this->m_infoPC[$i]["distanceBirdFly"] < $dist) {
                    $index = $i;
                    $dist = $this->m_infoPC[$i]["distanceBirdFly"];
                }
            }
        }
        
        if ($index != - 1) {
            $nb = mt_rand(1, 100);
            if ($nb > (($dist - 1) * 20) / $ratio)
                return $this->m_infoPC[$index]["id"];
            else
                return 0;
        } else
            return 0;
    }

    protected function getMaxPC($charac, $ratio = 1, $isBirdFly = false)
    {
        $index = - 1;
        $max = - 10000;
        for ($i = 0; $i < count($this->m_infoPC); $i ++) {
            $nb = 0;
            $dist = $this->m_infoPC[$i]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$i]["distance"];
               
            // Calcul du coef d'aggro
            if (($distRealPA < $this->MAX_DISTANCE && !$isBirdFly)|| ( $isBirdFly && $dist <= 5)) {
                // taux reduction de l'aggro en fonction du nombre de monstre
                // ciblant déjà ce perso
                $dbt = new DBCollection("SELECT * FROM Player WHERE id_Player\$target=" . $this->m_infoPC[$i]["id"], $this->db);
                $target_BM = 5 * $dbt->count() * ceil($this->m_infoPC[$i]["level"] / 10);
                
                // taux reduction de l'aggro en fonction de la distance de la
                // cible
                $distance_BM = 5 * ceil($this->m_infoPC[$i]["level"] / 10) * $dist;
                
                // aggro = jet de charac choisi - réduction
                $nb = $this->m_infoPC[$i][$charac] - $distance_BM - $target_BM;
                
                if ($nb > $max) {
                    $index = $i;
                    $max = $nb;
                }
            }
        }
        if ($index != - 1) {
            $nb = mt_rand(1, 100);
            $dist = $isBirdFly ? $this->m_infoPC[$index]["distanceBirdFly"] : $this->m_infoPC[$index]["distance"];
            if ($nb > (($dist - 1) * 20) / $ratio)
                return $this->m_infoPC[$index]["id"];
            else
                return 0;
        } else
            return 0;
    }

    protected function getMinPC($charac, $ratio = 1, $isBirdFly = false)
    {
        $index = - 1;
        $min = 10000;
        for ($i = 0; $i < count($this->m_infoPC); $i ++) {
            $nb = 10001;
            $dist = $this->m_infoPC[$i]["distanceBirdFly"];
			$distRealPA = $this->m_infoPC[$i]["distance"];
            if (($distRealPA < $this->MAX_DISTANCE && !$isBirdFly)|| ( $isBirdFly && $dist <= 5)) {
                $nb = $this->m_infoPC[$i][$charac] + 5 * ($isBirdFly ? $this->m_infoPC[$i]["distanceBirdFly"] : $this->m_infoPC[$i]["distance"]);
			}
            
            if ($nb < $min) {
                $index = $i;
                $min = $nb;
            }
        }
        
        if ($index != - 1) {
            $nb = mt_rand(1, 100);
            $dist = $isBirdFly ? $this->m_infoPC[$index]["distanceBirdFly"] : $this->m_infoPC[$index]["distance"];
            if ($nb > (($dist - 1) * 20) / $ratio)
                return $this->m_infoPC[$index]["id"];
            else
                return 0;
        } else
            return 0;
    }

    protected function getPC($cond, $charac = 0, $ratio = 1, $isBirdFly = false)
    {
        if ($cond == "closer")
            return self::chooseCloserPC($ratio, $isBirdFly);
        
        if ($cond == "min")
            return self::getMinPC($charac, $ratio, $isBirdFly);
        
        if ($cond == "max")
            return self::getMaxPC($charac, $ratio, $isBirdFly);
    }

    protected function getCloserRace($id_BasicRace, $gap)
    {
        $dist = $this->MAX_DISTANCE;
        for ($i = 0; $i < count($this->m_infoNPC); $i ++) {
            if ($this->m_infoNPC[$i]["distance"] < $dist && ($this->m_infoNPC[$i]["id_BasicRace"] == $id_BasicRace)) {
                $index = $i;
                $dist = $this->m_infoNPC[$i]["distance"];
            }
        }
        
        if ($dist < $gap + 1)
            return $this->m_infoNPC[$index]["id"];
        
        return 0;
    }

    protected function getCloserState($state, $gap, $isBirdFly)
    {
        $dist = $this->MAX_DISTANCE;
        for ($i = 0; $i < count($this->m_infoPC); $i ++) {
			$distCalc = $isBirdFly ? $this->m_infoPC[$i]["distanceBirdFly"] : $this->m_infoPC[$i]["distance"];
            if ($distCalc < $dist && $this->m_infoPC[$i]["state"] == $state) {
                $index = $i;
                $dist = $this->m_infoPC[$i]["distance"];
            }
        }
        
        if ($dist < $gap + 1)
            return $this->m_infoPC[$index]["id"];
        
        return 0;
    }

    protected function getCloserWounded($state, $gap, $self = 0)
    {
        $min = 25;
        $index = - 1;
        for ($i = 0; $i < count($this->m_infoNPC); $i ++) {
            if ($this->m_infoNPC[$i]["distanceBirdFly"] * (($this->m_infoNPC[$i]["currhp"] * 5) / $this->m_infoNPC[$i]["hp"]) < $min &&
                 ($self or $this->m_npc->get("id") != $this->m_infoNPC[$i]["id"])) {
                $index = $i;
                $min = $this->m_infoNPC[$i]["distanceBirdFly"] * (($this->m_infoNPC[$i]["currhp"] * 5) / $this->m_infoNPC[$i]["hp"]);
            }
        }
        
        if ($index != - 1 && $this->m_infoNPC[$index]["distanceBirdFly"] < $gap + 1 && (($this->m_infoNPC[$index]["currhp"] * 5) / $this->m_infoNPC[$index]["hp"]) < $state)
            return $index;
        
        return - 1;
    }

    protected function getNBWounded()
    {
        $nb = 0;
        for ($i = 0; $i < count($this->m_infoNPC); $i ++) {
            if ($this->m_infoNPC[$i]["currhp"] != $this->m_infoNPC[$i]["hp"])
                $nb ++;
        }
        
        return $nb;
    }

    protected function getNbOpponentClose()
    {
        $nb = 0;
        for ($i = 0; $i < count($this->m_infoPC); $i ++) {
            if ($this->m_infoPC[$i]["distanceBirdFly"] == 1)
                $nb ++;
        }
        return $nb;
    }

    /*
     * ********************************************************* FONCTION DE DEPLACEMENT ******************************************************
     */
    protected function moveToCase($x, $y, $delay)
    {
        $stackResult = $this->m_graph->getShortestPath($x, $y);
        if (! $stackResult->isEmpty()) {
            $stackResult->setIteratorMode(SplDoublyLinkedList::IT_MODE_LIFO | SplDoublyLinkedList::IT_MODE_KEEP);
            $stackResult->rewind();
            $case = $stackResult->current();
            if ($this->m_graph->getGrille($case, "cost_PA") <= $this->m_npc->get("ap")) {
                if ($this->m_graph->getGrille($case, "EL_type") == "Building_15") {
                    // Boire bassin divin
                    self::spingDrinkElement($this->m_graph->getGrille($case, "EL_id")); // ca fonctionne
                } else {
                    if ($this->m_graph->getGrille($case, "EL_type") == "Building_27") {
                        // attack mur de terre
                        self::attackBuilding($this->m_graph->getGrille($case, "EL_id"));
                    } else {
                        if ($this->m_graph->getGrille($case, "EL_type") == "Exploitation") {
                            // detruire exploitation
                            self::destroy($this->m_graph->getGrille($case, "EL_id"));
                        } else {
                            $this->m_npc->set("x", $this->m_graph->getGrille($case, "x"));
                            $this->m_npc->set("y", $this->m_graph->getGrille($case, "y"));
                            
                            // Détermination du nombre de PA utilisé pour un déplacement
                            $ap = $this->m_graph->getGrille($case, "cost_PA");
                            
                            $this->m_npc->set("ap", $this->m_npc->get("ap") - $ap);
                            
							$this->m_npc->reloadPartialAttribute($this->m_npc->get("id"), "currhp", $db);
							
                            // Log de l'action
                            $param["ERROR"] = 0;
                            BasicActionFactory::globalInfo($this->m_npc, $param);
                            $param["TYPE_ATTACK"] = MOVE_EVENT;
                            Event::logAction($this->db, MOVE, $param);
                            $this->m_npc->updateDB($this->db);
                        }
                    }
                }
            }
            // Calcul du délai d'action
            $time = time();
            $time += mt_rand($delay, 2 * $delay) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            $this->m_npc->updateHidden($this->db);
            $this->m_npc->updateDB($this->db);
        }
        // }
    }

    protected function attackBuilding($idBuilding)
    {
        $param["XP"] = 0;
        
        require_once (HOMEPATH . "/factory/InteractionChecking.inc.php");
        
        $damage = $this->m_npc->getScore("damage");
        $mm = $this->m_npc->getScore("magicSkill");
        $dex = $this->m_npc->getScore("dexterity");
        $magicalDamage = 1.5 * ceil(sqrt($dex * $mm) / 4);
        // 1 -> Attaque normale
        // 2 -> Attaque magique
        $attackType = $magicalDamage >= $damage ? 2 : 1;
        
        $error = InteractionChecking::checkingAttackBuilding($this->m_npc, $idBuilding, $attackType, $param, $this->db);
        $param["ERROR"] = $error;
        if ($error == 0) {
            $action = $param["TYPE_ACTION"];
            Event::logAction($this->db, $action, $param);
        }
        return $error;
    }

    protected function destroy($idRessource)
    {
        $param["XP"] = 0;
        
        $error = BasicActionFactory::destroy($this->m_npc, $idRessource, $param, $this->db);
        $param["ERROR"] = $error;
        if ($error == 0) {
            $action = $param["TYPE_ACTION"];
            Event::logAction($this->db, $action, $param);
        }
        return $error;
    }

    protected function jumpToCase($x, $y, $delay)
    {
        $this->m_graph->initGraph();
        $chemin = $this->m_graph->getVoisin($this->m_graph->getCase($x, $y));
        if (count($chemin) != 0) {
            $this->m_npc->set("x", $this->m_graph->getGrille($chemin[0], "x"));
            $this->m_npc->set("y", $this->m_graph->getGrille($chemin[0], "y"));
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 2);
            $time = time();
            $time += mt_rand($delay, 2 * $delay) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            $param["ERROR"] = 0;
            BasicActionFactory::globalInfo($this->m_npc, $param);
            $param["TYPE_ATTACK"] = M_JUMP_EVENT;
            Event::logAction($this->db, M_JUMP, $param);
            $this->m_npc->updateHidden($this->db);
            $this->m_npc->updateDB($this->db);
        }
    }

    protected function moveAwayFromTarget($x, $y, $delay)
    {
        $dist = $this->m_graph->getRealDistance($x, $y);
        // distReel(46, $this->m_graph->getCase($x, $y));
        $voisin = array();
        $voisin = $this->m_graph->getVoisin($this->m_graph->getCase($this->m_npc->get("x"), $this->m_npc->get("y")));
        for ($i = 0; $i < count($voisin); $i ++) {
            if (distHexa($this->m_graph->getGrille($voisin[$i], "x"), $this->m_graph->getGrille($voisin[$i], "y"), $x, $y) > $dist) {
                // $this->m_graph->distReel($voisin[$i], $this->m_graph->getCase($x, $y)) > $dist) {
                $this->m_npc->set("x", $this->m_graph->getGrille($voisin[$i], "x"));
                $this->m_npc->set("y", $this->m_graph->getGrille($voisin[$i], "y"));
                $this->m_npc->set("ap", $this->m_npc->get("ap") - 1);
                $time = time();
                $time += mt_rand($delay, 2 * $delay) * 60;
                $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
                $param["ERROR"] = 0;
                BasicActionFactory::globalInfo($this->m_npc, $param);
                $param["TYPE_ATTACK"] = MOVE_EVENT;
                Event::logAction($this->db, MOVE, $param);
                $i = 7;
            }
        }
        $this->m_npc->updateHidden($this->db);
        $this->m_npc->updateDB($this->db);
    }

    protected function moveRandom($delay)
    {
        $this->m_graph->initGraph();
        $voisin = $this->m_graph->getVoisin($this->m_graph->getCase($this->m_npc->get("x"), $this->m_npc->get("y")));
        $nb = count($voisin);
        $dist = distHexa($this->m_npc->get("x"), $this->m_npc->get("y"), $this->m_npc->get("resurrectx"), $this->m_npc->get("resurrecty"));
        
        if ($nb != 0) {
            $vector = new VectorProba();
            for ($i = 0; $i < count($voisin); $i ++) {
                if (distHexa($this->m_npc->get("resurrectx"), $this->m_npc->get("resurrecty"), $this->m_graph->getGrille($voisin[$i], "x"), 
                    $this->m_graph->getGrille($voisin[$i], "y")) < $dist)
                    $vector->pushBack($i, 1.3 * $dist);
                else
                    $vector->pushBack($i, $dist);
            }
            
            $vector->setInterval(PREC);
            $index = $vector->searchUpperOrEqual(rand(1, PREC));
            if ($index >= 0) {
                $ret = $vector->getKey($index);
            }
            
            $case = $voisin[$ret];
            
            $this->m_npc->set("x", $this->m_graph->getGrille($case, "x"));
            $this->m_npc->set("y", $this->m_graph->getGrille($case, "y"));
            
            // Détermination du nombre de PA utilisé pour un déplacement
            $ap = $this->m_npc->getTimeToMove($this->db);
            $state = $this->m_npc->get("state");
            if ($state == "flying")
                $ap = 1;
            $this->m_npc->set("ap", $this->m_npc->get("ap") - $this->m_npc->getTimeToMove($this->db));
            
            $time = time();
            $time += mt_rand($delay, 2 * $delay) * 300;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            
            $param["ERROR"] = 0;
            BasicActionFactory::globalInfo($this->m_npc, $param);
            $param["TYPE_ATTACK"] = MOVE_EVENT;
            Event::logAction($this->db, MOVE, $param);
            $this->m_npc->updateHidden($this->db);
            $this->m_npc->updateDB($this->db);
        }
    }

    protected function moveTeleportClose($x, $y, $delay)
    {
        $this->m_graph->initGraph();
        $voisin = $this->m_graph->getVoisin($this->m_graph->getCase($x, $y));
        $this->m_npc->set("x", $this->m_graph->getGrille($voisin[0], "x"));
        $this->m_npc->set("y", $this->m_graph->getGrille($voisin[0], "y"));
        $this->m_npc->set("ap", $this->m_npc->get("ap") - 2);
        
        $time = time();
        $time += mt_rand($delay, 2 * $delay) * 60;
        $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
        
        $param["ERROR"] = 0;
        BasicActionFactory::globalInfo($this->m_npc, $param);
        $param["TYPE_ATTACK"] = M_TELEPORT_EVENT;
        Event::logAction($this->db, M_TELEPORT, $param);
        $this->m_npc->updateHidden($this->db);
        $this->m_npc->updateDB($this->db);
    }

    protected function moveTeleportAway($x, $y, $delay)
    {
        $this->m_graph->initGraph();
        $choix1 = $this->m_graph->getVoisin($this->m_graph->getCase($this->m_npc->get("x"), $this->m_npc->get("y")), 2);
        $choix2 = $this->m_graph->getVoisin($this->m_graph->getCase($x, $y), 3);
        if ((count(array_intersect($choix1, $choix2))) != 0) {
            $result = array_intersect($choix1, $choix2);
            $this->m_npc->set("x", $this->m_graph->getGrille(current($result), "x"));
            $this->m_npc->set("y", $this->m_graph->getGrille(current($result), "y"));
            $this->m_npc->set("ap", $this->m_npc->get("ap") - 2);
            
            $time = time();
            $time += mt_rand($delay, 2 * $delay) * 60;
            $this->m_npc->set("playatb", gmdate("Y-m-d H:i:s", $time));
            
            $param["ERROR"] = 0;
            BasicActionFactory::globalInfo($this->m_npc, $param);
            $param["TYPE_ATTACK"] = M_TELEPORT_EVENT;
            Event::logAction($this->db, M_TELEPORT, $param);
            $this->m_npc->updateHidden($this->db);
            $this->m_npc->updateDB($this->db);
        }
    }

    protected function teleportAway($player, $x, $y, $delay)
    {
        $player->set("x", $x);
        $player->set("y", $y);
    }

    protected function backToMyWorld()
    {
        BasicActionFactory::globalInfo($this->m_npc, $param);
        $param["TYPE_ATTACK"] = M_BACKWORLD_EVENT;
        Event::logAction($this->db, M_BACKWORLD, $param);
        $this->m_npc->deleteDB($this->db);
    }

    /*
     * ********************************************************* FONCTION
     * D'ATTAQUE ******************************************************
     */
    protected function attack($opponent, $modifierA = null, $modifierB = null, $ident = 0)
    {
        $param["XP"] = 0;
        $error = BasicActionFactory::attack($this->m_npc, $opponent, $param, $this->db, $modifierA, $modifierB, $ident);
        $param["ERROR"] = 0;
        if ($ident != 0 && $ident != M_POISON)
            $action = $ident;
        else
            $action = $param["TYPE_ACTION"];
        
        $param["IDENT"] = $ident;
        if ($param["PLAYER_KILLED"] == 0)
            Event::logAction($this->db, $action, $param);
        return $param["TARGET_KILLED"];
    }

    protected function projectionIfProtected($player)
    {
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id") . " AND name='défendu'", $this->db);
        if (! $dbc->eof()) {
            self::attack($player, null, null, ABILITY_PROJECTION);
            return true;
        }
        return false;
    }

    protected function bier()
    {
        $param["XP"] = 0;
        $error = PNJFactory::bier($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function lightning($opponent)
    {
        $param["XP"] = 0;
        $error = PNJFactory::lightning($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function poison($opponent)
    {
        $param["XP"] = 0;
        $error = PNJFactory::spit($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function stun($opponent, $ident = 1000)
    {
        $param["XP"] = 0;
        $error = BasicActionFactory::attack($this->m_npc, $opponent, $param, $this->db, null, null, $ident);
        $param["ERROR"] = 0;
        $param["IDENT"] = $ident;
        Event::logAction($this->db, ABILITY_STUNNED, $param);
    }

    protected function archery($opponent, $modifierA = null, $modifierB = null, $ident = 0)
    {
        $param["XP"] = 0;
        $equip = new Equipment();
        $equip->set("name", "Flèche barbelée");
        $equip->set("level", mt_rand(2, 5));
        $equip->set("id_BasicEquipment", 56);
        $equip->set("id_EquipmentType", 28);
        $equip->set("id_Player", $this->m_npc->get("id"));
        $equip->addDB($this->db);
        
        $error = BasicActionFactory::archery($this->m_npc, $opponent, $equip->get("id"), $param, $this->db);
        $param["ERROR"] = 0;
        if ($ident != 0)
            $action = $ident;
        else
            $action = $param["TYPE_ACTION"];
        
        $param["IDENT"] = $ident;
        Event::logAction($this->db, $action, $param);
        return $param["TARGET_KILLED"];
    }

    protected function twirl()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::twirl($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        // $param["IDENT"] = $ident;
        Event::logAction($this->db, ABILITY_TWIRL, $param);
    }

    protected function sharpen()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::sharpen($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        // $param["IDENT"] = $ident;
        Event::logAction($this->db, ABILITY_SHARPEN, $param);
    }

    protected function bdfMissed()
    {
        $param["XP"] = 0;
        $hp = max(1, $this->m_npc->get("currhp") - 8);
        $this->m_npc->set("currhp", $hp);
        $param["ERROR"] = 0;
        BasicActionFactory::globalInfo($this->m_npc, $param);
        $param["TYPE_ATTACK"] = M_KOBOLD_CRAP_EVENT;
        Event::logAction($this->db, M_KOBOLD_CRAP, $param);
    }

    protected function fireball($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::fireball($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
        return $param["TARGET_KILLED"];
    }

    protected function brasier()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $error = PNJFactory::brasier($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
        return $param["TARGET_KILLED"];
    }

    protected function blood($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::blood($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
        return $param["TARGET_KILLED"];
    }

    protected function instantBlood($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::instantBlood($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
        return $param["TARGET_KILLED"];
    }

    protected function barrier($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::barrier($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function tears($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::tears($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function shield($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::shield($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function regen($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::regen($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function sun()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::sun($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function neant()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $error = PNJFactory::neant($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function pluie()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $error = PNJFactory::pluie($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function anger($opponent, $charac, $delai = 0)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::anger($this->m_npc, $opponent, $charac, $param, $this->db);
        $param["ERROR"] = 0;
        if ($delai)
            $param["DELAI"] = $delai;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function autoRegen()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::autoregen($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function springDrink()
    {
        $curstate = min(floor($this->m_npc->get("currhp") * 5 / $this->m_npc->get("hp")), 4);
        if ($this->m_npc->get("ap") >= 2 && $curstate <= 3) {
            $dbbuilding = new DBCollection(
                "SELECT * FROM Building WHERE id_BasicBuilding = 15 and (abs(x-" . $this->m_npc->get("x") . ") + abs(y-" . $this->m_npc->get("y") . ") + abs(x+y-" .
                     $this->m_npc->get("x") . "-" . $this->m_npc->get("y") . "))/2 <= 1 order by level desc", $this->db);
            if (! $dbbuilding->eof()) {
                self::spingDrinkElement($dbbuilding->get("id"));
            }
        }
    }

    private function spingDrinkElement($idDrink)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        $error = BasicActionFactory::drinkSpring($this->m_npc, $idDrink, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function curse($opponent, $charac, $modifier = null, $delai = 0)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::curse($this->m_npc, $opponent, $charac, $param, $this->db, $modifier);
        $param["ERROR"] = 0;
        if ($delai)
            $param["DELAI"] = $delai;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function armor($opponent, $delai = 0)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::armor($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        if ($delai)
            $param["DELAI"] = $delai;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function guard()
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::guard($this->m_npc, $param, $this->db);
        $param["ERROR"] = 0;
        $param["DELAI"] = 2;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function wall($case)
    {
        $param["XP"] = 0;
        $place = array();
        for ($i = 0; $i < 6; $i ++) {
            $place["x"][$i] = $this->m_graph->getGrille($case[$i], "x");
            $place["y"][$i] = $this->m_graph->getGrille($case[$i], "y");
        }
        
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $error = SpellFactory::wall($this->m_npc, $place, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function firstAid($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::firstAid($this->m_npc, $opponent, $param, $this->db, $this->m_npc->get("level"));
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = M_HEAL;
        $param["TYPE_ATTACK"] = M_HEAL_EVENT;
        Event::logAction($this->db, M_HEAL, $param);
    }

    protected function bolas($opponent)
    {
        $param["XP"] = 0;
        $bolas = new Equipment();
        $bolas->set("name", "Bolas");
        $bolas->set("id_Player", $this->m_npc->get("id"));
        $bolas->set("weared", "YES");
        $bolas->set("id_EquipmentType", 1);
        $bolas->set("id_BasicEquipment", 205);
        $bolas->set("level", 1);
        $bolas->set("durability", mt_rand(1, 3));
        $bolas->addDBr($this->db);
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::bolas($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function immobilise($opponent)
    {
        $param["XP"] = 0;
        $bolas = new Equipment();
        $bolas->set("name", "Branche d'Etranglesaule");
        $bolas->set("id_Player", $this->m_npc->get("id"));
        $bolas->set("weared", "YES");
        $bolas->set("id_EquipmentType", 1);
        $bolas->set("id_BasicEquipment", 205);
        $bolas->set("level", 1);
        $bolas->set("durability", mt_rand(10, 50));
        $bolas->addDBr($this->db);
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = AbilityFactory::bolas($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        if ($param["TYPE_ATTACK"] == ABILITY_BOLAS_EVENT_SUCCESS)
            $param["TYPE_ATTACK"] = M_BRANCH_EVENT_SUCCESS;
        else
            $param["TYPE_ATTACK"] = M_BRANCH_EVENT_FAIL;
        
        $param["TYPE_ACTION"] = M_BRANCH;
        Event::logAction($this->db, M_BRANCH, $param);
    }

    protected function arrest($opponent)
    {
        if ($this->m_npc->get("id") < 1500) {
            $this->m_npc->set("x", 464);
            $this->m_npc->set("y", 375);
            $this->m_npc->set("inbuilding", 1076);
        }
        if ($this->m_npc->get("id") > 1500 && $this->m_npc->get("id") < 2500) {
            $this->m_npc->set("x", 256);
            $this->m_npc->set("y", 301);
            $this->m_npc->set("inbuilding", 1252);
        }
        if ($this->m_npc->get("id") > 2500) {
            $this->m_npc->set("x", 564);
            $this->m_npc->set("y", 344);
            $this->m_npc->set("inbuilding", 1570);
        }
        
        $this->m_npc->set("moneyBank", 0);
        $this->m_npc->set("nbkill", $this->m_npc->get("nbkill") + 1);
        $this->m_npc->set("nbkillpc", $this->m_npc->get("nbkillpc") + 1);
        $this->m_npc->set("prison", 0);
        
        $this->m_npc->set("room", 19);
        $this->m_npc->set("id_Player\$target", 0);
        
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $error = BasicActionFactory::arrest($opponent, 1, $param, $this->db, 1);
        $param["ERROR"] = 0;
        $param["TYPE_ATTACK"] = ARREST_EVENT_FORCED;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    protected function morph($opponent)
    {
        $param["XP"] = 0;
        BasicActionFactory::globalInfo($this->m_npc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $modifier = new Modifier();
        $characLabel = array(
            "hp",
            "dexterity",
            "strength",
            "speed",
            "magicSkill",
            "attack",
            "defense",
            "damage",
            "timeAttack"
        );
        foreach ($characLabel as $name) {
            $modifier->addModif($name, DICE_D6, $opponent->getObj("Modifier")
                ->getAtt($name, DICE_D6));
            $modifier->addModif($name, DICE_ADD, $opponent->getObj("Modifier")
                ->getAtt($name, DICE_ADD));
            $modifier->addModif($name . "_bm", DICE_D6, $opponent->getObj("Modifier")
                ->getAtt($name . "_bm", DICE_D6));
            $modifier->addModif($name . "_bm", DICE_ADD, $opponent->getObj("Modifier")
                ->getAtt($name . "_bm", DICE_ADD));
        }
        
        $modifier->addModif("armor", DICE_D6, 
            $opponent->getObj("Modifier")
                ->getAtt("armor", DICE_D6) + $opponent->getObj("Modifier")
                ->getAtt("armor" . "_bm", DICE_D6));
        $modifier->addModif("armor", DICE_ADD, 
            $opponent->getObj("Modifier")
                ->getAtt("armor", DICE_ADD) + $opponent->getObj("Modifier")
                ->getAtt("armor" . "_bm", DICE_ADD));
        $modifier->addModif("armor_bm", DICE_D6, 0);
        $modifier->addModif("armor_bm", DICE_ADD, 0);
        
        $this->m_npc->set("currhp", $opponent->get("currhp"));
        $this->m_npc->set("hp", $opponent->get("hp"));
        $id = $modifier->addDBr($this->db);
        $this->m_npc->set("id_Modifier", $id);
        if ($opponent->get("status") == 'PC')
            $this->m_npc->set("racename", $opponent->get("name"));
        else
            $this->m_npc->set("racename", $opponent->get("racename"));
        
        $this->m_npc->set("id_Player\$target", 0);
        $this->m_npc->set("id_BasicRace", $opponent->get("id_BasicRace"));
        $this->m_npc->set("level", $opponent->get("level"));
        $this->m_npc->set("gender", $opponent->get("gender"));
        if ($opponent->getScore("damage") >= $opponent->getScore("magicSkill")) {
            $this->m_npc->set("behaviour", 34);
            $this->m_npc->set("fightStyle", 1);
        } else {
            $this->m_npc->set("behaviour", 35);
            $this->m_npc->set("fightStyle", 4);
        }
        
        if ($opponent->get("picture") != "") {
            $this->m_npc->set("picture", $opponent->get("picture"));
        }
        
        PlayerFactory::initBM($this->m_npc, $this->m_npc->getObj("Modifier"), $param, $this->db);
        $param["ERROR"] = 0;
        $param["TYPE_ATTACK"] = M_MORPH_EVENT;
        $param["TYPE_ACTION"] = M_MORPH;
        
        Event::logAction($this->db, M_MORPH, $param);
        return $param["TARGET_KILLED"];
    }

    protected function infest($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $ret = PNJFactory::infest($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
        return $ret;
    }

    protected function drain($opponent)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $ret = PNJFactory::drain($this->m_npc, $opponent, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
        return $ret;
    }

    protected function invoke($id_BasicRace)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
        $place = BasicActionFactory::getAleaFreePlace($this->m_npc->get("x"), $this->m_npc->get("y"), $this->m_npc->get("map"), 1, $this->db);
        if ($place != 0) {
            $error = SpellFactory::FireCall($this->m_npc, $place["x"], $place["y"], 0, $param, $this->db, $id_BasicRace);
            $param["ERROR"] = 0;
            $param["AP"] = 8;
            Event::logAction($this->db, $param["TYPE_ACTION"], $param);
            $dbc = new DBCollection("DELETE FROM BM WHERE id_Player=" . $this->m_npc->get("id") . " AND name='Golem de Feu'", $this->db);
        }
    }

    protected function aether($opponent, $x, $y)
    {
        $param["XP"] = 0;
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $error = PNJFactory::Grapnel($this->m_npc, $opponent, $x, $y, $param, $this->db);
        $param["ERROR"] = 0;
        Event::logAction($this->db, $param["TYPE_ACTION"], $param);
    }

    /*
     * ************************************************* COMPORTEMENT DES
     * MONSTRES SUITE A UNE ATTAQUE *************************************
     */
    public function getNBGroup($band)
    {
        $dbe = new DBCollection("SELECT id FROM Player WHERE id_Team=" . $band, $this->db);
        return $dbe->count();
    }

    protected function getMaxWounded($band)
    {
        $dbe = new DBCollection("SELECT * FROM Player WHERE id_Team=" . $band . " AND currhp!=hp", $this->db);
        $target = 0;
        $max = 0;
        while (! $dbe->eof()) {
            if ($max <= ($dbe->get("hp") - $dbe->get("currhp"))) {
                $max = $dbe->get("hp") - $dbe->get("currhp");
                $target = $dbe->get("id");
            }
            $dbe->next();
        }
        return $target;
    }

    protected function chooseNPCwithTarget($band)
    {
        $dbe = new DBCollection("SELECT * FROM Player WHERE id_Team=" . $band . " AND id_Player\$target!=0", $this->db);
        $target = 0;
        $dist = 100;
        while (! $dbe->eof()) {
            $target = $dbe->get("id");
            $dbe->next();
        }
        return $target;
    }
}
?>



  

  
