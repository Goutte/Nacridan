<?php

class oo_text extends oo_element
{

    public $m_maxlength;

    public $m_minlength;

    public $m_length_e;

    public $m_valid_regex;

    public $m_valid_icase;

    public $m_valid_e;

    public $m_pass;

    public $m_size;

    public $m_check;

    public $m_icase;
    
    // Constructor
    function oo_text($a)
    {
        $this->setup_element($a);
        if (isset($a["pass"]) && $a["pass"] == 1) {
            $this->m_pass = 1;
        }
    }

    function self_get($val)
    {
        $str = "";
        
        $v = htmlspecialchars($this->m_value);
        $n = $this->m_name;
        
        $str .= "<input name='$n' value=\"$v\"";
        $str .= ($this->m_pass) ? " type='password'" : " type='text'";
        
        if ($this->m_errorclass && $this->m_error == true) {
            $str .= " class='$this->m_errorclass'";
        }
        
        if ($this->m_maxlength) {
            $str .= " maxlength='$this->m_maxlength'";
        }
        if ($this->m_size) {
            $str .= " size='$this->m_size'";
        }
        if ($this->m_extrahtml) {
            $str .= " $this->m_extrahtml";
        }
        if ($this->m_check) {
            preg_match("/(.+)check$/", $n, $nc);
            $str .= " check='{$nc[1]}'";
        }
        $str .= "/>";
        
        return $str;
    }

    function self_validate($val)
    {
        if (! is_array($val)) {
            $val = array(
                $val
            );
        }
        
        foreach ($val as $k => $v) {
            if ($this->m_length_e && (strlen($v) < $this->m_minlength)) {
                return $this->m_length_e;
            }
            
            if ($this->m_valid_e && (($this->m_icase && ! preg_match('/' . $this->m_valid_regex . '/i', $v)) || (! $this->m_icase && ! preg_match('/' . $this->m_valid_regex . '/i', $v)))) {
                return $this->m_valid_e;
            }
        }
        return false;
    }
} // end TEXT

?>
