<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");
require_once (HOMEPATH . "/class/BM.inc.php");
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");

class SpellFactory
{

    /* *************************************************************** FONCTIONS AUXILLIAIRES **************************************** */
    static private function globalInfo($playerSrc, &$param)
    {
        $param["PLAYER_NAME"] = $playerSrc->get("name");
        if ($param["PLAYER_NAME"] == "")
            $param["PLAYER_NAME"] = $playerSrc->get("racename");
        $param["PLAYER_RACE"] = $playerSrc->get("id_BasicRace");
        $param["PLAYER_ID"] = $playerSrc->get("id");
        $param["PLAYER_GENDER"] = $playerSrc->get("gender");
        
        $param["PLAYER_LEVEL"] = $playerSrc->get("level");
        $param["MEMBER_ID"] = $playerSrc->get("id_Member");
        
        if ($playerSrc->get("status") == "NPC")
            $param["PLAYER_IS_NPC"] = 1;
        else
            $param["PLAYER_IS_NPC"] = 0;
    }

    static private function globalInfoOpponent($playerSrc, &$param)
    {
        $param["TARGET_NAME"] = $playerSrc->get("name");
        if ($param["TARGET_NAME"] == "")
            $param["TARGET_NAME"] = $playerSrc->get("racename");
        $param["TARGET_RACE"] = $playerSrc->get("id_BasicRace");
        $param["TARGET_ID"] = $playerSrc->get("id");
        $param["TARGET_GENDER"] = $playerSrc->get("gender");
        
        $param["TARGET_LEVEL"] = $playerSrc->get("level");
        $param["TARGET_ID"] = $playerSrc->get("id");
        $param["TARGET_MEMBER"] = $playerSrc->get("id_Member");
        
        $param["TARGET_X"] = $playerSrc->get("x");
        $param["TARGET_Y"] = $playerSrc->get("y");
        $param["TARGET_XP"] = $playerSrc->get("xp");
        
        $param["TARGET_KILLED"] = 0;
        
        if ($playerSrc->get("status") == "NPC")
            $param["TARGET_IS_NPC"] = 1;
        else
            $param["TARGET_IS_NPC"] = 0;
    }

    static protected function modifierScoreToArray($eqmodifier, &$param)
    {
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getScoreWithNoBM($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function modifierToArray($eqmodifier, &$param)
    {
        $eqmodifier->initCharacStr();
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getModifStr($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function getScore($baseap, $success, &$param, $malus = 0)
    {
        $param["ENHANCE"] = 0;
        $param["PERCENT"] = $success - $malus;
        $param["SCORE"] = mt_rand(1, 100);
        $param["SUCCESS"] = 0;
        $param["SCOREB"] = 0;
        
        if ($param["SCORE"] <= $success - $malus) {
            $param["SUCCESS"] = 1;
            $param["SCOREB"] = mt_rand(1, 100);
            if ($param["SCOREB"] > $success)
                $param["ENHANCE"] = 1;
        }
        
        if ($baseap > 2 && ! $param["SUCCESS"])
            $baseap = min($baseap, 3);
        
        $param["AP"] = $baseap;
    }

    static function magicSpell($playerSrc, $ident, $malus, &$param, &$param2, $db)
    {
        BasicActionFactory::globalInfo($playerSrc, $param2);
        $dbc = new DBCollection(
            "SELECT MagicSpell.id,MagicSpell.skill, BasicMagicSpell.level, BasicMagicSpell.PA FROM MagicSpell LEFT JOIN BasicMagicSpell ON MagicSpell.id_BasicMagicSpell=BasicMagicSpell.id WHERE BasicMagicSpell.id=" .
                 ($ident - 200) . " AND id_Player=" . $playerSrc->get("id"), $db);
        if (! $dbc->eof()) {
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
            if (! $dbbm->eof()) {
                $malus += - $dbbm->get("level");
                $param["MALUS"] = - $malus;
            }
            $magicspellap = $dbc->get("PA");
            $success = $dbc->get("skill");
            self::getScore($magicspellap, $success, $param, $malus);
            
            $param["NO_ENHANCE"] = 0;
            if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && $playerSrc->get("id_Member") > 0) {
                $param["ENHANCE"] = 0;
                $param["NO_ENHANCE"] = 1;
            }
            
            if ($playerSrc->get("modeAnimation") == 1) {
                $param["ENHANCE"] = 0;
            }
            
            if ($success < 90 && $param["ENHANCE"] > 0) {
                $newsucess = min($success + $param["ENHANCE"], 90);
                $dbu = new DBCollection("UPDATE MagicSpell SET skill=" . $newsucess . " WHERE id=" . $dbc->get("id") . " AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            } else {
                if ($success >= 90)
                    $param["SCOREB"] = 0;
            }
            if (! $param["SUCCESS"] && $success < 90 && $param["NO_ENHANCE"] == 0) {
                $date = gmdate('Y-m-d H:i:s');
                $bonus = floor((100 - $param["PERCENT"]) / ($dbc->get("level") + 2));
                $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
                if (! $dbbm->eof())
                    $dbbm = new DBCollection("UPDATE BM SET level=level+" . $bonus . " WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db, 0, 0, false);
                else
                    $dbi = new DBCollection(
                        "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $playerSrc->get("id") . "," .
                             $playerSrc->get("id") . "," . $bonus . "," . $ident . ", 'POSITIVE', 'Concentration' ,0,'" . $date . "')", $db, 0, 0, false);
            } else
                $dbu = new DBCollection("DELETE FROM BM WHERE name='Concentration' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            
            return ACTION_NO_ERROR;
        }
        return MAGICSPELL_ERROR;
    }

    /* *********************************************************** LES SORTILEGES DE BASES *********************************************** */
    static function spell($playerSrc, $ident, $opponent, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        switch ($ident) {
            case SPELL_BURNING:
                self::burning($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_TEARS:
                self::tears($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_ARMOR:
                self::armor($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_CURE:
                self::cure($playerSrc, $opponent, $param, $db);
                break;
            case M_INSTANTBLOOD:
                self::instantBlood($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_REGEN:
                self::regen($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_FIREBALL:
                self::fireball($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_BLOOD:
                self::blood($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_NEGATION:
                self::negation($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_SHIELD:
                self::shield($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_BLESS:
                self::bless($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_BARRIER:
                self::barrier($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_BUBBLE:
                self::bubble($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_LIGHTTOUCH:
                self::lightTouch($playerSrc, $opponent, $param, $db);
                break;
            case SPELL_RECALL:
                self::recall($playerSrc, $opponent, $param, $db);
                break;
            
            default:
                break;
        }
    }

    static function AngerCurse($playerSrc, $ident, $opponent, $charac, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        switch ($ident) {
            case SPELL_CURSE:
                self::curse($playerSrc, $opponent, $charac, $param, $db);
                break;
            case SPELL_ANGER:
                self::anger($playerSrc, $opponent, $charac, $param, $db);
                break;
            
            default:
                break;
        }
    }

    static function spellzone($playerSrc, $ident, $x, $y, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        switch ($ident) {
            case SPELL_FIRE:
                self::fire($playerSrc, $x, $y, $param, $db);
                break;
            case SPELL_RAIN:
                self::rain($playerSrc, $x, $y, $param, $db);
                break;
            default:
                break;
        }
    }

    static function interBurning($playerSrc, $protecteur, &$param, &$db, $modifierA = null, $modifierB = null, $opponent)
    {
        BasicActionFactory::autoHustleInterposition($protecteur, $opponent, $db);
        
        self::globalInfo($playerSrc, $param);
        
        self::globalInfoOpponent($protecteur, $param);
        $param["NAME"] = $opponent->get("name");
        $param["INTER_ID"] = $opponent->get("id");
        $param["TYPE_ACTION"] = SPELL_BURNING;
        $param["PROTECTION"] = 1;
        $date = gmdate("Y-m-d H:i:s");
        
        SpellFactory::passiveBarrier($playerSrc, $protecteur, $param, $db);
        if ($param["PLAYER_KILLED"])
            $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_FAIL;
        else {
            
            // Jets de dés
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 or $playerSrc->get("status") == 'NPC') {
                $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
                if ($playerSrc->get("status") == 'PC')
                    PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            } else
                $param["PLAYER_ATTACK"] = $playerSrc->getScore("dexterity");
                
                // $param["TARGET_DEFENSE"]=$opponentModif->getScore("defense");
            
            $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
            $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
            $modifier = new Modifier();
            $modifier->addModif("damage", DICE_D6, $param["SPELL_LEVEL"]);
            $param["TARGET_DAMAGE"] = floor($modifier->getScoreWithNoBM("damage") * 1.7);
            $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGE"];
            
            // Si l'adversaire possède une bulle de vie
            SpellFactory::PassiveBubble($protecteur, $param, $db);
            $param["TARGET_MM"] = $protecteur->getScore("magicSkill");
            $param["ARMOR_BONUS"] = 10 + 2 * floor(1 + $param["TARGET_MM"] / 8);
            $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - ($protecteur->getScore("armor") + floor($protecteur->getScore("armor") * $param["ARMOR_BONUS"] / 100)), 0);
            $hp = $protecteur->get("currhp") - $param["TARGET_HP"];
            $param["XP"] += 1;
            if ($param["TARGET_HP"] > 0)
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                // Mort de la cible
            if ($hp <= 0) {
                BasicActionFactory::kill($playerSrc, $protecteur, $param, $db);
                $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_DEATH;
            } else {
                $param["TYPE_ATTACK"] = SPELL_INTER_EVENT_SUCCESS;
                PlayerFactory::loseHP($protecteur, $playerSrc, $param["TARGET_HP"], $param, $db);
                $opponent->updateDBr($db);
                // $param["TYPE_ATTACK"]= MAGICAL_ATTACK_EVENT_SUCCESS;
            }
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
            }
        }
        $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    static function burning($playerSrc, $opponent, &$param, &$db, $modifierA = null, $modifierB = null)
    {
        
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_BURNING;
        
        // Si l'opposant est sous protection
        $param["PROTECTION"] = 0;
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='défendu'", $db);
        if (! $dbc->eof()) {
            $protecteur = new Player();
            $protecteur->externDBObj("Modifier");
            $protecteur->load($dbc->get("id_Player\$src"), $db);
            if ($protecteur->get("state") != "turtle" && ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 1 &&
                 $protecteur->get("inbuilding") == $opponent->get("inbuilding")) ||
                 ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 0) && $protecteur->get("room") == $opponent->get("room"))))
                $param["PROTECTION"] = 1;
            
            if ($protecteur->get("id") == $playerSrc->get("id"))
                $param["PROTECTION"] = 0;
        }
        
        if ($param["PROTECTION"] == 1)
            self::interBurning($playerSrc, $protecteur, $param, $db, $modifierA, $modifierB, $opponent);
        else {
            // Création clone et application des modifier
            $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
            $opponentModif = clone ($opponent->getObj("Modifier"));
            
            SpellFactory::passiveBarrier($playerSrc, $opponent, $param, $db);
            if ($param["PLAYER_KILLED"])
                $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
            else {
                // Jets de dés
                if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 or $playerSrc->get("status") == 'NPC') {
                    $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("attack");
                    if ($playerSrc->get("status") == 'PC')
                        PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
                } else
                    $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("dexterity");
                
                $param["TARGET_DEFENSE"] = $opponentModif->getScore("defense");
                
                // Attaque magique réussi
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                    $param["PLAYER_MM"] = $playerSrcModif->getScore("magicSkill");
                    $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
                    $modifier = new Modifier();
                    $modifier->addModif("damage", DICE_D6, $param["SPELL_LEVEL"]);
                    $param["TARGET_DAMAGE"] = floor($modifier->getScoreWithNoBM("damage") * 1.7);
                    $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGE"];
                    
                    // Si l'adversaire possède une bulle de vie
                    SpellFactory::PassiveBubble($opponent, $param, $db);
                    
                    $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - ($opponentModif->getScore("armor")), 0);
                    $hp = $opponent->get("currhp") - $param["TARGET_HP"];
                    $param["XP"] += 1;
                    if ($param["TARGET_HP"] > 0)
                        PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                        
                        // Mort de la cible
                    if ($hp <= 0)
                        BasicActionFactory::kill($playerSrc, $opponent, $param, $db);
                    else {
                        
                        PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_HP"], $param, $db);
                        $opponent->updateDBr($db);
                        $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
                    }
                } else {
                    // Attaque magique raté
                    if ($playerSrc->get("status") == 'PC')
                        BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
                    PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
                    $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_FAIL;
                }
                
                if ($playerSrc->get("modeAnimation") == 0) {
                    PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                    $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
                    $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
                }
            }
            $playerSrc->updateDBr($db);
        }
        self::removeAnger($playerSrc, 1, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function armor($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_ARMOR;
        $param["TYPE_ATTACK"] = SPELL_ARMOR_EVENT;
        
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5);
        
        $param["TARGET_ARMOR"] = 2.4;
        $param["TARGET_ARMOR"] += $param["SPELL_LEVEL"] * 1.2;
        $levelBM = 2 + $param["SPELL_LEVEL"];
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Armure d\'Athlan'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbu = new DBCollection("UPDATE BM SET life=1, level=" . $levelBM . " WHERE id_Player=" . $opponent->get("id") . " AND name='Armure d\'Athlan'", $db, 0, 0, false);
        else
            $dbi = new DBCollection(
                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (9," . $opponent->get("id") . "," . $playerSrc->get("id") . "," .
                     $levelBM . ", 'POSITIVE', 'Armure d\'Athlan' ,1,'" . $date . "')", $db, 0, 0, false);
        
        if ($playerSrc->get("id") == $opponent->get("id")) {
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 1;
        } else {
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 0;
            $opponent->updateDBr($db);
        }
        
        // $param["XP"] +=1;
        if ($playerSrc->get("status") == "PC") {
            $chance = mt_rand(0, 1);
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2.5);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function tears($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_TEARS;
        $param["TYPE_ATTACK"] = SPELL_TEARS_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, ceil($param["SPELL_LEVEL"]));
        $param["HP_POTENTIEL"] = $modifier->getScoreWithNoBM("hp");
        $param["SELF"] = 0;
        if ($playerSrc->get("id") == $opponent->get("id")) {
            $param["SELF"] = 1;
            $param["HP_HEAL"] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
        } else {
            $param["HP_HEAL"] = PlayerFactory::addHP($opponent, $param["HP_POTENTIEL"], $db, 0);
            $opponent->updateDB($db);
        }
        
        if ($playerSrc->get("status") == "PC") {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 7);
            }
        }
        
        $playerSrc->updateDbr($db);
        return ACTION_NO_ERROR;
    }

    static function instantBlood($playerSrc, $opponent, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = M_INSTANTBLOOD;
        
        // Création clone et application des modifier
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $opponentModif = clone ($opponent->getObj("Modifier"));
        
        // Jets de dés
        $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("magicSkill");
        $param["TARGET_DEFENSE"] = $opponentModif->getScore("magicSkill");
        
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
            $param["SPELL_LEVEL"] = 1 + ceil(($param["PLAYER_ATTACK"] - $param["TARGET_DEFENSE"]) / 6);
            $modifier = new Modifier();
            $modifier->addModif("damage", DICE_D6, $param["SPELL_LEVEL"]);
            $param["TARGET_DAMAGETOT2"] = floor($modifier->getScoreWithNoBM("damage"));
            $param["TARGET_DAMAGE"] = $param["TARGET_DAMAGETOT2"];
            // Si l'adversaire possède une bulle de vie
            SpellFactory::PassiveBubble($opponent, $param, $db);
            $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - floor($opponentModif->getScore("armor") / 2), 0);
            $hp = $opponent->get("currhp") - $param["TARGET_HP"];
            $param["XP"] += 1;
            if ($hp <= 0)
                BasicActionFactory::kill($playerSrc, $opponent, $param, $db);
            else {
                PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_HP"], $param, $db);
                $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
            }
            
            if ($opponent->get("status") == "NPC") {
                require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                PNJFactory::runIA($opponent, $playerSrc->get("id"), $db);
            }
            if ($param["TARGET_HP"] > 0)
                PlayerFactory::Enhance($opponent, MAGICAL_DEFENSE_EVENT_FAIL, $param, $db);
            $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
        } else { // Attaque magique raté
            
            PlayerFactory::Enhance($opponent, MAGICAL_DEFENSE_EVENT_SUCCESS, $param, $db);
            $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_FAIL;
        }
        
        if ($playerSrc->get("status") == "PC") {
            BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            $opponent->updateDBr($db);
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 8);
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function kine($playerSrc, $opponent, $x, $y, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_KINE;
        
        $param["X"] = $x;
        $param["Y"] = $y;
        $oldx = $opponent->get("x");
        $oldy = $opponent->get("y");
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        
        $param["TARGET_MM"] = floor($opponent->getScore("magicSkill") / 4);
        $param["MOVE"] = 0;
        if ($param["SPELL_LEVEL"] > $param["TARGET_MM"]) {
            $param["TYPE_ATTACK"] = SPELL_KINE_EVENT_SUCCESS;
            require_once (HOMEPATH . "/lib/MapInfo.inc.php");
            $mapinfo = new MapInfo($db);
            $validzone = $mapinfo->getValidMap($x, $y, 0, $playerSrc->get("map"));
            if ($validzone[0][0] && BasicActionFactory::freePlace($x, $y, $playerSrc->get("map"), $db)) {
                $opponent->set("x", $x);
                $opponent->set("y", $y);
                $opponent->updateDB($db);
                if ($opponent->get("id_BasicRace") == 263)
                    $dbc = new DBCollection("UPDATE Player SET x=" . $x . ", y=" . $y . " WHERE x=" . $oldx . " AND y=" . $oldy . " AND id=" . $opponent->get("id_Owner"), $db, 0, 0, 
                        false);
                $param["MOVE"] = 1;
                if ($playerSrc->get("status") == "PC") {
                    PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                }
            }
        } else {
            $param["TYPE_ATTACK"] = SPELL_KINE_EVENT_FAIL;
            if ($playerSrc->get("status") == "PC") {
                $param["XP"] -= 1;
            }
        }
        
        if ($playerSrc->get("status") == "PC") {
            $chance = mt_rand(0, 1);
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3);
                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
            }
        }
        $playerSrc->updateDbr($db);
        return ACTION_NO_ERROR;
    }

    /* *********************************************************** LES SORTILEGES ECOLE de GUERISON *********************************************** */
    static function cure($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_CURE;
        $param["TYPE_ATTACK"] = SPELL_CURE_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        
        $dbbm = new DBCollection(
            "UPDATE BM SET value=value-" . $param["SPELL_LEVEL"] . ", level = level-" . $param["SPELL_LEVEL"] . " WHERE effect='NEGATIVE' AND id_Player=" . $opponent->get("id"), 
            $db, 0, 0, false);
        if ($playerSrc->get("id") == $opponent->get("id")) {
            $param["SELF"] = 1;
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        } else {
            $param["SELF"] = 0;
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $opponent->updateDBr($db);
        }
        
        $chance = mt_rand(0, 1);
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
        }
        $playerSrc->updateDbr($db);
        return ACTION_NO_ERROR;
    }

    static function regen($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_REGEN;
        $param["TYPE_ATTACK"] = SPELL_REGEN_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, ceil($param["SPELL_LEVEL"]));
        $param["TARGET_REGEN"] = floor($modifier->getScoreWithNoBM("hp") / 2);
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Régénération'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbx = new DBCollection("UPDATE BM SET life=3, date='" . $date . "', value=" . $param["TARGET_REGEN"] . " WHERE id=" . $dbc->get('id'), $db, 0, 0, false);
        else
            $dbi = new DBCollection(
                "INSERT INTO BM (id_Player,id_Player\$src, level, value,name,life,date) VALUES (" . $opponent->get("id") . "," . $playerSrc->get("id") . "," . $param["SPELL_LEVEL"] .
                     ", " . $param["TARGET_REGEN"] . ", 'Régénération' ,3,'" . $date . "')", $db, 0, 0, false);
        
        if ($playerSrc->get("id") == $opponent->get("id"))
            $param["SELF"] = 1;
        else
            $param["SELF"] = 0;
        
        if ($playerSrc->get("status") == "PC") {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 8);
            }
        }
        $playerSrc->updateDbr($db);
        return ACTION_NO_ERROR;
    }

    static function spring($playerSrc, $ident, $x, $y, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_SPRING;
        $param["TYPE_ATTACK"] = SPELL_SPRING_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $param["XB"] = $x;
        $param["YB"] = $y;
        
        $dbb = new DBCollection(
            "INSERT INTO Building (name, id_BasicBuilding,id_City, id_Player, level, value, sp, currsp, progress, x ,y, map) VALUES ('Bassin Divin', 15, 0," . $playerSrc->get("id") .
                 "," . $param["SPELL_LEVEL"] . ", 5, 15 ,15,15," . $x . "," . $y . "," . $playerSrc->get("map") . ")", $db, 0, 0, false);
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 9);
        }
        $playerSrc->updateDbr($db);
        return ACTION_NO_ERROR;
    }

    static function rain($playerSrc, $x, $y, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["X"] = $x;
        $param["Y"] = $y;
        
        // Création clone et application des modifier
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        
        // Jets de dés
        $param["PLAYER_MM"] = $playerSrcModif->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, ceil($param["SPELL_LEVEL"]));
        $param["HP_POTENTIEL"] = ceil($modifier->getScoreWithNoBM("hp") / 2);
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $nb = 0;
        $dbp = new DBCollection(
            "SELECT * FROM Player WHERE disabled=0 AND inbuilding=0 and state!= 'turtle' AND map=" . $playerSrc->get("map") . " AND (abs(x-" . $x . ") + abs(y-" . $y .
                 ") + abs(x+y-" . $x . "-" . $y . "))/2<=1", $db, 0, 0);
        while (! $dbp->eof()) {
            $opponent->load($dbp->get("id"), $db);
            if ($opponent->get("hidden") % 10 != 2) {
                $nb ++;
                BasicActionFactory::globalInfoOpponent($opponent, $param[$nb]);
                BasicActionFactory::globalInfoOpponent($opponent, $param);
                
                if ($playerSrc->get("id") == $opponent->get("id")) {
                    $param["SELF"][$nb] = 1;
                    $param["HP_HEAL"][$nb] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
                } else {
                    $param["SELF"][$nb] = 0;
                    $param["HP_HEAL"][$nb] = PlayerFactory::addHP($opponent, $param["HP_POTENTIEL"], $db, 0);
                    echo $param["HP_HEAL"][$nb];
                    
                    $opponent->updateDB($db);
                }
                
                $param["ERROR"] = 0;
                $param["TYPE_ACTION"] = PASSIVE_RAIN;
                $param["TYPE_ATTACK"] = PASSIVE_RAIN_EVENT;
                $param["HP_HL"] = $param["HP_HEAL"][$nb];
                if ($param["SELF"][$nb] != 1)
                    Event::logAction($db, PASSIVE_RAIN, $param);
            }
            $dbp->next();
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        $param["NB"] = $nb;
        $param["TYPE_ACTION"] = SPELL_RAIN;
        $param["TYPE_ATTACK"] = SPELL_RAIN_EVENT;
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function reload($playerSrc, $id_Spring, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        
        $param["TYPE_ACTION"] = SPELL_RELOAD;
        $param["TYPE_ATTACK"] = SPELL_RELOAD_EVENT;
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id_Spring . " and id_BasicBuilding=15", $db);
        $param["BUILDING_LEVEL"] = $dbc->get("level");
        if ($dbc->get("level") - 2 <= $param["SPELL_LEVEL"]) {
            $dbc = new DBCollection("UPDATE Building SET value=5, currsp=sp WHERE id=" . $id_Spring . " and id_BasicBuilding=15", $db, 0, 0, false);
            if ($playerSrc->get("status") == "PC") {
                $chance = mt_rand(0, 1);
                if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
                    PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
                
                if ($playerSrc->get("modeAnimation") == 0) {
                    $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
                    PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                }
                $playerSrc->updateDBr($db);
            }
        } else {
            $param["TYPE_ATTACK"] = SPELL_RELOAD_EVENT_FAIL;
            $param["XP"] -= 1;
        }
        return ACTION_NO_ERROR;
    }

    static function sun($playerSrc, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_SUN;
        $param["TYPE_ATTACK"] = SPELL_SUN_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + ceil($param["PLAYER_MM"] / 8);
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Soleil de Guérison'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbx = new DBCollection("UPDATE BM SET life = 3, value='" . $param["SPELL_LEVEL"] . "' WHERE id_Player=" . $dbc->get('id'), $db, 0, 0, false);
        else {
            $bm = new BM();
            $bm->set("name", "Soleil de guérison");
            $bm->set("value", $param["SPELL_LEVEL"]);
            $bm->set("level", $param["SPELL_LEVEL"]);
            $bm->set("life", "2");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
        }
        
        if ($playerSrc->get("status") == "PC") {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 10);
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            }
            $playerSrc->updateDBr($db);
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function passiveSun($playerSrc, $damage, $db)
    {
        if ($damage != 0 && $playerSrc->get("inbuilding") == 0) {
            BasicActionFactory::globalInfoOpponent($playerSrc, $param);
            $param["TYPE_ACTION"] = PASSIVE_SUN;
            $param["TYPE_ATTACK"] = PASSIVE_SUN_EVENT;
            $opponent = new Player();
            $xp = $playerSrc->get("x");
            $yp = $playerSrc->get("y");
            $map = $playerSrc->get("map");
            
            $dbtri = new DBCollection(
                "SELECT case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                     " then p2.id_FighterGroup else p1.id_FighterGroup end as id_FighterGroup FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                     ID_BASIC_RACE_FEU_FOL . ") WHERE p1.id=" . $playerSrc->get("id"), $db);
            
            if ($dbtri->get("id_FighterGroup") == 0) {
                $dbx = new DBCollection("SELECT * FROM BM WHERE name='Soleil de guérison' AND id_Player=" . $playerSrc->get("id"), $db);
                if (! $dbx->eof()) {
                    BasicActionFactory::globalInfo($playerSrc, $param);
                    $param["PLAYER_HP"] = PlayerFactory::addHP($playerSrc, ceil($damage * $dbx->get("value") / 100), $db, 0);
                    $playerSrc->updateDB($db);
                    $param["ERROR"] = 0;
                    $param["DELAI"] = 2;
                    $param["TYPE_ACTION"] = PASSIVE_SUN;
                    $param["TYPE_ATTACK"] = PASSIVE_SUN_EVENT;
                    Event::logAction($db, PASSIVE_SUN, $param);
                }
            } else {
                $dbc = new DBCollection(
                    "SELECT * FROM Player WHERE id_FighterGroup=" . $dbtri->get("id_FighterGroup") . " AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" .
                         $xp . "-" . $yp . "))/2 <= 5 ORDER BY RAND()", $db);
                $soleilnotDone = true;
                while (! $dbc->eof() && $soleilnotDone) {
                    $dbx = new DBCollection("SELECT * FROM BM WHERE name='Soleil de guérison' AND id_Player=" . $dbc->get("id") . " ORDER BY RAND()", $db);
                    if (! $dbx->eof()) {
                        $opponent->load($dbc->get("id"), $db);
                        BasicActionFactory::globalInfo($opponent, $param);
                        $param["PLAYER_HP"] = PlayerFactory::addHP($playerSrc, ceil($damage * $dbx->get("value") / 100), $db, 0);
                        $playerSrc->updateDB($db);
                        $param["ERROR"] = 0;
                        $param["DELAI"] = 2;
                        $param["TYPE_ACTION"] = PASSIVE_SUN;
                        $param["TYPE_ATTACK"] = PASSIVE_SUN_EVENT;
                        Event::logAction($db, PASSIVE_SUN, $param);
                        $soleilnotDone = false;
                    }
                    $dbc->next();
                }
            }
        }
    }

    /* *********************************************************** LES SORTILEGES ECOLE de PROTECTION *********************************************** */
    static function shield($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_SHIELD;
        $param["TYPE_ATTACK"] = SPELL_SHIELD_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5);
        $modifier = new Modifier();
        $modifier->addModif("defense", DICE_MULT, $param["SPELL_LEVEL"]);
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Bouclier magique'", $db);
        $date = gmdate('Y-m-d H:i:s');
        
        if (! $dbc->eof())
            $dbu = new DBCollection("UPDATE BM SET life=1, level=" . $param["SPELL_LEVEL"] . " WHERE id_Player=" . $opponent->get("id") . " AND name='Bouclier magique'", $db, 0, 0, 
                false);
        else
            $dbi = new DBCollection(
                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (11," . $opponent->get("id") . "," . $playerSrc->get("id") .
                     "," . $param["SPELL_LEVEL"] . ", 'POSITIVE', 'Bouclier magique' ,1,'" . $date . "')", $db, 0, 0, false);
        
        if ($playerSrc->get("id") == $opponent->get("id")) {
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 1;
        } else {
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 0;
            $opponent->updateDBr($db);
        }
        
        if ($playerSrc->get("status") == "PC") {
            $chance = mt_rand(0, 1);
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2.5);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function bless($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_BLESS;
        $param["TYPE_ATTACK"] = SPELL_BLESS_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5);
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Bénédiction'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbx = new DBCollection("UPDATE BM SET life=3, date='" . $date . "', level=" . $param["SPELL_LEVEL"] . " WHERE id=" . $dbc->get('id'), $db, 0, 0, false);
        else {
            $dbi = new DBCollection(
                "INSERT INTO BM (id_Player,id_Player\$src,name, level,date,life) VALUES (" . $opponent->get("id") . "," . $playerSrc->get("id") . ", 'Bénédiction' ," .
                     $param["SPELL_LEVEL"] . ",'" . $date . "',3)", $db, 0, 0, false);
        }
        if ($playerSrc->get("id") == $opponent->get("id"))
            $param["SELF"] = 1;
        else
            $param["SELF"] = 0;
        
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        
        $chance = mt_rand(0, 1);
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2.5);
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function barrier($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_BARRIER;
        $param["TYPE_ATTACK"] = SPELL_BARRIER_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5);
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Barrière enflammée'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbx = new DBCollection("UPDATE BM SET life=2, level='" . $param["SPELL_LEVEL"] . "', value='" . $param["SPELL_LEVEL"] . "' WHERE id=" . $dbc->get('id'), $db, 0, 0, 
                false);
        
        else {
            $bm = new BM();
            $bm->set("name", "Barrière enflammée");
            $bm->set("life", "2");
            $bm->set("level", $param["SPELL_LEVEL"]);
            $bm->set("value", $param["SPELL_LEVEL"]);
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $opponent->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
        }
        
        if ($playerSrc->get("id") == $opponent->get("id")) {
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 1;
        } else {
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 0;
            $opponent->updateDBr($db);
        }
        
        if ($playerSrc->get("status") == "PC") {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2.5);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function passiveBarrier($playerSrc, $opponent, &$param, $db)
    {
        $param["PLAYER_KILLED"] = 0;
        $dbx = new DBCollection("SELECT * FROM BM WHERE name='Barrière enflammée' AND id_Player=" . $opponent->get("id"), $db);
        if (! $dbx->eof()) {
            
            $param["BARRIER_LEVEL"] = $dbx->get("level");
            $modifier = new Modifier();
            $modifier->addModif("damage", DICE_D6, $param["BARRIER_LEVEL"]);
            $param["PLAYER_DAMAGE"] = floor($modifier->getScoreWithNoBM("damage") * 0.5);
            $hp = $playerSrc->get("currhp") - $param["PLAYER_DAMAGE"];
            if ($hp <= 0) {
                BasicActionFactory::globalInfoOpponent($playerSrc, $param2);
                $param["PLAYER_KILLED"] = 1;
                $param2["BARRIER_LEVEL"] = $param["BARRIER_LEVEL"];
                $param2["PLAYER_DAMAGE"] = $param["PLAYER_DAMAGE"];
                $param2["INTER_NAME"] = $opponent->get("name");
                $param2["ERROR"] = 0;
                $param2["ARREST"] = 0;
                $param2["XP"] = 0;
                $param2["TYPE_ACTION"] = PASSIVE_BARRIER_DEATH;
                $param2["TYPE_ATTACK"] = PASSIVE_BARRIER_DEATH_EVENT;
                $param2["DELAI"] = 2;
                
                if ($dbx->get("id_Player\$src") == $opponent->get("id")) {
                    BasicActionFactory::globalInfo($opponent, $param2);
                    BasicActionFactory::kill($opponent, $playerSrc, $param2, $db);
                    PlayerFactory::addXP($opponent, $param2["XP"], $db);
                } else {
                    $wizard = new Player();
                    $wizard->load($dbx->get("id_Player\$src"), $db);
                    BasicActionFactory::globalInfo($wizard, $param2);
                    BasicActionFactory::kill($wizard, $playerSrc, $param2, $db);
                    PlayerFactory::addXP($wizard, $param2["XP"], $db);
                    $wizard->updateDB($db);
                    $opponent->reloadPartialAttribute($opponent->get("id"), "xp", $db);
                }
                Event::logAction($db, PASSIVE_BARRIER_DEATH, $param2);
            } else {
                $playerSrc->set("currhp", $hp);
                $playerSrc->updateDB($db);
            }
        }
    }

    static function passiveBubble($opponent, &$param, $db)
    {
        $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bulle de vie' AND id_Player=" . $opponent->get("id"), $db);
        $param["BUBBLE_CANCEL"] = 0;
        if (! $dbc->eof()) {
            $param["BUBBLE_LIFE"] = $dbc->get("value");
            $param["BUBBLE_LEVEL"] = $dbc->get("level");
            $hitp = $param["BUBBLE_LIFE"] - $param["TARGET_DAMAGETOT2"];
            if ($hitp <= 0) {
                $dbc = new DBCollection("DELETE FROM BM WHERE name='Bulle de vie' AND id_Player=" . $opponent->get("id"), $db, 0, 0, false);
                $param["TARGET_DAMAGETOT2"] -= $param["BUBBLE_LIFE"];
            } else {
                $dbc = new DBCollection("UPDATE BM SET value=" . $hitp . ", level=" . ceil($hitp / 5) . " WHERE name='Bulle de vie' AND id_Player=" . $opponent->get("id"), $db, 0, 
                    0, false);
                $param["TARGET_DAMAGETOT2"] = 0;
                $param["BUBBLE_CANCEL"] = 1;
            }
            return 1;
        }
        return 0;
    }

    static function bubble($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_BUBBLE;
        $param["TYPE_ATTACK"] = SPELL_BUBBLE_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5);
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Bulle de vie'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbx = new DBCollection("UPDATE BM SET value=" . ($param["SPELL_LEVEL"] * 3) . ", level='" . $param["SPELL_LEVEL"] . "' WHERE id=" . $dbc->get('id'), $db, 0, 0, false);
        
        else {
            $bm = new BM();
            $bm->set("name", "Bulle de vie");
            $bm->set("life", "-2");
            $bm->set("level", $param["SPELL_LEVEL"]);
            $bm->set("value", $param["SPELL_LEVEL"] * 3);
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $opponent->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
        }
        
        if ($playerSrc->get("id") == $opponent->get("id")) {
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 1;
        } else {
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 0;
            $opponent->updateDBr($db);
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function negation($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_NEGATION;
        $param["TYPE_ATTACK"] = SPELL_NEGATION_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        
        PlayerFactory::deleteAllBM($opponent, $param, $db, 0, 1);
        
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5);
        $bm = new BM();
        $bm->set("name", "Souffle de négation");
        $bm->set("id_StaticModifier", 0);
        $bm->set("life", "-2");
        $bm->set("level", floor(max(30, 100 - $param["SPELL_LEVEL"])));
        $bm->set("value", 1);
        $bm->set("id_Player\$src", $playerSrc->get("id"));
        $bm->set("id_Player", $opponent->get("id"));
        $bm->set("date", gmdate("Y-m-d H:i:s"));
        $bm->addDBr($db);
        
        $opponent->set("hidden", floor($opponent->get("hidden") / 10) * 10 + 2);
        $opponent->set("currhp", 1);
        
        PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
        $opponent->updateDBr($db);
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function wall($playerSrc, $case, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_WALL;
        $param["TYPE_ATTACK"] = SPELL_WALL_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        $param["SPELL_LEVEL"] = max(1, floor(min($param["PLAYER_STRENGTH"], $param["PLAYER_MM"]) / 3.5));
        
        $sp = $param["SPELL_LEVEL"] * 15;
        $param["NB_WALL"] = count($case["x"]);
        
        for ($i = 0; $i < count($case["x"]); $i ++) {
            $param["X"][$i] = $case["x"][$i];
            $param["Y"][$i] = $case["y"][$i];
            $dbb = new DBCollection(
                "INSERT INTO Building (name, id_BasicBuilding,id_City, level, id_Player,  sp, currsp, progress, x ,y, map) VALUES ('Mur de terre', 27, 0," . $param["SPELL_LEVEL"] .
                     "," . $playerSrc->get("id") . "," . $sp . "," . $sp . "," . $sp . "," . $case["x"][$i] . "," . $case["y"][$i] . "," . $playerSrc->get("map") . ")", $db, 0, 0, 
                    false);
        }
        
        if ($playerSrc->get("status") == 'PC') {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    /* *********************************************************** LES SORTILEGES ECOLE des LIMBES *********************************************** */
    static function interFireball($playerSrc, $protecteur, &$param, &$db, $modifierA = null, $modifierB = null, $opponent)
    {
        BasicActionFactory::autoHustleInterposition($protecteur, $opponent, $db);
        
        self::globalInfo($playerSrc, $param);
        
        self::globalInfoOpponent($protecteur, $param);
        $param["NAME"] = $opponent->get("name");
        $param["INTER_ID"] = $opponent->get("id");
        $param["TYPE_ACTION"] = SPELL_FIREBALL;
        $param["PROTECTION"] = 1;
        $date = gmdate("Y-m-d H:i:s");
        
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        
        // Jets de dés
        if (PlayerFactory::getTypeMagicArmAtt($playerSrc, $db) > 0 or $playerSrc->get("status") == 'NPC') {
            $param["PLAYER_ATTACK"] = floor($playerSrc->getScore("attack") * 1.2);
            if ($playerSrc->get("status") == 'PC')
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        } else
            $param["PLAYER_ATTACK"] = floor(1.2 * $playerSrc->getScore("dexterity"));
            
            // $param["TARGET_DEFENSE"]=$opponentModif->getScore("defense");
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $modifier = new Modifier();
        $modifier->addModif("damage", DICE_D6, 1.2 * $param["SPELL_LEVEL"]);
        $param["TARGET_DAMAGE"] = floor($modifier->getScoreWithNoBM("damage"));
        $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGE"];
        
        // Si l'adversaire possède une bulle de vie
        SpellFactory::PassiveBubble($protecteur, $param, $db);
        $param["TARGET_MM"] = $protecteur->getScore("magicSkill");
        $param["ARMOR_BONUS"] = 10 + 2 * floor(1 + $param["TARGET_MM"] / 8);
        $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - floor((($protecteur->getScore("armor") + floor($protecteur->getScore("armor") * $param["ARMOR_BONUS"] / 100))) / 2), 
            0);
        $hp = $protecteur->get("currhp") - $param["TARGET_HP"];
        $param["XP"] += 1;
        if ($param["TARGET_HP"] > 0)
            PlayerFactory::Enhance($protecteur, DEFENSE_EVENT_FAIL, $param, $db);
            
            // Mort de la cible
        if ($hp <= 0) {
            BasicActionFactory::kill($playerSrc, $protecteur, $param, $db);
            $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_DEATH;
        } else {
            $param["TYPE_ATTACK"] = SPELL_INTER_EVENT_SUCCESS;
            PlayerFactory::loseHP($protecteur, $playerSrc, $param["TARGET_HP"], $param, $db);
            $opponent->updateDBr($db);
            // $param["TYPE_ATTACK"]= MAGICAL_ATTACK_EVENT_SUCCESS;
        }
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
        }
        $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    static function fireball($playerSrc, $opponent, &$param, &$db, $modifierA = null, $modifierB = null)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_FIREBALL;
        
        // Création clone et application des modifier
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $opponentModif = clone ($opponent->getObj("Modifier"));
        
        // Si l'opposant est sous protection
        $param["PROTECTION"] = 0;
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='défendu'", $db);
        if (! $dbc->eof()) {
            $protecteur = new Player();
            $protecteur->externDBObj("Modifier");
            $protecteur->load($dbc->get("id_Player\$src"), $db);
            if ($protecteur->get("state") != "turtle" && ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 1 &&
                 $protecteur->get("inbuilding") == $opponent->get("inbuilding")) ||
                 ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 0) && $protecteur->get("room") == $opponent->get("room"))))
                $param["PROTECTION"] = 1;
            
            if ($protecteur->get("id") == $playerSrc->get("id"))
                $param["PROTECTION"] = 0;
        }
        
        if ($param["PROTECTION"] == 1)
            self::interFireball($playerSrc, $protecteur, $param, $db, $modifierA, $modifierB, $opponent);
        else {
            // Jets de dés
            if (PlayerFactory::getTypeMagicArmAtt($playerSrc, $db) > 0 or $playerSrc->get("status") == 'NPC') {
                $param["PLAYER_ATTACK"] = floor(1.2 * $playerSrcModif->getScore("attack"));
                // require_once(HOMEPATH."/Factory/PlayerFactory.inc.php");
            } else
                $param["PLAYER_ATTACK"] = floor(1.2 * $playerSrcModif->getScore("dexterity"));
            
            $param["TARGET_DEFENSE"] = $opponentModif->getScore("defense");
            $param["GOLEM"] = 0;
            
            // Attaque magique réussi
            if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
                if (! $dbc->eof()) {
                    $opponent->set("currhp", $opponent->get("currhp") + 10 * $dbc->get("level"));
                    $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db, 0, 0, false);
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param2, $db, 0);
                    $opponent->updateDBr($db);
                    $param["PIEGE"] = 1;
                }
                
                $param["PLAYER_MM"] = $playerSrcModif->getScore("magicSkill");
                $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
                $modifier = new Modifier();
                $modifier->addModif("damage", DICE_D6, 1.2 * $param["SPELL_LEVEL"]);
                $param["TARGET_DAMAGETOT2"] = floor($modifier->getScoreWithNoBM("damage"));
                $param["TARGET_DAMAGE"] = $param["TARGET_DAMAGETOT2"];
                if ($opponent->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && $opponent->get("id_Member") == 0) {
                    $opponent->set("hp", $playerSrc->get("hp") + 15);
                    $opponent->set("currhp", $playerSrc->get("currhp") + 15);
                    $opponent->addModif("dexterity", DICE_D6, 1);
                    $opponent->addModif("magicSkill", DICE_D6, 1);
                    $opponent->set("level", $opponent->get("level") + 1);
                    $opponent->updateDBr($db);
                    $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
                    $param["GOLEM"] = 1;
                } else {
                    // Si l'adversaire possède une bulle de vie
                    SpellFactory::PassiveBubble($opponent, $param, $db);
                    $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - floor($opponentModif->getScore("armor") / 2), 0);
                    $hp = $opponent->get("currhp") - $param["TARGET_HP"];
                    $param["XP"] += 1;
                    if ($param["TARGET_HP"] > 0)
                        PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                        
                        // Mort de la cible
                    if ($hp <= 0)
                        BasicActionFactory::kill($playerSrc, $opponent, $param, $db);
                    else {
                        PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_HP"], $param, $db);
                        $opponent->updateDBr($db);
                        $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
                    }
                }
            } else {
                // Attaque magique raté
                if ($playerSrc->get("id_Member") != 0)
                    BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
                
                $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_FAIL;
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            }
            
            if ($playerSrc->get("status") == "PC") {
                if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                    PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
                
                if ($playerSrc->get("modeAnimation") == 0) {
                    PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                    $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
                    $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
                }
            }
            $playerSrc->updateDBr($db);
        }
        self::removeAnger($playerSrc, 1, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function pillars($playerSrc, $ident, $x1, $y1, $x2, $y2, $x3, $y3, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_PILLARS;
        $param["TYPE_ATTACK"] = SPELL_PILLARS_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0) {
            $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        } else
            $param["PLAYER_ATTACK"] = $playerSrc->getScore("dexterity");
        
        $param["PLAYER_DEX"] = $playerSrc->getScore("dexterity");
        $param["SPELL_LEVEL"] = ceil(sqrt($param["PLAYER_MM"] * $param["PLAYER_ATTACK"]) / 3.5);
        
        $param["X"][1] = $x1;
        $param["Y"][1] = $y1;
        $param["X"][2] = $x2;
        $param["Y"][2] = $y2;
        $param["X"][3] = $x3;
        $param["Y"][3] = $y3;
        
        $modifier = new Modifier();
        $modifier->addModif("damage", DICE_D6, $param["SPELL_LEVEL"] * 2);
        $buildingDamage = floor($modifier->getScoreWithNoBM("damage"));
        for ($i = 1; $i < 4; $i ++) {
            $dbc = new DBCollection(
                "SELECT Building.*,City.captured,City.id_Player as governor FROM Building left outer join City on Building.id_City = City.id WHERE Building.x=" . $param["X"][$i] .
                     " AND Building.y=" . $param["Y"][$i] . " AND id_BasicBuilding!=14 AND Building.map=" . $playerSrc->get("map"), $db);
            if (! $dbc->eof()) {
                if ($dbc->get("captured") < 4 && $dbc->get("id_BasicBuilding") != 15 && $dbc->get("id_BasicBuilding") != 27) {
                    $param["BUILDING_DAMAGE"] = 0;
                } else {
                    $param["BUILDING_DAMAGE"] = $buildingDamage;
                }
                $building = new Building();
                $building->load($dbc->get("id"), $db);
                $param["BUILDING_LEVEL"][$i] = $building->get("level");
                $param["BUILDING_NAME"][$i] = $building->get("name");
                
                if ($building->get("id_Player") != 0) {
                    $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $building->get("id_Player"), $db);
                    $dest = array();
                    $dest[$dbp->get("name")] = $dbp->get("name");
                } else {
                    if ($dbc->get("captured") > 3) {
                        $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("governor"), $db);
                        $dest = array();
                        $dest[$dbp->get("name")] = $dbp->get("name");
                    }
                }
                
                if ($param["BUILDING_DAMAGE"] >= $building->get("currsp")) {
                    if ($building->get("id_Player") != 0 || $dbc->get("captured") > 3) {
                        $body = "Votre bâtiment : " . $building->get("name") . " (" . $building->get("id") . ") situé en " . $dbc->get("x") . "/" . $dbc->get("y") . " a été détruit";
                        MailFactory::sendMsg($playerSrc, localize("Bâtiment détruit"), $body, $dest, 2, $err, $db, - 1, 0);
                    }
                    BasicActionFactory::collapseBuilding($building, $playerSrc, $db);
                    if ($building->get("name") == "Rempart" or $building->get("id_BasicBuilding") == 21 or $building->get("id_BasicBuilding") == 26) {
                        $building->set("progress", 0);
                        $building->set("currsp", 0);
                        $building->set("repair", 0);
                        $building->updateDB($db);
                    } else
                        $building->deleteDB($db);
                    
                    $param["BUILDING_DEATH"][$i] = 1;
                } else {
                    if ($building->get("id_Player") != 0 || $dbc->get("captured") > 3) {
                        $body = "Votre bâtiment : " . $building->get("name") . " (" . $building->get("id") . ") situé en " . $dbc->get("x") . "/" . $dbc->get("y") .
                             " a été endommagé";
                        MailFactory::sendMsg($playerSrc, localize("Bâtiment endommagé"), $body, $dest, 2, $err, $db, - 1, 0);
                    }
                    $building->set("currsp", $building->get("currsp") - $param["BUILDING_DAMAGE"]);
                    $building->updateDB($db);
                    $param["BUILDING_DEATH"][$i] = 0;
                }
            }
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4.5);
            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4.5);
        }
        $playerSrc->updateDBr($db);
        $playerSrc->updateHidden($db);
        
        self::removeAnger($playerSrc, 1, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function demonpunch($playerSrc, $opponent, $slot, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_DEMONPUNCH;
        
        // Création clone et application des modifier
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $opponentModif = clone ($opponent->getObj("Modifier"));
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0) {
            $param["PLAYER_ATTACK"] = floor($playerSrcModif->getScore("attack") * 1.34);
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        } else
            $param["PLAYER_ATTACK"] = floor($playerSrcModif->getScore("dexterity") * 1.34);
        
        $param["TARGET_DEFENSE"] = $opponentModif->getScore("defense");
        
        self::passiveBarrier($playerSrc, $opponent, $param, $db);
        if ($param["PLAYER_KILLED"])
            $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_FAIL;
        else {
            
            // Attaque magique réussi
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                $param["XP"] += 1;
                $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                $param["PLAYER_MM"] = $playerSrcModif->getScore("magicSkill");
                $param["SPELL_LEVEL"] = 1 + ceil($param["PLAYER_MM"] / 8);
                if ($opponent->get("status") == "NPC") {
                    $opponent->getObj("Modifier")->setModif("armor", DICE_ADD, max(0, $opponent->getModif("armor", DICE_ADD) - $param["SPELL_LEVEL"] * 1.2));
                    // playerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                    
                    require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                    PNJFactory::runIA($opponent, $playerSrc->get("id"), $db);
                } else {
                    $id_Equip = PlayerFactory::getIdEquipFromSlot($opponent->get("id"), $slot, $db);
                    $point = $param["SPELL_LEVEL"] * 4;
                    $param["POINT"] = $point;
                    $equip = new Equipment();
                    if ($id_Equip != 0) {
                        $equip->load($id_Equip, $db);
                        $pt = max($equip->get("durability"), 0);
                        $equip->set("durability", max($equip->get("durability") - $point, 0));
                        $param["NAME_OBJECT"][0] = $equip->get("name");
                        $param["DUR_OBJECT"][0] = $equip->get("durability");
                        $equip->updateDB($db);
                        $point -= $pt;
                    } else
                        $param["NAME_OBJECT"][0] = "none";
                    
                    $i = 1;
                    $over = 0;
                    while ($point > 0 && ! $over) {
                        $id = PlayerFactory::getRndEquip($opponent->get("id"), $db);
                        if ($id == 0)
                            $over = 1;
                        else {
                            $equip->load($id, $db);
                            $pt = $equip->get("durability");
                            $equip->set("durability", max($equip->get("durability") - $point, 0));
                            $param["NAME_OBJECT"][$i] = $equip->get("name");
                            $param["DUR_OBJECT"][$i] = $equip->get("durability");
                            $equip->updateDB($db);
                            $point -= $pt;
                            $i ++;
                        }
                    }
                    
                    $param["NB_OBJECT"] = $i;
                    playerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                }
            } else { // Attaque magique raté
                $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_FAIL;
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            }
        }
        
        $opponent->updateDBr($db);
        
        if ($playerSrc->get("status") == 'PC') {
            BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3.5);
                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3.5);
            }
        }
        
        $playerSrc->updateDBr($db);
        self::removeAnger($playerSrc, 1, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function fire($playerSrc, $x, $y, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfo($playerSrc, $param2);
        
        $param["X"] = $x;
        $param["Y"] = $y;
        
        // Création clone et application des modifier
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        
        // Jets de dés
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 or $playerSrc->get("status") == 'NPC') {
            $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("attack");
            // require_once(HOMEPATH."/Factory/PlayerFactory.inc.php");
        } else
            $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("dexterity");
        
        $param["PLAYER_MM"] = $playerSrcModif->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + ceil($param["PLAYER_MM"] / 8);
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $nb = 0;
        $dbp = new DBCollection(
            "SELECT * FROM Player WHERE disabled=0 AND inbuilding=0 AND state!= 'turtle' AND map=" . $playerSrc->get("map") . " AND (abs(x-" . $x . ") + abs(y-" . $y .
                 ") + abs(x+y-" . $x . "-" . $y . "))/2<=1", $db, 0, 0);
        
        $isPJHit = false;
        
        while (! $dbp->eof()) {
            $opponent->load($dbp->get("id"), $db);
            
            if ($opponent->get("hidden") % 10 != 2) {
                
                $nb ++;
                
                $param["ERROR"] = 0;
                $param["TYPE_ACTION"] = PASSIVE_FIRE;
                
                $param2 = array();
                $param2["TYPE_ACTION"] = PASSIVE_FIRE;
                $param2["XP"] = 0;
                
                BasicActionFactory::globalInfo($playerSrc, $param2);
                
                BasicActionFactory::globalInfoOpponent($opponent, $param[$nb]);
                BasicActionFactory::globalInfoOpponent($opponent, $param);
                BasicActionFactory::globalInfoOpponent($opponent, $param2);
                
                $param["TARGET_DEFENSE"][$nb] = $opponent->getScore("defense");
                $param["KILLED"][$nb] = 0;
                $param["SELF"][$nb] = 1;
                if ($dbp->get("id") != $playerSrc->get("id"))
                    $param["SELF"][$nb] = 0;
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$nb]) {
                    
                    $isPJHit = true;
                    
                    // Araignée en mode piège - annulation du bm
                    $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
                    if (! $dbc->eof()) {
                        $opponent->set("currhp", $opponent->get("currhp") + 10 * $dbc->get("level"));
                        $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db, 0, 0, false);
                        PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param3, $db, 0);
                        $opponent->updateDBr($db);
                    }
                    
                    $modifier = new Modifier();
                    $modifier->addModif("damage", DICE_D6, $param["SPELL_LEVEL"]);
                    $mult = 0.8;
                    
                    $param["TARGET_DAMAGE"][$nb] = floor($modifier->getScoreWithNoBM("damage") * $mult);
                    $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGE"][$nb];
                    
                    // Si l'adversaire possède une bulle de vie
                    if (SpellFactory::PassiveBubble($opponent, $param, $db)) {
                        $param["BUBBLE_LIFE2"][$nb] = $param["BUBBLE_LIFE"];
                        $param["BUBBLE_CANCEL2"][$nb] = $param["BUBBLE_CANCEL"];
                        $param["BUBBLE_LEVEL2"][$nb] = $param["BUBBLE_LEVEL"];
                    } else
                        $param["BUBBLE_LIFE"] = 0;
                    
                    $param["TARGET_DAM"] = $param["TARGET_DAMAGE"][$nb];
                    $param["TARGET_DAM2"] = $param["TARGET_DAMAGETOT2"];
                    $param["TARGET_DAMAGE2"][$nb] = $param["TARGET_DAMAGETOT2"];
                    
                    $hp = $opponent->get("currhp") - $param["TARGET_DAMAGETOT2"];
                    if ($param["TARGET_DAMAGETOT2"] > 0)
                        PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                        
                        // Mort de la cible
                    if ($hp <= 0) {
                        if ($dbp->get("id") == $playerSrc->get("id"))
                            BasicActionFactory::autokill($playerSrc, $param2, $db);
                        else {
                            BasicActionFactory::kill($playerSrc, $opponent, $param2, $db);
                            $param["TYPE_ATTACK"] = $param2["TYPE_ATTACK"];
                            $param["KILLED"][$nb] = 1;
                            $param["NOMBRE"] = $param2["NOMBRE"];
                            
                            if (! isset($param["PLAYER" . 1])) {
                                for ($j = 1; $j < $param2["NOMBRE"]; $j ++) {
                                    $param["PLAYER" . $j] = $param2["PLAYER" . $j];
                                    if (isset($param["GAIN" . $j]))
                                        $param["GAIN" . $j] += $param2["GAIN" . $j];
                                    else
                                        $param["GAIN" . $j] = $param2["GAIN" . $j];
                                }
                            } else {
                                for ($j = 1; $j < $param2["NOMBRE"]; $j ++) {
                                    for ($k = 1; $k < $param2["NOMBRE"]; $k ++) {
                                        if ($param["PLAYER" . $k] == $param2["PLAYER" . $j]) {
                                            if (isset($param["GAIN" . $k]))
                                                $param["GAIN" . $k] += $param2["GAIN" . $j];
                                            else
                                                $param["GAIN" . $k] = $param2["GAIN" . $j];
                                        }
                                    }
                                }
                            }
                            
                            if (isset($param2["EQUIPMENT_FALL"]))
                                $param["EQUIPMENT_FALL"][$nb] = $param2["EQUIPMENT_FALL"];
                            $param["MONEY_FALL"][$nb] = $param2["MONEY_FALL"];
                            if (isset($param2["DROP_NAME"]))
                                $param["DROP_NAME"][$nb] = $param2["DROP_NAME"];
                            if (isset($param2["LARCIN"]))
                                $param["LARCIN"][$nb] = $param2["LARCIN"];
                            
                            $param["XP"] += $param2["XP"];
                        }
                    } else {
                        // La cible ne meurt pas
                        $param["TYPE_ATTACK"] = SPELL_FIRE_EVENT_SUCCESS;
                        PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_DAMAGETOT2"], $param, $db);
                        $opponent->updateDBr($db);
                        if ($param["SELF"][$nb] == 1) {
                            $playerSrc->reloadPartialAttribute($playerSrc->get("id"), "currhp", $db);
                        }
                    }
                } else {
                    // La cible n'est pas touché
                    $param["TYPE_ATTACK"] = SPELL_FIRE_EVENT_FAIL;
                    if ($playerSrc->get("status") == 'PC')
                        BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
                    
                    PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
                    // $opponent->updateDBr($db);
                }
                
                // Log de l'action pour la cible
                $param["ERROR"] = 0;
                $param["TYPE_ACTION"] = PASSIVE_FIRE;
                $param["TARGET_DEF"] = $param["TARGET_DEFENSE"][$nb];
                if ($param["SELF"][$nb] != 1)
                    Event::logAction($db, PASSIVE_FIRE, $param);
                
                unset($param2);
            }
            $dbp->next();
        }
        
        if ($isPJHit) { // au moins 1 PJ a été touché ajout du PX de touche
            $param["XP"] += 1;
        }
        // Log de l'action pour le lanceur
        $param["TYPE_ACTION"] = SPELL_FIRE;
        $param["TYPE_ATTACK"] = SPELL_FIRE_EVENT;
        $param["NB"] = $nb;
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 6);
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        }
        
        $playerSrc->updateDBr($db);
        self::removeAnger($playerSrc, 1, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function blood($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_BLOOD;
        
        // Création clone et application des modifier
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $opponentModif = clone ($opponent->getObj("Modifier"));
        
        // Jets de dés
        $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("magicSkill");
        $param["TARGET_DEFENSE"] = $opponentModif->getScore("magicSkill");
        
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"]) {
            
            // Araignée en mode piège - annulation du bm
            $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
            if (! $dbc->eof()) {
                $opponent->set("currhp", $opponent->get("currhp") + 10 * $dbc->get("level"));
                $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db, 0, 0, false);
                PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param3, $db, 0);
                $opponent->updateDBr($db);
            }
            
            $param["SPELL_LEVEL"] = 1 + ceil(($param["PLAYER_ATTACK"] - $param["TARGET_DEFENSE"]) / 6);
            $param["TARGET_DAMAGE"] = floor($param["SPELL_LEVEL"] * 1.5);
            $date = gmdate('Y-m-d H:i:s');
            $bm = new BM();
            $bm->set("name", "Sang de Lave");
            $bm->set("life", "5");
            $bm->set("value", floor($param["SPELL_LEVEL"] * 1.5));
            $bm->set("level", $param["SPELL_LEVEL"]);
            $bm->set("effect", "NEGATIVE");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $opponent->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            
            $param["CANCEL"] = 0;
            // Si l'opposant est sous bénédiction
            $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $opponent->get("id"), $db);
            if (! $dbc->eof()) {
                $param["LEVEL_BENE"] = $dbc->get("level");
                if ($param["LEVEL_BENE"] >= $param["SPELL_LEVEL"])
                    $param["CANCEL"] = 1;
                else {
                    $param["CANCEL"] = 0;
                    
                    $bm->set("level", $param["SPELL_LEVEL"] - $param["LEVEL_BENE"]);
                    $param["VALUE"] = floor($bm->get("level") * 1.5);
                    $bm->set("value", $param["VALUE"]);
                }
            }
            if (! $param["CANCEL"]) {
                $bm->addDBr($db);
                $param["XP"] += 1;
            }
            if ($opponent->get("status") == "NPC" and $opponent->get("id_Member") == 0) {
                require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                PNJFactory::runIA($opponent, $playerSrc->get("id"), $db);
            }
            PlayerFactory::Enhance($opponent, MAGICAL_DEFENSE_EVENT_FAIL, $param, $db);
            $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_SUCCESS;
        } else {
            // Attaque magique raté
            PlayerFactory::Enhance($opponent, MAGICAL_DEFENSE_EVENT_SUCCESS, $param, $db);
            $param["TYPE_ATTACK"] = MAGICAL_ATTACK_EVENT_FAIL;
        }
        
        if ($playerSrc->get("id_Member") != 0) {
            BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
        }
        
        if ($playerSrc->get("status") == "PC") {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 8);
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function fireCall($playerSrc, $x, $y, $target_id = 0, &$param, &$db, $id_Race = ID_BASIC_RACE_FEU_FOL)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_FIRECALL;
        $param["TYPE_ATTACK"] = SPELL_FIRECALL_EVENT;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        
        if (PlayerFactory::getTypeMagicArmAtt($playerSrc, $db) > 0) {
            $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        } else
            $param["PLAYER_ATTACK"] = $playerSrc->getScore("dexterity");
        
        if ($target_id > 0) {
            $param["ID_FEU_FOL_TARGET"] = $target_id;
            $npc = new Player();
            $npc->externDBObj("Modifier");
            $npc->load($target_id, $db);
            BasicActionFactory::globalInfoOpponent($npc, $param);
            $param["FEU_FOL_MM"] = $npc->getScore("magicSkill");
            $param["FEU_FOL_ATTACK"] = $npc->getScore("attack");
            if ($param["FEU_FOL_MM"] * 0.8 < $param["PLAYER_MM"] && $param["FEU_FOL_ATTACK"] * 0.8 < $param["PLAYER_ATTACK"]) {
                $param["FEU_FOL_CAPTURE"] = 1;
                $dbi = new DBCollection("UPDATE Player set Id_Owner=" . $playerSrc->get("id") . ", id_Member=" . $playerSrc->get("id_Member") . " where id=" . $target_id, $db, 0, 0, 
                    false);
                $id = $target_id;
                $param["TYPE_ATTACK"] = SPELL_FIRECALL_CAPTURE_EVENT_SUCCESS;
            } else {
                $param["FEU_FOL_CAPTURE"] = 0;
                if ($playerSrc->get("modeAnimation") == 0)
                    $param["XP"] = $param["XP"] - 1;
                $param["TYPE_ATTACK"] = SPELL_FIRECALL_CAPTURE_EVENT_FAIL;
            }
            $param["TYPE_ACTION"] = SPELL_FIRECALL_CAPTURE;
        } else {
            $param["SPELL_LEVEL"] = ceil(sqrt($param["PLAYER_MM"] * $param["PLAYER_ATTACK"]) / 3.5);
            
            $npc = PNJFactory::getPNJFromLevel($id_Race, ceil($param["SPELL_LEVEL"] / 1.5), $db);
            
            $npc->set("room", $playerSrc->get("room"));
            $npc->set("inbuilding", $playerSrc->get("inbuilding"));
            $npc->set("x", $x);
            $npc->set("y", $y);
            
            $npc->set("map", $playerSrc->get("map"));
            
            $npc->set("id_Member", $playerSrc->get("id_Member"));
            $npc->set("id_Owner", $playerSrc->get("id"));
            $npc->updateHidden($db);
            $id = $npc->addDBr($db);
            PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
            $upgrade = new Upgrade();
            $npc->setObj("Upgrade", $upgrade, true);
            PlayerFactory::NewATB($npc, $param2, $db);
            $npc->set("ap", 0);
            BasicActionFactory::globalInfoOpponent($npc, $param);
        }
        if ($target_id == 0 || $param["FEU_FOL_CAPTURE"] == 1) {
            
            $dbcP = new DBCollection("select skill,id_BasicMagicSpell from MagicSpell where id_Player=" . $playerSrc->get("id") . " and id_BasicMagicSpell in (14,11)", $db, 0, 0, 
                true);
            $pctBdf = 0;
            $pctSdl = 0;
            while (! $dbcP->eof()) {
                if ($dbcP->get("id_BasicMagicSpell") == 11)
                    $pctBdf = $dbcP->get("skill");
                if ($dbcP->get("id_BasicMagicSpell") == 14)
                    $pctSdl = $dbcP->get("skill");
                $dbcP->next();
            }
            $dbi = new DBCollection("DELETE FROM MagicSpell where id_Player=" . $npc->get("id"), $db, 0, 0, false);
            $dbi = new DBCollection("INSERT INTO MagicSpell (id_Player, id_BasicMagicSpell, skill)  VALUES (" . $npc->get("id") . ",11, " . max($pctBdf, 80) . ")", $db, 0, 0, false);
            $dbi = new DBCollection("INSERT INTO MagicSpell (id_Player, id_BasicMagicSpell, skill)  VALUES (" . $npc->get("id") . ",14, " . max($pctSdl, 80) . ")", $db, 0, 0, false);
            
            $date = gmdate('Y-m-d H:i:s');
            $dbi = new DBCollection(
                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (17," . $playerSrc->get("id") . "," . $id .
                     ", 1 , 'POSITIVE', 'Golem de Feu' ,-2,'" . $date . "')", $db, 0, 0, false);
            if ($playerSrc->get("status") == "PC") {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            }
        }
        
        if ($playerSrc->get("status") == "PC") {
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 5);
            }
        }
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $playerSrc->updateDBr($db);
        self::removeAnger($playerSrc, 1, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function revokeGolem($playerSrc, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = REVOKE_GOLEM;
        $param["TYPE_ATTACK"] = REVOKE_GOLEM_EVENT;
        
        $dbd = new DBCollection("SELECT id_Player\$src as id FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name ='Golem de Feu'", $db);
        $golem = new Player();
        $idGolem = $dbd->get("id");
        $golem->load($idGolem, $db);
        
        // Si attaché le bolas tombe au sol
        if ($golem->get("state") == "creeping") {
            
            $array_result = PlayerFactory::getListIdBolas($golem, $db);
            foreach ($array_result as $i => $value) {
                $idBolas = $value;
                $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $idGolem . " AND name='A terre'", $db, 0, 0, false);
                $dbe = new DBCollection("SELECT name FROM Equipment WHERE id=" . $idBolas, $db);
                $cond = "";
                if ($dbe->get("name") == "Branche d'Etranglesaule")
                    $cond = ",id_BasicEquipment = 507, id_EquipmentType=33 ";
                $dbe = new DBCollection(
                    "UPDATE Equipment set x=" . $golem->get("x") . ", y=" . $golem->get("y") . ", room=" . $golem->get("room") . ", inbuilding=" . $golem->get("inbuilding") .
                         ", state='Ground', id_Player=0 " . $cond . " WHERE id=" . $idBolas, $db, 0, 0, false);
            }
        }
        
        $golem->resetExternDBObj();
        $golem->externDBObj("Modifier,Upgrade");
        $golem->deleteDBr($db);
        $dbd = new DBCollection("DELETE FROM MagicSpell WHERE id_Player=" . $idGolem, $db, 0, 0, false);
        $dbd = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Golem de Feu'", $db, 0, 0, false);
        // suppression des bonus exorcisme qu'il a créé
        $dbd = new DBCollection("DELETE FROM BM WHERE BM.id_Player\$src=" . $idGolem . "  AND BM.name='" . addslashes("Exorcisme de l'ombre") . "'", $db, 0, 0, false);
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    /* *********************************************************** LES SORTILEGES ECOLE d'ALTERATION *********************************************** */
    static function curse($playerSrc, $opponent, $characBase, &$param, &$db, $modifier = null)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_CURSE;
        
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $charac = array(
            "magicSkill",
            "speed"
        );
        foreach ($charac as $key => $name) {
            for ($type = 0; $type < DICE_CHARAC; $type ++) {
                if ($modifier != null)
                    $playerSrcModif->addModif($name, $type, $modifier->getModif($name, $type));
            }
        }
        
        $param["PLAYER_MM"] = $playerSrcModif->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrcModif->getScore("speed");
        $param["TARGET_MM"] = $opponent->getScore("magicSkill");
        $param["TYPE_ATTACK"] = SPELL_CURSE_EVENT_FAIL;
        $param["SUCCESS"] = 1;
        $date = gmdate('Y-m-d H:i:s');
        $param["SPELL_LEVEL"] = ceil(((min($param["PLAYER_SPEED"], $param["PLAYER_MM"])) * 1.5 - $param["TARGET_MM"]) / 5);
        if ($param["SPELL_LEVEL"] <= 0) {
            PlayerFactory::Enhance($opponent, MAGICAL_DEFENSE_EVENT_SUCCESS, $param, $db);
            $param["SUCCESS"] = 0;
        } else {
            // Pas de perte de vie donc 3 points de MM pour la résistance comme si réussi
            PlayerFactory::Enhance($opponent, MAGICAL_DEFENSE_EVENT_SUCCESS, $param, $db);
            $param["TYPE_ATTACK"] = SPELL_CURSE_EVENT_SUCCESS;
            switch ($characBase) {
                case 1:
                    $id_StaticModifier = 6;
                    $param["CHARAC_NAME"] = "DEXTÉRITÉ";
                    break;
                case 2:
                    $id_StaticModifier = 7;
                    $param["CHARAC_NAME"] = "FORCE";
                    break;
                case 3:
                    $id_StaticModifier = 8;
                    $param["CHARAC_NAME"] = "VITESSE";
                    break;
                case 4:
                    $id_StaticModifier = - 1;
                    $param["CHARAC_NAME"] = "ENCHANTEMENTS";
                    break;
                default:
                    $id_StaticModifier = 6;
                    $param["CHARAC_NAME"] = "DEXTÉRITÉ";
                    break;
            }
            
            if ($characBase != 4) {
                $bm = new BM();
                $bm->set("level", $param["SPELL_LEVEL"]);
                $bm->set("effect", "NEGATIVE");
                $bm->set("value", 0);
                $bm->set("name", "Malédiction d'Arcxos");
                $bm->set("life", "3");
                $bm->set("id_Player\$src", $playerSrc->get("id"));
                $bm->set("date", gmdate("Y-m-d H:i:s"));
                $bm->set("id_StaticModifier", $id_StaticModifier);
                $bm->set("id_Player", $opponent->get("id"));
                $opponent->addBM($bm, 3, $param, $db);
                PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            } else {
                $dbb = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Bénédiction'", $db);
                if (! $dbb->eof()) {
                    $param["LEVEL_BENEDICTION"] = $dbb->get("level");
                    if ($param["LEVEL_BENEDICTION"] >= $param["LEVEL_BM"])
                        $param["CANCEL"] = 1;
                    else {
                        $param["SPELL_LEVEL_FINAL"] = $param["SPELL_LEVEL"] - $param["LEVEL_BENEDICTION"];
                    }
                } else {
                    $param["SPELL_LEVEL_FINAL"] = $param["SPELL_LEVEL"];
                }
                $dbbm = new DBCollection(
                    "UPDATE BM SET value=value-" . $param["SPELL_LEVEL_FINAL"] . ", level = level-" . $param["SPELL_LEVEL_FINAL"] . " WHERE effect='POSITIVE' AND id_Player=" .
                         $opponent->get("id") . " 
                    AND name in ('Ailes de Colère', 'Armure d''Athlan','Barrière enflammée','Bouclier magique','Bulle de vie','Régénération' )", $db);
                if ($playerSrc->get("id") == $opponent->get("id")) {
                    $param["SELF"] = 1;
                    PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
                } else {
                    $param["SELF"] = 0;
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                    $opponent->updateDBr($db);
                }
            }
            // $opponent->updateDBr($db);
            
            /*
             * $dbbm = new DBCollection("SELECT id FROM BM WHERE id_StaticModifier_BM=".$id_StaticModifier." AND id_Player=".$opponent->get("id")." AND name='Malédiction d\'Arcxos'", $db);
             * if(!$dbbm->eof())
             * $dbu = new DBCollection("UPDATE BM SET id_StaticModifier_BM=".$id_StaticModifier.", life=2, level=".$param["SPELL_LEVEL"]." WHERE id_Player=".$opponent->get("id")." AND name='Malédiction d\'Arcxos'", $db,0,0,false);
             * else
             * $dbi=new DBCollection("INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (".$id_StaticModifier.",".$opponent->get("id").",".$playerSrc->get("id").",".$param["SPELL_LEVEL"].", 'NEGATIVE', 'Malédiction d\'Arcxos' ,2,'".$date."')",$db,0,0,false);
             *
             * PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
             *
             * $opponent->updateDBr($db);
             *
             */
            $param["XP"] += 1;
            if ($opponent->get("status") == "NPC") {
                require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                PNJFactory::runIA($opponent, $playerSrc->get("id"), $db);
            }
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        
        if ($playerSrc->get("status") == 'PC') {
            BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 4);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function removeAnger(&$playerSrc, $charac, &$param, &$db)
    {
        // Initialisation des params infos player
        BasicActionFactory::globalInfo($playerSrc, $param);
        if ($charac == 1) {
            $id_StaticModifier = 4;
            $param["CHARAC_NAME"] = "Attaque";
        } else {
            $id_StaticModifier = 5;
            $param["CHARAC_NAME"] = "Dégât";
        }
        $dbi = new DBCollection("delete from BM where id_StaticModifier_BM=" . $id_StaticModifier . " and id_Player=" . $playerSrc->get("id") . " and name='Ailes de Colère' ", $db, 
            0, 0, false);
        if ($playerSrc->get("status") == "PC") {
            $playerSrc->getObj("Modifier")->update();
        } else {
            $playerSrc->loadExternDBObj($db);
        }
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function anger($playerSrc, $opponent, $charac, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_ANGER;
        $param["TYPE_ATTACK"] = SPELL_ANGER_EVENT;
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $date = gmdate('Y-m-d H:i:s');
        if ($charac == 1) {
            $id_StaticModifier = 4;
            $param["CHARAC_NAME"] = "Attaque";
        } else {
            $id_StaticModifier = 5;
            $param["CHARAC_NAME"] = "Dégât";
        }
        
        // $param["SPELL_LEVEL"]= ceil(sqrt($param["PLAYER_SPEED"]*$param["PLAYER_MM"])/4);
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        
        $dbbm = new DBCollection("SELECT id FROM BM WHERE id_StaticModifier_BM=" . $id_StaticModifier . " AND id_Player=" . $opponent->get("id") . " AND name='Ailes de Colère'", 
            $db);
        if (! $dbbm->eof())
            $dbu = new DBCollection(
                "UPDATE BM SET id_StaticModifier_BM=" . $id_StaticModifier . ", life=3, level=" . $param["SPELL_LEVEL"] . " WHERE id_StaticModifier_BM=" . $id_StaticModifier .
                     " AND id_Player=" . $opponent->get("id") . " AND name='Ailes de Colère'", $db, 0, 0, false);
        else
            $dbi = new DBCollection(
                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, name,life,date) VALUES (" . $id_StaticModifier . "," . $opponent->get("id") . "," .
                     $playerSrc->get("id") . "," . $param["SPELL_LEVEL"] . ",'Ailes de Colère' ,3,'" . $date . "')", $db, 0, 0, false);
        
        if ($playerSrc->get("id") == $opponent->get("id")) {
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 1;
        } else {
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $param["SELF"] = 0;
            $opponent->updateDBr($db);
        }
        
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        if ($playerSrc->get("status") == "PC") {
            $chance = mt_rand(0, 1);
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2.5);
            }
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function teleport($playerSrc, $x, $y, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_TELEPORT;
        $param["X"] = $x;
        $param["Y"] = $y;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        
        require_once (HOMEPATH . "/lib/MapInfo.inc.php");
        $currmap = $playerSrc->get("map");
        $mapinfo = new MapInfo($db);
        
        $param["SUCCESS"] = 0;
        
        if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $x, $y) <= $param["SPELL_LEVEL"]) {
            $validzone = $mapinfo->getValidMap($x, $y, 0, $currmap);
            if ($param["CASE_BUSY"] || ! $validzone[0][0]) {
                BasicActionFactory::autokill($playerSrc, $param, $db);
                $param["TYPE_ATTACK"] = SPELL_TELEPORT_EVENT_DEATH;
                $playerSrc->reload($db);
                $param["XP"] -= 1;
            } else {
                $param["TYPE_ATTACK"] = SPELL_TELEPORT_EVENT_SUCCESS;
                $param["SUCCESS"] = 1;
                $playerSrc->set("x", $x);
                $playerSrc->set("y", $y);
                $playerSrc->updateHidden($db);
                $playerSrc->set("inbuilding", 0);
                $playerSrc->set("room", 0);
                $param["XP"] += 1;
                BasicActionFactory::updateInfoAfterMove($x, $y, $playerSrc, $db);
                $mapinfo->updateWayCount($x, $y, $currmap, $playerSrc);
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                
                $idCity = InsideBuildingFactory::getCityId($playerSrc, $db);
                if ($idCity > 0) {
                    if (BasicActionFactory::canEnterCity($playerSrc, $idCity, $db) == 0) {
                        if (mt_rand(0, 1) > 0) {
                            $city = new City();
                            $city->load($idCity, $db);
                            $subject = "Un intrus est apparu dans " . $city->get("name");
                            $body = "Une personne non authorisée à accèder au village " . $city->get("name") . " située en " . $city->get("x") . "/" . $city->get("y") .
                                 " s'y est téléporté.";
                            InsideBuildingFactory::informCityMember($idCity, $subject, $body, $db);
                        }
                    }
                }
            }
        } else {
            $param["TYPE_ATTACK"] = SPELL_TELEPORT_EVENT_FAIL;
            $param["XP"] -= 1;
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        if ($playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 4);
        }
        $playerSrc->updateHidden($db);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function massiveTP($playerSrc, $x, $y, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = S_MASSIVEINSTANTTP;
        $param["X"] = $x;
        $param["Y"] = $y;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        $param["TEMPLE_LEVEL"] = 0;
        $dbct = new DBCollection("SELECT level,id_City FROM Building WHERE  id_BasicBuilding=14 and id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        if ($dbct->count() > 0) {
            $param["SPELL_LEVEL"] += 50 + 30 * $dbct->get("level");
            $param["TEMPLE_LEVEL"] = $dbct->get("level");
        }
        require_once (HOMEPATH . "/lib/MapInfo.inc.php");
        $currmap = $playerSrc->get("map");
        
        $param["SUCCESS"] = 0;
        
        if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $x, $y) <= $param["SPELL_LEVEL"]) {
            $mapinfo = new MapInfo($db);
            $param["SUCCESS"] = 1;
            
            $xp = $playerSrc->get("x");
            $yp = $playerSrc->get("y");
            
            $dbc = new DBCollection(
                "SELECT p1.* FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" . ID_BASIC_RACE_FEU_FOL . ") WHERE p1.map=" . $currmap .
                     " AND (abs(p1.x-" . $xp . ") + abs(p1.y-" . $yp . ") + abs(p1.x+p1.y-" . $xp . "-" . $yp . "))/2 <=8 AND ((" . $playerSrc->get("id_FighterGroup") .
                     " > 0 and (p1.id_FighterGroup=" . $playerSrc->get("id_FighterGroup") . " or p2.id_FighterGroup=" . $playerSrc->get("id_FighterGroup") . ") or p1.id = " .
                     $playerSrc->get("id") . " or p2.id = " . $playerSrc->get("id") . " ))", $db);
            
            $pjTeleporte = array();
            $i = 0;
            while (! $dbc->eof()) {
                if ($dbc->get("id") != $playerSrc->get("id")) {
                    $state = $dbc->get("state");
                    if ($dbc->get("disabled") == 0 && $state != "questionning" && $state != "prison" && ! $dbc->get("recall")) {
                        
                        $opponent = new Player();
                        $opponent->externDBObj("Modifier");
                        $opponent->load($dbc->get("id"), $db);
                        
                        BasicActionFactory::globalInfoOpponent($opponent, $param);
                        
                        $place = BasicActionFactory::getAleaFreePlace($x, $y, $playerSrc->get("map"), 5, $db, 1);
                        if ($place != 0) {
                            $i ++;
                            $opponent->set("x", $place["x"]);
                            $opponent->set("y", $place["y"]);
                            $pjTeleporte[$i]["id"] = $dbc->get("id");
                            $pjTeleporte[$i]["name"] = $dbc->get("name");
                            $pjTeleporte[$i]["x"] = $place["x"];
                            $pjTeleporte[$i]["y"] = $place["y"];
                        } else {
                            break;
                        }
                        BasicActionFactory::updateInfoAfterMove($opponent->get("x"), $opponent->get("y"), $opponent, $db);
                        $mapinfo->updateWayCount($opponent->get("x"), $opponent->get("y"), $currmap, $opponent);
                        $opponent->updateHidden($db);
                        $opponent->updateDBr($db);
                        
                        $param["ERROR"] = 0;
                        $param["TYPE_ACTION"] = PASSIVE_MASSIVE_TELEPORTATION;
                        $param["TYPE_ATTACK"] = PASSIVE_MASSIVE_TELEPORTATION_EVENT;
                        $param["X_DEST"] = $pjTeleporte[$i]["x"];
                        $param["Y_DEST"] = $pjTeleporte[$i]["y"];
                        Event::logAction($db, PASSIVE_MASSIVE_TELEPORTATION, $param);
                        
                        $idCity = InsideBuildingFactory::getCityId($opponent, $db);
                        if ($idCity > 0) {
                            if (BasicActionFactory::canEnterCity($opponent, $idCity, $db) == 0) {
                                if (mt_rand(0, 1) > 0) {
                                    $city = new City();
                                    $city->load($idCity, $db);
                                    $subject = "Un intrus est apparu dans " . $city->get("name");
                                    $body = "Une personne non authorisée à accèder au village " . $city->get("name") . " située en " . $city->get("x") . "/" . $city->get("y") .
                                         " s'y est téléporté.";
                                    InsideBuildingFactory::informCityMember($idCity, $subject, $body, $db);
                                }
                            }
                        }
                    }
                }
                $dbc->next();
            }
            $param["TYPE_ATTACK"] = SPELL_MASSIVE_TELEPORT_EVENT_SUCCESS;
            $param["NB"] = $i;
            $param["XP"] += 1;
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $param["RESULT_PJ"] = $pjTeleporte;
            unset($pjTeleporte);
        } else {
            $param["TYPE_ATTACK"] = SPELL_MASSIVE_TELEPORT_EVENT_FAIL;
            $param["XP"] -= 1;
        }
        
        $param["TYPE_ACTION"] = S_MASSIVEINSTANTTP;
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        if ($playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 6);
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 6);
        }
        $playerSrc->updateHidden($db);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function soul($playerSrc, $x, $y, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SPELL_SOUL;
        $param["X"] = $x;
        $param["Y"] = $y;
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        
        $param["SUCCESS"] = 0;
        
        $isCaseFree = InteractionChecking::caseFree($x, $y, $playerSrc->get("map"), $db);
        if (! $isCaseFree) {
            $place = BasicActionFactory::getAleaFreePlace($x, $y, $playerSrc->get("map"), 3, $db, 1);
            if ($place != 0) {
                $x = $place["x"];
                $y = $place["y"];
                $isCaseFree = true;
            } else {
                $isCaseFree = true;
            }
        }
        $mapinfo = new MapInfo($db);
        $validzone = $mapinfo->getValidMap($x, $y, 0, $playerSrc->get("map"));
        if ($validzone[0][0] && distHexa($playerSrc->get("x"), $playerSrc->get("y"), $x, $y) <= $param["SPELL_LEVEL"] * 3) {
            $param["TYPE_ATTACK"] = SPELL_SOUL_EVENT_SUCCESS;
            $param["SUCCESS"] = 1;
            $npc = PNJFactory::getPNJFromLevel(112, 1, $db);
            $npc->set("x", $x);
            $npc->set("y", $y);
            $npc->set("racename", "Projection de l'âme");
            $npc->set("map", $playerSrc->get("map"));
            $npc->set("hidden", $npc->isInCity($db) * 10 + 3);
            $npc->set("id_Member", $playerSrc->get("id_Member"));
            $npc->set("id_Owner", $playerSrc->get("id"));
            $npc->addDBr($db);
            // $param["XP"] +=1;
            
            $date = gmdate('Y-m-d H:i:s');
            $dbi = new DBCollection(
                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (18," . $playerSrc->get("id") . "," . $playerSrc->get("id") .
                     ", 1 , 'POSITIVE', '" . addslashes("Projection de l'âme") . "' ,-2,'" . $date . "')", $db, 0, 0, false);
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        } else {
            $param["TYPE_ATTACK"] = SPELL_SOUL_EVENT_FAIL;
            $param["XP"] -= 1;
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        if ($playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2.5);
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function revokeSoul($playerSrc, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params infos player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = REVOKE_SOUL;
        // $param["TYPE_ATTACK"] = REVOKE_GOLEM_EVENT; Pas de log, me parait pas nécéssaire
        
        $dbd = new DBCollection("SELECT id FROM Player WHERE id_Member=" . $playerSrc->get("id_Member") . " AND id_BasicRace = 112", $db);
        if ($dbd->count() > 0) {
            $soul = new Player();
            $soul->load($dbd->get("id"), $db);
            $soul->resetExternDBObj();
            $soul->externDBObj("Modifier,Upgrade");
            $soul->deleteDBr($db);
        }
        
        $dbd = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='" . addslashes("Projection de l'âme") . "'", $db, 0, 0, false);
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        
        $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    static function lightTouch($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_LIGHTTOUCH;
        $param["TYPE_ATTACK"] = SPELL_LIGHTTOUCH_EVENT;
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        
        $param["AP_GAIN"] = min(0.2 * $param["SPELL_LEVEL"], 12 + floor($opponent->getObj("Modifier")->getModif("speed", DICE_D6) / 10) - $opponent->get("ap"));
        $opponent->set("ap", $opponent->get("ap") + $param["AP_GAIN"]);
        
        $date = gmdate('Y-m-d H:i:s');
        $dbi = new DBCollection(
            "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (21," . $playerSrc->get("id") . "," . $playerSrc->get("id") .
                 ", 1 , 'POSITIVE', '" . addslashes("Toucher de lumière") . "' ,1,'" . $date . "')", $db, 0, 0, false);
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        $chance = mt_rand(0, 1);
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0 && $chance)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        if ($playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2.5);
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2.5);
        }
        $playerSrc->updateDBr($db);
        $opponent->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function recall($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = SPELL_RECALL;
        $param["SUCCESS"] = 0;
        $date = gmdate('Y-m-d H:i:s');
        
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $param["SPELL_LEVEL"] = floor(min($param["PLAYER_SPEED"], $param["PLAYER_MM"]) / 3.5);
        
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        $xo = $opponent->get("x");
        $yo = $opponent->get("y");
        
        if (distHexa($xp, $yp, $xo, $yo) > $param["SPELL_LEVEL"] * 3)
            $param["TYPE_ATTACK"] = SPELL_RECALL_EVENT_FAIL;
        else {
            $param["TYPE_ATTACK"] = SPELL_RECALL_EVENT_SUCCESS;
            $param["SUCCESS"] = 1;
            $dest = array();
            $dest[$opponent->get("name")] = $opponent->get("name");
            require_once (HOMEPATH . "/factory/MailFactory.inc.php");
            MailFactory::sendSingleDestMsg($playerSrc, localize("Offre de rappel"), 
                localize($playerSrc->get("name") . " a réussi un sort de rappel sur vous. A votre prochain tour, vous aurez la possibilité de l'accepter ou de la refuser."), $dest, 
                2, $err2, $db, - 1, 0);
            
            // if(!$dbc->eof())
            // $dbu = new DBCollection("UPDATE BM SET life=1, level=".$param["SPELL_LEVEL"]." WHERE id_Player=".$opponent->get("id")." AND name='Armure d\'Atlhan'", $db,0,0,false);
            // else
            $dbi = new DBCollection(
                "INSERT INTO BM (id_Player,id_Player\$src,level, effect, name,life,date) VALUES (" . $opponent->get("id") . "," . $playerSrc->get("id") . "," . $param["SPELL_LEVEL"] .
                     ", 'POSITIVE', 'Rappel' ,1,'" . $date . "')", $db, 0, 0, false);
            
            $param["XP"] += 1;
            
            $idCity = InsideBuildingFactory::getCityId($playerSrc, $db);
            if ($idCity > 0) {
                if (BasicActionFactory::canEnterCity($playerSrc, $idCity, $db) == 0) {
                    if (mt_rand(0, 1) > 0) {
                        $city = new City();
                        $city->load($idCity, $db);
                        $subject = "Un intrus est appelé dans " . $city->get("name");
                        $body = "Une personne non authorisée à accèder au village " . $city->get("name") . " située en " . $city->get("x") . "/" . $city->get("y") .
                             " a été invitée à venir s'y introduire frauduleusement.";
                        InsideBuildingFactory::informCityMember($idCity, $subject, $body, $db);
                    }
                }
            }
        }
        
        if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0)
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
        if ($playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 6);
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 6);
        }
        $opponent->updateHidden($db);
        $playerSrc->updateDBr($db);
        $opponent->updateDBr($db);
        return ACTION_NO_ERROR;
    }
}
?>
