<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");
require_once (HOMEPATH . "/class/BM.inc.php");
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");

class AbilityFactory
{

    /* *************************************************************** FONCTIONS AUXILLIAIRES **************************************** */
    static protected function modifierScoreToArray ($eqmodifier, &$param)
    {
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getScoreWithNoBM($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function modifierToArray ($eqmodifier, &$param)
    {
        $eqmodifier->initCharacStr();
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getModifStr($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function getScore ($baseap, $success, &$param, $malus = 0)
    {
        $param["ENHANCE"] = 0;
        $param["PERCENT"] = $success - $malus;
        $param["SCORE"] = mt_rand(1, 100);
        $param["SUCCESS"] = 0;
        $param["SCOREB"] = 0;
        
        if ($param["SCORE"] <= $success - $malus) {
            $param["SUCCESS"] = 1;
            $param["SCOREB"] = mt_rand(1, 100);
            if ($param["SCOREB"] > $success)
                $param["ENHANCE"] = 1;
        }
        
        if ($baseap > 2 && ! $param["SUCCESS"])
            $baseap = min($baseap, 3);
        
        $param["AP"] = $baseap;
    }

    static function ability ($playerSrc, $ident, $malus, &$param, &$param2, $db)
    {
        BasicActionFactory::globalInfo($playerSrc, $param2);
        $dbc = new DBCollection("SELECT Ability.id, BasicAbility.modifierPA, BasicAbility.level, BasicAbility.timeAttack, Ability.skill FROM Ability LEFT JOIN BasicAbility ON Ability.id_BasicAbility=BasicAbility.id WHERE BasicAbility.id=" . ($ident - 100) . " AND id_Player=" . $playerSrc->get("id"), $db);
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
        if (! $dbc->eof()) {
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
            if (! $dbbm->eof()) {
                $malus += - $dbbm->get("level");
                $param["MALUS"] = - $malus;
            }
            $abilityap = $dbc->get("modifierPA");
            if ($dbc->get("timeAttack") == 'YES')
                $abilityap += $playerSrc->getScore("timeAttack");
            $success = $dbc->get("skill");
            self::getScore($abilityap, $success, $param, $malus);
            
            if ($playerSrc->get("modeAnimation") == 1) {
                $param["ENHANCE"] = 0;
            }
            
            if ($success < 90 && $param["ENHANCE"] > 0) {
                $newsucess = min($success + $param["ENHANCE"], 90);
                $dbu = new DBCollection("UPDATE Ability SET skill=" . $newsucess . " WHERE id=" . $dbc->get("id") . " AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            } else {
                if ($success >= 90)
                    $param["SCOREB"] = 0;
            }
            
            if (! $param["SUCCESS"] && $success < 90) {
                $bonus = floor((100 - $param["PERCENT"]) / ($dbc->get("level") + 2));
                $date = gmdate('Y-m-d H:i:s');
                $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
                if (! $dbbm->eof())
                    $dbbm = new DBCollection("UPDATE BM SET level=level+" . $bonus . " WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db, 0, 0, false);
                else
                    $dbi = new DBCollection("INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $playerSrc->get("id") . "," . $playerSrc->get("id") . "," . $bonus . "," . $ident . ", 'POSITIVE', 'Concentration' ,0,'" . $date . "')", $db, 0, 0, false);
            } else
                $dbu = new DBCollection("DELETE FROM BM WHERE name='Concentration' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            
            return ACTION_NO_ERROR;
        }
        return ABILITY_ERROR;
    }

    /* ************************************************************* AIGUILLEURS COMP D'ATTAQUE AVEC OPPOSANTS ************************************* */
    static function abilityAttack ($playerSrc, $ident, $opponent, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        $modifier = new Modifier();
        $param["IDENT"] = $ident;
        switch ($ident) {
            case ABILITY_DAMAGE:
                $modifier->setModif("damage", DICE_D6, (floor(($playerSrc->getModif("damage", DICE_D6) + $playerSrc->getModif("damage_bm", DICE_D6)) * 0.2)) + 2);
                $modifier->setModif("attack", DICE_D6, - floor(($playerSrc->getModif("attack", DICE_D6) + $playerSrc->getModif("attack_bm", DICE_D6)) * 0.2));
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, $modifier, null, $ident);
                break;
            case ABILITY_POWERFUL:
                $modifier->setModif("attack", DICE_D6, (floor(($playerSrc->getModif("damage", DICE_D6) + $playerSrc->getModif("damage_bm", DICE_D6)) * 0.3)) + 3);
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, $modifier, null, $ident);
                break;
            case ABILITY_TREACHEROUS:
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, $modifier, null, $ident);
                break;
            case ABILITY_THRUST:
                if ($opponent->get("status") == "PC")
                    $modifier->setModif("armor", DICE_ADD, - (floor($opponent->getModif("armor_bm", DICE_ADD) * 0.8)));
                else
                    $modifier->setModif("armor", DICE_ADD, - (floor($opponent->getModif("armor", DICE_ADD) * 0.8)));
                $modifier2 = new Modifier();
                $modifier2->setModif("damage", DICE_D6, - (floor(($playerSrc->getModif("damage", DICE_D6) + $playerSrc->getModif("damage_bm", DICE_D6)) * 0.3)));
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, $modifier2, $modifier, $ident);
                break;
            case ABILITY_STUNNED:
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, null, null, $ident);
                break;
            case ABILITY_KNOCKOUT:
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, null, null, $ident);
                break;
            case ABILITY_LIGHT:
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, null, null, $ident);
                break;
            case ABILITY_PROJECTION:
                BasicActionFactory::attack($playerSrc, $opponent, $param, $db, null, null, $ident);
                break;
            case ABILITY_DISARM:
                self::disarm($playerSrc, $opponent, $param, $db);
                break;
            case ABILITY_STEAL:
                self::steal($playerSrc, $opponent, $param, $db);
                break;
            case ABILITY_LARCENY:
                self::larceny($playerSrc, $opponent, $param, $db);
                break;
            case ABILITY_PROTECTION:
                self::protection($playerSrc, $opponent, $param, $db);
                break;
            case ABILITY_FIRSTAID:
                self::firstaid($playerSrc, $opponent, $param, $db);
                break;
            default:
                break;
        }
    }

    /* ************************************************************* AIGUILLEURS COMP D'ATTAQUE SANS OPPOSANT ************************************* */
    static function abilityWithoutTarget ($playerSrc, $ident, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        $modifier = new Modifier();
        $param["IDENT"] = $ident;
        switch ($ident) {
            case ABILITY_GUARD:
                self::guard($playerSrc, $param, $db);
                break;
            case ABILITY_RUN:
                self::run($playerSrc, $param, $db);
                break;
            case ABILITY_AUTOREGEN:
                self::autoRegen($playerSrc, $param, $db);
                break;
            case ABILITY_BRAVERY:
                self::bravery($playerSrc, $param, $db);
                break;
            case ABILITY_RESISTANCE:
                self::resistance($playerSrc, $param, $db);
                break;
            case ABILITY_EXORCISM:
                self::exorcism($playerSrc, $param, $db);
                break;
            case ABILITY_AMBUSH:
                self::ambush($playerSrc, $param, $db);
                break;
            default:
                break;
        }
    }

    /* *********************************************************** LES COMPETENCES DE BASES *********************************************** */
    static function firstAid ($playerSrc, $opponent, &$param, &$db, $bonus = 0)
    {
        // Initialisation des params info de player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = ABILITY_FIRSTAID;
        $param["TYPE_ATTACK"] = ABILITY_FIRSTAID_EVENT;
        
        // Effet
        $param["PLAYER_DEXTERITY"] = $playerSrc->getScore("dexterity");
        $param["LEVEL_COMP"] = floor($param["PLAYER_DEXTERITY"] / 8) + 1;
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, 0.5 * $param["LEVEL_COMP"]);
        $param["HP_POTENTIEL"] = $modifier->getScoreWithNoBM("hp");
        $param["HP"] = min($param["HP_POTENTIEL"], max(0, ceil(0.75 * $opponent->getObj("Modifier")->getModif("hp", DICE_ADD)) - $opponent->get("currhp")));
        $param["SELF"] = 0;
        if ($playerSrc->get("id") == $opponent->get("id")) {
            $param["SELF"] = 1;
            $param["HP_HEAL"] = PlayerFactory::addHP($playerSrc, $param["HP"], $db, 0);
        } else {
            $param["HP_HEAL"] = PlayerFactory::addHP($opponent, $param["HP"], $db, 0);
            $opponent->updateDB($db);
        }
        // $param["XP"] +=1;
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 5);
        }
    }

    static function guard ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_GUARD;
        $param["TYPE_ATTACK"] = ABILITY_GUARD_EVENT;
        
        // Effet
        $date = gmdate("Y-m-d H:i:s");
        $param["PLAYER_DEFENSE"] = $playerSrc->getScoreWithNoBM("defense");
        $param["GAIN_DEFENSE"] = (floor($param["PLAYER_DEFENSE"] / 8) + 1) / 2;
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Garde'", $db);
        $date = gmdate('Y-m-d H:i:s');
        if (! $dbc->eof())
            $dbu = new DBCollection("UPDATE BM SET life=1, level=" . ($param["GAIN_DEFENSE"] * 2) . " WHERE id_Player=" . $playerSrc->get("id") . " AND name='Garde'", $db, 0, 0, false);
        else
            $dbi = new DBCollection("INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (10," . $playerSrc->get("id") . "," . $playerSrc->get("id") . "," . ($param["GAIN_DEFENSE"] * 2) . ", 'POSITIVE', 'Garde' ,1,'" . $date . "')", $db, 0, 0, false);
        
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        // $param["XP"] +=1;
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        PlayerFactory::Enhance($playerSrc, ABILITY_GUARD_EVENT, $param, $db);
    }

    /* *********************************************************** LES COMPETENCES DE l'ECOLE DE LA GARDE D'OCTOBIAN *********************************************** */
    static function twirl ($playerSrc, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_TWIRL;
        $param["TYPE_ATTACK"] = ABILITY_TWIRL_EVENT;
        
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        $param["TARGET_DAMAGE_T"] = floor($playerSrc->getScore("damage") * 0.8);
        
        $dbe = new DBCollection("SELECT id FROM Player WHERE map=" . $playerSrc->get("map") . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2=1 AND inbuilding=0 AND (hidden=0 or hidden=10) AND disabled=0", $db);
        $param["NUMBER_OPPONENT"] = 0;
        $i = 0;
        
        $isPJHit = false;
        
        while (! $dbe->eof()) {
            if ($playerSrc->canSeePlayerById($dbe->get("id"), $db)) {
                $param["NUMBER_OPPONENT"] += 1;
                $opp = new Player();
                $opp->externDBObj("Modifier");
                $ret = $opp->load($dbe->get("id"), $db);
                
                $i ++;
                $param2 = array();
                
                $param["PROTECTION"][$i] = 0;
                $param2["XP"] = 0;
                // Si la cible est défendu
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opp->get("id") . " AND name='défendu'", $db);
                if (! $dbc->eof()) {
                    $protecteur = new Player();
                    $protecteur->externDBObj("Modifier");
                    $protecteur->externDBObj("Modifier");
                    $protecteur->load($dbc->get("id_Player\$src"), $db);
                    $param["INTER_NAME"][$i] = $opp->get("name");
                    $param["INTER_ID"][$i] = $opp->get("id");
                    if ($protecteur->get("state") != "turtle" && ((distHexa($opp->get("x"), $opp->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 1 && $protecteur->get("inbuilding") == $opp->get("inbuilding")) || ((distHexa($opp->get("x"), $opp->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 0) && $protecteur->get("room") == $opp->get("room"))))
                        $param["PROTECTION"][$i] = 1;
                    
                    if ($protecteur->get("id") == $playerSrc->get("id"))
                        $param["PROTECTION"][$i] = 0;
                }
                if ($param["PROTECTION"][$i]) {
                    $param2["INTER_NAME"] = $param["INTER_NAME"][$i];
                    self::hit2($playerSrc, $param["PLAYER_ATTACK"], $param["TARGET_DAMAGE_T"], $protecteur, $param2, $db, $opp);
                    $param["TARGET_MM"][$i] = $param2["TARGET_MM"];
                    $param["ARMOR_BONUS"][$i] = $param2["ARMOR_BONUS"];
                    $isPJHit = true;
                } else {
                    $param2["BUBBLE"] = 0;
                    self::hit($playerSrc, $param["PLAYER_ATTACK"], $param["TARGET_DAMAGE_T"], $opp, $param2, $db);
                    $param["TARGET_DEFENSE"][$i] = $param2["TARGET_DEFENSE"];
                    
                    if ($param2["TYPE_ATTACK"] != ATTACK_EVENT_FAIL) {
                        $isPJHit = true;
                    }
                }
                
                if ($param2["BUBBLE"]) {
                    $param["BUBBLE_LIFE2"][$i] = $param2["BUBBLE_LIFE"];
                    $param["BUBBLE_CANCEL2"][$i] = $param2["BUBBLE_CANCEL"];
                    $param["BUBBLE_LEVEL2"][$i] = $param2["BUBBLE_LEVEL"];
                }
                if (isset($param2["BARRIER_LEVEL"])) {
                    $param["BARRIER_LEVEL"][$i] = $param2["BARRIER_LEVEL"];
                    $param["PLAYER_DAMAGE"][$i] = $param2["PLAYER_DAMAGE"];
                }
                $param["PLAYER_KILLED"][$i] = $param2["PLAYER_KILLED"];
                
                // Pour l'affichage de la distribution des px
                if ($param2["TARGET_KILLED"]) {
                    $param["NOMBRE"] = $param2["NOMBRE"];
                    if (! isset($param["PLAYER" . 1])) {
                        for ($j = 1; $j < $param2["NOMBRE"]; $j ++) {
                            $param["PLAYER" . $j] = $param2["PLAYER" . $j];
                            if (isset($param["GAIN" . $j]))
                                $param["GAIN" . $j] += $param2["GAIN" . $j];
                            else
                                $param["GAIN" . $j] = $param2["GAIN" . $j];
                        }
                    } else {
                        for ($j = 1; $j < $param2["NOMBRE"]; $j ++) {
                            for ($k = 1; $k < $param2["NOMBRE"]; $k ++) {
                                if ($param["PLAYER" . $k] == $param2["PLAYER" . $j]) {
                                    if (isset($param["GAIN" . $k]))
                                        $param["GAIN" . $k] += $param2["GAIN" . $j];
                                    else
                                        $param["GAIN" . $k] = $param2["GAIN" . $j];
                                }
                            }
                        }
                    }
                    
                    // Param pour le fallmsg
                    if (isset($param2["EQUIPMENT_FALL"]))
                        $param["EQUIPMENT_FALL"][$i] = $param2["EQUIPMENT_FALL"];
                    
                    if (isset($param2["MONEY_FALL"]))
                        $param["MONEY_FALL"][$i] = $param2["MONEY_FALL"];
                    
                    if (isset($param2["DROP_NAME"]))
                        $param["DROP_NAME"][$i] = $param2["DROP_NAME"];
                    
                    if (isset($param2["LARCIN"]))
                        $param["LARCIN"][$i] = $param2["LARCIN"];
                }
                
                $param["TARGET_NAME"][$i] = $param2["TARGET_NAME"];
                $param["TARGET_ID_TWIRL"][$i] = $param2["TARGET_ID"];
                $param["TARGET_GENDER"][$i] = $param2["TARGET_GENDER"];
                $param["TARGET_HP"][$i] = $param2["TARGET_HP"];
                $param["TARGET_KILLED"][$i] = $param2["TARGET_KILLED"];
                $param["XP"] += $param2["XP"];
                
                if (isset($param2["PLAYER_KILLED"]) && $param2["PLAYER_KILLED"]) {
                    break;
                }
                
                unset($param2);
                $dbe->next();
            }
        }
        
        if ($isPJHit) { // au moins 1 PJ a été touché ajout du PX de touche
            $param["XP"] += 1;
        }
        
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        PlayerFactory::Enhance($playerSrc, ABILITY_TWIRL, $param, $db);
        $param["TYPE_ACTION"] = ABILITY_TWIRL;
        $param["TYPE_ATTACK"] = ABILITY_TWIRL_EVENT;
        
        SpellFactory::removeAnger($playerSrc, 1, $param, $db);
        SpellFactory::removeAnger($playerSrc, 2, $param, $db);
    }

    static function hit ($playerSrc, $attack, $damage, $opponent, &$param, &$db)
    {
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["XP"] = 0;
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        $param["TARGET_HP"] = 0;
        $param["TARGET_KILLED"] = 0;
        $param["TYPE_ACTION"] = PASSIVE_TWIRL;
        
        SpellFactory::passiveBarrier($playerSrc, $opponent, $param, $db);
        if ($param["PLAYER_KILLED"])
            $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
        else {
            
            // Attaque réussi
            if ($attack >= $param["TARGET_DEFENSE"] + 1) {
                $param["TARGET_DAMAGETOT2"] = $damage;
                $param["TARGET_DAMAGE"] = $damage;
                require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
                $param["BUBBLE"] = SpellFactory::PassiveBubble($opponent, $param, $db);
                $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - $opponent->getScore("armor"), 0);
                $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
                if (! $dbc->eof()) {
                    $opponent->set("currhp", $opponent->get("currhp") + 10 * $dbc->get("level"));
                    $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db, 0, 0, false);
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param2, $db, 0);
                    $opponent->updateDBr($db);
                    $param["PIEGE"] = 1;
                }
                $hp = $opponent->get("currhp") - $param["TARGET_HP"];
                if ($param["TARGET_HP"] > 0)
                    PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                    
                    // Mort de la cible
                if ($hp <= 0)
                    BasicActionFactory::kill($playerSrc, $opponent, $param, $db);
                else {
                    if ($playerSrc->get("status") == 'PC')
                        BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
                    $param["TYPE_ATTACK"] = ATTACK_EVENT_SUCCESS;
                    
                    PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_HP"], $param, $db);
                    
                    if ($opponent->get("status") == "NPC") {
                        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                        PNJFactory::runIA($opponent, $playerSrc->get("id"), $db);
                    }
                }
            } else { // Attaque raté
                if ($playerSrc->get("status") == 'PC')
                    BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
                $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            }
        }
        require_once (HOMEPATH . "/class/Event.inc.php");
        $param["ERROR"] = 0;
        
        $param["PLAYER_ATTACK"] = $attack;
        $param["TARGET_DAMAGE_T"] = $damage;
        $param["PROTECTION"] = 0;
        
        Event::logAction($db, PASSIVE_TWIRL, $param);
    }

    static function hit2 ($playerSrc, $attack, $damage, $protecteur, &$param, &$db, $opponent)
    {
        BasicActionFactory::autoHustleInterposition($protecteur, $opponent, $db);
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($protecteur, $param);
        $param["XP"] = 0;
        
        $param["TARGET_MM"] = $protecteur->getScore("magicSkill");
        $param["ARMOR_BONUS"] = 2 * floor(1 + $param["TARGET_MM"] / 8);
        $param["TARGET_DAMAGETOT2"] = $damage;
        $param["TARGET_DAMAGE"] = $damage;
        $param["BUBBLE"] = SpellFactory::PassiveBubble($protecteur, $param, $db);
        $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - ($protecteur->getScore("armor") + floor($protecteur->getScore("armor") * $param["ARMOR_BONUS"] / 100)), 0);
        SpellFactory::passiveBarrier($playerSrc, $protecteur, $param, $db);
        if ($param["PLAYER_KILLED"])
            $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
        else {
            $hp = $protecteur->get("currhp") - $param["TARGET_HP"];
            if ($param["TARGET_HP"] > 0)
                PlayerFactory::Enhance($protecteur, DEFENSE_EVENT_FAIL, $param, $db);
                // Mort de la cible
            if ($hp <= 0) {
                BasicActionFactory::kill($playerSrc, $protecteur, $param, $db);
                $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_DEATH;
            } else {
                $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_SUCCESS;
                PlayerFactory::loseHP($protecteur, $playerSrc, $param["TARGET_HP"], $param, $db);
                $protecteur->updateDBr($db);
            }
        }
        require_once (HOMEPATH . "/class/Event.inc.php");
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = PASSIVE_TWIRL;
        $param["PLAYER_ATTACK"] = $attack;
        $param["TARGET_DAMAGE_T"] = $damage;
        $param["PROTECTION"] = 1;
        
        Event::logAction($db, PASSIVE_TWIRL, $param);
    }

    static function sharpen ($playerSrc, $object, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::appear($playerSrc, $param, $db);
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_SHARPEN;
        $param["TYPE_ATTACK"] = ABILITY_SHARPEN_EVENT;
        $param["OBJECT_NAME"] = $object->get("name");
        $param["PLAYER_SPEED"] = $playerSrc->getScore("speed");
        $dbe = new DBCollection("UPDATE Equipment SET sharpen=" . ceil($param["PLAYER_SPEED"] / 5) . " WHERE (id_EquipmentType < 5 or id_EquipmentType=6 OR id_EquipmentType=7) and id_BasicEquipment <> 205 and id=" . $object->get("id"), $db, 0, 0, false);
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db);
        if ($playerSrc->get("modeAnimation") == 0 && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
        }
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
    }

    /* *********************************************************** LES COMPETENCES DE l'ECOLE DE L'OMBRE *********************************************** */
    static function bolas ($playerSrc, $opponent, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        $param["TYPE_ACTION"] = ABILITY_BOLAS;
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["BROKEN"] = 0;
        
        // Récupération du bolas
        $equip = new Equipment();
        $equip->load(PlayerFactory::getIdArm($playerSrc, $db), $db);
        $equip->set("weared", "No");
        $param["BOLAS_LEVEL"] = $equip->get("level");
        // formule temporaire testé par Lotharim
        // $param["PLAYER_ATTACK"] = floor(($playerSrc->getScore("attack") - (2 * $playerSrc->getScoreWithNoBM("dexterity")) + ($playerSrc->getScoreWithNoBM("speed") * 1 / 2)) * 1.3);
        // ancienne formule
        $param["PLAYER_ATTACK"] = floor($playerSrc->getScore("attack") * 1.3);
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        
        $array_result = PlayerFactory::getListIdBolas($opponent, $db);
        $param["NB_BOLAS"] = count($array_result);
        // Jet de bolas réussi
        if (($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) && $param["NB_BOLAS"] < 2) {
            $param["XP"] += 1;
            $param["TYPE_ATTACK"] = ABILITY_BOLAS_EVENT_SUCCESS;
            $equip->set("id_Player", $opponent->get("id"));
            $equip->set("state", "Transported");
            $opponent->set("state", "creeping");
            $bm = new BM();
            $bm->set("name", "A terre");
            $bm->set("life", "-2");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $opponent->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->set("id_StaticModifier", 20);
            // (#TAG108)
            $bm->addDBr($db);
            playerFactory::InitBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            playerFactory::InitBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        } else {
            // Attaque raté
            $date = gmdate('Y-m-d H:i:s');
            $param["TYPE_ATTACK"] = ABILITY_BOLAS_EVENT_FAIL;
            $equip->set("id_Player", 0);
            $equip->set("state", "Ground");
            $equip->set("dropped", $date);
            $equip->set("x", $opponent->get("x"));
            $equip->set("y", $opponent->get("y"));
            $equip->set("room", $opponent->get("room"));
            $equip->set("inbuilding", $opponent->get("inbuilding"));
            
            PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
        }
        
        if ($playerSrc->get("status") == "PC") {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
            if ($playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4);
                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 4);
            }
        }
        $opponent->updateDBr($db);
        $equip->updateDBr($db);
        
        SpellFactory::removeAnger($playerSrc, 1, $param, $db);
        
        return ACTION_NO_ERROR;
    }

    static function disarm ($playerSrc, $opponent, &$param, &$db)
    {
        $param["TYPE_ACTION"] = ABILITY_DISARM;
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        SpellFactory::passiveBarrier($playerSrc, $opponent, $param, $db);
        if ($param["PLAYER_KILLED"])
            $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
        else {
            // Jets de dés
            // $param["PLAYER_ATTACK"] = floor($playerSrc->getScore("attack") - (2 * $playerSrc->getScoreWithNoBM("dexterity")) + ($playerSrc->getScoreWithNoBM("speed") * 1 / 2));
            $param["PLAYER_ATTACK"] = floor($playerSrc->getScore("speed") * 3 / 2);
            $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
            
            // Attaque réussi
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                if ($param["ARM"] = PlayerFactory::getIdArm($opponent, $db, 1) != 0) {
                    $dbu = new DBCollection("UPDATE Equipment SET weared='NO', id_Player =0, room=" . $opponent->get("room") . ", inbuilding=" . $opponent->get("inbuilding") . ", state='Ground', x=" . $opponent->get("x") . ", y=" . $opponent->get("y") . ", dropped='" . gmdate("Y-m-d H:i:s") . "' WHERE id=" . PlayerFactory::getIdArm($opponent, $db, 1), $db, 0, 0, false);
                    playerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                    $param["TYPE_ATTACK"] = ABILITY_DISARM_EVENT_SUCCESS;
                    $param["XP"] ++;
                } else {
                    // Désarmer un mec sans arme - gg !
                    $param["STUPID"] = 1;
                    $param["TYPE_ATTACK"] = ABILITY_DISARM_EVENT_SUCCESS;
                }
                PlayerFactory::Enhance($playerSrc, ABILITY_DISARM_EVENT_SUCCESS, $param, $db);
            } else {
                $param["TYPE_ATTACK"] = ABILITY_DISARM_EVENT_FAIL;
                PlayerFactory::Enhance($playerSrc, ABILITY_DISARM_EVENT_FAIL, $param, $db);
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            }
            
            if ($playerSrc->get("status") == 'PC') {
                BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            }
        }
        SpellFactory::removeAnger($playerSrc, 1, $param, $db);
    }

    static function larceny ($playerSrc, $opponent, &$param, &$db)
    {
        $param["TYPE_ACTION"] = ABILITY_LARCENY;
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        // Jets de dés
        // $param["PLAYER_ATTACK"] = $playerSrc->getScore("speed") + floor(1 / 2 * $playerSrc->getScore("dexterity"));
        $param["PLAYER_ATTACK"] = floor($playerSrc->getScore("speed") * 3 / 2);
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        
        // Attaque réussi
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
            
            $param["TYPE_ATTACK"] = ABILITY_LARCENY_EVENT_SUCCESS;
            $bm = new BM();
            $bm->set("name", "Larçin");
            $bm->set("life", "0");
            $bm->set("effect", "NEGATIVE");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $opponent->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
            playerFactory::InitBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
        } else {
            $param["TYPE_ATTACK"] = ABILITY_LARCENY_EVENT_FAIL;
        }
        
        if ($playerSrc->get("status") == 'PC') {
            PlayerFactory::Enhance($playerSrc, $param["TYPE_ATTACK"], $param, $db);
            BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        }
    }

    static function steal ($playerSrc, $opponent, &$param, &$db)
    {
        $param["TYPE_ACTION"] = ABILITY_STEAL;
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        // Jets de dés
        $param["PLAYER_ATTACK"] = floor(3 / 2 * $playerSrc->getScore("speed"));
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        $param["AUTH"] = 0;
        // Attaque réussi
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
            // $param["XP"] +=1;
            $param["TYPE_ATTACK"] = ABILITY_STEAL_EVENT_SUCCESS;
            $param["MONEY"] = floor($opponent->get("money") / 2);
            $playerSrc->set("money", $playerSrc->get("money") + $param["MONEY"]);
            $opponent->set("money", $opponent->get("money") - $param["MONEY"]);
            $opponent->updateDB($db);
            if ($param["PLAYER_ATTACK"] >= 2 * $param["TARGET_DEFENSE"])
                $param["AUTH"] = 2;
        } else {
            $param["TYPE_ATTACK"] = ABILITY_STEAL_EVENT_FAIL;
            $param["MONEY"] = 0;
        }
        if ($playerSrc->get("status") == "PC") {
            if ($param["AUTH"] != 2)
                BasicActionFactory::legalAction($playerSrc, $opponent, $param, $db, 0);
            PlayerFactory::Enhance($playerSrc, $param["TYPE_ATTACK"], $param, $db);
        }
        
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
    }

    static function ambush ($playerSrc, &$param, &$db)
    {
        $param["TYPE_ACTION"] = ABILITY_AMBUSH;
        $param["TYPE_ATTACK"] = ABILITY_AMBUSH_EVENT;
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        if ($playerSrc->get("room") != 0 && $playerSrc->get("inbuilding") != 0) {
            $param["AMBUSH_INBUILDING"] = 1;
            $dbp = new DBCollection("SELECT * FROM Player WHERE room= " . $playerSrc->get("room") . " AND inbuilding=" . $playerSrc->get("inbuilding") . " AND map=" . $playerSrc->get("map") . "  AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 5", $db);
        } else {
            $dbp = new DBCollection("SELECT * FROM Player WHERE map=" . $playerSrc->get("map") . "  AND (((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 5 and room in(0," . TOWER_ROOM . "," . EXCHANGE_ROOM . "," . TEMPLE_ROOF_ROOM . "," . PALACE_BELL_TOWER_ROOM . ")) OR ((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 2 AND room in(" . ENTRANCE_ROOM . ")))", $db);
        }
        while (! $dbp->eof()) {
            $bm = new BM();
            $bm->set("name", "Embuscade");
            $bm->set("life", "-2");
            $bm->set("id_Player", $dbp->get("id"));
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
            
            $dbp->next();
        }
        
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        }
        $playerSrc->set("hidden", floor($playerSrc->get("hidden") / 10) * 10 + 1);
        $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    /* *********************************************************** LES COMPETENCES DE l'ECOLE DU PALADIN *********************************************** */
    static function autoRegen ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_AUTOREGEN;
        $param["TYPE_ATTACK"] = ABILITY_AUTOREGEN_EVENT;
        
        // Effet
        $param["PLAYER_STRENGTH"] = $playerSrc->getScore("strength");
        $param["HP_POTENTIEL"] = ceil((30 + ceil($param["PLAYER_STRENGTH"] / 2)) * $playerSrc->get("hp") / 100);
        $param["HP_HEAL"] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
        $param["MALUS_STRENGTH"] = floor(($playerSrc->getModif("strength", DICE_D6) + $playerSrc->getModif("strength_bm", DICE_D6)) / 2);
        
        $static = new StaticModifier();
        $static->setModif("strength", DICE_D6, - $param["MALUS_STRENGTH"]);
        
        $date = gmdate('Y-m-d H:i:s');
        $bm = new BM();
        $bm->set("name", "autoRegen");
        $bm->set("life", "2");
        $bm->set("id_Player\$src", $playerSrc->get("id"));
        $bm->set("id_Player", $playerSrc->get("id"));
        $bm->set("date", gmdate("Y-m-d H:i:s"));
        $bm->setObj("StaticModifier", $static, true);
        $bm->addDBr($db);
        
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        // $param["XP"] +=1;
        
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
            $playerSrc->setSub("Upgrade", "hp", $playerSrc->getSub("Upgrade", "hp") + 6);
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        }
        
        $playerSrc->updateDB($db);
    }

    static function DisabledBravery ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = DISABLED_BRAVERY;
        $param["TYPE_ATTACK"] = DISABLED_BRAVERY_EVENT;
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND id_Player\$src=" . $playerSrc->get("id") . " AND name='Aura de courage'", $db);
        if (! $dbc->eof()) {
            
            $dbi = new DBCollection("delete from BM where id_Player\$src=" . $playerSrc->get("id") . " and id_Player='" . $playerSrc->get("id") . "' AND name='Aura de courage'", $db, 0, 0, false);
            if ($playerSrc->get("status") == "PC") {
                $playerSrc->getObj("Modifier")->update();
            }
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $playerSrc->updateDBr($db);
            
            $dbp = new DBCollection("SELECT id_Player FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " AND name='Aura de courage'", $db);
            while (! $dbp->eof()) {
                $dbx = new DBCollection("DELETE FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " and id_Player='" . $dbp->get("id_Player") . "' AND name='Aura de courage'", $db, 0, 0, false);
                $opponent = new Player();
                $opponent->externDBObj("Modifier");
                $opponent->load($dbp->get("id_Player"), $db);
                $idOpponent = $opponent->get("id");
                if (isset($idOpponent) && ($idOpponent >= 0) && $idOpponent != "") { // pour les auras qui sont supprimées pour des PJs disparus entre temps
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                    $opponent->updateDBr($db);
                }
                $dbp->next();
            }
        }
        return ACTION_NO_ERROR;
    }

    static function bravery ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_BRAVERY;
        $param["TYPE_ATTACK"] = ABILITY_BRAVERY_EVENT;
        $map = $playerSrc->get("map");
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND id_Player\$src=" . $playerSrc->get("id") . " AND name='Aura de courage'", $db);
        if (! $dbc->eof()) {
            // Aura déjà activé;
        } else {
            $param["BONUS_ATTACK"] = floor(5 + $playerSrc->get("currhp") / 20);
            $param["MALUS_LIFE"] = floor(5 * $playerSrc->get("hp") / 100);
            
            $dbc = new DBCollection("SELECT p1.id FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" . ID_BASIC_RACE_FEU_FOL . ") WHERE p1.map=" . $map . " AND (abs(p1.x-" . $xp . ") + abs(p1.y-" . $yp . ") + abs(p1.x+p1.y-" . $xp . "-" . $yp . "))/2 <=8 AND ((" . $playerSrc->get("id_FighterGroup") . " > 0 and (p1.id_FighterGroup=" . $playerSrc->get("id_FighterGroup") . " or p2.id_FighterGroup=" . $playerSrc->get("id_FighterGroup") . ") or p1.id = " . $playerSrc->get("id") . " or p2.id = " . $playerSrc->get("id") . "))", $db);
            while (! $dbc->eof()) {
                $static = new StaticModifier();
                $static->setModif("attack", DICE_MULT, $param["BONUS_ATTACK"]);
                
                $bm = new BM();
                if ($dbc->get("id") == $playerSrc->get("id"))
                    $bm->set("value", $param["MALUS_LIFE"]);
                $bm->set("name", "Aura de courage");
                $bm->set("life", - 2);
                $bm->set("id_Player\$src", $playerSrc->get("id"));
                $bm->set("id_Player", $dbc->get("id"));
                $bm->set("date", gmdate("Y-m-d H:i:s"));
                $bm->setObj("StaticModifier", $static, true);
                $bm->addDBr($db);
                
                $opponent = new Player();
                $opponent->externDBObj("Modifier");
                $opponent->load($dbc->get("id"), $db);
                PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                $opponent->updateDBr($db);
                $dbc->next();
            }
            
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            // $param["XP"] +=1;
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0)
                $playerSrc->setSub("Upgrade", "hp", $playerSrc->getSub("Upgrade", "hp") + 5);
            
            $playerSrc->updateDB($db);
        }
    }

    static function DisabledResistance ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = DISABLED_RESISTANCE;
        $param["TYPE_ATTACK"] = DISABLED_RESISTANCE_EVENT;
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND id_Player\$src=" . $playerSrc->get("id") . " AND name='Aura de résistance'", $db);
        if (! $dbc->eof()) {
            
            $dbi = new DBCollection("delete from BM where id_Player\$src=" . $playerSrc->get("id") . " and id_Player='" . $playerSrc->get("id") . "' AND name='Aura de résistance'", $db, 0, 0, false);
            if ($playerSrc->get("status") == "PC") {
                $playerSrc->getObj("Modifier")->update();
            }
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            $playerSrc->updateDBr($db);
            
            $dbp = new DBCollection("SELECT id_Player FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " AND name='Aura de résistance'", $db);
            while (! $dbp->eof()) {
                $dbx = new DBCollection("DELETE FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " and id_Player='" . $dbp->get("id_Player") . "' AND name='Aura de résistance'", $db, 0, 0, false);
                $opponent = new Player();
                $opponent->externDBObj("Modifier");
                $opponent->load($dbp->get("id_Player"), $db);
                $idOpponent = $opponent->get("id");
                if (isset($idOpponent) && ($idOpponent >= 0) && $idOpponent != "") { // pour les auras qui sont supprimées pour des PJs disparus entre temps
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                    $opponent->updateDBr($db);
                }
                $dbp->next();
            }
        }
        
        return ACTION_NO_ERROR;
    }

    static function resistance ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_RESISTANCE;
        $param["TYPE_ATTACK"] = ABILITY_RESISTANCE_EVENT;
        
        $map = $playerSrc->get("map");
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND id_Player\$src=" . $playerSrc->get("id") . " AND name='Aura de résistance'", $db);
        if (! $dbc->eof()) {
            // Aura déjà activé;
        } else {
            $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
            $param["BONUS_ARMOR"] = floor(10 + $param["PLAYER_MM"] / 4);
            $static = new StaticModifier();
            $static->set("armor", DICE_MULT, $param["BONUS_ARMOR"]);
            $dbc = new DBCollection("SELECT p1.id FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" . ID_BASIC_RACE_FEU_FOL . ") WHERE p1.map=" . $map . " AND (abs(p1.x-" . $xp . ") + abs(p1.y-" . $yp . ") + abs(p1.x+p1.y-" . $xp . "-" . $yp . "))/2 <=8 AND ((" . $playerSrc->get("id_FighterGroup") . " > 0 and (p1.id_FighterGroup=" . $playerSrc->get("id_FighterGroup") . " or p2.id_FighterGroup=" . $playerSrc->get("id_FighterGroup") . ") or p1.id = " . $playerSrc->get("id") . " or p2.id = " . $playerSrc->get("id") . " ))", $db);
            while (! $dbc->eof()) {
                $static = new StaticModifier();
                $static->setModif("armor", DICE_MULT, $param["BONUS_ARMOR"]);
                
                $bm = new BM();
                if ($dbc->get("id") == $playerSrc->get("id"))
                    $static->setModif("strength", DICE_MULT, - 20);
                $bm->setObj("StaticModifier", $static, true);
                $bm->set("name", "Aura de résistance");
                $bm->set("life", - 2);
                $bm->set("id_Player\$src", $playerSrc->get("id"));
                $bm->set("id_Player", $dbc->get("id"));
                $bm->set("date", gmdate("Y-m-d H:i:s"));
                $bm->addDBr($db);
                
                $opponent = new Player();
                $opponent->externDBObj("Modifier");
                $opponent->load($dbc->get("id"), $db);
                PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                $opponent->updateDBr($db);
                
                $dbc->next();
            }
            
            PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            // $param["XP"] +=1;
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 1);
            }
            $playerSrc->updateDB($db);
        }
    }

    static function exorcism ($playerSrc, &$param, &$db)
    {
        $param["TYPE_ACTION"] = ABILITY_EXORCISM;
        $param["TYPE_ATTACK"] = ABILITY_EXORCISM_EVENT;
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        BasicActionFactory::globalInfo($playerSrc, $param);
        // BasicActionFactory::globalInfo($playerSrc,$param2);
        
        $param["PLAYER_MM"] = min(150, $playerSrc->getScore("magicSkill"));
        $dbp = new DBCollection("SELECT Player.id AS id,Player.name AS namePlayer, BM.value as value, BM.id as idBM FROM BM INNER JOIN Player ON Player.id=BM.id_Player\$src WHERE  id_Player=" . $playerSrc->get("id") . " AND BM.name='" . addslashes("Exorcisme de l'ombre") . "'", $db);
        $i = 0;
        $param["NUMBER_OPPONENT"] = $dbp->count();
        
        while (! $dbp->eof()) {
            $i ++;
            $opp = new Player();
            $opp->externDBObj("Modifier");
            $ret = $opp->load($dbp->get("id"), $db);
            BasicActionFactory::globalInfo($playerSrc, $param2);
            BasicActionFactory::globalInfoOpponent($opp, $param2);
            if (disthexa($xp, $yp, $opp->get("x"), $opp->get("y")) > 5 || ($opp->get("room") != $playerSrc->get("room"))) {
                $param["FAR"][$i] = 1;
                $param["TARGET_NAME"][$i] = $dbp->get("namePlayer");
                $param["TARGET_VALUE_ID"][$i] = $dbp->get("id");
            } else {
                
                $param2["XP"] = 0;
                $param["FAR"][$i] = 0;
                $param["TARGET_DAMAGETOT2"][$i] = floor(($dbp->get("value") * $param["PLAYER_MM"] / 100));
                $param["TARGET_DAMAGE"][$i] = $param["TARGET_DAMAGETOT2"][$i];
                $param2["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGETOT2"][$i];
                $param["BUBBLE"][$i] = SpellFactory::PassiveBubble($opp, $param2, $db);
                if ($param["BUBBLE"][$i]) {
                    $param["BUBBLE_LIFE2"][$i] = $param2["BUBBLE_LIFE"];
                    $param["BUBBLE_CANCEL2"][$i] = $param2["BUBBLE_CANCEL"];
                    $param["BUBBLE_LEVEL2"][$i] = $param2["BUBBLE_LEVEL"];
                }
                $param["TARGET_HP"][$i] = $param2["TARGET_DAMAGETOT2"];
                $hp = $opp->get("currhp") - $param["TARGET_HP"][$i];
                if ($opp->get("status") == "NPC") {
                    require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
                    PNJFactory::runIA($opp, $playerSrc->get("id"), $db);
                }
                $param2["TARGET_KILLED"] = 0;
                if ($hp <= 0) {
                    $param2["TYPE_ACTION"] = ABILITY_EXORCISM;
                    $param2["TYPE_ATTACK"] = ABILITY_EXORCISM_EVENT;
                    BasicActionFactory::kill($playerSrc, $opp, $param2, $db);
                    // Pour l'affichage de la distribution des px
                    if ($param2["TARGET_KILLED"]) {
                        $param["NOMBRE"] = $param2["NOMBRE"];
                        for ($j = 1; $j < $param2["NOMBRE"]; $j ++) {
                            $param["PLAYER" . $j] = $param2["PLAYER" . $j];
                            if (isset($param["GAIN" . $j]))
                                $param["GAIN" . $j] += $param2["GAIN" . $j];
                            else
                                $param["GAIN" . $j] = $param2["GAIN" . $j];
                        }
                        // Param pour le fallmsg
                        if (isset($param2["EQUIPMENT_FALL"]))
                            $param["EQUIPMENT_FALL"][$i] = $param2["EQUIPMENT_FALL"];
                        
                        if (isset($param2["MONEY_FALL"]))
                            $param["MONEY_FALL"][$i] = $param2["MONEY_FALL"];
                        
                        if (isset($param2["DROP_NAME"]))
                            $param["DROP_NAME"][$i] = $param2["DROP_NAME"];
                        
                        if (isset($param2["LARCIN"]))
                            $param["LARCIN"][$i] = $param2["LARCIN"];
                    }
                } else {
                    if ($playerSrc->get("status") == 'PC')
                        BasicActionFactory::legalAction($playerSrc, $opp, $param, $db, 0);
                    $param2["TYPE_ATTACK"] = PASSIVE_EXORCISM_EVENT;
                    $param2["TARGET_KILLED"] = 0;
                    $param2["XP"] = 0;
                    
                    PlayerFactory::loseHP($opp, $playerSrc, $param["TARGET_HP"][$i], $param, $db);
                    $opp->updateDBr($db);
                }
                
                $param2["ERROR"] = 0;
                $param2["BUBBLE"] = $param["BUBBLE"][$i];
                $param2["TYPE_ACTION"] = PASSIVE_EXORCISM;
                $param2["PLAYER_MM"] = $param["PLAYER_MM"];
                $param2["TARGET_DAMAGE"] = $param["TARGET_DAMAGE"][$i];
                $param2["TARGET_HP"] = $param["TARGET_HP"][$i];
                
                $param["TARGET_NAME"][$i] = $param2["TARGET_NAME"];
                $param["TARGET_GENDER"][$i] = $param2["TARGET_GENDER"];
                $param["TARGET_KILLED"][$i] = $param2["TARGET_KILLED"];
                $param["TARGET_VALUE_ID"][$i] = $param2["TARGET_ID"];
                
                BasicActionFactory::globalInfo($playerSrc, $param2);
                
                Event::logAction($db, PASSIVE_EXORCISM, $param2);
                
                $param["XP"] += $param2["XP"];
                
                $dbe = new DBCollection("DELETE FROM BM WHERE id=" . $dbp->get("idBM"), $db, 0, 0, false);
            }
            $dbp->next();
        }
        
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
            $playerSrc->setSub("Upgrade", "hp", $playerSrc->getSub("Upgrade", "hp") + 4);
        }
    }

    static function protection ($playerSrc, $opponent, &$param, &$db)
    {
        $param["TYPE_ACTION"] = ABILITY_PROTECTION;
        $param["TYPE_ATTACK"] = ABILITY_PROTECTION_EVENT;
        
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        $bm = new BM();
        $bm->set("name", "défendu");
        $bm->set("life", "-2");
        $bm->set("id_Player\$src", $playerSrc->get("id"));
        $bm->set("id_Player", $opponent->get("id"));
        $bm->set("date", gmdate("Y-m-d H:i:s"));
        $bm->addDBr($db);
        playerFactory::InitBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
            $playerSrc->setSub("Upgrade", "hp", $playerSrc->getSub("Upgrade", "hp") + 4);
        }
        $playerSrc->updateDB($db);
    }

    static function disabledProtection ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = DISABLED_PROTECTION;
        $param["TYPE_ATTACK"] = DISABLED_PROTECTION_EVENT;
        
        $dbc = new DBCollection("DELETE FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " AND name='défendu'", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    /* *********************************************************** LES COMPETENCES DE l'ECOLE DES ARCHERS DE KRIMA *********************************************** */
    static function run ($playerSrc, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_RUN;
        $param["TYPE_ATTACK"] = ABILITY_RUN_EVENT;
        $date = gmdate('Y-m-d H:i:s');
        // Effet
        
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Course Celeste'", $db);
        if (! $dbc->eof())
            $dbx = new DBCollection("UPDATE BM SET life=1 WHERE id=" . $dbc->get('id_StaticModifier_BM'), $db, 0, 0, false);
        else {
            $bm = new BM();
            $bm->set("name", "Course Celeste");
            $bm->set("life", "1");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
        }
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $param["XP"] = 0;
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0)
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
        
        $playerSrc->updateDB($db);
    }

    static function flaming ($playerSrc, $building, $id_Arrow, &$param, &$db, $modifierA)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = ABILITY_FLAMING;
        
        $param["BUILDING_NAME"] = $building->get("name");
        $param["BUILDING_LEVEL"] = $building->get("level");
        $date = gmdate('Y-m-d H:i:s');
        $param["BUILDING_DEATH"] = 0;
        
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $charac = array(
                "attack",
                "damage"
        );
        foreach ($charac as $key => $name) {
            for ($type = 0; $type < DICE_CHARAC; $type ++) {
                if ($modifierA != null)
                    $playerSrcModif->addModif($name, $type, $modifierA->getModif($name, $type));
            }
        }
        
        // Jets de dés
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        $param["PLAYER_DAMAGE"] = BasicActionFactory::getDamageArchery($param["PLAYER_ATTACK"], 0) * 2 + $playerSrcModif->getScore("damage");
        $param["BUILDING_DAMAGE"] = $param["PLAYER_DAMAGE"] + 100;
        
        $dbc = new DBCollection("SELECT captured,id_Player as governor FROM City WHERE id=" . $building->get("id_City"), $db);
        if ($dbc->count() > 0 && $dbc->get("captured") <= 3 && $building->get("id_BasicBuilding") != 15 && $building->get("id_BasicBuilding") != 27) {
            $param["BUILDING_DAMAGE"] = 0;
        }
        $dest = array();
        $dbu = new DBCollection("DELETE FROM Equipment WHERE id=" . $id_Arrow, $db, 0, 0, false);
        if ($building->get("id_Player") != 0) {
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $building->get("id_Player"), $db);
            $dest[$dbp->get("name")] = $dbp->get("name");
        } else {
            if ($dbc->get("captured") > 3) {
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("governor"), $db);
                $dest = array();
                $dest[$dbp->get("name")] = $dbp->get("name");
            }
        }
        
        if ($param["BUILDING_DAMAGE"] >= $building->get("currsp")) {
            $body = "Votre bâtiment : " . $building->get("name") . " (" . $building->get("id") . ") situé en " . $building->get("x") . "/" . $building->get("y") . " a été détruit";
            MailFactory::sendMsg($playerSrc, localize("Bâtiment détruit"), $body, $dest, 2, $err, $db, - 1, 0);
            
            BasicActionFactory::collapseBuilding($building, $playerSrc, $db);
            if ($building->get("name") == "Rempart" or $building->get("id_BasicBuilding") == 21 or $building->get("id_BasicBuilding") == 26) {
                $building->set("progress", 0);
                $building->set("currsp", 0);
                $building->set("repair", 0);
                $building->updateDB($db);
            } else
                $building->deleteDB($db);
            $param["TYPE_ATTACK"] = ABILITY_FLAMING_EVENT_DEATH;
            $param["BUILDING_DEATH"] = 1;
        } else {
            $body = "Votre bâtiment : " . $building->get("name") . " (" . $building->get("id") . ") situé en " . $building->get("x") . "/" . $building->get("y") . " a été endommagé";
            MailFactory::sendMsg($playerSrc, localize("Bâtiment endommagé"), $body, $dest, 2, $err, $db, - 1, 0);
            
            $building->set("currsp", $building->get("currsp") - $param["BUILDING_DAMAGE"]);
            $building->updateDB($db);
            $param["TYPE_ATTACK"] = ABILITY_FLAMING_EVENT_SUCCESS;
        }
        
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 8);
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        }
        $playerSrc->updateDBr($db);
        $playerSrc->updateHidden($db);
        return ACTION_NO_ERROR;
    }

    static function flyArrow ($playerSrc, $opponent, $arrow, $arrowmodifier, &$param, &$db)
    {
        BasicActionFactory::appear($playerSrc, $param, $db);
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        $date = gmdate('Y-m-d H:i:s');
        
        $date = gmdate("Y-m-d H:i:s");
        $param["ARROW2"] = 0;
        $param["ARROW3"] = 0;
        $param["TARGET_KILLED"][2] = 0;
        $ft = 0;
        
        for ($i = 1; $i < $param["NB"]; $i ++) {
            $dead = 0;
            $param2 = array();
            $param2["XP"] = $param["XP"];
            if ($i == 2) {
                if ($opponent[2]->get("id") == $opponent[1]->get("id")) {
                    if ($param["TARGET_KILLED"][1] == 1) {
                        $param["ARROW2"] = 1;
                        $dead = 1;
                    } else
                        $opponent[2]->set("currhp", $opponent[1]->get("currhp"));
                }
            }
            if ($i == 3) {
                if ($opponent[3]->get("id") == $opponent[1]->get("id")) {
                    if ($param["TARGET_KILLED"][1] == 1) {
                        $param["ARROW3"] = 1;
                        $dead = 1;
                    } else
                        $opponent[3]->set("currhp", $opponent[1]->get("currhp"));
                }
                if ($opponent[3]->get("id") == $opponent[2]->get("id")) {
                    if ($param["TARGET_KILLED"][2] == 1) {
                        $param["ARROW3"] = 1;
                        $dead = 1;
                    } else
                        $opponent[3]->set("currhp", $opponent[2]->get("currhp"));
                }
            }
            if ($dead == 0) {
                $param2["XP"] = 0;
                
                // Détermination du malus dû à la portée
                $dx = $playerSrc->get("x") - $opponent[$i]->get("x");
                $dy = $playerSrc->get("y") - $opponent[$i]->get("y");
                $dist = (abs($dx) + abs($dy) + abs($dx + $dy)) / 2;
                $malus = min((0.1 * (1 - $dist)), 0);
                
                $param["MALUS_GAP"][$i] = - ($malus * 100);
                $modifierA = new Modifier();
                $malusDistance = floor(min($playerSrc->getModif("dexterity", DICE_D6), $playerSrc->getModif("speed", DICE_D6)) * $malus);
                $malusAttVit = (- 1.5) * max(($playerSrc->getModif("dexterity", DICE_D6) - $playerSrc->getModif("speed", DICE_D6)), 0); // donne un malus si attaque sup à vitesse
                $modifierA->setModif("attack", DICE_D6, $malusDistance + $malusAttVit);
                $modifierA->setModif("damage", DICE_ADD, $arrowmodifier[$i]->getModif("damage", DICE_ADD));
                $modifierA->setModif("attack", DICE_ADD, $arrowmodifier[$i]->getModif("attack", DICE_ADD));
                $modifierA->addModifObjToBM($arrowmodifier[$i]);
                $modifierA->validateBM();
                
                BasicActionFactory::archery($playerSrc, $opponent[$i], $arrow[$i]->get("id"), $param2, $db, $modifierA, null, ABILITY_FLYARROW);
                
                $param["PLAYER_ATTACK"][$i] = $param2["PLAYER_ATTACK"];
                $param["BONUS_DAMAGE"][$i] = $param2["BONUS_DAMAGE"];
                $param["TARGET_NAME"][$i] = $param2["TARGET_NAME"];
                $param["TARGET_ID_FA"][$i] = $param2["TARGET_ID"];
                $param["TARGET_GENDER"][$i] = $param2["TARGET_GENDER"];
                $param["TARGET_DEFENSE"][$i] = $param2["TARGET_DEFENSE"];
                $param["TARGET_DAMAGE"][$i] = $param2["TARGET_DAMAGE"];
                $param["TARGET_DAMAGETOT2"][$i] = $param2["TARGET_DAMAGETOT2"];
                $param["TARGET_HP"][$i] = $param2["TARGET_HP"];
                $param["TARGET_KILLED"][$i] = $param2["TARGET_KILLED"];
                $param["PROTECTION"][$i] = $param2["PROTECTION"];
                
                if ($param["PROTECTION"][$i]) {
                    $param["NAME"][$i] = $param2["NAME"];
                    $param["TARGET_MM"][$i] = $param2["TARGET_MM"];
                    $param["ARMOR_BONUS"][$i] = $param2["ARMOR_BONUS"];
                    $param["INTER_ID"][$i] = $param2["INTER_ID"];
                }
                // Pour l'affichage de la distribution des px
                if ($param["TARGET_KILLED"][$i]) {
                    $param["NOMBRE"] = $param2["NOMBRE"];
                    for ($j = 1; $j < $param2["NOMBRE"]; $j ++) {
                        $param["PLAYER" . $j] = $param2["PLAYER" . $j];
                        if (isset($param["GAIN" . $j]))
                            $param["GAIN" . $j] += $param2["GAIN" . $j];
                        else
                            $param["GAIN" . $j] = $param2["GAIN" . $j];
                    }
                    
                    if (isset($param2["EQUIPMENT_FALL"]))
                        $param["EQUIPMENT_FALL"][$i] = $param2["EQUIPMENT_FALL"];
                    $param["MONEY_FALL"][$i] = $param2["MONEY_FALL"];
                    if (isset($param2["DROP_NAME"]))
                        $param["DROP_NAME"][$i] = $param2["DROP_NAME"];
                    if (isset($param2["LARCIN"]))
                        $param["LARCIN"][$i] = $param2["LARCIN"];
                }
                $param2["MALUS_GAP"] = $param["MALUS_GAP"][$i];
                $param2["ERROR"] = 0;
                
                $param["XP"] += $param2["XP"];
                if ($param["TARGET_DAMAGE"][$i] > 0) {
                    $ft ++;
                    if ($ft > 1)
                        $param["XP"] -= 1;
                }
                
                if ($param2["BUBBLE"]) {
                    $param["BUBBLE_LIFE2"][$i] = $param2["BUBBLE_LIFE"];
                    $param["BUBBLE_CANCEL2"][$i] = $param2["BUBBLE_CANCEL"];
                    $param["BUBBLE_LEVEL2"][$i] = $param2["BUBBLE_LEVEL"];
                } else
                    $param2["BUBBLE_LIFE"] = 0;
                
                require_once (HOMEPATH . "/class/Event.inc.php");
                $param["ERROR"] = 0;
                $param2["TYPE_ACTION"] = PASSIVE_FLYARROW;
                Event::logAction($db, PASSIVE_FLYARROW, $param2);
                unset($param2);
            }
            SpellFactory::removeAnger($playerSrc, 1, $param, $db);
            SpellFactory::removeAnger($playerSrc, 2, $param, $db);
        }
        
        $param["TYPE_ACTION"] = ABILITY_FLYARROW;
        $param["TYPE_ATTACK"] = ABILITY_FLYARROW_EVENT;
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
        }
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }
}
?>