<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");
require_once (HOMEPATH . "/class/BM.inc.php");
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");

class TalentFactory
{

    /* *************************************************************** FONCTIONS AUXILLIAIRES **************************************** */
    static private function globalInfo($playerSrc, &$param)
    {
        $param["PLAYER_NAME"] = $playerSrc->get("name");
        if ($param["PLAYER_NAME"] == "")
            $param["PLAYER_NAME"] = $playerSrc->get("racename");
        $param["PLAYER_RACE"] = $playerSrc->get("id_BasicRace");
        $param["PLAYER_ID"] = $playerSrc->get("id");
        $param["PLAYER_GENDER"] = $playerSrc->get("gender");
        
        $param["PLAYER_LEVEL"] = $playerSrc->get("level");
        $param["MEMBER_ID"] = $playerSrc->get("id_Member");
        
        if ($playerSrc->get("status") == "NPC")
            $param["PLAYER_IS_NPC"] = 1;
        else
            $param["PLAYER_IS_NPC"] = 0;
    }

    static private function globalInfoOpponent($playerSrc, &$param)
    {
        $param["TARGET_NAME"] = $playerSrc->get("name");
        if ($param["TARGET_NAME"] == "")
            $param["TARGET_NAME"] = $playerSrc->get("racename");
        $param["TARGET_RACE"] = $playerSrc->get("id_BasicRace");
        $param["TARGET_ID"] = $playerSrc->get("id");
        $param["TARGET_GENDER"] = $playerSrc->get("gender");
        
        $param["TARGET_LEVEL"] = $playerSrc->get("level");
        $param["TARGET_ID"] = $playerSrc->get("id");
        $param["TARGET_MEMBER"] = $playerSrc->get("id_Member");
        
        $param["TARGET_KILLED"] = 0;
        
        if ($playerSrc->get("status") == "NPC")
            $param["TARGET_IS_NPC"] = 1;
        else
            $param["TARGET_IS_NPC"] = 0;
    }

    static protected function modifierScoreToArray($eqmodifier, &$param)
    {
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getScoreWithNoBM($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function modifierToArray($eqmodifier, &$param)
    {
        $eqmodifier->initCharacStr();
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getModifStr($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function getScore($baseap, $success, &$param, $malus = 0)
    {
        $param["ENHANCE"] = 0;
        $param["PERCENT"] = $success - $malus;
        $param["SCORE"] = mt_rand(1, 100);
        $param["SUCCESS"] = 0;
        $param["SCOREB"] = 0;
        
        if ($param["SCORE"] <= $success - $malus) {
            $param["SUCCESS"] = 1;
            $param["SCOREB"] = mt_rand(1, 100);
            if ($param["SCOREB"] > $success)
                $param["ENHANCE"] = 1;
        }
        
        if ($baseap > 2 && ! $param["SUCCESS"])
            $baseap = min($baseap, 3);
        
        $param["AP"] = $baseap;
    }

    static function talent($playerSrc, $ident, $malus, &$param, &$param2, $db)
    {
        self::globalInfo($playerSrc, $param2);
        $dbc = new DBCollection(
            "SELECT Talent.id,Talent.skill, BasicTalent.PA, BasicTalent.name FROM Talent LEFT JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id WHERE BasicTalent.id=" .
                 ($ident - 300) . " AND id_Player=" . $playerSrc->get("id"), $db);
        if (! $dbc->eof()) {
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
            if (! $dbbm->eof()) {
                $malus += - $dbbm->get("level");
                $param["MALUS"] = - $malus;
            }
            $param2["TALENT_NAME"] = $dbc->get("name");
            $talentap = $dbc->get("PA");
            $success = $dbc->get("skill");
            self::getScore($talentap, $success, $param, $malus);
            
            if ($playerSrc->get("modeAnimation") == 1) {
                $param["ENHANCE"] = 0;
            }
            
            if ($success < 90 && $param["ENHANCE"] > 0) {
                $newsucess = min($success + $param["ENHANCE"], 90);
                $dbu = new DBCollection("UPDATE Talent SET skill=" . $newsucess . " WHERE id=" . $dbc->get("id") . " AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            } else {
                if ($success >= 90)
                    $param["SCOREB"] = 0;
            }
            if (! $param["SUCCESS"] && $success < 90) {
                $bonus = floor((100 - $param["PERCENT"]) / 2);
                $date = gmdate('Y-m-d H:i:s');
                $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db);
                if (! $dbbm->eof())
                    $dbbm = new DBCollection("UPDATE BM SET level=level+" . $bonus . " WHERE id_Player=" . $playerSrc->get("id") . " AND value=" . $ident, $db, 0, 0, false);
                else
                    $dbi = new DBCollection(
                        "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $playerSrc->get("id") . "," .
                             $playerSrc->get("id") . "," . $bonus . "," . $ident . ", 'POSITIVE', 'Concentration' ,0,'" . $date . "')", $db, 0, 0, false);
            } else
                $dbu = new DBCollection("DELETE FROM BM WHERE name='Concentration' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            BasicActionFactory::appear($playerSrc, $param, $db);
            return ACTION_NO_ERROR;
        }
        return TALENT_ERROR;
    }

    static protected function getScoreTool($success, &$param)
    {
        $param["TPERCENT"] = $success;
        $param["TSCORE"] = mt_rand(1, 100);
        $param["TSUCCESS"] = 0;
        
        if ($param["TSCORE"] <= $success) {
            $param["TSUCCESS"] = 1;
            return 1;
        }
        return 0;
    }

    /* *************************************************************** SAVOIR FAIRE D'EXTRACTION **************************************************** */
    static function extraction($playerSrc, $ident, $exploitation, &$param, &$db)
    {
        switch ($ident) {
            case TALENT_MINE:
                self::mine($playerSrc, $exploitation, $param, $db);
                break;
            case TALENT_DISMEMBER:
                self::dismember($playerSrc, $exploitation, $param, $db);
                break;
            case TALENT_SCUTCH:
                self::scutch($playerSrc, $exploitation, $param, $db);
                break;
            case TALENT_HARVEST:
                self::harvest($playerSrc, $exploitation, $param, $db);
                break;
            default:
                break;
        }
    }

    static function mine($playerSrc, $exploitation, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get(("inbuilding") > 0))
            return ACTION_NOT_ALLOWED;
            
            // Pour le message
        $param["TYPE_ATTACK"] = TALENT_MINE_EVENT;
        $param["TYPE_ACTION"] = TALENT_MINE;
        $param["TYPE"] = $exploitation->get("type");
        $param["GNAME"] = $exploitation->get("name");
        $param["LEVEL"] = $exploitation->get("level");
        $param["LEVELG"] = $exploitation->get("level");
        $param["TOOL"] = 0;
        $param["BROKEN"] = 0;
        
        // Création de la gemme récoltée
        $equip = new Equipment();
        $equip->set("id_Player", $playerSrc->get("id"));
        switch ($param["GNAME"]) {
            case "Emeraude":
                $equip->set("id_BasicEquipment", 100);
                break;
            case "Rubis":
                $equip->set("id_BasicEquipment", 101);
                break;
        }
        $equip->set("id_EquipmentType", 30);
        if ($param["INV"] == 1)
            $equip->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), 1, $db));
        $equip->set("name", $param["GNAME"]);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("level", $param["LEVEL"]);
        
        // Si le joueur porte un pioche
        $dbe = new DBCollection(
            "SELECT Equipment.id as id, BasicEquipment.frequency as freq FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE id_Player=" .
                 $playerSrc->get("id") . " AND Equipment.durability!= 0 AND weared='YES' AND id_BasicEquipment=200", $db);
        $founded = $dbe->count();
        if ($founded) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            if (self::getScoreTool(min(100, 82 - $dbe->get("freq") * 2 + 5 * max(0, $outils->get("level") - 5)), $param)) {
                $equip->set("level", $exploitation->get("level") + 1);
                $param["LEVEL"] = $exploitation->get("level") + 1;
            }
            
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
                // $outils->deleteDB($db);
            } // else {
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
            // }
        }
        
        // mise automatique en vente si liste de vente vide
        $dbforsale = new DBCollection("SELECT Equipment.id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND sell>0", $db);
        if ($dbforsale->count() == 0) {
            $defaultprice = EquipFactory::getPriceBasicEquipment($equip->get("id_BasicEquipment"), $equip->get("level"), $db) * 2;
            $equip->set("sell", $defaultprice);
        }
        
        // Traitement
        $equip->addDBr($db);
        $exploitation->set("value", $exploitation->get("value") - 1);
        $playerSrc->updateDBr($db);
        
        if ($exploitation->get("value") == 0)
            $exploitation->deleteDB($db);
        else
            $exploitation->updateDB($db);
    }

    static function dismember($playerSrc, $exploitation, &$param, &$db)
    {
        // Pour le message
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get(("inbuilding") > 0))
            return ACTION_NOT_ALLOWED;
        
        $param["TYPE_ATTACK"] = TALENT_DISMEMBER_EVENT;
        $param["TYPE_ACTION"] = TALENT_DISMEMBER;
        $param["TOOL"] = 0;
        $param["BROKEN"] = 0;
        $param["ELEMENT_LEVEL"] = max(1, floor(log($exploitation->get("level"), 2)) - 1);
        $param["LEVELD"] = $exploitation->get("level");
        
        // Création cuir ou écaille suivant la dépouille
        $eq = new Equipment();
        if ($exploitation->get("name") == "Cuir") {
            $param["ELEMENT"] = "cuir";
            $eq->set("name", "Cuir");
            $eq->set("id_BasicEquipment", 103);
        }
        if ($exploitation->get("name") == "Ecaille") {
            $param["ELEMENT"] = "écaille";
            $eq->set("name", "Ecaille");
            $eq->set("id_BasicEquipment", 104);
        }
        
        $param["ELEMENT_LEVEL"] = max(1, floor(log($exploitation->get("level"), 2)) - 1);
        
        // Si le joueur porte une pince à dépecer
        $dbe = new DBCollection(
            "SELECT Equipment.id as id, BasicEquipment.frequency as freq FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE id_Player=" .
                 $playerSrc->get("id") . " AND Equipment.durability!= 0 AND weared='YES' AND id_BasicEquipment=201", $db);
        if ($dbe->count()) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            
            if (self::getScoreTool(min(100, 82 - $dbe->get("freq") * 2 + 5 * max(0, $outils->get("level") - 5)), $param)) {
                $param["ELEMENT_LEVEL"] ++;
            }
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
            } else {
                $param["BROKEN"] = 0;
            }
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
        }
        
        // Traitement
        $eq->set("id_Player", $playerSrc->get("id"));
        $eq->set("id_EquipmentType", 31);
        $eq->set("date", gmdate("Y-m-d H:i:s"));
        $eq->set("level", $param["ELEMENT_LEVEL"]);
        $eq->set("state", "carried");
        if ($param["INV"] == 2)
            $eq->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), 2, $db));
            
            // mise automatique en vente si liste de vente vide
        $dbforsale = new DBCollection("SELECT Equipment.id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND sell>0", $db);
        if ($dbforsale->count() == 0) {
            $defaultprice = EquipFactory::getPriceBasicEquipment($eq->get("id_BasicEquipment"), $eq->get("level"), $db) * 2;
            $eq->set("sell", $defaultprice);
        }
        $eq->addDBr($db);
        
        if ($exploitation->get("value") == 1)
            $exploitation->deleteDBr($db);
        else {
            $exploitation->set("value", $exploitation->get("value") - 1);
            $exploitation->updateDB($db);
        }
        $playerSrc->updateDB($db);
        return ACTION_NO_ERROR;
    }

    static function scutch($playerSrc, $exploitation, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get(("inbuilding") > 0))
            return ACTION_NOT_ALLOWED;
            
            // Pour le message
        $param["TYPE_ATTACK"] = TALENT_SCUTCH_EVENT;
        $param["TYPE_ACTION"] = TALENT_SCUTCH;
        // $param["TYPE"] = $exploitation->get("type");
        $param["ELEMENT_LEVEL"] = $exploitation->get("level");
        $param["LEVELC"] = $exploitation->get("level");
        $param["TOOL"] = 0;
        $param["BROKEN"] = 0;
        
        // Création du lin récoltée
        $equip = new Equipment();
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("id_BasicEquipment", 105);
        $equip->set("id_EquipmentType", 31);
        if ($param["INV"] == 2)
            $equip->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), 2, $db));
        $equip->set("name", "Lin");
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        
        // Si le joueur porte une teilleuse
        $dbe = new DBCollection(
            "SELECT Equipment.id as id, BasicEquipment.frequency as freq FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE id_Player=" .
                 $playerSrc->get("id") . " AND Equipment.durability!= 0 AND weared='YES' AND id_BasicEquipment=202", $db);
        $founded = $dbe->count();
        if ($founded) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            if (self::getScoreTool(min(100, 82 - $dbe->get("freq") * 2 + 5 * max(0, $outils->get("level") - 5)), $param)) {
                $equip->set("level", $exploitation->get("level") + 1);
                $param["ELEMENT_LEVEL"] = $exploitation->get("level") + 1;
            }
            
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
            } else {
                $param["BROKEN"] = 0;
            }
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
        }
        
        // Traitement
        $equip->set("level", $param["ELEMENT_LEVEL"]);
        
        // mise automatique en vente si liste de vente vide
        $dbforsale = new DBCollection("SELECT Equipment.id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND sell>0", $db);
        if ($dbforsale->count() == 0) {
            $defaultprice = EquipFactory::getPriceBasicEquipment($equip->get("id_BasicEquipment"), $equip->get("level"), $db) * 2;
            $equip->set("sell", $defaultprice);
        }
        
        $equip->addDBr($db);
        $exploitation->set("value", $exploitation->get("value") - 1);
        
        if ($exploitation->get("value") == 0)
            $exploitation->deleteDB($db);
        else
            $exploitation->updateDB($db);
        
        $playerSrc->updateDBr($db);
    }

    static function harvest($playerSrc, $exploitation, &$param, &$db)
    {
        if ($playerSrc->get(("inbuilding") > 0))
            return ACTION_NOT_ALLOWED;
        
        self::globalInfo($playerSrc, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_HARVEST_EVENT;
        $param["TYPE_ACTION"] = TALENT_HARVEST;
        $param["ELEMENT_LEVEL"] = $exploitation->get("level");
        $param["LEVELB"] = $exploitation->get("level");
        $param["TOOL"] = 0;
        $param["BROKEN"] = 0;
        
        // Création de l'herbe suivant le type de buisson
        $equip = new Equipment();
        if ($exploitation->get("name") == "Razhot") {
            $param["ELEMENT"] = "racine de Razhot";
            $equip->set("name", "Racine de Razhot");
            $equip->set("id_BasicEquipment", 107);
        }
        if ($exploitation->get("name") == "Garnach") {
            $param["ELEMENT"] = "graine de Garnach";
            $equip->set("name", "Graine de Garnach");
            $equip->set("id_BasicEquipment", 108);
        }
        if ($exploitation->get("name") == "Folliane") {
            $param["ELEMENT"] = "feuille de Folliane";
            $equip->set("name", "Feuille de Folliane");
            $equip->set("id_BasicEquipment", 109);
        }
        
        // Création de l'herbe récoltée
        
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("id_EquipmentType", 32);
        if ($param["INV"] == 3)
            $equip->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), 3, $db));
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        
        // Si le joueur porte un pressoir
        $dbe = new DBCollection(
            "SELECT Equipment.id as id, BasicEquipment.frequency as freq FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE id_Player=" .
                 $playerSrc->get("id") . " AND Equipment.durability!= 0 AND weared='YES' AND id_BasicEquipment=203", $db);
        $founded = $dbe->count();
        if ($founded) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            if (self::getScoreTool(min(100, 82 - $dbe->get("freq") * 2 + 5 * max(0, $outils->get("level") - 5)), $param)) {
                $equip->set("level", $exploitation->get("level") + 1);
                $param["ELEMENT_LEVEL"] = $exploitation->get("level") + 1;
            }
            
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
            } else {
                $param["BROKEN"] = 0;
            }
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
        }
        
        $equip->set("level", $param["ELEMENT_LEVEL"]);
        
        // mise automatique en vente si liste de vente vide
        $dbforsale = new DBCollection("SELECT Equipment.id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND sell>0", $db);
        if ($dbforsale->count() == 0) {
            $defaultprice = EquipFactory::getPriceBasicEquipment($equip->get("id_BasicEquipment"), $equip->get("level"), $db) * 2;
            $equip->set("sell", $defaultprice);
        }
        // Traitement
        $equip->addDBr($db);
        $exploitation->set("value", $exploitation->get("value") - 1);
        
        $playerSrc->updateDBr($db);
        
        if ($exploitation->get("value") == 0) {
            
            // Si c'est un buisson libre, naissance d'un nouveau buisson sur une case de même type de terrain.
            if ($exploitation->get("zone") > 3) {
                $place = array();
                $place = BasicActionFactory::getAleaFreeCaseInZone($exploitation->get("zone"), $exploitation->get("map"), $db);
                $exp = new Exploitation();
                $exp->set("name", $exploitation->get("name"));
                $exp->set("type", $exploitation->get("type"));
                $exp->set("level", $exploitation->get("level"));
                $exp->set("zone", $exploitation->get("zone"));
                $exp->set("value", mt_rand(2, 4));
                $exp->set("map", $exploitation->get("map"));
                $exp->set("x", $place["x"]);
                $exp->set("y", $place["y"]);
                $exp->addDB($db);
            }
            $exploitation->deleteDB($db);
        } else
            $exploitation->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function speak($playerSrc, $opponent, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_SPEAK_EVENT;
        $param["TYPE_ACTION"] = TALENT_SPEAK;
        $param["BROKEN"] = 0;
        $param["TOOL"] = 0;
        $param["ELEMENT"] = "fer";
        $param["LEVELK"] = $opponent->get("level");
        
        $param["ELEMENT_LEVEL"] = max(1, floor(log($opponent->get("level"), 2)) - 1);
        // Si le joueur porte une gemme de conviction
        $dbe = new DBCollection(
            "SELECT Equipment.id as id, BasicEquipment.frequency as freq FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE id_Player=" .
                 $playerSrc->get("id") . " AND Equipment.durability!= 0 AND weared='YES' AND id_BasicEquipment=204", $db);
        $founded = $dbe->count();
        if ($founded) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            if (self::getScoreTool(min(100, 82 - $dbe->get("freq") * 2 + 5 * max(0, $outils->get("level") - 5)), $param)) {
                $param["ELEMENT_LEVEL"] ++;
            }
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
            }
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
        }
        
        // Création du fer récupéré
        $equip = new Equipment();
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("level", $param["ELEMENT_LEVEL"]);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("id_EquipmentType", 31);
        $equip->set("id_BasicEquipment", 106);
        $equip->set("name", "Fer");
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        if ($param["INV"] == 2)
            $equip->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), 2, $db));
            // mise automatique en vente si liste de vente vide
        $dbforsale = new DBCollection("SELECT Equipment.id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND sell>0", $db);
        if ($dbforsale->count() == 0) {
            $defaultprice = EquipFactory::getPriceBasicEquipment($equip->get("id_BasicEquipment"), $equip->get("level"), $db) * 2;
            $equip->set("sell", $defaultprice);
        }
        
        $equip->addDBr($db);
        
        // Disparition de la créature qui repart dans son monde :P
        if (mt_rand(0, 1))
            PNJFactory::DeleteDBnpc($opponent, $param, $db, null, 0);
    }

    static function cut($playerSrc, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_CUT_EVENT;
        $param["TYPE_ACTION"] = TALENT_CUT;
        $param["BROKEN"] = 0;
        $param["TOOL"] = 0;
        $bonus = 0;
        
        switch (getLandType($playerSrc->get("x"), $playerSrc->get("y"), $playerSrc->get("map"), $db)) {
            case "forest":
                $param["ELEMENT_LEVEL"] = 2;
                break;
            case "plain":
            case "flowers":
            case "mountain":
            case "marsh":
                $param["ELEMENT_LEVEL"] = 1;
                break;
            default:
                $param["ELEMENT_LEVEL"] = 0;
                return;
                break;
        }
        
        if ($param["ELEMENT_LEVEL"] > 0) {
            // Si le joueur porte un cauchoir
            $dbe = new DBCollection(
                "SELECT Equipment.id as id, BasicEquipment.frequency as freq FROM Equipment LEFT JOIN BasicEquipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE id_Player=" .
                     $playerSrc->get("id") . " AND Equipment.durability!= 0 AND weared='YES' AND id_BasicEquipment=206", $db);
            $founded = $dbe->count();
            if ($founded) {
                $outils = new Equipment();
                $outils->load($dbe->get("id"), $db);
                $param["TOOL"] = 1;
                if (self::getScoreTool(min(100, 82 - $dbe->get("freq") * 2 + 5 * max(0, $outils->get("level") - 5)), $param))
                    $param["ELEMENT_LEVEL"] = $param["ELEMENT_LEVEL"] + 1;
                
                if ($outils->get("durability") == 1) {
                    $param["BROKEN"] = 1;
                } else {
                    $param["BROKEN"] = 0;
                }
                $outils->set("durability", $outils->get("durability") - 1);
                $outils->updateDB($db);
            }
            
            // Création du bois récupéré
            $equip = new Equipment();
            $equip->set("id_Player", $playerSrc->get("id"));
            $equip->set("level", $param["ELEMENT_LEVEL"]);
            $equip->set("date", gmdate("Y-m-d H:i:s"));
            $equip->set("id_EquipmentType", 31);
            $equip->set("id_BasicEquipment", 110);
            $equip->set("name", "Bois");
            $equip->set("date", gmdate("Y-m-d H:i:s"));
            if ($param["INV"] == 2)
                $equip->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), 2, $db));
                
                // mise automatique en vente si liste de vente vide
            $dbforsale = new DBCollection("SELECT Equipment.id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND sell>0", $db);
            if ($dbforsale->count() == 0) {
                $defaultprice = EquipFactory::getPriceBasicEquipment($equip->get("id_BasicEquipment"), $equip->get("level"), $db);
                $equip->set("sell", $defaultprice);
            }
            
            $equip->addDBr($db);
        } else {
            return ACTION_NOT_ALLOWED;
        }
    }

    static function KradjeckCall($playerSrc, $sql_gate, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_KRADJECKCALL_EVENT;
        $param["TYPE_ACTION"] = TALENT_KRADJECKCALL;
        
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        $map = $playerSrc->get("map");
        
        $dbc = new DBCollection(
            "SELECT * FROM Player WHERE id_BasicRace='253' AND map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 5", $db);
        $param["GATE_LEVEL"] = $sql_gate->get("level");
        $param["KRADJ_NB"] = $dbc->count();
        
        $param["PERCENT"] = max(5, 100 - $param["GATE_LEVEL"] - ($param["KRADJ_NB"] * $param["KRADJ_NB"]) * ($param["KRADJ_NB"] <= 2 ? 10 : 5));
        $param["SCORE"] = mt_rand(1, 100);
        
        $kradjeckLevel = $param["GATE_LEVEL"] + mt_rand(0, ceil($param["GATE_LEVEL"] / 7));
        
        if ($param["SCORE"] <= $param["PERCENT"]) {
            require_once (HOMEPATH . "/lib/Map.inc.php");
            $map = new Map($db, $playerSrc->get("map"));
            $place = array();
            $place = BasicActionFactory::getAleaFreePlace($sql_gate->get("x"), $sql_gate->get("y"), $sql_gate->get("map"), 3, $db);
            if ($place != 0)
                $map->insertNPC($kradjeckLevel, 253, $place["x"], $place["y"], $sql_gate->get("map"), 0);
        }
    }

    static function Monster($playerSrc, $opponent, $charac1, $charac2, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_MONSTER_EVENT;
        $param["TYPE_ACTION"] = TALENT_MONSTER;
        
        $param["TARGET_HP"] = $opponent->get("currhp");
        $characLabel = array(
            "1" => "attack",
            "2" => "damage",
            "3" => "defense",
            "4" => "armor",
            "5" => "magicSkill"
        );
        $characName = array(
            "1" => "Attaque",
            "2" => "Dégât",
            "3" => "Défense",
            "4" => "Armure",
            "5" => "Maitrise de la magie"
        );
        
        $param["CHARAC1_VALUE"] = $opponent->getScore($characLabel[$charac1]);
        $param["CHARAC1_NAME"] = $characName[$charac1];
        $param["CHARAC2_VALUE"] = $opponent->getScore($characLabel[$charac2]);
        $param["CHARAC2_NAME"] = $characName[$charac2];
        $param["MONSTER_LEVEL"] = $opponent->get("level");
    }

    static function Detection($playerSrc, $exploitationType, $niveau, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_DETECTION_EVENT;
        $param["TYPE_ACTION"] = TALENT_DETECTION;
        $param["DIRECTION"] = 0;
        
        $name = array(
            "1" => "Lin",
            "2" => "Razhot",
            "3" => "Garnach",
            "4" => "Folliane",
            "5" => "Emeraude",
            "6" => "Rubis"
        );
        $nameExp = array(
            "1" => "Champs de Lin",
            "2" => "Buisson de Razhot",
            "3" => "Buisson de Garnach",
            "4" => "Buisson de Folliane",
            "5" => "Gisement d'Emeraude",
            "6" => "Gisement de Rubis"
        );
        $map = $playerSrc->get("map");
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        $dbc = new DBCollection(
            "SELECT x, y, ((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2) as dist FROM Exploitation WHERE name='" . $name[$exploitationType] .
                 "' AND level>=" . $niveau . " ORDER BY ((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2)", $db);
        if ($dbc->count() == 0)
            $param["DIRECTION"] = 99;
        else {
            if ($dbc->get("dist") > 6) {
                $param["DIST"] = $dbc->get("dist") + mt_rand(- floor($dbc->get("dist") / 5), floor($dbc->get("dist") / 5));
            } else {
                $param["DIST"] = floor($dbc->get("dist"));
            }
            if ($param["DIST"] <= 30) {
                if ($dbc->get("dist") <= 5)
                    $param["DIRECTION"] = "";
                else {
                    $diffy = $dbc->get("y") - $playerSrc->get("y");
                    $diffx = $dbc->get("x") - $playerSrc->get("x");
                    
                    if ($diffy <= 0 && $diffx <= 0 && abs($diffy) <= abs($diffx))
                        $param["DIRECTION"] = "Ouestan";
                    if ($diffy <= 0 && $diffx <= 0 && abs($diffy) >= abs($diffx))
                        $param["DIRECTION"] = "Nordan";
                        
                        // 2
                    if ($diffy <= 0 && $diffx >= 0 && abs($diffx) <= abs($diffy / 2) && abs($diffy) >= abs($diffx))
                        $param["DIRECTION"] = "Nordan";
                    if ($diffy <= 0 && $diffx >= 0 && abs($diffx) >= abs($diffy / 2))
                        $param["DIRECTION"] = "Izan";
                    if ($diffy <= 0 && $diffx >= 0 && abs($diffy) >= abs($diffx / 2) && abs($diffx) >= abs($diffy))
                        $param["DIRECTION"] = "Izan";
                    if ($diffy <= 0 && $diffx >= 0 && abs($diffy) <= abs($diffx / 2))
                        $param["DIRECTION"] = "Estan";
                        
                        // 3
                    if ($diffx >= 0 && $diffy >= 0 && abs($diffx) >= abs($diffy))
                        $param["DIRECTION"] = "Estan";
                    if ($diffx >= 0 && $diffy >= 0 && abs($diffx) <= abs($diffy))
                        $param["DIRECTION"] = "Sudan";
                        // 4
                    if ($diffy >= 0 && $diffx <= 0 && abs($diffx) <= abs($diffy / 2))
                        $param["DIRECTION"] = "Sudan";
                    if ($diffy >= 0 && $diffx <= 0 && abs($diffx) >= abs($diffy / 2) && abs($diffx) >= abs($diffy))
                        $param["DIRECTION"] = "Dustan";
                    if ($diffy >= 0 && $diffx <= 0 && abs($diffy) >= abs($diffx / 2) && abs($diffx) >= abs($diffy))
                        $param["DIRECTION"] = "Dustan";
                    if ($diffy >= 0 && $diffx <= 0 && abs($diffy) <= abs($diffx / 2))
                        $param["DIRECTION"] = "Ouestan";
                }
                
                $param["X"] = $dbc->get("x");
                $param["Y"] = $dbc->get("y");
            }
        }
        
        $param["NAME"] = $nameExp[$exploitationType];
        $param["NIVEAU"] = $niveau;
    }

    static function CloseGate($playerSrc, $gate, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        
        // Pour le message
        $param["TYPE_ATTACK"] = TALENT_CLOSEGATE_EVENT;
        $param["TYPE_ACTION"] = TALENT_CLOSEGATE;
        
        $param["GATE_LEVEL"] = $gate->get("level");
        
        $chance = mt_rand(0, 100);
        if ($chance < 4 * $gate->get("level") - 4 * $playerSrc->get("level")) {}
        
        $param["GATE_ID"] = $gate->get("id");
        $param["GATE_X"] = $gate->get("x");
        $param["GATE_Y"] = $gate->get("y");
        
        $gate->set("nbNPC", - 1);
        $time = time();
        $time += 3 * 24 * 3600;
        $gate->set("activation", gmdate("Y-m-d H:i:s", $time));
        $gate->updateDB($db);
    }

    /* *************************************************************** SAVOIR FAIRE DE RAFFINAGE **************************************************** */
    static function refine($playerSrc, $ident, $object1, $object2, &$param, &$db)
    {
        $param["TYPE_ACTION"] = $ident;
        
        switch ($ident) {
            case TALENT_IRONREFINE:
                $numTool = 213;
                break;
            case TALENT_LEATHERREFINE:
                $numTool = 214;
                break;
            case TALENT_SCALEREFINE:
                $numTool = 215;
                break;
            case TALENT_LINENREFINE:
                $numTool = 216;
                break;
            case TALENT_PLANTREFINE:
                $numTool = 217;
                break;
            case TALENT_WOODREFINE:
                $numTool = 218;
                break;
            case TALENT_GEMREFINE:
                $numTool = 219;
                break;
            default:
                break;
        }
        
        $param["BROKEN"] = 0;
        $param["TOOL"] = 0;
        $param["GUILD_BONUS"] = 0;
        $param["BONUS"] = 0;
        $param["PRICE"] = 0;
        
        $param["NAME"] = $object1->get("name");
        $param["LEVEL_OBJECT1"] = $object1->get("level");
        $param["LEVEL_OBJECT2"] = $object2->get("level");
        if ($playerSrc->get("room") == WORKSHOP_ROOM && $playerSrc->get("money") >= GUILD_BONUS_PRICE) {
            $dbB = new DBCollection("SELECT level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
            $param["GUILD_BONUS"] = $dbB->get("level") * 7;
            $param["BONUS"] = $param["GUILD_BONUS"];
            $param["PRICE"] = GUILD_BONUS_PRICE;
            $playerSrc->set("money", $playerSrc->get("money") - $param["PRICE"]);
            $playerSrc->updateDB($db);
        } else {
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND durability!= 0 AND weared='YES' AND id_BasicEquipment=" . $numTool, 
                $db);
            $founded = $dbe->count();
            if ($founded) {
                $outils = new Equipment();
                $outils->load($dbe->get("id"), $db);
                $param["TOOL"] = 1;
                $param["TOOL_LEVEL"] = $outils->get("level");
                $param["BONUS"] = max(0, 30 + $outils->get("level") * 8 - 6 * max($param["LEVEL_OBJECT2"], $param["LEVEL_OBJECT1"]));
                
                if ($outils->get("durability") == 1) {
                    $param["BROKEN"] = 1;
                } else {
                    $param["BROKEN"] = 0;
                }
                $outils->set("durability", $outils->get("durability") - 1);
                $outils->updateDB($db);
            }
        }
        
        $param["PERCENT"] = min(100, 
            max(1, 105 - 15 * abs($param["LEVEL_OBJECT2"] - $param["LEVEL_OBJECT1"]) - max($param["LEVEL_OBJECT2"] * 5, $param["LEVEL_OBJECT1"] * 5) + $param["BONUS"]));
        $param["SCORE"] = mt_rand(1, 100);
        
        if ($param["SCORE"] <= $param["PERCENT"]) {
            $param["TYPE_ATTACK"] = TALENT_REFINE_EVENT_SUCCESS;
            if ($param["LEVEL_OBJECT1"] >= $param["LEVEL_OBJECT2"]) {
                $param["FINAL_LEVEL"] = $object1->get("level") + 2;
                $object1->set("level", $param["FINAL_LEVEL"]);
                $object1->updateDB($db);
                $object2->deleteDB($db);
            } else {
                $param["FINAL_LEVEL"] = $object2->get("level") + 2;
                $object2->set("level", $param["FINAL_LEVEL"]);
                $object2->updateDB($db);
                $object1->deleteDB($db);
            }
        } else
            $param["TYPE_ATTACK"] = TALENT_REFINE_EVENT_FAIL;
    }

    /* *************************************************************** SAVOIR FAIRE D'ARTISANAT **************************************************** */
    static function craft($playerSrc, $ident, $id_BasicEquipment, $object, $niveau, &$param, &$db)
    {
        $coef = 20;
        $param["SUCCESS"] = 1;
        $param["FULL"] = 0;
        $param["GUILD_BONUS"] = 0;
        $param["PRICE"] = 0;
        if ($playerSrc->get("room") == WORKSHOP_ROOM && $playerSrc->get("money") >= GUILD_BONUS_PRICE) {
            $dbB = new DBCollection("SELECT level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
            $param["GUILD_BONUS"] = 7 * $dbB->get("level");
            $param["PRICE"] = GUILD_BONUS_PRICE;
            $playerSrc->set("money", $playerSrc->get("money") - $param["PRICE"]);
            $playerSrc->updateDB($db);
        }
        
        switch ($ident) {
            case TALENT_IRONCRAFT:
                $param["TYPE_ATTACK"] = TALENT_IRONCRAFT_EVENT;
                $param["TYPE_ACTION"] = TALENT_IRONCRAFT;
                $param["TALENT_CARAC"] = "Force";
                $upgradeName = "strength";
                $numTool = 207;
                $param["PLAYER_SKILL"] = $playerSrc->getScore("strength") + $param["GUILD_BONUS"];
                break;
            case TALENT_LEATHERCRAFT:
                $param["TYPE_ATTACK"] = TALENT_LEATHERCRAFT_EVENT;
                $param["TYPE_ACTION"] = TALENT_LEATHERCRAFT;
                $numTool = 208;
                $upgradeName = "dexterity";
                $param["TALENT_CARAC"] = "Dextérité";
                $param["PLAYER_SKILL"] = $playerSrc->getScore("dexterity") + $param["GUILD_BONUS"];
                break;
            case TALENT_SCALECRAFT:
                $param["TYPE_ATTACK"] = TALENT_SCALECRAFT_EVENT;
                $param["TYPE_ACTION"] = TALENT_SCALECRAFT;
                $numTool = 209;
                $param["TALENT_CARAC"] = "Vitesse";
                $upgradeName = "speed";
                $param["PLAYER_SKILL"] = $playerSrc->getScore("speed") + $param["GUILD_BONUS"];
                break;
            case TALENT_LINENCRAFT:
                $param["TYPE_ATTACK"] = TALENT_LINENCRAFT_EVENT;
                $param["TYPE_ACTION"] = TALENT_LINENCRAFT;
                $numTool = 210;
                $param["TALENT_CARAC"] = "Maîtrise de la magie";
                $upgradeName = "magicSkill";
                $param["PLAYER_SKILL"] = $playerSrc->getScore("magicSkill") + $param["GUILD_BONUS"];
                $coef += 5;
                break;
            case TALENT_PLANTCRAFT:
                $param["TYPE_ATTACK"] = TALENT_PLANTCRAFT_EVENT;
                $param["TYPE_ACTION"] = TALENT_PLANTCRAFT;
                $numTool = 211;
                $param["TALENT_CARAC"] = "Maîtrise";
                $upgradeName = "magicSkill";
                $coef -= 5;
                $param["PLAYER_SKILL"] = ceil(sqrt($playerSrc->getScore("magicSkill") * $playerSrc->getScore("speed"))) + $param["GUILD_BONUS"];
                break;
            case TALENT_WOODCRAFT:
                $param["TYPE_ATTACK"] = TALENT_WOODCRAFT_EVENT;
                $param["TYPE_ACTION"] = TALENT_WOODCRAFT;
                $numTool = 212;
                $upgradeName = "dexterity";
                $param["TALENT_CARAC"] = "Dextérité";
                // $coef -= 10;
                $param["PLAYER_SKILL"] = $playerSrc->getScore("dexterity") + $param["GUILD_BONUS"];
                break;
            default:
                break;
        }
        
        $dbskill = new DBCollection("SELECT * FROM Talent WHERE id_BasicTalent=" . ($ident - 300) . " and id_Player=" . $playerSrc->get("id"), $db);
        $skill = $dbskill->get("skill");
        
        // Détermination du bonus de l'outils
        $param["BROKEN"] = 0;
        $param["TOOL"] = 0;
        $param["BONUS"] = 0;
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND durability!= 0 AND weared='YES' AND id_BasicEquipment=" . $numTool, $db);
        $founded = $dbe->count();
        if ($founded) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            $param["TOOL_LEVEL"] = $outils->get("level");
            if ($param["NEW"])
                $lev = $niveau;
            else
                $lev = $object->get("level");
            
            $param["BONUS"] = max(2, 8 * ($outils->get("level") + 3) - 6 * $lev);
            
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
            } else {
                $param["BROKEN"] = 0;
            }
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
        }
        
        $param["LEVEL_RAWMATERIAL"] = $object->get("level");
        if ($id_BasicEquipment >= 207 && $id_BasicEquipment <= 212) {
            $param["EQUIP_LEVEL"] = $niveau;
            $param["EQUIP_ID"] = $id_BasicEquipment;
        } elseif ($param["NEW"]) {
            $param["EQUIP_LEVEL"] = min($object->get("level"), $niveau);
            $param["EQUIP_ID"] = $id_BasicEquipment;
        } else {
            $param["EQUIP_LEVEL"] = $object->get("level");
            $param["EQUIP_ID"] = $object->get("id");
            echo $param["EQUIP_ID"];
        }
        
        $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / $coef);
        
        // Si la maitrise d'artisanat n'est pas suffisante, rien ne se fait.
        if ($param["LEVEL_CRAFT"] < $param["EQUIP_LEVEL"]) {
            $param["SUCCESS"] = 0;
            $param["XP"] = 0;
        } else { // Si la maitrise d'artisant est suffisante
            if ($playerSrc->get("modeAnimation") == 0) {
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                if ($ident == TALENT_PLANTCRAFT) {
                    $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2);
                    $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
                } else
                    $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
            }
            
            $param["END"] = 0;
            // Si confection d'un nouvelle objet
            if ($param["NEW"]) {
                // Vitesse de confection
                
                $price = EquipFactory::getPriceBasicEquipment($param["EQUIP_ID"], $param["EQUIP_LEVEL"], $db);
                $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(max(0, 2 * $price - 48))) + 4 * ($skill - 50))) + $param["BONUS"]);
                $object->deleteDB($db);
                
                // Si l'objet à construire est une flèche
                if ($id_BasicEquipment == 55 or $id_BasicEquipment == 56) {
                    $param["END"] = 1;
                    $nb = max(0, 5 - 1.4 * ($param["EQUIP_LEVEL"] - 1) + 0.1 * ($skill - 50) + ($param["BONUS"] / 100) * 3);
                    $param["EQUIP_SUP"] = ceil(floor($nb) - floor(max(0, 3 - 1.4 * ($param["EQUIP_LEVEL"] - 1) + 0.1 * ($skill - 50))));
                    $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $id_BasicEquipment, $db);
                    $param["EQUIP_NAME"] = $dbc->get("name");
                    $param["NB_EQUIP"] = 0;
                    
                    for ($i = 0; $i < floor($nb); $i ++) {
                        $good = 0;
                        if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db))
                            $good = 1;
                        if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 5, $db))
                            $good = 2;
                        
                        if ($good) {
                            $equip = new Equipment();
                            $equip->set("id_Player", $playerSrc->get("id"));
                            $equip->set("level", $param["EQUIP_LEVEL"]);
                            $equip->set("date", gmdate("Y-m-d H:i:s"));
                            $equip->set("durability", 60);
                            $equip->set("progress", 100);
                            $equip->set("id_EquipmentType", $dbc->get("id_EquipmentType"));
                            $equip->set("id_BasicEquipment", $id_BasicEquipment);
                            $equip->set("name", $dbc->get("name"));
                            $equip->set("date", gmdate("Y-m-d H:i:s"));
                            if ($good == 2)
                                $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), "5", $db));
                            
                            $equip->addDBr($db);
                            $param["NB_EQUIP"] ++;
                        }
                    }
                    if ($param["NB_EQUIP"] != floor($nb))
                        $param["FULL"] = 1;
                }
                
                // Si l'objet à construire est une potion
                if ($id_BasicEquipment >= 400 && $id_BasicEquipment <= 410) {
                    $param["END"] = 2;
                    $nb = max(0, 3 - 0.8 * ($param["EQUIP_LEVEL"] - 1) + 0.1 * ($skill - 50) + ($param["BONUS"] / 100) * 3);
                    $param["EQUIP_SUP"] = ceil(floor($nb) - floor(max(0, 3 - 0.8 * ($param["EQUIP_LEVEL"] - 1) + 0.1 * ($skill - 50))));
                    $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $id_BasicEquipment, $db);
                    $param["EQUIP_NAME"] = $dbc->get("name");
                    $param["NB_EQUIP"] = 0;
                    
                    for ($i = 0; $i < floor($nb); $i ++) {
                        $good = 0;
                        if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db))
                            $good = 1;
                        if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 4, $db))
                            $good = 2;
                        
                        if ($good) {
                            $equip = new Equipment();
                            $equip->set("id_Player", $playerSrc->get("id"));
                            $equip->set("level", $param["EQUIP_LEVEL"]);
                            $equip->set("date", gmdate("Y-m-d H:i:s"));
                            $equip->set("durability", 60);
                            $equip->set("progress", 100);
                            $equip->set("id_EquipmentType", $dbc->get("id_EquipmentType"));
                            $equip->set("id_BasicEquipment", $id_BasicEquipment);
                            $equip->set("name", $dbc->get("name"));
                            $equip->set("date", gmdate("Y-m-d H:i:s"));
                            if ($good == 2)
                                $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), "4", $db));
                            
                            $equip->addDBr($db);
                            $param["NB_EQUIP"] ++;
                        }
                    }
                    if ($param["NB_EQUIP"] != floor($nb))
                        $param["FULL"] = 1;
                }
                
                // Si l'objet n'est pas une flèche ni une potion de niveau petit
                if (! $param["END"]) {
                    $good = 1;
                    // Outils
                    if ($id_BasicEquipment >= 207 and $id_BasicEquipment <= 212) {
                        if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 6, $db))
                            $good = 2;
                    }
                    $param["PROGRESS"] = $param["SPEED"];
                    $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $id_BasicEquipment, $db);
                    $param["EQUIP_NAME"] = $dbc->get("name");
                    $equip = new Equipment();
                    $equip->set("id_EquipmentType", $dbc->get("id_EquipmentType"));
                    $equip->set("id_BasicEquipment", $id_BasicEquipment);
                    $equip->set("id_Player", $playerSrc->get("id"));
                    $equip->set("level", $param["EQUIP_LEVEL"]);
                    $equip->set("date", gmdate("Y-m-d H:i:s"));
                    $coef = 1;
                    if ($equip->get("id_BasicEquipment") >= 200 && $equip->get("id_BasicEquipment") <= 206 && $equip->get("id_BasicEquipment") != 205)
                        $coef = $equip->get("level");
                    if ($id_BasicEquipment >= 207 and $id_BasicEquipment <= 212 and $good == 2) {
                        $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), "6", $db));
                    }
                    $equip->set("durability", $dbc->get("durability") * $coef);
                    $equip->set("progress", $param["SPEED"]);
                    
                    $equip->set("name", $dbc->get("name"));
                    $equip->set("date", gmdate("Y-m-d H:i:s"));
                    $equip->addDBr($db);
                }
            } else { // Suite d'un objet déjà commencé
                
                $price = EquipFactory::getPriceEquipment($param["EQUIP_ID"], $db);
                $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(max(0, 2 * $price - 48))) + 4 * ($skill - 50))) + $param["BONUS"]);
                $param["EQUIP_NAME"] = $object->get("name");
                if (($param["PROGRESS"] = $param["SPEED"] + $object->get("progress")) >= 100)
                    $param["PROGRESS"] = 100;
                
                $dbc = new DBCollection("UPDATE Equipment SET progress=" . $param["PROGRESS"] . " WHERE id=" . $object->get("id"), $db, 0, 0, false);
            }
        }
    }

    /* *************************************************************** SAVOIR FAIRE D'ARTISANAT **************************************************** */
    static function enchant($playerSrc, $object, $gem, $wood, $id_Template, $niveau, &$param, &$db)
    {
        $param["TYPE_ATTACK"] = TALENT_GEMCRAFT_EVENT;
        $param["TYPE_ACTION"] = TALENT_GEMCRAFT;
        $upgradeName = "magicSkill";
        $param["BROKEN"] = 0;
        $param["TOOL"] = 0;
        $param["BONUS"] = 0;
        $param["SUCCESS"] = 1;
        
        $dbskill = new DBCollection("SELECT * FROM Talent WHERE id_BasicTalent=13 and id_Player=" . $playerSrc->get("id"), $db);
        $skill = $dbskill->get("skill");
        
        $param["GUILD_BONUS"] = 0;
        $param["PRICE"] = 0;
        if ($playerSrc->get("room") == WORKSHOP_ROOM && $playerSrc->get("money") >= GUILD_BONUS_PRICE) {
            $dbB = new DBCollection("SELECT level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
            $param["GUILD_BONUS"] = 7 * $dbB->get("level");
            $param["PRICE"] = GUILD_BONUS_PRICE;
            $playerSrc->set("money", $playerSrc->get("money") - $param["PRICE"]);
            $playerSrc->updateDB($db);
        }
        
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $playerSrc->get("id") . " AND durability!= 0 AND weared='YES' AND id_BasicEquipment=220", $db);
        $founded = $dbe->count();
        if ($founded) {
            $outils = new Equipment();
            $outils->load($dbe->get("id"), $db);
            $param["TOOL"] = 1;
            $param["TOOL_LEVEL"] = $outils->get("level");
            if ($param["CRAFT_TYPE"] != 2)
                $lev = $niveau;
            else {
                if ($object->get("progress") < 100)
                    $lev = $object->get("level");
                else {
                    $dbc = new DBCollection(
                        "SELECT Template.level as level, Template.id as id, BasicTemplate.name as name, BasicTemplate.name2 as name2 FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id = Template.id_BasicTemplate WHERE Template.id_Equipment =" .
                             $object->get("id") . " order by Template.pos asc", $db);
                    if ($dbc->count() < 2)
                        $lev = $dbc->get("level");
                    else {
                        $dbc = new DBCollection(
                            "SELECT Template.level as level, Template.id as id, BasicTemplate.name as name, BasicTemplate.name2 as name2 FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id = Template.id_BasicTemplate WHERE Template.pos = 2 AND Template.id_Equipment =" .
                                 $object->get("id"), $db);
                        $lev = $dbc->get("level");
                    }
                }
            }
            $param["BONUS"] = max(2, 8 * ($outils->get("level") + 3) - 6 * $lev);
            
            if ($outils->get("durability") == 1) {
                $param["BROKEN"] = 1;
            } else {
                $param["BROKEN"] = 0;
            }
            $outils->set("durability", $outils->get("durability") - 1);
            $outils->updateDB($db);
        }
        
        $param["PLAYER_SKILL"] = $playerSrc->getScore("magicSkill") + $param["GUILD_BONUS"];
        
        $param["END"] = 0;
        switch ($param["CRAFT_TYPE"]) {
            // Créer un bâton ou un sceptre
            case 1:
                if ($param["EQUIP_ID"] == 220)
                    $param["EQUIP_LEVEL"] = $niveau;
                else {
                    $param["WOOD_LEVEL"] = $wood->get("level");
                    $param["GEM_LEVEL"] = $gem->get("level");
                    $param["EQUIP_LEVEL"] = min($param["WOOD_LEVEL"], $param["GEM_LEVEL"], $niveau);
                }
                $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / 25);
                
                if ($param["LEVEL_CRAFT"] < $param["EQUIP_LEVEL"]) {
                    $param["SUCCESS"] = 0;
                    $param["XP"] = 0;
                } else {
                    if ($playerSrc->get("modeAnimation") == 0) {
                        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                        $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
                    }
                    $price = EquipFactory::getPriceBasicEquipment($param["EQUIP_ID"], $param["EQUIP_LEVEL"], $db);
                    $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(max(0, 2 * $price - 48))) + 4 * ($skill - 50))) + $param["BONUS"]);
                    
                    $param["PROGRESS"] = $param["SPEED"];
                    $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $param["EQUIP_ID"], $db);
                    $param["EQUIP_NAME"] = $dbc->get("name");
                    $equip = new Equipment();
                    $equip->set("id_Player", $playerSrc->get("id"));
                    $equip->set("level", $param["EQUIP_LEVEL"]);
                    $equip->set("date", gmdate("Y-m-d H:i:s"));
                    $equip->set("durability", 60);
                    $equip->set("progress", $param["SPEED"]);
                    $equip->set("id_EquipmentType", $dbc->get("id_EquipmentType"));
                    $equip->set("id_BasicEquipment", $dbc->get("id"));
                    $equip->set("name", $dbc->get("name"));
                    $equip->set("date", gmdate("Y-m-d H:i:s"));
                    $equip->addDBr($db);
                    
                    $gem->deleteDB($db);
                    $wood->deleteDB($db);
                }
                break;
            // Continuer une pièce
            case 2:
                // Sceptre ou baton a continuer
                $param["EQUIP_ID"] = $object->get("id");
                if ($object->get("progress") < 100) {
                    $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / 25);
                    $param["EQUIP_LEVEL"] = $object->get("level");
                    if ($param["LEVEL_CRAFT"] < $param["EQUIP_LEVEL"]) {
                        $param["SUCCESS"] = 0;
                        $param["XP"] = 0;
                    } 

                    else {
                        if ($playerSrc->get("modeAnimation") == 0) {
                            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                            $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
                        }
                        $price = EquipFactory::getPriceEquipment($param["EQUIP_ID"], $db);
                        $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(2 * $price - 48)) + 4 * ($skill - 50))) + $param["BONUS"]);
                        
                        $param["EQUIP_NAME"] = $object->get("name");
                        if (($param["PROGRESS"] = $param["SPEED"] + $object->get("progress")) >= 100)
                            $param["PROGRESS"] = 100;
                        
                        $dbc = new DBCollection("UPDATE Equipment SET progress=" . $param["PROGRESS"] . " WHERE id=" . $object->get("id"), $db, 0, 0, false);
                    }
                }
                // Si on continue un enchantement
                if ($object->get("templateProgress") < 100 && $object->get("templateProgress") > 0) {
                    $param["EQUIP_LEVEL"] = $object->get("level");
                    $param["CRAFT_TYPE"] = 5;
                    $dbc = new DBCollection(
                        "SELECT Template.level as level, Template.id as id, BasicTemplate.name as name, BasicTemplate.name2 as name2 FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id = Template.id_BasicTemplate WHERE Template.id_Equipment =" .
                             $object->get("id"), $db);
                    
                    // on continue un template mineur
                    if ($dbc->count() < 2) {
                        $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / 40);
                        $param["TEMPLATE_LEVEL"] = $dbc->get("level");
                        if ($param["LEVEL_CRAFT"] < $param["TEMPLATE_LEVEL"])
                            $param["SUCCESS"] = 0;
                        else {
                            if ($playerSrc->get("modeAnimation") == 0) {
                                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                                $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
                            }
                            $price = EquipFactory::getPriceEnchant($param["TEMPLATE_LEVEL"], $db);
                            $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(2 * $price - 48)) + 4 * ($skill - 50))) + $param["BONUS"]);
                            $param["EQUIP_NAME"] = $object->get("name");
                            $param["EQUIP_LEVEL"] = "(" . $object->get("level") . ") " . $object->get("extraname");
                            
                            if (($param["PROGRESS"] = $param["SPEED"] + $object->get("templateProgress")) >= 100)
                                $param["PROGRESS"] = 100;
                            
                            $dbc = new DBCollection("UPDATE Equipment SET templateProgress=" . $param["PROGRESS"] . " WHERE id=" . $object->get("id"), $db, 0, 0, false);
                        }
                    } else { // on continue un template majeur
                        
                        $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / 60);
                        $dbc = new DBCollection(
                            "SELECT Template.level as level, Template.id as id, BasicTemplate.name as name, BasicTemplate.name2 as name2 FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id = Template.id_BasicTemplate WHERE Template.pos = 2 AND Template.id_Equipment =" .
                                 $object->get("id"), $db);
                        $param["TEMPLATE_LEVEL"] = $dbc->get("level");
                        if ($param["LEVEL_CRAFT"] < $param["TEMPLATE_LEVEL"])
                            $param["SUCCESS"] = 0;
                        else {
                            if ($playerSrc->get("modeAnimation") == 0) {
                                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                                $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
                            }
                            $price = EquipFactory::getPriceMajorEnchant($param["TEMPLATE_LEVEL"], $db);
                            $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(2 * $price - 48)) + 4 * ($skill - 50))) + $param["BONUS"]);
                            $param["EQUIP_NAME"] = $object->get("name");
                            $dbx = new DBCollection(
                                "SELECT level, BasicTemplate.name as name FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id = Template.id_BasicTemplate WHERE Template.pos = 1 AND Template.id_Equipment =" .
                                     $object->get("id"), $db);
                            
                            $param["EQUIP_LEVEL"] = "(" . $object->get("level") . ") " . $dbx->get("name") . " (" . $dbx->get("level") . ") " . $dbc->get("name2") . " (" .
                                 $param["TEMPLATE_LEVEL"] . ")";
                            
                            if (($param["PROGRESS"] = $param["SPEED"] + $object->get("templateProgress")) >= 100)
                                $param["PROGRESS"] = 100;
                            
                            $dbc = new DBCollection("UPDATE Equipment SET templateProgress=" . $param["PROGRESS"] . " WHERE id=" . $object->get("id"), $db, 0, 0, false);
                        }
                    }
                }
                break;
            // Enchantement mineur
            case 3:
                $param["GEM_LEVEL"] = $gem->get("level");
                $param["EQUIP_NAME"] = $object->get("name");
                $param["EQUIP_LEVEL"] = $object->get("level");
                $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / 40);
                $param["TEMPLATE_LEVEL"] = min($param["GEM_LEVEL"], $object->get("level"), $niveau);
                if ($param["LEVEL_CRAFT"] < $param["TEMPLATE_LEVEL"]) {
                    $param["SUCCESS"] = 0;
                    $param["XP"] = 0;
                } 

                else {
                    $gem->deleteDB($db);
                    if ($playerSrc->get("modeAnimation") == 0) {
                        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                        $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
                    }
                    $price = EquipFactory::getPriceEnchant($param["TEMPLATE_LEVEL"], $db);
                    
                    $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(2 * $price - 48)) + 4 * ($skill - 50))) + $param["BONUS"]);
                    
                    $dbc = new DBCollection("SELECT name FROM BasicTemplate WHERE id=" . $id_Template, $db);
                    $param["TEMPLATE_NAME"] = $dbc->get("name") . " (" . $param["TEMPLATE_LEVEL"] . ")";
                    
                    $param["PROGRESS"] = min($param["SPEED"], 100);
                    
                    $object->set("extraname", $param["TEMPLATE_NAME"]);
                    $object->set("templateProgress", $param["PROGRESS"]);
                    $object->updateDB($db);
                    
                    $template = new Template();
                    $template->set("id_BasicTemplate", $id_Template);
                    $template->set("id_Equipment", $object->get("id"));
                    $template->set("pos", 1);
                    $template->set("level", $param["TEMPLATE_LEVEL"]);
                    $template->addDbr($db);
                }
                break;
            // Enchantement majeur
            case 4:
                $param["GEM_LEVEL"] = $gem->get("level");
                $param["EQUIP_NAME"] = $object->get("name");
                $param["LEVEL_CRAFT"] = 1 + floor($param["PLAYER_SKILL"] / 60);
                $param["TEMPLATE_LEVEL"] = min($param["GEM_LEVEL"], $object->get("level"), $niveau);
                $param["EQUIP_LEVEL"] = $object->get("level");
                if ($param["LEVEL_CRAFT"] < $param["TEMPLATE_LEVEL"]) {
                    $param["SUCCESS"] = 0;
                    $param["XP"] = 0;
                } 

                else {
                    $gem->deleteDB($db);
                    if ($playerSrc->get("modeAnimation") == 0) {
                        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                        $playerSrc->setSub("Upgrade", $upgradeName, $playerSrc->getSub("Upgrade", $upgradeName) + 4);
                    }
                    // if($param["TOOL"])
                    // $param["BONUS"] = floor(10 * ($outils->get("level"))/($param["TEMPLATE_LEVEL"]));
                    
                    $price = EquipFactory::getPriceMajorEnchant($param["TEMPLATE_LEVEL"], $db);
                    
                    $param["SPEED"] = min(100, min(100, max(1, 100 - floor(10 * sqrt(2 * $price - 48)) + 4 * ($skill - 50))) + $param["BONUS"]);
                    
                    $dbc = new DBCollection("SELECT name2 FROM BasicTemplate WHERE id=" . $id_Template, $db);
                    $param["TEMPLATE_NAME"] = $object->get("extraname") . " " . $dbc->get("name2") . " (" . $param["TEMPLATE_LEVEL"] . ")";
                    
                    $param["PROGRESS"] = min($param["SPEED"], 100);
                    
                    $object->set("extraname", $param["TEMPLATE_NAME"]);
                    $object->set("templateProgress", $param["PROGRESS"]);
                    $object->updateDB($db);
                    
                    $template = new Template();
                    $template->set("id_BasicTemplate", $id_Template);
                    $template->set("id_Equipment", $object->get("id"));
                    $template->set("pos", 2);
                    $template->set("level", $param["TEMPLATE_LEVEL"]);
                    $template->addDbr($db);
                }
                break;
        }
    }
}
?>
