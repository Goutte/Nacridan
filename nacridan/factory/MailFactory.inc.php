<?php
require_once (HOMEPATH . "/class/MailBody.inc.php");
require_once (HOMEPATH . "/class/MailSend.inc.php");
require_once (HOMEPATH . "/translation/Translation.php");
require_once (HOMEPATH . "/class/Mail.inc.php");
require_once (HOMEPATH . "/class/MailTrade.inc.php");
require_once (HOMEPATH . "/class/MailAlert.inc.php");

class MailFactory
{

    static function getArrayCond($array, $condName)
    {
        $msg = "";
        $first = 1;
        
        if (isset($array)) {
            foreach ($array as $key => $val) {
                if ($first)
                    $msg = $condName . "=" . $val;
                else
                    $msg .= " OR " . $condName . "=" . $val;
                $first = 0;
            }
        }
        return $msg;
    }

    static function getNamesCond($names, $limit)
    {
        $first = 1;
        $cond = "";
        $cptOrig = 0;
        foreach ($names as $name) {
            if ($cptOrig == $limit)
                break;
            
            $dest = trim($name);
            if ($dest != "") {
                if ($first) {
                    $first = 0;
                    $cond = "name=\"" . $dest . "\"";
                } else {
                    $cond .= "OR name=\"" . $dest . "\"";
                }
                $cptOrig ++;
            }
        }
        return $cond;
    }

    static function sendSingleMsgById($playerSrc, $title, $body, $array, $type, &$err, $db, $limit = 10, $savesend = 1)
    {
        $cond = self::getArrayCond($array, "id");
        
        $dbp = new DBCollection("SELECT id,name FROM Player WHERE " . $cond, $db);
        
        while (! $dbp->eof()) {
            $dest[$dbp->get("name")] = $dbp->get("name");
            $dbp->next();
        }
        
        $err2 = "";
        foreach ($array as $name) {
            self::sendMsg($playerSrc, $title, $body, array(
                $name => $name
            ), $type, $err2, $db, $limit, $savesend);
        }
        $err = $err2;
    }

    static function sendSingleDestMsg($playerSrc, $title, $body, $array, $type, &$err, $db, $limit = 10, $savesend = 1)
    {
        $err2 = "";
        foreach ($array as $name) {
            self::sendMsg($playerSrc, $title, $body, array(
                $name => $name
            ), $type, $err2, $db, $limit, $savesend);
        }
        $err = $err2;
    }

    static function sendMsg($playerSrc, $title, $body, $array, $type, &$err, $db, $limit = 10, $savesend = 1)
    {
        if ($playerSrc->get("id_Mission") != 0) {
            $err = localize("ERREUR !!! Expéditeur non valide !!! ");
            return - 1;
        }
        
        $datetime = gmdate("Y-m-d H:i:s");
        
        if ($title == "") {
            $title = localize("(pas de titre)");
        }
        
        $first = 1;
        $cond = "";
        $cptOrig = 0;
        
        $array2 = array();
        
        foreach ($array as $name) {
            $dest = trim($name);
            
            if (isset($name{0}) && $name{0} == "[") {
                if ($name == "[Tout l'Ordre]") {
                    if ($playerSrc->get("id_Team") > 0) {
                        $dbt = new DBCollection(
                            "select name from Player p1 inner join (SELECT min(id)as id FROM Player WHERE id_Team=" . $playerSrc->get("id_Team") .
                                 " group by id_Member) p on p1.id=p.id", $db);
                        while (! $dbt->eof()) {
                            $array2[$dbt->get("name")] = 1;
                            $dbt->next();
                        }
                    }
                } else {
                    
                    $aliasName = substr($name, 1, - 1);
                    $dbs = new DBCollection("SELECT alias FROM MailAlias WHERE id_Player=" . $playerSrc->get("id") . " AND name=\"" . addslashes($aliasName) . "\"", $db);
                    if (! $dbs->eof()) {
                        $arr = array_keys(unserialize($dbs->get("alias")));
                        foreach ($arr as $value)
                            $array2[$value] = 1;
                    }
                }
            } else {
                $array2[$name] = 1;
            }
        }
        
        foreach ($array2 as $name => $val) {
            // if($cptOrig==$limit)
            // break;
            
            $dest = trim($name);
            
            if ($dest != "") {
                if ($first) {
                    $first = 0;
                    $cond = "name='" . quote_smart($dest) . "' ";
                } else {
                    $cond .= "OR name='" . quote_smart($dest) . "' ";
                }
                $cptOrig ++;
            }
        }
        
        if ($cond == "") {
            $err = localize("ERREUR !!! Destinataire(s) non valide !!! ");
            return - 1;
        }
        
        $idSender = $playerSrc->get("id");
        $dbp = new DBCollection("SELECT id,name FROM Player WHERE " . $cond, $db);
        $dest = "";
        $destid = "";
        $first = 1;
        $cpt = 0;
        while (! $dbp->eof()) {
            if ($first) {
                $first = 0;
                $dest = $dbp->get("name");
                $destid = $dbp->get("id");
            } else {
                $dest .= "," . $dbp->get("name");
                $destid .= "," . $dbp->get("id");
            }
            $cpt ++;
            $dbp->next();
        }
        
        if ($cpt == 0) {
            $err = localize("ERREUR !!! Destinataire(s) non valide !!! ");
            return - 1;
        }
        
        $mailBody = new MailBody();
        $mailBody->set("id_Player", $idSender);
        $mailBody->set("recipientid", $destid);
        $mailBody->set("recipient", $dest);
        $mailBody->set("body", $body);
        
        if ($savesend)
            $mailBody->set("cpt", $cpt + 1);
        else
            $mailBody->set("cpt", $cpt);
        
        $mailBody->set("title", stripslashes($title));
        $mailBody->set("date", $datetime);
        $mailBody->set("type", $type);
        $idBody = $mailBody->addDB($db);
        
        if ($savesend) {
            if ($type != 1 || $type != 2) {
                $mailSend = new MailSend();
                $mailSend->set("id_Player", $idSender);
                $mailSend->set("id_MailBody", ($idBody));
                $mailSend->addDB($db);
            }
        }
        
        $ret = 0;
        $dbp->first();
        while (! $dbp->eof() && $ret != - 1) {
            
            // echo $dbp->get("id")."/".$dbp->get("name");
            
            if ($type == 0) {
                $mail = new Mail();
                $mail->set("id_Player\$sender", $idSender);
                $mail->set("id_Player\$receiver", $dbp->get("id"));
                $mail->set("id_MailBody", $idBody);
                $mail->set("title", stripslashes($title));
                $mail->set("date", $datetime);
                $ret = $mail->addDB($db);
            } elseif ($type == 1) {
                $mailTrade = new MailTrade();
                $mailTrade->set("id_Player\$sender", $idSender);
                $mailTrade->set("id_Player\$receiver", $dbp->get("id"));
                $mailTrade->set("id_MailBody", $idBody);
                $mailTrade->set("title", stripslashes($title));
                $mailTrade->set("date", $datetime);
                $ret = $mailTrade->addDB($db);
            } elseif ($type == 2) {
                $mailAlert = new MailAlert();
                $mailAlert->set("id_Player\$sender", $idSender);
                $mailAlert->set("id_Player\$receiver", $dbp->get("id"));
                $mailAlert->set("id_MailBody", $idBody);
                $mailAlert->set("title", stripslashes($title));
                $mailAlert->set("date", $datetime);
                $ret = $mailAlert->addDB($db);
            }
            
            $dbs = new DBCollection(
                "SELECT mailMsg,email FROM Player LEFT JOIN Member ON  Member.id=Player.id_Member LEFT JOIN MemberOption ON Member.id=MemberOption.id_Member LEFT JOIN MemberDetail ON Member.id=MemberDetail.id_Member WHERE Player.id=" .
                     $dbp->get("id"), $db);
            
            if (! $dbs->eof()) {
                if ($dbs->get("mailMsg") == 1)
                    self::sendEmail($dbs->get("email"), $dbp->get("name"), $idSender, $playerSrc->get("name"), $body, stripslashes($title), $res);
            }
            
            $dbp->next();
        }
        
        if ($ret != - 1) {
            if ($cpt != $cptOrig) {
                $err = " (" . localize("ATTENTION !!! Message envoyé mais certains destinataires sont inconnus !!! ") . ")";
                return - 1;
            } else {
                $err = localize("Votre Message a bien été envoyé");
                return 0;
            }
        } else {
            $err = localize("Problème de messagerie, aucun message délivré !!!");
            return - 1;
        }
        return 0;
    }

    static function sendEmail($email, $name, $idSender, $nameSender, $body, $title, &$res)
    {
        if (SMTPDOMAIN != "nacridan.com")
            return;
        
        require_once (HOMEPATH . "/lib/swift/Swift.php");
        require_once (HOMEPATH . "/lib/swift/Swift/Connection/SMTP.php");
        
        $subject = "[Nacridan] Évènement de type E-mail: " . $title;
        $message = "Votre Personnage (" . $name . ") a reçu un nouveau message de la part de " . $nameSender . " (" . $idSender . ")\n\n";
        $message .= "-------------------------------------\n";
        $message .= $title . "\n";
        $message .= "-------------------------------------\n";
        $message .= $body . "\n";
        $message .= "-------------------------------------\n\n\n\n";
        $message .= "-------------------------------------\n";
        $message .= "Vous pouvez aussi le consulter sur " . CONFIG_ROOT . " dans la section 'Messages'\n\n";
        $message .= "-------------------------------------\n";
        $message .= "Rappel : Si vous ne désirez plus recevoir cet E-Mail, allez dans la section 'Options' puis 'membre' de votre compte pour désactiver l'envoi automatique.\n";
        $message .= "-------------------------------------\n";
        
        $to = $email;
        $mailer = new Swift(new Swift_Connection_SMTP(SMTPHOST));
        if ($mailer->isConnected()) // Optional
{
            
            if (SMTPLOGIN != "" && SMTPPASS != "") {
                require_once (HOMEPATH . "/lib/swift/Swift/Authenticator/PLAIN.php");
                $mailer->loadAuthenticator(new Swift_Authenticator_PLAIN());
                $mailer->authenticate(SMTPLOGIN, SMTPPASS);
            }
            
            // Add as many parts as you need here
            $mailer->addPart($message);
            $mailer->addPart(str_replace("\n", '<br />', $message), 'text/html');
            $mailer->setCharset("UTF-8");
            $mailer->send($to, "Nacridan <noreply@" . SMTPDOMAIN . ".>", $subject);
            
            $mailer->close();
        }
    }
}
?>
