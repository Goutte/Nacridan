<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");
require_once (HOMEPATH . "/class/BM.inc.php");
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");
require_once (HOMEPATH . "/lib/Map.inc.php");

class PlayerFactory
{

    static private function globalInfo($playerSrc, &$param)
    {
        $param["PLAYER_NAME"] = $playerSrc->get("name");
        if ($param["PLAYER_NAME"] == "")
            $param["PLAYER_NAME"] = $playerSrc->get("racename");
        $param["PLAYER_RACE"] = $playerSrc->get("id_BasicRace");
        $param["PLAYER_ID"] = $playerSrc->get("id");
        $param["PLAYER_GENDER"] = $playerSrc->get("gender");
        $param["PLAYER_LEVEL"] = $playerSrc->get("level");
        $param["MEMBER_ID"] = $playerSrc->get("id_Member");
        
        if ($playerSrc->get("status") == "NPC")
            $param["PLAYER_IS_NPC"] = 1;
        else
            $param["PLAYER_IS_NPC"] = 0;
    }

    static public function addHP($playerSrc, $value, $db, $autoUpdate)
    {
        $regen = 0;
        if ($playerSrc->get("currhp") < $playerSrc->getScore('hp')) {
            $currhp = $playerSrc->get("currhp");
            $newhp = min($currhp + $value, $playerSrc->getObj("Modifier")->getScore("hp"));
            $playerSrc->set("currhp", $newhp);
            $regen = $newhp - $currhp;
        }
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return $regen;
    }

    static public function loseHP($player, $opponent, $value, &$param, $db)
    {
        if ($opponent->get("status") == 'PC' or $opponent->get("id_Member") != 0)
            BasicActionFactory::legalAction($opponent, $player, $param, $db, 0);
        
        $player->set("currhp", $player->get("currhp") - $value);
        
        if ($player->get("status") == "NPC" && $player->get("id_Member") == 0) {
            require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
            PNJFactory::runIA($player, $opponent->get("id"), $db);
        }
        
        if ($player->get("status") == 'PC' && $player->get("id") != $opponent->get("id")) {
            self::bonusExorcisme($opponent, $player, $value, $db);
        }
        SpellFactory::passiveSun($player, $value, $db);
        $player->updateDB($db);
        
        if ($player->get("status") == "NPC" && ($player->get("racename") == "Garde du Palais" || $player->get("racename") == "Prêtre")) {
            $building = new Building();
            $building->load($player->get("inbuilding"), $db);
            
            $city = new City();
            $city->load($building->get("id_City"), $db);
            
            $subject = "Le " . $player->get("racename") . " de " . $city->get("name") . " a été attaqué!";
            $body = "\nLe " . $player->get("racename") . " de " . $city->get("name") . " située en " . $city->get("x") . "/" . $city->get("y") . " a été attaqué par " .
                 $opponent->get("name") . "(" . $opponent->get("id") . ")";
            
            InsideBuildingFactory::informCityMember($building->get("id_City"), $subject, $body, $db);
        }
    }

    static function bonusExorcisme($playerSrc, $opponent, $damage, $db)
    {
        $dba = new DBCollection("SELECT id FROM Ability WHERE id_Player=" . $opponent->get("id") . " AND id_BasicAbility=16", $db);
        if (! $dba->eof() && $damage != 0) {
            $bm = new BM();
            $bm->set("name", "Exorcisme de l'ombre");
            $bm->set("life", 3);
            $bm->set("value", $damage);
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $opponent->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
        }
    }

    static function newATB($playerSrc, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        $newtime = gmstrtotime($playerSrc->get("nextatb"));
        $newDLA = 0;
        $param["DLA"] = 0;
        $param["TYPE_ACTION"] = NEWATB;
        if ($newtime < (time() + date("I") * 3600)) {
            $newDLA = 1;
            $param["DLA"] = 1;
            $dbu = new DBCollection("UPDATE Event SET active=0 WHERE active=1 AND id_Player\$src=" . $playerSrc->get("id"), $db, 0, 0, false);
            $nextatb = $newtime;
            $time = time() - $nextatb;
            $atb = $playerSrc->get("atb") * 60;
            $ratio = floor($time / $atb);
            if ($ratio < 0) // semble inutile ce test car si ratio < 0, on passe par le if
                $ratio = 0;
            
            $nextatb += $atb + ($atb * $ratio);
            while ($nextatb < (time() + date("I") * 3600)) // semble encore inutile.
                $nextatb += $atb;
            
            $playerSrc->set("curratb", $playerSrc->get("nextatb"));
            $playerSrc->set("nextatb", gmdate("Y-m-d H:i:s", $nextatb));
            $curatb = $nextatb - $playerSrc->get("atb") * 60;
            $playerSrc->set("playatb", gmdate("Y-m-d H:i:s", $curatb));
            if (isset($_SERVER['REMOTE_ADDR']))
                $playerSrc->set("ip", $_SERVER['REMOTE_ADDR']);
            
            $playerSrc->set("ip_data", "0");
            $playerSrc->updateHidden($db);
            if ($playerSrc->get("advert") > 0)
                $playerSrc->set("advert", ($playerSrc->get("advert") - 1));
            
            if ($playerSrc->get("state") != "prison" && $playerSrc->get("status") == 'PC')
                $playerSrc->set("prison", max(0, $playerSrc->get("prison") - 0.1));
                
                // Test de maintien en vie du Feu-Fol
            $param["GOLEM_NOT_ENOUGH_MM"] = 0;
            $dbc = new DBCollection("SELECT * FROM Player WHERE id_BasicRace=" . ID_BASIC_RACE_FEU_FOL . " AND id_Owner=" . $playerSrc->get("id"), $db);
            if (! $dbc->eof()) {
                if ($playerSrc->getScore("magicSkill") < 2 * $dbc->get("level")) {
                    $golem = new Player();
                    $golem->load($dbc->get("id"), $db);
                    
                    $param3 = array();
                    $param3["TARGET_ID"] = $playerSrc->get("id");
                    $param3["TARGET_RACE"] = $playerSrc->get("id_BasicRace");
                    $param3["TYPE_ATTACK"] = SPELL_FEU_FOL_DEATH_MM_EVENT;
                    $param3["PLAYER_ID"] = $golem->get("id");
                    $param3["MEMBER_ID"] = 0;
                    $param3["PLAYER_RACE"] = ID_BASIC_RACE_FEU_FOL;
                    $param3["ERROR"] = 0;
                    $param3["TYPE_ACTION"] = SPELL_FEU_FOL_DEATH_MM;
                    Event::logAction($db, SPELL_FEU_FOL_DEATH_MM, $param3);
                    unset($param3);
                    
                    PNJFactory::deleteDBnpc($golem, $param2, $db);
                    $param["GOLEM_NOT_ENOUGH_MM"] = 1;
                }
            }
        }
        
        $playerSrc->set("hp", $playerSrc->getModif("hp", DICE_ADD));
        $modif = $playerSrc->getObj("Modifier");
        self::initBM($playerSrc, $modif, $param, $db, $newDLA);
        
        if ($newDLA) {
            $param["AP"] = min(
                max(12 + floor(($playerSrc->getObj("Modifier")->getModif("speed", DICE_D6) + $playerSrc->getObj("Modifier")->getModif("speed_bm", DICE_D6)) / 10), 11), 18);
            $playerSrc->set("ap", $param["AP"]);
            if ($playerSrc->get("overload") > 0)
                $playerSrc->set("ap", floor($playerSrc->get("ap") / (2 * $playerSrc->get("overload"))));
        }
        
        $param["RESURRECT"] = 0;
        if ($playerSrc->get("resurrect") == 1) {
            $param["RESURRECT"] = 1;
            $playerSrc->set("resurrect", 0);
        }
        
        $param["BOLAS"] = 0;
        if ($playerSrc->get("state") == "creeping")
            $param["BOLAS"] = 1;
            
            // Gestion des caravanes
        $param["LATE_CARAVAN"] = 0;
        $param["LOST_TURTLE"] = 0;
        $param["BOLAS_TURTLE"] = 0;
        
        // Test pour savoir si la tortue est morte
        $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $playerSrc->get("id"), $db, 0, 0);
        if ($dbc->count() > 0) {
            $dbt = new DBCollection("SELECT id, state FROM Player WHERE id_Member=" . $playerSrc->get("id_Member") . " AND id_Caravan=" . $dbc->get("id"), $db, 0, 0);
            if ($dbt->count() == 0) {
                $param["LOST_TURTLE"] = 1;
                $playerSrc->set("merchant_level", max(0.1, $playerSrc->get("merchant_level") - 0.8));
                $dbdp = new DBCollection("DELETE FROM Equipment WHERE id_BasicEquipment=600 AND id_Player=" . $playerSrc->get("id") . " AND id_Caravan=" . $dbc->get("id"), $db, 0, 
                    0, false);
                $dbdc = new DBCollection("DELETE FROM Caravan WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
            } 

            elseif ($dbt->get("state") == "creeping")
                $param["BOLAS_TURTLE"] = 1;
        }
        
        // Récupération du nombre de message non lu
        $dbm = new DBCollection("SELECT count(id) as num FROM Mail WHERE new=1 and id_Player\$receiver =" . $playerSrc->get("id"), $db);
        $dbmTrd = new DBCollection("SELECT count(id) as num FROM MailTrade WHERE new=1 and id_Player\$receiver =" . $playerSrc->get("id"), $db);
        $param["MSG"] = $dbm->get("num");
        $param["MSG_TRD"] = $dbmTrd->get("num");
        
        // Test pour savoir si une mission est en retard
        $dbcdate = new DBCollection("SELECT * FROM Caravan WHERE date<NOW() AND id_Player=" . $playerSrc->get("id"), $db, 0, 0);
        if ($dbcdate->count() > 0) {
            $param["LATE_CARAVAN"] = 1;
            $playerSrc->set("merchant_level", max(0.1, $playerSrc->get("merchant_level") - 0.2));
            // $date = time() + 2*24*60*60;
            // $dbh = new DBCollection("UPDATE Caravan SET date='".gmdate('Y-m-d H:i:s', $date)." WHERE id=".$dbc->get("id"), $db,0,0,false);
        }
        // Fin de gestion des caravances
        // ------------------------------------------------ Escorte -------------------------------------------------
        // Gestion des missions en cours
        $param["NPC_ESCORT_DIED"] = 0;
        $param["LATE_ESCORT"] = 0;
        // Test pour savoir si le pnj a escorter est mort
        
        $dbmission = new DBcollection("SELECT * FROM Mission WHERE Mission_Active=1 AND id_Player=" . $playerSrc->get("id"), $db, 0, 0);
        
        if ($dbmission->count() > 0) {
            $idmission = $dbmission->get("id");
            $dbnpc = new DBCollection("SELECT * FROM Player WHERE id_Mission=" . $idmission, $db, 0, 0);
            if ($dbnpc->count() == 0) {
                $param["NPC_ESCORT_DIED"] = 1;
                $playerSrc->set("merchant_level", $playerSrc->get("merchant_level") - 0.25);
            }
        }
        
        $dbcdate = new DBCollection("SELECT * FROM Mission WHERE Mission_Active=1 AND date<NOW() AND id_Player=" . $playerSrc->get("id"), $db, 0, 0);
        if ($dbcdate->count() > 0) {
            $param["LATE_ESCORT"] = 1;
            $playerSrc->set("merchant_level", $playerSrc->get("merchant_level") - 0.25);
        }
        // DELETE MISSION
        if ($param["NPC_ESCORT_DIED"] || $param["LATE_ESCORT"]) {
            $delMission = new DBCollection("DELETE FROM Mission WHERE id=" . $idmission, $db, 0, 0, false);
            $delMissionCondition = new DBCollection("DELETE FROM MissionCondition WHERE idMission=" . $idmission, $db, 0, 0, false);
            $delMissionReward = new DBCollection("DELETE FROM MissionReward WHERE idMission=" . $idmission, $db, 0, 0, false);
            $delparch = new DBCollection("DELETE FROM Equipment WHERE id_BasicEquipment=600 AND id_Mission=" . $idmission, $db, 0, 0, false);
            if ($param["LATE_ESCORT"]) {
                $npc = new Player();
                $npc->load($dbnpc->get("id"), $db);
                PNJFactory::deleteDBnpc($npc, $param, $db, null, 0);
            }
        }
        
        // ------------------------------------------- Fin escorte -----------------------------------------
        
        $param["AP"] = $playerSrc->get("ap");
        $playerSrc->updateDBr($db);
        return NEWATB_NO_ERROR;
    }

    public static function looseMoney($playerSrc, $moneyfall = 0, &$db)
    {
        $date = gmdate('Y-m-d H:i:s');
        if ($moneyfall != 0) {
            $dbe = new DBCollection(
                "SELECT id,po FROM Equipment WHERE state='Ground' AND room=" . $playerSrc->get("room") . " AND x=" . $playerSrc->get("x") . " AND y=" . $playerSrc->get("y") .
                     " AND po != 0", $db);
            if (! $dbe->eof()) {
                $money = $moneyfall + $dbe->get("po");
                $dbe = new DBCollection("UPDATE Equipment SET dropped='" . $date . "' , po=" . $money . " WHERE id=" . $dbe->get("id"), $db, 0, 0, false);
            } else {
                $dbe = new DBCollection(
                    "INSERT INTO Equipment (state,x,y,po,dropped,room,inbuilding) VALUES ('Ground'," . $playerSrc->get("x") . "," . $playerSrc->get("y") . "," . $moneyfall . ",'" .
                         $date . "'," . $playerSrc->get("room") . "," . $playerSrc->get("inbuilding") . ")", $db, 0, 0, false);
            }
            $playerSrc->set("money", $playerSrc->get("money") - $moneyfall);
        }
        // echo $ouioui;
    }

    static function resurrection($playerSrc, &$param, $db)
    {
        $param["MONEY_FALL"] = 0;
        if ($playerSrc->get("id_BasicRace") != 5) {
            // Perte de 90% des pièces d'or
            if ($playerSrc->get("modeAnimation") == 0) {
                $moneyfall = floor($playerSrc->get("money") * 0.9);
            } else {
                $moneyfall = 0;
            }
            $param["MONEY_FALL"] = $moneyfall;
            
            // Si dans un royaume, récupération de l'or par un garde et mise sur le compte en banque sauf si l'aventurier à un sursis de prison
            $mapObj = new Map($db, $playerSrc->get("map"));
            if (($mapObj->getStartKingdomIdFromMap($playerSrc->get("x"), $playerSrc->get("y")) != ID_UNKNOWN_KINGDOM) && $param["PLAYER_IS_NPC"] == 0 &&
                 $playerSrc->get("prison") == 0) {
                $playerSrc->set("moneyBank", $playerSrc->get("moneyBank") + $moneyfall);
                $playerSrc->set("money", $playerSrc->get("money") - $moneyfall);
            } else {
                if ($playerSrc->get("modeAnimation") == 0) {
                    self::looseMoney($playerSrc, $moneyfall, $db);
                }
            }
            // Perte de l'xp si pas de level_up
            if ($playerSrc->get("levelUp") != 1) {
                if ($playerSrc->get("modeAnimation") == 0) {
                    $pxTonextLevelUp = getXPLevelUP($playerSrc->get("level"));
                    $pourcentage = $playerSrc->get("xp") / $pxTonextLevelUp;
                    $finalPX = 0;
                    switch (true) {
                        case ($pourcentage >= 0.2 && $pourcentage < 0.4):
                            $finalPX = ceil($pxTonextLevelUp * 0.2);
                            break;
                        case ($pourcentage >= 0.4 && $pourcentage < 0.6):
                            $finalPX = ceil($pxTonextLevelUp * 0.4);
                            break;
                        case ($pourcentage >= 0.6 && $pourcentage < 0.8):
                            $finalPX = ceil($pxTonextLevelUp * 0.6);
                            break;
                        case ($pourcentage >= 0.8 && $pourcentage < 1):
                            $finalPX = ceil($pxTonextLevelUp * 0.8);
                            break;
                        default:
                            $finalPX = 0;
                    }
                    $playerSrc->set("xp", $finalPX);
                }
            } else {
                $param["LEVEL_UP"] = 1;
                $param["TARGET_XP"] = $playerSrc->get("xp") - getXPLevelUP($playerSrc->get("level"));
                $playerSrc->set("xp", getXPLevelUP($playerSrc->get("level")));
            }
        }
        
        $param["LARCIN"] = 0;
        
        // Si attaché le bolas tombe au sol
        if ($playerSrc->get("state") == "creeping") {
            
            $array_result = PlayerFactory::getListIdBolas($playerSrc, $db);
            foreach ($array_result as $i => $value) {
                $idBolas = $value;
                $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='A terre'", $db, 0, 0, false);
                $dbe = new DBCollection("SELECT name FROM Equipment WHERE id=" . $idBolas, $db);
                $cond = "";
                if ($dbe->get("name") == "Branche d'Etranglesaule")
                    $cond = ",id_BasicEquipment = 507, id_EquipmentType=33 ";
                $dbe = new DBCollection(
                    "UPDATE Equipment set x=" . $playerSrc->get("x") . ", y=" . $playerSrc->get("y") . ", room=" . $playerSrc->get("room") . ", inbuilding=" .
                         $playerSrc->get("inbuilding") . ", state='Ground', id_Player=0 " . $cond . " WHERE id=" . $idBolas, $db, 0, 0, false);
            }
            $playerSrc->set("state", "walking");
        }
        
        // Si sous l'effet d'un larçin perte d'un objet de l'inventaire aléatoire
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Larçin'", $db);
        if (! $dbc->eof()) {
            $param["LARCIN"] = 1;
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='NO' AND id_EquipmentType <= 40 AND id_Player=" . $playerSrc->get("id"), $db);
            $nbe = $dbe->count();
            if ($nbe == 0)
                $param["LARCIN"] = 2;
            $alea = mt_rand(1, $nbe);
            while ($alea > 1) {
                $dbe->next();
                $alea --;
            }
            $dbx = new DBCollection(
                "UPDATE Equipment SET id_Player=0, room=" . $playerSrc->get("room") . ", inbuilding=" . $playerSrc->get("inbuilding") . ", state='Ground', x=" . $playerSrc->get(
                    "x") . ", y=" . $playerSrc->get("y") . ", map=" . $playerSrc->get("map") . ", dropped='" . gmdate("Y-m-d H:i:s") . "' WHERE id=" . $dbe->get("id"), $db, 0, 0, 
                false);
        }
        
        // objet maudit tombe au sol
        $dbc = new DBCollection("SELECT id,name,extraname FROM Equipment WHERE maudit=1 AND id_Player=" . $playerSrc->get("id"), $db);
        while (! $dbc->eof()) {
            if (! isset($param["EQUIPMENT_FALL"])) {
                $param["EQUIPMENT_FALL"] = 0;
            }
            if ($param["EQUIPMENT_FALL"] == 0) {
                $param["EQUIPMENT_FALL"] = 1;
                $param["DROP_NAME"] = "L'objet maudit " . $dbc->get("name") . ($dbc->get("extraname") == "" ? "" : " " . $dbc->get("extraname"));
            } else {
                $param["DROP_NAME"] .= "<br/>L'objet maudit " . $dbc->get("name") . ($dbc->get("extraname") == "" ? "" : " " . $dbc->get("extraname"));
            }
            $dbx = new DBCollection(
                "UPDATE Equipment SET id_Player=0, weared='NO', room=" . $playerSrc->get("room") . ", inbuilding=" . $playerSrc->get("inbuilding") . ", state='Ground', x=" .
                     $playerSrc->get("x") . ", y=" . $playerSrc->get("y") . ", map=" . $playerSrc->get("map") . ", dropped='" . gmdate("Y-m-d H:i:s") . "' WHERE id=" .
                     $dbc->get("id"), $db, 0, 0, false);
            $dbc->next();
        }
        
        // suppression BM
        self::deleteAllBM($playerSrc, $param, $db, 0, 1);
        // suppression bassins divins et murs de terre
        self::deleteAllMagicsBuilding($playerSrc, $param, $db);
        
        // Teleport sur le lieu de resurrection
        if ($playerSrc->get("id_BasicRace") != 5) {
            // Mise du malus de chapelle
            $date = gmdate('Y-m-d H:i:s');
            $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND id_StaticModifier_BM=19", $db);
            if ($dbc->count() == 0)
                $dbi = new DBCollection(
                    "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (19," . $playerSrc->get("id") . ",2, 1 , 'POSITIVE', '" .
                         addslashes("Protection Chapelle") . "' ,2,'" . $date . "')", $db, 0, 0, false);
            else
                $dbc = new DBCollection("UPDATE BM SET life=2 WHERE id_Player=" . $playerSrc->get("id") . " AND id_StaticModifier_BM=19", $db, 0, 0, false);
            
            $temple = new Building();
            
            $dbs = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("resurrectid"), $db);
            if ($dbs->count() > 0 && $playerSrc->get("resurrectid") > 0) {
                $id_temple = $playerSrc->get("resurrectid");
            } else { // ressurection par Défaut à Earok
                $dbs = new DBCollection("SELECT id FROM Building WHERE name='Temple' AND id_City=135", $db);
                $id_temple = $dbs->get("id");
            }
            $temple->load($id_temple, $db);
            $playerSrc->set("x", $temple->get("x"));
            $playerSrc->set("y", $temple->get("y"));
            $playerSrc->set("inbuilding", $temple->get("id"));
            $playerSrc->set("room", CHAPEL_ROOM);
            $playerSrc->set("resurrect", 1);
        } else {
            // mort d'un garde ressurection à la prison
            if ($playerSrc->get("id") < 1500) {
                $playerSrc->set("x", 464);
                $playerSrc->set("y", 375);
                $playerSrc->set("inbuilding", 1076);
            }
            if ($playerSrc->get("id") > 1500 && $playerSrc->get("id") < 2500) {
                $playerSrc->set("x", 256);
                $playerSrc->set("y", 301);
                $playerSrc->set("inbuilding", 1252);
            }
            if ($playerSrc->get("id") > 2500) {
                $playerSrc->set("x", 564);
                $playerSrc->set("y", 344);
                $playerSrc->set("inbuilding", 1570);
            }
            $playerSrc->set("room", ENTRANCE_ROOM);
            $playerSrc->set("ap", 50);
            $playerSrc->set("resurrect", 0);
        }
        $playerSrc->set("state", "walking");
        $playerSrc->set("lastdeath", gmdate("Y-m-d H:i:s"));
        $playerSrc->set("nbdeath", $playerSrc->get("nbdeath") + 1);
        $playerSrc->set("currhp", $playerSrc->getModif("hp", DICE_ADD));
        // $playerSrc->set("currhp",$playerSrc->get("hp"));
        $playerSrc->updateHidden($db);
        self::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        
        /* Inutile normalement */
        $playerSrc->updateDBr($db);
    }

    static function deleteFeuFol($playerSrc, &$param, $db)
    {
        $dbgolem = new DBCollection("SELECT id FROM Player WHERE id_Owner=" . $playerSrc->get("id") . " AND id_BasicRace=" . ID_BASIC_RACE_FEU_FOL, $db);
        if (! $dbgolem->eof()) {
            $dbd = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Golem de Feu'", $db, 0, 0, false);
            $alea = mt_rand(1, 2);
            if ($alea == 1) {
                // suppression du golem de feu
                $golem = new Player();
                $golem->externDBObj("Modifier");
                $golem->load($dbgolem->get("id"), $db);
                $dbd = new DBCollection("DELETE FROM MagicSpell WHERE id_Player=" . $dbgolem->get("id"), $db, 0, 0, false);
                $param2 = array();
                $param2["TARGET_ID"] = $playerSrc->get("id");
                $param2["TARGET_RACE"] = $playerSrc->get("id_BasicRace");
                $param2["TYPE_ATTACK"] = SPELL_FEU_FOL_DEATH_EVENT;
                $param2["PLAYER_ID"] = $dbgolem->get("id");
                $param2["MEMBER_ID"] = 0;
                $param2["PLAYER_RACE"] = ID_BASIC_RACE_FEU_FOL;
                $param2["ERROR"] = 0;
                $param["TYPE_ACTION"] = SPELL_FEU_FOL_DEATH;
                Event::logAction($db, SPELL_FEU_FOL_DEATH, $param2);
                unset($param2);
                PNJFactory::DeleteDBnpc($golem, $param, $db);
            } else {
                // le feu fol retrouve sa liberté
                $dbbm = new DBCollection("UPDATE Player set id_Owner=0, id_Member=0 WHERE id_Owner=" . $playerSrc->get("id") . " AND id_BasicRace=" . ID_BASIC_RACE_FEU_FOL, $db, 0, 
                    0, false);
                $param2 = array();
                $param2["TARGET_ID"] = $playerSrc->get("id");
                $param2["TARGET_RACE"] = $playerSrc->get("id_BasicRace");
                $param2["TYPE_ATTACK"] = SPELL_FIRECALL_PASSIVE_RELEASE_EVENT;
                $param2["PLAYER_ID"] = $dbgolem->get("id");
                $param2["MEMBER_ID"] = 0;
                $param2["PLAYER_RACE"] = ID_BASIC_RACE_FEU_FOL;
                $param2["ERROR"] = 0;
                $param["TYPE_ACTION"] = SPELL_FIRECALL_PASSIVE_RELEASE;
                Event::logAction($db, SPELL_FIRECALL_PASSIVE_RELEASE, $param2);
                unset($param2);
            }
        }
    }

    static function deleteAllBM($playerSrc, &$param, $db, $isDeleteBonusChapelle = 1, $isDeleteFeufol = 1)
    {
        // Suppression de tous les BM
        $dbbm = new DBCollection(
            "DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND life !=-3 " . ($isDeleteBonusChapelle == 1 ? "" : " AND name <> 'Protection Chapelle'"), $db, 0, 0, 
            false);
        $dbbm = new DBCollection("DELETE FROM BM WHERE name='défendu' AND id_Player\$src=" . $playerSrc->get("id"), $db, 0, 0, false);
        $dbbm = new DBCollection("DELETE FROM BM WHERE name='Aura de courage' AND id_Player\$src=" . $playerSrc->get("id"), $db, 0, 0, false);
        $dbbm = new DBCollection("DELETE FROM BM WHERE name='Aura de résistance' AND id_Player\$src=" . $playerSrc->get("id"), $db, 0, 0, false);
        $dbbm = new DBCollection("UPDATE Player SET recall=0 WHERE id IN (SELECT id_Player FROM BM WHERE name='Rappel' AND id_Player\$src=" . $playerSrc->get("id") . ")", $db, 0, 0, 
            false);
        $dbbm = new DBCollection("DELETE FROM BM WHERE name='Rappel' AND id_Player\$src=" . $playerSrc->get("id"), $db, 0, 0, false);
        
        if ($isDeleteFeufol == 1) {
            self::deleteFeuFol($playerSrc, $param, $db);
        }
        
        // Suppresion immédiate des auras du paladin pour le groupe de chasse
        if ($playerSrc->get("id_FighterGroup") != 0) {
            $dbp = new DBCollection("SELECT id FROM Player WHERE id_FighterGroup=" . $playerSrc->get("id_FighterGroup"), $db);
            while (! $dbp->eof()) {
                if ($dbp->get("id") != $playerSrc->get("id")) {
                    $mate = new Player();
                    $mate->externDBObj("Modifier");
                    $mate->load($dbp->get("id"), $db);
                    self::initBM($mate, $mate->getObj("Modifier"), $param2, $db, 0);
                }
                $dbp->next();
            }
        }
    }

    static function deleteAllMagicsBuilding($playerSrc, &$param, $db)
    {
        $dbe = new DBCollection("DELETE FROM Building WHERE (id_BasicBuilding=27 or id_BasicBuilding=15) AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
    }

    /* ************************************************************ FONCTIONS DE GESTION DES SACS ********************************************************** */
    static function checkingInvfullForObject($playerSrc, $id_EquipmentType, &$param, $db)
    {
        $good = 0;
        switch ($id_EquipmentType) {
            case 28:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 5, $db)) {
                    $good = 1;
                    $param["INV"] = 5;
                }
                
                break;
            case 29:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 4, $db)) {
                    $good = 1;
                    $param["INV"] = 4;
                }
                
                break;
            
            case 30:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 1, $db)) {
                    $good = 1;
                    $param["INV"] = 1;
                }
                
                break;
            
            case 31:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 2, $db)) {
                    $good = 1;
                    $param["INV"] = 2;
                }
                
                break;
            
            case 32:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 3, $db)) {
                    $good = 1;
                    $param["INV"] = 3;
                }
                
                break;
            case 40:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 6, $db)) {
                    $good = 1;
                    $param["INV"] = 6;
                }
                
                break;
            default:
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $good = 1;
                    $param["INV"] = 0;
                }
                break;
        }
        if ($good)
            return $param["INV"];
        
        return - 1;
    }

    static function getBag($id_Player, $typeName, $db)
    {
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $id_Player . " AND weared = 'Yes' AND durability!=0 AND name='" . addslashes($typeName) . "'", $db);
        $bag = new Equipment();
        $bag->load($dbe->get("id"), $db);
        return $bag;
    }

    static function getNbElementInBag($id_Player, $nameBag, $db)
    {
        if ($nameBag == "Sac d'équipements") {
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $id_Player . " AND state='Carried' AND weared = 'No' AND id_Equipment\$bag= 0", $db);
            return $dbe->count();
        } else {
            $bag = new Equipment();
            $bag = self::getBag($id_Player, $nameBag, $db);
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $id_Player . " AND weared = 'No' AND id_Equipment\$bag=" . $bag->get("id"), $db);
            return $dbe->count();
        }
    }

    static function getNbElementMaxInBag($id_Player, $nameBag, $db)
    {
        if ($nameBag == "Sac d'équipements")
            return NB_MAX_ELEMENT;
        
        $bag = new Equipment();
        $bag = self::getBag($id_Player, $nameBag, $db);
        if ($nameBag == "Carquois")
            return 6 + 2 * $bag->get("level");
        else
            return 2 + 2 * $bag->get("level");
    }

    static function checkPlaceBag($id_Player, $numBag, $db)
    {
        if ($numBag == 0) {
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $id_Player . " AND state='Carried' AND weared='No' AND id_Equipment\$bag=0", $db);
            if ($dbe->count() >= NB_MAX_ELEMENT)
                return 0;
            else
                return 1;
        } else {
            if (self::getIdBagByNum($id_Player, $numBag, $db)) {
                if (self::getNbElementInBag($id_Player, self::getNameBagByNum($id_Player, $numBag, $db), $db) <
                     self::getNbElementMaxInBag($id_Player, self::getNameBagByNum($id_Player, $numBag, $db), $db))
                    return 1;
            } else
                return 0;
        }
    }

    static function checkObjectInBag($id_EquipmentType, $numBag)
    {
        if (($id_EquipmentType != 30 && $numBag == 1) or ($id_EquipmentType != 31 && $numBag == 2) or ($id_EquipmentType != 32 && $numBag == 3) or
             ($id_EquipmentType != 29 && $numBag == 4) or ($id_EquipmentType != 28 && $numBag == 5) or ($id_EquipmentType != 40 && $numBag == 6))
            return 0;
        else
            return 1;
    }

    static function getIdBagByName($id_Player, $name, $db)
    {
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $id_Player . " AND weared = 'Yes' AND durability!=0 AND name='" . addslashes($name) . "'", $db);
        if ($dbe->count() == 0)
            return 0;
        return $dbe->get("id");
    }

    static function getNameBagByNum($id_Player, $num, $db)
    {
        switch ($num) {
            case 0:
                return "Sac d'équipements";
                break;
            case 1:
                return "Sac de gemmes";
                break;
            case 2:
                return "Sac de matières premières";
                break;
            case 3:
                return "Sac d'herbes";
                break;
            case 4:
                return PlayerFactory::getNameBelt($id_Player, $db);
                break;
            case 5:
                return "Carquois";
                break;
            case 6:
                return "Trousse à outils";
                break;
            default:
                break;
        }
    }

    static function getIdBagByNum($id_Player, $num, $db)
    {
        if ($num == 0)
            return 0;
        else {
            $nameBag = self::getNameBagByNum($id_Player, $num, $db);
            return self::getIdBagByName($id_Player, $nameBag, $db);
        }
    }

    static function getLevelBagByName($id_Player, $name, $db)
    {
        $dbe = new DBCollection("SELECT level FROM Equipment WHERE id_Player=" . $id_Player . " AND weared = 'YES' AND name='" . addslashes($name) . "'", $db);
        if (! $dbe->count())
            return 0;
        return $dbe->get("level");
    }

    /* ******************************************************** FONCTIONS DE GESTION DE L'EQUIPEMENT ET DES BM ************************** */
    static function getIdEquipFromSlot($id_Player, $slot, $db)
    {
        switch ($slot) {
            // casque
            case 1:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='12' AND id_Player=" . $id_Player, $db);
                break;
            // bottes
            case 2:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='13' AND id_Player=" . $id_Player, $db);
                break;
            // Jambières
            case 3:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='14' AND id_Player=" . $id_Player, $db);
                break;
            // buste
            case 4:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='11' AND id_Player=" . $id_Player, $db);
                break;
            // épaulière gauche
            case 5:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='17' AND id_Player=" . $id_Player, $db);
                break;
            // épaulière droite
            case 6:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='18' AND id_Player=" . $id_Player, $db);
                break;
            // bouclier
            case 7:
                $dbe = new DBCollection(
                    "SELECT id FROM Equipment WHERE weared='YES' AND ( id_EquipmentType='8' OR id_EquipmentType='9' OR id_EquipmentType='10')  AND id_Player=" . $id_Player, $db);
                break;
            // gants
            case 8:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='16' AND id_Player=" . $id_Player, $db);
                break;
            // ceinture
            case 9:
                $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND id_EquipmentType='20' AND id_Player=" . $id_Player, $db);
                break;
        }
        
        if (! $dbe->count())
            return 0;
        else
            return $dbe->get("id");
    }

    static function getRndEquip($id_Player, $db)
    {
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE weared='YES' AND durability!= 0 AND id_EquipmentType <'21' AND id_EquipmentType >'7' AND id_Player=" . $id_Player, 
            $db);
        if ($dbe->eof())
            return 0;
        
        $alea = mt_rand(1, $dbe->count());
        for ($i = 1; $i < $alea; $i ++)
            $dbe->next();
        
        return $dbe->get("id");
    }

    static function getNameBelt($id_Player, $db)
    {
        $cond = "(id_EquipmentType = 20)";
        $dbe = new DBCollection("SELECT name FROM Equipment WHERE id_Player=" . $id_Player . " AND weared = 'Yes' AND " . $cond, $db);
        $founded = $dbe->count();
        if (! $founded)
            return 0;
        return $dbe->get("name");
    }

    static function getTypeArm($player, $db)
    {
        $cond = "(id_EquipmentType <= 7 or id_EquipmentType =22)";
        $dbe = new DBCollection("SELECT id_EquipmentType FROM Equipment WHERE id_Player=" . $player->get("id") . " AND weared='Yes' AND durability!=0 AND " . $cond, $db);
        $founded = $dbe->count();
        if (! $founded)
            return 0;
        return $dbe->get("id_EquipmentType");
    }

    static function getTypeMagicArm($player, $db)
    {
        $cond = "(id_EquipmentType in ( 6 , 7 ) or id_BasicEquipment = 51)";
        $dbe = new DBCollection("SELECT id_EquipmentType FROM Equipment WHERE id_Player=" . $player->get("id") . " AND weared='Yes' AND durability!=0 AND " . $cond, $db);
        $founded = $dbe->count();
        if (! $founded)
            return 0;
        return $dbe->get("id_EquipmentType");
    }

    static function getTypeMagicArmAtt($player, $db)
    {
        $cond = "(id_EquipmentType in ( 6 , 7 ))";
        $dbe = new DBCollection("SELECT id_EquipmentType FROM Equipment WHERE id_Player=" . $player->get("id") . " AND weared='Yes' AND durability!=0 AND " . $cond, $db);
        $founded = $dbe->count();
        if (! $founded)
            return 0;
        return $dbe->get("id_EquipmentType");
    }

    static function getIdArm($player, $db, $durability = 0)
    {
        $cond = "(id_EquipmentType <= 7 or id_EquipmentType = 22)";
        if ($durability == 0)
            $cond .= " AND durability!=0";
        
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $player->get("id") . " AND weared = 'YES' AND " . $cond, $db);
        $founded = $dbe->count();
        if ($founded == 0)
            return 0;
        return $dbe->get("id");
    }

    static function getBasicArm($player, $db)
    {
        $cond = "(id_EquipmentType <= 7 or id_EquipmentType=22)";
        $dbe = new DBCollection("SELECT id_BasicEquipment FROM Equipment WHERE id_Player=" . $player->get("id") . " AND durability!=0 AND weared = 'YES' AND " . $cond, $db);
        $founded = $dbe->count();
        if ($founded == 0)
            return 0;
        return $dbe->get("id_BasicEquipment");
    }

    static function getTypeShield($player, $db)
    {
        $cond = "(id_EquipmentType = 4 OR id_EquipmentType = 8 OR id_EquipmentType = 9 OR id_EquipmentType = 10 OR id_EquipmentType = 21)";
        $dbe = new DBCollection("SELECT id_EquipmentType FROM Equipment WHERE id_Player=" . $player->get("id") . " AND durability!=0 AND  weared = 'YES' AND " . $cond, $db);
        $founded = $dbe->count();
        if ($founded == 0)
            return 0;
        return $dbe->get("id_EquipmentType");
    }

    static function getIdShield($player, $db)
    {
        $cond = "(id_EquipmentType = 8 OR id_EquipmentType = 9 OR id_EquipmentType = 10 or id_EquipmentType = 21)";
        $dbe = new DBCollection("SELECT id, durability FROM Equipment WHERE id_Player=" . $player->get("id") . " AND durability!=0 AND  weared = 'YES' AND " . $cond, $db);
        $founded = $dbe->count();
        if ($founded == 0)
            return 0;
        return $dbe->get("id");
    }

    static function getListIdBolas($player, $db)
    {
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $player->get("id") . " AND id_BasicEquipment=205 AND state='Transported'", $db);
        $array_result = array();
        $i = 0;
        while (! $dbe->eof()) {
            $array_result[$i] = $dbe->get("id");
            $dbe->next();
            $i += 1;
        }
        return $array_result;
    }

    static function initBM(&$player, &$modif, &$param, $db, $newdla = 0)
    {
        $saveModifier = new Modifier();
        $characLabel = array(
            "magicSkill_bm",
            "attack_bm",
            "defense_bm",
            "damage_bm"
        );
        if ($player->get("status") == "NPC") {
            foreach ($characLabel as $name) {
                $saveModifier->addModif($name, DICE_ADD, $modif->getAtt($name, DICE_ADD));
            }
        }
        $modif->resetBM();
        
        $add = "";
        if ($player->get("room") == 23)
            $add = " AND id_StaticModifier_BM != 19 ";
            
            // Réduction de la vie des BM et suppression de ceux qui sont terminés.
        if ($newdla)
            $dbbm = new DBCollection("UPDATE BM SET life=life-1 WHERE life>-1" . $add . " AND id_Player=" . $player->get("id"), $db, 0, 0, false);
        
        $dbbm = new DBCollection("DELETE BM FROM BM WHERE (life=-1 OR level < 1) AND id_Player=" . $player->get("id"), $db, 0, 0, false);
        
        // Application des BM particuliers qui peuvent entrainer la mort du perso.
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id"), $db);
        $bm = new StaticModifier();
        $bm->setAttach("StaticModifier_BM");
        $param["PLAYER_REGEN"] = 0;
        $param["PLAYER_RECALL"] = 0;
        $param["OVERLOAD"] = 0;
        
        $dead = 0;
        
        if ($dbbm->count() > 0) {
            
            while (! $dbbm->eof()) {
                if ($dbbm->get("name") == "Régénération" && $newdla) {
                    // $player->set("currhp", min($player->get("currhp")+$dbbm->get("value"), $player->get("hp")));
                    $param["PLAYER_REGEN"] += $dbbm->get("value");
                }
                if ($dbbm->get("name") == "Blessure Mortelle" && $newdla) {
                    $bm->load($dbbm->get("id_StaticModifier_BM"), $db);
                    $bm->updateFromBMLevel($dbbm->get("level"));
                    $bm->initCharacStr();
                    $param["PLAYER_REGEN"] += $bm->getModifStr("hp");
                }
                $dbbm->next();
            }
            // pour éviter que les blessures mortelles entrainent une perte de PAs
            $param["PLAYER_REGEN"] = max($param["PLAYER_REGEN"], 0);
            
            $dbbm->first();
            
            while (! $dbbm->eof()) {
                
                if ($dbbm->get("name") == "Aura de courage" && $newdla && ! $dead) {
                    $param["PLAYER_REGEN"] -= $dbbm->get("value");
                    if (- $param["PLAYER_REGEN"] >= $player->get("currhp")) {
                        $dead = 1;
                        BasicActionFactory::globalInfo($player, $param2);
                        $param2["TYPE_ATTACK"] = PASSIVE_BRAVERY_DEATH_EVENT;
                        $param2["ERROR"] = 0;
                        Event::logAction($db, PASSIVE_BRAVERY_DEATH, $param2);
                        if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 || ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
                            PlayerFactory::resurrection($player, $param, $db);
                        } else {
                            PNJFactory::DeleteDBnpc($player, $param, $db, null);
                        }
                    }
                }
                if ($dbbm->get("name") == "Sang de Lave" && $newdla && ! $dead) {
                    $param["PLAYER_REGEN"] -= $dbbm->get("value");
                    if (- $param["PLAYER_REGEN"] >= $player->get("currhp")) {
                        $dead = 1;
                        $killer = new Player();
                        $killer->externDBObj("Modifier");
                        $param2["XP"] = 0;
                        $param2["TYPE_ATTACK"] = PASSIVE_BLOOD_DEATH_EVENT;
                        $param2["ERROR"] = 0;
                        $param2["TYPE_ACTION"] = PASSIVE_BLOOD_DEATH;
                        
                        BasicActionFactory::globalInfoOpponent($player, $param2);
                        if ($killer->load($dbbm->get("id_Player\$src"), $db) != - 1) {
                            BasicActionFactory::globalInfo($killer, $param2);
                            BasicActionFactory::kill($killer, $player, $param2, $db);
                            PlayerFactory::addXP($killer, $param2["XP"], $db);
                            $killer->updateDB($db);
                        } else {
                            if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 ||
                                 ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
                                PlayerFactory::resurrection($player, $param, $db);
                            } else {
                                PNJFactory::DeleteDBnpc($player, $param, $db, null, 1);
                            }
                            $dbc = new DBCollection("SELECT * FROM Event WHERE id_Player\$src=" . $dbbm->get("id_Player\$src"), $db);
                            $dbbr = new DBCollection("SELECT * FROM BasicRace WHERE id=" . $dbc->get("id_BasicRace\$src"), $db);
                            $param2["PLAYER_NAME"] = $dbbr->get("name");
                            $param2["PLAYER_ID"] = $dbbm->get("id_Player\$src");
                            $param2["PLAYER_GENDER"] = $dbbr->get("gender");
                            $param2["PLAYER_RACE"] = $dbbr->get("id");
                            $param2["PLAYER_IS_NPC"] = 1;
                            $param2["MEMBER_ID"] = 0;
                        }
                        
                        Event::logAction($db, PASSIVE_BLOOD_DEATH, $param2);
                    }
                }
                if ($dbbm->get("name") == "Blessure Profonde" && $newdla && ! $dead) {
                    $param["PLAYER_REGEN"] -= $dbbm->get("value");
                    if (- $param["PLAYER_REGEN"] >= $player->get("currhp")) {
                        $param2["TYPE_ATTACK"] = PASSIVE_WOUND_DEATH_EVENT;
                        $param2["ERROR"] = 0;
                        $param2["TYPE_ACTION"] = PASSIVE_WOUND_DEATH;
                        $dead = 1;
                        $killer = new Player();
                        $killer->externDBObj("Modifier");
                        $param2["XP"] = 0;
                        BasicActionFactory::globalInfoOpponent($player, $param2);
                        if ($killer->load($dbbm->get("id_Player\$src"), $db) != - 1) {
                            BasicActionFactory::globalInfo($killer, $param2);
                            BasicActionFactory::kill($killer, $player, $param2, $db);
                            PlayerFactory::addXP($killer, $param2["XP"], $db);
                            $killer->updateDB($db);
                        } else {
                            if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 ||
                                 ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
                                PlayerFactory::resurrection($player, $param, $db);
                            } else {
                                PNJFactory::DeleteDBnpc($player, $param, $db, null);
                            }
                            $dbc = new DBCollection("SELECT * FROM Event WHERE id_Player\$src=" . $dbbm->get("id_Player\$src"), $db);
                            $dbbr = new DBCollection("SELECT * FROM BasicRace WHERE id=" . $dbc->get("id_BasicRace\$src"), $db);
                            $param2["PLAYER_NAME"] = $dbbr->get("name");
                            $param2["PLAYER_ID"] = $dbbm->get("id_Player\$src");
                            $param2["PLAYER_GENDER"] = $dbbr->get("gender");
                            $param2["PLAYER_RACE"] = $dbbr->get("id");
                            $param2["PLAYER_IS_NPC"] = 1;
                            $param2["MEMBER_ID"] = 0;
                        }
                        
                        Event::logAction($db, PASSIVE_WOUND_DEATH, $param2);
                    }
                }
                if ($dbbm->get("name") == "Poison" && $newdla && ! $dead) {
                    $param["PLAYER_REGEN"] -= $dbbm->get("value");
                    if (- $param["PLAYER_REGEN"] >= $player->get("currhp")) {
                        $dead = 1;
                        $killer = new Player();
                        $killer->externDBObj("Modifier");
                        $param2["XP"] = 0;
                        $param2["TYPE_ATTACK"] = PASSIVE_POISON_DEATH_EVENT;
                        $param2["ERROR"] = 0;
                        $param2["TYPE_ACTION"] = PASSIVE_POISON_DEATH;
                        
                        BasicActionFactory::globalInfoOpponent($player, $param2);
                        if ($killer->load($dbbm->get("id_Player\$src"), $db) != - 1) {
                            BasicActionFactory::globalInfo($killer, $param2);
                            BasicActionFactory::kill($killer, $player, $param2, $db);
                            PlayerFactory::addXP($killer, $param2["XP"], $db);
                            $killer->updateDB($db);
                        } else {
                            if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 ||
                                 ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
                                PlayerFactory::resurrection($player, $param, $db);
                            } else {
                                PNJFactory::DeleteDBnpc($player, $param, $db, null);
                            }
                            $dbc = new DBCollection("SELECT * FROM Event WHERE id_Player\$src=" . $dbbm->get("id_Player\$src"), $db);
                            $dbbr = new DBCollection("SELECT * FROM BasicRace WHERE id=" . $dbc->get("id_BasicRace\$src"), $db);
                            $param2["PLAYER_NAME"] = $dbbr->get("name");
                            $param2["PLAYER_ID"] = $dbbm->get("id_Player\$src");
                            $param2["PLAYER_GENDER"] = $dbbr->get("gender");
                            $param2["PLAYER_RACE"] = $dbbr->get("id");
                            $param2["PLAYER_IS_NPC"] = 1;
                            $param2["MEMBER_ID"] = 0;
                        }
                        
                        Event::logAction($db, PASSIVE_POISON_DEATH, $param2);
                    }
                }
                if ($dbbm->get("name") == "Infesté" && $newdla && ! $dead) {
                    $param["PLAYER_REGEN"] -= $dbbm->get("value");
                    if (- $param["PLAYER_REGEN"] >= $player->get("currhp")) {
                        $dead = 1;
                        BasicActionFactory::globalInfoOpponent($player, $param2);
                        $param2["PLAYER_ID"] = $dbbm->get("id_Player\$src");
                        $param2["PLAYER_GENDER"] = "F";
                        $param2["PLAYER_NAME"] = "Horreur des sous bois";
                        $param2["PLAYER_RACE"] = 129;
                        $param2["PLAYER_IS_NPC"] = 1;
                        $param2["MEMBER_ID"] = 0;
                        $param2["TYPE_ATTACK"] = PASSIVE_INFEST_DEATH_EVENT;
                        $param2["ERROR"] = 0;
                        $param2["TYPE_ACTION"] = PASSIVE_INFEST_DEATH;
                        Event::logAction($db, PASSIVE_INFEST, $param2);
                        if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 || ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
                            PlayerFactory::resurrection($player, $param, $db);
                        } else {
                            PNJFactory::DeleteDBnpc($player, $param, $db, null);
                        }
                    }
                }
                $dbbm->next();
            }
        }
        if (! $dead)
            $player->set("currhp", min($player->get("currhp") + $param["PLAYER_REGEN"], $player->get("hp")));
        
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $player->get("id"), $db);
        // Application des BM qui ont un id_StaticModifier
        while (! $dbbm->eof()) {
            
            if ($dbbm->get("id_StaticModifier_BM") != 0) {
                $bm->load($dbbm->get("id_StaticModifier_BM"), $db);
                if ($dbbm->get("level") >= 1) {
                    $bm->updateFromBMLevel($dbbm->get("level"));
                }
                $modif->addModifObjToBM($bm);
            }
            
            if ($dbbm->get("name") == "Rappel" && $newdla) {
                $param["PLAYER_RECALL"] = 1;
                $player->set("recall", 1);
            }
            
            $dbbm->next();
        }
        
        // Initialisation des BM des équipements
        if ($player->get("status") == 'PC') {
            $eq = new Equipment();
            $basic = new BasicEquipment();
            $eqmodifier = new Modifier();
            $modifier = new Modifier();
            $dbe = new DBCollection(
                "SELECT BasicEquipment.id_BasicMaterial as BM, " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . "," .
                     $basic->getASRenamer("BasicEquipment", "BA") .
                     ",mask,wearable  FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id LEFT JOIN EquipmentType ON Equipment.id_EquipmentType=EquipmentType.id WHERE Equipment.state='Carried' AND EquipmentType.small='No' AND weared='Yes' AND EquipmentType.id < 41 AND id_Player=" .
                     $player->get("id") . " GROUP BY Equipment.id ORDER BY RAND()", $db);
            $loads = 0;
            $plate = 0;
            $linen = 0;
            $player->set("overload", 0);
            while (! $dbe->eof()) {
                if ($dbe->get("EQdurability") != 0)
                    $loads += $dbe->get("EQlevel") * $dbe->get("EQlevel");
                
                if ($dbe->get("BM") == 1 && $dbe->get("EQid_EquipmentType") > 6 && $dbe->get("EQid_EquipmentType") < 22)
                    $plate += $dbe->get("EQlevel");
                
                if ($dbe->get("BM") == 5 && $dbe->get("EQid_EquipmentType") > 6 && $dbe->get("EQid_EquipmentType") < 22)
                    $linen += $dbe->get("EQlevel");
                
                $dbe->next();
            }
            $dbe->first();
            
            if ($plate > $modif->getAtt("strength", DICE_D6)) {
                $player->set("overload", 1);
                $param["OVERLOAD"] = 1;
            }
            if ($linen > $modif->getAtt("magicSkill", DICE_D6)) {
                $player->set("overload", 1);
                $param["OVERLOAD"] = 1;
            }
            
            if ($loads > $player->get("level") * 5) {
                $player->set("overload", 1);
                $param["OVERLOAD"] = 1;
                
                if ($loads > 2 * ($player->get("level") * 5)) {
                    $player->set("overload", 2);
                    $param["OVERLOAD"] = 2;
                }
            }
            
            while (! $dbe->eof()) {
                if ($dbe->get("EQdurability") != 0) {
                    $eq->DBLoad($dbe, "EQ");
                    $eqmodifier->DBLoad($dbe, "EQM");
                    $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
                    
                    // Si la pièce est aiguisé
                    if ($eq->get("sharpen") > 0)
                        $eqmodifier->addModif("damage", DICE_ADD, $eq->get("sharpen"));
                        
                        // Si la pièce est enchanté
                    if ($eq->get("extraname") != "") {
                        $tmodifier = new Modifier();
                        $template = new Template();
                        $dbt = new DBCollection(
                            "SELECT " . $template->getASRenamer("Template", "TP") . "," . $tmodifier->getASRenamer("Modifier_BasicTemplate", "MD") .
                                 " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate LEFT JOIN Modifier_BasicTemplate ON BasicTemplate.id_Modifier_BasicTemplate=Modifier_BasicTemplate.id WHERE id_Equipment=" .
                                 $eq->get("id") . " order by Template.pos asc", $db);
                        while (! $dbt->eof()) {
                            $template->DBLoad($dbt, "TP");
                            $tmodifier->DBLoad($dbt, "MD");
                            $tmodifier->updateFromTemplateLevel($template->get("level"));
                            $eqmodifier->addModifObj2($tmodifier);
                            $dbt->next();
                        }
                    }
                    
                    $modif->addModifObjToBM($eqmodifier);
                }
                $dbe->next();
            }
        } else {
            foreach ($characLabel as $name) {
                $modif->addModif($name, DICE_ADD, $saveModifier->getAtt($name, DICE_ADD));
            }
        }
        $modif->resetFigthCharac();
        self::initFigthCharac($player, $modif, $db);
    }

    static function initFigthCharac($player, &$modif, $db)
    {
        $modif->resetFigthCharac();
        $typeSword = self::getTypeArm($player, $db);
        $typeShield = self::getTypeShield($player, $db);
        
        $npc = 0;
        if ($player->get("status") == "NPC")
            $npc = $player->get("fightStyle");
        
        $modif->initFigthCharac($typeSword, $typeShield, $npc);
        $modif->validateBM();
        $modif->initCharacStr();
    }

    static function ReduceArmorDurability($playerSrc, &$param, $db)
    {
        $dbe = new DBCollection("SELECT * FROM Equipment WHERE weared='Yes' AND durability!=0 AND id_EquipmentType < 29 AND id_Player=" . $playerSrc->get("id"), $db);
        while (! $dbe->eof()) {
            $ran = mt_rand(0, 1);
            if ($ran) {
                $dbx = new DBCollection("UPDATE Equipment SET durability=durability-1 WHERE id=" . $dbe->get("id"), $db, 0, 0, false);
                $dbt = new DBCollection("SELECT durability FROM Equipment WHERE id=" . $dbe->get("id"), $db);
                if ($dbt->get("durability") == 0)
                    $param["ARMOR_BROKEN"] = 1;
            }
            $dbe->next();
        }
    }

    static function ReduceArmDurability($playerSrc, &$param, $db)
    {
        $dbe = new DBCollection("UPDATE Equipment SET sharpen=0  WHERE sharpen!= 0 AND id=" . self::getIdArm($playerSrc, $db), $db, 0, 0, false);
        if (mt_rand(0, 1)) {
            $dbe = new DBCollection("SELECT * FROM Equipment WHERE durability=1 AND id=" . self::getIdArm($playerSrc, $db), $db);
            $dbx = new DBCollection("UPDATE Equipment SET durability=durability-1  WHERE durability != 0 AND id=" . self::getIdArm($playerSrc, $db), $db, 0, 0, false);
            if (! $dbe->eof()) {
                
                $dby = new DBCollection("UPDATE Equipment SET sharpen=0  WHERE sharpen!= 0 AND id=" . self::getIdArm($playerSrc, $db), $db, 0, 0, false);
                $param["WEAPON_BROKEN"] = 1;
                PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            }
        }
    }

    static function ReduceShieldDurability($playerSrc, &$param, $db)
    {
        $dbe = new DBCollection("UPDATE Equipment SET sharpen=sharpen-1  WHERE sharpen!= 0 AND id=" . self::getIdShield($playerSrc, $db), $db, 0, 0, false);
        if (mt_rand(0, 1)) {
            $dbe = new DBCollection("SELECT * FROM Equipment WHERE durability=1 AND id=" . self::getIdShield($playerSrc, $db), $db);
            $dbx = new DBCollection("UPDATE Equipment SET durability=durability-1  WHERE durability != 0 AND id=" . self::getIdShield($playerSrc, $db), $db, 0, 0, false);
            if (! $dbe->eof()) {
                $dbe = new DBCollection("UPDATE Equipment SET sharpen=0  WHERE sharpen!= 0 AND id=" . self::getIdShield($playerSrc, $db), $db, 0, 0, false);
                $param["SHIELD_BROKEN"] = 1;
                PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            }
        }
    }

    /* ********************************************************************* FONCTIONS DE GESTION DU LEVEL UP ET DE PROGRESSION ********************************* */
    static function levelUp($playerSrc, &$param, &$db)
    {
        self::globalInfo($playerSrc, $param);
        
        if (! $playerSrc->get("levelUp"))
            return TARGET_FIELD_INVALID;
        
        $param["TYPE_ACTION"] = LEVEL_UP;
        $param["TYPE_ATTACK"] = LEVEL_UP_EVENT;
        $param["LEVEL"] = $playerSrc->get("level");
        // Ajout de 3 points de vie automatiquement
        $playerSrc->set("currhp", $playerSrc->get("currhp") + 3);
        $playerSrc->set("hp", $playerSrc->get("hp") + 3);
        $playerSrc->addModif("hp", DICE_ADD, 3);
        $level = $playerSrc->get("level");
        
        // Initialisation d'un tableau contenant la valeur des barres de progression
        $dbc = new DBCollection("SELECT * FROM Upgrade WHERE id=" . $playerSrc->get("id_Upgrade"), $db);
        $item = array();
        $item["hp"] = $dbc->get("hp");
        $item["strength"] = $dbc->get("strength");
        $item["dexterity"] = $dbc->get("dexterity");
        $item["speed"] = $dbc->get("speed");
        $item["magicSkill"] = $dbc->get("magicSkill");
        
        // Augmentation de la caracteristique la plus haute
        $field1 = array_search(max($item), $item);
        $param["CRABE"] = 0;
        if ($item[$field1] <= $playerSrc->get("level") * 3) {
            $param["TYPE_ATTACK"] = LEVEL_UP_FAIL;
            $playerSrc->set("levelUp", 0);
            $playerSrc->set("xp", max(0, $playerSrc->get("xp") - getXPLevelUP($level)));
            $playerSrc->updateDBr($db);
            $param["CRABE"] = 1;
        } else {
            if ($field1 == "hp") {
                $playerSrc->addModif("hp", DICE_ADD, 15);
                $playerSrc->set("currhp", $playerSrc->get("currhp") + 15);
                $playerSrc->set("hp", $playerSrc->get("hp") + 15);
            } else
                $playerSrc->addModif($field1, DICE_D6, 1);
                
                // Division par 2 de la carac la plus haute
            $item[$field1] = ceil($item[$field1] / 2);
            
            // Nouvelle augmentation de la carac désormais la plus haute
            $field2 = array_search(max($item), $item);
            if ($field2 == "hp") {
                $playerSrc->addModif("hp", DICE_ADD, 15);
                $playerSrc->set("hp", $playerSrc->get("hp") + 15);
                $playerSrc->set("currhp", $playerSrc->get("currhp") + 15);
            } else
                $playerSrc->addModif($field2, DICE_D6, 1);
                
                // Reduction des barres de progression
            $playerSrc->setSub("Upgrade", $field1, $playerSrc->getSub("Upgrade", $field1) - $item[$field2]);
            $playerSrc->setSub("Upgrade", $field2, 0);
            
            // Initialisation des nouvelles caracs
            self::initFigthCharac($playerSrc, $playerSrc->getObj("Modifier"), $db);
            $playerSrc->set("levelUp", 0);
            $playerSrc->set("xp", $playerSrc->get("xp") - getXPLevelUP($playerSrc->get("level")));
            $playerSrc->set("level", $playerSrc->get("level") + 1);
            $playerSrc->updateDBr($db);
            
            // Variable pour le message
            $param["LEVEL"] = $playerSrc->get("level");
            $param["FIELD1"] = $field1;
            $param["FIELD2"] = $field2;
        }
        return ACTION_NO_ERROR;
    }

    static function addXP($playerSrc, $value, $db)
    {
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->set("xp", $playerSrc->get("xp") + $value);
            self::checkLevelUp($playerSrc, $db);
        }
    }

    static function checkLevelUp($playerSrc, $db)
    {
        $level = $playerSrc->get("level");
        $xp = $playerSrc->get("xp");
        if ($xp >= getXPLevelUP($level))
            $playerSrc->set("levelUp", 1);
        
        $playerSrc->updateDBr($db);
    }
    
    // TAG#enhance
    static function Enhance($playerSrc, $typeAction, &$param, $db, $ident = 0)
    {
        if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->externDBObj("Upgrade");
            $playerSrc->loadExternDBObj($db);
            $typeArm = self::getTypeArm($playerSrc, $db);
            $typeShield = self::getTypeShield($playerSrc, $db);
            
            switch ($typeAction) {
                
                /* *********************************************** DEFENSE PHYSIQUE ET MAGIQUE **************************************** */
                case DEFENSE_EVENT_SUCCESS:
                    self::ReduceShieldDurability($playerSrc, $param, $db);
                    switch ($typeShield) {
                        case 0:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                        case 4:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                            break;
                        case 8:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                        
                        case 9:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                            break;
                        case 10:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                            break;
                        
                        default:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                    }
                    break;
                
                case DEFENSE_EVENT_FAIL:
                    self::ReduceArmorDurability($playerSrc, $param, $db);
                    $playerSrc->setSub("Upgrade", "hp", $playerSrc->getSub("Upgrade", "hp") + 6);
                    $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                    break;
                
                case MAGICAL_DEFENSE_EVENT_FAIL: // A voir ca pas forcément de perte de vie.
                    self::ReduceArmorDurability($playerSrc, $param, $db);
                    $playerSrc->setSub("Upgrade", "hp", $playerSrc->getSub("Upgrade", "hp") + 6);
                    $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 1);
                    break;
                
                case MAGICAL_DEFENSE_EVENT_SUCCESS:
                    $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3);
                    break;
                
                /* *********************************************** ATTAQUE PHYSIQUE **************************************** */
                case ACTION_EVENT_DEATH_NPC:
                case ACTION_EVENT_DEATH_PC:
                case ATTACK_EVENT_SUCCESS:
                case ARCHERY_EVENT_SUCCESS:
                    self::ReduceArmDurability($playerSrc, $param, $db);
                    if ($ident == ABILITY_LIGHT) {
                        switch ($typeArm) {
                            
                            case 1:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3);
                                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                                break;
                            
                            case 0:
                            case 2:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
                                break;
                            case 3:
                            case 4:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 5);
                                break;
                            case 6:
                            case 7:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 5);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 3);
                                break;
                            default:
                                break;
                        }
                    } elseif ($ident == ABILITY_FLYARROW) {
                        $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 1);
                        $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                    } elseif ($ident == ABILITY_NEGATION) {
                        // $playerSrc->setSub("Upgrade", "speed",$playerSrc->getSub("Upgrade", "speed")+3);
                        $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4);
                        $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                    } else {
                        switch ($typeArm) {
                            
                            case 1:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4);
                                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 1);
                                break;
                            
                            case 0:
                            case 2:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 6);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                                break;
                            case 3:
                            case 4:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 5);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
                                break;
                            case 22:
                            case 5:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 8);
                                break;
                            case 6:
                            case 7:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4);
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 1);
                                break;
                            default:
                                break;
                        }
                    }
                    
                    break;
                case ATTACK_EVENT_FAIL:
                case ARCHERY_EVENT_FAIL:
                    self::ReduceArmDurability($playerSrc, $param, $db);
                    if ($ident == ABILITY_LIGHT) {
                        switch ($typeArm) {
                            
                            case 1:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2);
                                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                                break;
                            
                            case 0:
                            case 2:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2);
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                                break;
                            case 3:
                            case 4:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                                break;
                            case 6:
                            case 7:
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                                break;
                            default:
                                break;
                        }
                    } elseif ($ident == ABILITY_FLYARROW) {
                        $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 1);
                        $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                    } elseif ($ident == ABILITY_NEGATION) {
                        $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
                        $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2);
                    } else {
                        switch ($typeArm) {
                            case 1:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                                $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                                break;
                            case 2:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4);
                                break;
                            case 3:
                            case 4:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
                                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                                break;
                            case 22:
                            case 5:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 4);
                                // $playerSrc->setSub("Upgrade", "speed",$playerSrc->getSub("Upgrade", "speed")+3);
                                break;
                            case 6:
                            case 7:
                                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2);
                                break;
                            
                            default:
                                break;
                        }
                    }
                    break;
                
                /* ************************************************ LES COMPETENCES ********************************************** */
                case ABILITY_GUARD_EVENT:
                    self::ReduceShieldDurability($playerSrc, $param, $db);
                    switch ($typeShield) {
                        case 0:
                        case 21:
                        case 8:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
                            break;
                        
                        case 9:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                        case 10:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                    }
                    break;
                
                case ABILITY_TWIRL:
                    self::ReduceArmDurability($playerSrc, $param, $db);
                    switch ($typeArm) {
                        case 1:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 1);
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 4);
                            break;
                        case 2:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 6);
                            break;
                        case 3:
                        case 4:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 8);
                            break;
                        case 6:
                        case 7:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 5);
                            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 4);
                            break;
                        
                        default:
                            break;
                    }
                    break;
                
                case ABILITY_DISARM_EVENT_SUCCESS:
                    self::ReduceArmDurability($playerSrc, $param, $db);
                    switch ($typeArm) {
                        case 1:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 7);
                            break;
                        case 2:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 3);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
                            
                            break;
                        case 3:
                        case 4:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 3);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 6);
                            break;
                        case 6:
                        case 7:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
                            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 3);
                            break;
                        
                        default:
                            break;
                    }
                    
                    break;
                
                case ABILITY_DISARM_EVENT_FAIL:
                    self::ReduceArmDurability($playerSrc, $param, $db);
                    switch ($typeArm) {
                        case 1:
                            // $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 1);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                        case 2:
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 1);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 3);
                            break;
                        case 3:
                        case 4:
                            // $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 2);
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 1);
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 4);
                            break;
                        case 6:
                        case 7:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
                            $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 2);
                            break;
                        
                        default:
                            break;
                    }
                    $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 4);
                    break;
                case ABILITY_STEAL_EVENT_FAIL:
                case ABILITY_STEAL_EVENT_SUCCESS:
                    $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
                    break;
                case ABILITY_LARCENY_EVENT_FAIL:
                case ABILITY_LARCENY_EVENT_SUCCESS:
                    // $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 1);
                    $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
                    break;
            }
            // mise a jour jauge Competence Garde Octobian
            switch ($typeAction) {
                case ATTACK_EVENT_SUCCESS:
                case ATTACK_EVENT_FAIL:
                    
                    $param["IDENT"] = $ident;
                    switch ($ident) {
                        case ABILITY_DAMAGE:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                        case ABILITY_POWERFUL:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                        case ABILITY_TREACHEROUS:
                            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                        case ABILITY_THRUST:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                        case ABILITY_STUNNED:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                        case ABILITY_KNOCKOUT:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                        case ABILITY_LIGHT:
                            break;
                        case ABILITY_PROJECTION:
                            $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
                            $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") - 2);
                            break;
                    }
            }
            
            $playerSrc->updateDBr($db);
            $playerSrc->unlinkExternDBObj("Upgrade");
        }
    }
}
?>
