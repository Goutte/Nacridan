<?php
// require_once(HOMEPATH."/economy/ddef.inc.php");
require_once (HOMEPATH . "/class/FighterGroup.inc.php");
require_once (HOMEPATH . "/factory/MailFactory.inc.php");

class GdCFactory
{

    static function askJoinGdC($playerSrc, $name, $type, &$err, $db)
    {
        $err = "";
        if (is_numeric($name))
            $dbp = new DBCollection("SELECT id, name FROM Player WHERE id=" . quote_smart($name), $db);
        else
            $dbp = new DBCollection("SELECT id, name FROM Player WHERE name='" . quote_smart($name) . "'", $db);
        
        if ($dbp->eof()) {
            $err = localize("Erreur : Ce joueur n'existe pas.");
            return;
        }
        
        if ($dbp->get("id") == $playerSrc->get("id")) {
            $err = localize("Erreur : Vous ne pouvez pas vous inviter vous même !");
            return;
        }
        
        if ($playerSrc->get("id_FighterGroup") != 0) {
            $dbx = new DBCollection("SELECT id_FighterGroup FROM Player WHERE id=" . $dbp->get("id"), $db);
            if ($dbx->get("id_FighterGroup") == $playerSrc->get("id_FighterGroup")) {
                $err = localize("Erreur : Ce joueur fait déjà partie de votre groupe de chasse !");
                return;
            }
        }
        
        $dbx = new DBCollection("SELECT id FROM FighterGroupAskJoin WHERE id_Player=" . $playerSrc->get('id') . " AND id_Guest=" . $dbp->get('id'), $db);
        if (! $dbx->eof()) {
            $err = localize("Erreur : Cette demande a déjà été faite. Vous devez attendre la réponse");
            return;
        }
        
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = array();
        $dest[$dbp->get("name")] = $dbp->get("name");
        $type = 2;
        MailFactory::sendSingleDestMsg($playerSrc, localize("Invitation GDC"), localize($playerSrc->get("name") . " vous a proposé de rejoindre son groupe. Pour accepter, rendez-vous dans l'onglet diplomatie/groupe de chasse."), $dest, $type, $err, $db, - 1, 0);
        
        $datetime = time();
        $dbi = new DBCollection("INSERT INTO FighterGroupAskJoin (id_Player,id_Guest,date) VALUES (" . $playerSrc->get("id") . "," . $dbp->get("id") . ",'" . gmdate("Y-m-d H:i:s", $datetime) . "')", $db, 0, 0, false);
        $err = localize("Votre invitation a été envoyée au joueur.");
        $playerSrc->updateDB($db);
    }

    static function validateJoinGdC($playerSrc, $id, $type, &$err, $db)
    {
        $master = new Player();
        $master->externDBObj("Modifier");
        $master->load($id, $db);
        $datetime = time();
        
        // Si le personnage est déjà dans un groupe
        if ($playerSrc->get("id_FighterGroup") != 0) {
            $err = "Vous devez d'abord quitter votre groupe avant de pouvoir en rejoindre un autre";
            return;
        }
        
        // S'il n'y a pas de groupe existant alors création d'un groupe
        if ($master->get("id_FighterGroup") == 0) {
            $group = new FighterGroup();
            $group->set("date", gmdate("Y-m-d H:i:s", $datetime));
            $group->set("id_Player", $master->get("id"));
            $id_Group = $group->addDB($db);
            if ($id_Group == - 1) {
                $err = "Erreur : le groupe de chasse n'a pas pu être créé. ";
                return;
            } else
                $dbc = new DBCollection("UPDATE Player SET id_FighterGroup=" . $id_Group . " WHERE id=" . $id, $db, 0, 0, false);
        } else { // Si le groupe existe déjà, ajout du nouveau membre
            $id_Group = $master->get("id_FighterGroup");
        }
        $playerSrc->set("id_FighterGroup", $id_Group);
        $playerSrc->updateDBr($db);
        
        // Suppression des invitations du joueur ayant accepté de rejoindre le groupe
        $dbc = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
        // Suppression de l'invitation acceptée
        $dbc = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Player=" . $master->get("id") . " AND id_Guest=" . $playerSrc->get("id"), $db, 0, 0, false);
        
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = array();
        $dest[$master->get("name")] = $master->get("name");
        $type = 2;
        MailFactory::sendSingleDestMsg($playerSrc, localize("Invitation GDC"), localize($playerSrc->get("name") . " a rejoint votre groupe de chasse."), $dest, $type, $err, $db, - 1, 0);
        $err = "Vous avez rejoint le groupe de chasse avec succès. Vos invitations ont été par conséquent annulées.";
    }

    static function refuseJoinGdC($playerSrc, $id, $type, $err, $db)
    {
        $master = new Player();
        $master->externDBObj("Modifier");
        $master->load($id, $db);
        
        $dbc = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Player=" . $master->get("id") . " AND id_Guest=" . $playerSrc->get("id"), $db, 0, 0, false);
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = array();
        $dest[$master->get("name")] = $master->get("name");
        $type = 2;
        MailFactory::sendSingleDestMsg($playerSrc, localize("Invitation GDC"), localize($playerSrc->get("name") . " a refusé votre invitation dans votre groupe de chasse."), $dest, $type, $err, $db, - 1, 0);
    }

    static function banGdCMember($playerSrc, $playeridarray, $type, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        
        if ($playerSrc->getSub("FighterGroup", "id_Player") != $playerSrc->get("id"))
            return "Vous devez être le leader du groupe pour bannir un membre";
        
        foreach ($playeridarray as $key => $val) {
            $dbu = new DBCollection("UPDATE Player SET id_FighterGroup=0 WHERE id=" . $val, $db, 0, 0, false);
            $dbx = new DBCollection("SELECT name FROM Player WHERE id=" . $val, $db);
            $dest[$dbx->get("name")] = $dbx->get("name");
        }
        
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $type = 2;
        MailFactory::sendSingleMsgById($playerSrc, localize("Information"), localize("Vous avez été exclu de votre groupe de chasse par le leader."), $playeridarray, $type, $err, $db, - 1, 0);
        
        // S'il n'y avait que le chef dans le groupe alors dissolution
        $info = "";
        $dbf = new DBCollection("SELECT id FROM Player WHERE id_FighterGroup=" . $playerSrc->get("id_FighterGroup"), $db);
        if ($dbf->count() == 1) {
            $type = 2;
            self::dissolGdC($playerSrc, $type, $err, $db, 1);
            $info = "Vous avez banni tous les membres, le groupe a donc été dissous.";
        }
        
        $err = localize("Les personnages séléctionnés ont été exclu de votre groupe de chasse. " . $info);
        return $err;
    }

    static function dissolGdC($playerSrc, $type, &$err, $db, $quit = 0)
    {
        $err = "";
        
        $dest = array();
        
        // Récupération des noms des joueurs appartenant au GdC pour envoi mail
        $dbu = new DBCollection("SELECT * FROM Player WHERE id_FighterGroup=" . $playerSrc->get("id_FighterGroup"), $db);
        while (! $dbu->eof()) {
            $dest[$dbu->get("name")] = $dbu->get("name");
            // suppression des auras
            $dbbm = new DBCollection("DELETE FROM BM WHERE name='Aura de courage' AND id_Player\$src=" . $dbu->get("id") . " AND id_Player <> " . $dbu->get("id"), $db, 0, 0, false);
            $dbbm = new DBCollection("DELETE FROM BM WHERE name='Aura de résistance' AND id_Player\$src=" . $dbu->get("id") . " AND id_Player <> " . $dbu->get("id"), $db, 0, 0, false);
            
            $dbu->next();
        }
        
        // Suppression des messages de la tribune.
        $dbd = new DBCollection("DELETE FROM  FighterGroupMsg WHERE id_FighterGroup=" . $playerSrc->get("id_FighterGroup"), $db, 0, 0, false);
        
        $dbu = new DBCollection("DELETE FROM FighterGroup  WHERE id=" . $playerSrc->get("id_FighterGroup"), $db, 0, 0, false);
        $dbu = new DBCollection("UPDATE Player SET id_FighterGroup=0 WHERE id_FighterGroup=" . $playerSrc->get("id_FighterGroup"), $db, 0, 0, false);
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        // Si dissolution par le leader
        if (! $quit) {
            $type = 2;
            MailFactory::sendSingleDestMsg($playerSrc, localize("Information"), localize("Le groupe de chasse a été dissous par le leader."), $dest, $type, $err, $db, - 1, 0);
            $err = "Le groupe de chasse a été dissous";
        }  // Si dernier membre qui quitte le groupe
else
            $err = "Vous ne faites plus partie d'aucun groupe de chasse.";
        
        return $err;
    }

    static function quitGdC($playerSrc, $type, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        $info = "";
        $dest = array();
        $idf = $playerSrc->get("id_FighterGroup");
        // S'il n'y avait que 2 joueurs dans le groupe alors dissolution
        $dbx = new DBCollection("SELECT id FROM Player WHERE id_FighterGroup=" . $idf, $db);
        if ($dbx->count() == 2) {
            $type = 2;
            self::dissolGdC($playerSrc, $type, $err, $db, 1);
            $info = "Le groupe a été dissous.";
        } else { // Si plus de 2 joueurs dans le GdC
                 // suppression des auras
            $dbbm = new DBCollection("DELETE FROM BM WHERE name='Aura de courage' AND id_Player\$src=" . $playerSrc->get("id") . " AND id_Player <> " . $playerSrc->get("id"), $db, 0, 0, false);
            $dbbm = new DBCollection("DELETE FROM BM WHERE name='Aura de résistance' AND id_Player\$src=" . $playerSrc->get("id") . " AND id_Player <> " . $playerSrc->get("id"), $db, 0, 0, false);
            $playerSrc->set("id_FighterGroup", 0);
            $playerSrc->updateDB($db);
            $err = "Vous ne faites plus partie d'aucun groupe de chasse";
        }
        
        // Envoi d'un message au leader du groupe
        $dbu = new DBCollection("SELECT id_Player, name FROM FighterGroup  LEFT JOIN Player ON Player.id_FighterGroup=FighterGroup.id WHERE Player.id_FighterGroup=" . $idf . " AND Player.id=FighterGroup.id_Player", $db);
        $dest = array();
        $dest[$dbu->get("name")] = $dbu->get("name");
        $type = 2;
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        MailFactory::sendSingleDestMsg($playerSrc, localize("Information GdC"), localize($playerSrc->get("name") . " a quitté le groupe de chasse." . $info), $dest, $type, $err2, $db, - 1, 0);
        // $err=$err2;
    }

    static function transfertGdC($playerSrc, $id_Successor, $type, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        $info = "";
        $dest = array();
        $type = 2;
        // Changement de leader
        $dbt = new DBCollection("SELECT * FROM Player WHERE id=" . $id_Successor, $db);
        $dbc = new DBCollection("UPDATE FighterGroup SET id_Player=" . $id_Successor . " WHERE id=" . $playerSrc->get("id_FighterGroup"), $db, 0, 0, false);
        
        // Envoi d'un message au nouveau leader du groupe
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $mast = array();
        $mast[$dbt->get("name")] = $dbt->get("name");
        MailFactory::sendSingleDestMsg($playerSrc, localize("Information GdC"), localize($playerSrc->get("name") . " vous a passé ses pouvoirs. Vous êtes le nouveau leader du groupe de chasse"), $mast, $type, $err, $db, - 1, 0);
        
        // Récupération des noms des joueurs appartenant au GdC pour envoi mail
        $dbu = new DBCollection("SELECT * FROM Player WHERE id_FighterGroup=" . $playerSrc->get("id_FighterGroup"), $db);
        while (! $dbu->eof()) {
            if ($dbu->get("name") != $dbt->get("name"))
                $dest[$dbu->get("name")] = $dbu->get("name");
            $dbu->next();
        }
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        MailFactory::sendSingleDestMsg($playerSrc, localize("Information GdC"), localize($dbt->get("name") . " est le nouveau leader du groupe de chasse"), $dest, $type, $err, $db, - 1, 0);
        
        // Message retour
        $err = "Vous avez passé vos pouvoirs avec succès. " . $dbt->get("name") . " est le nouveau leader du groupe de chasse.";
    }

    static function cancelInviteGdC($playerSrc, $arr, $type, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        $type = 2;
        $cond = self::getCondFromArray($arr, "id_Guest", "OR");
        $dbc = new DBCollection("DELETE FROM FighterGroupAskJoin WHERE id_Player=" . $playerSrc->get("id") . " AND " . $cond, $db, 0, 0, false);
    }

    static protected function getCondFromArray($arr, $fieldname, $cond)
    {
        $msg = "";
        $first = 1;
        
        foreach ($arr as $key => $val) {
            if ($first)
                $msg = $fieldname . "=" . $val;
            else
                $msg .= " " . $cond . " " . $fieldname . "=" . $val;
            $first = 0;
        }
        return $msg;
    }
}
?>
