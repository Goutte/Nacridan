<?php
require_once (HOMEPATH . "/class/Exploitation.inc.php");

class InteractionChecking
{

    /* ********************************************************** FONCTIONS AUXILLIAIRES ****************************************** */
    static function allowedToAct($playerSrc, $action, $db)
    {
        if ($action == NEWATB)
            return 1;
        
        if ($playerSrc->get("id_BasicRace") == 112 || $playerSrc->get("room") == 21)
            return 0;
        if ($playerSrc->get("id_BasicRace") == 263 && $action != FREE)
            return 0;
        
        return 1;
    }

    static function caseFree($x, $y, $map, $db)
    {
        $dbp = new DBCollection("SELECT id FROM Player WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbp->eof())
            return false;
        
        $dbe = new DBCollection("SELECT id FROM Exploitation WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbe->eof())
            return false;
        
        $dbb = new DBCollection("SELECT id FROM Building WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbb->eof())
            return false;
        
        $dbc = new DBCollection("SELECT id FROM City WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbc->eof())
            return false;
        
        return true;
    }

    static function distanceCheckingInteraction($playerSrc, $opponent, &$db, $distance, $sameroom = 1)
    {
        $dx = $playerSrc->get("x") - $opponent->get("x");
        $dy = $playerSrc->get("y") - $opponent->get("y");
        if ($distance < ((abs($dx) + abs($dy) + abs($dx + $dy)) / 2))
            return ACTION_TARGET_TOO_FAR;
        
        if ($playerSrc->get("room") != TOWER_ROOM && $playerSrc->get("room") != $opponent->get("room") && $sameroom)
            return ACTION_NOT_SAME_ROOM;
        
        return ACTION_NO_ERROR;
    }

    static function checkingInvFull($playerSrc, $ident, &$param, &$db)
    {
        switch ($ident) {
            case TALENT_MINE:
                if (PlayerFactory::getIdBagByName($playerSrc->get("id"), "Sac de gemmes", $db)) {
                    if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 1, $db)) {
                        $param["INV"] = 1;
                        return ACTION_NO_ERROR;
                    }
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $param["INV"] = 0;
                    return ACTION_NO_ERROR;
                }
                break;
            
            case TALENT_DISMEMBER:
            case TALENT_SCUTCH:
            case TALENT_SPEAK:
            case TALENT_CUT:
                
                if (PlayerFactory::getIdBagByName($playerSrc->get("id"), "Sac de matières premières", $db)) {
                    if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 2, $db)) {
                        
                        $param["INV"] = 2;
                        return ACTION_NO_ERROR;
                    }
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    echo "bizarre";
                    $param["INV"] = 0;
                    return ACTION_NO_ERROR;
                }
                break;
            
            case TALENT_HARVEST:
                if (PlayerFactory::getIdBagByName($playerSrc->get("id"), "Sac d'herbes", $db)) {
                    if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 3, $db)) {
                        $param["INV"] = 3;
                        return ACTION_NO_ERROR;
                    }
                }
                if (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)) {
                    $param["INV"] = 0;
                    return ACTION_NO_ERROR;
                }
                break;
            
            default:
                break;
        }
        return INV_FULL_ERROR;
    }

    static function checkingAbilityAPAndTurtle($playerSrc, $ident, &$db)
    {
        if ($playerSrc->get("state") == "turtle") {
            return ABILITY_FORBIDDEN_IN_TURTLE;
        }
        $dbc = new DBCollection(
            "SELECT * FROM Ability INNER JOIN BasicAbility ON Ability.id_BasicAbility=BasicAbility.id WHERE id_BasicAbility='" . ($ident - 100) . "' AND id_Player=" .
                 $playerSrc->get("id"), $db);
        
        if ($dbc->count() < 1)
            return ACTION_NOT_ALLOWED;
        
        $time = $dbc->get("modifierPA");
        if ($dbc->get("timeAttack") == 'YES')
            $time += $playerSrc->getScore("timeAttack");
        
        if ($playerSrc->get("ap") < $time)
            return ACTION_NOT_ENOUGH_AP;
        
        return ACTION_NO_ERROR;
    }

    static function checkingSpellAPAndTurtle($playerSrc, $ident, &$db)
    {
        if ($playerSrc->get("state") == "turtle") {
            return SPELL_FORBIDDEN_IN_TURTLE;
        }
        
        $dbc = new DBCollection(
            "SELECT * FROM MagicSpell LEFT JOIN BasicMagicSpell ON MagicSpell.id_BasicMagicSpell=BasicMagicSpell.id WHERE BasicMagicSpell.id=" . ($ident - 200) . " AND id_Player=" .
                 $playerSrc->get("id"), $db);
        $time = $dbc->get("PA");
        
        if ($playerSrc->get("ap") < $time)
            return ACTION_NOT_ENOUGH_AP;
        
        return ACTION_NO_ERROR;
    }

    static function checkingTalentAP($playerSrc, $ident, &$db)
    {
        $dbc = new DBCollection(
            "SELECT * FROM Talent LEFT JOIN BasicTalent ON Talent.id_BasicTalent=BasicTalent.id WHERE BasicTalent.id=" . ($ident - 300) . " AND id_Player=" . $playerSrc->get("id"), 
            $db);
        $time = $dbc->get("PA");
        
        if ($playerSrc->get("ap") < $time)
            return ACTION_NOT_ENOUGH_AP;
        
        return ACTION_NO_ERROR;
    }

    static function basicCheckingInteraction($playerSrc, $id_Opponent, &$db)
    {
        if ($id_Opponent <= 0)
            return TARGET_FIELD_INVALID;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        
        if ($opponent->load($id_Opponent, $db) == - 1)
            return ERROR_TARGET_LOADING;
        
        if ($opponent->get("disabled") != 0)
            return ACTION_DISABLED_TARGET;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        return ACTION_NO_ERROR;
    }

    /* ************************************************************* COMMON CHECKING ***************************************** */
    static function checkingCommonAbility($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db, $dist = 1)
    {
        $error = self::checkingAbilityAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        if ($ident == "ABILITY_BOLAS" && $opponent->get("state") == "creeping")
            return TARGET_ON_THE_GROUND_ERROR;
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, $dist);
        if ($error)
            return $error;
        
        $malus = 0;
        
        $error = AbilityFactory::ability($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        return ACTION_NO_ERROR;
    }

    static function checkingCommonSpell($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 1);
        if ($error)
            return $error;
        
        $malus = 0;
        
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        return ACTION_NO_ERROR;
    }

    /*
     * static function checkingCommonExtraction($playerSrc,$ident,$id_Exploitation, &$paramability, &$param, &$db,$distance)
     * {
     * $error=self::checkingTalentAP($playerSrc, $ident, $db);
     * if($error)
     * return $error;
     *
     * $error=self::checkingInvFull($playerSrc, $ident, $param, $db);
     * if($error)
     * return $error;
     *
     * $state = $playerSrc->get("state");
     * if($playerSrc->get("disabled")!=0 || $state=="questionning" || $state=="prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
     * return ACTION_NOT_ALLOWED;
     *
     *
     * if($id_Exploitation <= 0)
     * return ACTION_INVALID_TARGET;
     *
     *
     * $res= new Exploitation();
     * if($res->load($id_Exploitation,$db) == -1)
     * return ACTION_INVALID_TARGET;
     *
     * $dx = $playerSrc->get("x")-$res->get("x");
     * $dy = $playerSrc->get("y")-$res->get("y");
     * if( $distance < ((abs($dx) + abs($dy) + abs($dx + $dy))/2))
     * return ACTION_EXPLOITATION_TOO_FAR;
     *
     * $malus = 0;
     * $error=TalentFactory::talent($playerSrc,$ident,$malus,$paramability,$param,$db);
     * if($error)
     * return $error;
     *
     * return ACTION_NO_ERROR;
     * }
     */
    
    /* ************************************************************* ACTION DE BASE : ATTACK & ARCHERY ***************************************** */
    static function checkingGive($playerSrc, $id_Object, $id_Target, &$param, &$db)
    {
        if ($playerSrc->get("ap") < GIVE_OBJECT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Target, $db);
        if ($error)
            return $error;
        
        if ($id_Object <= 0)
            return OBJECT_FIELD_INVALID;
        
        $equip = new Equipment();
        if ($equip->load($id_Object, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($equip->get("weared") == 'YES')
            return WEARED_OBJECT_ERROR;
        
        if ($equip->get("id_Player") != $playerSrc->get("id"))
            return NOT_YOUR_OBJECT_ERROR;
        
        $target = new Player();
        $target->externDBObj("Modifier");
        $target->load($id_Target, $db);
        
        $param["INV"] = PlayerFactory::checkingInvfullForObject($target, $equip->get("id_EquipmentType"), $param, $db);
        if ($param["INV"] == - 1)
            return INV_FULL_ERROR;
        
        $error = self::distanceCheckingInteraction($playerSrc, $target, $db, 1);
        if ($error)
            return $error;
        
        $param["AP"] = GIVE_OBJECT_AP;
        $playerSrc->set("ap", $playerSrc->get("ap") - $param["AP"]);
        $error = BasicActionFactory::give($playerSrc, $equip, $target, $param, $db);
        return $error;
    }

    static function checkingGiveMoney($playerSrc, $id_Target, $value, &$param, &$db)
    {
        if ($playerSrc->get("ap") < GIVE_MONEY_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($value <= 0)
            return MONEY_FIELD_INVALID;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Target, $db);
        if ($error)
            return $error;
        
        if ($playerSrc->get("money") < $value)
            return NOT_ENOUGH_MONEY_ERROR;
        
        $target = new Player();
        $target->externDBObj("Modifier");
        $target->load($id_Target, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $target, $db, 1);
        if ($error)
            return $error;
        
        $param["AP"] = GIVE_MONEY_AP;
        $playerSrc->set("ap", $playerSrc->get("ap") - $param["AP"]);
        $error = BasicActionFactory::giveMoney($playerSrc, $target, $value, $param, $db);
        return $error;
    }

    static function checkingAttackBuilding($playerSrc, $id_Building, $typeAttack, &$param, &$db, $modifierA = null, $modifierB = null)
    {
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        if ($typeArm == 5)
            return ACTION_WRONG_ARM_ERROR;
        
        if ($typeAttack < 1)
            return TYPEATTACK_FIELD_INVALID;
        
        if ($typeAttack == 1 && $playerSrc->get("ap") < $playerSrc->getScore("timeAttack"))
            return ACTION_NOT_ENOUGH_AP;
        
        if ($typeAttack == 2 && $playerSrc->get("ap") < 8)
            return ACTION_NOT_ENOUGH_AP;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($id_Building <= 0)
            return BUILDING_FIELD_INVALID;
        
        if ($playerSrc->get("inbuilding") && $playerSrc->get("inbuilding") != $id_Building)
            return ACTION_BUILDING_TOO_FAR;
        
        $building = new Building();
        if ($building->load($id_Building, $db) == - 1)
            return ERROR_BUILDING_LOADING;
        
        $gap = 1;
        $dx = $playerSrc->get("x") - $building->get("x");
        $dy = $playerSrc->get("y") - $building->get("y");
        if (((abs($dx) + abs($dy) + abs($dx + $dy)) / 2) > $gap)
            return ACTION_BUILDING_TOO_FAR;
        
        $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
        if ($typeAttack == 1)
            $param["AP"] = $playerSrc->getScore("timeAttack");
        else
            $param["AP"] = 8;
        
        $playerSrc->set("ap", $playerSrc->get("ap") - $playerSrc->getScore("timeAttack"));
        $error = BasicActionFactory::attackBuilding($playerSrc, $building, $typeAttack, $param, $db);
        return $error;
    }

    static function checkingAttack($playerSrc, $id_Opponent, &$param, &$db, $modifierA = null, $modifierB = null)
    {
        if ($playerSrc->get("ap") < $playerSrc->getScore("timeAttack"))
            return ACTION_NOT_ENOUGH_AP;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 1);
        if ($error)
            return $error;
        
        $param["XP"] = 0;
        $param["AP"] = $playerSrc->getScore("timeAttack");
        $playerSrc->set("ap", $playerSrc->get("ap") - $playerSrc->getScore("timeAttack"));
        $error = BasicActionFactory::attack($playerSrc, $opponent, $param, $db);
        return $error;
    }

    static function checkingArchery($playerSrc, $id_Opponent, $id_Arrow, &$param, &$db, $modifierA = null, $modifierB = null)
    {
        if ($playerSrc->get("ap") < $playerSrc->getScore("timeAttack"))
            return ACTION_NOT_ENOUGH_AP;
        
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        if (! ($typeArm == 5 or $typeArm == 22))
            return ACTION_WRONG_ARM_ERROR;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 3);
        if ($error)
            return $error;
        
        if ($id_Arrow == - 1)
            return ARROW_FIELD_INVALID;
        
        $eq = new Equipment();
        $eqmodifier = new Modifier();
        
        $dbc = new DBCollection(
            "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") .
                 " FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id WHERE Equipment.id=" .
                 $id_Arrow, $db);
        if ($dbc->eof())
            return ERROR_OBJECT_LOADING;
            
            // Détermination du malus dû à la portée
        $dx = $playerSrc->get("x") - $opponent->get("x");
        $dy = $playerSrc->get("y") - $opponent->get("y");
        $dist = (abs($dx) + abs($dy) + abs($dx + $dy)) / 2;
        $malus = min((0.1 * (1 - $dist)), 0);
        $param["MALUS_GAP"] = - $malus * 100;
        $modifier = new Modifier();
        $modifier->setModif("attack", DICE_D6, (floor($playerSrc->getModif("attack", DICE_D6) * $malus)));
        
        // Détermination du bonus de la flèche
        $param["LEVEL_ARROW"] = $eq->get("level");
        $eq->DBLoad($dbc, "EQ");
        $eqmodifier->DBLoad($dbc, "EQM");
        $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
        $modifier->setModif("damage", DICE_ADD, $eqmodifier->getModif("damage", DICE_ADD));
        $modifier->setModif("attack", DICE_ADD, $eqmodifier->getModif("attack", DICE_ADD));
        
        $modifier->addModifObjToBM($eqmodifier);
        $modifier->validateBM();
        
        $param["XP"] = 0;
        $param["AP"] = $playerSrc->getScore("timeAttack");
        $playerSrc->set("ap", $playerSrc->get("ap") - $playerSrc->getScore("timeAttack"));
        $error = BasicActionFactory::archery($playerSrc, $opponent, $id_Arrow, $param, $db, $modifier);
        return $error;
    }

    static function checkingHustle($playerSrc, $id_Opponent, &$param, &$db)
    {
        $PA = $playerSrc->getTimeToMove($db);
        if ($playerSrc->get("ap") < ($PA + HUSTLE_AP))
            return ACTION_NOT_ENOUGH_AP;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 1, 0);
        if ($error)
            return $error;
            
            // Détermination du malus dû à la portée
        $param["XP"] = 0;
        $error = BasicActionFactory::hustle($playerSrc, $opponent, $param, $db);
        return $error;
    }

    /* ************************************************************* LES COMPETENCES D'ATTAQUE ******************************************************* */
    static function checkingAbilityAttack($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        $typeShield = PlayerFactory::getTypeShield($playerSrc, $db);
        $dbc = new DBCollection("SELECT * FROM BasicAbility WHERE id=" . ($ident - 100), $db);
        
        if ($typeArm == 5 && $dbc->get("usable") == 1)
            return ACTION_WRONG_ARM_ERROR;
        
        if ($typeShield != 21 && $ident == ABILITY_TREACHEROUS)
            return ACTION_WRONG_SHIELD_ERROR;
        
        $gap = 1;
        if ($ident == ABILITY_LARCENY)
            $gap = 2;
        $error = self::checkingCommonAbility($playerSrc, $ident, $id_Opponent, $paramability, $param, $db, $gap);
        if ($error)
            return $error;
        
        if ($paramability["SUCCESS"]) {
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($id_Opponent, $db);
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = AbilityFactory::abilityAttack($playerSrc, $ident, $opponent, $param, $db);
        }
        return $error;
    }

    static function checkingAbilityArchery($playerSrc, $ident, $id_Opponent, $id_Arrow, &$paramability, &$param, &$db, $modifierA = null, $modifierB = null)
    {
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        if (! ($typeArm == 5 or $typeArm == 22))
            return ACTION_WRONG_ARM_ERROR;
        
        if ($id_Arrow == - 1)
            return ARROW_FIELD_INVALID;
        
        $eq = new Equipment();
        $eqmodifier = new Modifier();
        $dbc = new DBCollection(
            "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") .
                 " FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id WHERE Equipment.id=" .
                 $id_Arrow, $db);
        if ($dbc->eof())
            return ERROR_OBJECT_LOADING;
        
        $gap = 3;
        $bm = 0;
        if ($ident == ABILITY_FAR) {
            $gap = 4;
            $bm = - 1;
            // $playerSrc->setSub("Upgrade", "dexterity",$playerSrc->getSub("Upgrade", "dexterity")+1);
        }
        $error = self::checkingCommonAbility($playerSrc, $ident, $id_Opponent, $paramability, $param, $db, $gap);
        if ($error)
            return $error;
        
        $param["IDENT"] = $ident;
        
        if ($paramability["SUCCESS"]) {
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($id_Opponent, $db);
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            
            $modifier = new Modifier();
            
            // Détermination du malus dû à la portée
            $dx = $playerSrc->get("x") - $opponent->get("x");
            $dy = $playerSrc->get("y") - $opponent->get("y");
            $dist = (abs($dx) + abs($dy) + abs($dx + $dy)) / 2;
            $malus = min(0, max(- 0.2, (0.1 * (1 - ($dist + $bm)))));
            $param["MALUS_GAP"] = $malus * 100;
            $modifier->setModif("attack", DICE_D6, (floor($playerSrc->getModif("attack", DICE_D6) * $malus)));
            
            // Détermination du bonus de la flèche
            $param["LEVEL_ARROW"] = $eq->get("level");
            $eq->DBLoad($dbc, "EQ");
            $eqmodifier->DBLoad($dbc, "EQM");
            $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
            $modifier->setModif("damage", DICE_ADD, $eqmodifier->getModif("damage", DICE_ADD));
            $modifier->setModif("attack", DICE_ADD, $eqmodifier->getModif("attack", DICE_ADD));
            // $modifier->addModifObjToBM($eqmodifier);
            $modifier->validateBM();
            $error = BasicActionFactory::archery($playerSrc, $opponent, $id_Arrow, $param, $db, $modifier, null, $ident);
        }
        return $error;
    }

    static function checkingAbilityFlyArrow($playerSrc, $ident, $target, &$paramability, &$param, &$db)
    {
        if ($playerSrc->get("state") == "turtle") {
            return ABILITY_FORBIDDEN_IN_TURTLE;
        }
        if ($playerSrc->get("ap") < $playerSrc->getScore("timeAttack"))
            return ACTION_NOT_ENOUGH_AP;
        
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        if (! ($typeArm == 5 or $typeArm == 22))
            return ACTION_WRONG_ARM_ERROR;
        
        if ($target[4] == $target[5] or $target[4] == $target[6] or $target[5] == $target[6])
            return ERROR_SAME_OBJECT;
        
        $opponent = array();
        $arrow = array();
        // $modifier = array();
        
        $j = 4;
        if ($target[3] == 0 && $target[6] == 0)
            $j = 3;
        
        $param["NB"] = $j;
        for ($i = 1; $i < $j; $i ++) {
            $error = self::basicCheckingInteraction($playerSrc, $target[$i], $db);
            if ($error)
                return $error;
            
            $opponent[$i] = new Player();
            $opponent[$i]->externDBObj("Modifier");
            $opponent[$i]->load($target[$i], $db);
            
            $error = self::distanceCheckingInteraction($playerSrc, $opponent[$i], $db, 3);
            if ($error)
                return $error;
                
                // Détermination du malus dû à la portée
                // $dx = $playerSrc->get("x") - $opponent[$i]->get("x");
                // $dy = $playerSrc->get("y") - $opponent[$i]->get("y");
                // $dist = (abs($dx) + abs($dy) + abs($dx + $dy)) / 2;
                // $malus = min((0.1 * (1 - $dist)), 0);
                
            // $param["MALUS_GAP"][$i] = - ($malus * 100);
                // $modifier[$i] = new Modifier();
                // $modifier[$i]->setModif("attack", DICE_D6, (floor($playerSrc->getModif("attack", DICE_D6) * $malus)));
                
            // Détermination du malus dû à la comp
                // $modifier[$i]->subModif("attack",DICE_D6,(floor($playerSrc->getModif("attack",DICE_D6)*0.5)));
            
            if ($target[$i + 3] == - 1)
                return TARGET_FIELD_INVALID;
            
            $arrow[$i] = new Equipment();
            $arrowmodifier[$i] = new Modifier();
            
            $dbc = new DBCollection(
                "SELECT " . $arrow[$i]->getASRenamer("Equipment", "EQ") . "," . $arrowmodifier[$i]->getASRenamer("Modifier_BasicEquipment", "EQM") .
                     " FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id WHERE Equipment.id=" .
                     $target[$i + 3], $db);
            if ($dbc->eof())
                return ERROR_OBJECT_LOADING;
                
                // Détermination du bonus de la flèche
            $arrow[$i]->DBLoad($dbc, "EQ");
            
            $arrowmodifier[$i]->DBLoad($dbc, "EQM");
            $arrowmodifier[$i]->updateFromEquipmentLevel($arrow[$i]->get("level"));
            // $modifier[$i]->setModif("damage", DICE_ADD, $arrowmodifier[$i]->getModif("damage", DICE_ADD));
            // $modifier[$i]->setModif("attack", DICE_ADD, $arrowmodifier[$i]->getModif("attack", DICE_ADD));
            // $modifier[$i]->addModifObjToBM($arrowmodifier[$i]);
            // $modifier[$i]->validateBM();
        }
        $error = AbilityFactory::ability($playerSrc, $ident, 0, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        $param["IDENT"] = $ident;
        
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = AbilityFactory::flyArrow($playerSrc, $opponent, $arrow, $arrowmodifier, $param, $db);
        }
    }

    static function checkingTwirl($playerSrc, $ident, &$paramability, &$param, &$db)
    {
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        if ($typeArm == 5)
            return ACTION_WRONG_ARM_ERROR;
        
        $error = self::checkingAbilityAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             $playerSrc->get("inbuilding"))
            return ACTION_NOT_ALLOWED;
        
        $malus = 0;
        $error = AbilityFactory::ability($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = AbilityFactory::twirl($playerSrc, $param, $db);
        }
        return $error;
    }

    static function checkingBolas($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        $BasicArm = PlayerFactory::getBasicArm($playerSrc, $db);
        if ($BasicArm != 205)
            return ACTION_WRONG_ARM_ERROR;
        
        $error = self::checkingCommonAbility($playerSrc, $ident, $id_Opponent, $paramability, $param, $db, 2);
        if ($error)
            return $error;
        
        if ($paramability["SUCCESS"]) {
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($id_Opponent, $db);
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $playerSrc->setSub("Upgrade", "speed", $playerSrc->getSub("Upgrade", "speed") + 5);
            $error = AbilityFactory::bolas($playerSrc, $opponent, $param, $db);
        }
        return $error;
    }

    /* ************************************************************* LES AUTRES COMPETENCES ******************************************************* */
    static function checkingAbilityWithoutTarget($playerSrc, $ident, &$paramability, &$param, &$db)
    {
        $error = self::checkingAbilityAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($ident == ABILITY_EXORCISM) {
            $curstate = min(floor($playerSrc->get("currhp") * 5 / $playerSrc->get("hp")), 4);
            if ($curstate > 2) {
                return ABILITY_EXORCISM_NOT_WELL_INJURED;
            }
        }
        
        $malus = 0;
        $error = AbilityFactory::ability($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = AbilityFactory::AbilityWithoutTarget($playerSrc, $ident, $param, $db);
        }
        return $error;
    }

    static function checkingFlaming($playerSrc, $ident, $id_Building, $id_Arrow, &$paramability, &$param, &$db)
    {
        $error = self::checkingAbilityAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $typeArm = PlayerFactory::getTypeArm($playerSrc, $db);
        if (! ($typeArm == 5 or $typeArm == 22))
            return ACTION_WRONG_ARM_ERROR;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($id_Building == - 1)
            return BUILDING_FIELD_INVALID;
        
        $bat = new Building();
        if ($bat->load($id_Building, $db) == - 1)
            return ERROR_BUILDING_LOADING;
            
            // if($playerSrc->get("inbuilding") && $playerSrc->get("inbuilding") != $id_Building)
            // return ACTION_BUILDING_TOO_FAR;
        
        $dx = $playerSrc->get("x") - $bat->get("x");
        $dy = $playerSrc->get("y") - $bat->get("y");
        if (((abs($dx) + abs($dy) + abs($dx + $dy)) / 2) > 3)
            return ACTION_BUILDING_TOO_FAR;
        
        if ($id_Arrow == - 1)
            return ARROW_FIELD_INVALID;
        
        $eq = new Equipment();
        $eqmodifier = new Modifier();
        $modifier = new Modifier();
        $dbc = new DBCollection(
            "SELECT " . $eq->getASRenamer("Equipment", "EQ") . "," . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") .
                 " FROM Equipment LEFT JOIN BasicEquipment  ON Equipment.id_BasicEquipment=BasicEquipment.id LEFT JOIN Modifier_BasicEquipment ON id_Modifier_BasicEquipment=Modifier_BasicEquipment.id WHERE Equipment.id=" .
                 $id_Arrow, $db);
        if ($dbc->eof())
            return ERROR_OBJECT_LOADING;
        
        $malus = 0;
        $error = AbilityFactory::ability($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        // Détermination du bonus de la flèche
        $eq->DBLoad($dbc, "EQ");
        $eqmodifier->DBLoad($dbc, "EQM");
        $eqmodifier->updateFromEquipmentLevel($eq->get("level"));
        $modifier->setModif("damage", DICE_ADD, $eqmodifier->getModif("damage", DICE_ADD));
        $modifier->setModif("attack", DICE_ADD, $eqmodifier->getModif("attack", DICE_ADD));
        $modifier->addModifObjToBM($eqmodifier);
        $modifier->validateBM();
        
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = AbilityFactory::flaming($playerSrc, $bat, $id_Arrow, $param, $db, $modifier);
        }
        return $error;
    }

    static function checkingSharpen($playerSrc, $ident, $id_Object, &$paramability, &$param, &$db)
    {
        $error = self::checkingAbilityAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        if ($id_Object <= 0)
            return OBJECT_FIELD_INVALID;
        
        $equip = new Equipment();
        if ($equip->load($id_Object, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($equip->get("id_EquipmentType") > 5 || $equip->get("id_BasicEquipment") == 205)
            return ACTION_WRONG_ARM_ERROR;
        
        if ($equip->get("id_Player") != $playerSrc->get("id"))
            return NOT_YOUR_OBJECT_ERROR;
        
        $malus = 0;
        $error = AbilityFactory::ability($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = AbilityFactory::sharpen($playerSrc, $equip, $param, $db);
        }
        return $error;
    }

    /* ************************************************************* LES SORTILEGES ******************************************************* */
    static function checkingSpellRecall($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        if ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TOWER_ROOM) {
            return ACTION_NOT_ALLOWED;
        }
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        if ($opponent->get("state") == "prison") {
            return ACTION_NOT_ALLOWED;
        }
        
        if ($opponent->get("status") == "NPC") {
            return ACTION_NOT_ALLOWED;
        }
        return self::checkingSpell($playerSrc, $ident, $id_Opponent, $paramability, $param, $db);
    }

    static function checkingSpell($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && $playerSrc->get("id_Owner") > 0) {
            $dbc = new DBCollection("SELECT * FROM Player WHERE id=" . $playerSrc->get("id_Owner"), $db);
            if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbc->get("x"), $dbc->get("y")) > 5)
                return GOLEM_CONTROL_LOST;
        }
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $gap = 1;
        if ($ident == SPELL_FIREBALL)
            $gap = 3;
        if ($ident == SPELL_LIGHTTOUCH || $ident == SPELL_REGEN || $ident == SPELL_BLOOD || $ident == M_INSTANTBLOOD)
            $gap = 2;
        
        if ($ident != SPELL_RECALL) {
            $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, $gap);
            if ($error)
                return $error;
        }
        $malus = 0;
        
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::spell($playerSrc, $ident, $opponent, $param, $db);
        }
        return $error;
    }

    static function checkingKine($playerSrc, $ident, $id_Opponent, $direction, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 5);
        if ($error)
            return $error;
        
        if ($direction == 0)
            return DIRECTION_FIELD_INVALID;
        
        $place = array();
        $x = $opponent->get("x");
        $y = $opponent->get("y");
        switch ($direction) {
            case 1:
                $x += 1;
                break;
            case 2:
                $y += 1;
                break;
            case 3:
                $x -= 1;
                break;
            case 4:
                $y -= 1;
                break;
            case 5:
                $x += 1;
                $y -= 1;
                break;
            case 6:
                $x -= 1;
                $y += 1;
                break;
            default:
                return DIRECTION_FIELD_INVALID;
        }
        
        $malus = 0;
        
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::kine($playerSrc, $opponent, $x, $y, $param, $db);
        }
        return $error;
    }

    static function checkingAngerCurse($playerSrc, $ident, $id_Opponent, $charac, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        if ($charac <= 0)
            return CHARAC_FIELD_INVALID;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $gap = 2;
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, $gap);
        if ($error)
            return $error;
        
        $malus = 0;
        
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::AngerCurse($playerSrc, $ident, $opponent, $charac, $param, $db);
        }
        return $error;
    }

    static function checkingSpring($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TOWER_ROOM))
            return ACTION_NOT_ALLOWED;
        
        if (! self::caseFree($x, $y, $playerSrc->get("map"), $db))
            return ERROR_PLACE_NOT_FREE;
        
        if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $x, $y) > 2)
            return ACTION_PLACE_TOO_FAR;
        
        $dbe = new DBCollection("SELECT * FROM Building WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicBuilding=15", $db);
        if ($dbe->count() >= 2)
            return TOO_MANY_SPRING;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::spring($playerSrc, $ident, $x, $y, $param, $db);
        }
        return $error;
    }

    static function checkingGolemDeFeu($playerSrc, &$db)
    {
        $dba = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Golem de feu'", $db);
        if (! $dba->eof())
            return FIRE_GOLEM_ALREADY_EXISTING;
        else
            return ACTION_NO_ERROR;
    }

    static function checkingFireCall($playerSrc, $ident, $target_id, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        $error = self::checkingGolemDeFeu($playerSrc, $db);
        if ($error)
            return $error;
        $state = $playerSrc->get("state");
        
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $place["x"] = 0;
        $place["y"] = 0;
        
        if (isset($target_id) && $target_id > 0) {
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($target_id, $db);
            
            if ($opponent->get("id_Owner") != 0 || $opponent->get("id_Member") != 0 || $opponent->get("id_BasicRace") != ID_BASIC_RACE_FEU_FOL)
                return ACTION_NOT_ALLOWED;
            
            $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 1);
            if ($error)
                return $error;
        } else {
            $place = array();
            if ($playerSrc->get("inbuilding")) {
                $place["x"] = $playerSrc->get("x");
                $place["y"] = $playerSrc->get("y");
            } else {
                $place = BasicActionFactory::getAleaFreePlace($playerSrc->get("x"), $playerSrc->get("y"), $playerSrc->get("map"), 1, $db);
                if ($place == 0)
                    return SPELL_FIRECALL_NO_PLACE;
            }
        }
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::fireCall($playerSrc, $place["x"], $place["y"], $target_id, $param, $db);
        }
        return $error;
    }

    static function checkingTeleport($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($x <= 0 or $y <= 0)
            return PLACE_FIELD_INVALID;
        
        $param["CASE_BUSY"] = 0;
        if (! self::caseFree($x, $y, $playerSrc->get("map"), $db))
            $param["CASE_BUSY"] = 1;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::teleport($playerSrc, $x, $y, $param, $db);
        }
        return $error;
    }

    static function checkingSoul($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($x <= 0 or $y <= 0)
            return PLACE_FIELD_INVALID;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        $param["XP"] = 0;
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::soul($playerSrc, $x, $y, $param, $db);
        }
        return $error;
    }

    static function checkingMassiveTP($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $dbct = new DBCollection("SELECT level,id_City FROM Building WHERE  id_BasicBuilding=14 and id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        if ($dbct->count() <= 0) {
            return ACTION_NOT_ALLOWED;
        }
        
        if ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TELEPORT_ROOM) {
            return ACTION_NOT_ALLOWED;
        }
        
        if ($x <= 0 or $y <= 0)
            return PLACE_FIELD_INVALID;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        $param["XP"] = 0;
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::massiveTP($playerSrc, $x, $y, $param, $db);
        }
        return $error;
    }

    static function checkingZone($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $x, $y) > 2)
            return ACTION_PLACE_TOO_FAR;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::spellzone($playerSrc, $ident, $x, $y, $param, $db);
        }
        return $error;
    }

    static function checkingReload($playerSrc, $ident, $id_Spring, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TOWER_ROOM))
            return ACTION_NOT_ALLOWED;
        
        if ($id_Spring <= 0)
            return BUILDING_FIELD_INVALID;
        
        $building = new Building();
        if ($building->load($id_Spring, $db) == - 1)
            return ERROR_BUILDING_LOADING;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::reload($playerSrc, $id_Spring, $param, $db);
        }
        return $error;
    }

    static function checkingSun($playerSrc, $ident, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TOWER_ROOM))
            return ACTION_NOT_ALLOWED;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::sun($playerSrc, $param, $db);
        }
        return $error;
    }

    static function checkingDemonpunch($playerSrc, $ident, $id_Opponent, $slot, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 1);
        if ($error)
            return $error;
        
        if ($slot <= 0)
            return ACTION_WRONG_ARM_ERROR;
        
        if ($slot != 10 && $opponent->get("status") == "NPC")
            return DEMONPUNCH_NPC_ERROR;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::demonpunch($playerSrc, $opponent, $slot, $param, $db);
        }
        return $error;
    }

    static function checkingWall($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TOWER_ROOM))
            return ACTION_NOT_ALLOWED;
        
        $case = array();
        
        $dist = array();
        for ($i = 1; $i < 5; $i ++) {
            $dist[$i] = 0;
            if ($x[$i] != - 1) {
                $case["x"][] = $x[$i];
                $case["y"][] = $y[$i];
            }
        }
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicBuilding=27", $db);
        if (($dbc->count() + count($case["x"])) > 10)
            return SPELL_WALL_TOO_MANY;
        
        for ($i = 0; $i < count($case["x"]); $i ++) {
            if (! self::caseFree($case["x"][$i], $case["y"][$i], $playerSrc->get("map"), $db))
                return SPELL_WALL_BUSY;
            
            if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $case["x"][$i], $case["y"][$i]) > 2)
                return ACTION_PLACE_TOO_FAR;
            
            for ($j = 1; $j < count($case["x"]); $j ++) {
                if (distHexa($case["x"][$i], $case["y"][$i], $case["x"][$j], $case["y"][$j]) == 0 && $i != $j)
                    return SPELL_WALL_SAME_PLACE;
            }
        }
        
        /*
         * //Test pour savoir si le mur est conforme
         * for($i=1; $i<5 ; $i++)
         * {
         * if(distHexa($playerSrc->get("x"), $playerSrc->get("y"), $case["x"][$i], $case["y"][$i]) > 2)
         * return ACTION_INCORRECT_POSITION;
         * }
         *
         * for($i=1; $i<5 ; $i++)
         * {
         *
         * }
         *
         * $count = 0;
         * for($i=1; $i<5 ; $i++)
         * {
         * if($dist[$i] == 0)
         * return SPELL_WALL_DISCONNECT;
         *
         * if($dist[$i] == 1)
         * $count++;
         *
         * }
         *
         * if($count > 2)
         * return ACTION_WALL_DISCONNECT;
         *
         */
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::wall($playerSrc, $case, $param, $db);
        }
        return $error;
    }

    static function checkingNegation($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        $error = InteractionChecking::checkingSpell($playerSrc, $ident, $id_Opponent, $paramability, $param, $db);
        if ($error)
            return $error;
        if ($playerSrc->get("inbuilding"))
            return ACTION_NOT_ALLOWED;
        return $error;
    }

    static function checkingPillars($playerSrc, $ident, $x, $y, &$paramability, &$param, &$db)
    {
        $error = self::checkingSpellAPAndTurtle($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             ($playerSrc->get("inbuilding") && $playerSrc->get("room") != TOWER_ROOM))
            return ACTION_NOT_ALLOWED;
        
        $x1 = $x[1];
        $x2 = $x[2];
        $x3 = $x[3];
        $y1 = $y[1];
        $y2 = $y[2];
        $y3 = $y[3];
        
        // Pas la même case
        if (($x1 == $x2 && $y1 == $y2) or ($x1 == $x3 && $y1 == $y3) or ($x3 == $x2 && $y3 == $y2))
            return PILLARS_INCORRECT_POSITION;
            
            // Alignés
            // if(( ($x1 != $x2) && ($y1 != $y2 or $y2 != $y3)) or (($y1 != $y2) && ($x1 != $x2 or $x2 != $x3)))
            // return PILLARS_INCORRECT_POSITION;
            
        // Adjacentes
        if (distHexa($x1, $y1, $x2, $y2) > 2 or distHexa($x1, $y1, $x3, $y3) > 2 or distHexa($x3, $y3, $x2, $y2) > 2)
            return PILLARS_INCORRECT_POSITION;
        
        $malus = 0;
        $error = SpellFactory::magicSpell($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"] && ! ($error)) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = SpellFactory::pillars($playerSrc, $ident, $x1, $y1, $x2, $y2, $x3, $y3, $param, $db);
        }
        return $error;
    }

    /* ************************************************************* LES TALENT D'EXTRACTION ******************************************************* */
    static function checkingExtraction($playerSrc, $ident, $id_Exploitation, &$paramability, &$param, &$db)
    {
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::checkingInvFull($playerSrc, $ident, $param, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($id_Exploitation <= 0)
            return EXPLOITATION_FIELD_INVALID;
        
        $res = new Exploitation();
        if ($res->load($id_Exploitation, $db) == - 1)
            return ERROR_EXPLOITATION_LOADING;
        
        $dx = $playerSrc->get("x") - $res->get("x");
        $dy = $playerSrc->get("y") - $res->get("y");
        if (((abs($dx) + abs($dy) + abs($dx + $dy)) / 2) > 1)
            return ACTION_EXPLOITATION_TOO_FAR;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $res = new Exploitation();
            $res->load($id_Exploitation, $db);
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::extraction($playerSrc, $ident, $res, $param, $db);
        }
        return $error;
    }

    static function checkingSpeak($playerSrc, $ident, $id_Opponent, &$paramability, &$param, &$db)
    {
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::checkingInvFull($playerSrc, $ident, $param, $db);
        if ($error)
            return $error;
        
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 1);
        if ($error)
            return $error;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::speak($playerSrc, $opponent, $param, $db);
        }
        return $error;
    }

    static function checkingCut($playerSrc, $ident, &$paramability, &$param, &$db)
    {
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $error = self::checkingInvFull($playerSrc, $ident, $param, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp") ||
             $playerSrc->get("inbuilding") > 0)
            return ACTION_NOT_ALLOWED;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::cut($playerSrc, $param, $db);
        }
        return $error;
    }

    /* ****************************************** LES TALENTS NATURE ************************ */
    static function checkingKrajeckCall($playerSrc, $ident, &$paramability, &$param, &$db)
    {
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $map = $playerSrc->get("map");
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        $dbe = new DBCollection("SELECT * FROM Gate WHERE nbNPC > 0 and map=" . $map . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 5", 
            $db);
        if ($dbe->count() == 0)
            return NO_GATE;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::KradjeckCall($playerSrc, $dbe, $param, $db);
        }
        return $error;
    }

    static function checkingTalentMonster($playerSrc, $ident, $id_Opponent, $charac1, $charac2, &$paramability, &$param, &$db)
    {
        $error = self::basicCheckingInteraction($playerSrc, $id_Opponent, $db);
        if ($error)
            return $error;
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $opponent->load($id_Opponent, $db);
        
        if ($charac1 == 0 || $charac2 == 0)
            return CHARAC_FIELD_INVALID;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $error = self::distanceCheckingInteraction($playerSrc, $opponent, $db, 5);
        if ($error)
            return $error;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::Monster($playerSrc, $opponent, $charac1, $charac2, $param, $db);
        }
        return $error;
    }

    static function checkingTalentDetection($playerSrc, $ident, $exploitationType, $niveau, &$paramability, &$param, &$db)
    {
        if ($exploitationType == 0)
            return EXPLOITATIONTYPE_FIELD_INVALID;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::Detection($playerSrc, $exploitationType, $niveau, $param, $db);
        }
        return $error;
    }

    static function checkingTalentCloseGate($playerSrc, $ident, $id_Gate, &$paramability, &$param, &$db)
    {
        if ($id_Gate == 0)
            return CHOICE_FIELD_INVALID;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        $map = $playerSrc->get("map");
        
        $dbnt = new DBCollection(
            "SELECT id,x,y, id_City, ((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2) AS dist FROM Building WHERE map=" . $playerSrc->get("map") .
                 " AND name='Temple' ORDER BY dist", $db);
        if ($dbnt->get("dist") > 15)
            return TALENT_CLOSEGATE_TOO_FAR;
        
        $gate = new Gate();
        if ($gate->load($id_Gate, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::CloseGate($playerSrc, $gate, $param, $db);
        }
        return $error;
    }

    /* ************************************************************* LES TALENT DE RAFFINAGE ******************************************************* */
    static function checkingRefine($playerSrc, $ident, $id_Object1, $id_Object2, &$paramability, &$param, &$db)
    {
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        if ($id_Object1 == - 1 or $id_Object2 == - 1)
            return OBJECT_FIELD_INVALID;
        
        $object1 = new Equipment();
        if ($object1->load($id_Object1, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        $object2 = new Equipment();
        if ($object2->load($id_Object2, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($object1->get("id") == $object2->get("id"))
            return ERROR_SAME_OBJECT;
        if ($object1->get("id_Player") != $playerSrc->get("id") or $object2->get("id_Player") != $playerSrc->get("id"))
            return NOT_YOUR_OBJECT_ERROR;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        if ($ident == TALENT_PLANTREFINE || $ident == TALENT_GEMREFINE)
            if ($object1->get("id_BasicEquipment") != $object2->get("id_BasicEquipment"))
                return ERROR_NOT_SAME_TYPE_OBJECT;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::refine($playerSrc, $ident, $object1, $object2, $param, $db);
        }
        return $error;
    }

    /* ************************************************************* LES TALENT DE CONFECTION ******************************************************* */
    static function checkingCraft($playerSrc, $ident, $id_BasicEquipment, $id_Object, $niveau, &$paramability, &$param, &$db)
    {
        $param["XP"] = 0;
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $equip = new Equipment();
        
        $param["NEW"] = 1;
        if ($id_BasicEquipment == 0) // choix d'un equipement déjà commencé
{
            
            if ($equip->load($id_Object, $db) == - 1)
                return ERROR_OBJECT_LOADING;
            
            $param["NEW"] = 0;
            if ($equip->get("progress") == 100)
                return TERMINATED_OBJECT_ERROR;
        }
        
        // Outils
        if ($id_BasicEquipment >= 207 and $id_BasicEquipment <= 212) {
            if ($id_Object != 0)
                return TALENT_RAWMATERIAL_IGNORED;
            
            if (! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db) && ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 6, $db))
                return INV_FULL_ERROR;
        } else {
            
            if ($equip->load($id_Object, $db) == - 1)
                return ERROR_OBJECT_LOADING;
                
                // Si on continue pas un outils
            if ($equip->get("id_BasicEquipment") < 207 or $equip->get("id_BasicEquipment") > 212) {
                
                if ($param["NEW"] == 1 && ($equip->get("id_EquipmentType") < 30 || $equip->get("id_EquipmentType") > 32))
                    return ERROR_WRONG_RAWMATERIAL;
                
                if ($niveau > $equip->get("level") && $param["NEW"] == 1)
                    return ERROR_LEVEL_TOO_HIGH;
                
                if ($equip->get("id_Player") != $playerSrc->get("id"))
                    return NOT_YOUR_OBJECT_ERROR;
                    
                    // Si on confectionne une flèche
                if ($id_BasicEquipment == 55 or $id_BasicEquipment == 56) {
                    if ($equip->get("id_Equipment\$bag") != 0 && $equip->get("id_Equipment\$bag") != 5 && ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db) &&
                         ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 5, $db))
                        return INV_FULL_ERROR;
                }  // Si on confectionne une potion
else {
                    if ($id_BasicEquipment >= 400 && $id_BasicEquipment <= 410) {
                        if ($equip->get("id_Equipment\$bag") != 0 && $equip->get("id_Equipment\$bag") != 4 && ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db) &&
                             ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 4, $db))
                            return INV_FULL_ERROR;
                    } elseif ($equip->get("id_Equipment\$bag") != 0 && ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db))
                        return INV_FULL_ERROR;
                }
                
                if ($ident == TALENT_PLANTCRAFT) {
                    $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $id_BasicEquipment, $db);
                    $dbd = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $equip->get("id_BasicEquipment"), $db);
                    if ($dbc->get("id_BasicMaterial") != $dbd->get("id_BasicMaterial"))
                        return ERROR_WRONG_RAWMATERIAL;
                }
            }
        }
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::craft($playerSrc, $ident, $id_BasicEquipment, $equip, $niveau, $param, $db);
        }
        return $error;
    }

    static function checkingGemCraft($playerSrc, $ident, $id_Equip, $id_Gem, $id_Wood, $id_Template, $niveau, &$paramability, &$param, &$db)
    {
        $param["XP"] = 0;
        $error = self::checkingTalentAP($playerSrc, $ident, $db);
        if ($error)
            return $error;
        
        $gem = new Equipment();
        $wood = new Equipment();
        $equip = new Equipment();
        
        if ($id_Equip == 220) // création de l'outils sans matières premières
{
            $param["CRAFT_TYPE"] = 1;
            $param["EQUIP_ID"] = $id_Equip;
            if ($id_Template != 0)
                return TALENT_GEMCRAFT_TEMPLATE_IGNORED;
            
            if ($id_Wood != 0)
                return TALENT_RAWMATERIAL_IGNORED;
            
            if ($id_Gem != 0)
                return TALENT_RAWMATERIAL_IGNORED;
            
            if (! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db))
                return INV_FULL_ERROR;
        }        

        // Création d'un baton ou un sceptre
        elseif ($id_Equip == 11 or $id_Equip == 12 or $id_Equip == 204 or $id_Equip == 57 or $id_Equip == 58) {
            $param["CRAFT_TYPE"] = 1;
            $param["EQUIP_ID"] = $id_Equip;
            if ($id_Template != 0)
                return TALENT_GEMCRAFT_TEMPLATE_IGNORED;
            
            if ($id_Wood == - 1 or $id_Gem == - 1)
                return RAWMATERIAL_FIELD_INVALID;
            
            if ($wood->load($id_Wood, $db) == - 1)
                return ERROR_OBJECT_LOADING;
            
            if ($wood->get("id_Player") != $playerSrc->get("id"))
                return NOT_YOUR_OBJECT_ERROR;
            
            if ($gem->load($id_Gem, $db) == - 1)
                return RAWMATERIAL_FIELD_INVALID;
            
            if ($gem->get("id_Player") != $playerSrc->get("id"))
                return NOT_YOUR_OBJECT_ERROR;
            
            if ($gem->get("id_BasicEquipment") != 100)
                return ERROR_WRONG_RAWMATERIAL;
            
            if ($niveau > min($wood->get("level"), $gem->get("level")))
                return ERROR_LEVEL_TOO_HIGH;
            
            if ($gem->get("id_Equipment\$bag") != 0 && $wood->get("id_Equipment\$bag") != 0 && ! PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db))
                return INV_FULL_ERROR;
        } 

        else {
            
            if ($equip->load($id_Equip, $db) == - 1)
                return ERROR_OBJECT_LOADING;
            
            if ($equip->get("id_Player") != $playerSrc->get("id"))
                return NOT_YOUR_OBJECT_ERROR;
                
                // Equipement déjà commencé
            if ($equip->get("progress") < 100 or ($equip->get("templateProgress") > 0 and $equip->get("templateProgress") < 100)) {
                $param["CRAFT_TYPE"] = 2;
                // if($id_Template != 0)
                // return TALENT_GEMCRAFT_TEMPLATE_IGNORED;
                
                if ($id_Gem != 0 or $id_Wood != 0)
                    return TALENT_RAWMATERIAL_IGNORED;
                
                if ($equip->get("id_Player") != $playerSrc->get("id"))
                    return NOT_YOUR_OBJECT_ERROR;
            } else {
                
                if ($gem->load($id_Gem, $db) == - 1)
                    return ERROR_OBJECT_LOADING;
                
                if ($id_Wood != 0)
                    return TALENT_RAWMATERIAL_IGNORED;
                    // Création d'un template mineur
                if ($equip->get("extraname") == "") {
                    if ($id_Template == 0)
                        return TALENT_GEMCRAFT_TEMPLATE_NO_BE_IGNORED;
                    $param["CRAFT_TYPE"] = 3;
                    if ($gem->get("id_BasicEquipment") != 100)
                        return ERROR_WRONG_RAWMATERIAL;
                    
                    if ($niveau > $gem->get("level") or $niveau > $equip->get("level"))
                        return ERROR_LEVEL_TOO_HIGH;
                } else {
                    $param["CRAFT_TYPE"] = 4;
                    if ($id_Template == 0)
                        return TALENT_GEMCRAFT_TEMPLATE_NO_BE_IGNORED;
                    
                    if ($gem->get("id_BasicEquipment") != 101)
                        return ERROR_WRONG_RAWMATERIAL;
                    
                    if ($niveau > $gem->get("level") or $niveau > $equip->get("level"))
                        return ERROR_LEVEL_TOO_HIGH;
                }
            }
        }
        
        $state = $playerSrc->get("state");
        if ($playerSrc->get("disabled") != 0 || $state == "questionning" || $state == "prison" || $playerSrc->get("recall") || $playerSrc->get("levelUp"))
            return ACTION_NOT_ALLOWED;
        
        $malus = 0;
        $error = TalentFactory::talent($playerSrc, $ident, $malus, $paramability, $param, $db);
        if ($error)
            return $error;
        
        $param["AP"] = $paramability["AP"];
        $playerSrc->set("ap", $playerSrc->get("ap") - $paramability["AP"]);
        if ($paramability["SUCCESS"]) {
            $param["XP"] = $playerSrc->get("modeAnimation") == 0 ? 1 : 0;
            $error = TalentFactory::enchant($playerSrc, $equip, $gem, $wood, $id_Template, $niveau, $param, $db);
        }
        return $error;
    }
}

?>
