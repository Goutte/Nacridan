<?php
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");
require_once (HOMEPATH . "/class/BM.inc.php");
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");
require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/lib/Map.inc.php");

class BasicActionFactory
{

    /* *************************************************************** FONCTIONS AUXILLIAIRES **************************************** */
    static public function globalInfo($playerSrc, &$param)
    {
        $param["PLAYER_NAME"] = $playerSrc->get("name");
        if ($param["PLAYER_NAME"] == "")
            $param["PLAYER_NAME"] = $playerSrc->get("racename");
        $param["PLAYER_RACE"] = $playerSrc->get("id_BasicRace");
        $param["PLAYER_ID"] = $playerSrc->get("id");
        $param["PLAYER_GENDER"] = $playerSrc->get("gender");
        
        $param["PLAYER_LEVEL"] = $playerSrc->get("level");
        $param["MEMBER_ID"] = $playerSrc->get("id_Member");
        
        if ($playerSrc->get("status") == "NPC")
            $param["PLAYER_IS_NPC"] = 1;
        else
            $param["PLAYER_IS_NPC"] = 0;
    }

    static public function globalInfoOpponent($playerSrc, &$param)
    {
        $param["TARGET_NAME"] = $playerSrc->get("name");
        if ($param["TARGET_NAME"] == "")
            $param["TARGET_NAME"] = $playerSrc->get("racename");
        $param["TARGET_RACE"] = $playerSrc->get("id_BasicRace");
        $param["TARGET_ID"] = $playerSrc->get("id");
        $param["TARGET_GENDER"] = $playerSrc->get("gender");
        
        $param["TARGET_LEVEL"] = $playerSrc->get("level");
        $param["TARGET_ID"] = $playerSrc->get("id");
        $param["TARGET_MEMBER"] = $playerSrc->get("id_Member");
        
        $param["TARGET_X"] = $playerSrc->get("x");
        $param["TARGET_Y"] = $playerSrc->get("y");
        $param["TARGET_XP"] = $playerSrc->get("xp");
        $param["TARGET_KILLED"] = 0;
        
        if ($playerSrc->get("status") == "NPC")
            $param["TARGET_IS_NPC"] = 1;
        else
            $param["TARGET_IS_NPC"] = 0;
    }

    static protected function modifierScoreToArray($eqmodifier, &$param)
    {
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getScoreWithNoBM($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static protected function modifierToArray($eqmodifier, &$param)
    {
        $eqmodifier->initCharacStr();
        foreach ($eqmodifier->m_characLabel as $key) {
            if (($str = $eqmodifier->getModifStr($key)) != 0) {
                $param[$key] = $str;
            }
        }
    }

    static function freePlace($x, $y, $map, $db, $object = 0, $building = 0)
    {
        $result = self::freePlaceAllInfos($x, $y, $map, $db, $object, $building);
        return $result["place"];
    }

    static function freePlaceAllInfos($x, $y, $map, $db, $object = 0, $building = 0)
    {
        $place = 1;
        $type = "";
        $id = - 1;
        if ($object == 1) {
            $dbp = new DBCollection("SELECT id FROM Equipment WHERE x=" . $x . " AND y=" . $y . " AND state='Ground' AND map=" . $map, $db);
            if (! $dbp->eof()) {
                $place = 0;
                $type = "Equipment";
                $id = $dbp->get("id");
            }
        }
        
        $dbp = new DBCollection("SELECT id FROM Player WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbp->eof()) {
            $place = 0;
            $type = "Player";
            $id = $dbp->get("id");
        }
        $dbp = new DBCollection("SELECT id FROM Gate WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbp->eof()) {
            $place = 0;
            $type = "Gate";
            $id = $dbp->get("id");
        }
        
        $dbe = new DBCollection("SELECT id FROM Exploitation WHERE x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
        if (! $dbe->eof()) {
            $place = 0;
            $type = "Exploitation";
            $id = $dbe->get("id");
        }
        
        if ($building == 0) {
            $dbb = new DBCollection("SELECT id FROM Building WHERE id_BasicBuilding !=26 AND x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
            if (! $dbb->eof()) {
                $place = 0;
                $type = "Building";
                $id = $dbb->get("id");
            }
        } else {
            $dbb = new DBCollection("SELECT id FROM Building WHERE progress=sp AND id_BasicBuilding !=26 AND x=" . $x . " AND y=" . $y . " AND map=" . $map, $db);
            if (! $dbb->eof()) {
                $place = 0;
                $type = "Building";
                $id = $dbb->get("id");
            }
        }
        $mapinfo = new MapInfo($db);
        $validzone = $mapinfo->getValidMap($x, $y, 0, $map);
        /*
         * if(empty($validzone)) // cas ou un anim laisse stupidement un perso en dehors de la map
         * $place = 0;
         * else
         */
        if (! $validzone[0][0])
            $place = 0;
        $result = array();
        $result["place"] = $place;
        $result["type"] = $type;
        $result["id"] = $id;
        return $result;
    }

    static function getAleaFreePlace($x, $y, $map, $gap, $db, $object = 0)
    {
        $place = array();
        $k = 0;
        for ($i = - $gap; $i < $gap + 1; $i ++) {
            for ($j = - $gap; $j < $gap + 1; $j ++) {
                if (! ($i == 1 && $j == 1) && ! ($i == - 1 && $j == - 1)) {
                    if (self::freePlace($x + $i, $y + $j, $map, $db, $object)) {
                        $place[$k]["x"] = $x + $i;
                        $place[$k]["y"] = $y + $j;
                        $k ++;
                    }
                }
            }
        }
        if ($k == 0)
            return 0;
        
        $choice = mt_rand(0, $k - 1);
        return $place[$choice];
    }

    static function getAleaFreeCaseInZone($zone, $map, $db)
    {
        $place = array();
        $go = 1;
        while ($go) {
            // Montagne
            if ($zone == 3) {
                if (mt_rand(1, 7) < 2) {
                    $place["x"] = mt_rand(48, 155);
                    $place["y"] = mt_rand(74, 206);
                } else {
                    $place["x"] = mt_rand(250, 618);
                    $place["y"] = mt_rand(1, 304);
                }
                
                if (self::freePlace($place["x"], $place["y"], $map, $db, 0) && getLandType($place["x"], $place["y"], $map, $db) === "mountain")
                    return $place;
            }
            // Forêt
            if ($zone == 4) {
                $nb = mt_rand(1, 7);
                if ($nb == 1) {
                    $place["x"] = mt_rand(41, 162);
                    $place["y"] = mt_rand(205, 248);
                }
                if ($nb == 2) {
                    $place["x"] = mt_rand(91, 156);
                    $place["y"] = mt_rand(46, 99);
                }
                if ($nb == 3) {
                    $place["x"] = mt_rand(171, 260);
                    $place["y"] = mt_rand(11, 67);
                }
                if ($nb == 4) {
                    $place["x"] = mt_rand(342, 436);
                    $place["y"] = mt_rand(342, 388);
                }
                if ($nb >= 5) {
                    $place["x"] = mt_rand(205, 382);
                    $place["y"] = mt_rand(128, 249);
                }
                
                if (self::freePlace($place["x"], $place["y"], $map, $db, 0) && getLandType($place["x"], $place["y"], $map, $db) == "forest")
                    return $place;
            }
            // Plaine
            if ($zone == 5) {
                
                $place["x"] = mt_rand(14, 635);
                $place["y"] = mt_rand(1, 395);
                
                if (self::freePlace($place["x"], $place["y"], $map, $db, 0) && getLandType($place["x"], $place["y"], $map, $db) == "plain")
                    return $place;
            }
        }
    }
    
    // TAG#99 (pour retrouver cet endroit via un grep à partir du tag dans les règles)
    static function getDamageArchery($attack, $defense, $ident = 0)
    {
        $delta = $attack - $defense;
        
        $damage = 0.45 * (min($delta, $attack / 3));
        
        if ($delta > $attack / 3)
            $damage += 0.35 * min($delta - $attack / 3, $attack / 3);
        
        if ($delta > 2 * $attack / 3)
            $damage += 0.25 * ($delta - 2 * $attack / 3);
        
        if ($ident == ABILITY_FLYARROW) {
            $damage = $damage * 4 / 5;
        }
        $damage = floor($damage);
        return $damage;
    }

    static function canEnterTemple($playerSrc, $idBuilding, $db)
    {
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $idBuilding, $db);
        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $dbb->get("id_City"), $db);
        
        if ($dbb->eof())
            return 0;
        else {
            if ($dbc->get("captured") < 4)
                return 1;
            else {
                $dbs = new DBCollection("SELECT * FROM Player WHERE inbuilding=" . $idBuilding . " AND id_City=" . $dbc->get("id") . " AND racename='Prêtre'", $db);
                if ($dbs->eof())
                    return 0;
                    
                    // Si c'est le protecteur ou si je suis dans la ville
                if (($playerSrc->get("id") == $dbc->get("id_Player")))
                    return 1;
                    
                    // Si ouvert à tout le monde
                if ($dbc->get("accessTemple") == 1)
                    return 1;
                    
                    // Si fermé à tout le monde
                if ($dbc->get("accessTemple") == 2)
                    return 0;
                    
                    // Si ouvert aux alliés uniquement
                if (($dbc->get("accessTemple") % 3) == 0) {
                    
                    $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                    // Même Team
                    if ($dbc->get("accessTemple") % 10 == 3 && $dbp->get("id_Team") != 0) {
                        $dba = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                        if ($dba->get("id_Team") == $playerSrc->get("id_Team"))
                            return 1;
                    }
                    
                    // Alliances Personnelles PJ
                    if (($dbc->get("accessTemple") / 10) % 10 == 3) {
                        $dba = new DBCollection(
                            "SELECT * FROM PJAlliancePJ WHERE type='Allié' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Player\$dest=" . $playerSrc->get("id"), $db);
                        if (! $dba->eof())
                            return 1;
                    }
                    
                    // Alliances Personnelles Ordre
                    if (($dbc->get("accessTemple") / 100) % 10 == 3) {
                        $dba = new DBCollection(
                            "SELECT * FROM PJAlliance WHERE type='Allié' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Team=" . $playerSrc->get("id_Team"), $db);
                        if (! $dba->eof())
                            return 1;
                    }
                    
                    // Alliances Ordre PJ
                    if (($dbc->get("accessTemple") / 1000) % 10 == 3) {
                        $dba = new DBCollection(
                            "SELECT * FROM TeamAlliancePJ WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerSrc->get("id"), $db);
                        if (! $dba->eof())
                            return 1;
                    }
                    
                    // Alliances Ordre PJ
                    if (($dbc->get("accessTemple") / 10000) % 10 == 3) {
                        $dba = new DBCollection(
                            "SELECT * FROM TeamAlliance WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerSrc->get("id_Team"), $db);
                        if (! $dba->eof())
                            return 1;
                    }
                    return 0;
                }
                // Si fermé à mes enemis
                if ($dbc->get("accessTemple") == 4) {
                    $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                    
                    // Alliances Personnelles PJ
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliancePJ WHERE type='Ennemi' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Player\$dest=" . $playerSrc->get("id"), $db);
                    if (! $dba->eof())
                        return 0;
                        // Alliances Personnelles PJ
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliance WHERE type='Ennemi' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Team=" . $playerSrc->get("id_Team"), $db);
                    if (! $dba->eof())
                        return 0;
                        // Alliances Ordre PJ
                    $dba = new DBCollection("SELECT * FROM TeamAlliancePJ WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerSrc->get("id"), 
                        $db);
                    if (! $dba->eof())
                        return 0;
                        // Alliances Ordre PJ
                    $dba = new DBCollection(
                        "SELECT * FROM TeamAlliance WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerSrc->get("id_Team"), $db);
                    if (! $dba->eof())
                        return 0;
                    
                    return 1;
                }
            }
        }
    }

    static function canEnterCity($playerSrc, $idCity, $db)
    {
        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $idCity, $db);
        
        if ($dbc->eof())
            return 0;
        
        $playerToEnter = $playerSrc;
        if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) { // si le perso est un feu fol ses droits sont vérifiés avec le propriétaire du feu fol
            $id = $playerSrc->get("id_Owner");
            $playerToEnter = new Player();
            $playerToEnter->externDBObj("Modifier");
            $playerToEnter->load($id, $db);
        } else {
            if ($playerSrc->get("status") == "NPC" && $playerSrc->get("id_Mission") > 0) { // si le perso est un feu fol ses droits sont vérifiés avec le propriétaire du feu fol
                $dbmi = new DBCollection(
                    "SELECT Player.id FROM Equipment inner join Player on Player.id = Equipment.id_Player WHERE Equipment.id_Mission=" . $playerSrc->get("id_Mission") .
                         " and Player.id_Member =" . $playerSrc->get("id_Member"), $db);
                $id = $dbmi->get("id");
                $playerToEnter = new Player();
                $playerToEnter->externDBObj("Modifier");
                $playerToEnter->load($id, $db);
            } else {
                $playerToEnter = $playerSrc;
            }
        }
        
        if ($dbc->get("captured") < 4)
            return 1;
        else {
            // Si c'est le protecteur ou si je suis dans la ville
            if (($playerToEnter->get("id") == $dbc->get("id_Player")))
                return 1;
                
                // Si ouvert à tout le monde
            if ($dbc->get("door") == 1)
                return 1;
                
                // Si fermé à tout le monde et hors de la ville
            if ($dbc->get("door") == 2)
                return 0;
                
                // Si ouvert aux alliés uniquement et hors de la ville
            if (($dbc->get("door") % 3) == 0) {
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                // Même Team
                if ($dbc->get("door") % 10 == 3 && $dbp->get("id_Team") != 0) {
                    if ($dbp->get("id_Team") == $playerToEnter->get("id_Team"))
                        return 1;
                }
                
                // Alliances Personnelles PJ
                if (($dbc->get("door") / 10) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliancePJ WHERE type='Allié' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Player\$dest=" . $playerToEnter->get("id"), $db);
                    if (! $dba->eof())
                        return 1;
                }
                
                // Alliances Personnelles Ordre
                if (($dbc->get("door") / 100) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliance WHERE type='Allié' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Team=" . $playerToEnter->get("id_Team"), $db);
                    if (! $dba->eof())
                        return 1;
                }
                
                // Alliances Ordre PJ
                if (($dbc->get("door") / 1000) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM TeamAlliancePJ WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerToEnter->get("id"), $db);
                    if (! $dba->eof())
                        return 1;
                }
                
                // Alliances Ordre PJ
                if (($dbc->get("door") / 10000) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM TeamAlliance WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerToEnter->get("id_Team"), $db);
                    if (! $dba->eof())
                        return 1;
                }
                return 0;
            }
            // Si fermé à mes ennemis
            if ($dbc->get("door") == 4) {
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                
                // Alliances Personnelles PJ
                $dba = new DBCollection(
                    "SELECT * FROM PJAlliancePJ WHERE type='Ennemi' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Player\$dest=" . $playerToEnter->get("id"), $db);
                if (! $dba->eof())
                    return 0;
                    // Alliances Personnelles PJ
                $dba = new DBCollection(
                    "SELECT * FROM PJAlliance WHERE type='Ennemi' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Team=" . $playerToEnter->get("id_Team"), $db);
                if (! $dba->eof())
                    return 0;
                    // Alliances Ordre PJ
                $dba = new DBCollection("SELECT * FROM TeamAlliancePJ WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerToEnter->get("id"), 
                    $db);
                if (! $dba->eof())
                    return 0;
                    // Alliances Ordre PJ
                $dba = new DBCollection(
                    "SELECT * FROM TeamAlliance WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerToEnter->get("id_Team"), $db);
                if (! $dba->eof())
                    return 0;
                
                return 1;
            }
            return 1;
        }
    }

    static function canEnter($playerSrc, $idBuilding, $db)
    {
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $idBuilding, $db);
        
        if ($dbb->eof())
            return 0;
        
        $dbc = new DBCollection("SELECT * FROM City WHERE id=" . $dbb->get("id_City"), $db);
        
        if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbb->get("x"), $dbb->get("y")) > 1)
            return 0;
        else {
            $playerToEnter = $playerSrc;
            if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) { // si le perso est un feu fol ses droits sont vérifiés avec le propriétaire du feu fol
                $id = $playerSrc->get("id_Owner");
                $playerToEnter = new Player();
                $playerToEnter->externDBObj("Modifier");
                $playerToEnter->load($id, $db);
            } else {
                if ($playerSrc->get("status") == "NPC" && $playerSrc->get("id_Mission") > 0) { // si le perso est un feu fol ses droits sont vérifiés avec le propriétaire du feu fol
                    $dbmi = new DBCollection(
                        "SELECT Player.id FROM Equipment inner join Player on Player.id = Equipment.id_Player WHERE Equipment.id_Mission=" . $playerSrc->get("id_Mission") .
                             " and Player.id_Member =" . $playerSrc->get("id_Member"), $db);
                    $id = $dbmi->get("id");
                    $playerToEnter = new Player();
                    $playerToEnter->externDBObj("Modifier");
                    $playerToEnter->load($id, $db);
                } else {
                    $playerToEnter = $playerSrc;
                }
            }
            if ($dbb->get("progress") != $dbb->get("sp"))
                return 0;
            
            switch ($dbb->get("id_BasicBuilding")) {
                case 1:
                case 2:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 14:
                    return 1;
                    break;
                // Cas des remparts et des portes fermées
                case 21:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 28:
                    if ($dbb->get("id_BasicBuilding") == 28)
                        $type = "accessEntrepot";
                    else
                        $type = "door";
                    if ($dbc->get("captured") < 4)
                        return 1;
                    else {
                        // Si c'est le protecteur ou si je suis dans la ville
                        if (($playerToEnter->get("id") == $dbc->get("id_Player")) || ($playerSrc->isInCity($db) && $type == "door"))
                            return 1;
                            
                            // Si ouvert à tout le monde
                        if ($dbc->get($type) == 1)
                            return 1;
                            
                            // Si fermé à tout le monde et hors de la ville
                        if ($dbc->get($type) == 2 && (! $playerSrc->isInCity($db) || $type != "door"))
                            return 0;
                            
                            // Si ouvert aux alliés uniquement et hors de la ville
                        if (($dbc->get($type) % 3) == 0 && (! $playerSrc->isInCity($db) || $type != "door")) {
                            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                            // Même Team
                            if ($dbc->get($type) % 10 == 3 && $dbp->get("id_Team") != 0) {
                                if ($dbp->get("id_Team") == $playerToEnter->get("id_Team"))
                                    return 1;
                            }
                            
                            // Alliances Personnelles PJ
                            if (($dbc->get($type) / 10) % 10 == 3) {
                                $dba = new DBCollection(
                                    "SELECT * FROM PJAlliancePJ WHERE type='Allié' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Player\$dest=" .
                                         $playerToEnter->get("id"), $db);
                                if (! $dba->eof())
                                    return 1;
                            }
                            
                            // Alliances Personnelles Ordre
                            if (($dbc->get($type) / 100) % 10 == 3) {
                                $dba = new DBCollection(
                                    "SELECT * FROM PJAlliance WHERE type='Allié' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Team=" . $playerToEnter->get("id_Team"), 
                                    $db);
                                if (! $dba->eof())
                                    return 1;
                            }
                            
                            // Alliances Ordre PJ
                            if (($dbc->get($type) / 1000) % 10 == 3) {
                                $dba = new DBCollection(
                                    "SELECT * FROM TeamAlliancePJ WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerToEnter->get("id"), $db);
                                if (! $dba->eof())
                                    return 1;
                            }
                            
                            // Alliances Ordre PJ
                            if (($dbc->get($type) / 10000) % 10 == 3) {
                                $dba = new DBCollection(
                                    "SELECT * FROM TeamAlliance WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" .
                                         $playerToEnter->get("id_Team"), $db);
                                if (! $dba->eof())
                                    return 1;
                            }
                            return 0;
                        }
                        // Si fermé à mes ennemis
                        if ($dbc->get($type) == 4 && (! $playerSrc->isInCity($db) || $type != "door")) {
                            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player"), $db);
                            
                            // Alliances Personnelles PJ
                            $dba = new DBCollection(
                                "SELECT * FROM PJAlliancePJ WHERE type='Ennemi' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Player\$dest=" . $playerToEnter->get("id"), 
                                $db);
                            if (! $dba->eof())
                                return 0;
                                // Alliances Personnelles PJ
                            $dba = new DBCollection(
                                "SELECT * FROM PJAlliance WHERE type='Ennemi' AND id_Player\$src =" . $dbc->get("id_Player") . " AND id_Team=" . $playerToEnter->get("id_Team"), $db);
                            if (! $dba->eof())
                                return 0;
                                // Alliances Ordre PJ
                            $dba = new DBCollection(
                                "SELECT * FROM TeamAlliancePJ WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerToEnter->get("id"), $db);
                            if (! $dba->eof())
                                return 0;
                                // Alliances Ordre PJ
                            $dba = new DBCollection(
                                "SELECT * FROM TeamAlliance WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerToEnter->get("id_Team"), 
                                $db);
                            if (! $dba->eof())
                                return 0;
                            
                            return 1;
                        }
                        return 1;
                    }
                    break;
                
                default:
                    return 0;
            }
        }
    }

    static function warnOwner($opponent, $db, $nb)
    {
        $owner = new Player();
        $owner->load($opponent->get("id_Owner"), $db);
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = array();
        $dest[$owner->get("name")] = $owner->get("name");
        if ($nb == 263)
            $body = "Votre tortue a été attaqué.";
        if ($nb == ID_BASIC_RACE_FEU_FOL)
            $body = "Votre Feu Fol a été attaqué.";
        MailFactory::sendMsg($opponent, localize("Attention !"), $body, $dest, 2, $err2, $db, - 1, 0);
    }

    static function legalAction($player, $opponent, &$param, $db, $killed)
    {
        if ($opponent->get("id_BasicRace") == 263 && ! $killed)
            self::warnOwner($opponent, $db, 263);
        
        if ($opponent->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && ! $killed)
            self::warnOwner($opponent, $db, ID_BASIC_RACE_FEU_FOL);
        
        $mapObj = new Map($db, $player->get("map"));
        if ($mapObj->getStartKingdomIdFromMap($player->get("x"), $player->get("y")) != ID_UNKNOWN_KINGDOM) {
            if ($player->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && ($opponent->get("status") == "PC" or $opponent->get("id_BasicRace") == 263)) {
                $owner = new Player();
                $owner->load($player->get("id_Owner"), $db);
                if (! $killed) {
                    self::warning($owner, $opponent, $param, $db); // feu fol controlé
                } else
                    self::questionning($owner, $opponent, $param, $db, "kill");
            } 

            elseif ($param["TYPE_ACTION"] == ABILITY_STEAL && $opponent->get("status") == "PC")
                self::questionning($player, $opponent, $param, $db, "steal");
            elseif (isset($param["TYPE_ATTACK"]) && $param["TYPE_ATTACK"] == ABILITY_DISARM_EVENT_SUCCESS && $opponent->get("status") == "PC")
                self::questionning($player, $opponent, $param, $db, "disarm");
            elseif ($param["TYPE_ACTION"] == ABILITY_DISARM && $opponent->get("status") == "PC")
                self::warning($player, $opponent, $param, $db);
            
            elseif (! $killed) {
                
                if ($opponent->get("id_BasicRace") == 263 or $opponent->get("status") == "PC")
                    self::warning($player, $opponent, $param, $db);
                elseif ($opponent->get("id_BasicRace") == 5 && ! $player->isWanted($db))
                    self::questionning($player, $opponent, $param, $db, "hit");
                elseif ($opponent->get("id_BasicRace") == 5 && $player->isWanted($db))
                    self::addSentence($player, $opponent, $param, $db, 2, "hit");
            } else {
                if (($opponent->get("id_BasicRace") == 263 or $opponent->get("status") == "PC") && ! $player->isWanted($db))
                    self::questionning($player, $opponent, $param, $db, "kill");
                elseif (($opponent->get("id_BasicRace") == 263 or $opponent->get("status") == "PC") && $player->isWanted($db))
                    self::addSentence($player, $opponent, $param, $db, 2, "kill");
                elseif ($opponent->get("id_BasicRace") == 5 && ! $player->isWanted($db))
                    self::chase($player, $opponent, $param, $db);
                elseif ($opponent->get("id_BasicRace") == 5 && $player->isWanted($db))
                    self::reinforcement($player, $opponent, $param, $db, 4);
            }
        }
    }

    static function warning($player, $opponent, &$param, $db)
    {
        if ($player->get("prison") == 0 && $opponent->get("prison") == 0) // 1 seul avertissement au début or opponent is a bad boy, no sentence
{
            $mapObj = new Map($db, $player->get("map"));
            $idStartKingdom = $mapObj->getStartKingdomIdFromMap($player->get("x"), $player->get("y"));
            
            if ($idStartKingdom == ID_ARTASSE)
                $id = 1000;
            elseif ($idStartKingdom == ID_EAROK)
                $id = 2000;
            else
                $id = 3000;
            $garde = new Player();
            $garde->load($id, $db);
            require_once (HOMEPATH . "/factory/MailFactory.inc.php");
            $dest = array();
            $dest[$player->get("name")] = $player->get("name");
            $body = "Rappel\n Vous vous trouvez dans un royaume où le meurtre et le vol sont interdits. Le désarmement de votre adversaire dans le but de voler l'arme est également interdit, les gardes sont particulièrement rigides sur ce point. Les tortues géantes sont également protégées dans cet espace.\n Pour cette action vous avez reçu 12 heures de prison avec sursis.";
            MailFactory::sendMsg($garde, localize("Attention !"), $body, $dest, 2, $err2, $db, - 1, 0);
            $param["MAIL"] = 1;
        }
        if ($player->get("prison") < 1) {
            $player->set("prison", 1);
            $player->updateDB($db);
        }
    }

    static function questionning($player, $opponent, &$param, $db, $action = "kill")
    {
        // opponent is a bad boy, no sentence
        if ($opponent->get("prison") == 0) {
            $ret = self::moveGuard($player, $opponent, $param, $db);
            if ($ret) {
                $dbc = new DBCollection("SELECT * FROM Player WHERE prison=" . $player->get("id"), $db);
                $garde = new Player();
                $garde->load($dbc->get("id"), $db);
                // log de l'action ramasser les pièces d'or
                
                if ($opponent->get("status") == "PC" && $action == "kill" && floor($opponent->get("money") * 0.9) > 0) {
                    // $garde->set("moneyBank", floor($opponent->get("money")*0.9));
                    BasicActionFactory::globalInfo($garde, $param2);
                    $param2["TYPE_ACTION"] = PICK_UP_OBJECT;
                    $param2["TYPE_ATTACK"] = PICK_UP_EVENT;
                    $param2["DELAI"] = 2;
                    $param2["ERROR"] = 0;
                    Event::logAction($db, $param2["TYPE_ACTION"], $param2);
                    
                    BasicActionFactory::globalInfoOpponent($opponent, $param2);
                    $param2["TYPE_ACTION"] = BANK_TRANSFERT_PLAYER;
                    $param2["TYPE_ATTACK"] = BANK_TRANSFERT_PLAYER_EVENT;
                    $param2["MONEY_TRANSFERED"] = floor($opponent->get("money") * 0.9);
                    $param2["FINAL_COST"] = floor($opponent->get("money") * 0.9);
                    $param2["TYPE"] = 1;
                    $param2["DELAI"] = 3;
                    $param2["ERROR"] = 0;
                    Event::logAction($db, $param2["TYPE_ACTION"], $param2);
                }
                
                $player->set("state", "questionning");
                
                // Construction du message à l'interpellé
                require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                $dest = array();
                $dest[$player->get("name")] = $player->get("name");
                $surcis = floor($player->get("prison") * 12);
                $peine = $surcis + 24;
                
                if ($action == "steal") {
                    $type = 2;
                    $body = "\n Vous êtes arrêté pour avoir volé " . $opponent->get("name");
                    $body .= ". Vous devez rendre l'argent volée et payer 20 pièces d'or d'amende sinon vous serez conduit en prison";
                    $garde->set("moneyBank", $param["MONEY"]);
                    $garde->updateDB($db);
                }
                
                if ($action == "disarm") {
                    $type = 1;
                    $body = "\n Vous êtes arrêté pour avoir désarmé de force " . $opponent->get("name");
                    $body .= ". Votre peine de prison s'élève à " . $peine . " heures de prison (24 heures pour votre action + " . $surcis . " heures de récidive)";
                }
                
                if ($action == "kill") {
                    
                    if ($opponent->get("status") == "PC" && floor($opponent->get("money") * 0.9) > 0) {
                        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                        $dest2 = array();
                        $dest2[$opponent->get("name")] = $opponent->get("name");
                        $body2 = "Vous venez d'être assassiné dans un royaume où le meurtre est interdit. Le garde qui est intervenu sur place pour arrêter votre meutrier a récupéré les pièces d'or que vous avez perdu. Elles ont été placées sur votre compte en banque.";
                        MailFactory::sendMsg($garde, localize("Mort"), $body2, $dest2, 2, $err2, $db, - 1, 0);
                    }
                    if ($opponent->get("name") != "")
                        $body = "\n Vous êtes arrêté pour le meurtre de " . $opponent->get("name");
                    else
                        $body = "\n Vous êtes arrêté pour le meurtre de " . $opponent->get("racename");
                    
                    $type = 1;
                    $body .= ". Votre peine de prison s'élève à " . $peine . " heures de prison (24 heures pour votre action + " . $surcis . " heures de récidive)";
                }
                
                if ($action == "hit") {
                    $type = 1;
                    $body = "\n Vous êtes arrêté pour avoir agressé " . $opponent->get("name");
                    $body .= ". Votre peine de prison s'élève à " . $peine . " heures de prison (24 heures pour votre action + " . $surcis . " heures de récidive)";
                }
                
                MailFactory::sendMsg($garde, localize("Arrestation !"), $body, $dest, 2, $err2, $db, - 1, 0);
                $param["MAIL"] = 1;
                $date = gmdate('Y-m-d H:i:s');
                // $player->set("prison", $player->get("prison") +2);
                $dbi = new DBCollection(
                    "INSERT INTO BM (id_Player,id_Player\$src, value, level, name,life,date) VALUES (" . $player->get("id") . "," . $garde->get("id") . ", 2," . $type .
                         ", 'Arrestation' ,-2,'" . $date . "')", $db, 0, 0, false);
                $player->updateDB($db);
            }
        } else {
            self::warning($player, $opponent, $param, $db);
        }
    }

    static function addSentence($player, $opponent, &$param, $db, $value, $action)
    {
        // opponent is a bad boy, no sentence
        if ($opponent->get("prison") == 0) {
            $dbbm = new DBCollection("SELECT value FROM BM WHERE id_Player=" . $player->get("id") . " AND name='Arrestation'", $db);
            
            if ($dbbm->get("value") < 16) // si déjà 8 jours de prison plus à ça pres
{
                $dbc = new DBCollection("SELECT * FROM Player WHERE prison=" . $player->get("id"), $db);
                $garde = new Player();
                $garde->load($dbc->get("id"), $db);
                $dbi = new DBCollection("UPDATE BM SET value=value+" . $value . " WHERE id_Player=" . $player->get("id") . " AND name='Arrestation'", $db, 0, 0, false);
                
                if ($action == "hit")
                    $body = "\n Vous venez d'agresser " . $opponent->get("name") . ". Vous peine de prison s'alourdit de " . ($value * 12) . " heures";
                else
                    $body = "\n Vous venez de tuer " . $opponent->get("name") . ". Vous peine de prison s'alourdit de " . ($value * 12) . " heures";
                    
                    // $surcis = $player->get("prison");
                    // $peine = $surcis*12 + 24;
                    // $body.=". Votre peine de prison s'élève à ".$peine." heures de prison (24 heures pour votre action + ".$surcis." heures de récidive)";
                    // $playerSrc->set("prison", $playerSrc->get("prison")+$value);
                require_once (HOMEPATH . "/factory/MailFactory.inc.php");
                $dest = array();
                $dest[$player->get("name")] = $player->get("name");
                MailFactory::sendMsg($garde, localize("Peine de prison !"), $body, $dest, 2, $err2, $db, - 1, 0);
                $param["MAIL"] = 1;
                $date = gmdate('Y-m-d H:i:s');
                $player->updateDB($db);
            }
        } else {
            self::warning($player, $opponent, $param, $db);
        }
    }

    static function chase($player, $opponent, &$param, $db)
    {
        $date = gmdate('Y-m-d H:i:s');
        $dbi = new DBCollection(
            "INSERT INTO BM (id_Player,id_Player\$src, value, name,life,date) VALUES (" . $player->get("id") . "," . $garde->get("id") . ", 4, 'Arrestation' ,-2,'" . $date . "')", 
            $db, 0, 0, false);
        self::moveGuard($player, $opponent, $param, $db);
        $dbu = new DBCollection("UPDATE Player SET id_Player\$target=" . $player->get("id") . " WHERE prison=" . $player->get("id"), $db, 0, 0, false);
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = array();
        $dest[$player->get("name")] = $player->get("name");
        $body = "\n Vous venez de tuer un garde. Des gardes de la ville sont à votre poursuite.";
        $dbc = new DBCollection("SELECT * FROM Player WHERE prison=" . $player->get("id"), $db);
        $garde = new Player();
        $garde->load($dbc->get("id"), $db);
        MailFactory::sendMsg($garde, localize("Arrestation !"), $body, $dest, 2, $err2, $db, - 1, 0);
    }

    static function reinforcement($player, $opponent, &$param, $db, $value)
    {
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dest = array();
        $dest[$player->get("name")] = $player->get("name");
        $dbc = new DBCollection("SELECT * FROM Player WHERE prison=" . $player->get("id"), $db);
        $garde = new Player();
        $garde->load($dbc->get("id"), $db);
        $body = "\n Vous venez de tuer un garde. Vous peine de prison s'alourdit de 2 jours";
        MailFactory::sendMsg($garde, localize("Peine de prison !"), $body, $dest, 2, $err2, $db, - 1, 0);
        
        $dbi = new DBCollection("UPDATE BM SET value=value+" . $value . " WHERE id_Player=" . $player->get("id") . " AND name='Arrestation'", $db, 0, 0, false);
        $param["MAIL"] = 1;
        self::moveGuard($player, $opponent, $param, $db);
        $dbu = new DBCollection("UPDATE Player SET id_Player\$target=" . $player->get("id") . " WHERE prison=" . $player->get("id"), $db, 0, 0, false);
    }

    static function moveGuard($player, $opponent, &$param, $db)
    {
        $mapObj = new Map($db, $player->get("map"));
        $idStartKingdom = $mapObj->getStartKingdomIdFromMap($player->get("x"), $player->get("y"));
        
        if ($idStartKingdom == ID_ARTASSE)
            $id = 1000;
        elseif ($idStartKingdom == ID_EAROK)
            $id = 2000;
        else
            $id = 3000;
        
        $param2["DELAI"] = 1;
        
        $garde = new Player();
        $garde->load($id, $db);
        $i = 1;
        while ($garde->get("prison") != 0 && $i < 6) {
            $garde->load($id + $i, $db);
            $i ++;
        }
        
        $garde2 = new Player();
        $garde2->load($id + $i, $db);
        while ($garde2->get("prison") != 0 && $i < 6) {
            $garde2->load($id + $i, $db);
            $i ++;
        }
        $good = 0;
        
        if ($garde->get("prison") == 0 && $garde->get("id") > 0) {
            if ($player->get("inbuilding")) {
                $good ++;
                $garde->set("x", $player->get("x"));
                $garde->set("y", $player->get("y"));
                $garde->set("prison", $player->get("id"));
                $garde->set("inbuilding", $player->get("inbuilding"));
                $garde->set("room", $player->get("room"));
                self::globalInfo($garde, $param2);
                $param2["TYPE_ACTION"] = M_TELEPORT;
                $param2["TYPE_ATTACK"] = M_TELEPORT_EVENT;
                Event::logAction($db, M_TELEPORT, $param2);
                $garde->updateDB($db);
            } else {
                $place = self::getAleaFreePlace($player->get("x"), $player->get("y"), $player->get("map"), 1, $db);
                if ($place != 0) {
                    $good ++;
                    $garde->set("x", $place["x"]);
                    $garde->set("y", $place["y"]);
                    $garde->set("inbuilding", $player->get("inbuilding"));
                    $garde->set("room", $player->get("room"));
                    $garde->set("prison", $player->get("id"));
                    $garde->updateHidden($db);
                    
                    self::globalInfo($garde, $param2);
                    $param2["TYPE_ACTION"] = M_TELEPORT;
                    $param2["TYPE_ATTACK"] = M_TELEPORT_EVENT;
                    Event::logAction($db, M_TELEPORT, $param2);
                    $garde->updateDB($db);
                }
            }
        }
        if ($garde2->get("prison") == 0 && $garde2->get("id") > 0) {
            if ($player->get("inbuilding")) {
                $good += 2;
                $garde2->set("x", $player->get("x"));
                $garde2->set("y", $player->get("y"));
                $garde->set("inbuilding", $player->get("inbuilding"));
                $garde->set("room", $player->get("room"));
                self::globalInfo($garde2, $param3);
                $garde2->set("prison", $player->get("id"));
                $param3["TYPE_ACTION"] = M_TELEPORT;
                $param3["TYPE_ATTACK"] = M_TELEPORT_EVENT;
                Event::logAction($db, M_TELEPORT, $param3);
                $garde2->updateDB($db);
            } else {
                $place = self::getAleaFreePlace($player->get("x"), $player->get("y"), $player->get("map"), 1, $db);
                if ($place != 0) {
                    $good += 2;
                    $garde2->set("x", $place["x"]);
                    $garde2->set("y", $place["y"]);
                    $garde2->set("inbuilding", 0);
                    $garde2->set("room", 0);
                    $garde2->set("prison", $player->get("id"));
                    $garde2->updateHidden($db);
                    self::globalInfo($garde2, $param2);
                    $param2["TYPE_ACTION"] = M_TELEPORT;
                    $param2["TYPE_ATTACK"] = M_TELEPORT_EVENT;
                    Event::logAction($db, M_TELEPORT, $param2);
                    $garde2->updateDB($db);
                }
            }
        }
        
        return $good; // 0 -> aucun garde ; 1 -> garde 1 seulement ; 2 -> garde 2 seulement ; 3 les deux gardes
    }

    static function kill($player, $opponent, &$param, &$db)
    {
        $param["TARGET_KILLED"] = 1;
        $player->set("nbkill", ($player->get("nbkill") + 1));
        $px = 1;
        
        // Pas de px pour un feu fol controlé
        if ($opponent->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && $opponent->get("id_Member") != 0)
            $px = 0;
            // Pas de px pour le kill de quelqu'un du même groupe de chasse
        if ($player->get("id_FighterGroup") != 0 && $opponent->get("id_FighterGroup") == $player->get("id_FighterGroup"))
            $px = 0;
            // pas de px pour le kill de quelqu'un de même id member
        if ($player->get("id_Member") == $opponent->get("id_Member"))
            $px = 0;
        
        if ($px)
            self::divisionXP($player, $opponent->get("level"), $param, $db);
        else
            $param["NOMBRE"] = 0;
        
        if ($player->get("status") == 'PC' or $player->get("id_Member") != 0)
            self::legalAction($player, $opponent, $param, $db, 1);
        else
            $player->set("id_Player\$target", 0);
        
        if ($opponent->get("status") == "PC" && $opponent->get("authlevel") >= 0 || ($opponent->get("id_BasicRace") == 5 && $opponent->get("racename") != "Garde du Palais")) {
            $player->set("nbkillpc", ($player->get("nbkillpc") + 1));
            $param["TYPE_ATTACK"] = ACTION_EVENT_DEATH_PC;
            PlayerFactory::resurrection($opponent, $param, $db);
        } else {
            $param["TYPE_ATTACK"] = ACTION_EVENT_DEATH_NPC;
            if ($opponent->get("id_BasicRace") == 263) {
                $param["TURTLE_KILLED"] = 1;
                $player->set("merchant_level", 0);
            }
            
            if ($opponent->get("id_Member") > 0) {
                $dbs = new DBCollection(
                    "SELECT fightMsg,deadMsg,email FROM Player LEFT JOIN Member ON  Member.id=Player.id_Member LEFT JOIN MemberOption ON Member.id=MemberOption.id_Member LEFT JOIN MemberDetail ON Member.id=MemberDetail.id_Member WHERE Player.id=" .
                         $opponent->get("id"), $db);
                if (! $dbs->eof()) {
                    $deadMsg = $dbs->get("deadMsg");
                    $targetEmail = $dbs->get("email");
                }
                if ($deadMsg == 1) {
                    $body = "\nVotre suivant  " . $opponent->get("racename") . "(" . $opponent->get("id") . ") a été tué par " . $player->get("name") . "(" . $player->get("id") .
                         ").\nVous trouverez plus de détails dans les évènements de celui-ci.";
                    
                    $dbpp = new DBCollection("select name from Player where id=" . $opponent->get("id_Owner"), $db);
                    if ($dbpp->count() > 0) {
                        $dest = array();
                        $dest[$dbpp->get("name")] = $dbpp->get("name");
                        MailFactory::sendMsg($opponent, "Mort de votre suivant " . $opponent->get("racename") . "(" . $opponent->get("id") . ")", $body, $dest, 2, $err, $db, 1, 0);
                    }
                }
            }
            
            PNJFactory::DeleteDBnpc($opponent, $param, $db, $player);
        }
        
        $player->updateDB($db);
    }

    static function autokill($player, &$param, &$db)
    {
        $param["TARGET_KILLED"] = 1;
        $param["TYPE_ATTACK"] = ACTION_EVENT_DEATH_PC;
        if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 || ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
            PlayerFactory::resurrection($player, $param, $db);
        } else {
            PNJFactory::DeleteDBnpc($player, $param, $db, null);
        }
    }

    static function divisionXP($player, $level, &$param, &$db)
    {
        $xp = $player->get("x");
        $yp = $player->get("y");
        $group = $player->get("id_FighterGroup");
        $pxKill = max($level, 5 + 2 * $level - $player->get("level"));
        $param["NOMBRE"] = 1;
        if ($group == 0) {
            $param["XP"] += $pxKill;
        } else {
            $dbc = new DBCollection(
                "SELECT * FROM Player WHERE room!=21 AND id_FighterGroup=" . $group . " AND map=" . $player->get("map") . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" .
                     $xp . "-" . $yp . "))/2 <=8 ORDER BY RAND()", $db);
            
            $nombre = $dbc->count();
            $param["NOMBRE"] = $nombre;
            $reste = $pxKill % $nombre;
            $i = 0;
            while (! $dbc->eof()) {
                
                $bonus = 0;
                if ($reste > 0)
                    $bonus = 1;
                $reste --;
                
                if ($dbc->get("id") != $player->get("id")) {
                    $i ++;
                    
                    $equipier = new Player();
                    $equipier->load($dbc->get("id"), $db);
                    
                    $gain = floor($pxKill / $nombre) + $bonus;
                    
                    if ($equipier->get("modeAnimation") == 1) {
                        $gain = 0;
                    }
                    
                    $param["PLAYER" . $i] = $equipier->get("name");
                    if (isset($param["GAIN" . $i]))
                        $param["GAIN" . $i] += $gain;
                    else
                        $param["GAIN" . $i] = $gain;
                    
                    PlayerFactory::addXP($equipier, $gain, $db);
                    self::globalInfo($equipier, $param2);
                    $param2["TYPE_ACTION"] = GAIN_XP;
                    $param2["TYPE_ATTACK"] = GAIN_XP_EVENT;
                    $param2["ERROR"] = 0;
                    $param2["PLAYER_NAME"] = $param["PLAYER_NAME"];
                    $param2["KILLER_NAME"] = $param["PLAYER_NAME"];
                    $param2["KILLER_ID"] = $param["PLAYER_ID"];
                    $param2["TARGET_NAME"] = $param["TARGET_NAME"];
                    $param2["TARGET_ID"] = $param["TARGET_ID"];
                    
                    $param2["TARGET_LEVEL"] = $param["TARGET_LEVEL"];
                    $param2["GAIN_XP"] = $gain;
                    
                    Event::logAction($db, GAIN_XP, $param2);
                    $equipier->updateDB($db);
                } else {
                    if ($player->get("modeAnimation") == 0) {
                        $param["XP"] += floor($pxKill / $nombre) + $bonus;
                    }
                }
                $dbc->next();
            }
        }
    }

    /* *************************************************************** MENU ACTION **************************************************** */
    static function concentration($playerSrc, &$param, $db)
    {
        $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Concentration' ORDER BY date DESC", $db);
        
        if ($dbbm->eof() && $playerSrc->get("status") == "NPC")
            return;
        else {
            $playerSrc->set("ap", $playerSrc->get("ap") - 2);
            $num = floor($dbbm->get("value") / 100);
            if ($num == 1)
                $dba = new DBCollection(
                    "SELECT * FROM Ability LEFT JOIN BasicAbility ON Ability.id_BasicAbility=BasicAbility.id WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicAbility=" .
                         ($dbbm->get("value") - 100), $db);
            if ($num == 2)
                $dba = new DBCollection(
                    "SELECT * FROM MagicSpell LEFT JOIN BasicMagicSpell ON MagicSpell.id_BasicMagicSpell=BasicMagicSpell.id WHERE id_Player=" . $playerSrc->get("id") .
                         " AND id_BasicMagicSpell=" . ($dbbm->get("value") - 200), $db);
            if ($num == 3)
                $dba = new DBCollection(
                    "SELECT * FROM Talent LEFT JOIN BasicTalent ON BasicTalent.id=Talent.id_BasicTalent WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicTalent=" .
                         ($dbbm->get("value") - 300), $db);
            
            $param["NAME"] = $dba->get("name");
            
            $param["ENHANCE"] = 1;
            $param["PERCENT"] = $dba->get("skill");
            $param["SCORE"] = mt_rand(1, 100);
            if ($param["SCORE"] > $param["PERCENT"])
                $param["SUCCESS"] = 1;
            else
                $param["SUCCESS"] = 0;
            
            if ($playerSrc->get("modeAnimation") == 1) {
                $param["SUCCESS"] = 0;
            }
            
            if ($param["SUCCESS"] == 1) {
                $dbx = new DBCollection("DELETE FROM BM WHERE name='Concentration' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
                
                if ($num == 1)
                    $dba = new DBCollection("UPDATE Ability SET skill=skill+1 WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicAbility=" . ($dbbm->get("value") - 100), $db, 
                        0, 0, false);
                if ($num == 2)
                    $dba = new DBCollection(
                        "UPDATE MagicSpell SET skill=skill+1 WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicMagicSpell=" . ($dbbm->get("value") - 200), $db, 0, 0, false);
                if ($num == 3)
                    $dba = new DBCollection("UPDATE Talent SET skill=skill+1 WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicTalent=" . ($dbbm->get("value") - 300), $db, 
                        0, 0, false);
            }
        }
    }

    static function move($playerSrc, $x, $y, &$param, $db, $type = 1)
    {
        $dbc = new DBCollection("SELECT * FROM Player WHERE status='PC'", $db);
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = MOVE;
        if ($type == 0)
            $type = 1;
        $param["TYPE_ATTACK"] = $type;
        
        require_once (HOMEPATH . "/lib/MapInfo.inc.php");
        $currmap = $playerSrc->get("map");
        $mapinfo = new MapInfo($db);
        $validzone = $mapinfo->getValidMap($x, $y, 0, $currmap);
        $param["NEWX"] = $x;
        $param["NEWY"] = $y;
        
        if (! $validzone[0][0])
            return MOVE_INVALID_POSITION;
        
        if (! (self::freePlace($x, $y, $playerSrc->get("map"), $db, 0, 1)))
            return MOVE_CASE_NOT_FREE;
        
        $PA = $playerSrc->getTimetoMove($db);
        $param["AP"] = $PA;
        
        if ($playerSrc->get("state") == "creeping")
            $param["AP"] = 5;
        
        if ($playerSrc->get("ap") < $param["AP"])
            return MOVE_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && $playerSrc->get("id_Owner") > 0) {
            $dbc = new DBCollection("SELECT * FROM Player WHERE id=" . $playerSrc->get("id_Owner"), $db);
            
            if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbc->get("x"), $dbc->get("y")) > 5)
                return GOLEM_CONTROL_LOST;
            
            if (distHexa($x, $y, $dbc->get("x"), $dbc->get("y")) > 5)
                return GOLEM_MOVE_TOO_FAR;
        }
        
        if ($playerSrc->get("x") == $x && $playerSrc->get("y") == $y)
            return MOVE_POSITION_UNCHANGED;
        
        $dx = $playerSrc->get("x") - $x;
        $dy = $playerSrc->get("y") - $y;
        
        if ((abs($dx) + abs($dy) + abs($dx + $dy)) / 2 > 1)
            return MOVE_TOO_FAR;
            
            /*
         * $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND (name='Aura de courage' OR name='Aura de résistance') GROUP BY id_Player\$src",
         * $db);
         * while (! $dbc->eof()) {
         * $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("id_Player\$src"), $db);
         * if (distHexa($dbp->get("x"), $dbp->get("y"), $x, $y) > 8)
         * $dbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND (name='Aura de courage' OR name='Aura de résistance')", $db, 0, 0, false);
         *
         * $dbc->next();
         * }
         */
        
        self::updateInfoAfterMove($x, $y, $playerSrc, $db);
        $mapinfo->updateWayCount($x, $y, $currmap, $playerSrc);
        
        $playerSrc->set("x", $x);
        $playerSrc->set("y", $y);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - $PA);
        
        $playerSrc->reloadPartialAttribute($playerSrc->get("id"), "currhp", $db);
        
        $playerSrc->updateHidden($db);
        playerFactory::InitBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        
        if ($playerSrc->get("state") == "turtle") {
            $dbt = new DBCollection(
                "SELECT Player.id as id, Player.x as x, Player.y as y, Player.room as room FROM Player LEFT JOIN Caravan ON Player.id_Caravan=Caravan.id WHERE  Caravan.id_Player=" .
                     $playerSrc->get("id") . " AND Player.id_BasicRace=263", $db);
            if ($dbt->count() > 0) {
                $dbc = new DBCollection("UPDATE Player SET x=" . $x . ", y=" . $y . ", hidden=" . ($playerSrc->get("hidden") - 3) . " WHERE id=" . $dbt->get("id"), $db, 0, 0, false);
            }
        }
        
        $playerSrc->updateDB($db);
        return MOVE_NO_ERROR;
    }
    // to update status on Aura de Résistance and Aura de courgae after a move: move, hustle, teleport, recal...
    static function updateInfoAfterMove($x, $y, $playerSrc, $db)
    {
        self::updateFeuFolStatusAfterMove($x, $y, $playerSrc, $db);
        self::updateAuraInfoAfterMove($x, $y, $playerSrc, 'Aura de résistance', $db);
        self::updateAuraInfoAfterMove($x, $y, $playerSrc, 'Aura de courage', $db);
    }

    static function updateFeuFolStatusAfterMove($x, $y, $playerSrc, $db)
    {
        if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL && $playerSrc->get("id_Member") > 0) {
            $dbc = new DBCollection("SELECT * FROM Player WHERE id=" . $playerSrc->get("id_Owner"), $db);
            if ($dbc->count() > 0) {
                if (distHexa($x, $y, $dbc->get("x"), $dbc->get("y")) > 5) {
                    $feufolOwner = new Player();
                    $feufolOwner->externDBObj("Modifier");
                    $feufolOwner->load($dbc->get("id"), $db);
                    PlayerFactory::deleteFeuFol($feufolOwner, $param, $db);
                }
            }
        } else {
            $dbc = new DBCollection("SELECT * FROM Player WHERE id_BasicRace=" . ID_BASIC_RACE_FEU_FOL . " AND id_Owner=" . $playerSrc->get("id"), $db);
            if ($dbc->count() > 0) {
                if (distHexa($x, $y, $dbc->get("x"), $dbc->get("y")) > 5) {
                    PlayerFactory::deleteFeuFol($playerSrc, $param, $db);
                }
            }
        }
    }

    static function updateAuraInfoAfterMove($x, $y, $playerSrc, $auraName, $db)
    {
        $dbtri = new DBCollection(
            "SELECT case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                 " then p2.id_FighterGroup else p1.id_FighterGroup end as id_FighterGroup FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                 ID_BASIC_RACE_FEU_FOL . ") WHERE p1.id=" . $playerSrc->get("id"), $db);
        $idGdc = $dbtri->get("id_FighterGroup");
        // recherche des auras lancés par les membres de son GDC
        if ($idGdc != "" && $idGdc > 0) {
            $dbbm = new DBCollection(
                "SELECT BM.id_Player\$src as id_pj_src,BM.name,p1.x,p1.y,p1.map FROM BM inner join Player p1 on p1.id = BM.id_Player\$src WHERE BM.name='" . $auraName .
                     "' AND id_FighterGroup =" . $idGdc . " GROUP BY BM.id_Player\$src", $db);
            while (! $dbbm->eof()) {
                $dbp = new DBCollection(
                    "SELECT * FROM BM WHERE id_Player\$src = " . $dbbm->get("id_pj_src") . " and name='" . $auraName . "' AND id_Player =" . $playerSrc->get("id") .
                         " GROUP BY id_Player\$src", $db);
                if ($dbp->count() == 0 && distHexa($dbbm->get("x"), $dbbm->get("y"), $x, $y) <= 8) {
                    
                    $pjAuraSrc = new Player();
                    $pjAuraSrc->externDBObj("Modifier");
                    $pjAuraSrc->load($dbbm->get("id_pj_src"), $db);
                    
                    $bonusResistance = floor(10 + $pjAuraSrc->getScore("magicSkill") / 4);
                    $bonusBravery = floor(5 + $pjAuraSrc->get("currhp") / 20);
                    
                    $static = new StaticModifier();
                    if ($auraName == 'Aura de résistance') {
                        $static->setModif("armor", DICE_MULT, $bonusResistance);
                    }
                    if ($auraName == 'Aura de courage') {
                        $static->setModif("attack", DICE_MULT, $bonusBravery);
                    }
                    
                    $bm = new BM();
                    $bm->set("name", $auraName);
                    $bm->set("life", - 2);
                    $bm->set("id_Player\$src", $dbbm->get("id_pj_src"));
                    $bm->set("id_Player", $playerSrc->get("id"));
                    $bm->set("date", gmdate("Y-m-d H:i:s"));
                    $bm->setObj("StaticModifier", $static, true);
                    $bm->addDBr($db);
                }
                $dbbm->next();
            }
        }
        // recherche de toutes les auras qui concernent le PJ
        $dbbm = new DBCollection("SELECT * FROM BM WHERE BM.name='" . $auraName . "' AND BM.id_Player =" . $playerSrc->get("id") . " GROUP BY id_Player\$src", $db);
        while (! $dbbm->eof()) {
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbbm->get("id_Player\$src"), $db);
            while (! $dbp->eof()) {
                if (distHexa($dbp->get("x"), $dbp->get("y"), $x, $y) > 8 || $idGdc != $dbp->get("id_FighterGroup")) { // Le PJ est trop loin suppression de l'aura
                    $dbx = new DBCollection(
                        "DELETE FROM BM WHERE id_Player\$src=" . $dbbm->get("id_Player\$src") . " and id_Player='" . $playerSrc->get("id") . "' AND name='" . $auraName . "'", $db, 
                        0, 0, false);
                    $opponent = new Player();
                    $opponent->externDBObj("Modifier");
                    $opponent->load($dbp->get("id"), $db);
                    $idOpponent = $opponent->get("id");
                    if (isset($idOpponent) && ($idOpponent >= 0)) { // pour les auras qui sont supprimées pour des PJs disparus entre temps
                        PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                        $opponent->updateDBr($db);
                    }
                }
                $dbp->next();
            }
            $dbbm->next();
        }
        // recherche de toutes les auras créées par le PJ
        $dbbm = new DBCollection("SELECT * FROM BM WHERE name='" . $auraName . "' AND id_Player\$src =" . $playerSrc->get("id") . " GROUP BY id_Player", $db);
        $bonusResistance = floor(10 + $playerSrc->getScore("magicSkill") / 4);
        $bonusBravery = floor(5 + $playerSrc->get("currhp") / 20);
        while (! $dbbm->eof()) {
            // a poursuivre trouver les PJs a qui devraient profiter l'aura
            $dbp = new DBCollection(
                "SELECT p1.id,p1.x,p1.y, case when p1.id_BasicRace = " . ID_BASIC_RACE_FEU_FOL .
                     " then p2.id_FighterGroup else p1.id_FighterGroup end as id_FighterGroup  FROM Player p1 left outer join Player p2 on p2.id = p1.id_Owner and p1.id_BasicRace in (" .
                     ID_BASIC_RACE_FEU_FOL . ") WHERE ((abs(p1.x-" . $x . ") + abs(p1.y-" . $y . ") + abs(p1.x+p1.y-" . $x . "-" . $y . "))/2 <=8) or ((p1.id_FighterGroup=" .
                     $playerSrc->get("id_FighterGroup") . " and (p1.id = " . $playerSrc->get("id") . " or " . $playerSrc->get("id_FighterGroup") . " > 0)) or (p2.id_FighterGroup=" .
                     $playerSrc->get("id_FighterGroup") . " and (p2.id = " . $playerSrc->get("id") . " or " . $playerSrc->get("id_FighterGroup") . " > 0) ))", $db);
            while (! $dbp->eof()) {
                $dba = new DBCollection(
                    "SELECT * FROM BM WHERE name='" . $auraName . "' AND id_Player\$src =" . $playerSrc->get("id") . " and id_Player = " . $dbp->get("id") .
                         " GROUP BY id_Player\$src", $db);
                if ((distHexa($dbp->get("x"), $dbp->get("y"), $x, $y) <= 8) && $dbp->get("id_FighterGroup") == $idGdc) {
                    // distance <= 8 et même groupe de chasse on ajoute le BM
                    if ($dba->eof()) {
                        $static = new StaticModifier();
                        if ($auraName == 'Aura de résistance') {
                            $static->setModif("armor", DICE_MULT, $bonusResistance);
                        }
                        if ($auraName == 'Aura de courage') {
                            $static->setModif("attack", DICE_MULT, $bonusBravery);
                        }
                        
                        $bm = new BM();
                        $bm->set("name", $auraName);
                        $bm->set("life", - 2);
                        $bm->set("id_Player\$src", $playerSrc->get("id"));
                        $bm->set("id_Player", $dbp->get("id"));
                        $bm->set("date", gmdate("Y-m-d H:i:s"));
                        $bm->setObj("StaticModifier", $static, true);
                        $bm->addDBr($db);
                        
                        $opponent = new Player();
                        $opponent->externDBObj("Modifier");
                        $opponent->load($dbp->get("id"), $db);
                        PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                        $opponent->updateDBr($db);
                    }
                } else {
                    if (! $dba->eof()) {
                        // sinon suppression de l'aura
                        $dbx = new DBCollection(
                            "DELETE FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " and id_Player='" . $dbp->get("id") . "' AND name='" . $auraName . "'", $db, 0, 0, 
                            false);
                        $opponent = new Player();
                        $opponent->externDBObj("Modifier");
                        $opponent->load($dbp->get("id"), $db);
                        $idOpponent = $opponent->get("id");
                        if (isset($idOpponent) && ($idOpponent >= 0)) { // pour les auras qui sont supprimées pour des PJs disparus entre temps
                            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
                            $opponent->updateDBr($db);
                        }
                    }
                }
                $dbp->next();
            }
            $dbbm->next();
        }
    }

    static function enterBuilding($playerSrc, $idBuilding, &$param, $db)
    {
        $param["TYPE_ACTION"] = ENTER_BUILDING;
        $dbbuilding = new DBCollection("SELECT id,x,y,name,id_BasicBuilding,id_City,sp,currsp FROM Building WHERE id=" . $idBuilding, $db, 0, 0);
        $curbuilding = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        
        $dbcity = new DBCollection("SELECT * FROM City WHERE id=" . $dbbuilding->get("id_City"), $db);
        
        $param["AP"] = $playerSrc->getTimeToMove($db);
        if ($dbbuilding->get("name") == "Rempart" && $dbcity->get("door") > 1 && ($curbuilding->count() == 0 || $curbuilding->get("name") != "Rempart"))
            $param["AP"] = 3;
        
        if (self::canEnter($playerSrc, $idBuilding, $db) == 0)
            return MOVE_INVALID_POSITION;
        
        if ($playerSrc->get("ap") < $param["AP"])
            return MOVE_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
            $dbc = new DBCollection(
                "SELECT Player.id as id, Player.x as x, Player.y as y FROM Player LEFT JOIN BM ON Player.id=BM.id_Player WHERE id_Player\$src =" . $playerSrc->get("id"), $db);
            
            if (distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbc->get("x"), $dbc->get("y")) > 5)
                return GOLEM_CONTROL_LOST;
            
            if (distHexa($dbbuilding->get("x"), $dbbuilding->get("y"), $dbc->get("x"), $dbc->get("y")) > 5)
                return GOLEM_MOVE_TOO_FAR;
        }
        
        $playerSrc->set("x", $dbbuilding->get("x"));
        $playerSrc->set("y", $dbbuilding->get("y"));
        $playerSrc->set("inbuilding", $idBuilding);
        $playerSrc->set("room", 1);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - $param["AP"]);
        
        if ($playerSrc->get("state") == "turtle") {
            $dbt = new DBCollection("SELECT * FROM Player WHERE id_Owner=" . $playerSrc->get("id") . " AND id_BasicRace=263", $db);
            $turtle = new Player();
            $turtle->load($dbt->get("id"), $db);
            $turtle->set("x", $playerSrc->get("x"));
            $turtle->set("y", $playerSrc->get("y"));
            $turtle->set("inbuilding", $playerSrc->get("inbuilding"));
            $turtle->set("room", 1);
            $turtle->set("walking", 1);
            $turtle->updateHidden($db);
            $turtle->updateDB($db);
        }
        
        self::updateInfoAfterMove($dbbuilding->get("x"), $dbbuilding->get("y"), $playerSrc, $db);
        
        $playerSrc->updateHidden($db);
        $playerSrc->updateDB($db);
        
        return MOVE_NO_ERROR;
    }

    static function leaveBuilding($playerSrc, $xc, $yc, &$param, $db)
    {
        $dbbuilding = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbcity = new DBCollection("SELECT * FROM City WHERE id=" . $dbbuilding->get("id_City"), $db);
        
        if ($dbbuilding->get("name") == "Rempart" && $dbcity->get("door") > 1 && $playerSrc->get("ap") < 2.5 + $playerSrc->getTimetoMove($db))
            return MOVE_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("state") == "turtle") {
            $error = self::leaveBuildingWithTurtle($playerSrc, $xc, $yc, $param, $db);
            if (! $error && $dbbuilding->get("name") == "Rempart" && $dbcity->get("door") > 1) {
                $param["AP"] += 2.5;
                $playerSrc->set("ap", $playerSrc->get("ap") - 2.5);
            }
        } else {
            $error = self::move($playerSrc, $xc, $yc, $param, $db);
            if (! $error) {
                if ($dbbuilding->get("name") == "Rempart" && $dbcity->get("door") > 1) {
                    $param["AP"] += 2.5;
                    $playerSrc->set("ap", $playerSrc->get("ap") - 2.5);
                }
                $playerSrc->set("inbuilding", 0);
                $playerSrc->set("room", 0);
                $playerSrc->updateHidden($db);
                $playerSrc->updateDB($db);
                self::updateInfoAfterMove($xc, $yc, $playerSrc, $db);
            }
        }
        
        return $error;
    }

    static function leaveBuildingWithTurtle($playerSrc, $xc, $yc, &$param, $db)
    {
        $dbt = new DBCollection(
            "SELECT Player.id as id, Player.x as x, Player.y as y, Player.room as room FROM Player LEFT JOIN Caravan ON Player.id_Caravan=Caravan.id WHERE  Caravan.id_Player=" .
                 $playerSrc->get("id") . " AND Player.id_BasicRace=263", $db);
        $turtle = new Player();
        $turtle->load($dbt->get("id"), $db);
        
        $error = self::move($playerSrc, $xc, $yc, $param, $db);
        // $error2 = self::move($turtle,$xc,$yc,$param,$db); // marche pas place occupée ^^
        
        if (! $error) {
            
            $playerSrc->set("inbuilding", 0);
            $playerSrc->set("room", 0);
            $playerSrc->updateHidden($db);
            $playerSrc->updateDB($db);
            
            $turtle->set("x", $playerSrc->get("x"));
            $turtle->set("y", $playerSrc->get("y"));
            $turtle->set("inbuilding", 0);
            $turtle->updateHidden($db);
            $turtle->set("room", 0);
            $turtle->updateDB($db);
            
            self::updateInfoAfterMove($xc, $yc, $playerSrc, $db);
        }
        
        return $error;
    }

    static function autoHustleInterposition(&$playerSrc, &$opponent, $db)
    {
        self::appear($playerSrc, $param, $db);
        self::appear($opponent, $param, $db);
        
        $xtmp = $opponent->get("x");
        $ytmp = $opponent->get("y");
        
        $opponent->set("x", $playerSrc->get("x"));
        $opponent->set("y", $playerSrc->get("y"));
        $playerSrc->set("x", $xtmp);
        $playerSrc->set("y", $ytmp);
        $playerSrc->updateHidden($db);
        $opponent->updateHidden($db);
        
        self::updateInfoAfterMove($playerSrc->get("x"), $playerSrc->get("y"), $playerSrc, $db);
        self::updateInfoAfterMove($opponent->get("x"), $opponent->get("y"), $opponent, $db);
        
        $opponent->updateDB($db);
        $playerSrc->updateDB($db);
    }

    static function hustle($playerSrc, $opponent, &$param, $db)
    {
        self::appear($playerSrc, $param, $db);
        $param["TYPE_ACTION"] = HUSTLE;
        self::globalInfo($playerSrc, $param);
        self::globalInfoOpponent($opponent, $param);
        
        $PA = $playerSrc->getTimeToMove($db);
        $param["AP"] = HUSTLE_AP;
        
        if ($playerSrc->get("inbuilding") > 0) {
            $curbuilding = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
            $dbcity = new DBCollection("SELECT * FROM City WHERE id=" . $curbuilding->get("id_City"), $db);
            
            if ($curbuilding->count() > 0 && $curbuilding->get("name") == "Rempart" && $dbcity->get("door") > 1)
                $PA += 3;
        }
        
        if ($opponent->get("id_member") == 0 && $opponent->get("status") == 'NPC' && $opponent->get("level") > 2 && $opponent->get("id_BasicRace") != ID_BASIC_RACE_FEU_FOL &&
             $opponent->get("id_BasicRace") != 263) {
            return ACTION_NOT_ALLOWED;
        }
        
        if ($playerSrc->get("ap") < ($PA + HUSTLE_AP))
            return ACTION_NOT_ENOUGH_AP;
        
        $idBuilding = $playerSrc->get("inbuilding");
        
        $param["JET"] = 0;
        $param["SUCCESS"] = 1;
        
        // Si on est hors de la ville
        if ($opponent->isInCity($db, 0) == 0 || $opponent->get("id_BasicRace") == 263) {
            
            // Si on est pas dans un royaume de base ou pas dans une tortue
            $mapObj = new Map($db, $playerSrc->get("map"));
            if ($opponent->get("id_BasicRace") == 263 || ! ($mapObj->getStartKingdomIdFromMap($playerSrc->get("x"), $playerSrc->get("y")) != ID_UNKNOWN_KINGDOM)) {
                
                $param["JET"] = 1;
                $param["PLAYER_ATTACK"] = floor($playerSrc->getScore("strength") + ($playerSrc->getScore("dexterity") * 1 / 2));
                $param["TARGET_DEFENSE"] = floor($opponent->getScore("strength") + ($opponent->getScore("dexterity") * 1 / 2));
                if ($param["PLAYER_ATTACK"] <= $param["TARGET_DEFENSE"]) {
                    $param["SUCCESS"] = 0;
                    $param["TYPE_ATTACK"] = HUSTLE_EVENT_FAIL;
                }
            }
        }
        
        if ($param["SUCCESS"]) {
            $param["AP"] += $PA;
            $param["XOLD"] = $playerSrc->get("x");
            $param["YOLD"] = $playerSrc->get("y");
            $param["XNEW"] = $opponent->get("x");
            $param["YNEW"] = $opponent->get("y");
            
            // Si on bouscule une tortue avec quelqu'un à l'intérieur
            if ($opponent->get("id_BasicRace") == 263) {
                $dbn = new DBCollection("SELECT id, state FROM Player WHERE id=" . $opponent->get("id_Owner"), $db);
                if ($dbn->get("state") == "turtle")
                    $dbu = new DBCollection(
                        "UPDATE Player SET room=" . $playerSrc->get("room") . ", inbuilding=" . $playerSrc->get("inbuilding") . ",x=" . $playerSrc->get("x") . ", y=" .
                             $playerSrc->get("y") . " WHERE id=" . $dbn->get("id"), $db, 0, 0, false);
            }
            $param["TYPE_ATTACK"] = HUSTLE_EVENT_SUCCESS;
            $opponent->set("x", $playerSrc->get("x"));
            $opponent->set("y", $playerSrc->get("y"));
            $opponent->set("inbuilding", $idBuilding);
            $opponent->set("room", $playerSrc->get("room"));
            $opponent->updateHidden($db);
            
            $playerSrc->set("inbuilding", 0);
            $playerSrc->set("room", 0);
            $playerSrc->set("x", $param["XNEW"]);
            $playerSrc->set("y", $param["YNEW"]);
            $playerSrc->updateHidden($db);
            
            // Si on est dans une tortue
            if ($playerSrc->get("state") == "turtle") {
                $dbt = new DBCollection(
                    "SELECT Player.id as id FROM Player LEFT JOIN Caravan ON Player.id_Caravan=Caravan.id WHERE  Caravan.id_Player=" . $playerSrc->get("id") .
                         " AND Player.id_BasicRace=263", $db);
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=" . ($playerSrc->get("hidden") - 3) . ", room=" . $playerSrc->get("room") . ", inbuilding=" . $playerSrc->get("inbuilding") . ", x=" .
                         $playerSrc->get("x") . ", y=" . $playerSrc->get("y") . " WHERE id=" . $dbt->get("id"), $db, 0, 0, false);
            }
            $mapinfo = new MapInfo($db);
            $mapinfo->updateWayCount($param["XNEW"], $param["YNEW"], $playerSrc->get("map"), $playerSrc);
            $mapinfo->updateWayCount($playerSrc->get("x"), $playerSrc->get("y"), $opponent->get("map"), $opponent);
            
            self::updateInfoAfterMove($param["XNEW"], $param["YNEW"], $playerSrc, $db);
            self::updateInfoAfterMove($playerSrc->get("x"), $playerSrc->get("y"), $opponent, $db);
        }
        $param["ERROR"] = 0;
        $opponent->updateDB($db);
        $playerSrc->updateDB($db);
        $playerSrc->set("ap", $playerSrc->get("ap") - $param["AP"]);
    }

    static function interposition($playerSrc, $protecteur, &$param, $db, $modifierA, $modifierB, $ident, $opponent)
    {
        self::autoHustleInterposition($protecteur, $opponent, $db);
        
        self::globalInfo($playerSrc, $param);
        
        self::globalInfoOpponent($protecteur, $param);
        $param["NAME"] = $opponent->get("name");
        $param["INTER_ID"] = $opponent->get("id");
        $param["TYPE_ACTION"] = ATTACK;
        $param["IDENT"] = $ident;
        $param["PROTECTION"] = 1;
        $date = gmdate("Y-m-d H:i:s");
        
        // Création clone et application des modifiers - Redondant pour playerSrc je pense
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $protecteurModif = clone ($protecteur->getObj("Modifier"));
        $charac = array(
            "attack",
            "damage",
            "armor"
        );
        foreach ($charac as $key => $name) {
            for ($type = 0; $type < DICE_CHARAC; $type ++) {
                if ($modifierA != null)
                    $playerSrcModif->addModif($name, $type, $modifierA->getModif($name, $type));
                
                // if($modifierB!=null)
                // $protecteurModif->addModif($name,$type,$modifierB->getModif($name,$type));
            }
        }
        
        SpellFactory::passiveBarrier($playerSrc, $protecteur, $param, $db);
        if ($param["PLAYER_KILLED"])
            $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_FAIL;
        else {
            
            if ($ident == ABILITY_LIGHT)
                $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("magicSkill") * 1.5;
            else
                $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("attack");
            
            $param["BONUS_DAMAGE"] = floor($param["PLAYER_ATTACK"] / 4);
            $param["BONUS_DAMAGE2"] = $param["BONUS_DAMAGE"];
            if ($ident == ABILITY_TREACHEROUS) {
                $param["PLAYER_SPEED"] = $playerSrcModif->getScore("speed");
                $param["BONUS_DAMAGE"] += ceil($param["BONUS_DAMAGE"] * min(1.2, 0.5 + $param["PLAYER_SPEED"] / 100));
            }
            $param["TARGET_DAMAGE"] = $playerSrcModif->getScore("damage");
            $param["TARGET_DAMAGETOT"] = $param["TARGET_DAMAGE"] + $param["BONUS_DAMAGE"];
            $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGETOT"];
            
            // Si l'adversaire possède une bulle de vie
            SpellFactory::PassiveBubble($protecteur, $param, $db);
            
            $param["TARGET_MM"] = $protecteurModif->getScore("magicSkill");
            $param["ARMOR_BONUS"] = 10 + 2 * floor(1 + $param["TARGET_MM"] / 8);
            if ($ident == ABILITY_THRUST)
                $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - floor($protecteurModif->getScore("armor") * 0.2 * (1 + $param["ARMOR_BONUS"] / 100)), 0);
            else
                $param["TARGET_HP"] = max(
                    $param["TARGET_DAMAGETOT2"] - ($protecteurModif->getScore("armor") + floor($protecteurModif->getScore("armor") * $param["ARMOR_BONUS"] / 100)), 0);
            
            $hp = $protecteur->get("currhp") - $param["TARGET_HP"];
            $param["XP"] += 1;
            PlayerFactory::Enhance($playerSrc, ATTACK_EVENT_SUCCESS, $param, $db);
            if ($param["TARGET_HP"] > 0)
                PlayerFactory::Enhance($protecteur, DEFENSE_EVENT_FAIL, $param, $db);
                
                // Mort de la cible
            if ($hp <= 0) {
                self::kill($playerSrc, $protecteur, $param, $db);
                $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_DEATH;
            } else {
                $param["CANCEL"] = 0;
                if ($ident == ABILITY_KNOCKOUT) {
                    $param["CURSE"] = floor($param["TARGET_HP"] / 10);
                    // Si l'opposant est sous bénédiction
                    $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $protecteur->get("id"), $db);
                    if (! $dbc->eof()) {
                        $param["LEVEL_BENE"] = $dbc->get("level");
                        if ($param["LEVEL_BENE"] >= floor($param["TARGET_HP"] / 10))
                            $param["CANCEL"] = 1;
                        else {
                            $param["CANCEL"] = 0;
                            $param["CURSE"] = floor($param["TARGET_HP"] / 10) - $param["LEVEL_BENE"];
                        }
                    }
                    if (! $param["CANCEL"]) {
                        $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $protecteur->get("id") . " AND name='Blessure Mortelle'", $db);
                        if (! $dbbm->eof())
                            $dbu = new DBCollection("UPDATE BM SET level=level+" . $param["CURSE"] . " WHERE id_Player=" . $protecteur->get("id") . " AND name='Blessure Mortelle'", 
                                $db, 0, 0, false);
                        else
                            $dbi = new DBCollection(
                                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (2," . $protecteur->get("id") . "," .
                                     $playerSrc->get("id") . "," . $param["CURSE"] . ", 'NEGATIVE', 'Blessure Mortelle' ,-2,'" . $date . "')", $db, 0, 0, false);
                    }
                }
                
                if ($ident == ABILITY_STUNNED && floor($param["TARGET_HP"] / 10) > 0) {
                    $param["CURSE"] = floor($param["TARGET_HP"] / 10);
                    // Si l'opposant est sous bénédiction
                    $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $protecteur->get("id"), $db);
                    if (! $dbc->eof()) {
                        $param["LEVEL_BENE"] = $dbc->get("level");
                        if ($param["LEVEL_BENE"] >= floor($param["TARGET_HP"] / 10))
                            $param["CANCEL"] = 1;
                        else {
                            $param["CANCEL"] = 0;
                            $param["CURSE"] = floor($param["TARGET_HP"] / 10) - $param["LEVEL_BENE"];
                        }
                    }
                    if (! $param["CANCEL"]) {
                        $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $protecteur->get("id") . " AND name='Assommé'", $db);
                        if (! $dbbm->eof())
                            $dbu = new DBCollection(
                                "UPDATE BM SET life=2, level=level+" . floor($param["TARGET_HP"] / 10) . " WHERE id_Player=" . $protecteur->get("id") . " AND name='Assommé'", $db, 
                                0, 0, false);
                        else
                            $dbi = new DBCollection(
                                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (1," . $protecteur->get("id") . "," .
                                     $playerSrc->get("id") . "," . floor($param["TARGET_HP"] / 10) . ", 'NEGATIVE', 'Assommé' ,2,'" . $date . "')", $db, 0, 0, false);
                    }
                }
                if ($ident == ABILITY_PROJECTION) {
                    self::manageProjection($playerSrc, $protecteur, $param, $db);
                }
                
                if ($ident == ABILITY_THRUST && floor($param["TARGET_HP"] / 5) > 0) {
                    $param["CURSE"] = floor($param["TARGET_HP"] / 5);
                    // Si l'opposant est sous bénédiction
                    $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $protecteur->get("id"), $db);
                    $param["CANCEL"] = 0;
                    if (! $dbc->eof()) {
                        $param["LEVEL_BENE"] = $dbc->get("level");
                        if ($param["LEVEL_BENE"] >= floor($param["TARGET_HP"] / 5))
                            $param["CANCEL"] = 1;
                        else {
                            $param["CANCEL"] = 0;
                            $param["CURSE"] = floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"];
                        }
                    }
                    if (! $param["CANCEL"]) {
                        $dbi = new DBCollection(
                            "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $protecteur->get("id") . "," .
                                 $playerSrc->get("id") . "," . $param["CURSE"] . "," . $param["CURSE"] . ", 'NEGATIVE', 'Blessure Profonde' ,3,'" . $date . "')", $db, 0, 0, false);
                    }
                }
                
                if ($ident == M_POISON) {
                    $param["CURSE"] = mt_rand($playerSrc->get("level") / 2, $playerSrc->get("level"));
                    $param["LEVEL_CURSE"] = floor($param["CURSE"] / 3);
                    // Si l'opposant est sous bénédiction
                    $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $protecteur->get("id"), $db);
                    if (! $dbc->eof()) {
                        $param["CANCEL"] = 0;
                        $param["LEVEL_BENE"] = $dbc->get("level");
                        $param["LEVEL_CURSE"] -= $param["LEVEL_BENE"];
                        if ($param["LEVEL_CURSE"] <= 0)
                            $param["CANCEL"] = 1;
                    }
                    if (! $param["CANCEL"])
                        $dbi = new DBCollection(
                            "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $protecteur->get("id") . "," .
                                 $playerSrc->get("id") . "," . $param["LEVEL_CURSE"] . "," . ($param["LEVEL_CURSE"] * 3) . ", 'NEGATIVE', 'Poison' ,3,'" . $date . "')", $db, 0, 0, 
                                false);
                }
                
                $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_SUCCESS;
                PlayerFactory::loseHP($protecteur, $playerSrc, $param["TARGET_HP"], $param, $db);
            }
        }
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        PlayerFactory::initBM($protecteur, $protecteur->getObj("Modifier"), $param, $db);
        $protecteur->updateDBr($db);
    }

    static function interpositionArchery($playerSrc, $protecteur, $id_Arrow, &$param, $db, $modifierA, $modifierB, $ident, $opponent)
    {
        self::autoHustleInterposition($protecteur, $opponent, $db);
        
        self::globalInfo($playerSrc, $param);
        self::globalInfoOpponent($protecteur, $param);
        $param["NAME"] = $opponent->get("name");
        $param["INTER_ID"] = $opponent->get("id");
        $param["TYPE_ACTION"] = ARCHERY;
        $param["IDENT"] = $ident;
        $param["PROTECTION"] = 1;
        $date = gmdate("Y-m-d H:i:s");
        
        // Création clone et application des modifiers
        $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
        $protecteurModif = clone ($protecteur->getObj("Modifier"));
        $charac = array(
            "attack",
            "damage",
            "armor"
        );
        foreach ($charac as $key => $name) {
            for ($type = 0; $type < DICE_CHARAC; $type ++) {
                if ($modifierA != null)
                    $playerSrcModif->addModif($name, $type, $modifierA->getModif($name, $type));
                
                if ($modifierB != null)
                    $protecteurModif->addModif($name, $type, $modifierB->getModif($name, $type));
            }
        }
        
        // Tir réussi
        $param["BONUS_DAMAGE"] = 0;
        $param["TARGET_DEFENSE"] = 0;
        $param["TARGET_DAMAGETOT2"] = 0;
        $param["TARGET_DAMAGE"] = 0;
        $param["TARGET_HP"] = 0;
        $param["BUBBLE"] = 0;
        
        // Ajout de la flèche dans l'équipement de la cible pour récupération avec proba 1/2
        $chance = mt_rand(0, 1);
        // if($opponent->get("status") == 'NPC' && $chance)
        // $dbc = new DBCollection("UPDATE Equipment SET id_Player=".$opponent->get("id")." WHERE id=".$id_Arrow, $db,0,0,false);
        // else
        // $dbc = new DBCollection("DELETE FROM Equipment WHERE id=".$id_Arrow, $db,0,0,false);
        
        $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("attack");
        
        $param["TARGET_MM"] = $protecteurModif->getScore("magicSkill");
        $param["ARMOR_BONUS"] = $protecteurModif->getScore("armor") + floor($protecteurModif->getScore("armor") * (2 * floor(1 + $param["TARGET_MM"] / 8) / 100));
        
        $param["BONUS_DAMAGE"] = self::getDamageArchery($param["PLAYER_ATTACK"], $param["TARGET_DEFENSE"], $ident);
        $param["TARGET_DAMAGETOT2"] = $playerSrcModif->getScore("damage") + $param["BONUS_DAMAGE"];
        $param["TARGET_DAMAGE"] = $param["TARGET_DAMAGETOT2"];
        
        // Si l'adversaire possède une bulle de vie
        $param["BUBBLE"] = SpellFactory::PassiveBubble($protecteur, $param, $db);
        
        $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - $param["ARMOR_BONUS"], 0);
        
        $hp = $protecteur->get("currhp") - $param["TARGET_HP"];
        $param["XP"] += 1;
        PlayerFactory::Enhance($playerSrc, ARCHERY_EVENT_SUCCESS, $param, $db);
        if ($param["TARGET_HP"] > 0)
            PlayerFactory::Enhance($protecteur, DEFENSE_EVENT_FAIL, $param, $db);
            
            // Mort de la cible
        if ($hp <= 0) {
            self::kill($playerSrc, $protecteur, $param, $db);
            $param["TYPE_ATTACK"] = ATTACK_INTER_EVENT_DEATH;
        } else {
            if ($ident == ABILITY_DISABLING) {
                if ($param["TARGET_HP"] > 0) {
                    $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $protecteur->get("id") . " AND name='Blessure gênante'", $db);
                    if (! $dbbm->eof())
                        $dbu = new DBCollection("UPDATE BM SET life=1 WHERE id_Player=" . $protecteur->get("id") . " AND name='Blessure gênante'", $db, 0, 0, false);
                    else
                        $dbi = new DBCollection(
                            "INSERT INTO BM ( id_Player,id_Player\$src, effect, name,life,date) VALUES (" . $protecteur->get("id") . "," . $playerSrc->get("id") .
                                 ", 'NEGATIVE', 'Blessure gênante' ,1,'" . $date . "')", $db, 0, 0, false);
                }
            }
            if ($ident == ABILITY_NEGATION) {
                if ($param["TARGET_HP"] > 0) {
                    $param["MAGICSKILL"] = $playerSrcModif->getScore("magicSkill");
                    $level = 1 + floor($param["MAGICSKILL"] / 8);
                    $param["LEVEL_SPELL"] = $level;
                    $param["CURSE"] = $level;
                    $param["CANCEL"] = 0;
                    // Si l'opposant est sous bénédiction
                    $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $protecteur->get("id"), $db);
                    if (! $dbc->eof()) {
                        $param["LEVEL_BENE"] = $dbc->get("level");
                        if ($param["LEVEL_BENE"] >= $param["CURSE"])
                            $param["CANCEL"] = 1;
                        else
                            $param["CURSE"] -= $param["LEVEL_BENE"];
                    }
                    if (! $param["CANCEL"]) {
                        $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $protecteur->get("id") . " AND name='Flèche de négation'", $db);
                        if (! $dbbm->eof())
                            $dbu = new DBCollection(
                                "UPDATE BM SET life=1, level=level+" . $param["CURSE"] . " WHERE id_Player=" . $protecteur->get("id") . " AND name='flèche de négation'", $db, 0, 0, 
                                false);
                        else
                            $dbi = new DBCollection(
                                "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src, level, effect, name,life,date) VALUES (3, " . $protecteur->get("id") . "," .
                                     $playerSrc->get("id") . "," . $param["CURSE"] . ", 'NEGATIVE', 'Flèche de négation' ,1,'" . $date . "')", $db, 0, 0, false);
                    }
                }
            }
        }
        
        $param["TYPE_ATTACK"] = ARCHERY_INTER_EVENT_SUCCESS;
        PlayerFactory::loseHP($protecteur, $playerSrc, $param["TARGET_HP"], $param, $db);
        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
        PlayerFactory::initBM($protecteur, $protecteur->getObj("Modifier"), $param, $db);
        $protecteur->updateDBr($db);
        $playerSrc->updateDBr($db);
    }

    static function attackBuilding($playerSrc, $building, $typeAttack, &$param, &$db)
    {
        self::appear($playerSrc, $param, $db);
        self::globalInfo($playerSrc, $param);
        
        $param["BUILDING_NAME"] = $building->get("name");
        $param["BUILDING_LEVEL"] = $building->get("level");
        $param["BUILDING_HP"] = $building->get("currsp");
        $param["ATTACK"] = $typeAttack;
        $param["BUILDING_DEATH"] = 0;
        
        if ($typeAttack == 1) {
            $param["TYPE_ACTION"] = NORMAL_ATTACK_BUILDING;
            $param["BUILDING_DAMAGE"] = $playerSrc->getScore("damage");
            PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0)
                $playerSrc->setSub("Upgrade", "strength", $playerSrc->getSub("Upgrade", "strength") + 2);
        } else {
            $param["TYPE_ACTION"] = MAGICAL_ATTACK_BUILDING;
            $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
            $param["PLAYER_DEX"] = $playerSrc->getScore("dexterity");
            $param["SPELL_LEVEL"] = ceil(sqrt($param["PLAYER_DEX"] * $param["PLAYER_MM"]) / 4);
            $modifier = new Modifier();
            $modifier->addModif("damage", DICE_D6, $param["SPELL_LEVEL"] * 1.5);
            $param["BUILDING_DAMAGE"] = floor($modifier->getScoreWithNoBM("damage"));
            if ($playerSrc->get("status") == "PC" && $playerSrc->get("modeAnimation") == 0) {
                $playerSrc->setSub("Upgrade", "magicSkill", $playerSrc->getSub("Upgrade", "magicSkill") + 1);
                $playerSrc->setSub("Upgrade", "dexterity", $playerSrc->getSub("Upgrade", "dexterity") + 1);
            }
            if (PlayerFactory::getTypeMagicArm($playerSrc, $db) > 0) {
                PlayerFactory::ReduceArmDurability($playerSrc, $param, $db);
            }
        }
        
        $dbc = new DBCollection("SELECT captured,id_Player as governor FROM City WHERE id=" . $building->get("id_City"), $db);
        if ($dbc->count() > 0 && $dbc->get("captured") <= 3 && $building->get("id_BasicBuilding") != 15 && $building->get("id_BasicBuilding") != 27) {
            $param["BUILDING_DAMAGE"] = 0;
        }
        $dest = array();
        if ($building->get("id_Player") != 0) {
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $building->get("id_Player"), $db);
            $dest[$dbp->get("name")] = $dbp->get("name");
        } else {
            if ($dbc->get("captured") > 3) {
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("governor"), $db);
                $dest = array();
                $dest[$dbp->get("name")] = $dbp->get("name");
            }
        }
        
        if ($param["BUILDING_DAMAGE"] >= $building->get("currsp")) {
            $body = "Votre bâtiment : " . $building->get("name") . " (" . $building->get("id") . ") situé en " . $building->get("x") . "/" . $building->get("y") . " a été détruit";
            MailFactory::sendMsg($playerSrc, localize("Bâtiment détruit"), $body, $dest, 2, $err, $db, - 1, 0);
            $playerSrc->updateDBr($db);
            self::collapseBuilding($building, $playerSrc, $db);
            // en cas de suicide si le joueur detruit le batiment en étant à l'intérieur
            $playerSrc->reload($db);
            
            if ($building->get("name") == "Rempart" or $building->get("id_BasicBuilding") == 21 or $building->get("id_BasicBuilding") == 26) {
                $building->set("progress", 0);
                $building->set("currsp", 0);
                $building->set("repair", 0);
                $building->updateDB($db);
            } else
                $building->deleteDB($db);
            $param["TYPE_ATTACK"] = ATTACK_BUILDING_EVENT_DEATH;
            $param["BUILDING_DEATH"] = 1;
        } else {
            $body = "Votre bâtiment : " . $building->get("name") . " (" . $building->get("id") . ") situé en " . $building->get("x") . "/" . $building->get("y") . " a été endommagé";
            MailFactory::sendMsg($playerSrc, localize("Bâtiment endommagé"), $body, $dest, 2, $err, $db, - 1, 0);
            $building->set("currsp", $building->get("currsp") - $param["BUILDING_DAMAGE"]);
            $building->updateDB($db);
            $param["TYPE_ATTACK"] = ATTACK_BUILDING_EVENT_SUCCESS;
        }
        
        // Enhance
        $playerSrc->updateDBr($db);
        $playerSrc->updateHidden($db);
        return ACTION_NO_ERROR;
    }

    static function collapseBuilding($building, $agg, $db)
    {
        $date = gmdate('Y-m-d H:i:s');
        $dbp = new DBCollection("SELECT * FROM Player WHERE inbuilding=" . $building->get("id"), $db);
        $hp = floor($building->get("sp") / 10);
        $param["HP"] = $hp;
        while (! $dbp->eof()) {
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($dbp->get("id"), $db);
            $player->set("inbuilding", 0);
            $player->set("room", 0);
            self::globalInfo($player, $param);
            $param["ERROR"] = 0;
            if ($player->get("currhp") <= $hp) {
                $param["DEATH"] = 1;
                $param["TYPE_ACTION"] = BUILDING_COLLAPSE_DEATH;
                $param["TYPE_ATTACK"] = BUILDING_COLLAPSE_DEATH_EVENT;
                Event::logAction($db, BUILDING_COLLAPSE_DEATH, $param);
                if ($player->get("status") == "PC" && $player->get("authlevel") >= 0 || ($player->get("id_BasicRace") == 5 && $player->get("racename") != "Garde du Palais")) {
                    PlayerFactory::resurrection($player, $param, $db);
                } else {
                    PNJFactory::DeleteDBnpc($player, $param, $db, null);
                }
                $player->updateDB($db);
            } else {
                $param["DEATH"] = 0;
                $param["TYPE_ACTION"] = BUILDING_COLLAPSE;
                $param["TYPE_ATTACK"] = BUILDING_COLLAPSE_EVENT;
                Event::logAction($db, BUILDING_COLLAPSE, $param);
                $player->set("currhp", $player->get("currhp") - $hp);
                $player->updateDB($db);
            }
            $dbp->next();
        }
        
        // dropping objets and money on ground
        $dbe = new DBCollection(
            "update Equipment set room=0,inbuilding=0 WHERE state='Ground' AND map=" . $building->get("map") . " and x=" . $building->get("x") . " AND y=" . $building->get("y"), 
            $db, 0, 0, false);
        $dbe = new DBCollection(
            "SELECT id,po FROM Equipment WHERE state='Ground' AND map=" . $building->get("map") . " AND x=" . $building->get("x") . " AND y=" . $building->get("y") . " AND po != 0", 
            $db);
        $moneyGround = 0;
        while (! $dbe->eof()) {
            $moneyGround = $moneyGround + $dbe->get("po");
            $dbe->next();
        }
        if ($moneyGround > 0) {
            $dbe = new DBCollection(
                "delete FROM Equipment WHERE state='Ground' AND map=" . $building->get("map") . " AND x=" . $building->get("x") . " AND y=" . $building->get("y") .
                     "  AND name='' and po != 0", $db, 0, 0, false);
            $dbe = new DBCollection(
                "INSERT INTO Equipment (state,x,y,map,po,dropped,room,inbuilding) VALUES ('Ground'," . $building->get("x") . "," . $building->get("y") . "," . $building->get("map") .
                     "," . $moneyGround . ",'" . $date . "',0,0)", $db, 0, 0, false);
        }
        
        $dbcM = new DBCollection("select money from City WHERE id=" . $building->get("id_City"), $db);
        
        if ($building->get("money") > 0 || ($building->get("id_BasicBuilding") == 11 && $dbcM->get("money") > 0)) {
            $money = $building->get("money");
            $dbe = new DBCollection(
                "SELECT id,po FROM Equipment WHERE name='' and state='Ground' AND map=" . $building->get("map") . " AND x=" . $building->get("x") . " AND y=" . $building->get("y") .
                     " AND po > 0", $db);
            
            if ($building->get("id_BasicBuilding") == 11 && $dbcM->get("money") > 0) {
                $money += $dbcM->get("money");
                $dbcMu = new DBCollection("UPDATE City set money=0 WHERE id=" . $building->get("id_City"), $db, 0, 0, false);
            }
            
            if (! $dbe->eof()) {
                $money = $money + $dbe->get("po");
                $dbe = new DBCollection("UPDATE Equipment SET dropped='" . $date . "',room=0,inbuilding=0 , po=" . $money . " WHERE id=" . $dbe->get("id"), $db, 0, 0, false);
            } else {
                $dbe = new DBCollection(
                    "INSERT INTO Equipment (state,x,y,po,map,dropped,room,inbuilding) VALUES ('Ground'," . $building->get("x") . "," . $building->get("y") . "," . $money . "," .
                         $building->get("map") . ",'" . $date . "',0,0)", $db, 0, 0, false);
            }
        }
        // it is a shop, choose ramdomly 3 objets and drop on ground, others are deleted
        if ($building->get("id_BasicBuilding") == 5) {
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Shop = " . $building->get("id") . " order by rand()", $db);
            $i = 0;
            while (! $dbe->eof()) {
                if ($i < 3) {
                    $dbt = new DBCollection(
                        "UPDATE Equipment SET dropped='" . $date . "',room=0,inbuilding=0 ,id_shop = 0,state='Ground',x=" . $building->get("x") . ",y=" . $building->get("y") .
                             ",map=" . $building->get("map") . " WHERE id_Shop = " . $building->get("id") . " and id=" . $dbe->get("id"), $db, 0, 0, false);
                    $i ++;
                } else {
                    $dbt = new DBCollection("delete FROM Equipment WHERE id_Shop = " . $building->get("id") . " and id=" . $dbe->get("id"), $db, 0, 0, false);
                }
                $dbe->next();
            }
        }
        // c'est un entrepôt des objets tombent au sol
        // C'est une guilde les objets fabriqués ou en cours de fabrication tombent au sol
        if ($building->get("id_BasicBuilding") == 28 || $building->get("id_BasicBuilding") == 9) {
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE id_Shop = " . $building->get("id") . " order by rand()", $db);
            while (! $dbe->eof()) {
                $dbt = new DBCollection(
                    "UPDATE Equipment SET dropped='" . $date . "',room=0,inbuilding=0 ,id_shop = 0,state='Ground',x=" . $building->get("x") . ",y=" . $building->get("y") . ",map=" .
                         $building->get("map") . " WHERE id_Shop = " . $building->get("id") . " and id=" . $dbe->get("id"), $db, 0, 0, false);
                $dbe->next();
            }
        }
        // C'est un comptoir commercial, les objets non récupérés tombent au sol
        if ($building->get("id_BasicBuilding") == 4) {
            $dbe = new DBCollection(
                "SELECT Equipment.*,Player.name as playerName FROM Equipment left outer join Player on Player.id = Equipment.id_Caravan WHERE Equipment.id_Shop = " .
                     $building->get("id") . " ", $db);
            while (! $dbe->eof()) {
                $dest = array();
                if ($dbe->get("playerName") != "") {
                    $playerSrc = new Player();
                    $playerSrc->load(1, $db);
                    $dest[$dbe->get("playerName")] = $dbe->get("playerName");
                    $body = "Le comptoir commercial : " . $building->get("name") . " situé en " . $building->get("x") . "/" . $building->get("y") . " a été détruit.\n";
                    $body .= "L'objet " . $dbe->get("name") . " niveau " . $dbe->get("level") . " " . $dbe->get("extraname") . " que vous aviez envoyé tombe au sol.";
                    MailFactory::sendMsg($playerSrc, localize("Comptoir commercial détruit"), $body, $dest, 2, $err, $db, - 1, 0);
                }
                $dbt = new DBCollection(
                    "UPDATE Equipment SET dropped='" . $date . "',room=0,inbuilding=0 ,id_shop = 0,id_Warehouse=0,id_Caravan=0,state='Ground',po=0,x=" . $building->get("x") . ",y=" .
                         $building->get("y") . ",map=" . $building->get("map") . " WHERE id_Shop = " . $building->get("id") . " and id=" . $dbe->get("id"), $db, 0, 0, false);
                $dbe->next();
            }
        }
        // c'est un palais du gouverneur, le village n'est plus capturé.
        if ($building->get("id_BasicBuilding") == 11) {
            $dbu = new DBCollection(
                "UPDATE City SET id_Player=0, captured=(case when captured=4 then 3 else 0 end), door=1, accessTemple=1,accessEntrepot=1 WHERE id=" . $building->get("id_City"), $db, 
                0, 0, false);
            // le palais est detruit on supprime le prêtre
            self::killPriest($building->get("id_City"), $db);
            // le palais est detruit on supprime le garde
            self::killGuard($building->get("id_City"), $db);
            $dbu = new DBCollection("UPDATE Building SET id_BasicBuilding=26, name='Porte Ouverte' WHERE id_BasicBuilding=21 AND id_City=" . $building->get("id_City"), $db, 0, 0, 
                false);
        }
    }
    
    // Mort du prêtre
    static function killPriest($id_City, $db)
    {
        $dbp = new DBCollection("SELECT * FROM Player WHERE racename='Prêtre' AND id_City=" . $id_City, $db);
        
        if (! $dbp->eof()) {
            $priest = new Player();
            $priest->load($dbp->get("id"), $db);
            PNJFactory::DeleteDBnpc($priest, $param, $db, null);
        }
    }
    
    // Mort du garde
    static function killGuard($id_City, $db)
    {
        $dbp = new DBCollection("SELECT * FROM Player WHERE racename='Garde du Palais' AND id_City=" . $id_City, $db);
        if (! $dbp->eof()) {
            $gard = new Player();
            $gard->load($dbp->get("id"), $db);
            PNJFactory::DeleteDBnpc($gard, $param, $db, null);
        }
    }

    private static function manageProjection(&$playerSrc, &$opponent, &$param, $db)
    {
        $strengthPlayersrc = floor($playerSrc->getScore("strength") + ($playerSrc->getScore("dexterity") * 1 / 2));
        $strengthOpponent = floor($opponent->getScore("strength") + ($opponent->getScore("dexterity") * 1 / 2));
        
        if ($strengthPlayersrc > $strengthOpponent) {
            
            $xp = $playerSrc->get("x");
            $yp = $playerSrc->get("y");
            $xo = $opponent->get("x");
            $yo = $opponent->get("y");
            $movex = $xp > $xo ? 1 : ($xp == $xo ? 0 : - 1);
            $movey = $yp > $yo ? 1 : ($yp == $yo ? 0 : - 1);
            require_once (HOMEPATH . "/lib/MapInfo.inc.php");
            $mapinfo = new MapInfo($db);
            $validzone = $mapinfo->getValidMap($xo - $movex, $yo - $movey, 0, $playerSrc->get("map"));
            $param["MOVE1"] = 0;
            $param["MOVE2"] = 0;
            $resultFreeplace = self::freePlaceAllInfos($xo - $movex, $yo - $movey, $playerSrc->get("map"), $db);
            if ($validzone[0][0] && ($resultFreeplace["place"] == 1 || ($resultFreeplace["type"] != "Building" && $resultFreeplace["type"] != "Player"))) {
                $param["MOVE1"] = 1;
                $opponent->set("x", $opponent->get("x") - $movex);
                $opponent->set("y", $opponent->get("y") - $movey);
                
                $validzone = $mapinfo->getValidMap($xo - 2 * $movex, $yo - 2 * $movey, 0, $playerSrc->get("map"));
                $resultFreeplace = self::freePlaceAllInfos($xo - 2 * $movex, $yo - 2 * $movey, $playerSrc->get("map"), $db);
                if ($validzone[0][0] && $param["MOVE1"] && ($resultFreeplace["place"] == 1 || ($resultFreeplace["type"] != "Building" && $resultFreeplace["type"] != "Player"))) {
                    $param["MOVE2"] = 1;
                    $opponent->set("x", $opponent->get("x") - $movex);
                    $opponent->set("y", $opponent->get("y") - $movey);
                }
            }
            if ($resultFreeplace["place"] == 0) {
                $typeElement = $resultFreeplace["type"];
                $typeId = $resultFreeplace["id"];
                
                if ($typeElement == "Building") {
                    $bObstacle = new Building();
                    $bObstacle->load($typeId, $db);
                    $param["NAME_BUILDING"] = $bObstacle->get("name");
                    $param["BUILDING_ID"] = $bObstacle->get("id");
                    
                    self::globalInfo($opponent, $param3);
                    $param3["NAME_BUILDING"] = $bObstacle->get("name");
                    $param3["BUILDING_ID"] = $bObstacle->get("id");
                    
                    $param["TYPE_ACTION_PJO"] = PASSIVE_PROJECTION_BUILDING;
                    $date = gmdate("Y-m-d H:i:s");
                    $param3["TYPE_ACTION"] = PASSIVE_PROJECTION_BUILDING;
                    $param3["TYPE_ATTACK"] = PASSIVE_PROJECTION_BUILDING_EVENT;
                    $param["OPP_EXTRA_DAMAGE"] = 0;
                    $param["BUILDING_EXTRA_DAMAGE"] = 0;
                    $opponentDamage = floor($param["TARGET_DAMAGETOT2"] * 0.3);
                    
                    // Si l'adversaire possède une bulle de vie
                    $param4 = array();
                    $param4["TARGET_DAMAGETOT2"] = $opponentDamage;
                    $param3["BUBBLE_OPP"] = 0;
                    $param3["BUBBLE_OPP"] = SpellFactory::PassiveBubble($opponent, $param4, $db);
                    if ($param3["BUBBLE_OPP"] > 0) {
                        $param3["BUBBLE_CANCEL_OPP"] = $param4["BUBBLE_CANCEL"];
                        $param3["BUBBLE_LIFE_OPP"] = $param4["BUBBLE_LIFE"];
                        $param3["BUBBLE_LEVEL_OPP"] = $param4["BUBBLE_LEVEL"];
                    }
                    $opponentDamage = $param4["TARGET_DAMAGETOT2"];
                    unset($param4);
                    
                    $damageBuilding = floor($opponent->get("hp") / 3);
                    
                    $dbc = new DBCollection("SELECT captured,id_Player as governor FROM City WHERE id=" . $bObstacle->get("id_City"), $db);
                    if ($dbc->count() > 0 && $dbc->get("captured") <= 3 && $bObstacle->get("id_BasicBuilding") != 27) {
                        $damageBuilding = 0;
                    }
                    
                    $dest = array();
                    if ($bObstacle->get("id_Player") != 0) {
                        $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $bObstacle->get("id_Player"), $db);
                        $dest[$dbp->get("name")] = $dbp->get("name");
                    } else {
                        if ($dbc->get("captured") > 3) {
                            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $dbc->get("governor"), $db);
                            $dest = array();
                            $dest[$dbp->get("name")] = $dbp->get("name");
                        }
                    }
                    
                    $hpOpponent = $opponent->get("currhp") - $opponentDamage;
                    $spBuilding = $bObstacle->get("currsp") - $damageBuilding;
                    
                    $param["OPP_EXTRA_DAMAGE"] = $opponentDamage;
                    $param["BUILDING_EXTRA_DAMAGE"] = $damageBuilding;
                    
                    $param3["DAMAGE_OPP"] = $opponentDamage;
                    $param3["DAMAGE_OBST"] = $damageBuilding;
                    
                    if ($hpOpponent <= 0) {
                        $param4 = array();
                        self::kill($playerSrc, $opponent, $param4, $db);
                        $opponent->reload($db);
                        $param3["TYPE_ACTION"] = PASSIVE_PROJECTION_BUILDING;
                        $param3["TYPE_ATTACK"] = PASSIVE_PROJECTION_DEATH_BUILDING_EVENT;
                        $param3["OPP_KILLED"] = 1;
                        if ($param4["TARGET_KILLED"]) {
                            $param["NOMBRE"] = $param4["NOMBRE"];
                            for ($j = 1; $j < $param4["NOMBRE"]; $j ++) {
                                $param["PLAYER" . $j] = $param4["PLAYER" . $j];
                                if (isset($param["GAIN" . $j]))
                                    $param["GAIN" . $j] += $param4["GAIN" . $j];
                                else
                                    $param["GAIN" . $j] = $param4["GAIN" . $j];
                            }
                            // Param pour le fallmsg
                            if (isset($param4["EQUIPMENT_FALL"]))
                                $param["EQUIPMENT_FALL"] = $param4["EQUIPMENT_FALL"];
                            if (isset($param4["MONEY_FALL"]))
                                $param["MONEY_FALL"] = $param4["MONEY_FALL"];
                            if (isset($param4["DROP_NAME"]))
                                $param["DROP_NAME"] = $param4["DROP_NAME"];
                            if (isset($param4["LARCIN"]))
                                $param["LARCIN"] = $param4["LARCIN"];
                        }
                        
                        unset($param4);
                    } else {
                        $param["CANCEL"] = 0;
                        $param3["OPP_KILLED"] = 0;
                        PlayerFactory::loseHP($opponent, $playerSrc, $opponentDamage, $param3, $db);
                    }
                    
                    if ($spBuilding <= 0) {
                        $body = "Votre bâtiment : " . $bObstacle->get("name") . " (" . $bObstacle->get("id") . ") situé en " . $bObstacle->get("x") . "/" . $bObstacle->get("y") .
                             " a été détruit";
                        MailFactory::sendMsg($playerSrc, localize("Bâtiment détruit"), $body, $dest, 2, $err, $db, - 1, 0);
                        $playerSrc->updateDBr($db);
                        self::collapseBuilding($bObstacle, $playerSrc, $db);
                        
                        if ($bObstacle->get("name") == "Rempart" or $bObstacle->get("id_BasicBuilding") == 21 or $bObstacle->get("id_BasicBuilding") == 26) {
                            $bObstacle->set("progress", 0);
                            $bObstacle->set("currsp", 0);
                            $bObstacle->set("repair", 0);
                            $bObstacle->updateDB($db);
                        } else
                            $bObstacle->deleteDB($db);
                        $param3["TYPE_ATTACK"] = PASSIVE_PROJECTION_DESTROY_BUILDING_EVENT;
                        $param3["OBST_KILLED"] = 1;
                    } else {
                        if ($damageBuilding > 0) {
                            $body = "Votre bâtiment : " . $bObstacle->get("name") . " (" . $bObstacle->get("id") . ") situé en " . $bObstacle->get("x") . "/" . $bObstacle->get("y") .
                                 " a été endommagé";
                            MailFactory::sendMsg($playerSrc, localize("Bâtiment endommagé"), $body, $dest, 2, $err, $db, - 1, 0);
                            $bObstacle->set("currsp", $spBuilding);
                            $bObstacle->updateDB($db);
                        }
                    }
                    
                    PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                    $opponent->updateDBr($db);
                    $param3["ERROR"] = 0;
                    Event::logAction($db, $param3["TYPE_ACTION"], $param3);
                }
                if ($typeElement == "Player") {
                    $pObstacle = new Player();
                    $pObstacle->externDBObj("Modifier");
                    $pObstacle->load($typeId, $db);
                    
                    if ($pObstacle->get("hidden") % 10 != 2) { // pour souffle de la négation
                        
                        self::appear($pObstacle, $param, $db);
                        $param["NAME_PJO"] = $pObstacle->get("name");
                        $param["PJO_ID"] = $pObstacle->get("id");
                        $param["TYPE_ACTION_PJO"] = PASSIVE_PROJECTION;
                        
                        $date = gmdate("Y-m-d H:i:s");
                        
                        self::globalInfo($opponent, $param3);
                        self::globalInfoOpponent($pObstacle, $param3);
                        // faire résolution deuxième barrière
                        SpellFactory::passiveBarrier($opponent, $pObstacle, $param3, $db);
                        
                        $param["PJO_EXTRA_DAMAGE"] = 0;
                        $param["OPP_EXTRA_DAMAGE"] = 0;
                        
                        if (isset($param3["BARRIER_LEVEL"])) {
                            $param["OPP_EXTRA_DAMAGE"] = $param3["PLAYER_DAMAGE"];
                        }
                        
                        if ($param3["PLAYER_KILLED"]) {
                            $param["TYPE_ATTACK_PJO"] = PASSIVE_PROJECTION;
                            $param["PLAYER_KILLED_PJO"] = 1;
                        } else {
                            $armorTotal = $pObstacle->getScore("armor") + $opponent->getScore("armor");
                            $extendDamage = floor($param["TARGET_DAMAGETOT2"] * 0.3);
                            // calcul des dégâts reçues en fonction de l'armure
                            if ($armorTotal > 0) {
                                $damageObstacle = floor(ceil($opponent->getScore("armor") / $armorTotal) * $extendDamage);
                                $opponentDamage = floor(ceil($pObstacle->getScore("armor") / $armorTotal) * $extendDamage);
                            } else {
                                $damageObstacle = floor(0.5 * $extendDamage);
                                $opponentDamage = floor(0.5 * $extendDamage);
                            }
                            // Si l'obstacle possède une bulle de vie
                            $param4 = array();
                            $param4["TARGET_DAMAGETOT2"] = $damageObstacle;
                            $param3["BUBBLE_OBST"] = 0;
                            $param3["BUBBLE_OBST"] = SpellFactory::PassiveBubble($pObstacle, $param4, $db);
                            if ($param3["BUBBLE_OBST"] > 0) {
                                $param3["BUBBLE_CANCEL_OBST"] = $param4["BUBBLE_CANCEL"];
                                $param3["BUBBLE_LIFE_OBST"] = $param4["BUBBLE_LIFE"];
                                $param3["BUBBLE_LEVEL_OBST"] = $param4["BUBBLE_LEVEL"];
                            }
                            $damageObstacle = $param4["TARGET_DAMAGETOT2"];
                            unset($param4);
                            
                            // Si l'adversaire possède une bulle de vie
                            $param4 = array();
                            $param4["TARGET_DAMAGETOT2"] = $opponentDamage;
                            $param3["BUBBLE_OPP"] = 0;
                            $param3["BUBBLE_OPP"] = SpellFactory::PassiveBubble($opponent, $param4, $db);
                            if ($param3["BUBBLE_OPP"] > 0) {
                                $param3["BUBBLE_CANCEL_OPP"] = $param4["BUBBLE_CANCEL"];
                                $param3["BUBBLE_LIFE_OPP"] = $param4["BUBBLE_LIFE"];
                                $param3["BUBBLE_LEVEL_OPP"] = $param4["BUBBLE_LEVEL"];
                            }
                            $opponentDamage = $param4["TARGET_DAMAGETOT2"];
                            unset($param4);
                            
                            $hpObstacle = $pObstacle->get("currhp") - $damageObstacle;
                            $hpOpponent = $opponent->get("currhp") - $opponentDamage;
                            
                            $param3["DAMAGE_OPP"] = $opponentDamage;
                            $param3["DAMAGE_OBST"] = $damageObstacle;
                            
                            $param["PJO_EXTRA_DAMAGE"] += $damageObstacle;
                            $param["OPP_EXTRA_DAMAGE"] += $opponentDamage;
                            
                            $param3["TYPE_ACTION"] = PASSIVE_PROJECTION;
                            $param3["TYPE_ATTACK"] = PASSIVE_PROJECTION_EVENT;
                            // Mort de la cible
                            if ($hpObstacle <= 0) {
                                $param4 = array();
                                self::kill($playerSrc, $pObstacle, $param4, $db);
                                $pObstacle->reload($db);
                                
                                $param3["TYPE_ACTION"] = PASSIVE_PROJECTION_DEATH_OBST;
                                $param3["TYPE_ATTACK"] = PASSIVE_PROJECTION_DEATH_OBST_EVENT;
                                $param3["OBST_KILLED"] = 1;
                                if ($param4["TARGET_KILLED"]) {
                                    $param["TARGET_KILLED_OBS"] = $param4["TARGET_KILLED"];
                                    $param["NOMBRE"] = $param4["NOMBRE"];
                                    for ($j = 1; $j < $param4["NOMBRE"]; $j ++) {
                                        $param["PLAYER" . $j] = $param4["PLAYER" . $j];
                                        if (isset($param["GAIN" . $j]))
                                            $param["GAIN" . $j] += $param4["GAIN" . $j];
                                        else
                                            $param["GAIN" . $j] = $param4["GAIN" . $j];
                                    }
                                    // Param pour le fallmsg
                                    if (isset($param4["EQUIPMENT_FALL"]))
                                        $param["EQUIPMENT_FALL_OBS"] = $param4["EQUIPMENT_FALL"];
                                    if (isset($param4["MONEY_FALL"]))
                                        $param["MONEY_FALL_OBS"] = $param4["MONEY_FALL"];
                                    if (isset($param4["DROP_NAME"]))
                                        $param["DROP_NAME_OBS"] = $param4["DROP_NAME"];
                                    if (isset($param4["LARCIN"]))
                                        $param["LARCIN_OBS"] = $param4["LARCIN"];
                                }
                                unset($param4);
                            } else {
                                $param["CANCEL"] = 0;
                                PlayerFactory::loseHP($pObstacle, $playerSrc, $damageObstacle, $param3, $db);
                                $param3["OBST_KILLED"] = 0;
                            }
                            if ($hpOpponent <= 0) {
                                $param4 = array();
                                self::kill($playerSrc, $opponent, $param4, $db);
                                $opponent->reload($db);
                                $param3["TYPE_ACTION"] = PASSIVE_PROJECTION_DEATH_OPP;
                                $param3["TYPE_ATTACK"] = PASSIVE_PROJECTION_DEATH_OPP_EVENT;
                                $param3["OPP_KILLED"] = 1;
                                if ($param4["TARGET_KILLED"]) {
                                    $param["NOMBRE"] = $param4["NOMBRE"];
                                    for ($j = 1; $j < $param4["NOMBRE"]; $j ++) {
                                        $param["PLAYER" . $j] = $param4["PLAYER" . $j];
                                        if (isset($param["GAIN" . $j]))
                                            $param["GAIN" . $j] += $param4["GAIN" . $j];
                                        else
                                            $param["GAIN" . $j] = $param4["GAIN" . $j];
                                    }
                                    // Param pour le fallmsg
                                    if (isset($param4["EQUIPMENT_FALL"]))
                                        $param["EQUIPMENT_FALL"] = $param4["EQUIPMENT_FALL"];
                                    if (isset($param4["MONEY_FALL"]))
                                        $param["MONEY_FALL"] = $param4["MONEY_FALL"];
                                    if (isset($param4["DROP_NAME"]))
                                        $param["DROP_NAME"] = $param4["DROP_NAME"];
                                    if (isset($param4["LARCIN"]))
                                        $param["LARCIN"] = $param4["LARCIN"];
                                }
                                
                                unset($param4);
                            } else {
                                $param["CANCEL"] = 0;
                                $param3["OPP_KILLED"] = 0;
                                PlayerFactory::loseHP($opponent, $playerSrc, $opponentDamage, $param3, $db);
                            }
                        }
                        PlayerFactory::addXP($playerSrc, $param["XP"], $db);
                        PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                        PlayerFactory::initBM($pObstacle, $pObstacle->getObj("Modifier"), $param, $db);
                        $opponent->updateDBr($db);
                        $pObstacle->updateDBr($db);
                        $param3["ERROR"] = 0;
                        Event::logAction($db, PASSIVE_PROJECTION, $param3);
                    }
                }
            }
            $opponent->updateHidden($db);
        }
    }

    static function attack($playerSrc, $opponent, &$param, &$db, $modifierA = null, $modifierB = null, $ident = 0)
    {
        self::appear($playerSrc, $param, $db);
        
        // Si l'opposant est sous protection
        $param["PROTECTION"] = 0;
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='défendu'", $db);
        if (! $dbc->eof()) {
            $protecteur = new Player();
            $protecteur->externDBObj("Modifier");
            $protecteur->load($dbc->get("id_Player\$src"), $db);
            if ($protecteur->get("state") != "turtle" && ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 1 &&
                 $protecteur->get("inbuilding") == $opponent->get("inbuilding")) ||
                 ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 0) && $protecteur->get("room") == $opponent->get("room"))))
                $param["PROTECTION"] = 1;
            
            if ($protecteur->get("id") == $playerSrc->get("id"))
                $param["PROTECTION"] = 0;
        }
        
        // Si l'opposant est en mode piège
        $param["PIEGE"] = 0;
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
        if (! $dbc->eof())
            $param["PIEGE"] = 1;
        
        if ($param["PROTECTION"])
            self::interposition($playerSrc, $protecteur, $param, $db, $modifierA, $modifierB, $ident, $opponent);
        elseif ($param["PIEGE"]) {
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
            $opponent->set("currhp", $opponent->get("currhp") + 10 * $dbbm->get("level"));
            $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db, 0, 0, false);
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $opponent->updateDBr($db);
            self::attack($opponent, $playerSrc, $param, $db, null, null, 500);
        } else {
            if ($ident == 500)
                $param["PIEGE"] = 1;
                // Initialisation des params info player et opponent
            self::globalInfo($playerSrc, $param);
            self::globalInfoOpponent($opponent, $param);
            $param["TYPE_ACTION"] = ATTACK;
            $param["IDENT"] = $ident;
            $date = gmdate("Y-m-d H:i:s");
            
            // Création clone et application des modifiers
            $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
            $opponentModif = clone ($opponent->getObj("Modifier"));
            $charac = array(
                "attack",
                "damage",
                "armor"
            );
            foreach ($charac as $key => $name) {
                for ($type = 0; $type < DICE_CHARAC; $type ++) {
                    if ($modifierA != null)
                        $playerSrcModif->addModif($name, $type, $modifierA->getModif($name, $type));
                    
                    if ($modifierB != null)
                        $opponentModif->addModif($name, $type, $modifierB->getModif($name, $type));
                }
            }
            
            $param["BONUS_DAMAGE"] = 0;
            $param["BONUS_DAMAGE"] = 0;
            $param["TARGET_DAMAGETOT2"] = 0;
            $param["TARGET_DAMAGE"] = 0;
            $param["TARGET_HP"] = 0;
            $param["BUBBLE"] = 0;
            
            // Jets de dés
            $param["TARGET_DAMAGE"] = $playerSrcModif->getScore("damage");
            $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("attack");
            
            if ($ident == ABILITY_LIGHT) {
                $param["PLAYER_ATTACK"] = floor($playerSrcModif->getScore("magicSkill") * 1.5);
                $dbc = new DBCollection("SELECT class FROM BasicRace WHERE id=" . $opponent->get("id_BasicRace"), $db);
                if ($dbc->get("class") == "morts-vivant") {
                    $param["UNDEAD"] = 1;
                    $param["TARGET_DAMAGE"] = floor($playerSrcModif->getScore("damage") * 1.3);
                }
            }
            $param["TARGET_DEFENSE"] = $opponentModif->getScore("defense");
            /*
             */
            // SI le joueur a une barrière enflammée
            require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
            SpellFactory::passiveBarrier($playerSrc, $opponent, $param, $db);
            if ($param["PLAYER_KILLED"])
                $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
            else {
                // Attaque réussi
                
                if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                    $param["BONUS_DAMAGE"] = floor(($param["PLAYER_ATTACK"] - $param["TARGET_DEFENSE"]) / 4);
                    $param["BONUS_DAMAGE2"] = $param["BONUS_DAMAGE"];
                    if ($ident == ABILITY_TREACHEROUS) {
                        $param["PLAYER_SPEED"] = $playerSrcModif->getScore("speed");
                        $param["BONUS_DAMAGE"] += ceil($param["BONUS_DAMAGE"] * min(1.2, (0.5 + $param["PLAYER_SPEED"] / 100)));
                    }
                    $param["TARGET_DAMAGETOT"] = $param["TARGET_DAMAGE"] + $param["BONUS_DAMAGE"];
                    $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGETOT"];
                    // Si l'adversaire possède une bulle de vie
                    SpellFactory::PassiveBubble($opponent, $param, $db);
                    
                    $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - $opponentModif->getScore("armor"), 0);
                    
                    $hp = $opponent->get("currhp") - $param["TARGET_HP"];
                    $param["XP"] += 1;
                    PlayerFactory::Enhance($playerSrc, ATTACK_EVENT_SUCCESS, $param, $db, $ident);
                    if ($param["TARGET_HP"] > 0)
                        PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                        // Mort de la cible
                    if ($hp <= 0) {
                        if ($ident == 1000) {
                            $param["TYPE_ACTION"] = ATTACK;
                            $param["TYPE_ATTACK"] = M_STUN_EVENT;
                            $opponent->set("state", "stunned");
                            $opponent->set("currhp", 1);
                        } else
                            self::kill($playerSrc, $opponent, $param, $db);
                    } else {
                        $param["CANCEL"] = 0;
                        if ($ident == ABILITY_KNOCKOUT) {
                            $param["CURSE"] = floor($param["TARGET_HP"] / 10);
                            // Si l'opposant est sous bénédiction
                            $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $opponent->get("id"), $db);
                            if (! $dbc->eof()) {
                                $param["LEVEL_BENE"] = $dbc->get("level");
                                if ($param["LEVEL_BENE"] >= floor($param["TARGET_HP"] / 10))
                                    $param["CANCEL"] = 1;
                                else {
                                    $param["CANCEL"] = 0;
                                    $param["CURSE"] = floor($param["TARGET_HP"] / 10) - $param["LEVEL_BENE"];
                                }
                            }
                            if (! $param["CANCEL"]) {
                                $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Blessure Mortelle'", $db);
                                if (! $dbbm->eof())
                                    $dbu = new DBCollection(
                                        "UPDATE BM SET level=level+" . $param["CURSE"] . " WHERE id_Player=" . $opponent->get("id") . " AND name='Blessure Mortelle'", $db, 0, 0, 
                                        false);
                                else
                                    $dbi = new DBCollection(
                                        "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (2," . $opponent->get("id") . "," .
                                             $playerSrc->get("id") . "," . $param["CURSE"] . ", 'NEGATIVE', 'Blessure Mortelle' ,-2,'" . $date . "')", $db, 0, 0, false);
                            }
                        }
                        
                        if ($ident == ABILITY_STUNNED && floor($param["TARGET_HP"] / 10) > 0) {
                            $param["CURSE"] = floor($param["TARGET_HP"] / 10);
                            // Si l'opposant est sous bénédiction
                            $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $opponent->get("id"), $db);
                            if (! $dbc->eof()) {
                                $param["LEVEL_BENE"] = $dbc->get("level");
                                if ($param["LEVEL_BENE"] >= floor($param["TARGET_HP"] / 10))
                                    $param["CANCEL"] = 1;
                                else {
                                    $param["CANCEL"] = 0;
                                    $param["CURSE"] = floor($param["TARGET_HP"] / 10) - $param["LEVEL_BENE"];
                                }
                            }
                            if (! $param["CANCEL"]) {
                                $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Assommé'", $db);
                                if (! $dbbm->eof())
                                    $dbu = new DBCollection(
                                        "UPDATE BM SET life=2, level=level+" . $param["CURSE"] . " WHERE id_Player=" . $opponent->get("id") . " AND name='Assommé'", $db, 0, 0, 
                                        false);
                                else
                                    $dbi = new DBCollection(
                                        "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, effect, name,life,date) VALUES (1," . $opponent->get("id") . "," .
                                             $playerSrc->get("id") . "," . $param["CURSE"] . ", 'NEGATIVE', 'Assommé' ,2,'" . $date . "')", $db, 0, 0, false);
                            }
                        }
                        
                        if ($ident == ABILITY_THRUST && floor($param["TARGET_HP"] / 5) > 0) {
                            $param["CURSE"] = floor($param["TARGET_HP"] / 5);
                            // Si l'opposant est sous bénédiction
                            $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $opponent->get("id"), $db);
                            if (! $dbc->eof()) {
                                $param["LEVEL_BENE"] = $dbc->get("level");
                                if ($param["LEVEL_BENE"] >= floor($param["TARGET_HP"] / 5))
                                    $param["CANCEL"] = 1;
                                else {
                                    $param["CANCEL"] = 0;
                                    $param["CURSE"] = floor($param["TARGET_HP"] / 5) - $param["LEVEL_BENE"];
                                }
                            }
                            if (! $param["CANCEL"]) {
                                $dbi = new DBCollection(
                                    "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $opponent->get("id") . "," .
                                         $playerSrc->get("id") . "," . $param["CURSE"] . "," . $param["CURSE"] . ", 'NEGATIVE', 'Blessure Profonde' ,3,'" . $date . "')", $db, 0, 0, 
                                        false);
                            }
                        }
                        
                        if ($ident == M_POISON) {
                            $param["CURSE"] = mt_rand($playerSrc->get("level") / 2, $playerSrc->get("level"));
                            $param["LEVEL_CURSE"] = floor($param["CURSE"] / 3);
                            // Si l'opposant est sous bénédiction
                            $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $opponent->get("id"), $db);
                            if (! $dbc->eof()) {
                                $param["CANCEL"] = 0;
                                $param["LEVEL_BENE"] = $dbc->get("level");
                                $param["LEVEL_CURSE"] -= $param["LEVEL_BENE"];
                                if ($param["LEVEL_CURSE"] <= 0)
                                    $param["CANCEL"] = 1;
                            }
                            if (! $param["CANCEL"])
                                $dbi = new DBCollection(
                                    "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src,level, value, effect, name,life,date) VALUES (0," . $opponent->get("id") . "," .
                                         $playerSrc->get("id") . "," . $param["LEVEL_CURSE"] . "," . ($param["LEVEL_CURSE"] * 3) . ", 'NEGATIVE', 'Poison' ,3,'" . $date . "')", $db, 
                                        0, 0, false);
                        }
                        
                        PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_HP"], $param, $db);
                        if ($ident == ABILITY_PROJECTION) {
                            self::manageProjection($playerSrc, $opponent, $param, $db);
                        }
                        $param["TYPE_ATTACK"] = ATTACK_EVENT_SUCCESS;
                        
                        PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                    }
                } else { // Attaque raté
                    if ($playerSrc->get("status") == 'PC')
                        self::legalAction($playerSrc, $opponent, $param, $db, 0);
                    $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
                    PlayerFactory::Enhance($playerSrc, ATTACK_EVENT_FAIL, $param, $db, $ident);
                    PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
                }
            }
            
            if ($playerSrc->get("status") == 'PC')
                PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            
            $opponent->updateDBr($db);
            // PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
            
            $playerSrc->updateDBr($db);
        }
        SpellFactory::removeAnger($playerSrc, 1, $param, $db);
        SpellFactory::removeAnger($playerSrc, 2, $param, $db);
        return ACTION_NO_ERROR;
    }

    static function archery($playerSrc, &$opponent, $id_Arrow, &$param, &$db, $modifierA = null, $modifierB = null, $ident = 0)
    {
        self::appear($playerSrc, $param, $db);
        
        // Si l'opposant est sous protection
        $param["PROTECTION"] = 0;
        $dbc = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='défendu'", $db);
        if (! $dbc->eof()) {
            $protecteur = new Player();
            $protecteur->externDBObj("Modifier");
            $protecteur->load($dbc->get("id_Player\$src"), $db);
            if ($protecteur->get("state") != "turtle" && ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 1 &&
                 $protecteur->get("inbuilding") == $opponent->get("inbuilding")) ||
                 ((distHexa($opponent->get("x"), $opponent->get("y"), $protecteur->get("x"), $protecteur->get("y")) == 0) && $protecteur->get("room") == $opponent->get("room"))))
                $param["PROTECTION"] = 1;
            
            if ($protecteur->get("id") == $playerSrc->get("id"))
                $param["PROTECTION"] = 0;
        }
        
        if ($param["PROTECTION"])
            self::interpositionArchery($playerSrc, $protecteur, $id_Arrow, $param, $db, $modifierA, $modifierB, $ident, $opponent);
        else {
            // Initialisation des params infos player et opponent
            self::globalInfo($playerSrc, $param);
            self::globalInfoOpponent($opponent, $param);
            $param["TYPE_ACTION"] = ARCHERY;
            
            $date = gmdate("Y-m-d H:i:s");
            // Création clone et application des modifier
            $playerSrcModif = clone ($playerSrc->getObj("Modifier"));
            $opponentModif = clone ($opponent->getObj("Modifier"));
            $charac = array(
                "attack",
                "damage"
            );
            foreach ($charac as $key => $name) {
                for ($type = 0; $type < DICE_CHARAC; $type ++) {
                    if ($modifierA != null)
                        $playerSrcModif->addModif($name, $type, $modifierA->getModif($name, $type));
                    
                    if ($modifierB != null)
                        $opponentModif->addModif($name, $type, $modifierB->getModif($name, $type));
                }
            }
            
            // Jets de dés
            $param["PLAYER_ATTACK"] = $playerSrcModif->getScore("attack");
            $param["TARGET_DEFENSE"] = $opponentModif->getScore("defense");
            
            // Tir réussi
            $param["BONUS_DAMAGE"] = 0;
            $param["TARGET_DAMAGETOT2"] = 0;
            $param["TARGET_DAMAGE"] = 0;
            $param["TARGET_HP"] = 0;
            $param["BUBBLE"] = 0;
            $param["IDENT"] = $ident;
            
            // Ajout de la flèche dans l'équipement de la cible pour récupération avec proba
            $arrow = new Equipment();
            $arrow->load($id_Arrow, $db);
            $succes = $arrow->get("level") * 5 + 20;
            $chance = mt_rand(1, 100);
            
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
                // Ajout de la flèche dans l'équipement de la cible pour récupération avec proba
                if ($opponent->get("status") == 'NPC' && $opponent->get("id_BasicRace") != ID_BASIC_RACE_FEU_FOL && $chance >= $succes)
                    $dbc = new DBCollection("UPDATE Equipment SET sell=0, id_Equipment\$bag= 0, id_Player=" . $opponent->get("id") . " WHERE id=" . $id_Arrow, $db, 0, 0, false);
                else
                    $dbc = new DBCollection("DELETE FROM Equipment WHERE id=" . $id_Arrow, $db, 0, 0, false);
                
                $param["BONUS_DAMAGE"] = self::getDamageArchery($param["PLAYER_ATTACK"], $param["TARGET_DEFENSE"], $ident);
                $param["TARGET_DAMAGETOT2"] = $playerSrcModif->getScore("damage") + $param["BONUS_DAMAGE"];
                $param["TARGET_DAMAGE"] = $param["TARGET_DAMAGETOT2"];
                
                // Si l'adversaire possède une bulle de vie
                require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
                $param["BUBBLE"] = SpellFactory::PassiveBubble($opponent, $param, $db);
                $param["TARGET_HP"] = max($param["TARGET_DAMAGETOT2"] - $opponentModif->getScore("armor"), 0);
                
                $param["PIEGE"] = 0;
                $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db);
                if (! $dbbm->eof()) {
                    $opponent->set("currhp", $opponent->get("currhp") + 10 * $dbbm->get("level"));
                    $dbbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Piège'", $db, 0, 0, false);
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param2, $db, 0);
                    $opponent->updateDBr($db);
                    $param["PIEGE"] = 1;
                }
                
                $hp = $opponent->get("currhp") - $param["TARGET_HP"];
                $param["XP"] += 1;
                PlayerFactory::Enhance($playerSrc, ARCHERY_EVENT_SUCCESS, $param, $db, $ident);
                if ($param["TARGET_HP"] > 0)
                    PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                
                if ($ident == ABILITY_NEGATION) {
                    $param["MAGICSKILL"] = $playerSrcModif->getScore("magicSkill");
                }
                // Mort de la cible
                if ($hp <= 0)
                    self::kill($playerSrc, $opponent, $param, $db);
                else {
                    if ($ident == ABILITY_DISABLING) {
                        if ($param["TARGET_HP"] > 0) {
                            $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Blessure gênante'", $db);
                            if (! $dbbm->eof())
                                $dbu = new DBCollection("UPDATE BM SET life=1 WHERE id_Player=" . $opponent->get("id") . " AND name='Blessure gênante'", $db, 0, 0, false);
                            else
                                $dbi = new DBCollection(
                                    "INSERT INTO BM ( id_Player,id_Player\$src, effect, name,life,date) VALUES (" . $opponent->get("id") . "," . $playerSrc->get("id") .
                                         ", 'NEGATIVE', 'Blessure gênante' ,1,'" . $date . "')", $db, 0, 0, false);
                        }
                    }
                    if ($ident == ABILITY_NEGATION) {
                        if ($param["TARGET_HP"] > 0) {
                            $level = 1 + floor($param["MAGICSKILL"] / 8);
                            $param["LEVEL_SPELL"] = $level;
                            $param["CURSE"] = $level;
                            $param["CANCEL"] = 0;
                            // Si l'opposant est sous bénédiction
                            $dbc = new DBCollection("SELECT * FROM BM WHERE name='Bénédiction' AND id_Player=" . $opponent->get("id"), $db);
                            if (! $dbc->eof()) {
                                $param["LEVEL_BENE"] = $dbc->get("level");
                                if ($param["LEVEL_BENE"] >= $param["CURSE"])
                                    $param["CANCEL"] = 1;
                                else
                                    $param["CURSE"] -= $param["LEVEL_BENE"];
                            }
                            if (! $param["CANCEL"]) {
                                $dbbm = new DBCollection("SELECT id FROM BM WHERE id_Player=" . $opponent->get("id") . " AND name='Flèche de négation'", $db);
                                if (! $dbbm->eof())
                                    $dbu = new DBCollection(
                                        "UPDATE BM SET life=1, level=level+" . $param["CURSE"] . " WHERE id_Player=" . $opponent->get("id") . " AND name='flèche de négation'", $db, 
                                        0, 0, false);
                                else
                                    $dbi = new DBCollection(
                                        "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src, level, effect, name,life,date) VALUES (3, " . $opponent->get("id") . "," .
                                             $playerSrc->get("id") . "," . $param["CURSE"] . ", 'NEGATIVE', 'Flèche de négation' ,1,'" . $date . "')", $db, 0, 0, false);
                            }
                        }
                    }
                    
                    PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db);
                    PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_HP"], $param, $db);
                    $opponent->updateDBr($db);
                    $param["TYPE_ATTACK"] = ARCHERY_EVENT_SUCCESS;
                }
            } else { // Tir raté
                     // depot de la fleche au sol
                if ($chance >= $succes) {
                    $date = gmdate('Y-m-d H:i:s');
                    $dbc = new DBCollection(
                        "UPDATE Equipment SET sell=0, id_Equipment\$bag= 0, id_Player=0, state='Ground', dropped='" . $date . "',
                        x=" . $opponent->get("x") . ", y=" . $opponent->get("y") . ", room=" . $opponent->get("room") . ", inbuilding=" . $opponent->get("inbuilding") .
                             " WHERE id=" . $id_Arrow, $db);
                } else
                    $dbc = new DBCollection("DELETE FROM Equipment WHERE id=" . $id_Arrow, $db, 0, 0, false);
                
                if ($playerSrc->get("status") == 'PC')
                    self::legalAction($playerSrc, $opponent, $param, $db, 0);
                PlayerFactory::Enhance($playerSrc, ARCHERY_EVENT_FAIL, $param, $db);
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
                $param["TYPE_ATTACK"] = ARCHERY_EVENT_FAIL;
            }
            
            PlayerFactory::addXP($playerSrc, $param["XP"], $db);
            $playerSrc->updateDBr($db);
        }
        SpellFactory::removeAnger($playerSrc, 1, $param, $db);
        SpellFactory::removeAnger($playerSrc, 2, $param, $db);
        
        return ACTION_NO_ERROR;
    }

    static function giveMoney(&$playerSrc, $opponent, $value, &$param, &$db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        self::globalInfoOpponent($opponent, $param);
        self::appear($playerSrc, $param, $db);
        $param["TYPE_ACTION"] = GIVE_MONEY;
        $param["TYPE_ATTACK"] = GIVE_MONEY_EVENT;
        
        $opponent->set("money", $opponent->get("money") + $value);
        $param["VALUE"] = $value;
        $playerSrc->set("money", $playerSrc->get("money") - $value);
        
        $playerSrc->updateDB($db);
        $opponent->updateDB($db);
    }

    static function rpAction(&$playerSrc, $actionid, &$param, &$db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        self::appear($playerSrc, $param, $db);
        $param["TYPE_ACTION"] = RP_ACTION;
        $param["TYPE_ATTACK"] = RP_ACTION_EVENT + $actionid;
        
        if ($playerSrc->get("ap") < RP_ACTION_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $dbrp = new DBCollection("SELECT id_Player FROM RolePlayAction WHERE id=" . quote_smart($actionid), $db);
        if ($playerSrc->get("id") != $dbrp->get("id_Player"))
            return ACTION_NOT_ALLOWED;
        
        $playerSrc->set("ap", $playerSrc->get("ap") - 1);
        
        $playerSrc->updateDB($db);
    }

    /* *************************************************************** ACTION CONTEXTUELLE **************************************************** */
    static function free($playerSrc, &$param, $db)
    {
        if ($playerSrc->get("ap") < FREE_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $param["TYPE_ACTION"] = FREE;
        self::appear($playerSrc, $param, $db);
        self::globalInfo($playerSrc, $param);
        
        $dbt = new DBCollection(
            "SELECT Player.id, Player.state, Player.x, Player.y FROM Player LEFT JOIN Caravan ON Caravan.id=Player.id_Caravan WHERE Caravan.id_Player=" . $playerSrc->get("id"), $db);
        
        if ($playerSrc->get("state") == "creeping") {
            $param["TYPE_ATTACK"] = FREE_EVENT;
            $objPlayer = $playerSrc;
        } else {
            $param["TYPE_ATTACK"] = FREE_TURTLE_EVENT;
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $playerSrc->get("id_Owner"), $db);
            $objPlayer = new Player();
            $objPlayer->externDBObj("Modifier");
            $objPlayer->load($dbt->get("id"), $db);
        }
        // partie commune pour le joueur et la tortue
        $dbIdBM = new DBCollection("select min(id) as id FROM BM WHERE id_Player=" . $objPlayer->get("id") . " AND name='A terre'", $db);
        $dbbm = new DBCollection("DELETE FROM BM WHERE id = " . $dbIdBM->get("id"), $db, 0, 0, false);
        PlayerFactory::initBM($objPlayer, $objPlayer->getObj("Modifier"), $param, $db);
        $array_result = PlayerFactory::getListIdBolas($objPlayer, $db);
        if (count($array_result) > 0) {
            $idBolas = $array_result[0];
            if (count($array_result) <= 1) { // il est possible d'être entravé par plusieurs Bolas
                $objPlayer->set("state", "walking");
                $param["PC_STILL_CREEPING"] = "NO";
            } else {
                $param["PC_STILL_CREEPING"] = "YES";
            }
            $dbe = new DBCollection("SELECT name FROM Equipment WHERE id=" . $idBolas, $db);
            $param["NAME"] = $dbe->get("name");
            $cond = "";
            if ($param["NAME"] == "Branche d'Etranglesaule")
                $cond = ",id_BasicEquipment = 507, id_EquipmentType=33 ";
            $dbe = new DBCollection(
                "UPDATE Equipment set inbuilding=" . $objPlayer->get("inbuilding") . ", room=" . $objPlayer->get("room") . " , x=" . $objPlayer->get("x") . ", y=" .
                     $objPlayer->get("y") . ", state='Ground', id_Player=0 " . $cond . " WHERE id=" . $idBolas, $db, 0, 0, false);
            $objPlayer->updateDBr($db);
        }
        
        $playerSrc->set("ap", $playerSrc->get("ap") - 8);
        $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    static function destroy($playerSrc, $id_Exploitation, &$param, $db)
    {
        $param["TYPE_ACTION"] = DESTROY;
        $param["TYPE_ATTACK"] = DESTROY_EVENT;
        self::appear($playerSrc, $param, $db);
        self::globalInfo($playerSrc, $param);
        $dbbm = new DBCollection("DELETE FROM Exploitation WHERE id=" . $id_Exploitation, $db, 0, 0, false);
        $playerSrc->set("ap", $playerSrc->get("ap") - DESTROY_AP);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function rideTurtle($playerSrc, $idTurtle, &$param, $db)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ACTION"] = RIDE_TURTLE;
        $param["TYPE_ATTACK"] = RIDE_TURTLE_EVENT;
        self::globalInfo($playerSrc, $param);
        $dbc = new DBCollection("SELECT * FROM Player WHERE id=" . $idTurtle, $db);
        
        $playerSrc->set("hidden", floor($playerSrc->get("hidden") / 10) * 10 + 3);
        $playerSrc->set("state", "turtle");
        $playerSrc->set("x", $dbc->get("x"));
        $playerSrc->set("y", $dbc->get("y"));
        
        // $dbx = new DBCollection("UPDATE Player SET state='turtle' WHERE id=".$idTurtle, $db,0,0,false);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - RIDE_TURTLE_AP);
        $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function unmountTurtle($playerSrc, $idTurtle, $xnew, $ynew, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = TURTLE_UNMOUNT;
        $param["TYPE_ATTACK"] = TURTLE_UNMOUNT_EVENT;
        
        $playerSrc->set("state", "walking");
        $playerSrc->set("x", $xnew);
        $playerSrc->set("y", $ynew);
        $playerSrc->set("hidden", $playerSrc->get("hidden") - 3);
        $playerSrc->updateHidden($db);
        $cond = "";
        if ($playerSrc->get("inbuilding"))
            $dbu = new DBCollection("UPDATE Player SET room=2 WHERE id=" . $idTurtle, $db, 0, 0, false);
        $playerSrc->set("ap", $playerSrc->get("ap") - TURTLE_UNMOUNT_AP);
        $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function recall($playerSrc, $choice, &$param, $db)
    {
        $param["TYPE_ACTION"] = RECALL;
        self::globalInfo($playerSrc, $param);
        $dbc = new DBCollection(
            "SELECT Player.id as id, Player.name as name FROM Player LEFT JOIN BM ON BM.id_Player\$src = Player.id WHERE BM.id_Player=" . $playerSrc->get("id") .
                 " AND BM.name='Rappel'", $db);
        if ($dbc->count() > 0) {
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($dbc->get("id"), $db);
            self::globalInfoOpponent($opponent, $param);
            $dest = array();
            $dest[$dbc->get("name")] = $dbc->get("name");
            require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        } else {
            $choice = 2;
        }
        if ($choice == 0)
            return INVALID_FIRST_FIELD;
        
        if ($choice == 2) {
            $param["CHOICE"] = 0;
            $param["TYPE_ATTACK"] = RECALL_EVENT_REFUSED;
            if ($dbc->count() > 0) {
                MailFactory::sendSingleDestMsg($playerSrc, localize("Rappel refusé"), localize($playerSrc->get("name") . " a refusé votre demande de rappel."), $dest, 2, $err2, 
                    $db, - 1, 0);
            }
            $playerSrc->set("recall", 0);
        }
        
        if ($choice == 1) {
            $param["CHOICE"] = 1;
            $param["TYPE_ATTACK"] = RECALL_EVENT_ACCEPTED;
            $xo = $opponent->get("x");
            $yo = $opponent->get("y");
            $map = $opponent->get("map");
            $done = 0;
            
            if ($playerSrc->get("state") == "turtle") {
                $playerSrc->set("state", "walking");
            }
            if ($opponent->get("inbuilding")) {
                $playerSrc->set("x", $xo);
                $playerSrc->set("y", $yo);
                $param["X"] = $xo;
                $param["Y"] = $yo;
                $playerSrc->set("inbuilding", $opponent->get("inbuilding"));
                $playerSrc->set("room", $opponent->get("room"));
                $done = 1;
            } else {
                for ($i = - 1; $i < 2; $i ++) {
                    for ($j = - 1; $j < 2; $j ++) {
                        if (self::freePlace($xo + $i, $yo + $j, $map, $db) && $done == 0) {
                            $playerSrc->set("x", $xo + $i);
                            $playerSrc->set("y", $yo + $j);
                            $param["X"] = $xo + $i;
                            $param["Y"] = $yo + $j;
                            $done = 1;
                        }
                    }
                }
            }
            
            if ($done == 1) {
                $playerSrc->set("recall", 0);
                MailFactory::sendSingleDestMsg($playerSrc, localize("Rappel réussi"), localize($playerSrc->get("name") . " a été téléporté à coté de vous."), $dest, 2, $err2, $db, 
                    - 1, 0);
                $param["AP"] = $playerSrc->get("ap");
                $playerSrc->set("ap", 0);
                $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Rappel'", $db, 0, 0, false);
                if ($playerSrc->get("state") == "turtle") {
                    $playerSrc->get("state", "walking");
                    $dbc = new DBCollection("UPDATE Player SET state='walking' WHERE id_Member=" . $playerSrc->get("id_Member") . " AND id_BasicRace=263", $db, 0, 0, false);
                }
                require_once (HOMEPATH . "/lib/MapInfo.inc.php");
                $currmap = $playerSrc->get("map");
                $mapinfo = new MapInfo($db);
                self::updateInfoAfterMove($param["X"], $param["Y"], $playerSrc, $db);
                $mapinfo->updateWayCount($param["X"], $param["Y"], $currmap, $playerSrc);
                $playerSrc->updateHidden($db);
            } else
                return ROOM_FULL_ERROR;
        }
        
        $param["ERROR"] = 0;
        $playerSrc->updateDB($db);
        return ACTION_NO_ERROR;
    }

    static function arrest($playerSrc, $choice, &$param, $db, $violence = 0)
    {
        $param["TYPE_ACTION"] = ARREST;
        $param["CHOICE"] = $choice;
        self::globalInfo($playerSrc, $param);
        $dbc = new DBCollection(
            "SELECT Player.moneyBank as moneyBank, Player.id as id, Player.name as name, BM.value as value, BM.id_Player\$src as src FROM Player LEFT JOIN BM ON BM.id_Player\$src = Player.id WHERE BM.id_Player=" .
                 $playerSrc->get("id") . " AND BM.name='Arrestation'", $db);
        
        if ($choice == 0)
            return CHOICE_FIELD_INVALID;
        
        $cond = "";
        if ($violence == 1)
            $cond = ", nbkillpc=nbkillpc+1";
            // Paye l'amende et rend l'argent
        if ($choice == 3) {
            $param["TYPE_ATTACK"] = PENALTY_EVENT_ACCEPTED;
            if ($playerSrc->get("money") < $dbc->get("moneyBank") + 20)
                return NOT_ENOUGH_MONEY_ERROR;
            
            $playerSrc->set("state", "walking");
            
            // Log du garde qui rend l'argent à la victime
            $garde = new Player();
            $garde->externDBObj("Modifier");
            $garde->load($dbc->get("id"), $db);
            self::globalInfo($garde, $param2);
            $dbm = new DBCollection("SELECT * FROM Event WHERE id_Player\$src=" . $playerSrc->get("id") . " AND typeAction=" . ABILITY_STEAL . " ORDER BY date DESC", $db);
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($dbm->get("id_Player\$dest"), $db);
            self::globalInfoOpponent($opponent, $param2);
            $param2["TYPE_ACTION"] = GIVE_MONEY;
            $param2["TYPE_ATTACK"] = GIVE_MONEY_EVENT;
            $param2["VALUE"] = $dbc->get("moneyBank");
            $param2["DELAI"] = 2;
            $param2["ERROR"] = 0;
            Event::logAction($db, $param2["TYPE_ACTION"], $param2);
            
            $playerSrc->set("money", $playerSrc->get("money") - ($dbc->get("moneyBank") + 20));
            $dbu = new DBCollection("UPDATE Player SET money=money+" . $dbc->get("moneyBank") . " WHERE id=" . $dbm->get("id_Player\$dest"), $db, 0, 0, false);
            $dbu = new DBCollection("DELETE FROM BM WHERE name='Arrestation' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            if ($dbc->get("src") < 1500)
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=10, moneyBank=0, nbkill=nbkill+1 " . $cond .
                         " , x=464, y=375, prison=0, inbuilding=1076, room=19, id_Player\$target = 0 WHERE status='NPC' AND prison=" . $playerSrc->get("id"), $db, 0, 0, false);
            if ($dbc->get("src") > 2500)
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=10, moneyBank=0, nbkill=nbkill+1 " . $cond .
                         " , x=564, y=344, prison=0, inbuilding=1570, room=19, id_Player\$target = 0 WHERE status='NPC' AND prison=" . $playerSrc->get("id"), $db, 0, 0, false);
            if ($dbc->get("src") > 1500 && $dbc->get("src") < 2500)
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=10, moneyBank=0, nbkill=nbkill+1 " . $cond .
                         " , x=256, y=301, prison=0, inbuilding=1252, room=19, id_Player\$target = 0 WHERE status='NPC' AND prison=" . $playerSrc->get("id"), $db, 0, 0, false);
        }
        
        // Je suis conduit en prison parce que j'ai tué ou parce que je peux pas payer l'amende
        if ($choice == 1 or $choice == 4) {
            if ($dbc->get("moneyBank") != 0) {
                // Log du garde qui rend l'argent à la victime
                $garde = new Player();
                $garde->externDBObj("Modifier");
                $garde->load($dbc->get("id"), $db);
                self::globalInfo($garde, $param2);
                // A changer, pas sur que ca soit le dernier volé dans le cas ou l'arrestation est violente donc pas forcément immédiate au vol
                $dbm = new DBCollection("SELECT * FROM Event WHERE id_Player\$src=" . $playerSrc->get("id") . " AND typeAction=" . ABILITY_STEAL . " ORDER BY date DESC", $db);
                if ($dbm->count() > 0) {
                    $opponent = new Player();
                    
                    $opponent->externDBObj("Modifier");
                    $opponent->load($dbm->get("id_Player\$dest"), $db);
                    self::globalInfoOpponent($opponent, $param2);
                    $param2["TYPE_ACTION"] = GIVE_MONEY;
                    $param2["TYPE_ATTACK"] = GIVE_MONEY_EVENT;
                    $param2["VALUE"] = $dbc->get("moneyBank");
                    $param2["DELAI"] = 2;
                    $param2["ERROR"] = 0;
                    Event::logAction($db, $param2["TYPE_ACTION"], $param2);
                    
                    $playerSrc->set("money", max(0, $playerSrc->get("money") - ($dbc->get("moneyBank") + 20)));
                    $dbu = new DBCollection("UPDATE Player SET money=money+" . $dbc->get("moneyBank") . " WHERE id=" . $dbm->get("id_Player\$dest"), $db, 0, 0, false);
                }
            }
            
            if ($dbc->get("src") < 1500) {
                $playerSrc->set("x", 464);
                $playerSrc->set("y", 375);
                $playerSrc->set("inbuilding", 1076);
                $param["VILLE"] = "Artasse";
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=10, moneyBank=0, nbkill=nbkill+1 " . $cond .
                         " , x=464, y=375, prison=0, inbuilding=1076, room=19, id_Player\$target = 0 WHERE status='NPC' AND prison=" . $playerSrc->get("id"), $db, 0, 0, false);
            }
            if ($dbc->get("src") > 1500 && $dbc->get("src") < 2500) {
                $playerSrc->set("x", 256);
                $playerSrc->set("y", 301);
                $playerSrc->set("inbuilding", 1252);
                $param["VILLE"] = "Earok";
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=10, moneyBank=0, nbkill=nbkill+1 " . $cond .
                         " , x=256, y=301, prison=0, inbuilding=1252, room=19, id_Player\$target = 0 WHERE status='NPC' AND prison=" . $playerSrc->get("id"), $db, 0, 0, false);
            }
            if ($dbc->get("src") > 2500) {
                $playerSrc->set("x", 564);
                $playerSrc->set("y", 344);
                $playerSrc->set("inbuilding", 1570);
                $param["VILLE"] = "Tonak";
                $dbu = new DBCollection(
                    "UPDATE Player SET hidden=10, moneyBank=0, nbkill=nbkill+1 " . $cond .
                         " , x=564, y=344, prison=0, inbuilding=1570, room=19, id_Player\$target = 0 WHERE status='NPC' AND prison=" . $playerSrc->get("id"), $db, 0, 0, false);
            }
            
            $dbu = new DBCollection("DELETE FROM BM WHERE name='Arrestation' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            $param["TYPE_ATTACK"] = ARREST_EVENT_ACCEPTED;
            $playerSrc->set("state", "prison");
            $dbj = new DBCollection("DELETE FROM BM WHERE name='A terre' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            $param["X"] = $playerSrc->get("x");
            $param["Y"] = $playerSrc->get("y");
            $playerSrc->set("hidden", 10);
            $param["SURCIS"] = $playerSrc->get("prison") * 12;
            $param["DER"] = $dbc->get("value") * 12;
            $param["PEINETOT"] = ($param["SURCIS"] + $param["DER"]);
            $playerSrc->set("prison", $playerSrc->get("prison") + $dbc->get("value"));
            $playerSrc->set("room", 19);
            $time = time();
            $time += $playerSrc->get("prison") * 60 * 60 * 12;
            $date = gmdate("Y-m-d H:i:s", $time);
            
            $dbu = new DBCollection("DELETE FROM BM WHERE name='Bolas' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            $dbu = new DBCollection("DELETE FROM Equipment WHERE name='Bolas' AND state='Transported' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
            $dbi = new DBCollection(
                "INSERT INTO BM (id_Player,id_Player\$src, name,life,date) VALUES (" . $playerSrc->get("id") . "," . $dbc->get("src") . ", 'En Prison' ,-2,'" . $date . "')", $db, 0, 
                0, false);
        }
        
        // Je refuse l'arrestation
        if ($choice == 2) {
            $param["TYPE_ATTACK"] = ARREST_EVENT_REFUSED;
            $playerSrc->set("state", "walking");
            $dbu = new DBCollection("UPDATE Player SET id_Player\$target=" . $playerSrc->get("id") . " WHERE prison=" . $playerSrc->get("id"), $db, 0, 0, false);
            $dbu = new DBCollection("UPDATE BM SET value=value+4 WHERE name='Arrestation' AND id_Player=" . $playerSrc->get("id"), $db, 0, 0, false);
        }
        
        $param["ERROR"] = 0;
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param2, $db, 0);
        $playerSrc->updateDB($db);
        return ACTION_NO_ERROR;
    }

    static function exitJail($playerSrc, &$param, $db)
    {
        $param["TYPE_ACTION"] = EXIT_JAIL;
        $param["TYPE_ATTACK"] = EXIT_JAIL_EVENT;
        self::globalInfo($playerSrc, $param);
        $playerSrc->set("state", "walking");
        $param["PRISON"] = $playerSrc->get("prison") * 12;
        $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='En Prison'", $db, 0, 0, false);
        $playerSrc->updateDB($db);
        return ACTION_NO_ERROR;
    }

    static function graff($playerSrc, &$param, $graf, $fini, $db)
    {
        $param["TYPE_ACTION"] = GRAFFITI;
        $param["TYPE_ATTACK"] = GRAFFITI_EVENT;
        self::globalInfo($playerSrc, $param);
        $param["FINI"] = $fini;
        $date = gmdate('Y-m-d H:i:s');
        $dbbm = new DBCollection(
            "INSERT INTO Graffiti (id_Player, id_Building, date_Graff, text, terminé) VALUES(" . $playerSrc->get("id") . "," . $playerSrc->get("inbuilding") . ",'" . $date . "','" .
                 $graf . "','" . $fini . "')", $db, 0, 0, false);
        $playerSrc->updateDB($db);
        $playerSrc->set("ap", $playerSrc->get("ap") - GRAFF_AP);
        return ACTION_NO_ERROR;
    }

    static function continueGraff($playerSrc, &$param, $text, $graf, $fini, $db)
    {
        $param["TYPE_ACTION"] = CONTINUE_GRAFFITI;
        $param["TYPE_ATTACK"] = CONTINUE_GRAFFITI_EVENT;
        self::globalInfo($playerSrc, $param);
        $date = gmdate('Y-m-d H:i:s');
        $param["FINI"] = $fini;
        if ($param["FINI"] != 1)
            $param["FINI"] = 0;
        
        $dbp = new DBCollection("SELECT text FROM Graffiti WHERE id=" . $graf, $db);
        $textentier = $dbp->get("text") . $text;
        
        $dbbm = new DBCollection("UPDATE Graffiti SET date_Graff='" . $date . "', text='" . $textentier . "', terminé=" . $param["FINI"] . " WHERE id=" . $graf, $db, 0, 0, false);
        $playerSrc->updateDB($db);
        $playerSrc->set("ap", $playerSrc->get("ap") - CONTINUE_GRAFF_AP);
        return ACTION_NO_ERROR;
    }

    static function consultGraff($playerSrc, &$param, $db)
    {
        $param["TYPE_ACTION"] = CONTINUE_GRAFFITI;
        $param["TYPE_ATTACK"] = CONTINUE_GRAFFITI_EVENT;
        self::globalInfo($playerSrc, $param);
        $date = gmdate('Y-m-d H:i:s');
        $i = 0;
        $dbp = new DBCollection("SELECT * FROM Graffiti WHERE id_Building=" . $playerSrc->get("inbuilding"), $db);
        while (! $dbp->eof()) {
            if ($dbp->get("terminé") == true) {
                $dbid = new DBCollection("SELECT name FROM Player WHERE id=" . $dbp->get("id_Player"), $db);
                $param["Text" . $i] = $dbp->get("date_Graff") . " par " . ($dbid->count() > 0 ? $dbid->get("name") : "Inconnu") . " : " . $dbp->get("text");
                $i += 1;
            }
            $dbp->next();
        }
        $param["NB1"] = $i;
        $dbp = new DBCollection("SELECT * FROM Graffiti WHERE id_Building=" . $playerSrc->get("inbuilding"), $db);
        while (! $dbp->eof()) {
            if (! $dbp->get("terminé") == true) {
                $param["Text" . $i] = "# " . $dbp->get("text");
                $i += 1;
            }
            $dbp->next();
        }
        $param["NB2"] = $i;
        $playerSrc->updateDB($db);
        $playerSrc->set("ap", $playerSrc->get("ap") - CONSULT_GRAFF_AP);
        return ACTION_NO_ERROR;
    }

    static function appear($playerSrc, &$param, $db)
    {
        // $param["TYPE_ACTION"] = APPEAR;
        // self::globalInfo($playerSrc,$param);
        if ($playerSrc->get("state") != "turtle") {
            $playerSrc->set("hidden", floor($playerSrc->get("hidden") / 10) * 10);
            $playerSrc->updateDB($db);
        }
        // $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=".$playerSrc->get("id")." AND name='Embuscade'", $db,0,0,false);
        $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player\$src=" . $playerSrc->get("id") . " AND name='Embuscade'", $db, 0, 0, false);
        
        $dbbm = new DBCollection("select id,level,id_Player\$src from BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Souffle de négation'", $db);
        if ($dbbm->count() > 0) {
            // ajout du malus de sortie de souffle de négation
            $bm = new BM();
            $bm->set("name", "Malus Souffle de négation");
            $bm->set("id_StaticModifier", 24);
            $bm->set("life", "2");
            $bm->set("level", $dbbm->get("level"));
            $bm->set("value", 1);
            $bm->set("effect", "NEGATIVE");
            $bm->set("id_Player\$src", $dbbm->get("id_Player\$src"));
            $bm->set("id_Player", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->addDBr($db);
            $dbbmd = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='Souffle de négation'", $db, 0, 0, false);
        }
        return ACTION_NO_ERROR;
    }

    /* *********************************************************** MENU OBJET *********************************************** */
    static function pick($playerSrc, $idObject, $inv, &$param, &$db)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ACTION"] = PICK_UP_OBJECT;
        $param["TYPE_ATTACK"] = PICK_UP_EVENT;
        
        self::globalInfo($playerSrc, $param);
        if ($playerSrc->get("ap") < PICK_UP_OBJECT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $equip = new Equipment();
        if ($equip->load($idObject, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL)
            return ACTION_NOT_ALLOWED;
        
        if ($equip->get("id_Player") != 0)
            return NOT_YOUR_OBJECT_ERROR;
        
        if ($playerSrc->get("state") == "turtle")
            return NOT_IN_TURTLE;
        
        if (distHexa($equip->get("x"), $equip->get("y"), $playerSrc->get("x"), $playerSrc->get("y")) > 1)
            return ACTION_OBJECT_TOO_FAR;
        
        if ($equip->get("name") == "") {
            $playerSrc->set("money", $playerSrc->get("money") + $equip->get("po"));
            $equip->deleteDB($db);
        } else {
            if (! PlayerFactory::checkPlaceBag($playerSrc->get("id"), $inv - 1, $db))
                return INV_FULL_ERROR;
            
            if (! PlayerFactory::checkObjectInBag($equip->get("id_EquipmentType"), $inv - 1))
                return WRONG_BAG_ERROR;
            if (($equip->get("id_EquipmentType") >= 41 and $equip->get("id_EquipmentType") <= 44) or $equip->get("id_EquipmentType") == 20 or $equip->get("id_EquipmentType") == 46)
                $dbe = new DBCollection("UPDATE Equipment SET id_" . $playerSrc->m_className . "=" . $playerSrc->get("id") . " WHERE id_Equipment\$bag=" . $equip->get("id"), $db, 0, 
                    0, false);
            
            $equip->set("id_" . $playerSrc->m_className, $playerSrc->get("id"));
            $equip->set("state", "Carried");
            $equip->set("weared", "No");
            $equip->set("collected", gmdate("Y-m-d H:i:s"));
            $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $inv - 1, $db));
            $equip->updateDB($db);
        }
        
        $playerSrc->set("ap", $playerSrc->get("ap") - PICK_UP_OBJECT_AP);
        $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    static function wear($playerSrc, $idObject, &$param, &$db, $autoUpdate = 1)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ACTION"] = WEAR_EQUIPMENT;
        
        self::globalInfo($playerSrc, $param);
        if ($playerSrc->get("ap") < WEAR_EQUIPMENT_AP && $playerSrc->get("incity") == 0)
            return ACTION_NOT_ENOUGH_AP;
        
        $dbe = new DBCollection(
            "SELECT E2.* FROM Equipment AS E1 LEFT JOIN Equipment AS E2 ON E1.id_" . $playerSrc->m_className . "=E2.id_" . $playerSrc->m_className .
                 " LEFT JOIN EquipmentType AS ET1 ON E1.id_EquipmentType=ET1.id LEFT JOIN EquipmentType AS ET2 ON E2.id_EquipmentType=ET2.id WHERE E1.id=" . $idObject .
                 " AND E1.id_" . $playerSrc->m_className . "=" . $playerSrc->get("id") . " AND E2.weared=\"Yes\" AND (ET1.mask & ET2.mask)", $db);
        $error = 0;
        while (! $dbe->eof()) {
            $isBagTools = false;
            $isMainBag = false;
            $bagId = 0;
            
            if ($dbe->get("id_EquipmentType") == 40 && (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 6, $db))) {
                $isBagTools = true;
                $bagId = 6;
            }
            if ($dbe->get("id_EquipmentType") != 40 || ! $isBagTools) {
                if (! (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)))
                    $error = INV_FULL_ERROR;
                else {
                    $isMainBag = true;
                }
            }
            $dbe->next();
        }
        
        if ($error != 0)
            return $error;
        if ($dbe->count() > 0)
            $dbe->first();
        while (! $dbe->eof()) {
            $bagId = PlayerFactory::getIdBagByNum($playerSrc->get("id"), "0", $db);
            
            if ($dbe->get("id_EquipmentType") == 40 && (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 6, $db))) {
                $bagId = PlayerFactory::getIdBagByNum($playerSrc->get("id"), "6", $db);
            }
            $dbt = new DBCollection(
                "UPDATE Equipment set weared='No',id_Equipment\$bag=" . $bagId . " WHERE id_" . $playerSrc->m_className . "=" . $playerSrc->get("id") . " and id=" . $dbe->get("id"), 
                $db, 0, 0, false);
            $dbe->next();
        }
        
        $dbt = new DBCollection(
            "SELECT Equipment.id FROM Equipment LEFT JOIN EquipmentType ON EquipmentType.id=id_EquipmentType WHERE wearable=\"Yes\" AND weared=\"No\" AND id_" .
                 $playerSrc->m_className . "=" . $playerSrc->get("id") . " AND Equipment.id=" . $idObject, $db, 0, 0);
        if (! $dbt->eof()) {
            $dbu = new DBCollection(
                "UPDATE Equipment set weared='Yes',sell=0,id_Equipment\$bag=0 WHERE id_" . $playerSrc->m_className . "=" . $playerSrc->get("id") . " and id=" . $dbt->get("id"), $db, 
                0, 0, false);
        }
        
        $playerSrc->set("ap", $playerSrc->get("ap") - WEAR_EQUIPMENT_AP);
        $modif = $playerSrc->getObj("Modifier");
        PlayerFactory::initFigthCharac($playerSrc, $modif, $db);
        PlayerFactory::initBM($playerSrc, $modif, $param, $db);
        if ($autoUpdate)
            $playerSrc->updateDBr($db);
        
        return ACTION_NO_ERROR;
    }

    static function unWear($playerSrc, $idObject, &$param, &$db)
    {
        self::appear($playerSrc, $param, $db);
        $param["TYPE_ACTION"] = UNWEAR_EQUIPMENT;
        
        self::globalInfo($playerSrc, $param);
        if ($playerSrc->get("ap") < UNWEAR_EQUIPMENT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $dbt = new DBCollection(
            "SELECT Equipment.* FROM Equipment LEFT JOIN EquipmentType ON EquipmentType.id=id_EquipmentType WHERE wearable=\"Yes\" AND weared=\"Yes\" AND id_" .
                 $playerSrc->m_className . "=" . $playerSrc->get("id") . " AND Equipment.id=" . $idObject, $db, 0, 0);
        
        $isBagTools = false;
        $isMainBag = false;
        $bagId = PlayerFactory::getIdBagByNum($playerSrc->get("id"), "0", $db);
        ;
        
        if ($dbt->get("id_EquipmentType") == 40 && (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 6, $db))) {
            $isBagTools = true;
            $bagId = PlayerFactory::getIdBagByNum($playerSrc->get("id"), "6", $db);
            ;
        }
        if ($dbt->get("id_EquipmentType") != 40 || ! $isBagTools) {
            if (! (PlayerFactory::checkPlaceBag($playerSrc->get("id"), 0, $db)))
                return INV_FULL_ERROR;
            else {
                $isMainBag = true;
            }
        }
        
        $dbt = new DBCollection("UPDATE Equipment set weared='No',id_Equipment\$bag=" . $bagId . "  WHERE id_" . $playerSrc->m_className . " AND id=" . $idObject, $db, 0, 0, false);
        $playerSrc->set("ap", $playerSrc->get("ap") - UNWEAR_EQUIPMENT_AP);
        $modif = $playerSrc->getObj("Modifier");
        PlayerFactory::initFigthCharac($playerSrc, $modif, $db);
        PlayerFactory::initBM($playerSrc, $modif, $param, $db);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function drop($playerSrc, $idObject, &$param, &$db, $autoUpdate = 1)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ACTION"] = DROP_OBJECT;
        $param["TYPE_ATTACK"] = DROP_EVENT;
        self::globalInfo($playerSrc, $param);
        if ($playerSrc->get("ap") < DROP_OBJECT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $equip = new Equipment();
        if ($equip->load($idObject, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($equip->get("weared") == "Yes")
            return WEARED_OBJECT_ERROR;
        
        if ($equip->get("id_" . $playerSrc->m_className) != $playerSrc->get("id"))
            return NOT_YOUR_OBJECT_ERROR;
        
        if (($playerSrc->get("inbuilding") != 0) && ($playerSrc->get("room") != BEDROOM)) {
            $dbe = new DBCollection("SELECT id FROM Equipment WHERE state='Ground' AND inbuilding=" . $playerSrc->get("inbuilding") . " AND room=" . $playerSrc->get("room"), $db, 0, 
                0);
            if ($dbe->count() > NB_MAX_ROOM_ITEMS)
                return ROOM_FULL_ERROR;
        }
        
        // Si l'objet est un sac, un carquois ou une ceinture, tous les objets qu'il contient sont donnés
        if (($equip->get("id_EquipmentType") >= 41 and $equip->get("id_EquipmentType") <= 44) or $equip->get("id_EquipmentType") == 20 or $equip->get("id_EquipmentType") == 46)
            $dbe = new DBCollection("UPDATE Equipment SET id_Player=0 WHERE id_Equipment\$bag=" . $equip->get("id"), $db, 0, 0, false);
        
        $equip->set("dropped", gmdate("Y-m-d H:i:s"));
        $equip->set("id_" . $playerSrc->m_className, 0);
        $equip->set("state", "Ground");
        $equip->set("x", $playerSrc->get("x"));
        $equip->set("y", $playerSrc->get("y"));
        $equip->set("inbuilding", $playerSrc->get("inbuilding"));
        $equip->set("room", $playerSrc->get("room"));
        
        $equip->updateDB($db);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - DROP_OBJECT_AP);
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function give($playerSrc, $equip, $target, &$param, &$db, $autoUpdate = 1)
    {
        self::appear($playerSrc, $param, $db);
        
        self::globalInfo($playerSrc, $param);
        self::globalInfoOpponent($target, $param);
        $param["TYPE_ACTION"] = GIVE_OBJECT;
        $param["TYPE_ATTACK"] = GIVE_OBJECT_EVENT;
        $param["OBJECT_NAME"] = $equip->get("name");
        $param["OBJECT_ID"] = $equip->get("id");
        
        // Si l'objet est un sac, un carquois ou une ceinture, tous les objets qu'il contient sont donnés
        if (($equip->get("id_EquipmentType") >= 41 and $equip->get("id_EquipmentType") <= 44) or $equip->get("id_EquipmentType") == 20 or $equip->get("id_EquipmentType") == 46)
            $dbe = new DBCollection("UPDATE Equipment SET id_Player=" . $target->get("id") . " WHERE id_Equipment\$bag=" . $equip->get("id"), $db, 0, 0, false);
        
        $equip->set("id_Player", $target->get("id"));
        $equip->set("state", "Carried");
        $equip->set("sell", 0);
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($target->get("id"), $param["INV"], $db));
        $equip->updateDB($db);
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    static function store($playerSrc, $idObject, $inv, &$param, &$db, $autoUpdate = 1)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ACTION"] = STORE_OBJECT;
        self::globalInfo($playerSrc, $param);
        if (($playerSrc->get("inbuilding") && $playerSrc->get("ap") < STORE_OBJECT_INBUILDING_AP) || (! $playerSrc->get("inbuilding") && $playerSrc->get("ap") < STORE_OBJECT_AP))
            return ACTION_NOT_ENOUGH_AP;
        
        $equip = new Equipment();
        if ($equip->load($idObject, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($equip->get("id_Player") != $playerSrc->get("id"))
            return NOT_YOUR_OBJECT_ERROR;
        
        if ($equip->get("weared") == 'Yes')
            return WEARED_OBJECT_ERROR;
        
        if (! PlayerFactory::checkPlaceBag($playerSrc->get("id"), $inv - 1, $db))
            return INV_FULL_ERROR;
        
        if (! PlayerFactory::checkObjectInBag($equip->get("id_EquipmentType"), $inv - 1))
            return WRONG_BAG_ERROR;
        
        if ($equip->get("id_Equipment\$bag") == PlayerFactory::getIdBagByNum($playerSrc->get("id"), $inv - 1, $db))
            return SAME_PLACE_ERROR;
        
        $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $inv - 1, $db));
        $equip->updateDB($db);
        $param["AP"] = STORE_OBJECT_AP;
        if ($playerSrc->get("inbuilding")) {
            $playerSrc->set("ap", $playerSrc->get("ap") - STORE_OBJECT_INBUILDING_AP);
            $param["AP"] = STORE_OBJECT_INBUILDING_AP;
        } else
            $playerSrc->set("ap", $playerSrc->get("ap") - STORE_OBJECT_AP);
        
        $playerSrc->updateDB($db);
        return ACTION_NO_ERROR;
    }

    static function drinkSpring($playerSrc, $id_Spring, &$param, &$db)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ATTACK"] = DRINK_SPRING_EVENT;
        $param["TYPE_ACTION"] = DRINK_SPRING;
        
        self::globalInfo($playerSrc, $param);
        if ($playerSrc->get("ap") < DRINK_SPRING_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $spring = new Building();
        
        if ($spring->load($id_Spring, $db) == - 1)
            return ERROR_BUILDING_LOADING;
        
        $param["SPRING_LEVEL"] = $spring->get("level");
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, floor($spring->get("level") / 2));
        $param["HP_POTENTIEL"] = $modifier->getScoreWithNoBM("hp");
        $param["HP_HEAL"] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
        $spring->set("value", $spring->get("value") - 1);
        if ($spring->get("value") == 0)
            $spring->deleteDB($db);
        else
            $spring->updateDB($db);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - DRINK_SPRING_AP);
        $playerSrc->updateDB($db);
        $param["ERROR"] = 0;
        return ACTION_NO_ERROR;
    }

    static function usePotion($playerSrc, $id_Potion, &$param, &$db)
    {
        self::appear($playerSrc, $param, $db);
        
        $param["TYPE_ATTACK"] = USE_POTION_EVENT;
        $param["TYPE_ACTION"] = USE_POTION;
        $param["ERROR"] = 0;
        
        self::globalInfo($playerSrc, $param);
        
        $potion = new Equipment();
        if ($potion->load($id_Potion, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($potion->get("id_" . $playerSrc->m_className) != $playerSrc->get("id"))
            return NOT_YOUR_OBJECT_ERROR;
        
        $ap = 2;
        if ($potion->get("id_Equipment\$bag") != 0)
            $ap = 1;
        if ($playerSrc->get("ap") < $ap)
            return ACTION_NOT_ENOUGH_AP;
        
        $param["POTION_LEVEL"] = $potion->get("level");
        $param["POTION_NAME"] = $potion->get("name");
        
        $eqmodifier = new Modifier();
        $dbx = new DBCollection("SELECT id_Modifier_BasicEquipment FROM BasicEquipment WHERE id=" . $potion->get("id_BasicEquipment"), $db);
        $dbe = new DBCollection(
            "SELECT " . $eqmodifier->getASRenamer("Modifier_BasicEquipment", "EQM") . " FROM Modifier_BasicEquipment WHERE id=" . $dbx->get("id_Modifier_BasicEquipment"), $db);
        $eqmodifier->DBLoad($dbe, "EQM");
        $eqmodifier->updateFromEquipmentLevel($potion->get("level"));
        
        if ($potion->get("id_BasicEquipment") == 400) {
            $param["HP_POTENTIEL"] = $eqmodifier->getScore("hp");
            $param["HP_HEAL"] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
        } else {
            self::modifierToArray($eqmodifier, $param);
            $static = new StaticModifier();
            $static->addModifObj($eqmodifier);
            
            $bm = new BM();
            $bm->set("name", $param["POTION_NAME"]);
            $bm->set("level", 1);
            $bm->set("value", $potion->get("level"));
            $bm->set("life", "3");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("id_Player", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->setObj("StaticModifier", $static, true);
            
            // suppression du BM de la potion précédente du même type.
            // $dbbm = new DBCollection("DELETE FROM BM WHERE id_Player=" . $playerSrc->get("id") . " AND name='" . $param["POTION_NAME"] . "'", $db, 0, 0, false);
            
            $bm->addDBr($db);
            
            // Test de tolérance des potions
            $dbh = new DBCollection(
                "SELECT sum(value) as drug, min(life) as life FROM BM WHERE id_Player=" . $playerSrc->get("id") .
                     " AND (name='Potion de force' OR name='Potion de dextérité' OR name='Potion de vitesse' OR name='Potion de magie')", $db);
            
            if (! $dbh->eof() && ($dbh->get("drug") - 1) * ($dbh->get("drug") - 1) > $playerSrc->get("level")) {
                $param["DRUG"] = 1;
                $bm = new BM();
                $bm->set("name", "drogué");
                $bm->set("effect", "POSITIVE");
                $bm->set("level", min(4, ($dbh->get("drug") - 1) * ($dbh->get("drug") - 1) - $playerSrc->get("level")));
                $bm->set("life", 3);
                $bm->set("id_Player\$src", $playerSrc->get("id"));
                $bm->set("id_Player", $playerSrc->get("id"));
                $bm->set("id_StaticModifier", 20);
                $bm->set("date", gmdate("Y-m-d H:i:s"));
                $playerSrc->addBM($bm, 3, $param, $db);
            }
        }
        
        $potion->deleteDB($db);
        $param["SELF"] = 1;
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $playerSrc->set("ap", $playerSrc->get("ap") - $ap);
        $playerSrc->updateDB($db);
        return ACTION_NO_ERROR;
    }

    /*
     * static function useParchment($playerSrc, $id_scroll, &$param, &$db)
     * {
     * self::appear($playerSrc, $param, $db);
     *
     * $param["TYPE_ATTACK"] = USE_PARCHMENT_EVENT;
     * $param["TYPE_ACTION"] = USE_PARCHMENT;
     * $param["ERROR"] =0;
     *
     * self::globalInfo($playerSrc,$param);
     *
     * $scroll = new Equipment();
     * if($scroll->load($id_scroll,$db)==-1)
     * return ERROR_OBJECT_LOADING;
     *
     * if($scroll->get("id_".$playerSrc->m_className)!=$playerSrc->get("id"))
     * return NOT_YOUR_OBJECT_ERROR;
     *
     *
     * if($playerSrc->get("ap")< USE_PARCHMENT_AP)
     * return ACTION_NOT_ENOUGH_AP;
     *
     * $dbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=".$scroll->get("id_Mission"), $db);
     * $param["EQUIP_NAME"] = $dbe->get("name");
     *
     * $dbk = new DBCollection("SELECT * FROM Knowledge WHERE id_Player=".$playerSrc->get("id")." AND id_BasicEquipment=".$scroll->get("id_Mission"), $db);
     * if(!$dbk->eof())
     * return KNOWLEDGE_ALREADY_KNOWN;
     * $know = new Knowledge();
     * $know->set("id_Player", $playerSrc->get("id"));
     * $know->set("id_BasicTalent", $scroll->get("id_Warehouse"));
     * $know->set("id_BasicEquipment", $scroll->get("id_Mission"));
     * $know->addDB($db);
     *
     * $playerSrc->set("ap",$playerSrc->get("ap")- USE_PARCHMENT_AP);
     * $playerSrc->updateDB($db);
     * return ACTION_NO_ERROR;
     * }
     */
    static function RestorePV()
    {
        // A coller dans la fonction move et faire avancer le prêtre.
        if ($playerSrc->get("id") == 2) {
            $dbc = new DBCollection("SELECT * FROM Player WHERE status='PC'", $db);
            while (! $dbc->eof()) {
                $nb = 0;
                $player = new Player();
                $player->externDBObj("Modifier");
                $player->load($dbc->get("id"), $db);
                
                $nb += $player->getObj("Modifier")->getAtt("strength", DICE_D6) - 4;
                $nb += $player->getObj("Modifier")->getAtt("dexterity", DICE_D6) - 3;
                $nb += $player->getObj("Modifier")->getAtt("speed", DICE_D6) - 3;
                $nb += $player->getObj("Modifier")->getAtt("magicSkill", DICE_D6) - 3;
                
                $vie = 2 * ($player->get("level") - 1) - $nb;
                
                $hp = 40 + 3 * ($player->get("level") - 1) + 15 * $vie;
                $player->set("currhp", $hp);
                $player->set("hp", $hp);
                $player->getObj("Modifier")->setModif("hp", DICE_ADD, $hp);
                $player->updateDBr($db);
                $dbc->next();
            }
        }
    }
    
    // ---------------------------------------------------------------- Mission Escorte ------------------------------------------------------------
    static function useParchment($playerSrc, $id_Parchment, &$param, &$db)
    {
        self::appear($playerSrc, $param, $db);
        self::globalInfo($playerSrc, $param);
        
        $parchment = new Equipment();
        if ($parchment->load($id_Parchment, $db) == - 1)
            return ERROR_OBJECT_LOADING;
        
        if ($playerSrc->get("ap") < USE_PARCHMENT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $id_Parchment = $parchment->get("id");
        
        $eqparchment = new DBCollection("SELECT id_Player,id_Mission FROM Equipment WHERE id=" . $id_Parchment, $db);
        // $typeMis = $eqparchment->get("id_Mission");
        $idmission = $eqparchment->get("id_Mission");
        // $dbm = new DBCollection("SELECT id FROM Mission WHERE id=".$eqparchment->get("id_Mission"),$db);
        $mission = new Mission();
        $mission->load($idmission, $db);
        $miname = $mission->get("name");
        
        $condition = new DBCollection("SELECT * FROM MissionCondition WHERE idMission=" . $idmission, $db);
        $Xstart = $condition->get("positionXstart");
        $Ystart = $condition->get("positionYstart");
        $Xstop = $condition->get("positionXstop");
        $Ystop = $condition->get("positionYstop");
        
        $reward = new DBCollection("SELECT * FROM MissionReward WHERE idMission=" . $idmission, $db);
        $adMoneyStart = $reward->get("moneyStart");
        $adMoneyStop = $reward->get("moneyStop");
        // $adXpStart=$reward->get("xpStart");
        // $adXpStop=$reward->get("xpStop");
        
        $point = "";
        // Si le parchemin est valider au point de départ mission escorte
        if ($playerSrc->get("x") == $Xstart && $playerSrc->get("y") == $Ystart) {
            
            $rayon = 5;
            $x = $playerSrc->get("x");
            $y = $playerSrc->get("y");
            $map = $playerSrc->get("map");
            if ($mission->get("Mission_Active") == 0) {
                // check si une mission est déjà entamée
                $dbp = new DBCollection("SELECT id FROM Mission WHERE id_Player=" . $playerSrc->get("id"), $db);
                if ($dbp->count() > 0)
                    return ALREADY_GOT_MISSION;
                    
                    // Création du PNJ a la validation du parchemin pour débuter la mission
                $dbesc = new DBCollection(
                    "SELECT EscortMissionRolePlay.id_Race, EscortMissionRolePlay.gender FROM EscortMissionRolePlay LEFT JOIN Mission ON Mission.id_EscortMissionRolePlay=EscortMissionRolePlay.id WHERE Mission.id=" .
                         $eqparchment->get("id_Mission"), $db);
                $idrace = $dbesc->get("id_Race");
                if ($dbesc->get("gender") == "Femme") {
                    $gender = "F";
                } else {
                    $gender = "M";
                }
                
                $npc = PNJFactory::getPNJFromLevel($idrace, 2, $db);
                
                $npc->set("x", $x);
                $npc->set("y", $y);
                $npc->set("inbuilding", $playerSrc->get("inbuilding"));
                $npc->set("room", $playerSrc->get("room"));
                $npc->set("map", $playerSrc->get("map"));
                $npc->set("id_Member", $playerSrc->get("id_Member"));
                $npc->set("id_Mission", $idmission);
                $npc->set("gender", $gender);
                
                $npc->updateHidden($db);
                $npc->updateDBr($db);
                PlayerFactory::NewATB($npc, $param2, $db);
                
                $money = $playerSrc->get("money");
                $money += $adMoneyStart;
                $playerSrc->set("money", $money);
                $playerSrc->set("ap", ($playerSrc->get("ap") - USE_PARCHMENT_AP));
                $param["MONEY"] = $adMoneyStart;
                $param["MIS_ACTIV"] = 0;
                
                $dist = distHexa($Xstart, $Ystart, $Xstop, $Ystop);
                $duration = 2 * ceil($dist / 24); // en jours
                $interval = new DateInterval("P" . $duration . "D");
                $limitdate = date_format(date_add(date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s")), $interval), "Y-m-d H:i:s");
                
                // trigger_error("on est juste avant la maj mission");
                $mission->set("Mission_Active", 1);
                $mission->set("id_Player", $playerSrc->get("id"));
                $mission->set("date", $limitdate);
                $mission->updateDBr($db);
                // $misactive=new DBCollection("UPDATE Mission SET Mission_Active=1,id_Player=".$playerSrc->get("id")." WHERE NumMission=".$id_Parchment,$db,0,0,false);
                $point = "Starting";
            } else {
                $point = "Starting";
                $param["MIS_ACTIV"] = 1;
            }
        } // si non si le parchemin est valider au point d'arrivée
elseif ($playerSrc->get("x") == $Xstop && $playerSrc->get("y") == $Ystop) {
            $rayon = 5;
            $x = $playerSrc->get("x");
            $y = $playerSrc->get("y");
            $map = $playerSrc->get("map");
            $getpnj = new DBCollection(
                "SELECT * FROM Player WHERE id_Member=" . $playerSrc->get("id_Member") . " AND status='NPC' 
		AND id_Mission=" . $idmission . " AND map=" . $map . " AND (abs(x-" . $x . ") + abs(y-" . $y . ") + abs(x+y-" . $x . "-" . $y . "))/2<=" . $rayon, $db); // On test si le pnj n'est pas trop éloigner du joueur.
            $presencePnj = $getpnj->count();
            if ($presencePnj == 0) // Si le PNJ est trop loin du joueur on envoie un message qui dit de le rapprocher
{
                // return ACTION_NOT_PRESENCE_PNJ;
                $point = "NO";
            } else // Si non on valide la fin de la mission
{
                $money = $playerSrc->get("money");
                $money += $adMoneyStop;
                $playerSrc->set("money", $money);
                
                // On supprime le PNJ une fois la mission terminé
                
                $npc = new Player();
                $npc->load($getpnj->get("id"), $db);
                PNJFactory::deleteDBnpc($npc, $param, $db, null, 0);
                // On supprime les données de mission pour éviter de générer une ereur avaec la prochaine DLA
                $delMission = new DBCollection("DELETE FROM Mission WHERE id=" . $idmission, $db, 0, 0, false);
                $delMissionCondition = new DBCollection("DELETE FROM MissionCondition WHERE idMission=" . $idmission, $db, 0, 0, false);
                $delMissionReward = new DBCollection("DELETE FROM MissionReward WHERE idMission=" . $idmission, $db, 0, 0, false);
                // On supprime le parchemin pour ne pas le réutiliser plusieur fois
                $delparch = new DBCollection("DELETE FROM Equipment WHERE id=" . $id_Parchment, $db, 0, 0, false);
                $playerSrc->set("money", $money);
                $playerSrc->set("ap", ($playerSrc->get("ap") - USE_PARCHMENT_AP));
                $param["MONEY"] = $adMoneyStop;
                $point = "Stop";
            }
        } else {
            $point = "NO";
            return ACTION_TARGET_TOO_FAR;
        }
        
        $playerSrc->updateDB($db);
        
        $param["MISSION_NAME"] = $miname;
        $param["ID"] = 1; // escorte
        $param["TYPE_ATTACK"] = USE_PARCHMENT_EVENT;
        $param["TYPE_ACTION"] = USE_PARCHMENT;
        $param["POINT"] = $point;
        $param["ERROR"] = 0;
        
        return ACTION_NO_ERROR;
    }

    public function InitMonster($db)
    {
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        $dbc = new DBCollection("SELECT * FROM City WHERE id=135 OR id=159 or id=151 or captured=2", $db);
        while (! $dbc->eof()) {
            for ($i = 0; $i < 3; $i ++) {
                
                $xfinal = $dbc->get("x") + mt_rand(- 8, - 10);
                $yfinal = $dbc->get("y") + mt_rand(- 8, - 10);
                
                if (self::freeplace($xfinal, $yfinal, 1, $db)) {
                    $npc = PNJFactory::getPNJFromLevel(mt_rand(100, 102), 1, $db);
                    $npc->set("x", $xfinal);
                    $npc->set("y", $yfinal);
                    $npc->set("map", 1);
                    $npc->set("hidden", 0);
                    $npc->set("playATB", gmdate(time()));
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                    $npc->updateDBr($db);
                }
            }
            for ($i = 0; $i < 3; $i ++) {
                $xfinal = $dbc->get("x") + mt_rand(- 8, - 10);
                $yfinal = $dbc->get("y") + mt_rand(8, 10);
                
                if (self::freeplace($xfinal, $yfinal, 1, $db)) {
                    $npc = PNJFactory::getPNJFromLevel(mt_rand(100, 102), 1, $db);
                    $npc->set("x", $xfinal);
                    $npc->set("y", $yfinal);
                    $npc->set("map", 1);
                    $npc->set("hidden", 0);
                    $npc->set("playATB", gmdate(time()));
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                    $npc->updateDBr($db);
                }
            }
            for ($i = 0; $i < 3; $i ++) {
                $xfinal = $dbc->get("x") + mt_rand(- 8, - 10);
                $yfinal = $dbc->get("y") + mt_rand(- 2, 2);
                
                if (self::freeplace($xfinal, $yfinal, 1, $db)) {
                    $npc = PNJFactory::getPNJFromLevel(mt_rand(100, 102), 1, $db);
                    $npc->set("x", $xfinal);
                    $npc->set("y", $yfinal);
                    $npc->set("map", 1);
                    $npc->set("hidden", 0);
                    $npc->set("playATB", gmdate(time()));
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                    $npc->updateDBr($db);
                }
            }
            for ($i = 0; $i < 3; $i ++) {
                $xfinal = $dbc->get("x") + mt_rand(8, 10);
                $yfinal = $dbc->get("y") + mt_rand(- 2, 2);
                
                if (self::freeplace($xfinal, $yfinal, 1, $db)) {
                    $npc = PNJFactory::getPNJFromLevel(mt_rand(100, 102), 1, $db);
                    $npc->set("x", $xfinal);
                    $npc->set("y", $yfinal);
                    $npc->set("map", 1);
                    $npc->set("hidden", 0);
                    $npc->set("playATB", gmdate(time()));
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                    $npc->updateDBr($db);
                }
            }
            
            for ($i = 0; $i < 3; $i ++) {
                $xfinal = $dbc->get("x") + mt_rand(- 2, 2);
                $yfinal = $dbc->get("y") + mt_rand(8, 10);
                
                if (self::freeplace($xfinal, $yfinal, 1, $db)) {
                    $npc = PNJFactory::getPNJFromLevel(mt_rand(100, 102), 1, $db);
                    $npc->set("x", $xfinal);
                    $npc->set("y", $yfinal);
                    $npc->set("map", 1);
                    $npc->set("hidden", 0);
                    $npc->set("playATB", gmdate(time()));
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                    $npc->updateDBr($db);
                }
            }
            
            for ($i = 0; $i < 3; $i ++) {
                $xfinal = $dbc->get("x") + mt_rand(- 2, 2);
                $yfinal = $dbc->get("y") + mt_rand(- 8, - 10);
                if (self::freeplace($xfinal, $yfinal, 1, $db)) {
                    $npc = PNJFactory::getPNJFromLevel(mt_rand(100, 102), 1, $db);
                    $npc->set("x", $xfinal);
                    $npc->set("y", $yfinal);
                    $npc->set("map", 1);
                    $npc->set("hidden", 0);
                    $npc->set("playATB", gmdate(time()));
                    PNJFactory::addBonusPNJ($npc->getObj("Modifier"), $npc->get("id_BasicRace"), $npc->get("level"), $db);
                    $npc->updateDBr($db);
                }
            }
            $dbc->next();
        }
    }

    /* *********************************************************** SONDAGE *********************************************************** */
    function answerToPoll($currMember_id, $param, $db)
    {
        $dbpoll = new DBCollection("SELECT * FROM Nacripoll_Main WHERE id=" . $param['POLL_ID'], $db);
        $participant_list = unserialize($dbpoll->get('participant_list'));
        
        if ($dbpoll->get('status') != 'active') {
            return POLL_INACTIVE;
        } elseif (in_array($currMember_id, $participant_list))
            return POLL_ALREADY_PARTICIPATED;
        else {
            // First check if it was answered correctly
            $valid_answer = true;
            $dbq = new DBCollection("SELECT id FROM Nacripoll_Question WHERE id_Nacripoll_Main=" . $dbpoll->get('id'), $db);
            $iQ = 0;
            while (! $dbq->eof()) {
                $iQ ++;
                if (! isset($param['q_' . $iQ]))
                    $valid_answer = false;
                
                $dbq->next();
            }
            if (! $valid_answer)
                return POLL_MISSING_ANSWER;
            else {
                $participant_list[] = $currMember_id;
                $dba = new DBCollection("UPDATE Nacripoll_Main SET participant_list='" . serialize($participant_list) . "' WHERE id=" . $dbpoll->get('id'), $db);
                
                $dbq = new DBCollection("SELECT id FROM Nacripoll_Question WHERE id_Nacripoll_Main=" . $dbpoll->get('id'), $db);
                $iQ = 0;
                while (! $dbq->eof()) {
                    $iQ ++;
                    $dba = new DBCollection("UPDATE Nacripoll_Answer SET votes=(votes+1) WHERE id=" . quote_smart($param['q_' . $iQ]), $db);
                    $dbq->next();
                }
                return POLL_NO_ERROR;
            }
        }
    }
}
?>
