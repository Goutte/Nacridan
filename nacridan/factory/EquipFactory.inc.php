<?php
require_once (HOMEPATH . "/economy/edef.inc.php");
require_once (HOMEPATH . "/factory/PNJFactory.inc.php");

class EquipFactory
{

    static public function getCondFromArray($arr, $fieldname, $cond)
    {
        $msg = "";
        $first = 1;
        if (is_array($arr)) {
            foreach ($arr as $key => $val) {
                if ($first)
                    $msg = $fieldname . "=" . $val;
                else
                    $msg .= " " . $cond . " " . $fieldname . "=" . $val;
                $first = 0;
            }
        } else {
            $msg = $fieldname . "=" . $arr;
        }
        return $msg;
    }

    static function getDurability($equip, $db)
    {
        return self::getDurabilityDetail($equip->get("id_BasicEquipment"), $equip->get("level"), $equip->get("durability"), $db);
    }

    static function getDurabilityDetail($idBasicEquip, $level, $durability, $db)
    {
        $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $idBasicEquip, $db);
        if ($dbc->get("id") >= 200 && $dbc->get("id") <= 206 && $dbc->get("id") != 205)
            $durTot = $level * $dbc->get("durability");
        else
            $durTot = $dbc->get("durability");
        
        $ratio = round($durability / $durTot, 2);
        if ($ratio == 1)
            return "Neuf";
        if ($ratio >= 0.7)
            return "Bon état";
        if ($ratio <= 0.7 and $ratio >= 0.3)
            return "Etat moyen";
        if ($ratio < 0.3 and $ratio > 0)
            return "Mauvais état";
        if ($ratio == 0)
            return "Cassé(e)";
    }

    static function getPriceBasicEquipment($id_BasicEquipment, $LvL, $db)
    {
        $dbbe = new DBCollection("SELECT frequency FROM BasicEquipment WHERE id=" . $id_BasicEquipment, $db);
        $PB = $dbbe->get("frequency");
        
        if ($LvL == 1)
            $price = $PB;
        else
            $price = floor($PB * pow((3 / 2), ($LvL - 1)) + pow((3 / 2), $LvL));
        
        return $price;
    }

    static function getPriceEnchant($LvL, $db)
    {
        if ($LvL == 1)
            $price = 50;
        else
            $price = floor(50 * pow((3 / 2), ($LvL - 1)) + pow((3 / 2), $LvL));
        
        return $price;
    }

    static function getPriceMajorEnchant($LvL, $db)
    {
        if ($LvL == 1)
            $price = 100;
        else
            $price = floor(100 * pow((3 / 2), ($LvL - 1)) + pow((3 / 2), $LvL));
        
        return $price;
    }

    static function getPriceEquipment($id, $db)
    {
        $dbbe = new DBCollection(
            "SELECT BasicEquipment.frequency,Equipment.extraname,Equipment.level FROM BasicEquipment LEFT JOIN Equipment ON Equipment.id_BasicEquipment=BasicEquipment.id WHERE Equipment.id=" .
                 $id, $db);
        
        $LvL = $dbbe->get("level");
        $PB = $dbbe->get("frequency");
        
        if ($dbbe->get("extraname") != "") {
            $i = 0;
            
            $template = new Template();
            $dbt = new DBCollection(
                "SELECT BasicTemplate.name AS name1, BasicTemplate.name2 as name2, " . $template->getASRenamer("Template", "TP") .
                     " FROM Template LEFT JOIN BasicTemplate ON BasicTemplate.id=Template.id_BasicTemplate  WHERE id_Equipment=" . $id . " order by Template.pos asc", $db);
            
            while (! $dbt->eof()) {
                $i ++;
                $template->DBLoad($dbt, "TP");
                
                if ($i == 1)
                    $Tlvl1 = $template->get("level");
                if ($i == 2)
                    $Tlvl2 = $template->get("level");
                
                $dbt->next();
            }
        }
        
        $price = 0;
        if ($dbbe->get("extraname") != "") {
            if ($i == 1)
                $price += floor(50 * pow((3 / 2), ($Tlvl1 - 1)) + pow((3 / 2), $Tlvl1));
            if ($i == 2)
                $price += floor(100 * pow((3 / 2), ($Tlvl2 - 1)) + pow((3 / 2), $Tlvl2));
        }
        
        if ($LvL == 1)
            $price += $PB;
        else
            $price += floor($PB * pow((3 / 2), ($LvL - 1)) + pow((3 / 2), $LvL));
        
        return $price;
    }

    static function getPriceForSaleWithReductionDurability($id, $db)
    {
        $dbbe = new DBCollection("SELECT * FROM Equipment WHERE Equipment.id=" . $id, $db);
        $basePrice = floor(self::getPriceEquipment($id, $db));
        $price = max(1, floor(($basePrice - self::getPriceToRepair($id, $db)) * ((50 - 4 * (max(0, $dbbe->get("level") - 3))) / 100)));
        
        // Vente des outils d'artisanat à 2po*level pour éviter les abus
        if (($dbbe->get("id_BasicEquipment") >= 207 and $dbbe->get("id_BasicEquipment") <= 212) or $dbbe->get("id_BasicEquipment") == 220)
            $price = 2 * $dbbe->get("level");
        
        return $price;
    }

    static function getPriceToRepair($id, $db)
    {
        $basePrice = floor(self::getPriceEquipment($id, $db) / 2);
        $dbbe = new DBCollection("SELECT level, durability, id_BasicEquipment FROM Equipment WHERE Equipment.id=" . $id, $db);
        $dbc = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $dbbe->get("id_BasicEquipment"), $db);
        if ($dbc->get("durability") == 0)
            return 0;
        
        if ($dbbe->get("id_BasicEquipment") >= 200 && $dbbe->get("id_BasicEquipment") <= 206 && $dbbe->get("id_BasicEquipment") != 205)
            $durmax = $dbbe->get("level") * $dbc->get("durability");
        else
            $durmax = $dbc->get("durability");
        
        $usure = $durmax - $dbbe->get("durability");
        return ceil(($basePrice / 200) * $usure * (60 / $durmax));
    }

    /* ************************************************************* POUR LE MENU ECONOMIE ****************************** */
    static function revokeBuilding($playerSrc, $where, &$err, $db)
    {
        $dbe = new DBCollection("DELETE FROM Building WHERE " . $where, $db, 0, 0, false);
        
        $err = "Action réussie";
    }

    static function saleEquipment($playerSrc, $eqid, $price, &$err, $db)
    {
        if ($playerSrc->get("id_BasicRace") == 263 || $playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
            $err = localize("Vous ne pouvez pas mettre en vente d'objet avec ce personnage.");
        } else {
            if ($price == 0 || $price < 0 || $price == "") {
                $err = localize("Ce prix n'est pas valide.");
            } else {
                $dbu = new DBCollection("UPDATE Equipment SET sell=" . $price . " WHERE id_Player=" . $playerSrc->get("id") . " AND id=" . $eqid, $db, 0, 0, false);
                if ($dbu->count() == 1)
                    $err = localize("Les Modifications ont été réalisées avec succès.");
                else
                    $err = localize("Vous devez choisir un objet valide.");
            }
        }
    }

    static function removeEquipFromForSale($playerSrc, $equipidarray, &$err, $db, $autoUpdate = 1)
    {
        $err = "";
        $where = self::getCondFromArray($equipidarray, "id", "OR");
        $dbu = new DBCollection("UPDATE Equipment SET sell=0 WHERE id_Player=" . $playerSrc->get("id") . " AND " . $where, $db, 0, 0, false);
        $err = localize("Les Modifications ont été réalisées avec succès.");
    }

    static function buySeveralEquipment($playerSrc, $where, $id_Seller, &$err, $db)
    {
        // $where=self::getCondFromArray($equipidarray,"id","OR");
        // echo $where;
        $dbe = new DBCollection("SELECT * FROM Equipment WHERE " . $where, $db);
        
        $price = 0;
        $id_Player = $dbe->get("id_Player");
        while (! $dbe->eof()) {
            $price += $dbe->get("sell");
            
            if ($id_Player != $dbe->get("id_Player")) {
                $err = localize("Vous ne pouvez pas acheter des objets appartement à différentes personnes en même temps");
                return;
            }
            
            $dbe->next();
        }
        
        if ($price > $playerSrc->get("money")) {
            $err = localize("Il vous est impossible d'acheter ces objets car vous n'avez pas assez d'argent.");
            return;
        }
        
        $nb = 0;
        $dbe->first();
        while (! $dbe->eof()) {
            if ($err == "") {
                $nb ++;
                self::buyEquipment($playerSrc, $dbe->get("id"), $err, $param, $db);
                $param["OBJECT_NAME"][$nb] = $param["OBJECT"];
            }
            $dbe->next();
        }
        $param["NB"] = $nb;
        $param["ERROR"] = 0;
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        BasicActionFactory::globalInfo($playerSrc, $param);
        $opponent = new Player();
        $opponent->load($id_Seller, $db);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = BUY_PJ;
        $param["TYPE_ATTACK"] = BUY_PJ_EVENT;
        $param["PRICE"] = $price;
        Event::logAction($db, BUY_PJ, $param);
        
        if ($err == "")
            $err = localize("L'achat a été réalisé avec succès.");
        $playerSrc->set("ap", $playerSrc->get("ap") - BUY_AP);
        $playerSrc->updateDB($db);
    }

    static function buyEquipment($playerSrc, $eqid, &$err, &$param, $db)
    {
        if ($playerSrc->get("id_BasicRace") == 263 || $playerSrc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
            $err = localize("Vous ne pouvez pas mettre en vente d'objet avec ce personnage.");
            return;
        }
        
        $equip = new Equipment();
        $equip->load($eqid, $db);
        $param["OBJECT"] = $equip->get("name") . " niveau " . $equip->get("level") . " " . $equip->get("extraname");
        require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");
        $good = 0;
        $param["INV"] = PlayerFactory::checkingInvfullForObject($playerSrc, $equip->get("id_EquipmentType"), $param, $db);
        if ($param["INV"] == - 1) {
            $err = localize("Certains objets n'ont pas pu être acheté car vos sacs sont pleins.") . "<br/>";
            return;
        }
        
        $dbs = new DBCollection(
            "SELECT Equipment.sell, Player.map, Player.x, Player.y, Player.id  FROM Player LEFT JOIN Equipment ON Equipment.id_Player=Player.id WHERE Equipment.id=" . $eqid, $db);
        
        if ($dbs->get("map") != $playerSrc->get("map") || distHexa($dbs->get("x"), $dbs->get("y"), $playerSrc->get("x"), $playerSrc->get("y")) > 1) {
            $err = localize("Certains objets n'ont pas pu être acheté car vous vous trouvez trop loin de la personne qui les vend.");
            return;
        }
        if ($dbs->get("sell") > $playerSrc->get("money")) {
            $err = localize("Il vous est impossible d'acheter cet objet car vous n'avez pas assez d'argent.");
            return;
        }
        
        $equip->set("x", $playerSrc->get("x"));
        $equip->set("y", $playerSrc->get("y"));
        $equip->set("sell", 0);
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->set("id_Equipment\$bag", playerFactory::getIdBagByNum($playerSrc->get("id"), $param["INV"], $db));
        
        // Si l'objet est un sac, un carquois ou une ceinture, tous les objets qu'il contient sont donnés
        if (($equip->get("id_EquipmentType") >= 41 and $equip->get("id_EquipmentType") <= 44) or $equip->get("id_EquipmentType") == 20 or $equip->get("id_EquipmentType") == 46)
            $dbe = new DBCollection("UPDATE Equipment SET id_Player=" . $playerSrc->get("id") . " WHERE id_Equipment\$bag=" . $equip->get("id"), $db, 0, 0, false);
        
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->updateDB($db);
        
        $dbt = new DBCollection("UPDATE Player SET money=money+" . $dbs->get("sell") . " WHERE id=" . $dbs->get("id"), $db, 0, 0, false);
        $founded = $dbt->count();
        if ($founded) {
            $playerSrc->set("money", $playerSrc->get("money") - $dbs->get("sell"));
        } else {
            $err = localize("Certaines objets n'ont pas pu être acheté car l'argent n'a pas pu être donné au vendeur. Essayez à nouveau.");
            return;
        }
        
        $playerSrc->updateDB($db);
    }
}
?>
