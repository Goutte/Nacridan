<?php
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/class/Equipment.inc.php");
require_once (HOMEPATH . "/class/StaticModifierProfil.inc.php");
require_once (HOMEPATH . "/class/BM.inc.php");
require_once (HOMEPATH . "/class/Upgrade.inc.php");
require_once (HOMEPATH . "/class/BasicRace.inc.php");
require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
require_once (HOMEPATH . "/factory/EquipFactory.inc.php");
require_once (HOMEPATH . "/factory/CityFactory.inc.php");
require_once (HOMEPATH . "/factory/MailFactory.inc.php");
require_once (HOMEPATH . "/factory/MissionFactory.inc.php");
require_once (HOMEPATH . "/lib/MapInfo.inc.php");
require_once (HOMEPATH . "/factory/PlayerFactory.inc.php");

class InsideBuildingFactory
{

    /* *************************************************************** FONCTIONS AUXILLIAIRES **************************************** */
    static public function initSchoolList($id_BasicBuilding)
    {
        $schoolList = array();
        if ($id_BasicBuilding == 6)
            $schoolList[SCHOOL_BASIC_WARRIOR] = 0;
        if ($id_BasicBuilding == 7)
            $schoolList[SCHOOL_BASIC_MAGIC] = 0;
        return $schoolList;
    }

    static public function initBuildingProfit($id_BasicBuilding, $db)
    {
        $profit = array();
        
        $dbt = new DBCollection("SELECT * FROM BuildingAction WHERE id_BasicBuilding=" . $id_BasicBuilding, $db);
        
        while (! $dbt->eof()) {
            $profit[$dbt->get("name")] = $dbt->get("profit");
            $dbt->next();
        }
        
        return $profit;
    }

    static public function getCondFromArray($arr, $fieldname, $cond)
    {
        $msg = "";
        $first = 1;
        if (is_array($arr)) {
            foreach ($arr as $key => $val) {
                if ($first)
                    $msg = $fieldname . "=" . $val;
                else
                    $msg .= " " . $cond . " " . $fieldname . "=" . $val;
                $first = 0;
            }
        } else {
            $msg = $fieldname . "=" . $arr;
        }
        return $msg;
    }

    static public function globalInfo($playerSrc, &$param)
    {
        $param["PLAYER_NAME"] = $playerSrc->get("name");
        if ($param["PLAYER_NAME"] == "")
            $param["PLAYER_NAME"] = $playerSrc->get("racename");
        $param["PLAYER_RACE"] = $playerSrc->get("id_BasicRace");
        $param["PLAYER_ID"] = $playerSrc->get("id");
        $param["PLAYER_GENDER"] = $playerSrc->get("gender");
        
        $param["PLAYER_LEVEL"] = $playerSrc->get("level");
        $param["MEMBER_ID"] = $playerSrc->get("id_Member");
        
        if ($playerSrc->get("status") == "NPC")
            $param["PLAYER_IS_NPC"] = 1;
        else
            $param["PLAYER_IS_NPC"] = 0;
    }

    static function prerequisitesForLearningAbility($idplayer, $idschool, $level, $db)
    {
        if ($level == 0)
            return true;
        
        $dba = new DBCollection(
            "SELECT Ability.skill, BasicAbility.level FROM Ability LEFT JOIN BasicAbility ON BasicAbility.id = Ability.id_BasicAbility WHERE (BasicAbility.school=" . $idschool .
                 " OR BasicAbility.school=9) AND BasicAbility.level=" . ($level - 1) . " AND Ability.id_Player=" . $idplayer, $db);
        
        $compteur = 0;
        while (! $dba->eof()) {
            if ($dba->get("skill") >= 80 && $dba->get("level") == 0)
                return true;
            
            if ($dba->get("skill") >= 90) {
                $compteur += 1;
                if ($compteur >= 2)
                    return true;
            }
            $dba->next();
        }
        
        $dba = new DBCollection(
            "SELECT MagicSpell.skill, BasicMagicSpell.level FROM MagicSpell LEFT JOIN BasicMagicSpell ON BasicMagicSpell.id = MagicSpell.id_BasicMagicSpell WHERE (BasicMagicSpell.school=" .
                 $idschool . " OR BasicMagicSpell.school=10) AND BasicMagicSpell.level=" . ($level - 1) . " AND MagicSpell.id_Player=" . $idplayer, $db);
        $compteur = 0;
        while (! $dba->eof()) {
            if ($dba->get("skill") >= 80 && $dba->get("level") == 0)
                return true;
            
            if ($dba->get("skill") >= 90) {
                $compteur += 1;
                if ($compteur >= 2)
                    return true;
            }
            $dba->next();
        }
        
        return false;
    }

    static function prerequisitesForLearningMagicSpell($idplayer, $idschool, $level, $db)
    {
        if ($level == 0)
            return true;
        
        $dba = new DBCollection(
            "SELECT MagicSpell.skill, BasicMagicSpell.level FROM MagicSpell LEFT JOIN BasicMagicSpell ON BasicMagicSpell.id = MagicSpell.id_BasicMagicSpell WHERE (BasicMagicSpell.school=" .
                 $idschool . " OR BasicMagicSpell.school=10) AND BasicMagicSpell.level=" . ($level - 1) . " AND MagicSpell.id_Player=" . $idplayer, $db);
        
        $compteur = 0;
        while (! $dba->eof()) {
            if ($dba->get("skill") >= 80 && $dba->get("level") == 0)
                return true;
            
            if ($dba->get("skill") >= 90) {
                $compteur += 1;
                if ($compteur >= 2)
                    return true;
            }
            $dba->next();
        }
        
        $dba = new DBCollection(
            "SELECT Ability.skill, BasicAbility.level FROM Ability LEFT JOIN BasicAbility ON BasicAbility.id = Ability.id_BasicAbility WHERE (BasicAbility.school=" . $idschool .
                 " OR BasicAbility.school=9) AND BasicAbility.level=" . ($level - 1) . " AND Ability.id_Player=" . $idplayer, $db);
        
        $compteur = 0;
        while (! $dba->eof()) {
            if ($dba->get("skill") >= 80 && $dba->get("level") == 0)
                return true;
            
            if ($dba->get("skill") >= 90) {
                $compteur += 1;
                if ($compteur >= 2)
                    return true;
            }
            $dba->next();
        }
        
        return false;
    }

    static function prerequisitesForLearningTalent($playerSrc, $id_BasicTalent, $db)
    {
        $dbT = new DBCollection("SELECT * FROM Talent WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicTalent=" . $id_BasicTalent, $db);
        if ($dbT->eof())
            return true;
        
        $dbK = new DBCollection("SELECT * FROM Knowledge WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicTalent=" . $id_BasicTalent, $db);
        if (($dbT->get("skill") - 50) / 5 >= ($dbK->count() - 1))
            return true;
        
        return false;
    }

    /* *************************************************************** TEMPLE **************************************************** */
    static function teleport($playerSrc, $tid, &$param, $db, $autoUpdate = 1)
    {
        $temple = new Building();
        $temple->load($tid, $db);
        
        if (! BasicActionFactory::canEnterTemple($playerSrc, $playerSrc->get("inbuilding"), $db))
            return ACTION_NOT_ALLOWED;
            
            // Test d'autorisation d'entrée dans la ville par téléportation
        $city = new City();
        $city->load($temple->get("id_City"), $db);
        $allowed = 0;
        
        if ($city->get("captured") < 4)
            $allowed = 1;
        else {
            $dbc = new DBCollection("SELECT * FROM Player WHERE inbuilding=" . $tid . " AND id_City=" . $temple->get("id_City") . " AND racename='Prêtre'", $db);
            if ($dbc->eof())
                return TELEPORT_PRIEST_DEAD;
                
                // Si c'est le protecteur
            if (($playerSrc->get("id") == $city->get("id_Player")))
                $allowed = 1;
                
                // Si ouvert à tout le monde
            if ($city->get("accessTemple") == 1)
                $allowed = 1;
                
                /*
             * //Si fermé à tout le monde et hors de la ville
             * if($city->get("accessTemple") == 2)
             * $allowed = 0;
             */
                
            // Si ouvert aux alliés uniquement
            if (($city->get("accessTemple") % 3) == 0) {
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $city->get("id_Player"), $db);
                // Même Team
                if ($city->get("accessTemple") % 10 == 3) {
                    if ($dbp->get("id_Team") == $playerSrc->get("id_Team") && $dbp->get("id_Team") != 0)
                        $allowed = 1;
                }
                // Alliances Personnelles PJ
                if (($city->get("accessTemple") / 10) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliancePJ WHERE type='Allié' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Player\$dest=" . $playerSrc->get("id"), $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
                
                // Alliances Personnelles Ordre
                if (($city->get("accessTemple") / 100) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliance WHERE type='Allié' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Team=" . $playerSrc->get("id_Team"), $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
                
                // Alliances Ordre PJ
                if (($city->get("accessTemple") / 1000) % 10 == 3) {
                    $dba = new DBCollection("SELECT * FROM TeamAlliancePJ WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerSrc->get("id"), 
                        $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
                
                // Alliances Ordre PJ
                if (($city->get("accessTemple") / 10000) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM TeamAlliance WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerSrc->get("id_Team"), $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
            }
            
            // Si fermé à mes enemis
            if ($city->get("accessTemple") == 4) {
                $allowed = 1;
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $city->get("id_Player"), $db);
                // Alliances Personnelles PJ
                $dba = new DBCollection(
                    "SELECT * FROM PJAlliancePJ WHERE type='Ennemi' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Player\$dest=" . $playerSrc->get("id"), $db);
                if (! $dba->eof())
                    $allowed = 0;
                    // Alliances Personnelles PJ
                $dba = new DBCollection("SELECT * FROM PJAlliance WHERE type='Ennemi' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Team=" . $playerSrc->get("id_Team"), 
                    $db);
                if (! $dba->eof())
                    $allowed = 0;
                    // Alliances Ordre PJ
                $dba = new DBCollection("SELECT * FROM TeamAlliancePJ WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerSrc->get("id"), 
                    $db);
                if (! $dba->eof())
                    $allowed = 0;
                    // Alliances Ordre PJ
                $dba = new DBCollection(
                    "SELECT * FROM TeamAlliance WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerSrc->get("id_Team"), $db);
                if (! $dba->eof())
                    $allowed = 0;
            }
        }
        
        if ($allowed) {
            $param["ERROR"] = 0;
            $param["XNEW"] = $temple->get("x");
            $param["YNEW"] = $temple->get("y");
            $distTP = distHexa($playerSrc->get("x"), $playerSrc->get("y"), $param["XNEW"], $param["YNEW"]);
            $param["PRICE"] = floor(TELEPORT_PRICE * $distTP + CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), TELEPORT, $db));
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), TELEPORT, $db);
            
            self::globalInfo($playerSrc, $param);
            if ($playerSrc->get("ap") < TELEPORT_AP)
                return ACTION_NOT_ENOUGH_AP;
            
            if ($playerSrc->get("money") < $param["PRICE"])
                return NOT_ENOUGH_MONEY_ERROR;
            
            if ($playerSrc->get("inbuilding") == 0)
                return ERROR_NOT_IN_BUILDING;
            
            $param["TAXE"] = 0;
            $param["TYPE_ACTION"] = TELEPORT;
            $param["TYPE_ATTACK"] = TELEPORT_EVENT;
            
            // pour le livre des comptes de la cité si elle est contrôlé
            $dbc = new DBCollection(
                "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
            $date = gmdate("Y-m-d H:i:s");
            
            if ($dbc->get("captured") > 3)
                $dbu = new DBCollection(
                    "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                         $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . "," . $profit .
                         ",'" . $date . "')", $db, 0, 0, false);
            
            $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
            
            $param["XOLD"] = $playerSrc->get("x");
            $param["YOLD"] = $playerSrc->get("y");
            
            $playerSrc->set("x", $temple->get("x"));
            $playerSrc->set("y", $temple->get("y"));
            $playerSrc->set("inbuilding", $temple->get("id"));
            $playerSrc->set("room", ENTRANCE_ROOM);
            $playerSrc->set("money", $playerSrc->get("money") - $param["PRICE"]);
            $playerSrc->set("ap", $playerSrc->get("ap") - TELEPORT_AP);
            $playerSrc->updateHidden($db);
            
            BasicActionFactory::updateInfoAfterMove($temple->get("x"), $temple->get("y"), $playerSrc, $db);
            
            if ($autoUpdate)
                $playerSrc->updateDB($db);
            
            return ACTION_NO_ERROR;
        } else
            return TEMPLE_TELEPORT_NOT_ALLOWED;
    }

    static function templeHeal($playerSrc, $tlevel, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        if ($dbc->get("captured") > 3) {
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), TEMPLE_HEAL, $db);
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), TEMPLE_HEAL, $db);
        } else {
            $price = min($playerSrc->get("level"), 5);
            $profit = 1;
        }
        
        if ($playerSrc->get("ap") < TEMPLE_HEAL_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! BasicActionFactory::canEnterTemple($playerSrc, $playerSrc->get("inbuilding"), $db))
            return ACTION_NOT_ALLOWED;
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = TEMPLE_HEAL;
        $param["TYPE_ATTACK"] = TEMPLE_HEAL_EVENT;
        
        $param["PRICE"] = $price;
        $param["SPELL_LEVEL"] = 6 * $tlevel;
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, ceil($param["SPELL_LEVEL"]));
        $param["HP_POTENTIEL"] = $modifier->getScoreWithNoBM("hp");
        $param["HP_HEAL"] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - TEMPLE_HEAL_AP));
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        $playerSrc->updateDB($db);
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" . $date .
                     "')", $db, 0, 0, false);
        return ACTION_NO_ERROR;
    }

    static function templeBlessing($playerSrc, $tlevel, &$param, $db, $autoUpdate = 1)
    {
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), TEMPLE_BLESSING, $db);
        $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), TEMPLE_BLESSING, $db);
        self::globalInfo($playerSrc, $param);
        if ($playerSrc->get("ap") < TEMPLE_BLESSING_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), TEMPLE_BLESSING, $db))
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! BasicActionFactory::canEnterTemple($playerSrc, $playerSrc->get("inbuilding"), $db))
            return ACTION_NOT_ALLOWED;
        
        $param["TYPE_ACTION"] = TEMPLE_BLESSING;
        $param["TYPE_ATTACK"] = TEMPLE_BLESSING_EVENT;
        $param["SPELL_LEVEL"] = 6 * $tlevel;
        $param["PRICE"] = $price;
        $param["TAXE"] = 0;
        $param["ERROR"] = 0;
        
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $dbbm = new DBCollection(
            "UPDATE BM SET value=value-" . $param["SPELL_LEVEL"] . ", level = level-" . $param["SPELL_LEVEL"] . " WHERE effect='NEGATIVE' AND id_Player=" . $playerSrc->get("id"), 
            $db, 0, 0, false);
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - TEMPLE_BLESSING_AP));
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
            
            // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" . $date .
                     "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    static function templeResurrect($playerSrc, &$param, $db, $autoUpdate = 1)
    {
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), TEMPLE_RESURRECT, $db);
        $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), TEMPLE_RESURRECT, $db);
        
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("money") < CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), TEMPLE_RESURRECT, $db))
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! BasicActionFactory::canEnterTemple($playerSrc, $playerSrc->get("inbuilding"), $db))
            return ACTION_NOT_ALLOWED;
            
            // Test d'autorisation de placer son lieu de résurection dans ce temple
        $dbb = new DBCollection("SELECT id,id_City,X,Y FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        
        $city = new City();
        $city->load($dbb->get("id_City"), $db);
        $allowed = 0;
        
        if ($city->get("captured") < 4)
            $allowed = 1;
        else {
            $dbc = new DBCollection("SELECT * FROM Player WHERE inbuilding=" . $dbb->get("id") . " AND id_City=" . $dbb->get("id_City") . " AND racename='Prêtre'", $db);
            if ($dbc->eof())
                return RESURRECT_PRIEST_DEAD;
                
                // Si c'est le protecteur
            if (($playerSrc->get("id") == $city->get("id_Player")))
                $allowed = 1;
                
                // Si ouvert à tout le monde
            if ($city->get("accessTemple") == 1)
                $allowed = 1;
                
                /*
             * //Si fermé à tout le monde et hors de la ville
             * if($city->get("accessTemple") == 2)
             * $allowed = 0;
             */
                
            // Si ouvert aux alliés uniquement et hors de la ville
            if (($city->get("accessTemple") % 3) == 0) {
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $city->get("id_Player"), $db);
                // Même Team
                if ($city->get("accessTemple") % 10 == 3) {
                    $dba = new DBCollection("SELECT * FROM Player WHERE id=" . $city->get("id_Player"), $db);
                    if ($dba->get("id_Team") == $playerSrc->get("id_Team"))
                        $allowed = 1;
                }
                // Alliances Personnelles PJ
                if (($city->get("accessTemple") / 10) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliancePJ WHERE type='Allié' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Player\$dest=" . $playerSrc->get("id"), $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
                
                // Alliances Personnelles Ordre
                if (($city->get("accessTemple") / 100) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM PJAlliance WHERE type='Allié' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Team=" . $playerSrc->get("id_Team"), $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
                
                // Alliances Ordre PJ
                if (($city->get("accessTemple") / 1000) % 10 == 3) {
                    $dba = new DBCollection("SELECT * FROM TeamAlliancePJ WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerSrc->get("id"), 
                        $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
                
                // Alliances Ordre PJ
                if (($city->get("accessTemple") / 10000) % 10 == 3) {
                    $dba = new DBCollection(
                        "SELECT * FROM TeamAlliance WHERE type='Allié' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerSrc->get("id_Team"), $db);
                    if (! $dba->eof())
                        $allowed = 1;
                }
            }
            
            // Si fermé à mes enemis
            if ($city->get("accessTemple") == 4) {
                $allowed = 1;
                $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $city->get("id_Player"), $db);
                // Alliances Personnelles PJ
                $dba = new DBCollection(
                    "SELECT * FROM PJAlliancePJ WHERE type='Ennemi' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Player\$dest=" . $playerSrc->get("id"), $db);
                if (! $dba->eof())
                    $allowed = 0;
                    // Alliances Personnelles PJ
                $dba = new DBCollection("SELECT * FROM PJAlliance WHERE type='Ennemi' AND id_Player\$src =" . $city->get("id_Player") . " AND id_Team=" . $playerSrc->get("id_Team"), 
                    $db);
                if (! $dba->eof())
                    $allowed = 0;
                    // Alliances Ordre PJ
                $dba = new DBCollection("SELECT * FROM TeamAlliancePJ WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Player=" . $playerSrc->get("id"), 
                    $db);
                if (! $dba->eof())
                    $allowed = 0;
                    // Alliances Ordre PJ
                $dba = new DBCollection(
                    "SELECT * FROM TeamAlliance WHERE type='Ennemi' AND id_Team\$src =" . $dbp->get("id_Team") . " AND id_Team\$dest=" . $playerSrc->get("id_Team"), $db);
                if (! $dba->eof())
                    $allowed = 0;
            }
        }
        
        if ($allowed) {
            $param["TYPE_ACTION"] = TEMPLE_RESURRECT;
            $param["TYPE_ATTACK"] = TEMPLE_RESURRECT_EVENT;
            $param["PRICE"] = $price;
            $param["TAXE"] = 0;
            $param["ERROR"] = 0;
            $param["X"] = $dbb->get("X");
            $param["Y"] = $dbb->get("Y");
            
            // taxe
            $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
            
            $playerSrc->set("resurrectid", $playerSrc->get("inbuilding"));
            $playerSrc->set("money", ($playerSrc->get("money") - $param["PRICE"]));
            
            // pour le livre des comptes de la cité si elle est contrôlé
            $dbc = new DBCollection(
                "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
            $date = gmdate("Y-m-d H:i:s");
            if ($dbc->get("captured") > 3)
                $dbu = new DBCollection(
                    "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                         $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" .
                         $date . "')", $db, 0, 0, false);
            
            if ($autoUpdate)
                $playerSrc->updateDB($db);
            
            return ACTION_NO_ERROR;
        } else
            return TEMPLE_RESURECTION_NOT_ALLOWED;
    }

    static function templePalace($playerSrc, $x, $y, $NameVillage, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < TEMPLE_PALACE_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < TEMPLE_PALACE_PRICE)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! BasicActionFactory::freePlace($x, $y, $playerSrc->get("map"), $db, 1) || distHexa($playerSrc->get("x"), $playerSrc->get("y"), $x, $y) > 3)
            return PLACE_FIELD_INVALID;
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbu = new DBCollection(
            "UPDATE City SET id_Player=" . $playerSrc->get("id") . ", captured=(case when captured=3 then 4 else 5 end), loyalty=50, door=1 WHERE id=" . $dbc->get("id_City"), $db, 
            0, 0, false);
        $dbp = new DBCollection("SELECT * FROM BasicBuilding WHERE id=11", $db);
        $sp = CityFactory::getBuildingSP(11, $db);
        
        // $dbi = new DBCollection("INSERT INTO Building (name, id_BasicBuilding, id_City, id_Player, level, sp, currsp, x, y, map, repair, progress) VALUES ('Palais du Gouverneur', 11, ".$dbc->get("id_City").",".$playerSrc->get("id").", 1, ".$sp.",".$sp.",".$x.",".$y.",".$playerSrc->get("map").", 1, 0)",$db,0,0,false);
        $palais = new Building();
        $palais->set("x", $x);
        $palais->set("y", $y);
        $palais->set("map", $playerSrc->get("map"));
        $palais->set("id_Player", $playerSrc->get("id"));
        $palais->set("id_City", $dbc->get("id_City"));
        $palais->set("id_BasicBuilding", 11);
        $palais->set("level", 1);
        $palais->set("sp", $sp);
        $palais->set("name", "Palais du Gouverneur");
        $palais->set("currsp", $sp);
        $palais->set("repair", 1);
        $palais->set("progress", 1);
        $id_Palais = $palais->addDB($db);
        
        $param["NAME"] = $dbp->get("name");
        $param["PRICE"] = TEMPLE_PALACE_PRICE;
        $param["CONSTRUCTION_DURATION_MIN"] = ceil($sp / CONSTRUCTION_SPEED_MAX);
        $param["CONSTRUCTION_DURATION_MAX"] = ceil($sp / CONSTRUCTION_SPEED_MIN);
        $param["X"] = $x;
        $param["Y"] = $y;
        
        $dbb = new DBCollection("SELECT * FROM Building WHERE id_City=" . $dbc->get("id_City"), $db);
        while (! $dbb->eof()) {
            if ($dbb->get("id_BasicBuilding") == 10)
                $money = - 1;
            else
                $money = 0;
            
            $dbu = new DBCollection(
                "UPDATE Building SET money=" . $money . ", profit='" . addslashes(serialize(self::initBuildingProfit($dbb->get("id_BasicBuilding"), $db))) . "' WHERE id=" .
                     $dbb->get("id"), $db, 0, 0, false);
            $dbb->next();
        }
        
        /* nommer le village à la construction du palais. */
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $village = quote_smart($NameVillage);
        
        $dbu = new DBCollection("UPDATE City SET name='" . $village . "' WHERE id=" . $dbc->get("id_City"), $db, 0, 0, false);
        /* fin */
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - TEMPLE_PALACE_AP));
        $playerSrc->set("money", ($playerSrc->get("money") - TEMPLE_PALACE_PRICE));
        
        /* ------------------------------------------On trace un cercle autour des villages capturer-------------------------------------------- */
        // $dbcapture=new DBCollection("SELECT * FROM City WHERE captured=4",$db);
        /* Tente d'ouvrir l'image */
        $im = @imagecreatefrompng('..' . MAPS_DYNAMIC_FOLDER . '/mapghostcitycaptured01.png');
        $gray = imagecolorallocate($im, 0, 0, 0);
        
        /* Traitement en cas d'échec */
        if (! $im) {
            /* Création d'une image vide */
            $im = imagecreatetruecolor(150, 30);
            $bgc = imagecolorallocate($im, 0, 255, 0);
            $tc = imagecolorallocate($im, 0, 0, 0);
            
            imagefilledrectangle($im, 0, 0, 150, 30, $bgc);
            // imageellipse($im,352,183,5,5,$gray);
            
            /* On y affiche un message d'erreur */
            imagestring($im, 1, 5, 5, 'Erreur de chargement ' . $imgname, $tc);
        } else {
            // On trace un cercle autour de tous les villages capturés
            
            imageellipse($im, $x, $y, 15, 15, $gray); // imageellipse fonction PHP permettant de dessiner une forme géométrique éllipse.
                                                          // Voir doc pour plus d'info : http://www.php.net/manual/fr/function.imageellipse.php
        }
        imagepng($im, '..' . MAPS_DYNAMIC_FOLDER . '/mapghostcitycaptured01.png'); // création de la nouvelle image
        imagedestroy($im); // on l'ibère l'espace mémoire allouer à la création
        
        /* creation d'un garde à la construction du palais */
        self::createGardForACity($dbc->get("id_City"), $id_Palais, $db);
        
        /* creation d'un prêtre dans le temple à la construction du palais */
        self::createPriestForACity($dbc->get("id_City"), $db);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = TEMPLE_PALACE;
        $param["TYPE_ATTACK"] = TEMPLE_PALACE_EVENT;
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** HOUSE/HOSTEL **************************************************** */
    static function bedroomDeposit($playerSrc, $moneydep, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $param["MONEYDEP"] = $moneydep;
        
        $dbB = new DBCollection("SELECT id,money FROM Building WHERE id=" . $playerSrc->get("inbuilding") . " and id_Player = " . $playerSrc->get("id"), $db, 0, 0);
        if ($dbB->eof())
            return ACTION_NOT_ALLOWED;
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BEDROOM_DEPOSIT;
        $param["TYPE_ATTACK"] = BEDROOM_DEPOSIT_EVENT;
        
        if ($playerSrc->get("money") < $moneydep)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($moneydep) or (is_numeric($moneydep) and $moneydep < 0))
            return MONEY_FIELD_INVALID;
        
        $playerSrc->set("money", ($playerSrc->get("money") - $moneydep));
        // mise à jour caisse de la maison
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $moneydep . " WHERE id=" . $playerSrc->get("inbuilding") . " and id_Player = " . $playerSrc->get("id"), $db, 0, 
            0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function bedroomWithdraw($playerSrc, $money, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $param["MONEY"] = $money;
        
        $dbB = new DBCollection("SELECT id,money FROM Building WHERE id=" . $playerSrc->get("inbuilding") . " and id_Player = " . $playerSrc->get("id"), $db, 0, 0);
        if ($dbB->eof())
            return ACTION_NOT_ALLOWED;
        
        if ($dbB->get("money") < $money)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($money) or (is_numeric($money) and $money < 0))
            return MONEY_FIELD_INVALID;
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BEDROOM_WITHDRAW;
        $param["TYPE_ATTACK"] = BEDROOM_WITHDRAW_EVENT;
        $playerSrc->set("money", ($playerSrc->get("money") + $money));
        
        // mise à jour caisse de la maison
        $dbu = new DBCollection("UPDATE Building SET money=money-" . $money . " WHERE id=" . $playerSrc->get("inbuilding") . " and id_Player = " . $playerSrc->get("id"), $db, 0, 0, 
            false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function bedroomSleeping($playerSrc, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = BEDROOM_SLEEPING;
        
        if ($playerSrc->get("ap") < SLEEPING_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["HPGAIN"] = 0;
        $param["TYPE_ATTACK"] = HOUSE_SLEEPING_EVENT;
        
        $dbb = new DBCollection("SELECT id_BasicBuilding FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - SLEEPING_AP));
        if ($dbb->get("id_BasicBuilding") == 1) // si on est dans une auberge
{
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), BEDROOM_SLEEPING, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), BEDROOM_SLEEPING, $db);
            $playerSrc->set("money", ($playerSrc->get("money") - $price));
            $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
            
            // pour le livre des comptes de la cité si elle est contrôlé
            $dbc = new DBCollection(
                "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
            $date = gmdate("Y-m-d H:i:s");
            if ($dbc->get("captured") > 3 && $dbb->get("id_BasicBuilding") == 1)
                $dbu = new DBCollection(
                    "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                         $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" .
                         $date . "')", $db, 0, 0, false);
            
            $param["TYPE_ATTACK"] = HOSTEL_SLEEPING_EVENT;
        }
        
        $hpgain = $playerSrc->get("hp") - $playerSrc->get("currhp");
        $param["HPGAIN"] = PlayerFactory::addHP($playerSrc, $hpgain, $db, 0);
        
        $param["ERROR"] = ACTION_NO_ERROR;
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** HOSTEL **************************************************** */
    static function hostelDrink($playerSrc, &$param, $db, $autoUpdate = 1)
    {
        if ($playerSrc->get("inbuilding") != 0) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), HOSTEL_DRINK, $db);
            $param["PRICE"] = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_DRINK, $db);
            self::globalInfo($playerSrc, $param);
            
            if ($playerSrc->get("ap") < HOSTEL_DRINK_AP)
                return ACTION_NOT_ENOUGH_AP;
            
            if ($playerSrc->get("money") < CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_DRINK, $db))
                return NOT_ENOUGH_MONEY_ERROR;
            
            $playerSrc->set("money", $playerSrc->get("money") - $param["PRICE"]);
            // taxe
            $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        }
        
        if (rand(1, 2) > 1) {
            $effect = 'NEGATIVE';
            
            $bm = array(
                1,
                3,
                6,
                7
            );
            $id_StaticModifier = $bm[array_rand($bm)];
            $text = "-1D6 de ";
        } else {
            $effect = 'POSITIVE';
            $text = "+1D6 de ";
            $id_StaticModifier = mt_rand(13, 16);
        }
        
        $bm_name = array();
        $bm_name[1] = "vitesse";
        $bm_name[3] = "MM";
        $bm_name[6] = "dextérité";
        $bm_name[7] = "force";
        $bm_name[13] = "force";
        $bm_name[14] = "dextérité";
        $bm_name[15] = "vitesse";
        $bm_name[16] = "MM";
        $param["TEXT"] = $text . $bm_name[$id_StaticModifier];
        
        $date = gmdate("Y-m-d H:i:s");
        
        $dbi = new DBCollection(
            "INSERT INTO BM (id_StaticModifier_BM, id_Player,id_Player\$src, level, effect, name,life,date) VALUES (" . $id_StaticModifier . " , " . $playerSrc->get("id") . "," .
                 $playerSrc->get("id") . ",2, '" . $effect . "', 'Boisson' ,0,'" . $date . "')", $db, 0, 0, false);
        
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db);
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - HOSTEL_DRINK_AP));
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = HOSTEL_DRINK;
        $param["TYPE_ATTACK"] = HOSTEL_DRINK_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . "," . $profit . ",'" .
                     $date . "')", $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function hostelChat($playerSrc, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < HOSTEL_CHAT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["MISSION"] = 0;
        $param["JET"] = rand(1, 100);
        
        $dbb = new DBCollection("SELECT level,x,y,id_City FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $city = $dbb->get("id_City");
        $x = $dbb->get("x");
        $y = $dbb->get("y");
        $dbc = new DBCollection("SELECT map FROM City WHERE id=" . $city, $db);
        $map = $dbc->get("map");
        $blevel = $dbb->get("level");
        $param["LIMIT"] = max(0, min(100, 100 * $playerSrc->get("merchant_level") - 30 + 2 * $blevel));
        
        if ($param["JET"] <= $param["LIMIT"]) {
            $param["MISSION"] = 1;
            MissionFactory::getRndParchment($blevel, $x, $y, $map, $playerSrc->get("id"), $db);
        }
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - HOSTEL_CHAT_AP));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = HOSTEL_CHAT;
        $param["TYPE_ATTACK"] = HOSTEL_CHAT_EVENT;
        
        return ACTION_NO_ERROR;
    }

    static function hostelRound($playerSrc, &$param, $db, $autoUpdate = 1)
    {
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), HOSTEL_ROUND, $db);
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < HOSTEL_ROUND_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_ROUND, $db))
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["MISSION"] = 0;
        $param["JET"] = rand(1, 100);
        $param["PRICE"] = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_ROUND, $db);
        $param["TAXE"] = 0;
        
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $dbb = new DBCollection("SELECT level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $blevel = $dbb->get("level");
        $param["LIMIT"] = 50 + 5 * $blevel;
        
        if ($param["JET"] < $param["LIMIT"])
            $param["MISSION"] = 1;
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - HOSTEL_ROUND_AP));
        $playerSrc->set("money", $playerSrc->get("money") - $param["PRICE"]);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = HOSTEL_ROUND;
        $param["TYPE_ATTACK"] = HOSTEL_ROUND_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . "," . $profit . ",'" .
                     $date . "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    static function hostelNews($playerSrc, $title, $content, $create, &$param, $db, $autoUpdate = 1)
    {
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), HOSTEL_NEWS, $db);
        self::globalInfo($playerSrc, $param);
        $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_NEWS, $db);
        
        if ($playerSrc->get("ap") < HOSTEL_NEWS_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($create == 'yes') {
            $dbB = new DBCollection("SELECT id_City,level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
            
            $news = new News();
            $news->set("id_Player", $playerSrc->get("id"));
            $news->set("id_Building", $playerSrc->get("inbuilding"));
            $news->set("x", $playerSrc->get("x"));
            $news->set("y", $playerSrc->get("y"));
            $news->set("map", $playerSrc->get("map"));
            
            $news->set("level", $dbB->get("level"));
            $news->set("title", base64_decode($title));
            $news->set("content", base64_decode($content));
            $news->set("date", gmdate("Y-m-d h:m:s"));
            
            if ($playerSrc->get("authlevel") > 10) {
                $news->set("type", 1);
            }
            
            $news->updateDB($db);
            
            $playerSrc->set("ap", ($playerSrc->get("ap") - HOSTEL_NEWS_AP));
            $playerSrc->set("money", ($playerSrc->get("money") - $price));
            
            $param["PRICE"] = $price;
            $param["TAXE"] = 0;
            
            $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
            
            $param["ERROR"] = 0;
            $param["TYPE_ACTION"] = HOSTEL_NEWS;
            $param["TYPE_ATTACK"] = HOSTEL_NEWS_EVENT;
            
            // pour le livre des comptes de la cité si elle est contrôlé
            $dbc = new DBCollection(
                "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
            $date = gmdate("Y-m-d H:i:s");
            if ($dbc->get("captured") > 3)
                $dbu = new DBCollection(
                    "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                         $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" .
                         $date . "')", $db, 0, 0, false);
            
            if ($autoUpdate)
                $playerSrc->updateDB($db);
            
            return ACTION_NO_ERROR;
        } else {
            return NEWS_DO_NOT_CREATE;
        }
    }

    /* ------------------------------------------------ Traitement message commercial ----------------------------------------------- */
    static function hostelTrade($playerSrc, $title, $content, &$param, $db, $autoUpdate = 1)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), HOSTEL_TRADE, $db);
        self::globalInfo($playerSrc, $param);
        $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_TRADE, $db);
        
        if ($playerSrc->get("ap") < HOSTEL_TRADE_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $x = $playerSrc->get("x");
        $y = $playerSrc->get("y");
        $map = $playerSrc->get("map");
        $level = $dbc->get("level");
        $rayon = (30 + (30 * $level));
        
        $pjActif = new DBCollection(
            "SELECT * FROM Player WHERE authlevel=0 AND disabled!=99 AND status='PC' AND ADDDATE(playatb, INTERVAL 7 DAY)> NOW()
	AND map=" . $map . " AND (abs(x-" . $x . ") + abs(y-" . $y .
                 ") + abs(x+y-" . $x . "-" . $y . "))/2<=" . $rayon, $db);
        
        $dest = array();
        
        while (! $pjActif->eof()) {
            if ($pjActif->get("name") != $playerSrc->get("name"))
                $dest[] = $pjActif->get("name");
            $pjActif->next();
        }
        
        /*
         * $msgCommerce = new MailBody();
         * $msgCommerce->set("id_Player",$dbc->get("id_Player"));
         * $msgCommerce->set("recipientid",$pjActif->get("id"));
         * $msgCommerce->set("type",1);
         * $msgCommerce->addDB($db);
         */
        
        $type = '1';
        /*
         * $dbu = new DBCollection("UPDATE MailBody SET type=1 WHERE id_Player=".$dbc->get("id_Player")." AND recipientid= '".$pjActif->get("id")."' ", $db,0,0,false);
         */
        $playerSrc->set("ap", ($playerSrc->get("ap") - HOSTEL_TRADE_AP));
        $playerSrc->set("money", ($playerSrc->get("money") - CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), HOSTEL_TRADE, $db)));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
            
            /* ------------- Envoie du message ------------------ */
        $err = "";
        MailFactory::sendMsg($playerSrc, $title, $content, $dest, $type, $err, $db);
        
        $param["PRICE"] = $price;
        $param["DEST"] = $dest;
        $param["RAYON"] = $rayon;
        $param["TITLE"] = $title;
        $param["BODY"] = $content;
        $param["CPT"] = $playerSrc->get("name");
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = HOSTEL_TRADE;
        $param["TYPE_ATTACK"] = HOSTEL_TRADE_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" . $date .
                     "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** MAGIC SCHOOL **************************************************** */
    static function learnSpell($playerSrc, $idspell, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $BMS = new BasicMagicSpell();
        $BMS->load($idspell, $db);
        
        $param["SPELL_NAME"] = $BMS->get("name");
        
        $dbB = new DBCollection("SELECT value FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $costAP = LEARN_AP - $dbB->get("value");
        
        if ($BMS->get("level") == 0) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_SPELL_0, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_SPELL_0, $db);
        } elseif ($BMS->get("level") == 1) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_SPELL_1, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_SPELL_1, $db);
        } elseif ($BMS->get("level") == 2) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_SPELL_2, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_SPELL_2, $db);
        }
        
        if ($playerSrc->get("ap") < $costAP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! self::prerequisitesForLearningMagicSpell($playerSrc->get("id"), $BMS->get("school"), $BMS->get("level"), $db))
            return NEED_MORE_PROGRESS_ERROR;
        
        $param["TYPE_ACTION"] = SCHOOL_LEARN_SPELL;
        $param["TYPE_ATTACK"] = SCHOOL_LEARN_SPELL_EVENT;
        $param["ERROR"] = 0;
        $param["PRICE"] = $price;
        $param["LEVEL"] = $BMS->get("level");
        $param["TAXE"] = 0;
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $magicspell = new MagicSpell();
        $magicspell->set("id_Player", $playerSrc->get("id"));
        $magicspell->set("id_BasicMagicSpell", $idspell);
        $magicspell->set("skill", 50);
        $magicspell->updateDB($db);
        
        $param["AP"] = $costAP;
        $playerSrc->set("ap", ($playerSrc->get("ap") - $costAP));
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" . $date .
                     "')", $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** ABILITY SCHOOL **************************************************** */
    static function learnAbility($playerSrc, $idability, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $BA = new BasicAbility();
        $BA->load($idability, $db);
        
        $param["ABILITY_NAME"] = $BA->get("name");
        
        $dbB = new DBCollection("SELECT value FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $costAP = LEARN_AP - $dbB->get("value");
        
        if ($BA->get("level") == 0) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_ABILITY_0, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_ABILITY_0, $db);
        } elseif ($BA->get("level") == 1) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_ABILITY_1, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_ABILITY_1, $db);
        } elseif ($BA->get("level") == 2) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_ABILITY_2, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_ABILITY_2, $db);
        }
        
        $param["TYPE_ACTION"] = SCHOOL_LEARN_ABILITY;
        $param["TYPE_ATTACK"] = SCHOOL_LEARN_ABILITY_EVENT;
        $param["PRICE"] = $price;
        $param["LEVEL"] = $BA->get("level");
        $param["TAXE"] = 0;
        $param["ERROR"] = 0;
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        if ($playerSrc->get("ap") < $costAP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! self::prerequisitesForLearningAbility($playerSrc->get("id"), $BA->get("school"), $BA->get("level"), $db))
            return NEED_MORE_PROGRESS_ERROR;
        
        $ability = new Ability();
        $ability->set("id_Player", $playerSrc->get("id"));
        $ability->set("id_BasicAbility", $idability);
        $ability->set("skill", 50);
        $ability->updateDB($db);
        
        $param["AP"] = $costAP;
        $playerSrc->set("ap", ($playerSrc->get("ap") - $costAP));
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" . $date .
                     "')", $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** TALENT SCHOOL **************************************************** */
    static function learnTalent($playerSrc, $idtalent, $idbasic, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $BT = new BasicTalent();
        $BT->load($idtalent, $db);
        
        $param["TALENT_NAME"] = $BT->get("name");
        
        $reduce = 0;
        if ($BT->get("type") == 1) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_0, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_0, $db);
        } 

        elseif ($BT->get("type") == 2) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_1, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_1, $db);
        } 

        elseif ($BT->get("type") == 3) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_2, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_2, $db);
        } elseif ($BT->get("type") == 4) {
            $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_3, $db);
            $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SCHOOL_LEARN_TALENT_3, $db);
            $reduce = 2;
        }
        
        $dbB = new DBCollection("SELECT value FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $costAP = LEARN_AP - $dbB->get("value") - $reduce;
        
        $param["TYPE_ACTION"] = SCHOOL_LEARN_TALENT;
        $param["TYPE_ATTACK"] = SCHOOL_LEARN_TALENT_EVENT;
        $param["PRICE"] = $price;
        $param["TAXE"] = 0;
        $param["ERROR"] = 0;
        
        $dbCheck = new DBCollection("SELECT id FROM Knowledge WHERE id_Player=" . $playerSrc->get("id") . " and id_BasicTalent= " . $idtalent . " and id_BasicEquipment=" . $idbasic, 
            $db, 0, 0);
        
        if (! $dbCheck->eof()) {
            return KNOWLEDGE_ALREADY_KNOWN;
        }
        
        if ($playerSrc->get("ap") < $costAP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
            // taxe
        
        $param["NEW"] = 1;
        
        // Cas de l'artisanat
        if ($BT->get("type") == 3) {
            if (! self::prerequisitesForLearningTalent($playerSrc, $idtalent, $db))
                return NEED_MORE_PROGRESS_ERROR;
            if ($idbasic == 0)
                return CHOICE_FIELD_INVALID;
            
            $param["NEW"] = 0;
            $dbc = new DBCollection("SELECT * FROM Talent WHERE id_Player=" . $playerSrc->get("id") . " AND id_BasicTalent=" . $idtalent, $db);
            if ($dbc->eof()) {
                
                $param["NEW"] = 1;
                switch ($idtalent) {
                    case 7:
                        $id_Tool = 207;
                        break; // fer
                    case 8:
                        $id_Tool = 212;
                        break; // bois
                    case 9:
                        $id_Tool = 208;
                        break; // cuir
                    case 10:
                        $id_Tool = 209;
                        break; // ecaille
                    case 11:
                        $id_Tool = 210;
                        break; // lin
                    case 12:
                        $id_Tool = 211;
                        break; // plantes
                    case 13:
                        $id_Tool = 220;
                        break;
                    default:
                        break;
                }
                $knowledge2 = new Knowledge();
                $knowledge2->set("id_Player", $playerSrc->get("id"));
                $knowledge2->set("id_BasicTalent", $idtalent);
                $knowledge2->set("id_BasicEquipment", $id_Tool);
                $knowledge2->addDB($db);
            }
            
            $knowledge3 = new Knowledge();
            $knowledge3->set("id_Player", $playerSrc->get("id"));
            $knowledge3->set("id_BasicTalent", $idtalent);
            $knowledge3->set("id_BasicEquipment", $idbasic);
            $knowledge3->addDB($db);
            
            // perte de 2% de maitrise lorsqu'on apprend un nouvel objet.
            if (! $param["NEW"])
                $dbu = new DBCollection("UPDATE Talent Set skill=skill-2 WHERE id_player=" . $playerSrc->get("id") . " AND id_BasicTalent=" . $BT->get("id"), $db, 0, 0, false);
        }
        
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        if ($param["NEW"]) {
            $dbT = new DBCollection("SELECT type FROM BasicTalent WHERE id=" . $idtalent, $db, 0, 0);
            $talent = new Talent();
            $talent->set("id_Player", $playerSrc->get("id"));
            $talent->set("id_BasicTalent", $idtalent);
            if ($dbT->get("type") == 1)
                $talent->set("skill", 70);
            else
                $talent->set("skill", 50);
            
            $talent->updateDB($db);
        }
        
        $param["AP"] = $costAP;
        $playerSrc->set("ap", ($playerSrc->get("ap") - $costAP));
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $profit . ",'" . $date .
                     "')", $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** BANK **************************************************** */
    static function bankDeposit($playerSrc, $moneydep, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $param["MONEYDEP"] = $moneydep;
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), BANK_DEPOSIT, $db);
        $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), BANK_DEPOSIT, $db);
        
        $dbB = new DBCollection("SELECT id_City,level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        
        $city = new City();
        $city->load($dbB->get("id_City"), $db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BANK_DEPOSIT;
        $param["TYPE_ATTACK"] = BANK_DEPOSIT_EVENT;
        $param["FINALTAXE"] = floor($moneydep * $profit / 100);
        $param["FINALBANK"] = floor($moneydep * (100 - $price) / 100);
        
        if ($playerSrc->get("money") < $moneydep)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($moneydep) or (is_numeric($moneydep) and $moneydep < 0))
            return MONEY_FIELD_INVALID;
        
        $playerSrc->set("money", ($playerSrc->get("money") - $moneydep));
        $playerSrc->set("moneyBank", ($playerSrc->get("moneyBank") + $param["FINALBANK"]));
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["FINALTAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . ($moneydep - $param["FINALBANK"]) .
                     "," . $param["FINALTAXE"] . ",'" . $date . "')", $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function bankWithdraw($playerSrc, $money, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $param["MONEY"] = $money;
        
        if ($playerSrc->get("moneyBank") < $money)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($money) or (is_numeric($money) and $money < 0))
            return MONEY_FIELD_INVALID;
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BANK_WITHDRAW;
        $param["TYPE_ATTACK"] = BANK_WITHDRAW_EVENT;
        $playerSrc->set("money", ($playerSrc->get("money") + $money));
        $playerSrc->set("moneyBank", ($playerSrc->get("moneyBank") - $money));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function bankTransfert($playerSrc, $money, $targetid, $type, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), BANK_DEPOSIT, $db);
        $price = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), BANK_TRANSFERT, $db);
        
        $target = new Player();
        $target->load($targetid, $db);
        BasicActionFactory::globalInfoOpponent($target, $param);
        
        $param["TYPE"] = $type;
        $param["TARGET_ID"] = $targetid;
        $param["MONEY_TRANSFERED"] = $money;
        
        $param["TARGET_NAME"] = "";
        
        $dbB = new DBCollection("SELECT id_City,level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        
        $city = new City();
        $city->load($dbB->get("id_City"), $db);
        
        $param["TAXE"] = floor($money * $profit / 100);
        $param["FINAL_COST"] = floor($money + $money * ($price + $profit) / 100);
        
        if ($playerSrc->get("moneyBank") < $param["FINAL_COST"])
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($money) or (is_numeric($money) and $money < 0))
            return MONEY_FIELD_INVALID;
        
        if (! is_numeric($param["TARGET_ID"]))
            return ACTION_INVALID_TARGET;
        
        if ($param["TYPE"] == 1) {
            $dbP = new DBCollection("SELECT id FROM Player WHERE id=" . $param["TARGET_ID"], $db, 0, 0);
            if ($dbP->count() == 0)
                return ACTION_INVALID_TARGET;
            else {
                $param["TYPE_ACTION"] = BANK_TRANSFERT_PLAYER;
                $param["TYPE_ATTACK"] = BANK_TRANSFERT_PLAYER_EVENT;
                $targetPlayer = new Player();
                $targetPlayer->load($param["TARGET_ID"], $db);
                $targetPlayer->set("moneyBank", ($targetPlayer->get("moneyBank") + $param["MONEY_TRANSFERED"]));
                $targetPlayer->updateDB($db);
                $param["TARGET_NAME"] = $targetPlayer->get("name");
                // Faire un message pour l'avertir.
            }
        } else {
            $dbC = new DBCollection("SELECT id FROM City WHERE id=" . $param["TARGET_ID"], $db, 0, 0);
            if ($dbC->count() == 0)
                return ACTION_INVALID_TARGET;
            else {
                $param["TYPE_ACTION"] = BANK_TRANSFERT;
                $param["TYPE_ATTACK"] = BANK_TRANSFERT_EVENT;
                $targetCity = new City();
                $targetCity->load($param["TARGET_ID"], $db);
                $targetCity->set("money", ($targetCity->get("money") + $param["MONEY_TRANSFERED"]));
                $targetCity->updateDB($db);
                $param["TARGET_NAME"] = $targetCity->get("name");
            }
        }
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $playerSrc->set("moneyBank", ($playerSrc->get("moneyBank") - $param["FINAL_COST"]));
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . BANK_TRANSFERT . "," . BANK_TRANSFERT_EVENT . "," . $price . "," . $profit . ",'" . $date . "')", 
                    $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** SHOP **************************************************** */
    static function basicBuy($playerSrc, $idBasic, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), BASIC_SHOP_BUY, $db);
        
        $dbbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $idBasic, $db);
        
        // $dbe=new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND state='Carried' AND weared='No' AND id_Player=".$playerSrc->get("id"),$db);
        
        $price = $dbbe->get("frequency");
        $price += floor($price * ($profit - 10) / 100);
        $param["TAXE"] = floor($dbbe->get("frequency") * $profit / 100);
        
        // $idT = $dbbe->get("id_EquipmentType");
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["INV"] = PlayerFactory::checkingInvFullForObject($playerSrc, $dbbe->get("id_EquipmentType"), $param, $db);
        if ($param["INV"] == - 1)
            return INV_FULL_ERROR;
        
        if (! ($idT = 28 or $idT < 9 or ($idT > 8 and $idT < 19)))
            return INVALID_OBJECT_ERROR;
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BASIC_SHOP_BUY;
        $param["TYPE_ATTACK"] = BASIC_SHOP_BUY_EVENT;
        $param["EQ_NAME"] = $dbbe->get("name");
        $param["EQ_PRICE"] = $price;
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        // création de l'objet
        $equip = new Equipment();
        
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("id_EquipmentType", $dbbe->get("id_EquipmentType"));
        $equip->set("id_BasicEquipment", $dbbe->get("id"));
        $equip->set("name", $dbbe->get("name"));
        $equip->set("durability", $dbbe->get("durability"));
        $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $param["INV"], $db));
        $equip->set("state", "Carried");
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("level", 1);
        $equip->addDB($db);
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $param["TAXE"] . ",'" .
                     $date . "')", $db, 0, 0, false);
        
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function mainBuy($playerSrc, $idEquip, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), MAIN_SHOP_BUY, $db);
        
        $price = EquipFactory::getPriceEquipment($idEquip, $db);
        $price += floor($price * ($profit - 10) / 100);
        $param["TAXE"] = floor(EquipFactory::getPriceEquipment($idEquip, $db) * $profit / 100);
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $equip = new Equipment();
        $equip->load($idEquip, $db);
        
        if ($equip->get("id_Player") != 0)
            return ERROR_OBJECT_ALREADY_SOLD;
        
        $param["INV"] = PlayerFactory::checkingInvFullForObject($playerSrc, $equip->get("id_EquipmentType"), $param, $db);
        if ($param["INV"] == - 1)
            return INV_FULL_ERROR;
        
        $param["EQ_NAME"] = $equip->get("name") . " " . $equip->get("extraname");
        $param["EQ_PRICE"] = $price;
        $param["EQ_LEVEL"] = $equip->get("level");
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("id_Shop", 0);
        $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $param["INV"], $db));
        
        $equip->set("state", "Carried");
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->updateDB($db);
        
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = MAIN_SHOP_BUY;
        $param["TYPE_ATTACK"] = MAIN_SHOP_BUY_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $param["TAXE"] . ",'" .
                     $date . "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    static function shopSell($playerSrc, $idEquip, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (count($idEquip) == 0) {
            return ACTION_FIELD_ERROR; // Mettre la bonne erreur
        }
        
        $shop = new Building();
        $shop->load($playerSrc->get("inbuilding"), $db);
        
        $where = self::getCondFromArray($idEquip, "Equipment.id", "OR");
        $dbe = new DBCollection("SELECT id,name,extraname  FROM Equipment WHERE " . $where, $db);
        $i = 0;
        $param["TOTAL_PRICE"] = 0;
        
        while (! $dbe->eof()) {
            
            $param["EQ_NAME"][$i] = $dbe->get("name") . " " . $dbe->get("extraname");
            $param["EQ_PRICE"][$i] = EquipFactory::getPriceForSaleWithReductionDurability($dbe->get("id"), $db);
            
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            
            // suppression de tous les éléments contenu dans le sac vendu
            $ddu = new DBCollection("DELETE FROM Equipment WHERE id_Equipment\$bag=" . $equip->get("id"), $db, 0, 0, false);
            
            if ((abs($equip->get("level") - $shop->get("level")) < 2) && $equip->get("level") != 1) {
                $equip->set("id_Player", 0);
                $coef = 1;
                $dbx = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $equip->get("id_BasicEquipment"), $db);
                if ($equip->get("id_BasicEquipment") >= 200 && $equip->get("id_BasicEquipment") <= 206 && $equip->get("id_BasicEquipment") != 205)
                    $coef = $equip->get("level");
                $equip->set("durability", $dbx->get("durability") * $coef);
                $equip->set("id_Equipment\$bag", 0);
                $equip->set("id_Shop", $playerSrc->get("inbuilding"));
                $equip->updateDB($db);
            } else
                $equip->deleteDB($db);
            
            $param["TOTAL_PRICE"] += $param["EQ_PRICE"][$i];
            $i ++;
            $dbe->next();
        }
        
        $param["NOMBRE"] = $i;
        $playerSrc->set("money", ($playerSrc->get("money") + $param["TOTAL_PRICE"]));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = SHOP_SELL;
        $param["TYPE_ATTACK"] = SHOP_SELL_EVENT;
        return ACTION_NO_ERROR;
    }

    static function shopRepair($playerSrc, $idEquip, $price, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SHOP_REPAIR, $db);
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (count($idEquip) == 0) {
            return ACTION_FIELD_ERROR; // mettre la bonne erreur;
        }
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        $shop = new Building();
        $shop->load($playerSrc->get("inbuilding"), $db);
        
        $where = self::getCondFromArray($idEquip, "Equipment.id", "OR");
        $dbe = new DBCollection("SELECT *  FROM Equipment WHERE " . $where, $db);
        $i = 0;
        $param["TOTAL_PRICE"] = 0;
        $param["TAXE"] = 0;
        
        while (! $dbe->eof()) {
            
            $param["EQ_NAME"][$i] = $dbe->get("name") . " " . $dbe->get("extraname");
            $param["EQ_PRICE"][$i] = EquipFactory::getPriceToRepair($dbe->get("id"), $db);
            $param["EQ_PRICE"][$i] += floor($param["EQ_PRICE"][$i] * ($profit - 30) / 100);
            
            $param["TAXE"] += floor(EquipFactory::getPriceToRepair($dbe->get("id"), $db) * $profit / 100);
            
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            $dbd = new DBCollection("SELECT durability FROM BasicEquipment WHERE id=" . $equip->get("id_BasicEquipment"), $db);
            if ($dbe->get("id_BasicEquipment") >= 200 && $dbe->get("id_BasicEquipment") <= 206 && $dbe->get("id_BasicEquipment") != 205)
                $durMax = $dbd->get("durability") * $dbe->get("level");
            else
                $durMax = $dbd->get("durability");
            $equip->set("durability", $durMax);
            $equip->updateDB($db);
            $param["TOTAL_PRICE"] += $param["EQ_PRICE"][$i];
            $i ++;
            $dbe->next();
        }
        
        $param["NOMBRE"] = $i;
        $playerSrc->set("money", ($playerSrc->get("money") - $param["TOTAL_PRICE"]));
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = SHOP_REPAIR;
        $param["TYPE_ATTACK"] = SHOP_REPAIR_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["TOTAL_PRICE"] . "," .
                     $param["TAXE"] . ",'" . $date . "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** CRAFTSMAN GUILD **************************************************** */
    static function orderEquip($playerSrc, $idBasic, $level, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = GUILD_ORDER_EQUIP;
        $param["TYPE_ATTACK"] = GUILD_ORDER_EQUIP_EVENT;
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), GUILD_ORDER_EQUIP, $db);
        
        $dbB = new DBCollection("SELECT id_City,level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $dbbe = new DBCollection("SELECT *  FROM BasicEquipment WHERE id=" . quote_smart($idBasic), $db);
        $price = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $level, $db);
        $price += floor($price * ($profit - 10) / 100);
        $param["TAXE"] = floor(EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $level, $db) * $profit / 100);
        
        $idT = $dbbe->get("id_EquipmentType");
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($dbB->get("level") < $level)
            return GUILD_WRONG_LEVEL_ERROR;
        
        if (! ($idT < 23 or $idT == 44 or $idT == 40 or $idT == 41 or $idT == 42 or $idT == 43 or $idT == 46))
            return GUILD_WRONG_TYPE_ERROR;
        
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE extraname='' AND  id_Shop=" . $playerSrc->get("inbuilding") . " AND date>NOW()", $db, 0, 0);
        if ($dbe->count() >= (2 * $dbB->get("level")))
            return GUILD_MAX_ERROR;
        
        $param["EQ_NAME"] = $dbbe->get("name");
        $param["EQ_PRICE"] = $price;
        $param["EQ_LEVEL"] = quote_smart($level);
        
        $durability = $dbbe->get("durability");
        if ($dbbe->get("id") >= 200 && $dbbe->get("id") <= 206 && $dbbe->get("id") != 205) {
            $durability = $level * $dbbe->get("durability");
        }
        
        $equip = new Equipment();
        $equip->set("id_Shop", $playerSrc->get("inbuilding"));
        $equip->set("id_Warehouse", $playerSrc->get("id")); // Stockage temporaire de l'id du perso
        $equip->set("id_EquipmentType", $idT);
        $equip->set("id_BasicEquipment", $dbbe->get("id"));
        $equip->set("name", $dbbe->get("name"));
        $equip->set("durability", $durability);
        $interval = new DateInterval("P" . $level . "D");
        $creationdate = date_format(date_add(date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s")), $interval), "Y-m-d H:i:s");
        $equip->set("date", $creationdate);
        $equip->set("level", $level);
        $equip->addDB($db);
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $param["TAXE"] . ",'" .
                     $date . "')", $db, 0, 0, false);
        
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        
        return ACTION_NO_ERROR;
    }

    static function takeEquip($playerSrc, $id, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbe = new DBCollection(
            "SELECT id,name,date,id_Shop,id_Warehouse FROM Equipment WHERE id=" . quote_smart($id) . " AND id_Shop=" . $playerSrc->get("inbuilding") .
                 " AND date<NOW() AND id_Warehouse=" . $playerSrc->get("id"), $db, 0, 0);
        
        $dbpe = new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND weared='No' AND id_Player=" . $playerSrc->get("id"), $db);
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($dbe->count() == 0)
            return INVALID_OBJECT_ERROR;
        
        if ($dbpe->count() >= NB_MAX_ELEMENT)
            return INV_FULL_ERROR;
        
        $equip = new Equipment();
        $equip->load($dbe->get("id"), $db);
        
        $param["EQ_NAME"] = $equip->get("name");
        $param["EQ_LEVEL"] = $equip->get("level");
        
        $equip->set("id_Shop", 0);
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("state", "Carried");
        $equip->set("id_Warehouse", 0);
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = GUILD_TAKE_EQUIP;
        $param["TYPE_ATTACK"] = GUILD_TAKE_EQUIP_EVENT;
        return ACTION_NO_ERROR;
    }

    static function orderEnchant($playerSrc, $idEquip, $level, $idEnchant, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), GUILD_ORDER_ENCHANT, $db);
        
        $dbB = new DBCollection("SELECT id_City,level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        
        $equip = new Equipment();
        $equip->load($idEquip, $db);
        $price = EquipFactory::getPriceEnchant($level, $db);
        $price += floor($price * ($profit - 10) / 100);
        $param["TAXE"] = floor(EquipFactory::getPriceEnchant($level, $db) * $profit / 100);
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (($dbB->get("level") / 2) < $level)
            return GUILD_WRONG_LEVEL_ERROR;
        
        if ($equip->get("extraname") != "")
            return GUILD_ALREADY_ENCHANT_ERROR;
        
        if ($equip->get("level") < $level)
            return GUILD_INVALID_EQUIP_ERROR;
        
        $dbe = new DBCollection("SELECT id FROM Equipment WHERE extraname!='' AND id_Shop=" . $playerSrc->get("inbuilding") . " AND date>NOW()", $db, 0, 0);
        if ($dbe->count() >= $dbB->get("level"))
            return GUILD_MAX_ERROR;
        
        $dbt = new DBCollection("SELECT id,name FROM BasicTemplate WHERE id=" . quote_smart($idEnchant), $db, 0, 0);
        
        $param["EQ_NAME"] = $equip->get("name");
        $param["EN_NAME"] = $dbt->get("name");
        $param["EN_PRICE"] = $price;
        $param["EN_LEVEL"] = quote_smart($level);
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $equip->set("id_Player", 0);
        $equip->set("id_Shop", $playerSrc->get("inbuilding"));
        $equip->set("id_Warehouse", $playerSrc->get("id")); // Stockage temporaire de l'id du perso
        $equip->set("extraname", $param["EN_NAME"] . " (" . $param["EN_LEVEL"] . ")");
        $interval = new DateInterval("P" . (2 * $level) . "D");
        $creationdate = date_format(date_add(date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s")), $interval), "Y-m-d H:i:s");
        $equip->set("date", $creationdate);
        $equip->updateDB($db);
        
        $enchant = new Template();
        $enchant->set("id_Equipment", $equip->get("id"));
        $enchant->set("id_BasicTemplate", $dbt->get("id"));
        $enchant->set("level", $param["EN_LEVEL"]);
        $enchant->set("pos", 1);
        $enchant->addDB($db);
        
        // taxe
        
        $city = new City();
        $city->load($dbB->get("id_City"), $db);
        if ($city->get("id_Player") != 0) {
            $param["TAXE"] = floor($price * INBUILDING_TAXES_RATE);
            $city->set("money", ($city->get("money") + $param["TAXE"]));
            $city->updateDB($db);
        }
        
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = GUILD_ORDER_ENCHANT;
        $param["TYPE_ATTACK"] = GUILD_ORDER_ENCHANT_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $param["TAXE"] . ",'" .
                     $date . "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    static function takeEnchant($playerSrc, $id, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbe = new DBCollection(
            "SELECT id,name,date,id_Shop,id_Warehouse FROM Equipment WHERE id=" . quote_smart($id) . " AND id_Shop=" . $playerSrc->get("inbuilding") .
                 " AND date<NOW() AND id_Warehouse=" . $playerSrc->get("id"), $db, 0, 0);
        
        $dbpe = new DBCollection("SELECT id FROM Equipment WHERE id_Equipment\$bag=0 AND weared='No' AND id_Player=" . $playerSrc->get("id"), $db);
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($dbe->count() == 0)
            return INVALID_OBJECT_ERROR;
        
        if ($dbpe->count() >= NB_MAX_ELEMENT)
            return INV_FULL_ERROR;
        
        $equip = new Equipment();
        $equip->load($dbe->get("id"), $db);
        
        $param["EQ_NAME"] = $equip->get("name");
        $param["EN_NAME"] = $equip->get("extraname");
        
        $equip->set("id_Shop", 0);
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("state", "Carried");
        $equip->set("id_Warehouse", 0);
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = GUILD_TAKE_ENCHANT;
        $param["TYPE_ATTACK"] = GUILD_TAKE_ENCHANT_EVENT;
        return ACTION_NO_ERROR;
    }

    static function resetGauge($playerSrc, $caract, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = GUILD_RESET_GAUGE;
        $param["TYPE_ATTACK"] = GUILD_RESET_GAUGE_EVENT;
        self::globalInfo($playerSrc, $param);
        
        if ($caract == 1) {
            $caract = 'hp';
            $param["CARACT"] = 'Vie';
        } elseif ($caract == 2) {
            $caract = 'strength';
            $param["CARACT"] = 'Force';
        } elseif ($caract == 3) {
            $caract = 'dexterity';
            $param["CARACT"] = 'Dextérité';
        } elseif ($caract == 4) {
            $caract = 'speed';
            $param["CARACT"] = 'Vitesse';
        } elseif ($caract == 5) {
            $caract = 'magicSkill';
            $param["CARACT"] = 'Maitrise de la magie';
        } else {
            $caract = '';
        }
        
        if ($caract != '' && $playerSrc->get('level') < 6 && $playerSrc->get("modeAnimation") == 0) {
            $playerSrc->setSub("Upgrade", $caract, $playerSrc->getSub("Upgrade", $caract) - $playerSrc->getSub("Upgrade", $caract));
            $playerSrc->set("ap", $playerSrc->get("ap") - RESET_GAUGE_AP);
            $playerSrc->set("money", $playerSrc->get("money") - RESET_GAUGE_PRICE);
        }
        return ACTION_NO_ERROR;
    }

    /* *************************************************************** COMMERCIAL CENTER **************************************************** */
    static function caravanCreate($playerSrc, $level, $target, $load, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $param["CARAVAN_LEVEL"] = $level;
        $param["TAXE"] = 0;
        
        // ---------------- les check antihack
        
        $dbtarget = new DBCollection("SELECT id,x,y,id_City FROM Building WHERE id=" . $target, $db);
        $dist = distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbtarget->get("x"), $dbtarget->get("y"));
        $distmin = min(400, 15 + 6 * $level);
        $distmax = 6 * $level + 80;
        
        $mapObj = new Map($db, $playerSrc->get("map"));
        $idStartKingdomPlayer = $mapObj->getStartKingdomIdFromMap($playerSrc->get("x"), $playerSrc->get("y"));
        $idStartKingdomTarget = $mapObj->getStartKingdomIdFromMap($dbtarget->get("x"), $dbtarget->get("y"));
        
        $load = unserialize(base64_decode($load));
        $lvl = $load["level1"];
        $nb1 = $load["quantity1"];
        $dbbe = new DBCollection("SELECT id,frequency FROM BasicEquipment WHERE id=" . $load["type1"], $db, 0, 0);
        $price1 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
        $price1 = $price1 * $nb1;
        
        $price2 = 0;
        $price3 = 0;
        $nb2 = 0;
        $nb3 = 0;
        
        if (isset($load["type2"])) {
            $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $load["type2"], $db, 0, 0);
            $lvl = $load["level2"];
            $nb2 = $load["quantity2"];
            $price2 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
            $price2 = $price2 * $nb2;
        }
        
        if (isset($load["type3"])) {
            $dbbe = new DBCollection("SELECT id,name,frequency FROM BasicEquipment WHERE id=" . $load["type3"], $db, 0, 0);
            $lvl = $load["level3"];
            $nb3 = $load["quantity3"];
            $price3 = EquipFactory::getPriceBasicEquipment($dbbe->get("id"), $lvl, $db);
            $price3 = $price3 * $nb3;
        }
        
        $totalprice = $price1 + $price2 + $price3;
        $totalquantity = $nb1 + $nb2 + $nb3;
        $pricelimit = 60 + 100 * ($level - 1);
        
        if ($idStartKingdomPlayer != ID_UNKNOWN_KINGDOM && $idStartKingdomTarget != ID_UNKNOWN_KINGDOM && $idStartKingdomPlayer == $idStartKingdomTarget && $level > 4)
            return CARAVAN_INVALID_DESTINATION_TWICE_START_KINGDOM;
        
        if (($playerSrc->get("merchant_level") < $level) && ! (($playerSrc->get("merchant_level") > 0) && ($playerSrc->get("merchant_level") < 1) && ($level == 1)))
            return CARAVAN_INVALID_MERCHANT_LEVEL;
        
        if (($dist > $distmax) || ($dist < $distmin))
            return ACTION_INVALID_TARGET;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($totalquantity < NB_MIN_LOAD_CARAVAN)
            return CARAVAN_LOAD_TOO_SMALL;
        
        if ($playerSrc->get("money") < $totalprice)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($totalprice > $pricelimit)
            return INVALID_OBJECT_ERROR;
            
            // ------------------------- creation de la caravane
        
        $duration = 3 * ceil($dist / 24); // en jours
        $finalprice = floor($totalprice + $totalprice * $dist / 100);
        
        $interval = new DateInterval("P" . $duration . "D");
        $limitdate = date_format(date_add(date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s")), $interval), "Y-m-d H:i:s");
        
        $caravan = new Caravan();
        $caravan->set("level", $level);
        $caravan->set("content", serialize($load));
        $caravan->set("id_startbuilding", $playerSrc->get("inbuilding"));
        $caravan->set("id_endbuilding", $target);
        $caravan->set("id_Player", $playerSrc->get("id"));
        $caravan->set("finalprice", $finalprice);
        $caravan->set("date", $limitdate);
        $caravan->addDB($db);
        
        // ----------------------------- creation de la tortue
        
        $npc = PNJFactory::getPNJFromLevel(263, $caravan->get("level") + 3, $db);
        
        $npc->set("x", $playerSrc->get("x"));
        $npc->set("y", $playerSrc->get("y"));
        $npc->set("map", $playerSrc->get("map"));
        $npc->set("hidden", 10);
        $npc->set("id_Member", $playerSrc->get("id_Member"));
        $npc->set("id_Owner", $playerSrc->get("id"));
        $npc->set("id_BasicRace", 263); // TODO : refaire quand l'IA sera terminée
        $npc->set("racename", "Tortue Géante");
        $npc->set("id_Caravan", $caravan->get("id"));
        $npc->set("inbuilding", $playerSrc->get("inbuilding"));
        $npc->set("room", 2);
        $npc->updateDBr($db);
        PlayerFactory::NewATB($npc, $param2, $db);
        
        // PlayerFactory::initBM($npc, $npc->getObj("Modifier"), $param, $db, 1);
        
        // ----------------------------- creation du chargement
        
        $dbt = new DBCollection("SELECT id FROM Player WHERE id_BasicRace=263 AND id_Member=" . $playerSrc->get("id_Member"), $db, 0, 0);
        $dbbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $load["type1"], $db, 0, 0);
        
        for ($i = 1; $i <= $load["quantity1"]; $i ++) {
            $marchandise = new Equipment();
            $marchandise->set("name", $dbbe->get("name"));
            $marchandise->set("id_BasicEquipment", $dbbe->get("id"));
            $marchandise->set("id_EquipmentType", $dbbe->get("id_EquipmentType"));
            $marchandise->set("id_Player", $dbt->get("id"));
            $marchandise->set("state", "Carried");
            $marchandise->set("durability", $dbbe->get("durability"));
            $marchandise->set("level", $load["level1"]);
            $marchandise->addDB($db);
        }
        
        if (isset($load["type2"])) {
            
            $dbbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $load["type2"], $db, 0, 0);
            
            for ($i = 1; $i <= $load["quantity2"]; $i ++) {
                $marchandise = new Equipment();
                $marchandise->set("name", $dbbe->get("name"));
                $marchandise->set("id_BasicEquipment", $dbbe->get("id"));
                $marchandise->set("id_EquipmentType", $dbbe->get("id_EquipmentType"));
                $marchandise->set("id_Player", $dbt->get("id"));
                $marchandise->set("state", "Carried");
                $marchandise->set("durability", $dbbe->get("durability"));
                $marchandise->set("level", $load["level2"]);
                $marchandise->addDB($db);
            }
        }
        
        if (isset($load["type3"])) {
            
            $dbbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $load["type3"], $db, 0, 0);
            
            for ($i = 1; $i <= $load["quantity3"]; $i ++) {
                $marchandise = new Equipment();
                $marchandise->set("name", $dbbe->get("name"));
                $marchandise->set("id_BasicEquipment", $dbbe->get("id"));
                $marchandise->set("id_EquipmentType", $dbbe->get("id_EquipmentType"));
                $marchandise->set("id_Player", $dbt->get("id"));
                $marchandise->set("state", "Carried");
                $marchandise->set("durability", $dbbe->get("durability"));
                $marchandise->set("level", $load["level3"]);
                $marchandise->addDB($db);
            }
        }
        
        // ----------------------------- creation du parchemin
        
        $equip = new Equipment();
        $equip->set("id_Caravan", $caravan->get("id"));
        $equip->set("name", "Parchemin");
        $equip->set("id_BasicEquipment", 600);
        $equip->set("id_EquipmentType", 45);
        $equip->set("date", gmdate("Y-m-d H:i:s"));
        $equip->set("state", "Carried");
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->updateDB($db);
        
        // ----------------------------- paiements
        
        $param["CARAVAN_PRICE"] = $totalprice;
        $dbB = new DBCollection("SELECT id_City,level FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $city = new City();
        $city->load($dbB->get("id_City"), $db);
        
        /*
         * if($city->get("id_Player") !=0)
         * {
         * $param["TAXE"] = floor($param["CARAVAN_PRICE"]*INBUILDING_TAXES_RATE);
         * $city->set("money",($city->get("money") + $param["TAXE"]));
         * $city->updateDB($db);
         * }
         */
        $playerSrc->set("money", ($playerSrc->get("money") - $param["CARAVAN_PRICE"]));
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = CARAVAN_CREATE;
        $param["TYPE_ATTACK"] = CARAVAN_CREATE_EVENT;
        return ACTION_NO_ERROR;
    }

    static function caravanTerminate($playerSrc, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $playerSrc->get("id"), $db, 0, 0);
        $dbt = new DBCollection("SELECT id,inbuilding FROM Player WHERE id_BasicRace=263 AND id_Caravan=" . $dbc->get("id"), $db, 0, 0);
        $dbdate = new DBCollection("SELECT * FROM Caravan WHERE date<NOW() AND id_Player=" . $playerSrc->get("id"), $db, 0, 0);
        
        if ($dbc->eof())
            return CARAVAN_NOT_REAL;
        
        if ($dbt->eof())
            return CARAVAN_NO_TURTLE;
        
        if ($playerSrc->get("inbuilding") != $dbc->get("id_endbuilding") || $dbt->get("inbuilding") != $dbc->get("id_endbuilding"))
            return CARAVAN_WRONG_BUILDING;
            
            // param pour le message
        $param["MISSING_LOAD"] = 0;
        $param["CARAVAN_TOO_LATE"] = 0;
        $param["CARAVAN_LEVEL"] = $dbc->get("level");
        $param["PRICE"] = $dbc->get("finalprice");
        $param["LEVEL_PROGRESS"] = 0;
        
        // Vérification que le chargement est identique - Inutile pour l'instant car impossible de gruger.
        $load = unserialize($dbc->get("content"));
        $dbe1 = new DBCollection("SELECT id FROM Equipment WHERE id_Player=" . $dbt->get("id") . " AND level=" . $load["level1"] . " AND id_BasicEquipment=" . $load["type1"], $db, 
            0, 0);
        
        /*
         *
         * if($dbe1->count()<$load["quantity1"])
         * $param["MISSING_LOAD"] = 1;
         *
         * if(isset($load["type2"]))
         * {
         * $dbe2 = new DBCollection("SELECT id FROM Equipment WHERE id_Player=".$dbt->get("id")." AND level=".$load["level2"]." AND id_BasicEquipment=".$load["type2"],$db,0,0,0);
         * if($dbe2->count()<$load["quantity2"])
         * $param["MISSING_LOAD"] = 1;
         * }
         * if(isset($load["type3"]))
         * {
         * $dbe3 = new DBCollection("SELECT id FROM Equipment WHERE id_Player=".$dbt->get("id")." AND level=".$load["level3"]." AND id_BasicEquipment=".$load["type3"],$db,0,0,0);
         * if($dbe3->count()<$load["quantity3"])
         * $param["MISSING_LOAD"] =1;
         * }
         *
         * if($param["MISSING_LOAD"] == 1)
         * {
         * // Pour l'instant ca peut pas arriver, vu qu'on a pas d'action possible avec la tortue, après faudra traiter.
         * }
         */
        // Caravane en retard, récupération de sa mise et d'un peu de réputation perdue.
        if (! $dbdate->eof()) {
            // calcul de l'investissement
            $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $playerSrc->get("id"), $db);
            $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $dbc->get("id_endbuilding"), $db);
            $dist = distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbb->get("x"), $dbb->get("y"));
            $price = ceil(($dbc->get("finalprice") * 100) / (100 + $dist));
            $playerSrc->set("money", ($playerSrc->get("money") + $price));
            
            $param["CARAVAN_TOO_LATE"] = 1;
            $playerSrc->set("merchant_level", $playerSrc->get("merchant_level") + 0.25);
            $param["LEVEL_PROGRESS"] = 1;
        } else {
            // Bien joué
            $param["CARAVAN_LEVEL"] = $dbc->get("level");
            $param["PRICE"] = $dbc->get("finalprice");
            $param["LEVEL_PROGRESS"] = 0;
            
            if (floor($playerSrc->get("merchant_level")) <= $dbc->get("level")) {
                $playerSrc->set("merchant_level", $playerSrc->get("merchant_level") + 0.25);
                $param["LEVEL_PROGRESS"] = 1;
            }
            $playerSrc->set("money", ($playerSrc->get("money") + $param["PRICE"]));
        }
        
        // Destruction de la tortue et du chargement.
        $turtle = new Player();
        $turtle->load($dbt->get("id"), $db);
        require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
        PNJFactory::deleteDBnpc($turtle, $param2, $db, null, 0);
        $dbdp = new DBCollection("DELETE FROM Equipment WHERE id_BasicEquipment=600 AND id_Player=" . $playerSrc->get("id") . " AND id_Caravan=" . $dbc->get("id"), $db, 0, 0, false);
        $dbdc = new DBCollection("DELETE FROM Caravan WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
        
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = CARAVAN_TERMINATE;
        $param["TYPE_ATTACK"] = CARAVAN_TERMINATE_EVENT;
        return ACTION_NO_ERROR;
    }

    static function cancelCaravan($playerSrc, $db)
    {
        $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $playerSrc->get("id"), $db);
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $dbc->get("id_endbuilding"), $db);
        
        // Récupération de l'investissement
        $dist = distHexa($playerSrc->get("x"), $playerSrc->get("y"), $dbb->get("x"), $dbb->get("y"));
        $price = ceil(($dbc->get("finalprice") * 100) / (100 + $dist));
        $playerSrc->set("money", $playerSrc->get("money") + $price);
        
        // Destruction du chargement, de la tortue et de parchemin de mission
        $dbt = new DBCollection("SELECT id FROM Player WHERE id_BasicRace=263 AND id_Caravan=" . $dbc->get("id"), $db, 0, 0);
        if ($dbt->count()) {
            $turtle = new Player();
            $turtle->load($dbt->get("id"), $db);
            require_once (HOMEPATH . "/factory/PNJFactory.inc.php");
            PNJFactory::deleteDBnpc($turtle, $param2, $db, null, 0);
        } else {
            $playerSrc->set("merchant_level", max(0.1, $playerSrc->get("merchant_level") - 0.8));
            $playerSrc->updateDBr($db);
            $price = - 1;
        }
        $dbdp = new DBCollection("DELETE FROM Equipment WHERE id_BasicEquipment=600 AND id_Player=" . $playerSrc->get("id") . " AND id_Caravan=" . $dbc->get("id"), $db, 0, 0, false);
        $dbdc = new DBCollection("DELETE FROM Caravan WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
        $playerSrc->updateDB($db);
        return $price;
    }

    static function sendPackage($playerSrc, $where, $playerdest, $buildingdest, $price, &$err, $db)
    {
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SEND_PACKAGE, $db);
        $price2 = CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SEND_PACKAGE, $db);
        
        if ($playerSrc->get("money") < $price2) {
            $err = localize("Il vous est impossible d'effectuer cette action car vous n'avez pas assez d'argent.");
            return;
        }
        
        $dbe = new DBCollection("SELECT * FROM Equipment WHERE " . $where, $db);
        
        $globalObjetcsCost = 0;
        while (! $dbe->eof()) {
            if ($playerSrc->get("id") != $dbe->get("id_Player")) {
                $err = localize("Vous ne pouvez envoyer ces objets car l'un d'eux ne vous appartient plus");
                return;
            }
            $globalObjetcsCost += EquipFactory::getPriceEquipment($dbe->get("id"), $db);
            if ($dbe->get("id_EquipmentType") >= 41) {
                $dba = new DBCollection("SELECT * FROM Equipment WHERE id_Equipment\$bag=" . $dbe->get("id"), $db);
                while (! $dba->eof()) {
                    $globalObjetcsCost += EquipFactory::getPriceEquipment($dba->get("id"), $db);
                    $dba->next();
                }
            }
            $dbe->next();
        }
        
        if ($globalObjetcsCost * 2.5 < $price) {
            $err = localize("Le prix proposé pour les objets est beaucoup trop cher. C'est assimilable à de la fraude bancaire. Veuillez réduire le prix.");
            return;
        }
        
        $dbe->first();
        $date = gmdate("Y-m-d H:i:s");
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $buildingdest, $db);
        $nb = 0;
        $body = "\nUn colis vient de vous être envoyé par " . $playerSrc->get("name") . " au Comptoir Commercial situé en " . $dbb->get("x") . "/" . $dbb->get("y") .
             ".\n\nIl contient les objets suivants:\n";
        while (! $dbe->eof()) {
            $nb ++;
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            if ($equip->get("id_EquipmentType") >= 41) {
                $dba = new DBCollection("SELECT * FROM Equipment WHERE id_Equipment\$bag=" . $equip->get("id"), $db);
                while (! $dba->eof()) {
                    $raw = new Equipment();
                    $raw->load($dba->get("id"), $db);
                    // $equip->set("id_Shop", $dbb->get("id"));
                    // $equip->set("id_Warehouse", $playerdest);
                    // $equip->set("id_Caravan", $playerSrc->get("id"));
                    // $equip->set("po", $price);
                    $raw->set("id_Player", 0);
                    $raw->set("sell", 0);
                    $raw->set("collected", $date);
                    $raw->updateDB($db);
                    $dba->next();
                }
            }
            $equip->set("id_Shop", $dbb->get("id"));
            $equip->set("id_Warehouse", $playerdest);
            $equip->set("id_Caravan", $playerSrc->get("id"));
            $equip->set("po", $price);
            $equip->set("id_Player", 0);
            $equip->set("sell", 0);
            $equip->set("collected", $date);
            $equip->set("id_Equipment\$bag", 0);
            
            $param["OBJECT_NAME"][$nb] = $equip->get("name") . " niveau " . $dbe->get("level") . " " . $dbe->get("extraname");
            $body .= " -" . $param["OBJECT_NAME"][$nb] . "\n";
            $equip->updateDB($db);
            $dbe->next();
        }
        $body .= "\nIl vous en coutera le prix de " . $price . "PO.\n\n";
        $dbpp = new DBCollection("select name from Player where id=" . $playerdest, $db);
        $dest = array();
        $dest[$dbpp->get("name")] = $dbpp->get("name");
        MailFactory::sendMsg($playerSrc, localize("Envoi d'un colis"), $body, $dest, 1, $err, $db, 1, 0);
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $param["NB"] = $nb;
        $param["ERROR"] = 0;
        $param["PRICE"] = $price2;
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = SEND_PACKAGE;
        $param["TYPE_ATTACK"] = SEND_PACKAGE_EVENT;
        Event::logAction($db, SEND_PACKAGE, $param);
        $err = "Le colis a été envoyé avec succès";
        $playerSrc->set("money", $playerSrc->get("money") - $price2);
        $playerSrc->updateDB($db);
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . "," . $profit . ",'" .
                     $date . "')", $db, 0, 0, false);
    }

    static function collectPackage($playerSrc, $dbe, $price, &$err, $db)
    {
        $date = gmdate("Y-m-d H:i:s");
        
        $invCheck = array();
        for ($i = 0; $i <= 6; $i ++) {
            $nameBag = PlayerFactory::getNameBagByNum($playerSrc->get("id"), $i, $db);
            if ($i == 0 || PlayerFactory::getIdBagByNum($playerSrc->get("id"), $i, $db) > 0) {
                $nbMaxObjBag = PlayerFactory::getNbElementMaxInBag($playerSrc->get("id"), $nameBag, $db);
                $nbObjBag = PlayerFactory::getNbElementInBag($playerSrc->get("id"), $nameBag, $db);
            } else {
                $nbMaxObjBag = 0;
                $nbObjBag = 0;
            }
            $invCheck[$i] = array(
                "nbMax" => $nbMaxObjBag,
                "nbObj" => $nbObjBag,
                "current" => 0
            );
        }
        
        while (! $dbe->eof()) {
            $resultCheck = PlayerFactory::checkingInvfullForObject($playerSrc, $dbe->get("id_EquipmentType"), $param, $db);
            if ($resultCheck != - 1) {
                if ($resultCheck > 0) {
                    if ($invCheck[$resultCheck]["nbMax"] >= ($invCheck[$resultCheck]["nbObj"] + $invCheck[$resultCheck]["current"] + 1)) {
                        $invCheck[$resultCheck]["current"] += 1;
                    } else {
                        $resultCheck = 0;
                    }
                }
                if ($resultCheck == 0) {
                    if ($invCheck[$resultCheck]["nbMax"] >= ($invCheck[$resultCheck]["nbObj"] + $invCheck[$resultCheck]["current"] + 1)) {
                        $invCheck[$resultCheck]["current"] += 1;
                    } else {
                        $resultCheck = - 1;
                    }
                }
            }
            if ($resultCheck == - 1) {
                $err = "Vous ne pouvez pas récupérez ce colis car vous n'avez pas suffisament de place dans vos divers sacs";
                return;
            }
            
            $dbe->next();
        }
        
        $dbe->first();
        $dbb = new DBCollection("SELECT * FROM Building WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $body = "\nUn colis que vous aviez envoyé à " . $playerSrc->get("name") . " au Comptoir Commercial situé en " . $dbb->get("x") . "/" . $dbb->get("y") .
             " a été récupéré.\n\nIl contenait les objets suivants:\n";
        
        $nb = 0;
        while (! $dbe->eof()) {
            $nb ++;
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            if ($dbe->get("id_EquipmentType") >= 41) {
                $dba = new DBCollection("SELECT * FROM Equipment WHERE id_Equipment\$bag=" . $equip->get("id"), $db);
                while (! $dba->eof()) {
                    $raw = new Equipment();
                    $raw->load($dba->get("id"), $db);
                    $raw->set("id_Player", $playerSrc->get("id"));
                    $raw->set("collected", $date);
                    $raw->updateDB($db);
                    $dba->next();
                }
            }
            PlayerFactory::checkingInvfullForObject($playerSrc, $dbe->get("id_EquipmentType"), $param, $db);
            $id_exp = $equip->get("id_Caravan");
            $equip->set("id_Caravan", 0);
            $equip->set("id_Warehouse", 0);
            $equip->set("collected", $date);
            $equip->set("id_Player", $playerSrc->get("id"));
            $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $param["INV"], $db));
            $equip->set("po", 0);
            $equip->set("id_Shop", 0);
            $equip->updateDB($db);
            $param["OBJECT_NAME"][$nb] = $equip->get("name") . " niveau " . $dbe->get("level") . " " . $dbe->get("extraname");
            $body .= " -" . $param["OBJECT_NAME"][$nb] . "\n";
            $dbe->next();
        }
        
        $param["NB"] = $nb;
        $param["ERROR"] = 0;
        $param["PRICE"] = $price;
        $playerSrc->set("money", $playerSrc->get("money") - $price);
        
        $opponent = new Player();
        $opponent->load($id_exp, $db);
        $opponent->set("moneyBank", $opponent->get("moneyBank") + $price);
        $opponent->updateDB($db);
        
        $body .= "\nLa somme de " . $price . "PO a été créditée sur votre compte bancaire.\n\n";
        $dest = array();
        $dest[$opponent->get("name")] = $opponent->get("name");
        MailFactory::sendMsg($playerSrc, localize("Réception d'un colis"), $body, $dest, 1, $err, $db, 1, 0);
        
        // $dbm = new DBCollection("UPDATE Player SET moneyBank=(moneyBank+".$price.") WHERE id=".$id_exp, $db,0,0,false);
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = COLLECT_PACKAGE;
        $param["TYPE_ATTACK"] = COLLECT_PACKAGE_EVENT;
        Event::logAction($db, COLLECT_PACKAGE, $param);
        $err = localize("Le colis a été récupéré avec succès.");
        // $playerSrc->set("ap",$playerSrc->get("ap")-BUY_AP);
        $playerSrc->updateDB($db);
    }

    static function retrievePackage($playerSrc, $dbe, $price, &$err, $db)
    {
        $profit = 2 * CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SEND_PACKAGE, $db);
        $price2 = 2 * CityFactory::getBuildingActionPrice($playerSrc->get("inbuilding"), SEND_PACKAGE, $db);
        
        $date = gmdate("Y-m-d H:i:s");
        
        $invCheck = array();
        for ($i = 0; $i <= 6; $i ++) {
            $nameBag = PlayerFactory::getNameBagByNum($playerSrc->get("id"), $i, $db);
            if ($i == 0 || PlayerFactory::getIdBagByNum($playerSrc->get("id"), $i, $db) > 0) {
                $nbMaxObjBag = PlayerFactory::getNbElementMaxInBag($playerSrc->get("id"), $nameBag, $db);
                $nbObjBag = PlayerFactory::getNbElementInBag($playerSrc->get("id"), $nameBag, $db);
            } else {
                $nbMaxObjBag = 0;
                $nbObjBag = 0;
            }
            $invCheck[$i] = array(
                "nbMax" => $nbMaxObjBag,
                "nbObj" => $nbObjBag,
                "current" => 0
            );
        }
        
        while (! $dbe->eof()) {
            $resultCheck = PlayerFactory::checkingInvfullForObject($playerSrc, $dbe->get("id_EquipmentType"), $param, $db);
            
            if ($resultCheck != - 1) {
                if ($resultCheck > 0) {
                    if ($invCheck[$resultCheck]["nbMax"] >= ($invCheck[$resultCheck]["nbObj"] + $invCheck[$resultCheck]["current"] + 1)) {
                        $invCheck[$resultCheck]["current"] += 1;
                    } else {
                        $resultCheck = 0;
                    }
                }
                if ($param["INV"] == 0) {
                    if (NB_MAX_ELEMENT >= ($invCheck[$param["INV"]]["nbObj"] + $invCheck[$param["INV"]]["current"] + 1)) {
                        $invCheck[$param["INV"]]["current"] += 1;
                    } else {
                        $resultCheck = - 1;
                    }
                }
            }
            if ($resultCheck == - 1) {
                $err = "Vous ne pouvez pas récupérez ce colis car vous n'avez pas suffisament de place dans vos divers sacs";
                return;
            }
            
            $dbe->next();
        }
        
        $dbe->first();
        $dbb = new DBCollection("SELECT * FROM Building WHERE Building.id=" . $dbe->get("id_Shop"), $db);
        $body = "\nUn colis qui vous aviez été envoyé par " . $playerSrc->get("name") . " au Comptoir Commercial situé en " . $dbb->get("x") . "/" . $dbb->get("y") .
             " a été récupéré par son expéditeur.\n\nIl contenait les objets suivants:\n";
        
        $nb = 0;
        while (! $dbe->eof()) {
            $nb ++;
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            if ($dbe->get("id_EquipmentType") >= 41) {
                $dba = new DBCollection("SELECT * FROM Equipment WHERE id_Equipment\$bag=" . $equip->get("id"), $db);
                while (! $dba->eof()) {
                    $raw = new Equipment();
                    $raw->load($dba->get("id"), $db);
                    $raw->set("id_Player", $playerSrc->get("id"));
                    $raw->set("collected", $date);
                    $raw->updateDB($db);
                    $dba->next();
                }
            }
            PlayerFactory::checkingInvfullForObject($playerSrc, $dbe->get("id_EquipmentType"), $param, $db);
            $id_dest = $equip->get("id_Warehouse");
            $equip->set("id_Caravan", 0);
            $equip->set("id_Warehouse", 0);
            $equip->set("collected", $date);
            $equip->set("id_Player", $playerSrc->get("id"));
            $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $param["INV"], $db));
            $equip->set("po", 0);
            $equip->set("id_Shop", 0);
            $equip->updateDB($db);
            $param["OBJECT_NAME"][$nb] = $equip->get("name") . " niveau " . $dbe->get("level") . " " . $dbe->get("extraname");
            $body .= " -" . $param["OBJECT_NAME"][$nb] . "\n";
            $dbe->next();
        }
        
        $param["NB"] = $nb;
        $param["ERROR"] = 0;
        $param["PRICE"] = $price2;
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $profit . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $opponent = new Player();
        $opponent->load($id_dest, $db);
        $dest = array();
        $dest[$opponent->get("name")] = $opponent->get("name");
        MailFactory::sendMsg($playerSrc, localize("Annulation d'un colis"), $body, $dest, 1, $err, $db, 1, 0);
        
        require_once (HOMEPATH . "/factory/BasicActionFactory.inc.php");
        BasicActionFactory::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = RETRIEVE_PACKAGE;
        $param["TYPE_ATTACK"] = RETRIEVE_PACKAGE_EVENT;
        Event::logAction($db, RETRIEVE_PACKAGE, $param);
        $err = "Le colis a été récupéré avec succès";
        $playerSrc->set("money", $playerSrc->get("money") - $price2);
        $playerSrc->updateDB($db);
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . "," . $profit . ",'" .
                     $date . "')", $db, 0, 0, false);
    }

    /* ***************************************************************ENTREPOT****************************************************************** */
    static function buyEntrepot($playerSrc, $idEquip, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), EXCHANGE_GET_OBJECT, $db);
        
        $equip = new Equipment();
        $equip->load($idEquip, $db);
        
        if (($equip->get("id_BasicEquipment") >= 207 and $equip->get("id_BasicEquipment") <= 212) or $equip->get("id_BasicEquipment") == 220) {
            $price = EquipFactory::getPriceForSaleWithReductionDurability($dbe->get("id"), $db);
        } else {
            $price = EquipFactory::getPriceEquipment($idEquip, $db);
        }
        $price = ceil($price * $profit / 100);
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($equip->get("id_Player") != 0)
            return ERROR_OBJECT_ALREADY_SOLD;
        
        $param["INV"] = PlayerFactory::checkingInvFullForObject($playerSrc, $equip->get("id_EquipmentType"), $param, $db);
        if ($param["INV"] == - 1)
            return INV_FULL_ERROR;
        
        $param["EQ_NAME"] = $equip->get("name") . " " . $equip->get("extraname");
        $param["EQ_PRICE"] = $price;
        $param["EQ_LEVEL"] = $equip->get("level");
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $price . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        $equip->set("id_Player", $playerSrc->get("id"));
        $equip->set("id_Shop", 0);
        $equip->set("id_Equipment\$bag", PlayerFactory::getIdBagByNum($playerSrc->get("id"), $param["INV"], $db));
        
        $equip->set("state", "Carried");
        $equip->set("collected", gmdate("Y-m-d H:i:s"));
        $equip->updateDB($db);
        
        $playerSrc->set("money", ($playerSrc->get("money") - $price));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = EXCHANGE_GET_OBJECT;
        $param["TYPE_ATTACK"] = EXCHANGE_GET_OBJECT_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $price . "," . $price . ",'" . $date .
                     "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    static function sellEntrepot($playerSrc, $idEquip, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (count($idEquip) == 0) {
            return ACTION_FIELD_ERROR; // Mettre la bonne erreur
        }
        
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), EXCHANGE_OBJECT, $db);
        
        $shop = new Building();
        $shop->load($playerSrc->get("inbuilding"), $db);
        
        $dbob = new DBCollection("SELECT count(*) as nb  FROM Equipment WHERE id_shop=" . $playerSrc->get("inbuilding"), $db);
        if ($shop->get("level") * 20 <= $dbob->get("nb")) {
            return ERROR_ENTREPOT_FULL;
        }
        
        $where = self::getCondFromArray($idEquip, "Equipment.id", "OR");
        $dbe = new DBCollection("SELECT id,name,extraname,id_BasicEquipment,durability,level  FROM Equipment WHERE " . $where, $db);
        $i = 0;
        $param["TOTAL_PRICE"] = 0;
        
        while (! $dbe->eof()) {
            
            $param["EQ_NAME"][$i] = $dbe->get("name") . " " . $dbe->get("extraname");
            
            $price = 0;
            if (($dbe->get("id_BasicEquipment") >= 207 and $dbe->get("id_BasicEquipment") <= 212) or $dbe->get("id_BasicEquipment") == 220) {
                $price = EquipFactory::getPriceForSaleWithReductionDurability($dbe->get("id"), $db);
            } else {
                $price = EquipFactory::getPriceEquipment($dbe->get("id"), $db);
            }
            $param["EQ_PRICE"][$i] = ceil($price * $profit / 100);
            
            if ($shop->get("money") < $param["EQ_PRICE"][$i])
                return NOT_ENOUGH_MONEY_IN_BUILDING;
            
            if (EquipFactory::getPriceToRepair($dbe->get("id"), $db) > 0) {
                return OBJECT_NOT_NEW_ERROR;
            }
            
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            
            // suppression de tous les éléments contenu dans le sac vendu
            $ddu = new DBCollection("DELETE FROM Equipment WHERE id_Equipment\$bag=" . $equip->get("id"), $db, 0, 0, false);
            
            $equip->set("id_Player", 0);
            $coef = 1;
            $dbx = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $equip->get("id_BasicEquipment"), $db);
            if ($equip->get("id_BasicEquipment") >= 200 && $equip->get("id_BasicEquipment") <= 206 && $equip->get("id_BasicEquipment") != 205)
                $coef = $equip->get("level");
            $equip->set("durability", $dbx->get("durability") * $coef);
            $equip->set("id_Equipment\$bag", 0);
            $equip->set("id_Shop", $playerSrc->get("inbuilding"));
            $equip->updateDB($db);
            
            $shop->set("money", $shop->get("money") - $param["EQ_PRICE"][$i]);
            $shop->updateDB($db);
            
            $param["TOTAL_PRICE"] += $param["EQ_PRICE"][$i];
            $i ++;
            $dbe->next();
        }
        
        $param["NOMBRE"] = $i;
        $playerSrc->set("money", ($playerSrc->get("money") + $param["TOTAL_PRICE"]));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = EXCHANGE_OBJECT;
        $param["TYPE_ATTACK"] = EXCHANGE_OBJECT_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        $date = gmdate("Y-m-d H:i:s");
        if ($dbc->get("captured") > 3)
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . ",-" . $param["TOTAL_PRICE"] . ",-" .
                     $param["TOTAL_PRICE"] . ",'" . $date . "')", $db, 0, 0, false);
        
        return ACTION_NO_ERROR;
    }

    static function repairEntrepot($playerSrc, $idEquip, $price, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        $profit = CityFactory::getBuildingActionProfit($playerSrc->get("inbuilding"), SHOP_REPAIR_EX, $db);
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (count($idEquip) == 0) {
            return ACTION_FIELD_ERROR; // mettre la bonne erreur;
        }
        
        if ($playerSrc->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        $shop = new Building();
        $shop->load($playerSrc->get("inbuilding"), $db);
        
        $where = self::getCondFromArray($idEquip, "Equipment.id", "OR");
        $dbe = new DBCollection("SELECT *  FROM Equipment WHERE " . $where, $db);
        $i = 0;
        $param["TOTAL_PRICE"] = 0;
        $param["TAXE"] = 0;
        
        while (! $dbe->eof()) {
            
            $param["EQ_NAME"][$i] = $dbe->get("name") . " " . $dbe->get("extraname");
            $param["EQ_PRICE"][$i] = EquipFactory::getPriceToRepair($dbe->get("id"), $db);
            $param["EQ_PRICE"][$i] += floor($param["EQ_PRICE"][$i] * ($profit - 30) / 100);
            
            $param["TAXE"] += floor(EquipFactory::getPriceToRepair($dbe->get("id"), $db) * $profit / 100);
            
            $equip = new Equipment();
            $equip->load($dbe->get("id"), $db);
            $dbd = new DBCollection("SELECT durability FROM BasicEquipment WHERE id=" . $equip->get("id_BasicEquipment"), $db);
            if ($dbe->get("id_BasicEquipment") >= 200 && $dbe->get("id_BasicEquipment") <= 206 && $dbe->get("id_BasicEquipment") != 205)
                $durMax = $dbd->get("durability") * $dbe->get("level");
            else
                $durMax = $dbd->get("durability");
            $equip->set("durability", $durMax);
            $equip->updateDB($db);
            $param["TOTAL_PRICE"] += $param["EQ_PRICE"][$i];
            $i ++;
            $dbe->next();
        }
        
        $param["NOMBRE"] = $i;
        $playerSrc->set("money", ($playerSrc->get("money") - $param["TOTAL_PRICE"]));
        
        // taxe
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $param["TAXE"] . " WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        
        PlayerFactory::initBM($playerSrc, $playerSrc->getObj("Modifier"), $param, $db, 0);
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = SHOP_REPAIR_EX;
        $param["TYPE_ATTACK"] = SHOP_REPAIR_EX_EVENT;
        
        // pour le livre des comptes de la cité si elle est contrôlé
        $dbc = new DBCollection(
            "SELECT City.captured as captured, City.id as id FROM Building LEFT JOIN City ON Building.id_City=City.id WHERE Building.id=" . $playerSrc->get("inbuilding"), $db);
        
        if ($dbc->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbc->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["TOTAL_PRICE"] . "," .
                     $param["TAXE"] . ",'" . $date . "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    /* ***************************************************************PALAIS DU GOUVERRNEUR **************************************************** */
    static function buildingConstruct($playerSrc, $x, $y, $basicBuilding, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($basicBuilding <= 0)
            return BUILDING_FIELD_INVALID;
        
        if ($playerSrc->get("ap") < BUILDING_CONSTRUCTION_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE id=" . $basicBuilding, $db);
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $dbt = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=14 AND id_City=" . $dbc->get("id_City"), $db);
        
        $dbfs = new DBCollection(
            "SELECT MapGround.* FROM MapGround left outer join Building on MapGround.x=Building.x and MapGround.y=Building.y and MapGround.map =Building.map
                            WHERE (abs(MapGround.x-" .
                 $dbt->get("x") . ") + abs(MapGround.y-" . $dbt->get("y") . ") + abs(MapGround.x+MapGround.y-" . $dbt->get("x") . "-" . $dbt->get("y") .
                 "))/2 <4 and MapGround.map=1 and Building.y is null", $db);
        if ($dbfs->count() <= 3)
            return NOT_ENOUGH_SPACE_IN_VILLAGE;
        
        if ($dbh->get("money") < $dbb->get("price"))
            return NOT_ENOUGH_MONEY_ERROR2;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! BasicActionFactory::freePlace($x, $y, $playerSrc->get("map"), $db, 1) || distHexa($dbt->get("x"), $dbt->get("y"), $x, $y) > 3)
            return PLACE_FIELD_INVALID;
        
        $param["PRICE"] = $dbb->get("price");
        $param["NAME"] = $dbb->get("name");
        switch ($dbb->get("solidity")) {
            case "Fragile":
                $sp = FRAGILE;
                break;
            case "Medium":
                $sp = MEDIUM;
                break;
            case "Solid":
                $sp = SOLID;
                break;
            default:
                $sp = FRAGILE;
                break;
        }
        
        $param["CONSTRUCTION_DURATION_MIN"] = ceil($sp / ($dbc->get("level") * CONSTRUCTION_SPEED_MAX));
        $param["CONSTRUCTION_DURATION_MAX"] = ceil($sp / ($dbc->get("level") * CONSTRUCTION_SPEED_MIN));
        $param["X"] = $x;
        $param["Y"] = $y;
        
        $h = 0;
        $building = new Building();
        $building->set("x", $x);
        $building->set("y", $y);
        $building->set("id_BasicBuilding", $basicBuilding);
        $building->set("sp", $sp);
        $building->set("currsp", $sp);
        $building->set("repair", 1);
        $building->set("money", 1);
        if ($basicBuilding == 10)
            $building->set("money", - 1);
        $building->set("level", 1);
        if ($basicBuilding == 8) {
            $building->set("level", 0);
        }
        $building->set("name", $dbb->get("name"));
        $building->set("id_Player", 0);
        $building->set("id_City", $dbc->get("id_City"));
        $building->set("progress", 1);
        
        $schoolList = array();
        if ($basicBuilding == 6 or $basicBuilding == 7) {
            $schoolList = self::initSchoolList($basicBuilding);
            $building->set("school", serialize($schoolList));
        }
        
        $profit = array();
        if ($basicBuilding != 10) {
            $profit = self::initBuildingProfit($basicBuilding, $db);
            $building->set("profit", serialize($profit));
        }
        
        $building->addDB($db);
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - BUILDING_CONSTRUCTION_AP));
        $dbu = new DBCollection("UPDATE City SET money=money-" . $dbb->get("price") . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        // $playerSrc->set("money",($playerSrc->get("money") - $dbb->get("price")));
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BUILDING_CONSTRUCT;
        $param["TYPE_ATTACK"] = BUILDING_CONSTRUCT_EVENT;
        
        if ($dbh->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbh->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    static function buildingRepair($playerSrc, $id_Building, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($id_Building <= 0)
            return BUILDING_FIELD_INVALID;
        
        if ($playerSrc->get("ap") < BUILDING_REPAIR_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        require_once (HOMEPATH . "/factory/CityFactory.inc.php");
        $price = CityFactory::getPriceToRepairBuilding($id_Building, $db);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id_Building, $db);
        $dbh = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbt = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        if ($dbt->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["PRICE"] = $price;
        $param["ID"] = $dbc->get("id");
        $param["NAME"] = $dbc->get("name");
        
        $spp = $dbc->get("sp") - $dbc->get("currsp");
        $param["CONSTRUCTION_DURATION_MIN"] = ceil($spp / ($dbh->get("level") * floor(CONSTRUCTION_SPEED_MAX / 4)));
        $param["CONSTRUCTION_DURATION_MAX"] = ceil($spp / ($dbh->get("level") * floor(CONSTRUCTION_SPEED_MIN / 4)));
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - BUILDING_REPAIR_AP));
        $dbu = new DBCollection("UPDATE City SET money=money-" . $price . " WHERE id=" . $dbt->get("id"), $db, 0, 0, false);
        
        if ($dbc->get("progress") == 0)
            $dbu = new DBCollection("UPDATE Building SET repair=1, currsp=sp WHERE id=" . $id_Building, $db, 0, 0, false);
        else
            $dbu = new DBCollection("UPDATE Building SET repair=3 WHERE id=" . $id_Building, $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = BUILDING_REPAIR;
        $param["TYPE_ATTACK"] = BUILDING_REPAIR_EVENT;
        
        if ($dbt->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbt->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    static function buildingDestroy($playerSrc, $id_Building, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($id_Building <= 0)
            return BUILDING_FIELD_INVALID;
        
        if ($playerSrc->get("ap") < BUILDING_DESTROY_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $id_Building, $db);
        
        $priceToDestroy = floor($dbc->get("sp") * BUILDING_DESTROY_PRICE / 500);
        
        $dbh = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbt = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        if ($dbt->get("loyalty") < 70)
            return ERROR_LOYALTY_TOO_SMALL;
        
        if ($dbt->get("money") < $priceToDestroy)
            return NOT_ENOUGH_MONEY_ERROR2;
        
        $dbu = new DBCollection("UPDATE Building SET repair=2 WHERE id=" . $id_Building, $db, 0, 0, false);
        
        $param["PRICE"] = $priceToDestroy;
        $param["NAME"] = $dbc->get("name");
        
        $param["CONSTRUCTION_DURATION_MIN"] = ceil($dbc->get("progress") / ($dbh->get("level") * CONSTRUCTION_SPEED_MAX * 1.8));
        $param["CONSTRUCTION_DURATION_MAX"] = ceil($dbc->get("progress") / ($dbh->get("level") * CONSTRUCTION_SPEED_MIN * 1.8));
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - BUILDING_DESTROY_AP));
        // $playerSrc->set("money",($playerSrc->get("money") - BUILDING_DESTROY_PRICE));
        $dbu = new DBCollection("UPDATE City SET money=money-" . $priceToDestroy . " WHERE id=" . $dbt->get("id"), $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["ID"] = $dbc->get("id");
        $param["TYPE_ACTION"] = BUILDING_DESTROY;
        $param["TYPE_ATTACK"] = BUILDING_DESTROY_EVENT;
        
        if ($dbt->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbt->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        return ACTION_NO_ERROR;
    }

    /* Fonction pour nommer le village par le gouverneur */
    static function nameVillage($playerSrc, $nameVillage, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        if ($playerSrc->get("ap") < NAME_VILLAGE_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($dbh->get("money") < NAME_VILLAGE_PRICE) {
            return NOT_ENOUGH_MONEY_ERROR2;
        } else {
            $param["NAME"] = $dbc->get("name");
            $param["NAME_VILLAGE"] = $nameVillage;
        }
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["PRICE"] = NAME_VILLAGE_PRICE;
        $param["AP"] = NAME_VILLAGE_AP;
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - NAME_VILLAGE_AP));
        $dbu = new DBCollection("UPDATE City SET money=money-" . NAME_VILLAGE_PRICE . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        $dbu2 = new DBCollection("UPDATE City SET name='" . $nameVillage . "' WHERE id=" . $dbc->get("id_City"), $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = NAME_VILLAGE;
        $param["TYPE_ATTACK"] = NAME_VILLAGE_EVENT;
        
        if ($dbh->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbh->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    /* fonction permettant de fortifier le village */
    static function fortifyVillage($playerSrc, $fortifyVillage, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < FORTIFY_VILLAGE_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $citySize = 4;
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE name='Rempart'", $db);
        
        $dbjBeatenEarth = new DBCollection(
            "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map and City.id = " . $dbh->get("id") . " and MapGround.x between City.x-" . $citySize .
                 " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize . " and City.y+" . $citySize .
                 " inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                 $citySize . " and Ground.ground not in('cobblestone','beatenearth') group by City.id", $db);
        if ($dbjBeatenEarth->count() > 1) {
            return ERROR_TERRASSEMENT_NOT_DONE;
        }
        
        $price = $dbb->get("price") * 24;
        
        if ($dbh->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR2;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["PRICE"] = $price;
        $param["AP"] = FORTIFY_VILLAGE_AP;
        
        $dbt = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $dbh->get("id"), $db);
        $sp = CityFactory::getBuildingSP(16, $db);
        
        // Suprresion des rempart précédent
        $dbgh = new DBCollection("DELETE FROM Building WHERE name='Rempart' AND id_City=" . $dbh->get("id"), $db, 0, 0, false);
        
        // Récupération id du temple
        $dbt = new DBCollection("SELECT * FROM Building WHERE name='Temple' AND id_City=" . $dbh->get("id"), $db);
        $xp = $dbt->get("x");
        $yp = $dbt->get("y");
        
        // Update du captured du village
        $dbu = new DBCollection("UPDATE City SET captured=4 WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        // Update du hidden du prêtre et du garde
        $dbu = new DBCollection("UPDATE Player SET hidden=10 WHERE status='NPC' and racename='Garde du Palais' and inbuilding=" . $playerSrc->get("inbuilding"), $db, 0, 0, false);
        $dbu = new DBCollection("UPDATE Player SET hidden=10 WHERE status='NPC' and racename='Prêtre' and inbuilding=" . $dbt->get("id"), $db, 0, 0, false);
        // Update des joueurs dans la ville et sur les remparts
        $dbp = new DBCollection("UPDATE Player SET hidden=hidden+10 WHERE (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 < 4", $db, 0, 0, false);
        $dbp = new DBCollection("UPDATE Player SET hidden=hidden+20 WHERE (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 =4", $db, 0, 0, false);
        
        for ($i = - 4; $i < 5; $i ++) {
            for ($j = - 4; $j < 5; $j ++) {
                if (distHexa($dbc->get("x"), $dbc->get("y"), $dbc->get("x") + $i, $dbc->get("y") + $j) == 4) {
                    $wall = new Building();
                    $wall->set("x", $dbt->get("x") + $i);
                    $wall->set("y", $dbt->get("y") + $j);
                    $wall->set("map", $playerSrc->get("map"));
                    $wall->set("id_Player", $dbc->get("id_Player"));
                    $wall->set("id_City", $dbc->get("id_City"));
                    $wall->set("name", "Rempart");
                    
                    if (($i < 0 && $j > 0) || ($i > 0 && $j < 0))
                        $wall->set("id_BasicBuilding", 16);
                    
                    if (($i > 0 && $j > 0) || $i < 0 && $j < 0)
                        $wall->set("id_BasicBuilding", 17);
                    
                    if (($i > 0 && $j == - 4) || ($i < 0 && $j == 4))
                        $wall->set("id_BasicBuilding", 18);
                    
                    if ($i == 4 && $j == - 4)
                        $wall->set("id_BasicBuilding", 20);
                    
                    if ($i == 4 && $j == 0)
                        $wall->set("id_BasicBuilding", 22);
                    
                    if ($i == 0 && $j == 4)
                        $wall->set("id_BasicBuilding", 19);
                    
                    if ($i == - 4 && $j == 4)
                        $wall->set("id_BasicBuilding", 25);
                    
                    if ($i == - 4 && $j == 0)
                        $wall->set("id_BasicBuilding", 24);
                    
                    if ($i == 0 && $j == - 4)
                        $wall->set("id_BasicBuilding", 23);
                    
                    if (($i == 2 && $j == - 4) || ($i == - 2 && $j == 4)) {
                        $wall->set("id_BasicBuilding", 26);
                        $wall->set("name", "Porte Ouverte");
                    }
                    $wall->set("level", 1);
                    $wall->set("sp", $sp);
                    
                    $wall->set("currsp", $sp);
                    $wall->set("repair", 1);
                    $wall->set("progress", 0);
                    $id_wall = $wall->addDB($db);
                }
            }
        }
        
        $param["CONSTRUCTION_DURATION_MIN"] = ceil($sp / ($dbc->get("level") * CONSTRUCTION_SPEED_MAX));
        $param["CONSTRUCTION_DURATION_MAX"] = ceil($sp / ($dbc->get("level") * CONSTRUCTION_SPEED_MIN));
        $playerSrc->set("ap", ($playerSrc->get("ap") - FORTIFY_VILLAGE_AP));
        $dbu = new DBCollection("UPDATE City SET money=money-" . $price . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        // update de l'hidden du joueur qui lance la fortification car la requête est bloqué par la session.
        $playerSrc->updateHidden($db);
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = FORTIFY_VILLAGE;
        $param["TYPE_ATTACK"] = FORTIFY_VILLAGE_EVENT;
        
        if ($dbh->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbh->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    static function upgradeFortification($playerSrc, $id_City, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < FORTIFY_VILLAGE_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        $dbr = new DBCollection("SELECT * FROM Building WHERE name='Rempart' AND id_City=" . $dbc->get("id_City"), $db);
        $price = CityFactory::getPriceToUpgradeBuilding($dbr->get("id"), $db) * 24;
        
        $citySize = 4;
        $dbjBeatenEarth = new DBCollection(
            "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map and City.id = " . $dbh->get("id") . " and MapGround.x between City.x-" . $citySize .
                 " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize . " and City.y+" . $citySize .
                 " inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                 $citySize . " and Ground.ground not in('cobblestone','beatenearth') group by City.id", $db);
        if ($dbjBeatenEarth->count() > 1) {
            return ERROR_TERRASSEMENT_NOT_DONE;
        }
        
        if ($dbh->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR2;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["PRICE"] = $price;
        $param["AP"] = FORTIFY_VILLAGE_AP;
        
        $param["CONSTRUCTION_DURATION_MIN"] = ceil(1000 / ($dbc->get("level") * floor(CONSTRUCTION_SPEED_MAX / 4)));
        $param["CONSTRUCTION_DURATION_MAX"] = ceil(1000 / ($dbc->get("level") * floor(CONSTRUCTION_SPEED_MIN / 4)));
        
        $dbt = new DBCollection(
            "UPDATE Building SET progress=progress+" . FRAGILE . ", level=level+1, sp=sp+" . FRAGILE .
                 ", repair=3 WHERE (name='Rempart' or id_BasicBuilding=21 or id_BasicBuilding=26) AND id_City=" . $id_City, $db, 0, 0, false);
        $dbu = new DBCollection("UPDATE City SET money=money-" . $price . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - FORTIFY_VILLAGE_AP));
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = UPGRADE_FORTIFICATION;
        $param["TYPE_ATTACK"] = UPGRADE_FORTIFICATION_EVENT;
        if ($dbh->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbh->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    /* fonction permettant de terrasser le village */
    static function terrassementVillage($playerSrc, $choixTerrassement, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < TERRASSEMENT_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $citySize = 4;
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE name='Maison'", $db);
        $price = floor($dbb->get("price") / 4);
        if ($choixTerrassement == 'beatenearth') {
            $dbjBeatenEarth = new DBCollection(
                "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " .
                     $dbh->get("id") . " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize . " and City.y+" .
                     $citySize . "
               inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                     $citySize . " and Ground.ground not in('cobblestone','beatenearth','river','ocean') group by City.id", $db);
            $price = $price * $dbjBeatenEarth->get("nb");
        } else {
            if ($choixTerrassement == 'cobblestone') {
                $dbjCobbleStone = new DBCollection(
                    "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " .
                         $dbh->get("id") . " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize .
                         " and City.y+" . $citySize . "
               inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                         $citySize . " and Ground.ground not in('cobblestone','river','ocean') group by City.id", $db);
                $price = $price * $dbjCobbleStone->get("nb") * 2;
            } else {
                return ACTION_NOT_ALLOWED;
            }
        }
        
        if ($dbh->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR2;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["PRICE"] = $price;
        $param["AP"] = TERRASSEMENT_AP;
        
        $dbGround = new DBCollection("select * from Ground where ground = '" . $choixTerrassement . "'", $db);
        $param["TYPE_TERRASSEMENT"] = $dbGround->get("label");
        
        if ($choixTerrassement == 'beatenearth') {
            $dbjBeatenEarth = new DBCollection(
                "UPDATE `City`,MapGround,Ground set MapGround.id_Ground = '" . $dbGround->get("id") . "' where City.map=MapGround.map and Ground.id = MapGround.id_Ground 
               and City.id = " .
                     $dbh->get("id") . " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize . " and City.y+" .
                     $citySize . "
               and (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" . $citySize .
                     " and Ground.ground not in('cobblestone','beatenearth','river','ocean')", $db);
        } else {
            if ($choixTerrassement == 'cobblestone') {
                $dbjCobbleStone = new DBCollection(
                    "UPDATE `City`,MapGround,Ground set MapGround.id_Ground = '" . $dbGround->get("id") . "' where City.map=MapGround.map and Ground.id = MapGround.id_Ground 
               and City.id = " .
                         $dbh->get("id") . " and MapGround.x between City.x-" . $citySize . " and City.x+" . $citySize . " and MapGround.y between City.y-" . $citySize .
                         " and City.y+" . $citySize . "
               and (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" . $citySize .
                         " and Ground.ground not in('cobblestone','river','ocean')", $db);
            } else {
                return ACTION_NOT_ALLOWED;
            }
        }
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - TERRASSEMENT_AP));
        $dbu = new DBCollection("UPDATE City SET money=money-" . $price . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        // update de l'hidden du joueur qui lance la fortification car la requête est bloqué par la session.
        $playerSrc->updateHidden($db);
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = TERRASSEMENT;
        $param["TYPE_ATTACK"] = TERRASSEMENT_EVENT;
        
        if ($dbh->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbh->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    /* fonction permettant de terrasser le village */
    static function terraformationVillage($playerSrc, $choixTerraformation, $case, $ground, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($choixTerraformation == "choice" && $playerSrc->get("ap") < (TERRAFORMATION_AP / 10))
            return ACTION_NOT_ENOUGH_AP;
        if ($choixTerraformation != "choice" && $playerSrc->get("ap") < TERRAFORMATION_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        $citySize = 4;
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        $dbb = new DBCollection("SELECT * FROM BasicBuilding WHERE name='Maison'", $db);
        $price = floor($dbb->get("price") / 4);
        
        $param["AP"] = TERRAFORMATION_AP;
        $param["TERRAFORMATION_CASE"] = "";
        if ($choixTerraformation == 'beatenearth') {
            $dbjBeatenEarth = new DBCollection(
                "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " .
                     $dbh->get("id") . " and MapGround.x between City.x-" . ($citySize + 1) . " and City.x+" . ($citySize + 1) . " and MapGround.y between City.y-" . ($citySize + 1) .
                     " and City.y+" . ($citySize + 1) . "
                inner join Ground on Ground.id = MapGround.id_Ground  where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                     ($citySize + 1) . " and Ground.ground not in('cobblestone','beatenearth','river','ocean') group by City.id", $db);
            $price = $price * $dbjBeatenEarth->get("nb");
        } else {
            if ($choixTerraformation == 'cobblestone') {
                $dbjCobbleStone = new DBCollection(
                    "SELECT sum(cost_PA) as nb FROM `City` inner join MapGround on City.map=MapGround.map 
               and City.id = " .
                         $dbh->get("id") . " and MapGround.x between City.x-" . ($citySize + 1) . " and City.x+" . ($citySize + 1) . " and MapGround.y between City.y-" .
                         ($citySize + 1) . " and City.y+" . ($citySize + 1) . "
               inner join Ground on Ground.id = MapGround.id_Ground where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" .
                         ($citySize + 1) . " and Ground.ground not in('cobblestone','river','ocean') group by City.id", $db);
                $price = $price * $dbjCobbleStone->get("nb") * 2;
            } else {
                if ($choixTerraformation == 'choice') {
                    switch ($ground) {
                        case "plain":
                        case "mountain":
                        case "marsh":
                        case "forest":
                        case "desert":
                        case "cobblestone":
                        case "beatenearth":
                            $dbjChoice = new DBCollection(
                                "SELECT MapGround.x, MapGround.y,ground,cost_PA  FROM `City` inner join MapGround on City.map=MapGround.map 
 					   and City.id = " .
                                     $dbh->get("id") . " and MapGround.id = " . $case . " and MapGround.x between City.x-" . ($citySize + 2) . " and City.x+" . ($citySize + 2) .
                                     " and MapGround.y between City.y-" . ($citySize + 2) . " and City.y+" . ($citySize + 2) . "
					   inner join Ground on Ground.id = MapGround.id_Ground 
					   where (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 between " .
                                     ($citySize + 1) . " and " . ($citySize + 2) . " and Ground.ground not in ('river','ocean')", $db);
                            if ($dbjChoice->count() != 1) {
                                return ACTION_NOT_ALLOWED;
                            }
                            $param["TERRAFORMATION_CASE"] = $dbjChoice->get("x") . "/" . $dbjChoice->get("y");
                            $param["AP"] = (TERRAFORMATION_AP / 10);
                            $dbGround = new DBCollection("select * from Ground where ground = '" . $ground . "'", $db);
                            $price = floor($price * 4 * $dbjChoice->get("cost_PA") * $dbGround->get("cost_PA"));
                            break;
                        default:
                            return ACTION_NOT_ALLOWED;
                            break;
                    }
                } else
                    return ACTION_NOT_ALLOWED;
            }
        }
        
        if ($dbh->get("money") < $price)
            return NOT_ENOUGH_MONEY_ERROR2;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["PRICE"] = $price;
        
        if ($choixTerraformation == 'beatenearth') {
            $dbGround = new DBCollection("select * from Ground where ground = '" . $choixTerraformation . "'", $db);
            $param["TYPE_TERRAFORMATION"] = $dbGround->get("label");
            $dbjBeatenEarth = new DBCollection(
                "UPDATE `City`,MapGround, Ground set MapGround.id_Ground = '" . $dbGround->get("id") . "' where City.map=MapGround.map and Ground.id = MapGround.id_Ground 
               and City.id = " .
                     $dbh->get("id") . " and MapGround.x between City.x-" . ($citySize + 1) . " and City.x+" . ($citySize + 1) . " and MapGround.y between City.y-" . ($citySize + 1) .
                     " and City.y+" . ($citySize + 1) . "
               and (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" . ($citySize + 1) .
                     " and Ground.ground not in('cobblestone','beatenearth','river','ocean')", $db);
        } else {
            if ($choixTerraformation == 'cobblestone') {
                $dbGround = new DBCollection("select * from Ground where ground = '" . $choixTerraformation . "'", $db);
                $param["TYPE_TERRAFORMATION"] = $dbGround->get("label");
                $dbjCobbleStone = new DBCollection(
                    "UPDATE `City`,MapGround, Ground set MapGround.id_Ground = '" . $dbGround->get("id") . "' where City.map=MapGround.map and Ground.id = MapGround.id_Ground 
               and City.id = " .
                         $dbh->get("id") . " and MapGround.x between City.x-" . ($citySize + 1) . " and City.x+" . ($citySize + 1) . " and MapGround.y between City.y-" .
                         ($citySize + 1) . " and City.y+" . ($citySize + 1) . "
               and (abs(City.x-MapGround.x) + abs(City.y-MapGround.y) + abs(City.x+City.y-MapGround.x-MapGround.y))/2 =" . ($citySize + 1) .
                         " and Ground.ground not in('cobblestone','river','ocean')", $db);
            } else {
                
                if ($choixTerraformation == 'choice') {
                    $dbGround = new DBCollection("select * from Ground where ground = '" . $ground . "'", $db);
                    $param["TYPE_TERRAFORMATION"] = $dbGround->get("label");
                    $dbjChoice = new DBCollection("UPDATE MapGround set MapGround.id_Ground = '" . $dbGround->get("id") . "' where  MapGround.id = " . $case, $db, 0, 0, false);
                } else
                    return ACTION_NOT_ALLOWED;
            }
        }
        
        $playerSrc->set("ap", ($playerSrc->get("ap") - $param["AP"]));
        $dbu = new DBCollection("UPDATE City SET money=money-" . $price . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        // update de l'hidden du joueur qui lance la fortification car la requête est bloqué par la session.
        $playerSrc->updateHidden($db);
        $playerSrc->updateDB($db);
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = TERRAFORMATION;
        $param["TYPE_ATTACK"] = TERRAFORMATION_EVENT;
        
        if ($dbh->get("captured") > 3) {
            $date = gmdate("Y-m-d H:i:s");
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $playerSrc->get("id") . "," .
                     $dbh->get("id") . "," . $playerSrc->get("inbuilding") . "," . $param["TYPE_ACTION"] . "," . $param["TYPE_ATTACK"] . "," . $param["PRICE"] . ",0,'" . $date .
                     "')", $db, 0, 0, false);
        }
        
        return ACTION_NO_ERROR;
    }

    /* fonction pour gérer les portes du village */
    static function operateVillage($playerSrc, $choice, $openDoorFriend, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        if ($choice > 1)
            $dbu = new DBCollection("UPDATE Building SET id_BasicBuilding=21, name='Porte Fermée' WHERE id_BasicBuilding=26 AND id_City=" . $dbc->get("id_City"), $db, 0, 0, false);
        
        if ($choice == 1)
            $dbu = new DBCollection("UPDATE Building SET id_BasicBuilding=26, name='Porte Ouverte' WHERE id_BasicBuilding=21 AND id_City=" . $dbc->get("id_City"), $db, 0, 0, false);
        
        $param["DOOR"] = $choice;
        
        if ($choice == 3) {
            $choice = 0;
            foreach ($openDoorFriend as $key) {
                $choice += $key;
            }
        }
        $dbu = new DBCollection("UPDATE City SET door=" . $choice . " WHERE id=" . $dbc->get("id_City"), $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["OPTION_ALLY"] = $choice;
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = OPERATE_VILLAGE;
        $param["TYPE_ATTACK"] = OPERATE_VILLAGE_EVENT;
        
        return ACTION_NO_ERROR;
    }

    /* fonction pour gérer les portes du temple */
    static function operateVillageTemple($playerSrc, $choice, $openDoorFriend, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        $param["DOOR_TEMPLE"] = $choice;
        
        if ($choice == 3) {
            $choice = 0;
            foreach ($openDoorFriend as $key) {
                $choice += $key;
            }
        }
        $dbu = new DBCollection("UPDATE City SET accessTemple=" . $choice . " WHERE id=" . $dbc->get("id_City"), $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["OPTION_ALLY"] = $choice;
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = OPERATE_VILLAGE_TEMPLE;
        $param["TYPE_ATTACK"] = OPERATE_VILLAGE_TEMPLE_EVENT;
        
        return ACTION_NO_ERROR;
    }

    static function operateVillageEntrepot($playerSrc, $choice, $openDoorFriend, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        $param["DOOR_TEMPLE"] = $choice;
        
        if ($choice == 3) {
            $choice = 0;
            foreach ($openDoorFriend as $key) {
                $choice += $key;
            }
        }
        $dbu = new DBCollection("UPDATE City SET accessEntrepot=" . $choice . " WHERE id=" . $dbc->get("id_City"), $db, 0, 0, false);
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["OPTION_ALLY"] = $choice;
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = OPERATE_VILLAGE_ENTREPOT;
        $param["TYPE_ATTACK"] = OPERATE_VILLAGE_ENTREPOT_EVENT;
        
        return ACTION_NO_ERROR;
    }

    static function setTax($playerSrc, $profit, $idBuilding, $default, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < SET_TAX_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        for ($i = 0; $i < count($profit); $i ++) {
            if ($profit[$i] < 0)
                return MONEY_FIELD_INVALID;
        }
        $param["DEFAULT"] = $default;
        $dbb = new DBCollection("SELECT * FROM Building WHERE id=" . $idBuilding, $db);
        $tax = array();
        
        $dbt = new DBCollection("SELECT * FROM BuildingAction WHERE taxable='Yes' AND id_BasicBuilding=" . $dbb->get("id_BasicBuilding"), $db);
        // unset($profit);
        $i = 0;
        while (! $dbt->eof()) {
            
            if ($default)
                $tax[$dbt->get("name")] = $dbt->get("profit");
            else
                $tax[$dbt->get("name")] = $profit[$i];
            
            $i ++;
            $dbt->next();
        }
        
        $dbu = new DBCollection("UPDATE Building SET profit='" . addslashes(serialize($tax)) . "' WHERE id=" . $idBuilding, $db, 0, 0, false);
        
        $param["BUILDING_NAME"] = $dbb->get("name");
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = TREASURY_SET_TAX;
        $param["TYPE_ATTACK"] = TREASURY_SET_TAX_EVENT;
        return ACTION_NO_ERROR;
    }

    static function PalaceDeposit($playerSrc, $moneydep, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        
        $param["MONEYDEP"] = $moneydep;
        $param["TYPE_ACTION"] = PALACE_DEPOSIT;
        $param["TYPE_ATTACK"] = PALACE_DEPOSIT_EVENT;
        
        if ($playerSrc->get("money") < $moneydep)
            return NOT_ENOUGH_MONEY_ERROR;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($moneydep) or (is_numeric($moneydep) and $moneydep < 0))
            return MONEY_FIELD_INVALID;
        
        $param["ERROR"] = 0;
        $dbb = new DBCollection("SELECT id_City FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $dbu = new DBCollection("UPDATE City SET money=money+" . $moneydep . " WHERE id=" . $dbb->get("id_City"), $db, 0, 0, false);
        $playerSrc->set("money", ($playerSrc->get("money") - $moneydep));
        $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function palaceWithdraw($playerSrc, $money, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = PALACE_WITHDRAW;
        $param["TYPE_ATTACK"] = PALACE_WITHDRAW_EVENT;
        $param["MONEY"] = $money;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($money) or (is_numeric($money) and $money < 0))
            return MONEY_FIELD_INVALID;
        
        $dbb = new DBCollection("SELECT id_City FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $dbc = new DBCollection("SELECT money FROM City WHERE id=" . $dbb->get("id_City"), $db);
        
        if ($dbc->get("money") < $money)
            return NOT_ENOUGH_MONEY_ERROR;
        
        $param["ERROR"] = 0;
        $dbu = new DBCollection("UPDATE City SET money=money-" . $money . " WHERE id=" . $dbb->get("id_City"), $db, 0, 0, false);
        $playerSrc->set("money", ($playerSrc->get("money") + $money));
        $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }

    static function palaceTransferMoneyBat($playerSrc, $idBatiment, $money, &$param, $db)
    {
        self::globalInfo($playerSrc, $param);
        $param["TYPE_ACTION"] = PALACE_TRANSFER_MONEY;
        $param["TYPE_ATTACK"] = PALACE_TRANSFER_MONEY_EVENT;
        $param["MONEY"] = $money;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if (! is_numeric($money) or (is_numeric($money) and $money < 0))
            return MONEY_FIELD_INVALID;
        
        $dbb = new DBCollection("SELECT id_City FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db, 0, 0);
        $dbnb = new DBCollection("SELECT id,name FROM Building WHERE id=" . $idBatiment . "  AND money!=-1 AND id_BasicBuilding!=11 AND id_BasicBuilding!=14", $db, 0, 0);
        $dbc = new DBCollection("SELECT money FROM City WHERE id=" . $dbb->get("id_City"), $db);
        
        if ($dbnb->eof())
            return ACTION_NOT_ALLOWED;
        
        if ($dbc->get("money") < $money)
            return NOT_ENOUGH_MONEY_ERROR;
        
        $param["BAT_NAME"] = $dbnb->get("name");
        $param["BAT_ID"] = $dbnb->get("id");
        
        $param["ERROR"] = 0;
        $dbu = new DBCollection("UPDATE City SET money=money-" . $money . " WHERE id=" . $dbb->get("id_City"), $db, 0, 0, false);
        $dbu = new DBCollection("UPDATE Building SET money=money+" . $money . " WHERE id=" . $idBatiment, $db, 0, 0, false);
        $playerSrc->updateDB($db);
        
        return ACTION_NO_ERROR;
    }
    
    // --------------- Décision sale du gouverneur -----------------------
    static function palaceDecision($playerSrc, $decision, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < PALACE_DECISION_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $param["TYPE_ACTION"] = PALACE_DECISION;
        $param["TYPE_ATTACK"] = PALACE_DECISION_EVENT;
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        switch ($decision) {
            case 1:
                $loyal = max(0, $dbh->get("loyalty") - 10);
                $dbu = new DBCollection("UPDATE City SET loyalty=" . $loyal . " WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
                break;
            case 2:
                $dbu = new DBCollection("UPDATE City SET loyalty=loyalty+10 WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
                break;
            case 3:
                $npc = new Player();
                $npc = PNJFactory::getPNJFromLevel(100, 5, $db);
                $npc->set("x", $playerSrc->get("x"));
                $npc->set("y", $playerSrc->get("y"));
                $npc->set("map", $playerSrc->get("map"));
                $npc->set("id_City", $dbc->get("id_City"));
                $npc->set("id_BasicRace", 5);
                $npc->set("racename", "Garde du Palais");
                $npc->set("inbuilding", $playerSrc->get("inbuilding"));
                $npc->set("room", 24);
                $npc->updateHidden($db);
                $npc->updateDBr($db);
                PlayerFactory::NewATB($npc, $param2, $db);
                $param["TYPE_ATTACK"] = PALACE_HIREGUARD_EVENT;
                break;
            case 4:
        }
        
        $playerSrc->set("ap", $playerSrc->get("ap") - PALACE_DECISION_AP);
        $playerSrc->updateDB($db);
        $param["DECISION"] = $decision;
        $param["ERROR"] = 0;
        
        return ACTION_NO_ERROR;
    }

    static function createPutsch($playerSrc, $playerName, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < PALACE_DECISION_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        $dbp = new DBCollection("SELECT * FROM Player WHERE name='" . $playerName . "'", $db);
        if ($dbp->count() == 0)
            return ERROR_NOT_VALID_NAME;
        
        $leader = new Player();
        if ($leader->load($dbp->get("id"), $db) == - 1)
            return ERROR_TARGET_LOADING;
        
        BasicActionFactory::globalInfoOpponent($leader, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        if ($dbp->get("id") == $dbh->get("id_Player"))
            return ERROR_ALREADY_GOUVERNOR;
        
        if ($dbh->get("putsch") != "")
            $putsch = unserialize($dbh->get("putsch"));
        else
            $putsch = array();
        
        if (isset($putsch[$playerName]))
            return ERROR_PUTSCH_ALREADY_EXIST;
        
        $putsch[$playerName] = 10;
        $dbu = new DBCollection("UPDATE City SET putsch='" . serialize($putsch) . "' WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - PALACE_DECISION_AP);
        $playerSrc->updateDB($db);
        $param["LEADER_NAME"] = $playerName;
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = CREATE_PUTSCH;
        $param["TYPE_ATTACK"] = CREATE_PUTSCH_EVENT;
        
        if ($dbh->get("captured") > 3) {
            $subject = "Un putsch a été lancé " . $dbh->get("id") . "(" . $dbh->get("id") . ")";
            $body = "Très cher Gouverneur,\nUne partie dissidente du village a choisi de soutenir un nouveau gouverneur: " . $playerName . "(" . $dbp->get("id") .
                 ") .\n\nEn conséquence un putsch vient d'y être lancé.";
            $body .= "\nNous commes un petit nombre à continuer à vous soutenir. Veuillez rentrer en urgence pour garder la gouvernance du village.";
            $body .= "\n\nUn groupe de citoyens dévoués.";
            self::informCityMember($dbh->get("id"), $subject, $body, $db);
        }
        
        return ACTION_NO_ERROR;
    }

    static function getCityId($playerSrc, $db)
    {
        if ($playerSrc->isInCity($db)) {
            $dbt = new DBCollection(
                "SELECT * FROM Building WHERE name='Temple' AND (abs(x-" . $playerSrc->get("x") . ") + abs(y-" . $playerSrc->get("y") . ") + abs(x+y-" . $playerSrc->get("x") . "-" .
                     $playerSrc->get("y") . "))/2 <=5", $db);
            if (! $dbt->eof()) {
                return $dbt->get("id_City");
            } else {
                return - 1;
            }
        } else {
            return - 1;
        }
    }

    static function informCityMember($idCity, $subject, $body, $db)
    {
        $dbc = new DBCollection("SELECT Player.*,City.captured FROM Player inner join City on City.id_Player = Player.id WHERE City.id=" . $idCity, $db);
        if ($dbc->get("captured") > 3) {
            
            $format2 = "Y-m-d H:i:s";
            $time = (time() + date("I") * 3600) - 2 * 7 * 24 * 3600;
            $date = date($format2, $time);
            
            $dbe = new DBCollection("SELECT BuildingEvent.date from BuildingEvent WHERE BuildingEvent.id_City=" . $idCity . " and date > '" . $date . "'", $db);
            if (! $dbe->eof()) { // activité dans la ville depuis 14 jours
                
                $player = new Player();
                $player->load($dbc->get("id"), $db);
                
                $dest = array();
                if ($dbc->get("id_Team") > 0) {
                    // 24 - salle du gouverneur -17 pour avoir auth7
                    $dbt = new DBCollection(
                        "SELECT Player.name FROM TeamRank inner join TeamRankInfo on  TeamRank.id_TeamRankInfo=TeamRankInfo.id inner join Player on Player.id=TeamRank.id_Player WHERE TeamRankInfo.auth7='Yes' and TeamRank.id_Team=" .
                             $dbc->get("id_Team"), $db);
                    while (! $dbt->eof()) {
                        $dest[$dbt->get("name")] = $dbt->get("name");
                        $dbt->next();
                    }
                } else {
                    $dest[$dbc->get("name")] = $dbc->get("name");
                }
                MailFactory::sendMsg($player, $subject, $body, $dest, 2, $err, $db, 10, 0);
            }
        }
    }

    static function actPutsch($playerSrc, $playerName, $choice, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        if ($playerSrc->get("ap") < PALACE_DECISION_AP)
            return ACTION_NOT_ENOUGH_AP;
        
        if ($playerSrc->get("inbuilding") == 0)
            return ERROR_NOT_IN_BUILDING;
        
        if ($choice == 0)
            return CHOICE_FIELD_INVALID;
        
        if ($playerName == "")
            return TARGET_FIELD_INVALID;
        
        $dbp = new DBCollection("SELECT * FROM Player WHERE name='" . $playerName . "'", $db);
        $leader = new Player();
        
        if ($leader->load($dbp->get("id"), $db) == - 1)
            return ERROR_TARGET_LOADING;
        
        BasicActionFactory::globalInfoOpponent($leader, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbh = new DBCollection("SELECT * FROM City WHERE id=" . $dbc->get("id_City"), $db);
        
        $param["EFFECT"] = 0;
        if ($choice == 1) {
            $param["BONUS"] = - 10;
            $param["TYPE_ATTACK"] = STRUGGLE_PUTSCH_EVENT;
        } else {
            $param["BONUS"] = 10;
            $param["TYPE_ATTACK"] = SUPPORT_PUTSCH_EVENT;
        }
        $putsch = unserialize($dbh->get("putsch"));
        foreach ($putsch as $name => $percent) {
            
            if ($name === $playerName) {
                
                $putsch[$playerName] += $param["BONUS"];
                if ($putsch[$playerName] == 100) {
                    $param["TYPE_ATTACK"] = PUTSCH_EVENT_SUCCESS;
                    
                    self::changeGouvernor($dbh->get("id"), $playerName, $db);
                    unset($putsch);
                    $putsch = array();
                    $param["EFFECT"] = 3;
                } elseif ($putsch[$playerName] == 0) {
                    unset($putsch[$playerName]);
                    $param["EFFECT"] = 4;
                }
            }
        }
        
        $dbu = new DBCollection("UPDATE City SET putsch='" . serialize($putsch) . "' WHERE id=" . $dbh->get("id"), $db, 0, 0, false);
        
        $playerSrc->set("ap", $playerSrc->get("ap") - PALACE_DECISION_AP);
        $playerSrc->updateDB($db);
        $param["LEADER_NAME"] = $playerName;
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = ACT_PUTSCH;
        
        return ACTION_NO_ERROR;
    }

    static function controlTemple($playerSrc, $bannis, &$param, $db, $autoUpdate = 1)
    {
        self::globalInfo($playerSrc, $param);
        
        $dbc = new DBCollection("SELECT * FROM Building WHERE id=" . $playerSrc->get("inbuilding"), $db);
        $dbt = new DBCollection("SELECT * FROM Building WHERE id_City=" . $dbc->get("id_City") . " and name='Temple'", $db);
        $xp = $dbt->get("x");
        $yp = $dbt->get("y");
        
        // Détermination du temple non capturé le plus proche
        $dbnt = new DBCollection(
            "SELECT id,x,y, id_City, ((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2) AS dist FROM Building WHERE name='Temple' ORDER BY dist", $db);
        $done = 0;
        while (! $done) {
            $dbcity = new DBCollection("SELECT * FROM City WHERE id=" . $dbnt->get("id_City"), $db);
            if ($dbcity->get("captured") < 4) {
                $newTempleID = $dbnt->get("id");
                $x = $dbnt->get("x");
                $y = $dbnt->get("y");
                $done = 1;
            }
            $dbnt->next();
        }
        
        $namePl = array();
        
        // Création du message envoyé à tous les personnages bannis.
        require_once (HOMEPATH . "/factory/MailFactory.inc.php");
        $dbv = new DBCollection("SELECT * FROM Player WHERE id_BasicRace=6 AND id_City=" . $dbc->get("id_City"), $db);
        $pretre = new Player();
        $pretre->load($dbv->get("id"), $db);
        $body = "Vous avez été banni de votre lieu de résurrection par le gouverneur du village.\n Par conséquent, votre lieu de résurrection a été déplacé dans le temple situé en X=" .
             $x . ", Y=" . $y;
        
        foreach ($bannis as $key) {
            
            $dbp = new DBCollection("SELECT * FROM Player WHERE id=" . $key, $db);
            $dbu = new DBCollection("UPDATE Player SET resurrectid=" . $newTempleID . " WHERE id=" . $key, $db, 0, 0, false);
            $dest = array();
            $dest[$dbp->get("name")] = $dbp->get("name");
            MailFactory::sendMsg($pretre, localize("Lieu de résurrection"), $body, $dest, 2, $err, $db, - 1, 0);
            $namePl[] = $dbp->get("name");
        }
        
        if ($autoUpdate)
            $playerSrc->updateDB($db);
        
        $param["BANNIS"] = $bannis;
        $param["PLAYER"] = $namePl;
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = CONTROL_TEMPLE;
        $param["TYPE_ATTACK"] = CONTROL_TEMPLE_EVENT;
        
        return ACTION_NO_ERROR;
    }

    static function changeGouvernor($id_City, $playerName, $db)
    {
        $dbp = new DBCollection("SELECT * FROM Player WHERE name='" . $playerName . "'", $db);
        $dbu = new DBCollection("UPDATE City SET id_Player=" . $dbp->get("id") . " WHERE id=" . $id_City, $db, 0, 0, false);
    }

    static function updateCityNewAction($db)
    {
        $dbb = new DBCollection("SELECT * FROM Building WHERE profit!=''", $db);
        while (! $dbb->eof()) {
            $dba = new DBCollection("SELECT * FROM BuildingAction WHERE id_BasicBuilding=" . $dbb->get("id_BasicBuilding"), $db);
            $profit = unserialize($dbb->get("profit"));
            if ($dba->count() != count($profit)) {
                while (! $dba->eof()) {
                    if (! isset($profit[$dba->get("name")]))
                        $profit[$dba->get("name")] = $dba->get("profit");
                    
                    $dba->next();
                }
            }
            $dbu = new DBCollection("UPDATE Building SET profit='" . addslashes(serialize($profit)) . "' WHERE id=" . $dbb->get("id"), $db, 0, 0, false);
            $dbb->next();
        }
    }

    static function createGardForACity($id_city, $id_palais, $db)
    {
        /* creation d'un garde à la construction du palais */
        $dbt = new DBCollection("SELECT * FROM Building WHERE id_City=" . $id_city . " AND id =  $id_palais", $db);
        $npc = PNJFactory::getPNJFromLevel(100, 5 * $dbt->get("level"), $db);
        $npc->set("x", $dbt->get("x"));
        $npc->set("y", $dbt->get("y"));
        $npc->set("map", $dbt->get("map"));
        $npc->set("id_City", $id_city);
        $npc->set("id_BasicRace", 5);
        $npc->set("racename", "Garde du Palais");
        $npc->set("inbuilding", $id_palais);
        $npc->set("room", 24);
        $npc->updateHidden($db);
        $npc->updateDBr($db);
        PlayerFactory::NewATB($npc, $param2, $db);
    }

    static function createPriestForACity($id_city, $db)
    {
        /* creation d'un prêtre dans le temple à la construction du palais */
        $dbt = new DBCollection("SELECT * FROM Building WHERE id_City=" . $id_city . " AND name='Temple'", $db);
        $npc = PNJFactory::getPNJFromLevel(113, 6 * $dbt->get("level"), $db);
        $npc->set("x", $dbt->get("x"));
        $npc->set("y", $dbt->get("y"));
        $npc->set("map", $dbt->get("map"));
        $npc->set("id_City", $id_city);
        $npc->set("id_BasicRace", 6);
        $npc->set("racename", "Prêtre");
        $npc->set("inbuilding", $dbt->get("id"));
        $npc->set("room", 3);
        $npc->set("behaviour", 0);
        $npc->updateHidden($db);
        $npc->updateDBr($db);
        PlayerFactory::NewATB($npc, $param2, $db);
    }

    static function createAllPriest($db)
    {
        $dbb = new DBCollection("SELECT * FROM Building WHERE id_Player!=0 AND name='Temple'", $db);
        while (! $dbb->eof()) {
            $npc = new Player();
            $npc = PNJFactory::getPNJFromLevel(113, 6 * $dbb->get("level"), $db);
            $npc->set("x", $dbb->get("x"));
            $npc->set("y", $dbb->get("y"));
            $npc->set("map", $dbb->get("map"));
            $npc->set("id_City", $dbb->get("id_City"));
            $npc->set("id_BasicRace", 6);
            $npc->set("racename", "Prêtre");
            $npc->set("inbuilding", $dbb->get("id"));
            $npc->set("room", 3);
            $npc->set("behaviour", 0);
            $npc->updateHidden($db);
            $npc->addDBr($db);
            PlayerFactory::NewATB($npc, $param2, $db);
            
            $dbb->next();
        }
    }

    static function FillTalentCity($db)
    {
        $dbc = new DBCollection("SELECT * FROM Building WHERE id_BasicBuilding=8", $db);
        while (! $dbc->eof()) {
            $i = 1;
            unset($schoolList);
            $dbb = new DBCollection("SELECT * FROM BasicTalent ORDER BY RAND()", $db);
            while ($i < 6) {
                echo $i;
                $i ++;
                $schoolList[$dbb->get("id")] = $dbb->get("type");
                $dbb->next();
            }
            
            $dbu = new DBCollection("UPDATE Building SET school='" . serialize($schoolList) . "' WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
            $dbc->next();
        }
    }
}
?>
