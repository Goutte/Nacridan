<?php
require_once (HOMEPATH . "/lib/utils.inc.php");
require_once (HOMEPATH . "/lib/Behavior.inc.php");
require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
require_once (HOMEPATH . "/factory/InsideBuildingFactory.inc.php");

class PNJFactory
{

    static function deleteDBnpc(&$npc, &$param, &$db, $agresseur = null, $equipfall = 1)
    {
        if (isset($_SESSION["cq_players"][$npc->get("id")])) {
            unset($_SESSION["cq_players"][$npc->get("id")]);
        }
        
        if ($agresseur != null) {
            // Lancement de l'IA pour l'effet de la mort sur le groupe de monstre
            self::runIA($npc, $agresseur->get("id"), $db, 1);
            
            // Creation de la dépouille pour le cuir et l'écaille
            self::getCorpse($npc, $db);
            
            // Initialisation du drop
            $param["EQUIPMENT_FALL"] = 0;
            if ($equipfall) {
                $param["EQUIPMENT_FALL"] = self::getPNJdrop($npc, $param, $db);
                self::PNJdropArrow($npc, $db);
                $param["MONEY_FALL"] = self::getPNJmoney($npc, $db);
                PlayerFactory::looseMoney($npc, $param["MONEY_FALL"], $db);
            } else {
                $dbe = new DBCollection("SELECT id,extraname FROM Equipment WHERE id_Player=" . $npc->get("id"), $db);
                while (! ($dbe->eof())) {
                    if (! ($dbe->get("extraname") == ""))
                        $dbdt = new DBCollection("DELETE FROM Template WHERE id_Equipment=" . $dbe->get("id"), $db, 0, 0, false);
                    
                    $dbe->next();
                }
                
                $dbd = new DBCollection("DELETE FROM Equipment WHERE id_Player=" . $npc->get("id"), $db, 0, 0, false);
            }
        }
        
        // objet maudit tombe au sol
        $dbc = new DBCollection("SELECT id,name,extraname FROM Equipment WHERE maudit=1 AND id_Player=" . $npc->get("id"), $db);
        while (! $dbc->eof()) {
            if (! isset($param["EQUIPMENT_FALL"])) {
                $param["EQUIPMENT_FALL"] = 0;
            }
            if ($param["EQUIPMENT_FALL"] == 0) {
                $param["EQUIPMENT_FALL"] = 1;
                $param["DROP_NAME"] = "L'objet maudit " . $dbc->get("name") . ($dbc->get("extraname") == "" ? "" : " " . $dbc->get("extraname"));
            } else {
                $param["DROP_NAME"] .= "<br/>L'objet maudit " . $dbc->get("name") . ($dbc->get("extraname") == "" ? "" : " " . $dbc->get("extraname"));
            }
            $dbx = new DBCollection(
                "UPDATE Equipment SET id_Player=0, weared='NO', room=" . $npc->get("room") . ", inbuilding=" . $npc->get("inbuilding") . ", state='Ground', x=" . $npc->get("x") .
                     ", y=" . $npc->get("y") . ", map=" . $npc->get("map") . ", dropped='" . gmdate("Y-m-d H:i:s") . "' WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
            $dbc->next();
        }
        
        // Suppresion du BM si c'est un golem controlé
        if ($npc->get("id_Member") != 0 && $npc->get("id_BasicRace") == ID_BASIC_RACE_FEU_FOL) {
            
            $dbp = new DBCollection("DELETE FROM BM WHERE id_Player=" . $npc->get("id_Owner") . " AND name='Golem de Feu'", $db, 0, 0, false);
            $id = $npc->get("id_Owner");
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($id, $db);
            PlayerFactory::initBM($player, $player->getObj("Modifier"), $param2, $db, 0);
            echo "-" . $player->getObj("Modifier")->getAtt("magicSkill_bm", DICE_MULT);
            $player->updateDBr($db);
        }
        
        // Suppresion du BM si c'est une ame projetée
        if ($npc->get("id_Member") != 0 && $npc->get("id_BasicRace") == 112) {
            
            $dbp = new DBCollection("DELETE FROM BM WHERE id_Player=" . $npc->get("id_Owner") . " AND name='" . addslashes("Projection de l'âme") . "'", $db, 0, 0, false);
            $id = $npc->get("id_Owner");
            $player = new Player();
            $player->externDBObj("Modifier");
            $player->load($id, $db);
            PlayerFactory::initBM($player, $player->getObj("Modifier"), $param2, $db, 0);
            $player->updateDBr($db);
        }
        
        // Retour à la normal pour la personne dans la tortue
        if ($npc->get("id_BasicRace") == 263) {
            $dbn = new DBCollection("SELECT id, state FROM Player WHERE id=" . $npc->get("id_Owner"), $db);
            if ($dbn->count() > 0 && $dbn->get("state") == "turtle")
                $dbp = new DBCollection("UPDATE Player SET hidden=hidden-3, state='walking' WHERE id=" . $npc->get("id_Owner"), $db, 0, 0, false);
            
            /*
             * $dbc = new DBCollection("SELECT * FROM Caravan WHERE id_Player=" . $npc->get("id_Owner"), $db, 0, 0);
             * if ($dbc->count()) {
             * $dbdp = new DBCollection("DELETE FROM Equipment WHERE id_BasicEquipment=600 AND id_Player=" . $npc->get("id_Owner") . " AND id_Caravan=" . $dbc->get("id"), $db, 0,
             * 0, false);
             * $dbdc = new DBCollection("DELETE FROM Caravan WHERE id=" . $dbc->get("id"), $db, 0, 0, false);
             * }
             */
        }
        
        if ($npc->get("status") == "NPC" && ($npc->get("racename") == "Garde du Palais" || $npc->get("racename") == "Prêtre") && $agresseur != null) {
            $building = new Building();
            $building->load($npc->get("inbuilding"), $db);
            
            $city = new City();
            $city->load($building->get("id_City"), $db);
            
            $subject = "Le " . $npc->get("racename") . " de " . $city->get("name") . " a été tué!";
            $body = "\nLe " . $npc->get("racename") . " de " . $city->get("name") . " située en " . $city->get("x") . "/" . $city->get("y") . " a été tué par " .
                 $agresseur->get("name") . "(" . $agresseur->get("id") . ")";
            
            InsideBuildingFactory::informCityMember($building->get("id_City"), $subject, $body, $db);
        }
        
        if ($npc->get("racename") == "Prêtre" && $agresseur != null) {
            $xp = $npc->get("x");
            $yp = $npc->get("y");
            
            // Détermination du temple non capturé le plus proche
            $dbnt = new DBCollection(
                "SELECT id,x,y, id_City, ((abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2) AS dist FROM Building WHERE name='Temple' ORDER BY dist", 
                $db);
            $done = 0;
            while (! $done) {
                $dbcity = new DBCollection("SELECT * FROM City WHERE id=" . $dbnt->get("id_City"), $db);
                if ($dbcity->get("captured") < 4) {
                    $newTempleID = $dbnt->get("id");
                    $x = $dbnt->get("x");
                    $y = $dbnt->get("y");
                    $done = 1;
                }
                $dbnt->next();
            }
            
            require_once (HOMEPATH . "/factory/MailFactory.inc.php");
            
            $body = "Le prêtre responsable du temple où vous aviez fixé votre lieu de résurrection a été tué.\n Par conséquent, votre lieu de résurrection a été déplacé dans le temple situé en X=" .
                 $x . ", Y=" . $y;
            
            $dbp = new DBCollection("SELECT * FROM Player WHERE resurrectid=" . $npc->get("inbuilding"), $db);
            while (! $dbp->eof()) {
                $dest = array();
                $dest[$dbp->get("name")] = $dbp->get("name");
                MailFactory::sendMsg($npc, localize("Lieu de résurrection"), $body, $dest, 2, $err, $db, - 1, 0);
                $dbp->next();
            }
            
            $dbp = new DBCollection("UPDATE Player SET resurrectid=" . $newTempleID . " WHERE resurrectid=" . $npc->get("inbuilding"), $db, 0, 0, false);
            $date = gmdate("Y-m-d H:i:s");
            
            $dbci = new DBCollection("SELECT id_City FROM Building WHERE id=" . $npc->get("inbuilding"), $db);
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" .
                     ($agresseur != null ? $agresseur->get("id") : null) . "," . $dbci->get("id_City") . "," . $npc->get("inbuilding") . "," . PRIEST_DEATH . "," .
                     PRIEST_DEATH_EVENT . ",0,0,'" . $date . "')", $db, 0, 0, false);
        }
        
        if ($npc->get("racename") == "Garde du Palais" && $agresseur != null) {
            $date = gmdate("Y-m-d H:i:s");
            $dbci = new DBCollection("SELECT id_City FROM Building WHERE id=" . $npc->get("inbuilding"), $db);
            $dbu = new DBCollection(
                "INSERT INTO BuildingEvent (id_Player, id_City, id_Building, id_BuildingAction, id_BasicEvent, price, profit, date) VALUES (" . $agresseur->get("id") . "," .
                     $dbci->get("id_City") . "," . $npc->get("inbuilding") . "," . GUARD_DEATH . "," . GUARD_DEATH_EVENT . ",0,0,'" . $date . "')", $db, 0, 0, false);
        }
        
        $dbd = new DBCollection("DELETE FROM Ability WHERE id_Player=" . $npc->get("id"), $db, 0, 0, false);
        $dbd = new DBCollection("DELETE FROM MagicSpell WHERE id_Player=" . $npc->get("id"), $db, 0, 0, false);
        $dbd = new DBCollection("DELETE FROM BM WHERE id_Player=" . $npc->get("id"), $db, 0, 0, false);
        // suppression des bonus exorcisme qu'il a créé
        $dbd = new DBCollection("DELETE FROM BM WHERE BM.id_Player\$src=" . $npc->get("id") . "  AND BM.name='" . addslashes("Exorcisme de l'ombre") . "'", $db, 0, 0, false);
        $npc->resetExternDBObj();
        $npc->externDBObj("Modifier,Upgrade");
        $npc->deleteDBr($db);
    }

    static function PNJdropArrow($npc, &$db)
    {
        $date = gmdate("Y-m-d H:i:s");
        $dbm = new DBCollection(
            "UPDATE Equipment SET dropped='" . $date . "', id_Player=0, inbuilding=" . $npc->get("inbuilding") . ", room=" . $npc->get("room") . ", x=" . $npc->get("x") . ", y=" .
                 $npc->get("y") . ", map=" . $npc->get("map") . ", state='Ground' WHERE id_EquipmentType=28 AND id_Player=" . $npc->get("id"), $db, 0, 0, false);
        // Bolas
        $dbm = new DBCollection(
            "UPDATE Equipment SET dropped='" . $date . "', id_Player=0, inbuilding=" . $npc->get("inbuilding") . ", room=" . $npc->get("room") . ", x=" . $npc->get("x") . ", y=" .
                 $npc->get("y") . ", map=" . $npc->get("map") . ", state='Ground' WHERE id_BasicEquipment=205 AND id_Player=" . $npc->get("id"), $db, 0, 0, false);
    }

    static function getCorpse($npc, &$db)
    {
        $date = gmdate('Y-m-d H:i:s');
        $dbm = new DBCollection("SELECT dropItem FROM BasicRace WHERE id=" . $npc->get("id_BasicRace"), $db);
        if ($dbm->get("dropItem") == 1 or $dbm->get("dropItem") == 2) {
            $exp = new Exploitation();
            $exp->set("x", $npc->get("x"));
            $exp->set("y", $npc->get("y"));
            $exp->set("date", $date);
            $exp->set("map", $npc->get("map"));
            $exp->set("level", $npc->get("level"));
            $exp->set("type", "body");
            $exp->set("value", mt_rand(1, 3));
            if ($dbm->get("dropItem") == 1)
                $exp->set("name", "Cuir");
            else
                $exp->set("name", "Ecaille");
            
            $exp->addDB($db);
        }
    }

    static function getPNJdrop($npc, &$param, &$db)
    {
        $drop = new Equipment();
        $drop->set("x", $npc->get("x"));
        $drop->set("y", $npc->get("y"));
        $drop->set("map", $npc->get("map"));
        $drop->set("date", $date = gmdate("Y-m-d H:i:s"));
        $drop->set("dropped", $date = gmdate("Y-m-d H:i:s"));
        $drop->set("state", "ground");
        $drop->set("durability", mt_rand(10, 50));
        
        $dbm = new DBCollection("SELECT dropItem, percent FROM BasicRace WHERE id=" . $npc->get("id_BasicRace"), $db);
        $id = $dbm->get("dropItem");
        
        if ($id >= 499) {
            $drop->set("level", 1);
            if ($id > 600) {
                $drop->set("level", mt_rand(1, max(2, floor(log($npc->get("level"), 2)) - 1)));
                $nb = mt_rand(1, 3);
                for ($i = 1; $i < $nb; $i ++)
                    $id = floor($id / 100);
                
                $id = $id % 100;
            }
            
            $dbe = new DBCollection("SELECT * FROM BasicEquipment WHERE id=" . $id, $db);
            $drop->set("id_BasicEquipment", $dbe->get("id"));
            $drop->set("id_EquipmentType", $dbe->get("id_EquipmentType"));
            $drop->set("name", $dbe->get("name"));
            $param["DROP_NAME"] = $dbe->get("name");
        }
        
        if ($npc->get("id_BasicRace") == 153) {
            $drop->set("id_BasicEquipment", 101);
            $drop->set("id_EquipmentType", 30);
            $drop->set("name", "Rubis");
            $drop->set("level", mt_rand(1, max(2, floor(log($npc->get("level"), 2)) - 1)));
            $param["DROP_NAME"] = "Rubis";
        }
        
        if ($npc->get("id_BasicRace") == 263) {
            $dbcg = new DBCollection(
                "UPDATE Equipment SET inbuilding=" . $npc->get("inbuilding") . ", room=" . $npc->get("room") . ", state='Ground', x=" . $npc->get("x") . ", y=" . $npc->get("y") .
                     ", map=" . $npc->get("map") . ", id_Player=0, dropped=NOW() WHERE id_Player=" . $npc->get("id"), $db, 0, 0, false);
            $param["DROP_NAME"] = "Chargement";
            return $dbcg->count();
        }
        if ($drop->get("id_BasicEquipment") != 0 && mt_rand(1, 100) <= $dbm->get("percent")) {
            
            $drop->addDBr($db);
            return 1;
        }
        
        return 0;
    }

    static function getPNJmoney($npc, &$db)
    {
        $money = 0;
        switch ($npc->get("id_BasicRace")) {
            case 103:
                $money = mt_rand(2, 4);
                break;
            case 108:
                $money = mt_rand(5, 8);
                break;
            case 118:
                $money = mt_rand(9, 13);
            case 134:
                $money = mt_rand(14, 19);
                break;
        }
        
        if ($npc->get("money") > 0)
            $money = $npc->get("money");
        
        return $money;
    }

    static function getPNJFromLevel($idrace, $level, $db)
    {
        $modifier = new Modifier();
        $race = new BasicRace();
        $race->load($idrace, $db);
        $profil = new BasicProfil();
        $profil->load($race->get("id_BasicProfil"), $db);
        $npc = new Player();
        $npc->set("level", $level);
        
        // Base
        $modifier->setModif("hp", DICE_ADD, 10 + 2 * $level);
        $modifier->setModif("dexterity", DICE_D6, 2);
        $modifier->setModif("strength", DICE_D6, 1);
        $modifier->setModif("speed", DICE_D6, 1);
        $modifier->setModif("armor", DICE_ADD, floor($level / 5));
        $modifier->setModif("magicSkill", DICE_D6, 2);
        
        // Distribution des caracs de base
        $charac = array();
        $charac[0] = "hp";
        $charac[1] = "strength";
        $charac[2] = "dexterity";
        $charac[3] = "speed";
        $charac[4] = "magicSkill";
        
        $bonus = min($level, 8) + floor((max($level - 8, 0) / 3));
        for ($i = 0; $i < 2 * $level + $bonus; $i ++) {
            $choice = mt_rand(1, 15);
            $j = 0;
            
            while ($choice > 0) {
                if ($choice <= $profil->get($charac[$j]))
                    $att = $charac[$j];
                
                $choice -= $profil->get($charac[$j]);
                
                $j ++;
            }
            if ($att == "hp")
                $modifier->addModif($att, DICE_ADD, 15);
            else
                $modifier->addModif($att, DICE_D6, 1);
            
            if (mt_rand(0, 100) <= $profil->get("armor") * 10)
                $modifier->addModif("armor", DICE_ADD, 1);
        }
        
        $upgrade = new Upgrade();
        $npc->setObj("Modifier", $modifier, true);
        $npc->set("id_Upgrade", 0);
        $npc->set("behaviour", $race->get("behaviour"));
        $npc->set("fightStyle", $profil->get("fightStyle"));
        $npc->set("currhp", $modifier->getModif("hp", DICE_ADD));
        $npc->set("hp", $modifier->getModif("hp", DICE_ADD));
        $npc->set("id_BasicRace", $idrace);
        $npc->set("racename", $race->get("name"));
        $npc->set("name", NULL);
        $npc->set("status", "NPC");
        $npc->set("gender", $race->get("gender"));
        
        // $npc->set("state", $race->get("behavior"));
        $datetime = time();
        $npc->set("creation", gmdate("Y-m-d H:i:s", $datetime));
        $datetime += mt_rand(0, + 3600 * 6);
        $npc->set("curratb", gmdate("Y-m-d H:i:s", $datetime - 3600));
        $npc->set("playatb", gmdate("Y-m-d H:i:s", $datetime - 3600));
        $npc->set("nextatb", gmdate("Y-m-d H:i:s", $datetime - 3600));
        $npc->set("atb", 720);
        
        $npc->set("ap", 12 + floor($npc->getObj("Modifier")
            ->getModif("speed", DICE_D6) / 10));
        
        return $npc;
    }

    static function addBonusPNJ(&$modifier, $id_Race, $level, $db)
    {
        $profil = new BasicProfil();
        $profil->load($id_Race, $db);
        $charac = array();
        $charac[0] = "attack_bm";
        $charac[1] = "defense_bm";
        $charac[2] = "damage_bm";
        $charac[3] = "magicSkill_bm";
        
        for ($i = 0; $i < 4; $i ++) {
            if ($profil->get($charac[$i]) != 0) {
                for ($j = 0; $j < $level; $j ++) {
                    $nb = mt_rand(0, 100);
                    if ($nb <= $profil->get($charac[$i]) * 20) {
                        $modifier->addModif($charac[$i], DICE_ADD, 1);
                    }
                }
            }
        }
    }

    static function runIA($player, $id_Agresseur, $db, $dead = 0)
    {
        if ($player->get("id_BasicRace") == 253 && ! $dead) {
            require_once (HOMEPATH . "/factory/MailFactory.inc.php");
            $dest = array();
            $agg = new Player();
            $agg->load($id_Agresseur, $db);
            $dest[$agg->get("name")] = $agg->get("name");
            $dbc = new DBCollection("SELECT * FROM Talent WHERE id_Player=" . $id_Agresseur . " AND id_BasicTalent=5", $db);
            if ($dbc->eof())
                MailFactory::sendMsg($player, localize("Hrukug !"), "Keuw Sjra rikaxa bwsa grwedek !.", $dest, 0, $err2, $db, - 1, 0);
            else
                MailFactory::sendMsg($player, localize("Hé !"), "Pourquoi toi taper moi, moi rien fait !.", $dest, 0, $err2, $db, - 1, 0);
        }
        self::changeTarget($player, $id_Agresseur, $db, $dead);
        
        // $behavior = new Behavior($db);
        // $behavior->setRiposte(1);
        // $behavior->playNPC($player->get("id"));
    }

    static function changeTarget($player, $id_Agresseur, $db, $dead)
    {
        
        // Monstres de niveau 1 et 2
        if ($player->get("id_Player\$target") == 0 && $player->get("level") <= 2)
            $player->set("id_Player\$target", $id_Agresseur);
            
            // Kradjeck ferreux
        if ($player->get("id_Player\$target") == 0 && $player->get("id_BasicRace") == 253)
            $player->set("id_Player\$target", $id_Agresseur);
            
            // Chauves-souris
        if ($player->get("id_BasicRace") == 102) {
            if ($player->get("id_Player\$target") == 0)
                $player->set("id_Player\$target", $id_Agresseur);
            
            if ($player->get("id_Player\$target") != 0) {
                $rnd = mt_rand(0, 9);
                if ($rnd < 3)
                    $player->set("id_Player\$target", $id_Agresseur);
            }
        }
        // Rat géant
        if ($player->get("id_BasicRace") == 100) {
            if ($player->get("id_Player\$target") == 0)
                $player->set("id_Player\$target", $id_Agresseur);
            
            if ($player->get("id_Player\$target") != 0) {
                $rnd = mt_rand(0, 9);
                if ($rnd < 8)
                    $player->set("id_Player\$target", $id_Agresseur);
            }
        }
        // Crapaud géant
        if ($player->get("id_BasicRace") == 101 && $player->get("id_Player\$target") == 0) {
            $rnd = mt_rand(0, 9);
            if ($rnd < 5)
                $player->set("id_Player\$target", $id_Agresseur);
        }
        
        // Loup
        if ($player->get("id_BasicRace") == 104) {
            if ($player->get("id_Player\$target") == 0)
                $player->set("id_Player\$target", $id_Agresseur);
            if ($dead) {
                $dbl = new DBCollection("SELECT * FROM Player WHERE id_Team =" . $player->get("id_Team") . " AND id_BasicRace=104", $db);
                while (! $dbl->eof()) {
                    $rnd = mt_rand(1, 10);
                    if ($rnd <= 6 && distHexa($player->get("x"), $player->get("y"), $dbl->get("x"), $dbl->get("y")) <= 3)
                        $dblu = new DBCollection("UPDATE Player SET id_Player\$target=" . $id_Agresseur . " WHERE id =" . $dbl->get("id"), $db, 0, 0, false);
                    $dbl->next();
                }
            }
        }
        
        // Araignée éclipsante
        if ($player->get("id_BasicRace") == 105 && $player->get("id_Player\$target") == 0) {
            $rnd = mt_rand(0, 9);
            if ($rnd < 5)
                $player->set("id_Player\$target", $id_Agresseur);
        }
        
        // Kobold
        if ($player->get("id_BasicRace") == 103 && $player->get("id_Player\$target") == 0) {
            $rnd = mt_rand(0, 9);
            if ($rnd < 5)
                $player->set("id_Player\$target", $id_Agresseur);
        }
        
        // Araignée Piègeuse
        if ($player->get("id_BasicRace") == 111) {
            $player->set("id_Player\$target", $id_Agresseur);
        }
        
        $player->updateDB($db);
    }

    static function infest($playerSrc, $opponent, &$param, &$db)
    {
        
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        $date = gmdate('Y-m-d H:i:s');
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
            $bm = new BM();
            $bm->set("level", floor($playerSrc->get("level") / 3));
            $bm->set("value", floor($playerSrc->get("level") / 3));
            $bm->set("name", "infesté");
            $bm->set("effect", "NEGATIVE");
            $bm->set("life", "-2");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->set("id_StaticModifier", 22);
            $bm->set("id_Player", $opponent->get("id"));
            $opponent->addBM($bm, 2, $param, $db);
            
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $param["TYPE_ACTION"] = M_INFEST;
            $param["TYPE_ATTACK"] = M_INFEST_EVENT;
            PNJFactory::DeleteDBnpc($playerSrc, $param, $db, null, 0);
            $ret = 0;
        } else {
            $param["IDENT"] = 0;
            $param["PLAYER_KILLED"] = 0;
            $param["TYPE_ACTION"] = ATTACK;
            $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
            PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            $ret = 1;
        }
        
        $opponent->updateDBr($db);
        return $ret;
    }

    static function spit($playerSrc, $opponent, &$param, &$db)
    {
        
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        $date = gmdate('Y-m-d H:i:s');
        if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"] + 1) {
            $level = mt_rand($playerSrc->get("level") / 2, $playerSrc->get("level"));
            
            $bm = new BM();
            $bm->set("level", $level);
            $bm->set("effect", "NEGATIVE");
            $bm->set("value", 3 * $level);
            $bm->set("name", "Poison");
            $bm->set("life", "2");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->set("id_StaticModifier", 0);
            $bm->set("id_Player", $opponent->get("id"));
            $opponent->addBM($bm, 1, $param, $db);
            
            $param["TYPE_ACTION"] = M_SPIT;
            $param["TYPE_ATTACK"] = M_SPIT_EVENT;
            $ret = 0;
        } else {
            $param["IDENT"] = 0;
            $param["PLAYER_KILLED"] = 0;
            $param["TYPE_ACTION"] = ATTACK;
            $param["TYPE_ATTACK"] = ATTACK_EVENT_FAIL;
            PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            $ret = 1;
        }
        
        $opponent->updateDBr($db);
        return $ret;
    }

    static function bier($playerSrc, &$param, &$db)
    {
        
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        
        $date = gmdate('Y-m-d H:i:s');
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        $dbe = new DBCollection(
            "SELECT id FROM Player WHERE status='PC' AND map=" . $playerSrc->get("map") . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp .
                 "))/2=1 AND inbuilding=0 AND (hidden=0 or hidden=10) AND disabled=0", $db);
        $param["NUMBER_OPPONENT"] = $dbe->count();
        $i = 0;
        while (! $dbe->eof()) {
            
            $opp = new Player();
            $opp->externDBObj("Modifier");
            $ret = $opp->load($dbe->get("id"), $db);
            $param["LEVEL"] = mt_rand(floor($playerSrc->get("level") / 10), floor($playerSrc->get("level") / 5));
            $level = $param["LEVEL"];
            
            $bm = new BM();
            $bm->set("level", $level);
            $bm->set("value", 0);
            $bm->set("effect", "NEGATIVE");
            $bm->set("name", "Vapeur de bière");
            $bm->set("life", "2");
            $bm->set("id_Player\$src", $playerSrc->get("id"));
            $bm->set("date", gmdate("Y-m-d H:i:s"));
            $bm->set("id_StaticModifier", 12);
            $bm->set("id_Player", $opp->get("id"));
            $opp->addBM($bm, 1, $param, $db);
            
            $param["ERROR"] = 0;
            $param["TYPE_ACTION"] = PASSIVE_BIER;
            $param["TYPE_ATTACK"] = PASSIVE_BIER_EVENT;
            
            BasicActionFactory::globalInfoOpponent($opp, $param);
            Event::logAction($db, PASSIVE_BIER, $param);
            
            $dbe->next();
        }
        
        $param["TYPE_ACTION"] = M_BIER;
        $param["TYPE_ATTACK"] = M_BIER_EVENT;
    }

    static function lightning($playerSrc, $opponent, &$param, &$db)
    {
        
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        
        $date = gmdate('Y-m-d H:i:s');
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        $deltaX = $xp - $opponent->get("x");
        $deltaY = $yp - $opponent->get("y");
        
        $param["PLAYER_DAMAGE"] = floor($playerSrc->getScore("magicSkill") / 2);
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("defense");
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        
        $dbe = new DBCollection(
            "SELECT id FROM Player WHERE map=" . $playerSrc->get("map") . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp .
                 "))/2=1 AND inbuilding=0 AND (hidden=0 or hidden=10) AND disabled=0", $db);
        $param["NUMBER_OPPONENT"] = $dbe->count();
        $i = 0;
        while (! $dbe->eof()) {
            
            $opp = new Player();
            $opp->externDBObj("Modifier");
            $ret = $opp->load($dbe->get("id"), $db);
            $level = mt_rand(floor($playerSrc->get("level") / 10), floor($playerSrc->get("level") / 5));
            
            $dbbm = new DBCollection("SELECT * FROM BM WHERE id_Player=" . $opp->get("id") . " AND id_StaticModifier_BM=12", $db);
            if ($dbbm->count() == 1)
                $dbbm = new DBCollection("UPDATE BM SET life+=2, level+=" . $level . " WHERE id_Player=" . $opp->get("id") . " AND id_StaticModifier_BM=12", $db, 0, 0, false);
            else {
                $bm = new BM();
                $bm->set("level", $level);
                $bm->set("name", "Vapeur de bière");
                $bm->set("life", "2");
                $bm->set("id_Player\$src", $playerSrc->get("id"));
                $bm->set("date", gmdate("Y-m-d H:i:s"));
                $bm->set("id_StaticModifier", 12);
                $bm->set("id_Player", $opp->get("id"));
                $bm->addDBr($db);
            }
            
            $param["ERROR"] = 0;
            $param["TYPE_ACTION"] = PASSIVE_LIGHTNING;
            $param["TYPE_ATTACK"] = PASSIVE_LIGHTNING_EVENT;
            
            PlayerFactory::initBM($opp, $opp->getObj("Modifier"), $param, $db, 0);
            BasicActionFactory::globalInfoOpponent($opp, $param);
            Event::logAction($db, PASSIVE_LIGHTNING, $param);
            
            $dbe->next();
        }
        
        $param["TYPE_ACTION"] = M_LIGTHNING;
        $param["TYPE_ATTACK"] = M_LIGHTNING_EVENT;
    }

    static function drain($playerSrc, $opponent, &$param, &$db)
    {
        // Initialisation des params info de player
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        
        $param["PLAYER_SKILL"] = $playerSrc->getScore("magicSkill");
        $hp = floor($param["PLAYER_SKILL"] / 2);
        $opponent->set("currhp", max(1, $opponent->get("currhp") - 20));
        PlayerFactory::addHP($playerSrc, $hp, $db, 0);
        $param["TYPE_ACTION"] = M_DRAIN;
        $param["TYPE_ATTACK"] = M_DRAIN_EVENT;
        $opponent->updateDBr($db);
    }

    static function grapnel($playerSrc, $opponent, $x, $y, &$param, &$db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        BasicActionFactory::globalInfoOpponent($opponent, $param);
        $param["TYPE_ACTION"] = M_GRAPNEL;
        
        $param["X"] = $x;
        $param["Y"] = $y;
        
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        $param["TARGET_DEFENSE"] = $opponent->getScore("defense");
        $param["MOVE"] = 0;
        if ($param["PLAYER_ATTACK"] > $param["TARGET_DEFENSE"]) {
            $param["TYPE_ATTACK"] = M_GRAPNEL_EVENT_SUCCESS;
            require_once (HOMEPATH . "/lib/MapInfo.inc.php");
            $mapinfo = new MapInfo($db);
            $validzone = $mapinfo->getValidMap($x, $y, 0, $playerSrc->get("map"));
            if ($validzone[0][0] && BasicActionFactory::freePlace($x, $y, $playerSrc->get("map"), $db)) {
                $opponent->set("x", $x);
                $opponent->set("y", $y);
                $opponent->updateDB($db);
                if ($opponent->get("id_BasicRace") == 263 && $opponent->get("state") == "turtle")
                    $dbc = new DBCollection("UPDATE Player SET x=" . $x . ", y=" . $y . " WHERE id=" . $opponent->get("id_Owner"), $db, 0, 0, false);
                $param["MOVE"] = 1;
            }
        } else {
            $param["TYPE_ATTACK"] = M_GRAPNEL_EVENT_FAIL;
        }
        
        $playerSrc->updateDbr($db);
        return ACTION_NO_ERROR;
    }

    public function brasier($playerSrc, &$param, $db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        // Jets de dés
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $param["PLAYER_ATTACK"] = $playerSrc->getScore("attack");
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, ceil($param["SPELL_LEVEL"]));
        
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $dbp = new DBCollection("SELECT id FROM Player WHERE status='PC' AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 3", $db);
        $nb = 0;
        while (! $dbp->eof() && $nb < 3) {
            $nb ++;
            $opponent->load($dbp->get("id"), $db);
            BasicActionFactory::globalInfoOpponent($opponent, $param[$nb]);
            BasicActionFactory::globalInfoOpponent($opponent, $param);
            
            $param2 = array();
            $param2["XP"] = 0;
            $param["TARGET_DEFENSE"][$nb] = $opponent->getScore("defense");
            $param["KILLED"][$nb] = 0;
            $param["SELF"] = 0;
            if ($param["PLAYER_ATTACK"] >= $param["TARGET_DEFENSE"][$nb] + 1) {
                $param["TARGET_DAMAGE"][$nb] = floor($modifier->getScoreWithNoBM("hp"));
                $param["TARGET_DAMAGETOT2"] = $param["TARGET_DAMAGE"][$nb];
                // Si l'adversaire possède une bulle de vie
                require_once (HOMEPATH . "/factory/SpellFactory.inc.php");
                if (SpellFactory::PassiveBubble($opponent, $param, $db)) {
                    $param["BUBBLE_LIFE2"][$nb] = $param["BUBBLE_LIFE"];
                    $param["BUBBLE_CANCEL2"][$nb] = $param["BUBBLE_CANCEL"];
                    $param["BUBBLE_LEVEL2"][$nb] = $param["BUBBLE_LEVEL"];
                } else
                    $param["BUBBLE_LIFE"] = 0;
                
                $param["TARGET_DAM"] = $param["TARGET_DAMAGE"][$nb];
                $param["TARGET_DAM2"] = $param["TARGET_DAMAGETOT2"];
                $param["TARGET_DAMAGE2"][$nb] = $param["TARGET_DAMAGETOT2"];
                
                $hp = $opponent->get("currhp") - $param["TARGET_DAMAGETOT2"];
                if ($param["TARGET_DAMAGETOT2"] > 0)
                    PlayerFactory::Enhance($opponent, DEFENSE_EVENT_FAIL, $param, $db);
                    // Mort de la cible
                if ($hp <= 0) {
                    BasicActionFactory::kill($playerSrc, $opponent, $param2, $db);
                    $param["KILLED"][$nb] = 1;
                } else {
                    $param["TYPE_ATTACK"] = SPELL_FIRE_EVENT_SUCCESS;
                    PlayerFactory::loseHP($opponent, $playerSrc, $param["TARGET_DAMAGETOT2"], $param, $db);
                    $opponent->updateDBr($db);
                }
            } else {
                $param["TYPE_ATTACK"] = SPELL_FIRE_EVENT_FAIL;
                PlayerFactory::Enhance($opponent, DEFENSE_EVENT_SUCCESS, $param, $db);
            }
            
            $param["ERROR"] = 0;
            $param["TYPE_ACTION"] = PASSIVE_FIRE;
            $param["TARGET_DEF"] = $param["TARGET_DEFENSE"][$nb];
            Event::logAction($db, PASSIVE_FIRE, $param);
            unset($param2);
            $dbp->next();
        }
        
        $param["TYPE_ACTION"] = SPELL_FIRE;
        $param["TYPE_ATTACK"] = SPELL_FIRE_EVENT;
        $param["NB"] = $nb;
        
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    public function pluie($playerSrc, &$param, $db)
    {
        // Initialisation des params infos player et opponent
        BasicActionFactory::globalInfo($playerSrc, $param);
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        // Jets de dés
        $param["PLAYER_MM"] = $playerSrc->getScore("magicSkill");
        $param["SPELL_LEVEL"] = 1 + floor($param["PLAYER_MM"] / 8);
        $modifier = new Modifier();
        $modifier->addModif("hp", DICE_D6, ceil($param["SPELL_LEVEL"]));
        $param["HP_POTENTIEL"] = ceil($modifier->getScoreWithNoBM("hp") / 2);
        $nb = 0;
        $opponent = new Player();
        $opponent->externDBObj("Modifier");
        $dbp = new DBCollection(
            "SELECT id FROM Player WHERE id_Team=" . $playerSrc->get("id_Team") . " AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 5", $db);
        while (! $dbp->eof()) {
            $nb ++;
            $opponent->load($dbp->get("id"), $db);
            BasicActionFactory::globalInfoOpponent($opponent, $param[$nb]);
            BasicActionFactory::globalInfoOpponent($opponent, $param);
            
            if ($playerSrc->get("id") == $opponent->get("id")) {
                $param["SELF"][$nb] = 1;
                $param["HP_HEAL"][$nb] = PlayerFactory::addHP($playerSrc, $param["HP_POTENTIEL"], $db, 0);
            } else {
                $param["SELF"][$nb] = 0;
                $param["HP_HEAL"][$nb] = PlayerFactory::addHP($opponent, $param["HP_POTENTIEL"], $db, 0);
                $opponent->updateDB($db);
            }
            
            $param["ERROR"] = 0;
            $param["TYPE_ACTION"] = PASSIVE_RAIN;
            $param["TYPE_ATTACK"] = PASSIVE_RAIN_EVENT;
            $param["HP_HL"] = $param["HP_HEAL"][$nb];
            if ($param["SELF"][$nb] != 1)
                Event::logAction($db, PASSIVE_FIRE, $param);
            
            $dbp->next();
        }
        
        $param["NB"] = $nb;
        $param["TYPE_ACTION"] = SPELL_RAIN;
        $param["TYPE_ATTACK"] = SPELL_RAIN_EVENT;
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }

    public function Neant($playerSrc, &$param, $db)
    {
        BasicActionFactory::globalInfo($playerSrc, $param);
        
        $cond = "(name='Soleil de guérison' or name='Bénédiction' or name='Charme de vitalité' or name='Ailes de Colère' or name='Bouclier magique' or name='Barrière enflammée'";
        $cond .= " or name='Aura de résistance' or name='Aura de courage' or id_StaticModifier_BM = 9)";
        $xp = $playerSrc->get("x");
        $yp = $playerSrc->get("y");
        
        $dbp = new DBCollection("SELECT id FROM Player WHERE status='PC' AND (abs(x-" . $xp . ") + abs(y-" . $yp . ") + abs(x+y-" . $xp . "-" . $yp . "))/2 <= 4", $db);
        $nb = 0;
        require_once (HOMEPATH . "/factory/AbilityFactory.inc.php");
        $param2 = array();
        while (! $dbp->eof()) {
            unset($param2);
            $opponent = new Player();
            $opponent->externDBObj("Modifier");
            $opponent->load($dbp->get("id"), $db);
            $dbg = $dbc = new DBCollection("SELECT * FROM BM WHERE " . $cond . " AND id_Player=" . $opponent->get("id"), $db);
            $nb = 0;
            while (! $dbg->eof()) {
                $param2["BM_NAME"][$nb] = $dbg->get("name");
                if ($dbg->get("name") == 'Aura de courage')
                    AbilityFactory::DisabledBravery($opponent, $param2, $db);
                if ($dbg->get("name") == 'Aura de résistance')
                    AbilityFactory::DisabledResistance($opponent, $param2, $db);
                
                $nb ++;
                $dbg->next();
            }
            
            $dbx = new DBCollection("DELETE FROM BM WHERE id_Player=" . $opponent->get("id") . " AND " . $cond, $db, 0, 0, false);
            PlayerFactory::initBM($opponent, $opponent->getObj("Modifier"), $param, $db, 0);
            $opponent->updateDB($db);
            
            BasicActionFactory::globalInfo($playerSrc, $param2);
            BasicActionFactory::globalInfoOpponent($opponent, $param2);
            $param2["ERROR"] = 0;
            $param2["TYPE_ACTION"] = PASSIVE_VOID;
            $param2["TYPE_ATTACK"] = PASSIVE_VOID_EVENT;
            Event::logAction($db, PASSIVE_VOID, $param2);
            
            $dbp->next();
        }
        
        $param["ERROR"] = 0;
        $param["TYPE_ACTION"] = M_VOID;
        $param["TYPE_ATTACK"] = M_VOID_EVENT;
        $playerSrc->updateDBr($db);
        return ACTION_NO_ERROR;
    }
}
?>
